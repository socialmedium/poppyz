<?php
/**
 * @package Poppyz
 */
/*
Plugin Name: Poppyz
Plugin URI: https://poppyz.nl/
Description: Course plugin with payment
Version: 1.9.4.9
Author: Levie Company
Author URI: https://simonelevie.nl/
License: Commercial
Text Domain: poppyz
*/


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
define ('PPY_VERSION' , '1.9.4.9');


define ('PPY_DIR_PATH' , plugin_dir_path( __FILE__ ));
define ('PPY_DIR_URL' , plugin_dir_url( __FILE__ ));

define ('PPY_PREFIX' , 'ppy_');
define ('PPY_COURSE_PT' , 'ppy_course');
define ('PPY_LESSON_PT' , 'ppy_lesson');
define ('PPY_LESSON_CAT' , 'ppy_lesson_category');
define ('PPY_TIER_PT' , 'ppy_tier');
define ('PPY_COUPON_PT' , 'ppy_coupon');
define ('PPY_INVOICE_PT' , 'ppy_invoice');
define ('PPY_WEBSITE' , 'https://poppyz.nl');
define ('PPY_ITEM_NAME' , 'Poppyz');
define ('PPY_ITEM_ID' , 4333);

// 9531 1
//9536 3


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-poppyz-activator.php
 */
function activate_poppyz($networkwide) {
    global $ppy_lang;
    $ppy_lang->load_plugin_textdomain();
	require(PPY_DIR_PATH . 'includes/class-poppyz-activator.php');
	Poppyz_Activator::activate($networkwide);
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-poppyz-deactivator.php
 */
function deactivate_poppyz() {
    require_once (PPY_DIR_PATH . 'includes/class-poppyz-deactivator.php');
	Poppyz_Deactivator::deactivate();
	wp_clear_scheduled_hook( 'ppy_send_payment_reminders' );
}

register_activation_hook( __FILE__, 'activate_poppyz' );
register_deactivation_hook( __FILE__, 'deactivate_poppyz' );


function new_blog( $blog_id, $user_id, $domain, $path, $site_id, $meta ) {
	require_once(PPY_DIR_PATH . 'includes/class-poppyz-activator.php');
	Poppyz_Activator::new_blog( $blog_id );
}
add_action( 'wpmu_new_blog', 'new_blog', 10, 6);

if ( ! class_exists( 'RW_Meta_Box' ) )
    require_once PPY_DIR_PATH . 'includes/meta-box/meta-box.php';

require_once PPY_DIR_PATH . 'autoloader.php';

/**
 * The core plugin class that is used to define common methods for courses and lessons.
 */
require_once PPY_DIR_PATH . 'includes/class-poppyz-core.php';

/**
 * The plugin class that handles all payments.
 */
require_once PPY_DIR_PATH . 'includes/class-poppyz-payment.php';

/**
 * The plugin class that handles all invoices.
 */
require_once PPY_DIR_PATH . 'includes/class-poppyz-invoice.php';

/**
 * The plugin class that handles all coupons.
 */
require_once PPY_DIR_PATH . 'includes/class-poppyz-coupon.php';

/**
 * The plugin class that handles all statistics.
 */
require_once PPY_DIR_PATH . 'admin/class-poppyz-statistics.php';


require_once PPY_DIR_PATH . 'includes/class-poppyz-template.php';


/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require_once PPY_DIR_PATH . 'includes/class-poppyz.php';


if ( ! class_exists( 'Poppyz_API' ) )
	require_once PPY_DIR_PATH . 'public/class-poppyz-api.php';

/**
 * Used for plugin licensing and automatic updating
 */
if ( ! class_exists( 'EDD_SL_Plugin_Updater' ) )
	require_once PPY_DIR_PATH . 'includes/EDD/EDD_SL_Plugin_Updater.php';

function ppy_plugin_update() {
    // retrieve our license key from the DB
    $license_key = trim( Poppyz_Core::get_option( 'license_key' ) );

    // setup the updater
    $edd_updater = new EDD_SL_Plugin_Updater( PPY_WEBSITE, PPY_DIR_PATH . 'poppyz.php',
        array(
            'version' => PPY_VERSION,                    // current version number
            'license' => $license_key,             // license key (used get_option above to retrieve from DB)
            'item_id' => PPY_ITEM_ID,       // ID of the product
            'author'  => 'Poppyz', // author of this plugin
            'beta'    => false,
        )
    );
}

add_action( 'admin_init', 'ppy_plugin_update', 0 );


add_action('wp_trash_post', 'restrict_tier_deletion');
function restrict_tier_deletion($post_id) {
	$numberOfSubscribedUsers = Poppyz_Subscription::get_subscription_total_by_tier( $post_id );
	if( get_post_type($post_id) === PPY_TIER_PT && $numberOfSubscribedUsers != 0) {
		$message = sprintf("Sorry, you can't remove this tier because it has %s %s. Please unsubscribe these %s first.", $numberOfSubscribedUsers, _n('subscriber', 'subscribers', $numberOfSubscribedUsers), _n('subscriber', 'subscribers', $numberOfSubscribedUsers));
		wp_die($message);
	}
}


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.5.0
 */
function run_poppyz() {

	$plugin = new Poppyz();
	$plugin->run();

}
run_poppyz();
