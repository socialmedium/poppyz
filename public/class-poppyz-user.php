<?php
/**
 * This class handles all user functions
 *
 * @link       http://socialmedium.nl
 * @since      0.5.0
 *
 * @package    poppyz
 * @subpackage poppyz/public
 */
class Poppyz_User {

    private $username;
    private $email;
    private $password;
    private $password_2;
    private $website;
    private $first_name;
    private $last_name;
    private $honeypot;
    public $page;
    private $quick;

    public $custom_fields;

	private $donation;

    public function __construct( ){

        if ( is_page( Poppyz_Core::get_option( 'purchase_page') ) ) {
            $this->page = 'buy';
        } else if ( is_page( Poppyz_Core::get_option( 'edit-profile_page') ) ) {
            $this->page = 'edit';
        }
        $this->custom_fields = $this->custom_fields();
    }

    public static function custom_fields() {
        global $ppy_lang;
	    $fields = array(
	    	 __( 'Company' ,'poppyz') => 'company',
	         __( 'Address'  ,'poppyz') => 'address',
	         __( 'Zipcode'  ,'poppyz') => 'zipcode',
	         __( 'City' ,'poppyz') => 'city',
	         __( 'Country' ,'poppyz') => 'country',
	         __( 'Phone'  ,'poppyz') => 'phone'
        );

	    $fields = apply_filters( 'ppy_custom_fields', $fields );

        return $fields;
    }

    public static function check_missing_fields() {
        $user = wp_get_current_user();
        $user_id = $user->ID;

        $custom_fields = Poppyz_User::custom_fields();

        if ( isset( $_POST['save_custom_fields'] ) ) {
            foreach ( $_POST as $field => $value  ) {
                if ( in_array( $field, $custom_fields ) )
                    update_user_meta( $user_id, PPY_PREFIX . $field, $_POST[$field] );
            }
        }
        $missing = false;
        //check if fields required for the invoice are not filled
	    $exclude = array( 'company', 'phone' );
	    $exclude = apply_filters( 'ppy_exclude_custom_fields_validation', $exclude);
        foreach ( $custom_fields as $name => $value) {
            $field = get_user_meta( $user_id, PPY_PREFIX . $value, true );
            if ( empty( $field ) && !in_array( $value, $exclude) ) {
                $missing[$name] = $value;
            }
        }
        return $missing;
    }

    public function registration_form( $quick = false ) {

        global $ppy_lang;
        $html = '';

        if ( isset ( $_POST['reg_submit'] ) ) {
            global $reg_errors;

            if ( $reg_errors ) {
                foreach ( $reg_errors->get_error_messages() as $error ) {
                    $html .=  '<div  class="btn btn-block btn-lg btn-danger ppy-error-message">';
                    $html .=  $error . '<br/>';
                    $html .=  '</div>';
                }
            } elseif ( $this->page != 'edit' ) {
                $html .=  '<div style="margin-bottom: 6px">';
                $html .=   sprintf( __( 'Registration complete. Click <a href="%s">here</a> to login <br />'  ,'poppyz'),  wp_login_url( ) );
                $html .=  '</div>';
                return $html;
            } else {
                $html .=  '<div style="margin-bottom: 6px">';
                $html .=   sprintf( __( 'Update Successful.' ,'poppyz') );
                $html .=  '</div>';
            }
        }

        if ( $this->page != 'buy' ) $html .= '<form method="post" action="' . esc_url($_SERVER['REQUEST_URI']) .'">';
            $html .= '<div class="login-form">';

                $required = '';


                $fields = array(
                    'reg_user_email',
                    'reg_first_name',
                    'reg_last_name'
                );

                $is_loggedin = false;
                if ( is_user_logged_in() ) {
                    $is_loggedin = true;
                    $current_user = wp_get_current_user();
                    //$html .= '<fieldset><a class="et_pb_button " href="' . wp_logout_url() . '">' . __('Logout') . '</a></fieldset>'; //add logout link
                }

                //get default values


                foreach ( $fields as $field ) {
                    $key = str_replace( 'reg_' , '',  $field);
                    if (  $is_loggedin ) {
                        $$key =  ( isset( $_POST[$field] ) ) ? $_POST[$field] :  $current_user->$key ;
                        ${"text_" . $key} = Poppyz_Core::get_option( $field );
                    } else {
                        $$key =  ( isset( $_POST[$field] ) ) ? $_POST[$field] : '';
                        ${"text_" . $key} = Poppyz_Core::get_option( $field );
                    }
                }
                //this is a honeypot field used to filter out bots.
                $html .= ' <p style="display:none; position: absolute; left: -9999px;"> <input name="your_email" type="email" value=""
                        placeholder="'.  __('Email')  . '*" id="reg-email3" /> </p>';

                $html .= '<input type="hidden" name="recaptcha_response" id="recaptchaResponse" />';

                $email_text = $text_user_email ? $text_user_email : __('Email', 'poppyz');
                $first_name_text = $text_first_name ? $text_first_name : __('First Name', 'poppyz');
                $last_name_text = $text_last_name ? $text_last_name : __('Last Name', 'poppyz');


                $html .= ' <div class="registration-input-wrapper"><fieldset><input name="reg_user_email" type="email" value="' . $user_email . '"
                        id="reg-email" placeholder="'. $email_text . '*" class="registration-input" required /><div class="registration-placeholder-div">'. $email_text . '*</div> </fieldset></div>';

                if ( $this->page == 'edit' )  {
                    $html .= ' <div class="registration-input-wrapper"><fieldset><input name="reg_password" type="password" value=""
                        id="reg-pass" ' . $required . ' placeholder="'. __('Password', 'ppy') . '*" class="registration-input" /><div class="registration-placeholder-div">'. __('Password', 'ppy') . '*</div> </fieldset></div>';

                    $html .= ' <div class="registration-input-wrapper"><fieldset><input name="reg_password_2" type="password" value=""
                        id="reg-pass-2" ' . $required . ' placeholder="'. __('Confirm Password', 'ppy') . '*" class="registration-input" /><div class="registration-placeholder-div">'. __('Confirm Password', 'ppy') . '*</div> </fieldset></div>';
                }

                $html .= ' <div class="registration-input-wrapper"><fieldset><input name="reg_first_name" type="text" value="' . $first_name . '"
                        id="reg-first_name" placeholder="'. $first_name_text . '*" class="registration-input" required /><div class="registration-placeholder-div">'. $first_name_text . '*</div> </fieldset></div>';

                $html .= ' <div class="registration-input-wrapper"><fieldset><input name="reg_last_name" type="text" value="' . $last_name . '"
                        id="reg-last_name" placeholder="'. $last_name_text . '*" class="registration-input"  required /><div class="registration-placeholder-div">'. $last_name_text . '*</div> </fieldset></div>';


                //if on the purchase page make the custom fields required for the invoice
                $required =  ( $this->page == 'buy' ) ? 'required' : '';

                if ( !$quick ) {
                    foreach ( $this->custom_fields as $name => $value ) {

                        if (  $is_loggedin ) {
                            $$field =  ( isset( $_POST['reg_' . $value] ) ) ? $_POST['reg_' . $value] :  get_user_meta( get_current_user_id(), PPY_PREFIX . $value, true );
                        } else {
                            $$field =  ( isset( $_POST['reg_' . $value] ) ) ? $_POST['reg_' . $value] : '';
                        }

	                    $exclude = apply_filters( 'ppy_custom_fields_exclude', array() );

                        $required = apply_filters( 'ppy_custom_fields_required', $required, $value );
                        
	                    if ( !in_array( $value, $exclude) ) {
	                        //$required = ( Poppyz_Core::get_option( 'reg_required_' . $value ) == 'on' ) ? ' required ' : '';
	                        if ( Poppyz_Core::get_option( 'reg_disabled_' . $value ) == 'on' ) continue;
	                        if ($value == 'address' || $value == 'zipcode' || $value == 'city' || $value == 'country') {
	                            $required = 'required';
                            }
	                        if ( $value == 'company' ) {
	                            $required = '';
                            }
                            $asterisk = $required ? '*' : '';
                            if ( $value == 'country' ) {
                                $html .= '<div class="registration-input-wrapper"><fieldset>' . Poppyz_Core::country_select( 'reg_country', $$field, $required ) . '</fieldset></div>';
                                continue;
                            }
	                        $html .= ' <div class="registration-input-wrapper"><fieldset><input name="reg_' . $value .'" type="text" '. $required .'
	                                   value="' . $$field . '"
	                                   id="reg-' . $value .'" placeholder="' . $name . $asterisk . '"  class="registration-input" /><div class="registration-placeholder-div">' . $name . $asterisk . '</div>
	                               </fieldset></div>';
	                    }
	                    $html = apply_filters( 'ppy_custom_fields_html', $html, $$field, $name, $value );
                    }
                }

                if ( $this->page == 'buy' ) $html .= '<input type="hidden" value="1" name="buy" />';
                if ( $quick ) {
                    //require phone

                    if ( Poppyz_Core::get_option( 'reg_disabled_phone'  ) != 'on' ) {
                        $html .= ' <div class="registration-input-wrapper"><fieldset><label for="reg-phone"></label><input name="reg_phone" type="text"  required="required"
	                                   value=""
	                                   id="reg-phone"
	                                   placeholder="' . __('Phone' )  . '*" 
	                                   class="registration-input"
	                                   />
	                                   <div class="registration-placeholder-div">' . __('Phone' )  . '*</div>
	                               </fieldset></div>';
                    }

                    $html .= '<input type="hidden" value="1" name="quick" />';
                }
                if ( $this->page == 'edit' ) {
            $submit_text = __('Update', 'poppyz');
                } elseif ( $this->page == 'buy' ) {
                    $submit_text =  ''; //hide this button on the purchase page because we will be placing it on the tier description section
                } else {
                    $submit_text = __( 'Register','poppyz');
                }

                if ( $submit_text ) $html .= '<fieldset><label></label><input class="et_pb_button" type="submit" name="reg_submit" value="' .  $submit_text . '"/></fieldset> ';

                if ( $this->page != 'buy' ) $html .= '</form>';
            $html .= '</div>';
        return $html;
    }


    public function registration_validation() {

        $reg_errors = new WP_Error();
        $has_error = false;

        global $ppy_lang;

        $email = '';
        if (is_user_logged_in()) {
            $current_user = wp_get_current_user();
            $email = $current_user->user_email;
        }
        if ( !empty($this->honeypot ) ) {
            $reg_errors->add( 'bot_detected', __('Cannot create a user, please contact the website admin.') ) ;
            $has_error = true;
        }

        if ( Poppyz_Core::get_option( 'recaptcha_v3_secret_key') && isset( $_POST['recaptcha_response'] ) ) {
            $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
            $recaptcha_secret = Poppyz_Core::get_option( 'recaptcha_v3_secret_key');
            $recaptcha_response = $_POST['recaptcha_response'];

            // Make and decode POST request:
            $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
            $recaptcha = json_decode($recaptcha);


            if ($recaptcha->success && $recaptcha->score < 0.5) {
                $reg_errors->add( 'username_field_missing', __( 'Username is required.','poppyz') );
                $has_error = true;
            }
        }

        if ( $this->page != 'edit' ) {
            $user_name = trim( $this->username );

            if ( email_exists( $this->email ) ) {
                $this->add_user_to_subsite( $this->email ) ; //if multisite, add the user to this website and output the same error since they will still need to login.

                $current_url = get_permalink();
                if ( get_query_var( 'order-id' ) ) {
                    if (  get_option( 'permalink_structure' ) ) {
                        $slug = 'order-id/';
                    } else {
                        $slug = '&order-id=';
                    }
                    $current_url =  get_permalink() . $slug . get_query_var( 'order-id' ) . '?buy' ;
                }

                $reg_errors->add( 'email', sprintf( __( 'Email Already in use. Please <a href="%s">login here</a> to use it.'  ,'poppyz'),  wp_login_url( $current_url ) ) );
                $has_error = true;
            }

            if ( empty( $user_name ) ) {
                $reg_errors->add( 'username_field_missing', __( 'Username is required.' ,'poppyz') );
                $has_error = true;
            }
            if (strlen($this->username) < 4) {
                $reg_errors->add( 'username_length', __( 'Username too short. At least 4 characters is required' ,'poppyz') ) ;
                $has_error = true;
            }

            if ( username_exists( $this->username ) ) {
                $reg_errors->add( 'username_exists', __( 'Username is already taken','poppyz')) ;
                $has_error = true;
            }
            if ( !validate_username( $this->username  ) ) {
                $reg_errors->add( 'name_invalid', __( 'Sorry, the username you entered is not valid' ,'poppyz') );
                $has_error = true;
            }

        } else {
            if ( !empty( $this->password ) ) {

                if ( strlen( $this->password ) < 5) {
                    $reg_errors->add( 'password', __('Password length must be greater than 5','poppyz') ) ;
                    $has_error = true;
                }
                if ( empty( $this->password_2 ) ) {
                    $reg_errors->add( 'password_2', __('Please confirm password' ,'poppyz') ) ;
                    $has_error = true;
                }
                if ($this->password != $this->password_2 ) {
                    $reg_errors->add( 'password_mismatch', __('Passwords did not match','poppyz')) ;
                    $has_error = true;
                }
            }

            if ( $this->email != $email && email_exists( $this->email )) {
                $user_id = email_exists( $this->email ) ;
                if ( $user_id != $current_user->ID ) {
                    $reg_errors->add( 'email', __('Email already in use.' ,'poppyz') );
                    $has_error = true;
                }
            }

        }


        $email = trim( $this->email );
        if ( empty( $email ) ) {
            $reg_errors->add('email_field_missing', __( 'Email is required.' ,'poppyz') );
            $has_error = true;
        }
        $first_name = trim( $this->first_name ) ;
        if (  empty( $first_name ) ) {
            $reg_errors->add('first_name_field_missing', __( 'First name is required.' ,'poppyz') );
            $has_error = true;
        }
        $last_name = trim( $this->last_name );
        if (  empty( $last_name ) ) {
            $reg_errors->add('last_name_field_missing', __( 'Last name is required.','poppyz' ) );
            $has_error = true;
        }

        if (!is_email($this->email)) {
            $reg_errors->add('email_invalid', __('Email is not valid','poppyz' ) );
            $has_error = true;
        }


        //if on the purchase page make the custom fields required for the invoice
        if ( $this->page == 'buy' && !$this->quick ) {
	        $exclude = array( 'company' );
        	$exclude = apply_filters( 'ppy_exclude_custom_fields_validation', $exclude);
            foreach ( $this->custom_fields as $field => $value ) {
                if ( Poppyz_Core::get_option( 'reg_disabled_' . $value ) == 'on' ) continue; //if field is disabled, don't validate
                $param = ( isset( $_POST['reg_' . $value] ) ) ? trim( $_POST['reg_' . $value] ) : null;

                if ( empty( $param ) && !in_array( $value, $exclude ) ) {
                    $reg_errors->add($value . 'field_missing', sprintf( __( '%s is required.' ,'poppyz'), $field ) );
                    $has_error = true;
                }
            }
        }

        if ( $has_error ) {
            return $reg_errors;
        }
        return true;
    }

    public function save() {

        $validate = $this->registration_validation();
        if ( !is_wp_error( $validate ) ) {

            //register the user
            $user_data = array(
                'user_login' => esc_attr( $this->email ),
                'user_email' => esc_attr( $this->email ),
                'user_pass' =>  $this->password,
                'user_url' => esc_attr( $this->website ),
                'first_name' => esc_attr( $this->first_name ),
                'last_name' => esc_attr( $this->last_name ),
            );

            if ( $this->page == 'edit' ) {
                unset( $user_data['user_login'] );
                //if the password is empty don't save it.
                if ( empty( $user_data['user_pass'] ) ) unset( $user_data['user_pass'] );
                $user_data['ID'] = get_current_user_id();
                $user  = wp_update_user( $user_data );

                //when a user changes password wordpress automatically log the user out, with this we can sign the user back in.
                if ( empty( $user_data['user_pass'] ) ) {
                    wp_set_current_user( $user );
                    wp_set_auth_cookie( $user, false, is_ssl() );
                    wp_redirect( wp_get_referer() );
                }

            } else {
                $user = wp_insert_user( $user_data );
                $new_user_notification_disabled = Poppyz_Core::get_option( 'new_user_notification_disabled' );
                if ( $new_user_notification_disabled == 'on' ) {
                    wp_new_user_notification( $user, null, 'admin' );
                } else {
                    wp_new_user_notification( $user, null, 'both' );
                }
            }
            if ( !is_wp_error( $user ) ) {

                foreach ( $this->custom_fields as $cf ) {
                    if ( isset( $_POST['reg_' . $cf] ) ) {
                        $value = $_POST['reg_' . $cf];
	                    $value = apply_filters( 'ppy_custom_fields_value', $value, $cf );
                        update_user_meta( $user, PPY_PREFIX . $cf, $value );
                        do_action( 'ppy_custom_fields_save', $value, $cf, $user );
                    } else {
                        delete_user_meta( $user, PPY_PREFIX . $cf );
                    }
                }
				//check if the donation field is set and save it to user meta.
				if ( isset($_POST['additional_products']) && !empty($_POST['additional_products']) ) {
					//error_log(print_r($_POST['additional_products'], true));
					Poppyz_Core::set_additional_products_cookie($_POST['additional_products']);
				}
				if ( isset($_POST['ppy_donation_amount']) ) {
					Poppyz_Core::set_donation_amount_cookie($_POST['ppy_donation_amount']);
				}
				if ( isset($_POST['ppy_donation_amount_custom']) ) {
					Poppyz_Core::set_donation_amount_custom_cookie($_POST['ppy_donation_amount_custom']);
				}
				if ( isset($_POST[PPY_PREFIX .'donation_amount']) && !empty($_POST[PPY_PREFIX .'donation_amount']) ) {
					if ( $_POST[PPY_PREFIX .'donation_amount'] == "others" ) {
						$donation = $_POST[PPY_PREFIX .'donation_amount_custom'];
					} else {
						$donation = $_POST[PPY_PREFIX .'donation_amount'];
					}
					update_user_meta( $user, PPY_PREFIX . 'tier_donation_amount', $donation );
				}


                if ( $this->page != 'edit' ) {
                    $auto_login = Poppyz_Core::get_option( 'disable_auto_login' ) ;
                    if ( !$auto_login ) {
                        wp_set_current_user( $user );
                        wp_set_auth_cookie( $user, false, is_ssl() );
                        wp_redirect( '?buy' );
                    } else {
                        $url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
                        $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
                        wp_redirect( wp_login_url( $escaped_url. '?buy' ) );
                    }
                    exit;
                }

                if ( $this->page == 'edit' && isset( $_GET['redirect_to'] ) ) {
                    wp_redirect( $_GET['redirect_to']  );
                }
            }

        } else {
            global $reg_errors;
            $reg_errors = $validate;
        }
    }


    public function initialize_registration() {
		if ( empty($_POST['user_type']) ) {
			if ( $this->page != 'edit' ) {
				$this->username = $_POST['reg_user_email'];
			}
			$this->email = $_POST['reg_user_email'];
			if ( isset($_POST['reg_password'] ) && isset($_POST['reg_password_2'])) {
				$this->password = $_POST['reg_password'];
				$this->password_2 = $_POST['reg_password_2'];
			}
			$this->first_name = $_POST['reg_first_name'];
			$this->last_name = $_POST['reg_last_name'];
			$this->honeypot = $_POST['your_email'];
			$this->quick = isset( $_POST['quick'] );
			$this->donation = (isset($_POST[PPY_PREFIX .'donation_amount'])) ? $_POST[PPY_PREFIX .'donation_amount'] : "";
		}

        if ( !$this->quick ) {
            //loop through the custom fields
            /*foreach ( $this->custom_fields as $cf ) {
                $this->custom_fields[$cf] = $_POST['reg_' . $cf];
            }*/
        }

        /*$this->registration_validation( );
        $this->registration();*/
    }

    public function display_registration() {

        ob_start();

        if ( isset( $_POST['reg_submit'] ) ) {
            $this->username = $_POST['reg_user_email'];
            $this->email = $_POST['reg_user_email'];
            if ( isset($_POST['reg_password'] ) && isset($_POST['reg_password_2'])) {
                $this->password = $_POST['reg_password'];
                $this->password_2 = $_POST['reg_password_2'];
            }
            $this->first_name = $_POST['reg_first_name'];
            $this->last_name = $_POST['reg_last_name'];
            $this->honeypot = $_POST['your_email'];
            $this->quick = isset( $_POST['quick'] );


             //loop through the custom fields
            /*foreach ( $this->custom_fields as $cf ) {
                $this->custom_fields[$cf] = $_POST['reg_' . $cf];
            }*/

            $this->registration_validation( );
            $this->registration();
        } else {
            $this->registration_form();
        }


        return ob_get_clean();
    }

    public function add_user_to_subsite( $user_email ) {
        if (is_multisite()) {
            $blog_id = get_current_blog_id();
            $user_id = username_exists( $user_email );
            if( !$user_id ) {
                $user_id = email_exists( $user_email );
            }
            if ( $user_id && ! is_user_member_of_blog( $user_id, $blog_id ) ) {
                add_user_to_blog( $blog_id, $user_id, 'subscriber' );
            }
        }
    }

}
