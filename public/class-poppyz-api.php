<?php
/* Need to rework this to use the WP_REST_Controller class */

class Poppyz_API{

    /** Hook WordPress
     *	@return void
     */
    public function __construct(){
        add_filter('query_vars', array($this, 'add_query_vars'), 0);
        add_action('parse_request', array($this, 'sniff_request'), 0);
        add_action('init', array($this, 'add_endpoint'), 0);
    }

    /** Add public query vars
     *	@param array $vars List of current public query vars
     *	@return array $vars
     */
    public function add_query_vars($vars){
        $vars[] = '__api';
        $vars[] = 'tier_code';
        $vars[] = 'ppy_api';
        return $vars;
    }

    /** Add API Endpoint
     *	This is where the magic happens - brush up on your regex skillz
     *	@return void
     */
    public function add_endpoint(){
        add_rewrite_rule('^smc_api/subscriptions/?([0-9]+)?/?','index.php?__api=1&tier_code=$matches[1]', 'top');
        add_rewrite_rule('^ppy_api/?[^A-Za-z0-9]+?/?','index.php?ppy_api=$matches[1]', 'top');
    }
    /**	Sniff Requests
     *	This is where we hijack all API requests
     * 	If $_GET['__api'] is set, we kill WP and serve up pug bomb awesomeness
     *	@return die if API request
     */
    public function sniff_request(){
        global $wp;
        if ( isset( $wp->query_vars['__api'] ) ) { //this is from our old API for autorespond, can't remove this since it's still being used
            $this->authenticate();
            die;
        }
        //new API
        if ( isset($_GET['ppy_api'] ) ) {
            //error_log( 'PPY_API:: ' .  print_r($_REQUEST, true) );
            $this->authenticate();
            die();
        }

    }

    public function authenticate() {
        if(  isset( $_REQUEST['key'] ) && $_REQUEST['key'] == Poppyz_Core::get_option( 'secret_key' ) ){
            $this->handle_request_old();
        } elseif($_REQUEST['ppy_api']) {
            $this->handle_request($_REQUEST['ppy_api']);
        }
        else{
            $this->send_response('Authentication failed.', true);
        }
    }

    protected function handle_request($api) {
        global $wp, $wpdb;
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') return;
        if ( $api  == 'plugandpay') {
            $tier_id = $_POST['sku'];
            $email = $_POST['email'];
            $amount = $_POST['price'];
            $type = $_POST['billing_cycle'];
            $payment_id = !empty($_POST['mollie_id']) ? $_POST['mollie_id'] : 'tr_free';

            $query = new WP_Query( array( 'post_type' => PPY_TIER_PT, 'p' => $tier_id ) );
            if ( !$query->have_posts() )
                $this->send_response('The given tier code was not found.', true);

            $user_id = email_exists( $email );

            if ( !$user_id ) {
                $user_id = register_new_user( $email, $email );
            }
            if ( !$user_id || is_wp_error( $user_id )) {
                $this->send_response('User was not created!.', true);
            }

            if ( isset( $_POST['firstname'] ) ) {
                update_user_meta( $user_id, 'first_name', trim( $_POST['firstname'] ) );
            }
            if ( isset( $_POST['lastname'] ) ) {
                update_user_meta( $user_id, 'last_name', trim( $_POST['lastname'] ) );
            }
            if ( Poppyz_Subscription::get_subscription_by_payment_id( $payment_id ) ) {
                if ( get_post_meta($tier_id, 'partner_api_subs_id_' . $_POST['contract_id'], true )) {
                    error_log('Payment already exists. Probably reposted. Payment ID: ' . $payment_id);
                    return;
                }
            }

            $data = [
                'user_id' => $user_id,
                'tier_id' => $tier_id,
                'amount' => $amount,
                'payment_id' => $payment_id,
                'first' => isset($_POST['first']) &&  $_POST['first']== 1,
                'type' => $type,
                'coupon_code' => $_POST['discount_code'] ?? '',
                'payment_number' => 1,
                'order_id' => $_POST['order_id'],
                'contract_id' => $_POST['contract_id'],
            ];

            if ($type == 'single') {
                $data['payment_method'] = 'standard';
                $this->add_subscription( $data );
            } else {
                $data['payment_method'] = 'plan';

                //API token was entered in plugin settings, proceed to use the API.
                if (Poppyz_Core::get_option('plugandpay_token')) {
                    require_once PPY_DIR_PATH . "includes/Plugandpay/Plugandpay.php";
                    $plugandpay = new Plugandpay();
                    $order = $plugandpay->getOrder($_POST['order_id']);
                    if ( isset($order->data->items[0]->subscription->type)) {
                        if (  $order->data->items[0]->subscription->type == 'recurring'  ) {
                            $data['payment_method'] = 'membership';
                        } else {
                            $data['payment_method'] = 'plan';
                            $data['payments_total'] = isset($_POST['nr_of_cycles']) ? $_POST['nr_of_cycles'] : 0;
                        }
                    }
                }

                $this->add_subscription_plan( $data );
            }

            $this->send_response('Success');
        }
    }

    /** Handle Requests
     *	This is where we send off for an intense pug bomb package
     *	@return void
     */
    protected function handle_request_old( $method='subscription' ){
        global $wp, $wpdb;

        if ( $method  == 'subscription') {
            if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {

                $tier_id = (int) $wp->query_vars['tier_code'];
                $email = $_POST['email'];
                $amount = $_POST['amount'];

                if( !$tier_id )
                    $this->send_response('Tier code is missing.', true);
                if( !$email )
                    $this->send_response('Email is missing.', true);
                if( $amount === null )
                    $this->send_response('Amount is missing.', true);

                $query = new WP_Query( array( 'post_type' => PPY_TIER_PT, 'p' => $tier_id ) );

                if ( !$query->have_posts() )
                    $this->send_response('The given tier code was not found.', true);

                $user_id = email_exists( $email );

                if ( !$user_id ) {
                    $user_id = register_new_user( $email, $email );
                }

                $this->add_subscription_old( $user_id, $tier_id, $amount );

                $this->send_response('Success');
            }
        }
    }

    /** Response Handler
     *	This sends a JSON response to the browser
     */
    protected function send_response( $message, $error = false ){
        $response['message'] = $message;
        $response['error'] = $error;
        header('content-type: application/json; charset=utf-8');
        echo json_encode($response)."\n";
        error_log(json_encode($response)."\n");
        die;
    }

    public function add_subscription( $data ) {
        global $wpdb;
        $fields =  array(
            'course_id' => Poppyz_Core::get_course_by_tier( $data['tier_id'] ),
            'user_id' => $data['user_id'],
            'tier_id' => $data['tier_id'],
            'status' => 'on',
            'payment_status' => 'paid',
            'payment_method' => $data['payment_method'],
            'amount' => $data['amount'],
            'payment_id' => $data['payment_id'],
            'payments_made' => 1,
            'subscription_date' => date('Y-m-d H:i:s')
        );

        $values = array(
            '%d',
            '%d',
            '%d',
            '%s',
            '%s',
            '%s',
            '%d',
            '%s',
            '%d',
            '%s',
        );

        $wpdb->insert(
            $wpdb->prefix . 'course_subscriptions',
            $fields,
            $values
        );
        $subs_id = $wpdb->insert_id;

        $ppy_payment = new Poppyz_Payment();

        $invoice = new Poppyz_Invoice();

        $invoice_id = $invoice->save_invoice( $subs_id, $data );
        $ppy_payment->save_payment($subs_id, $data['payment_id'], $invoice_id, 1, 'paid', 'plugandpay', $data['order_id'] );
        update_post_meta($data['tier_id'], 'partner_api_subs_id_' . $data['contract_id'], $subs_id);
        Poppyz_Core::subscribe_newsletter( $data['user_id'], $data['tier_id'] );

        Poppyz_Subscription::send_thank_you_message( $subs_id, $data['user_id'] ); //only send on first payment
        Poppyz_Core::send_notification_email( $subs_id, $data['user_id'], array('admin' => true), 'new_purchase');

        return $subs_id;
    }
    public function add_subscription_plan( $data ) {
        global $wpdb;
        $ppy_sub = new Poppyz_Subscription();
        if ($data['first'] == 1) {
            $subs_id = $this->add_subscription($data);
            $payments_made = 0;
        } else {
            $subs_id = get_post_meta($data['tier_id'], 'partner_api_subs_id_' . $data['contract_id'], true);
            $subs = $ppy_sub->get_subscription_by_id( $subs_id );
            $payments_made = $subs->payments_made;

            //increment payments made
            $fields = array(
                'payments_made' => $payments_made + 1,
                'payment_id' => $data['payment_id'], //update the payment id again for memberships/payment plans
                'payment_status' => 'paid',
                'status' => 'on',
            );

            $fields_format = array(
                '%d',
                '%s',
                '%s',
                '%s',
            );

            global $wpdb;
            $wpdb->update(
                $wpdb->prefix . 'course_subscriptions',
                $fields,
                array( 'id' => $subs_id ),
                $fields_format,
                array( '%s' )
            );

            if ($data['payments_total'] > 0) {
                if (get_user_meta($subs->user_id, 'poppyz_total_payments_' . $subs_id, true) >= $data['payments_total']) {
                    update_user_meta($subs->user_id, 'poppyz_installments_done_' . $subs_id, '1');
                } else {
                    update_user_meta($subs->user_id, 'poppyz_total_payments_' . $subs_id, $payments_made + 1);
                }
            }

            $ppy_payment = new Poppyz_Payment();
            $invoice = new Poppyz_Invoice();
            $invoice_id = $invoice->save_invoice( $subs_id, $data );
            $ppy_payment->save_payment( $subs_id, $data['payment_id'], $invoice_id, $payments_made + 1, 'paid', 'plugandpay', $data['order_id'] );
            Poppyz_Core::subscribe_newsletter( $data['user_id'], $data['tier_id'] );

        }



    }

    public function add_subscription_old($user_id, $tier_id, $amount = '', $type ='standard' ) {
        global $wpdb;
        $ppy_payment = new Poppyz_Payment();

        $fields =  array(
            'course_id' => Poppyz_Core::get_course_by_tier( $tier_id ),
            'user_id' => $user_id,
            'tier_id' => $tier_id,
            'status' => 'on',
            'payment_status' => 'paid',
            'payment_method' => $type,
            'amount' => $amount,
            'subscription_date' => date('Y-m-d H:i:s')
        );

        $values = array(
            '%d',
            '%d',
            '%d',
            '%s',
            '%s',
            '%s',
            '%d',
            '%s',
        );

        $wpdb->insert(
            $wpdb->prefix . 'course_subscriptions',
            $fields,
            $values
        );

        Poppyz_Core::subscribe_newsletter( $user_id, $tier_id );
    }
}
new Poppyz_API();