(function( $ ) {
	'use strict';

	$(function() {


		$(document).on("click", "#apply-coupon", function(){
			var product_type = $(this).attr("class");
			var coupon_code = $('#coupon').val();
			var subs_id = $('#' + ppy_object.prefix +'subs_id').val();
			var base_price = $('#apply-coupon').attr("data-price");
			var tier_id = $('#tier_id').val();
			var total_payment = $("#total_payment").val();
			var vat_before_discount = $("#main_tier_vat").val();
			var main_tier_vat = $("#main_tier_vat").val();
			var main_tier_vat_raw = $("#main_tier_vat_raw").val();
			var price_tax = $("#price_tax").val();

			var total_vat = $("#total-vat").val();

			if ( coupon_code == "" ) {
				alert(ppy_object.messages.coupon_required);
				return false;
			}
			var data = {
				action: 'apply_coupon',
				security: ppy_object.ppy_ajax_nonce,
				coupon_code: coupon_code,
				subs_id: subs_id,
				base_price: base_price,
				tier_id: tier_id
			};
			$.ajax ({
				type: 'POST',
				dataType: "json",
				url: ppy_object.ajax_url,
				data: data,
				success: function(response) {
					if (response.success == true) {
						if (product_type == "ppyv") {
							if (price_tax != "inc") {
								var coupon_discount = parseFloat(response.coupon_discount) + parseFloat(vat_before_discount); //add the discount value and the old tax
							} else {
								var coupon_discount = parseFloat(response.coupon_discount);
							}
							var discounted_price = response.main_product_price_include_tax - coupon_discount; //remove the discount and the old tax`

							if (price_tax != "inc") {
								discounted_price = discounted_price + parseFloat(response.raw_tax_amount); // add the new calculated tax
							}

							$("#coupon_discount").val(response.coupon_discount);
							$('#coupon-container').hide();
							$('#coupon-wrapper').show();
							$('.purchase-tier-coupon-value').empty().append(response.content);
							$('#new-price-wrapper').show();
							$('.purchase-tier-new-price-value').empty().append(response.final_price);
							$('#main-product-vat').val(response.tax_amount);
							$('#main_tier_price_with_tax').val(discounted_price);

							//get the total with tax
							var total_with_tax_raw = $("#total-with-tax-raw").val();
							var main_tier_price = $("#main_tier_price").val();
							var new_main_tier_vat = $("#main_tier_price_with_tax").val();

							if (price_tax != "inc") {
								var exclude_main_tier = parseFloat(total_with_tax_raw) - (parseFloat(main_tier_price) + parseFloat(main_tier_vat));
							} else {
								var exclude_main_tier = parseFloat(total_with_tax_raw) - (parseFloat(main_tier_price));
							}
							var final_total_wit_tax = parseFloat(exclude_main_tier) + parseFloat(new_main_tier_vat);

							$('#total-with-tax').html(convertToEuro(parseFloat(final_total_wit_tax).toFixed(2)));
							checkTotalAmountForEproduct(parseFloat(final_total_wit_tax).toFixed(2));


						} else {
							$('#coupon-container').hide();
							$('#coupon-info, #final-price').show();
							$('#coupon-info > td').empty().append(response.content);
							$('#final-price > td').empty().append(response.final_price);
							$('#tax > td').empty().append(response.tax_amount);
							var coupon_discount = parseFloat(response.coupon_discount) + parseFloat(vat_before_discount); //add the discount value and the old tax
							var discounted_price = response.main_product_price_include_tax - coupon_discount; //remove the discount and the old tax`
							discounted_price = discounted_price + parseFloat(response.raw_tax_amount); // add the new calculated tax

							//compute the vat
							var new_vat = parseFloat(main_tier_vat_raw) - parseFloat(response.tax_amount_numeric_value);
							new_vat = parseFloat(total_vat) - new_vat;
							$('#total-vat-output').html(convertToEuro(parseFloat(new_vat).toFixed(2)));

							//remove the tier price with tax and replace with the discounted price
							var new_total_payment = total_payment - response.main_product_price_include_tax;
							new_total_payment = new_total_payment + response.final_price_with_tax;
							$('#total_payment_output').html(convertToEuro(parseFloat(new_total_payment).toFixed(2)));
						}

					} else {
						$('#coupon').focus();
						$('#coupon-container > td > p').remove();
						$('#coupon-container > td').append("<p class='error'><small>" + response.content + "</small></p>");
					}
				}
			});
		});

		var add_link = $('#coupon-container .coupon-add-link');
		var default_text = add_link.html();

		$('#coupon-container').on("click", ".coupon-add-link", function(){
			add_link = $(this);
			var hidden = $('#coupon-container .coupon-form');
			hidden.slideToggle('slow', function() {
				if (hidden.is(':hidden')) {
					add_link.html(default_text);
				}
				else {
					add_link.html(ppy_object.messages.no_coupon);
				}
			});
			return false;
		});

		$(document).on("click", "#remove-coupon", function(){

			var data = {
				action: 'remove_coupon',
				security: ppy_object.ppy_ajax_nonce
			};

			$.ajax ({
				type: 'POST',
				dataType: "json",
				url: ppy_object.ajax_url,
				data: data,
				success: function(response) {
					location.reload();
				}
			});
			return false;
		});

		var vatnumfield = $('#reg-vatnumber').parent('fieldset');
		var vatnumfieldlabel = $('label', vatnumfield);
		var vatnumfieldinput =  $('input', vatnumfield);
		var vatnumfieldlabeltext = vatnumfieldlabel.html();

		var companyinput = $('#reg-company');


		var vatshiftedfield = $('#reg-vatshifted').parent('fieldset');

		var belgium =  $('#ppy_belgium').val() == 1;

		if ( !belgium && $('#reg-vatshifted').length && !$('#reg-vatshifted').is(':checked')) {
            vatnumfield.hide();
		}


        jQuery('#reg-vatnumber').removeAttr('required');
		/*var country = $('select[name="reg_country"]').val();
		if (country === 'NL') {
            vatshiftedfield.hide();
            vatnumfield.hide();
            $('#reg-vatnumber').val('');
		}
        $('select[name="reg_country"]').change(function() {
            if ($(this).val() === 'NL') {
                vatshiftedfield.hide();
                vatnumfield.hide();
                $('#reg-vatnumber').val('');
            } else {
                vatshiftedfield.show();
			}
        });*/
		var back_button = $(".poppyz-back-button");
		back_button.attr('href', '#');
		back_button.click(function(e) {
			// prevent default behavior
			e.preventDefault();
			// Go back 1 page
			window.history.back();
		});
		$('#reg-vatshifted').change(function() {
			if ($(this).prop( "checked" ) == true) {
                vatnumfield.show();
				vatnumfieldlabel.html(vatnumfieldlabeltext+'*');
				vatnumfieldinput.attr('required', 'required');
				companyinput.attr('required', 'required');
			} else if (belgium) {
				vatnumfield.show();
			}

			if ( $(this).prop( "checked" ) != true ) {
				//vatnumfield.hide();
				$('#reg-vatnumber').val('');
				vatnumfieldlabel.html(vatnumfieldlabeltext);
				vatnumfieldinput.removeAttr('required');
				companyinput.removeAttr('required');
			}
        });
        $(".lesson-list.collapsible h3").next().attr('style', 'display: none !important;');

        $(".lesson-list.collapsible h3").click(function () {
            var header = jQuery(this);
            //getting the next element
            var content = header.next();
            //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
			if (content.is(':visible')) {
				content.slideUp();
				$(this).removeClass('opened');
			} else {
				content.slideDown();
				$(this).addClass('opened');
			}

        });
        $(".lesson-list.collapsible .module.current h3").click();

		/* Open lightbox on button click */
		$('.lightbox-toggle').click(function(){
			$('.backdrop').animate({'opacity':'.50'}, 300, 'linear').css('display', 'block');
			$('.poppyz-popup').fadeIn();
		});

		/* Click to close lightbox */
		$('.close, .backdrop').click(function(){
			$('.backdrop').animate({'opacity':'0'}, 300, 'linear', function(){
				$('.backdrop').css('display', 'none');
			});
			$('.poppyz-popup').fadeOut();
		});

		//move the Poppyz content into the main wrapper of Divi
		//$(".et_pb_row_1.inside-wrapper").appendTo('#et-boc');


		var terms_checkboxes = jQuery('#terms-content input[type=checkbox]');
		var pay_now_button = $('#pay-now');

		has_agreed_terms(terms_checkboxes);

		terms_checkboxes.click(function(){
			has_agreed_terms(terms_checkboxes);
		});

		function has_agreed_terms(checkbox) {
			if (checkbox.not(':checked').length > 0 ) {
				pay_now_button.addClass("inactive");

			} else {
				pay_now_button.removeClass("inactive");
			}
		}
		function convertToEuro(number) {
			var euro = number.replace(".", ",");
			euro = "&euro; "+euro.replace(",00", ",-");

			return euro;
		}

		function getDonationValue() {
			var donation = $("input[name='ppy_donation_amount']:checked").val();
			if (donation == "others") {
				var customPrice = parseInt($("#donation-custom-amount").val());
				if ( (isNaN(customPrice) || customPrice == 0 ) ) {
					var total = 0;
				} else if ( (!isNaN(customPrice) || customPrice != 0 ) ) {
					var total = customPrice;
				} else {
					var total = donation;
				}
			} else {
				donation = parseFloat(donation);
				var total = donation;
				if ( isNaN(total) ) {
					total = 0;
				}
			}

			return parseFloat(total).toFixed(2);
		}

		function getAdditionalTierTotal() {
			var subProductTotalPrice = 0;
			var subProductTotalPriceWithTax = 0;
			$("input:checkbox.child-additional-product:checked").each(function(){
				var checkboxId = $(this).attr('id');
				$("#additional-product-row-"+checkboxId).removeClass("inactive-additional-product");
				$("#additional-product-row-"+checkboxId).addClass("active-additional-product");
				subProductTotalPrice +=  parseFloat($(this).attr("data-price"),10);
			});
			return subProductTotalPrice;
		}

		function getAdditionalTierTotalPriceWithTax() {
			var subProductTotalPriceWithTax = 0;
			$("input:checkbox.child-additional-product:checked").each(function(){
				var checkboxId = $(this).attr('id');
				$("#price_with_tax_"+checkboxId).removeClass("inactive-additional-product");
				$("#additional-product-row-"+checkboxId).addClass("active-additional-product");
				var price_with_tax = $("#price_with_tax_"+checkboxId).val();
				subProductTotalPriceWithTax +=  parseFloat(price_with_tax);
			});
			return subProductTotalPriceWithTax;
		}

		$('.ppy_donation_amount').on('change', function() {
			var tierPrice = parseFloat($("#tier_price").val());
			var mainTierPriceWithTax = parseFloat($("#main_tier_price_with_tax").val());
			var donationValue = parseFloat(getDonationValue());
			var additionalTier = parseFloat(getAdditionalTierTotal());
			var additionalTierPriceWithTax = parseFloat(getAdditionalTierTotalPriceWithTax());

			var total = donationValue + additionalTier + tierPrice;
			$("#total").val(total);
			var totalWithTax = donationValue + additionalTierPriceWithTax + mainTierPriceWithTax;
			$("#purchase-total-value").html(convertToEuro(parseFloat(total).toFixed(2)));
			$("#total-with-tax").html(convertToEuro(parseFloat(totalWithTax).toFixed(2)));

			checkTotalAmountForEproduct(parseFloat(totalWithTax).toFixed(2));

			//eproduct coupon application
			applyCouponToEproducttotal();
		});
		$('#donation-custom-amount').click(function () {

			$("#donation-custom-amount-radio-button").prop("checked", true); ;
			var donation = parseInt($('#donation-custom-amount').val());
			//var donation = parseFloat(donation).toFixed(2);
			var tierPrice = parseInt($("#tier_price").val());
			var mainTierPriceWithTax = parseFloat($("#main_tier_price_with_tax").val());

			if ( (isNaN(donation) || donation == 0) &&  tierPrice > 0 ) {
					var total = tierPrice;
					var totalWithTax = mainTierPriceWithTax;
			} else if ( donation > 0 && tierPrice == 0 ) {
				var total = donation;
				var totalWithTax = donation;
			} else if ( tierPrice == 0 ) {
				var total = 0;
				var totalWithTax = 0;
			} else {
				var total = tierPrice + donation;
				total = parseFloat(total);
				var totalWithTax = mainTierPriceWithTax +donation;
			}
			var additionalTierTotal = parseFloat(getAdditionalTierTotal());
			total = total + additionalTierTotal;
			$("#purchase-total-value").html(convertToEuro(parseFloat(total).toFixed(2)));

			$("#total").val(total);

			var additionalTierPriceWithTax = parseFloat(getAdditionalTierTotalPriceWithTax());
			totalWithTax = totalWithTax + additionalTierPriceWithTax;
			$("#total-with-tax").html(convertToEuro(parseFloat(totalWithTax).toFixed(2)));

			checkTotalAmountForEproduct(parseFloat(totalWithTax).toFixed(2));

			//eproduct coupon application
			applyCouponToEproducttotal();
		});

		$('#donation-custom-amount').keyup(function() {
			$("#donation-custom-amount-radio-button").attr('checked', 'checked');
			var donation = parseFloat($('#donation-custom-amount').val());
			var tierPrice = parseFloat($("#tier_price").val());
			var total = tierPrice + donation;
			var mainTierPriceWithTax = parseFloat($("#main_tier_price_with_tax").val());
			var totalWithTax = mainTierPriceWithTax + donation;

			if ( isNaN(total) ) {
				total = tierPrice;
				totalWithTax = mainTierPriceWithTax;
			}
			var additionalTierTotal = parseFloat(getAdditionalTierTotal());
			total = total + additionalTierTotal;
			$("#purchase-total-value").html(convertToEuro(parseFloat(total).toFixed(2)));

			$("#total").val(total);

			var additionalTierPriceWithTax = parseFloat(getAdditionalTierTotalPriceWithTax());
			totalWithTax = totalWithTax + additionalTierPriceWithTax;
			$("#total-with-tax").html(convertToEuro(parseFloat(totalWithTax).toFixed(2)));

			checkTotalAmountForEproduct(parseFloat(totalWithTax).toFixed(2));

			//eproduct coupon application
			applyCouponToEproducttotal();
		});

		$( ".child-additional-product" ).on( "change", function() {
			var additionalTierTotal = parseFloat(getAdditionalTierTotal());
			var donationValue = parseFloat(getDonationValue());
			var tierPrice = parseFloat($("#tier_price").val());

			//get total price
			var total = additionalTierTotal + donationValue + tierPrice;
			var total_display = convertToEuro(total.toFixed(2));
			$("#purchase-total-value").html(total_display);
			$("#total").val(total);

			var additionalTierPriceWithTax = parseFloat(getAdditionalTierTotalPriceWithTax());
			var mainTierPriceWithTax = parseFloat($("#main_tier_price_with_tax").val());

			//get total price with tax
			var total_with_tax = additionalTierPriceWithTax + donationValue + mainTierPriceWithTax;
			$("#total-with-tax").html(convertToEuro(parseFloat(total_with_tax).toFixed(2)));

			checkTotalAmountForEproduct(parseFloat(total_with_tax).toFixed(2));

			//eproduct coupon application
			applyCouponToEproducttotal();
		} );

		function eproduct_total_with_coupon() {
			var total = $("#total").val();

			//get the total
			var coupon_discount = $("#coupon_discount").val();
			var new_total = parseFloat(total)-parseFloat(coupon_discount);

			if (coupon_discount != "" && coupon_discount != 0) {
				new_total = parseFloat(new_total).toFixed(2);
				$("#purchase-total-value").html(convertToEuro(new_total.toString()));
				setTimeout(getTotalPriceEproduct(), 15000);
			}
		}

		function getTotalPriceEproduct() {
			//get the total with tax
			var total_with_tax_raw = $("#total-with-tax-raw").val();
			if (coupon_discount != "" && coupon_discount != 0) {

				$("#total-with-tax").html(convertToEuro(parseFloat(total_with_tax_raw).toFixed(2).toString()));
			}
		}

		$('.eproduct-coupon-wrapper').on('DOMSubtreeModified', function(){
			eproduct_total_with_coupon();
		});

		function applyCouponToEproducttotal() {
			if( $('#product_type').length )
			{
				var productType = $("#product_type").val();
				if( productType == "ppyv_product" ) {
					eproduct_total_with_coupon();
				}

				//with discount
				var tier_discount = $("#tier_discount").val();
				if (tier_discount != "" && tier_discount != 0) {
					var total_with_tax_raw = $("#total-with-tax-raw").val();
					var discounted_main_product_price = $("#discounted_main_product_price").val();
					var main_tier_price = $("#main_tier_price").val();
					var main_tier_vat = $("#main_tier_vat").val();
					var price_tax = $("#price_tax").val();

					//remove the original price to the total amount
					if (price_tax != "inc") {
						var total = parseFloat(total_with_tax_raw) - (parseFloat(main_tier_price) + parseFloat(main_tier_vat));
					} else {
						var total = parseFloat(total_with_tax_raw) - parseFloat(main_tier_price);
					}


					//add the new discounted price to the toal amount
					var total = parseFloat(total) + parseFloat(discounted_main_product_price);

					$("#total-with-tax").html(convertToEuro(parseFloat(total).toFixed(2).toString()));
					$("#total-with-tax-raw").val(parseFloat(total).toFixed(2));
				}
			}
		}

		function checkTotalAmountForEproduct(totalWithTax) {
			if( $('#product_type').length )
			{
				var productType = $("#product_type").val();
				if( productType == "ppyv_product" ) {
					$("#total-with-tax-raw").val(parseFloat(totalWithTax).toFixed(2));
				}
			}
		}
    });

})( jQuery );
