<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      0.5.0
 *
 * @package    Poppyz
 * @subpackage Poppyz/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the sM Courses, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Poppyz
 * @subpackage Poppyz/public
 * @author     Socialmedium <info@socialmedium.nl>
 */
class Poppyz_Public {

    /**
     * The ID of this plugin.
     *
     * @since    0.5.0
     * @access   private
     * @var      string    $poppyz    The ID of this plugin.
     */
    private $poppyz;

    /**
     * The version of this plugin.
     *
     * @since    0.5.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;


    /**
     * Initialize the class and set its properties.
     *
     * @since    0.5.0
     * @var      string    $poppyz       The name of the plugin.
     * @var      string    $version    The version of this plugin.
     */
    public function __construct( $poppyz, $version ) {

        $this->poppyz = $poppyz;
        $this->version = $version;

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    0.5.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Poppyz_Public_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Poppyz_Public_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style( $this->poppyz, plugin_dir_url( __FILE__ ) . 'css/poppyz-public.css', array(), $this->version, 'all' );
        if ( $this->is_poppyz_page() && Poppyz_Core::get_option( 'disable_main_menu' ) == 'on' || get_post_meta(get_the_ID(), PPY_PREFIX . 'hide_main_menu', true )  ) {
            wp_enqueue_style( $this->poppyz . 'hide-menu', plugin_dir_url( __FILE__ ) . 'css/poppyz-public-hide-menu.css', array(), $this->version, 'all' );
        }
        if (is_page(Poppyz_Core::get_option('return_page'))) {
            wp_enqueue_style( $this->poppyz . 'jquery-confirm', plugin_dir_url( __FILE__ ) . 'css/jquery-confirm.min.css', array(), $this->version, 'all' );
        }

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    0.5.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Poppyz_Public_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Poppyz_Public_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script( $this->poppyz, plugin_dir_url( __FILE__ ) . 'js/poppyz-public.js', array( 'jquery' ), $this->version, false );


        if ( is_page( Poppyz_Core::get_option( 'register_page') ) || is_page( Poppyz_Core::get_option( 'purchase_page') ) ) {
            $rc3 = Poppyz_Core::get_option( 'recaptcha_v3_site_key');
            if ($rc3) {
                wp_enqueue_script(  'recaptcha-v3', 'https://www.google.com/recaptcha/api.js?render=' .  $rc3, array( 'jquery' ), $this->version, false );
                wp_add_inline_script( 'recaptcha-v3', "grecaptcha.ready(function () {
                                grecaptcha.execute('" . $rc3 . "', { action: 'register' }).then(function (token) {
                                    var recaptchaResponse = document.getElementById('recaptchaResponse');
                                    if (recaptchaResponse !== null) recaptchaResponse.value = token;
                                });
                            });"
                );
            }
        }


        if (is_page(Poppyz_Core::get_option('return_page'))) {
            wp_enqueue_script( 'jquery-confirm.min', plugin_dir_url( __FILE__ ) . 'js/jquery-confirm.min.js', array( 'jquery' ), $this->version, false );
        }

        global $ppy_lang;
        wp_localize_script( $this->poppyz, 'ppy_object',
            array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
                'prefix' => PPY_PREFIX,
                'site_url' => site_url(),
                'ppy_ajax_nonce' => wp_create_nonce( 'ppy_ajax_nonce'),
                'messages' => array (
                    'coupon_required' => __( 'Coupon code is required.' ,'poppyz'),
                    'no_coupon' => __( 'I don\'t have a coupon.' ,'poppyz'),
                )
            )
        );
    }

    public function adjacent_post_where( $sql ) {
        //use only if in the single lesson post type and if the user is not admin

        if ( !is_main_query() || !is_singular() || !is_singular( PPY_LESSON_PT )  ) {
            return $sql;
        }  elseif ( is_singular( PPY_LESSON_PT ) && is_user_logged_in() ) {
            $current_user = wp_get_current_user();

            $lesson_id = get_the_ID();
            $ppy_subs = new Poppyz_Subscription();

            $access = $ppy_subs->is_subscribed( $lesson_id, $current_user->ID);
            if ( is_wp_error ( $access ) ) {
                return $sql;
            }

            $the_post = get_post( $lesson_id );
            $patterns = array();
            $patterns[] = '/post_date/';
            $patterns[] = '/\'[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\'/';
            $replacements = array();
            $replacements[] = 'menu_order';
            $replacements[] = $the_post->menu_order;
            $sql =  preg_replace( $patterns, $replacements, $sql );
            $current_user = wp_get_current_user();
            $course_id = get_post_meta( $lesson_id, PPY_PREFIX . "lessons_course", true );
            $ids = $ppy_subs->get_lessons_by_subscription( $course_id, $current_user->ID );
            //the above code returns ALL subscriptions, meaning there will be sets of lessons per subscription. Here we will only take the first set to avoid complications.
            $a = array_slice($ids, 0, 1);
            $a_lessons = array_shift($a);
            $sql .= " AND p.ID IN (" . implode( ',', $a_lessons ) . ')' ;

        }
        return $sql;
    }

    public function adjacent_post_sort( $sql ) {
        //use only if in the single lesson post type and if the user is not admin
        if ( !is_main_query() || !is_singular() /*|| current_user_can( 'manage_options' )*/ )
            return $sql;

        $pattern = '/post_date/';
        $replacement = 'menu_order';

        return preg_replace( $pattern, $replacement, $sql );

    }

    function change_wp_title( $title ){

        if ( in_the_loop() && is_page( Poppyz_Core::get_option( 'return_page' ) ) && isset( $_GET['order_id'] ) ){
            global $wpdb;
            global $ppy_lang;
            $ppy_core = new Poppyz_Core();
            $table = $wpdb->prefix . "course_subscriptions";
            $order_id = $_GET['order_id'];
            $tier_id = $ppy_core->get_tier_by_slug( get_query_var( 'order-id' ) );
            $status = $wpdb->get_var( $wpdb->prepare(
                "
                    SELECT payment_status
                    FROM $table
                    WHERE order_id = %s
                    ",
                $order_id
            ) );

            if ($status == 'paid') {
                $title = get_post_meta( $tier_id, PPY_PREFIX . 'tier_thank_you_title', true );
                if (!$title) {
                    $title = Poppyz_Core::get_option('return_page_success_title');
                    if (!$title) {
                        $title = __('Thank you for your purchase.', 'poppyz');
                    }
                }
            } else {
                $title = __( 'Order cancelled', 'poppyz');
            }
        }

        if ( in_the_loop() && is_page( Poppyz_Core::get_option( 'purchase_page' ) ) && isset( $_REQUEST['buy'] ) && is_user_logged_in() ) {
            $ppy_core = new Poppyz_Core();
            $tier_id = $ppy_core->get_tier_by_slug( get_query_var( 'order-id' ) );
            $price = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true );

			$user_id = get_current_user_id();
			$donation = get_user_meta( $user_id, PPY_PREFIX . 'tier_donation_amount', true );

            if ( $price == 0 && empty($_POST[PPY_PREFIX . 'donation_amount']) && empty($donation) ) {
                global $ppy_lang;
                $title = get_post_meta( $tier_id, PPY_PREFIX . 'tier_thank_you_title', true );
                if (!$title) {
                    $title = Poppyz_Core::get_option('return_page_success_title');
                    if (!$title) {
                        $title = __( 'Thank you for subscribing', 'poppyz');
                    }
                }
            }
        }
        return $title;
    }

    function logout_redirect() {
        if (Poppyz_Core::get_option('redirect-logout_page')) {
            wp_redirect(Poppyz_Core::get_option('redirect-logout_page'));
            exit();
        } elseif ((int)Poppyz_Core::get_option('login_page') > 0) {
            wp_redirect(get_permalink(Poppyz_Core::get_option('login_page')));
            exit();
        } else {
            wp_redirect( home_url() );
            exit();
        }
    }

    function return_content( $content ) {
        if ( !is_singular() || !is_main_query() ) return $content;
        $template_args = [
            'content' =>  $content,
            'user_id' =>  wp_get_current_user() ? wp_get_current_user()->ID : null
        ];
        $return_content = '';
        if ( is_singular( PPY_COURSE_PT) ) {
            $return_content .= Poppyz_Template::get_template('public/course.php', $template_args );
        } elseif ( is_singular( PPY_LESSON_PT ) ) {
            $return_content .= Poppyz_Template::get_template('public/lesson.php', $template_args );
        } elseif ( Poppyz_Core::get_option( 'register_page') && is_page(  Poppyz_Core::get_option( 'register_page') ) || is_page(  Poppyz_Core::get_option( 'edit-profile_page'  ) ) ) {
            require_once( PPY_DIR_PATH . 'public/class-poppyz-user.php' );
            $user = new Poppyz_User();
            global $reg_errors;
            //if the page has been submitted without errors then do not display the register page's content anymore
            if ( isset ( $_POST['reg_submit'] ) && !$reg_errors &&  $user->page != 'edit'  ) {
                $return_content = $user->registration_form();
            } else {
                $return_content .= $user->registration_form();
            }

        } elseif ( is_page(  Poppyz_Core::get_option( 'login_page' ) ) ) {
            $return_content .= Poppyz_Template::get_template('public/login.php', $template_args );
        } elseif ( is_page(  Poppyz_Core::get_option( 'purchase_page' ) ) ) {
            require_once( PPY_DIR_PATH . 'public/class-poppyz-user.php' );
            $return_content .= Poppyz_Template::get_template('public/purchase.php', $template_args );
        } elseif ( is_page(  Poppyz_Core::get_option( 'account_page' ) ) ) {
            if ( !is_user_logged_in() ) {
                $return_content .= '<div id="et-boc" class="et-boc"><div class="et_pb_row et_pb_row_1 no-access-message">' . sprintf( __( 'You must first login to view you account. Click <a href="%s">here</a> to login. <br />' , 'poppyz'),  wp_login_url( get_permalink() ) ) . '</div>';
            } else {
                $return_content .= Poppyz_Template::get_template('public/account.php', $template_args );
            }

        } elseif ( isset( $_GET['order_id'] ) && isset( $_GET['return'] ) ||  ( isset( $_GET['success'] ) && isset( $_GET['subs_id'] ) ) ) {
            $return_content .= Poppyz_Template::get_template('public/return.php', $template_args );
        } else {
            $return_content .= $content;
        }

        return $return_content;
    }

    public function process_user() {

        if ( !is_admin() && !is_user_logged_in() && is_page( Poppyz_Core::get_option('edit-profile_page') ) ) {
            wp_redirect( wp_login_url( get_edit_profile_url() ) );
            die();
        }

        if ( isset( $_POST['reg_submit'] ) ) {
            require_once( PPY_DIR_PATH . 'public/class-poppyz-user.php' );
            $ppy_user = new Poppyz_User();
            $ppy_user->initialize_registration();
            $ppy_user->save();
        }
    }

    /**
     * Check if the user has access to a file before downloading
     *
     * @since    0.5.0
     */
    public function process_file() {

        if ( isset( $_GET['smcfile'] ) ) {
            Poppyz_Core::read_file( $_GET[ 'smcfile' ] );
        }

    }

    public function login_url( $login_url='', $redirect='' )
    {
        $login_page =  Poppyz_Core::get_option( 'login_page' );
        $new_login_url = get_permalink( $login_page );
        if ( !empty( $new_login_url )  &&  get_post_status( $login_page ) == 'publish' )  {
            $login_url = $new_login_url;
            if ( !empty( $redirect ) )
                $login_url = add_query_arg( 'redirect_to', urlencode($redirect), $new_login_url );
        }
        return $login_url;
    }

    public function custom_redirect_login( $redirect_to='', $request='' )
    {
        global $pagenow;
        if ( 'wp-login.php' == $pagenow &&  $_SERVER['REQUEST_METHOD'] == 'GET' && $this->login_url() != '' )
        {
            $redirect_url = $this->login_url( '', $redirect_to )  ;

            if (! empty($_GET['action']) )  {
                if ( 'lostpassword' == $_GET['action'] || 'rp' == $_GET['action'] )  {
                    return;
                }
                elseif ( 'register' == $_GET['action'] )  {
                    $redirect_url = get_permalink( Poppyz_Core::get_option( 'register_page' ) );
                }
                elseif ( 'logout' == $_GET['action'] ) {
                    wp_logout();
                }
            }
            elseif ( !empty( $_GET['loggedout'] )  ) {
                $redirect_url = add_query_arg( 'action', 'loggedout',  $this->login_url() );
            }

            wp_redirect( $redirect_url );
        }

    }

    function custom_login_failed( $username )
    {
        $referrer = wp_get_referer();

        if ( $referrer && ! strstr($referrer, 'wp-login') && ! strstr($referrer, 'wp-admin') )
        {
            $failed_array = array( 'action' => 'failed' );
            if ( !empty( $_POST['redirect_to'] ) ) {
                $redirect_to = urlencode( $_POST['redirect_to'] );
                $failed_array['redirect_to'] = $redirect_to;
            }

            if ( empty($_GET['loggedout']) )
                wp_redirect( add_query_arg( $failed_array,  $this->login_url() ) );
            else
                wp_redirect( add_query_arg('action', 'loggedout',  $this->login_url() ) );
            exit;
        }
    }
    function custom_authenticate_username_password( $user, $username, $password )
    {
        if ( is_a($user, 'WP_User') ) { return $user; }

        if ( empty($username) || empty($password) )
        {
            $error  = new WP_Error('authentication_failed', __('<strong>ERROR</strong>: Invalid username or incorrect password.', 'poppyz'));

            return $error;
        }
    }

    function login_redirect( $redirect_to, $request, $user ) {
        $redirect_page = Poppyz_Core::get_option( 'redirect-login_page' );
        $account_page = Poppyz_Core::get_option( 'account_page' );
        if( empty( $request ) ) {
            if  ( !empty( $redirect_page ) && $redirect_page != 'http://' ) {
                $redirect_to =  $redirect_page;
            } elseif ( !empty( $account_page ) ) {
                $redirect_to = get_permalink( $account_page );
            } else {
                $redirect_to = site_url();
            }
        }

        return $redirect_to;

    }


	public function apply_coupon() {
		$nonce = $_POST['security'];
		if ( !isset( $_POST['coupon_code'] )  || !wp_verify_nonce( $nonce, 'ppy_ajax_nonce' ) ) {
			die ( 'Invalid ajax call' );
		}

		$ppy_coupon = new Poppyz_Coupon();
		global $ppy_lang;

		$tier_id = $_POST['tier_id'];

		$coupon = $ppy_coupon->get_coupon_by_code( $_POST['coupon_code'] );
		if ( !$coupon ) {
			$return['content'] =  __('The coupon you entered is not valid.','poppyz');
		}
		$return['content'] = '';
		$return['final_price'] = '';
		$return['success'] = false;
		$return['coupon_discount'] = '';

		$valid_coupon = $ppy_coupon->is_coupon_valid( $coupon->ID, $tier_id  );
		if ( !is_wp_error( $valid_coupon ) && $valid_coupon ) {
			$id =  $coupon->ID;;
			$type = $ppy_coupon->get_coupon_type( $id );
			$amount = $ppy_coupon->get_coupon_amount( $id );
			$formatted_amount = $ppy_coupon->format_coupon_rate( $type, $amount );
			$base_price = $_POST['base_price'];

			$initial_price = (float)get_post_meta( $tier_id, PPY_PREFIX . 'tier_initial_price', true );

			if ( $initial_price ) {
				$base_price = $initial_price;
			}

			$ppy_coupon->set_coupon_session( $_POST['coupon_code'] );

			$final_price = $ppy_coupon->compute_discounted_price( $base_price, $tier_id, $_POST['coupon_code'] );
			$tier_discount = get_post_meta( $tier_id, PPY_PREFIX .  'tier_discount', true );

			//deduct the promotional discount to the final price
			if ( !empty($tier_discount) && $final_price>$tier_discount ) {
				$final_price = $final_price-$tier_discount;
				$return['final_price'] =  Poppyz_Core::format_price( $final_price );
				$return['tier_discount'] = $tier_discount;
			} else if( !empty($tier_discount) && $final_price<=$tier_discount ) {
				$return['final_price'] = 0;
			} else {
				$return['final_price'] =  Poppyz_Core::format_price( $final_price );
			}
			$price_tax = Poppyz_Core::get_price_tax( $tier_id );
			$tax_amount =  Poppyz_Core::get_fixed_tax_amount( $final_price, $tier_id, $price_tax );
			$tax_rate = (int)Poppyz_Core::get_tax_rate( $tier_id, get_current_user_id() );
			$return['tax_amount'] = ( $final_price > 0 ) ? Poppyz_Core::format_price( $tax_amount ) . ' ('. $tax_rate .'%)' : Poppyz_Core::format_price( 0 );
			$return['tax_amount_numeric_value'] = $tax_amount;

			$price_tax =  Poppyz_Core::get_price_tax( $tier_id );
			if ( $price_tax == 'inc' ) {
				$return['raw_tax_amount'] = 0;
				$return['final_price_with_tax'] = $final_price;
			} else {
				$raw_tax_amount = ($final_price > 0) ? $tax_amount : 0;
				$return['raw_tax_amount'] = $raw_tax_amount;
				$return['final_price_with_tax'] = $final_price+$raw_tax_amount;
			}

			$return['raw_unfiltered_tax_amount'] = ($final_price > 0) ? $tax_amount : 0;


			//$total_with_tax = Poppyz_Core::price_with_tax( $final_price, $sub->tier_id );

			//$ppy_sub->update_subscription( 'update', $_POST['subs_id'], array( 'price' => $final_price )  );

			$return['content'] =  '-' . $formatted_amount . ' <small><a href="#" id="remove-coupon">' .__( 'Remove coupon' ) . '</a></small>';
			$return['tier_discount'] = $tier_discount;

			$id = $coupon->ID;
			$return['coupon_discount'] = $ppy_coupon->get_coupon_amount( $id );

			//deduct the discount on the base price.

			if ( !empty($tier_discount) && $base_price>$tier_discount ) {
				$base_price = $base_price - $tier_discount;
			} else if ( !empty($tier_discount) && $base_price<=$tier_discount ) {
				$base_price = 0;
			}

			//get main product info
			//$price = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true );
			$return['main_product_price_include_tax'] = Poppyz_Core::price_with_tax( $base_price, $tier_id );

			$return['success'] = true;
		} else {
			$return['content'] = $valid_coupon->get_error_message();
		}

		echo json_encode( $return );

		wp_die();
	}

    public function remove_coupon() {
        $nonce = $_POST['security'];

        if ( ! wp_verify_nonce( $nonce, 'ppy_ajax_nonce' ) )
            die ('Invalid ajax call');

        Poppyz_Coupon::remove_coupon_session();

    }

    public function logout() {
        Poppyz_Coupon::remove_coupon_session();
    }

    public function logout_user() {
        if ( !empty( $_COOKIE['ppy_coupon'] ) ) {
            unset( $_COOKIE['ppy_coupon'] ) ;
            setcookie('ppy_coupon', '', time() - 3600, '/');
        }
    }

    public function tier_title( $tier_id, $price, $return = 'full' ) {
        $course_id = get_post_meta( $tier_id, PPY_PREFIX . 'tier_course', true );
        $title =  get_the_title( $course_id );

        //remove the course name if it's a bundled product
        if (Poppyz_Core::is_tier_bundled_products($tier_id)) {
            $title = '';
        }

		if ($return == 'title_only') {
			return $title . '-' . get_the_title( $tier_id ) ;
		}

        $payment_method = get_post_meta( $tier_id, PPY_PREFIX . 'payment_method', true );

        $initial_amount = get_post_meta( $tier_id,  PPY_PREFIX . 'tier_initial_price', true );

        global $ppy_lang;
        $every = '';
        $time['years'] = __( 'years' , 'poppyz');
        $time['year'] = __( 'year' , 'poppyz');
        $time['months'] = __( 'months', 'poppyz');
        $time['month'] = __( 'month' , 'poppyz');
        $time['days'] = __( 'days' , 'poppyz');
        $time['day'] = __( 'day' , 'poppyz');

        if ( $payment_method == 'plan' ) {
            $payment_frequency =  get_post_meta( $tier_id, PPY_PREFIX . 'payment_frequency', true );
            $timeframe = get_post_meta( $tier_id,  PPY_PREFIX . 'payment_timeframe', true );
            $number_of_payments = get_post_meta( $tier_id,  PPY_PREFIX . 'number_of_payments', true );

            if ( $payment_frequency > 1) {
                $every =  sprintf( __( 'per %s' , 'poppyz') , $payment_frequency ) . ' ' . $time[$timeframe];
            } else {
                $every =  __( 'per' , 'poppyz') . ' ' . $time[str_replace( 's', '', $timeframe )];
            }
            if ( $initial_amount ) {
                $number_of_payments = $number_of_payments - 1;
            }
            $every .= ' (' . $number_of_payments .'x) ';
        }
        if ( $payment_method == 'membership' ) {
            $timeframe = get_post_meta( $tier_id, PPY_PREFIX . 'membership_timeframe', true );
            $interval = get_post_meta( $tier_id, PPY_PREFIX . 'membership_interval', true );
            if ( $interval > 1 ) {
	            $every =  sprintf( __( 'per %s' , 'poppyz') , $interval ) . ' ' . $time[$timeframe] ;
            } else {
	            $timeframe = str_replace( 's', '', $timeframe );
	            $every =  __( 'per' , 'poppyz') . ' ' . $time[$timeframe];
            }
        }

		if ( $return == 'frequency' ) {
        	return $every;
		}

        $initial_amount_text = '';
        if ( $initial_amount ) {
            $initial_amount_text = sprintf( __( "Initially %s, and then " , 'poppyz'),  Poppyz_Core::format_price( $initial_amount ) );
        }

		//remove the course name if it's a bundled product
		if (Poppyz_Core::is_tier_bundled_products($tier_id)) {
			$initial_amount_text = ( !empty($initial_amount_text) ) ? $initial_amount_text : "";
			$payment_details =  "<span class='purchase-tier-price'>" . Poppyz_Core::format_price( $price ) . '</span>';

			if ( !empty($every) && !empty($payment_details) ) {
				$payment_details .= "<div class='tier-purchase-price-details'>". $initial_amount_text . Poppyz_Core::format_price( $price ) . ' ' . $every ."</div>";
			}
			return "<span class='purchase-tier-title'>".get_the_title( $tier_id ). "</span>" . $payment_details;
		}

		$payment_details =  "<span class='purchase-tier-price'>" . Poppyz_Core::format_price( $price ) . '</span>';
		if ( !empty($every) && !empty($payment_details) ) {
			$payment_details .= "<div class='tier-purchase-price-details'>". $initial_amount_text . Poppyz_Core::format_price( $price ) . ' ' . $every ."</div>";
		}

        $initial_interval = get_post_meta($tier_id, PPY_PREFIX . 'initial_payment_frequency', true);
        if ( $initial_interval ) {
            $initial_payment_timeframe = get_post_meta($tier_id, PPY_PREFIX . 'initial_payment_timeframe', true);
            $start_date = date_i18n(get_option('date_format'), strtotime( date('Y/m/d H:i:s') . ' + ' . $initial_interval . ' ' . $initial_payment_timeframe));
            $interval_details = '<span class="second-payment"> ' . sprintf( __( "Second payment on %s", 'poppyz' ), $start_date ) . '</span>';
            $payment_details =  $payment_details . ' ' . $interval_details;
        }

        if ( $return == 'payment_details' ) {
            return $payment_details;
        }
        return "<span class='purchase-tier-title'>".$title . '-' .get_the_title( $tier_id ). "</span>"  . $payment_details;
    }

    public function mailchimp_custom_fields( $user_id, $list_id, $data ) {
        return $data;
    }

    public function affwp_extended_integrations( $classes ) {
        $classes['poppyz'] =
            [
                'class' => '\Affiliate_WP_Poppyz',
                'name' => 'Poppyz',
                'file' => PPY_DIR_PATH . 'includes/affiliate-wp/class-poppyz.php',
            ];
        return $classes;
    }

    public function affwp_integrations_load() {
        $enabled = affiliate_wp()->settings->get( 'integrations', array() );
        $enabled = apply_filters( 'affwp_enabled_integrations',  $enabled);

        if ( isset( $enabled['poppyz'] ) )
            require_once( PPY_DIR_PATH . 'includes/affiliate-wp/class-poppyz.php' );
    }

    function endpoint() {
        if (is_plugin_active('affiliate-wp/affiliate-wp.php') && affwp_is_pretty_referral_urls() ) {
            $purchase_slug = get_post_field('post_name', Poppyz_Core::get_option( 'purchase_page'));
            add_rewrite_rule(
                '^' . $purchase_slug . '/order-id/([^/]+)/?$',
                'index.php?pagename=' . $purchase_slug . '&order-id=$matches[1]',
                'top'
            );

            add_rewrite_rule(
                '^' . $purchase_slug . '/order-id/([^/]+)/ref/([^/]+)/?$',
                'index.php?pagename=' . $purchase_slug . '&order-id=$matches[1]&ref=$matches[2]',
                'top'
            );
        } else {
            add_rewrite_endpoint( 'order-id',  EP_PERMALINK | EP_PAGES );
        }
    }

    function query_vars( $query_vars ) {
        $query_vars[] = 'order-id';
        if (is_plugin_active('affiliate-wp/affiliate-wp.php') && affwp_is_pretty_referral_urls() ) {
            $query_vars[] = 'ref';
        }
        return $query_vars;
    }


    function register_url( $register_url ) {
        return get_permalink( Poppyz_Core::get_option( 'register_page' ) );
    }

    function edit_profile_url( $url, $user_id, $scheme ) {

        if ( !current_user_can( 'manage_options' ) ) {
            return get_permalink( Poppyz_Core::get_option( 'edit-profile_page' ) );
        }
        else return $url;

    }

    public function custom_user_fields( $fields ) {
        $new_fields = [];
        foreach ( $fields as $key => $value ) {
            $updated_label = Poppyz_Core::get_option( 'reg_' . $value );
            if ( $updated_label ) {
                $new_fields[$updated_label] = $value;
            } else {
                $new_fields[$key] = $value;
            }
        }
        return $new_fields;
    }

    function lesson_category_archive($query) {
        if (!is_admin() && is_tax(PPY_LESSON_CAT) && $query->is_tax)
            $query->set( 'post_type', array(PPY_LESSON_PT, 'nav_menu_item', 'post') );
        remove_action( 'pre_get_posts', 'custom_post_archive' );
    }

    public function show_admin_bar($return) {
        if ( ! current_user_can( 'manage_options' ) ) {
            return Poppyz_Core::get_option( 'show_admin_bar' ) == 'on';
        }
        return $return;
    }

    public function fix_divi_edit_link( $is_shop ) {
        //trick divi to thinking this is a wc shop to get the proper ID of the post for use in the edit link
        if (get_post_type() == PPY_LESSON_PT || get_post_type() == PPY_COURSE_PT) {
            return true;
        }
        return $is_shop;
    }

    public function is_poppyz_page() {
        return get_post_type() == PPY_LESSON_PT || get_post_type() == PPY_COURSE_PT || is_tax( PPY_LESSON_CAT ) || get_post_type() == 'enhancedcategory';
    }

	public function is_payment_successful() {
		$nonce = $_POST['security'];
		if ( ! wp_verify_nonce( $nonce, 'ppy_ajax_nonce' ) )
			die ('Invalid ajax call');
		$ppy_sub = new Poppyz_Subscription();
		$subs = $ppy_sub->get_subscription_by_id($_POST['sub_id']);

		echo $subs->payment_id;
		die();
	}

}
