(function( $ ) {
	'use strict';

	/**
	 * All of the code for your Dashboard-specific JavaScript source
	 * should reside in this file.
	 *
	 * Note that this assume you're going to use jQuery, so it prepares
	 * the $ function reference to be used within the scope of this
	 * function.
	 *
	 * From here, you're able to define handlers for when the DOM is
	 * ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * Or when the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and so on.
	 *
	 * Remember that ideally, we should not attach any more than a single DOM-ready or window-load handler
	 * for any particular page. Though other scripts in WordPress core, other plugins, and other themes may
	 * be doing this, we should try to minimize doing that in our own work.
	 */

    $(function() {

        settings_hash_tab();

        $( ".ppy-loading").hide();
        $( "#ppy-settings-page").show();
        $( ".ppy-vertical-nav-tabs > li > a").append('<span class="ppy-menu-right-triangle "></span>');
        $( ".ppy-vertical-nav-tabs > li > a").addClass('dashicons-before');
        $( ".ppy-vertical-nav-tabs > li.has-tabs > a").append('<span class="dashicons dashicons-arrow-down-alt2"></span>');

        $( "#ppy-form-settings" ).tabs({
            activate: function(event, ui) {
                if (ui.newTab.hasClass('sub-tab')) {
                    //get second class which is the name of the "parent" tab.
                    //var classes = ui.newTab.attr("class").split(' ');
                    var hash = '#' +  ui.newPanel.attr("id");
                } else {
                    var hash = '#' +  ui.newPanel.attr("id")
                }
                history.replaceState({}, '', hash);
                settings_hash_tab(hash);
            }
        }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
        $( "#ppy-form-settings li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );

        $('.nav-tab-wrapper').each(function(){
            // For each set of tabs, we want to keep track of
            // which tab is active and it's associated content
            var $active, $content, $links = $(this).find('a');

            // If the location.hash matches one of the links, use that as the active tab.
            // If no match is found, use the first link as the initial active tab.
            $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
            $active.addClass('nav-tab-active');

            $content = $($active[0].hash);

            // Hide the remaining content
            $links.not($active).each(function () {
                $(this.hash).hide();
            });

            // Bind the click event handler
            $(this).on('click', 'a', function(e){
                // Make the old tab inactive.
                $active.removeClass('nav-tab-active');
                $content.hide();

                // Update the variables with the new link and content
                $active = $(this);
                $content = $(this.hash);

                // Make the tab active.
                $active.addClass('nav-tab-active');
                $content.show();


                settings_hash_tab(this.hash);


                // Prevent the anchor's default click action
                //e.preventDefault();
            });
        });

        //since sub tab links don't work on the fly, we will just create tab siblings but make them function as children of the "parent" tab.
        hide_sub_tabs();
        var parent_tab = $('#ppy-form-settings li.has-tabs > a');
        parent_tab.click(function(){
            //$('#ppy-form-settings li.ui-tabs-active').not('#'+$(this).parent().attr('id')).click();
            $('li.' + $(this).attr('href').substring(1)).slideToggle( 'fast', function(){

                if ($(this).is(':hidden')) {
                    parent_tab.children('span.dashicons').removeClass('dashicons-arrow-up-alt2');
                } else {
                    parent_tab.children('span.dashicons').addClass('dashicons-arrow-up-alt2');
                    //$(this).next().click();
                }
            });
            $('li.' + $(this).attr('href').substring(1)).first().children('a').click();

        });

        $('#ppy-form-settings li:not(.sub-tab) > a').click(function(){
            var parent = $(this).parent();
            $("#ppy-form-settings li.ui-tabs-active:not(.sub-tab)").not(parent).removeClass('ui-tabs-active').click();

        });

        $('#ppy-form-settings li.sub-tab a').click(function(){
            var classes = $(this).parent('li').attr("class").split(' ');
            //get the second class, this is the id of the "parent".
            $('a[href^="#'+ classes[1] +'"]').parent('li.has-tabs').addClass('ui-tabs-active');
        });

        if ( $("#ppy_tier_thank_you_page").val() === "" ) $("#thank-you-content").fadeIn();

        $("#ppy_tier_thank_you_page").change(function(){
           if ($(this).val() === "") {
               $("#thank-you-content").fadeIn();
           } else {
               $("#thank-you-content").fadeOut();
           }
        });

        $('a.ppy-toggle').click(function(){
            var button = $(this);
            console.log($(this));
            if (button.hasClass('open')) {
                button.removeClass('open');
                $("#add_user_to_course_wrapper").removeClass('active-nav-students');
                $("#add_user_to_course_wrapper").addClass('inactive-nav-students');
                $(".student-form-nav").removeClass('active-student-form-nav');
                $(".ppy-toggle-wrapper").fadeOut();

            } else {
                $(".ppy-toggle-wrapper").fadeIn();
                $(this).addClass('displayed');
                button.addClass('open');
                $('a.ppy-toggle-search-student').removeClass('open');
                $("#add_user_to_course_wrapper").removeClass('inactive-nav-students');
                $("#add_user_to_course_wrapper").addClass('active-nav-students');
                $(".student-form-nav").addClass('active-student-form-nav');

                //make the "Search" button inactive
                $("#search_user_to_course_wrapper").removeClass('active-nav-students');
                $("#search_user_to_course_wrapper").addClass('inactive-nav-students');

                //hide the student "Search" button
                $("#search-student").hide();

                $(".add-student-nav-wrapper").show();
                $("#add_subscription").show();
            }
        });
        $('a.ppy-toggle-search-student').click(function(){
            var button = $(this);
            if (button.hasClass('open')) {
                button.removeClass('open');
                $("#search_user_to_course_wrapper").removeClass('active-nav-students');
                $("#search_user_to_course_wrapper").addClass('inactive-nav-students');
                $(".student-form-nav").removeClass('active-student-form-nav');
                $(".ppy-toggle-wrapper").fadeOut();

            } else {
                $(".ppy-toggle-wrapper").fadeIn();
                $(this).addClass('displayed');
                button.addClass('open');
                $("#search_user_to_course_wrapper").removeClass('inactive-nav-students');
                $("#search_user_to_course_wrapper").addClass('active-nav-students');
                $(".ppy-toggle-student").removeClass('open');
                $(".student-form-nav").addClass('active-student-form-nav');

                //make the "Add a user to a course" button inactive
                $("#add_user_to_course_wrapper").removeClass('active-nav-students');
                $("#add_user_to_course_wrapper").addClass('inactive-nav-students');
                $(".add-student-nav-wrapper").hide();
                $("#add_subscription").hide();

                //hide the student "Search" button
                $("#search-student").show();
            }
        });

        $('.poppyz-tooltip').tooltip();

        $('#ppy-form-settings a.nav-tab:not(.sub-tab)').click(function(){

        });

        //notifications styling
        $('.poppyz-admin .notice').addClass('poppyz-notice dashicons-before');

        /*$(".white-box h2").next().hide();
        $(".white-box h2").click(function () {
            var header = jQuery(this);
            //getting the next element
            var content = header.next();
            //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
            content.slideToggle(500, function () {
                jQuery(header).toggleClass('opened highlight', $(this).is(':visible'));
            });

        });*/

        //clear the period selection when submitting the form because they are separate filters

        $('input#get-sales').click(function() {
            $('select#filter-links').val('');
        });

        $('select#period').change(function(){
            $('#ppy-start-date, #ppy-end-date').val('');
            //$(this).parents('.filter-form').submit();
            statistics_turnover("period");
        });
        $('select.filter-earnings').change(function(){
            $('#ppy-start-date, #ppy-end-date').val('');
            //$(this).parents('.filter-form').submit();
            statistics_bar_graph();
        });
        $('select.filter-overdue').change(function(){
            $('#ppy-start-date, #ppy-end-date').val('');
            statistics_overdue_pie_chart();
        });

        $('#get-sales').click(function() {
            statistics_turnover("by_date");
        });

        $('#get-future-payments').click(function() {
            statistics_future_payments();
        });
        $('#fp-reset').click(function() {
            statistics_future_payments(1);
        });

        $('select.filter-status-form').change(function(){
            $(this).parents('.filter-form').submit();
        });

        $(".statistics-earnings-nav").click(function() {
            if ($("#barchart").has(":contains('Cannot read properties of undefined')").length) {
                statistics_bar_graph();
            }
            if ($("#barchart").is(':empty')) {
                statistics_bar_graph();
            }
        });
        $(".statistics-overdue-nav").click(function() {
            if ($("#piechart_3d3").has(":contains('Cannot read properties of undefined')").length) {
                statistics_overdue_pie_chart();
            }
            if ($("#piechart_3d3").is(':empty')) {
                statistics_overdue_pie_chart();
            }
        });
        $(".statistics-future-payments-nav").click(function() {
            if( $('#fp_chart').is(':empty') ) {
                statistics_future_payments();
            }
        });

        if(window.location.hash) {
            var scrollto = $('form#ppy-form-settings ' + window.location.hash + ' > h2');
            if (scrollto.length) {
                $([document.documentElement, document.body]).animate({
                    scrollTop: scrollto.offset().top
                }, 2000);

            }
            var parent_tab = $('a[href^="'+ window.location.hash +'"]').parent('li.sub-tab');
            if (parent_tab.length) {
                var classes = parent_tab.attr("class").split(' ');
                $('li.' + classes[1]).slideDown();
            }

            $('a[href^="'+ window.location.hash +'"]').trigger('click');
        }


        function settings_hash_tab(hash) {
            var settings = jQuery('form#ppy-form-settings, .post-type-ppy_tier form#post');
            if (settings.length) {
                var currentUrl = settings.attr('action').split('#')[0];
                if (!hash) hash = location.hash;
                if (hash) settings.attr('action', currentUrl + hash);
            }
        }

        function hide_sub_tabs(id = false) {
            if (id) {

            } else {
                $('.sub-tab').hide();
            }
        }


        var poppyz_select = $('.poppyz-admin .poppyz-form select:not(.page-select), .poppyz-admin form#posts-filter select');
        change_select_color(poppyz_select);

        poppyz_select.click(function(){
            $(this).removeClass('placeholder-text');
        });

        poppyz_select.change(function(){
            change_select_color($(this));
        });

        function change_select_color(element) {
            if (element.val() === '-1') {
                element.addClass('placeholder-text');
            } else {
                element.removeClass('placeholder-text');
            }
        }

        var invoice_checkboxes = jQuery('#ppy-invoice table.invoices #the-list input[type=checkbox]');
        var invoice_export_buttons = $('.ppy-invoice-tools-wrapper button');

        has_invoice_checked(invoice_checkboxes);

        invoice_checkboxes.click(function(){
            has_invoice_checked(invoice_checkboxes);
        });

        $('#cb-select-all-1').click(function(){
            if ( $('#cb-select-all-1').is(":checked") ) {
                $("#invoice-select-all").val("1");
            } else {
                $("#invoice-select-all").val("");
            }
            has_invoice_checked($(this));
        });

        invoice_export_buttons.click(function(){
           if ( $(this).hasClass('inactive') ) {
               alert('Please select at least one invoice');
               return false;
           }
        });

        //prevent same page from being selected
        $(".page-select").change(function(){
            var selVal=[];

            $(".page-select").each(function(){
                if (this.value != '-1')
                    selVal.push(this.value);
            });

            //console.log($.inArray(this.value, selVal));


            $(".page-select").not(this).find("option").removeAttr("disabled").filter(function(){
                var a=$(this).parent("select").val();
                return (($.inArray(this.value, selVal) > -1) && (this.value!=a))
            }).attr("disabled","disabled");
        });

        $(".page-select").eq(0).trigger('change');

        function has_invoice_checked(checkbox) {
            if (checkbox.filter(':checked').length) {
                //invoice_export_buttons.prop("disabled", false);
                invoice_export_buttons.removeClass("inactive");
            } else {
                //invoice_export_buttons.prop("disabled", true);
                invoice_export_buttons.addClass("inactive");
            }
        }

        $('#ppy-invoice .search-box input[type="text"], #ppy-invoice .search-box input[type="search"]').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                event.preventDefault();
                $('#search-submit').click();
            }
        });

        $('.poppyz-table-id-checkbox').change(function() {
            var studentIdArray = [];
            $(".poppyz-table-id-checkbox:checked").each(function(){
                studentIdArray.push($(this).val());
            });
            if ( studentIdArray != "") {
                $('.export-button').prop('disabled', false);
                $('.export-button').removeClass("inactive");
                $('.export-button').addClass("students-export-button-active");
            } else {
                $('.export-button').prop('disabled', true);
                $('.export-button').addClass("inactive");
                $('.export-button').removeClass("students-export-button-active");
            }
            $("#student_id_array").val(JSON.stringify(studentIdArray));
            console.log(JSON.stringify(studentIdArray));
        });
        $(".export-button").click(function(){
           $("#export_type").val($(this).attr("id"));
           $("#student-form").submit();
        });

        $(".check_student_id").change(function() {
            if ($('.check_student_id').is(':checked')) {
                $( ".poppyz-table-id-checkbox" ).prop( "checked", true );
                $('.export-button').prop('disabled', false);
                $('.export-button').removeClass("inactive");
                $('.export-button').addClass("students-export-button-active");
                $('.poppyz-table-text').addClass("poppyz-table-row-active");
                $('.poppyz-table-text').removeClass("poppyz-table-row-inactive");
            } else {
                $( ".poppyz-table-id-checkbox" ).prop( "checked", false );
                $('.export-button').prop('disabled', true);
                $('.export-button').addClass("inactive");
                $('.export-button').removeClass("students-export-button-active");
                $('.poppyz-table-text').removeClass("poppyz-table-row-active");
                $('.poppyz-table-text').addClass("poppyz-table-row-inactive");
            }

        });
        $(".poppyz-table-id-checkbox").change(function() {
            var currentId = $(this).attr("id");

            if (!$(this).hasClass('poppyz-table-row-active')) {
                $('.poppyz-table-text').addClass("poppyz-table-row-inactive");
            }
            if ($(this).is(':checked')) {
                $('.poppyz-table-'+currentId+'-text').removeClass("poppyz-table-row-inactive");
                $('.poppyz-table-'+currentId+'-text').addClass("poppyz-table-row-active");

            } else {
                $('.poppyz-table-'+currentId+'-text').removeClass("poppyz-table-row-active");
            }
            var checkedBox = 0;
            var id = 0;
            $(".poppyz-table-id-checkbox").each(function() {
                if (!$("#"+id).is(':checked')) {
                } else {
                    checkedBox++;
                    $('.poppyz-table-'+id+'-text').removeClass("poppyz-table-row-inactive");
                }
                id++;
            });

            if (checkedBox == 0) {
                $('.poppyz-table-text').removeClass("poppyz-table-row-inactive");
            }

        });
        $('#bulk-action').click(function(event){
            var option = $("#student-bulk-action").find('option:selected');
            var action = option.val();
            var activeStudent = "";
            var cancelledStudent = "";
            var studentToUpdateCount = 0;

            $(".poppyz-table-id-checkbox[type=checkbox]:checked").each(function() {
                var checkboxId = $(this).attr("id");
                var userId = $(this).val();
                var userStatus = $("#student_status_"+checkboxId).val();
                var userName = $("#student_name_"+checkboxId).val();
                if ( action == "subscribe" && userStatus == "Active" ) {
                    if ( activeStudent != "" ) {
                        activeStudent += ", "+userName;
                    } else {
                        activeStudent += userName;
                    }
                } else if ( action == "cancel" && userStatus == "Inactive" ) {
                    if ( cancelledStudent != "" ) {
                        cancelledStudent += ", "+userName;
                    } else {
                        cancelledStudent += userName;
                    }
                } else {
                    studentToUpdateCount++;
                }
            });
            if( action == "cancel" ) {
                if ( cancelledStudent != "" ) {
                    var message = cancelledStudent + " is/are already cancelled.";
                    alert(message);
                }
                if ( studentToUpdateCount == 0 ) {
                    return false;
                }
            }
            if( action == "subscribe" ){
                if ( activeStudent != "" ) {
                    var message = activeStudent+" is/are already active.";
                    alert(message);
                }
                if ( studentToUpdateCount == 0 ) {
                    return false;
                }
                var message = "The selected users will be manually marked as paid. Do you want to continue?";
            } else {
                var message = "This will "+action+" the selected users subscription. Are you sure you want to continue?";
            }
            if(!confirm(message)){
                event.preventDefault();
            } else {
                $('#student-form').submit();
            }
        });

        //check if the page is class and run all the ajax for statistics charts
        if ($("body").hasClass("poppyz_page_poppyz-statistics")) {
            init_statistics_charts();
        }

        if ($("body").hasClass("poppyz_page_poppyz-invoices")) {
            total_earnings("invoice_total_earnings");
        }

        function init_statistics_charts() {

            var hash = location.hash.substr(1);
            if (hash == "overdue") {
                statistics_overdue_pie_chart();
                statistics_turnover("default");
                statistics_bar_graph();
                total_earnings("header_total_earnings");
            } else if (hash == "graph-period") {
                statistics_bar_graph();
                statistics_turnover("default");
                statistics_overdue_pie_chart();
                total_earnings("header_total_earnings");
            } else if (hash == "future-payments") {
                //statistics_future_payments();
                statistics_turnover("default");
                total_earnings("header_total_earnings");
            } else {
                statistics_overdue_pie_chart();
                statistics_bar_graph();
                statistics_turnover("default");
                total_earnings("header_total_earnings");
            }
        }

        function statistics_turnover(filter_type) {
            if (filter_type == "period") {
                var period = $("#period").val();
                var start_date = "";
                var end_date = "";
            } else if (filter_type == "by_date") {
                var period = "";
                var start_date = $("#ppy-start-date").val();
                var end_date = $("#ppy-end-date").val();
            } else {
                var period = $("#period").val();
                var start_date = $("#ppy-start-date").val();
                var end_date = $("#ppy-end-date").val();
            }
            $(".range_turnover").html("");

            $("#turnovers-table > tbody").html("");
            const myObj = {
                style: "currency",
                currency: "EUR"
            }
            $(".statistics_loading").fadeIn();
            var data = {
                action: 'statistics_turover',
                security: ppy_object.ppy_ajax_nonce,
                start_date: start_date,
                end_date : end_date,
                period : period
            };
            $.ajax ({
                type: 'POST',
                url: ppy_object.ajax_url,
                data: data,
                success: function( response ) {
                    var object = jQuery.parseJSON( response );
                    let total_turnover = 0;
                    $.each(object.turnovers.turnovers, function(key, value) {
                        total_turnover = Number(total_turnover)+Number(value.amount);
                        let num = Number(value.amount).toFixed(2);
                        let euro = Intl.NumberFormat('de-DE', {
                        });
                        let extension = "";

                        if(Number.isInteger(value.amount) == true) {
                            extension = ",-";
                        }

                        let table_content = "<td>"+value.invoice_id+"</td>";
                        table_content += "<td>"+value.subscription_id+"</td>";
                        table_content += "<td>"+value.tier_title+"</td>";
                        table_content += "<td>"+value.payment_date+"</td>";
                        table_content += "<td>€ "+euro.format(num)+extension+"</td>";

                        $("#turnovers-table > tbody").append("<tr>"+table_content+"</tr>");
                    });
                    let total_turnover_amount = Number(total_turnover).toFixed(2);
                    let euro = Intl.NumberFormat('de-DE', {
                    });
                    $("#total_turnover").html("€ "+euro.format(total_turnover_amount));
                    $(".statistics_loading").fadeOut();

                    const start_date = new Date(object.start);
                    const start_month = start_date.toLocaleString('default', { month: 'long' });
                    const start_year = start_date.getFullYear();
                    const start_dd = start_date.getDate();

                    const end_date = new Date(object.end);
                    const end_month = end_date.toLocaleString('default', { month: 'long' });
                    const end_year = end_date.getFullYear();
                    const end_dd = end_date.getDate();
                    if(object.start != null || object.end != null) {
                        $(".range_turnover").html(start_dd+" "+start_month+" "+start_year+" to "+end_dd+" "+end_month+" "+end_year);
                    }
                }
            });
        }

        function statistics_bar_graph() {
            var period = $('#graph-period').find(":selected").val();
            var data = {
                action: 'statistics_graph_data',
                security: ppy_object.ppy_ajax_nonce,
                graph_period : period
            };
            $(".statistics-loading-graph-period").fadeIn();
            $.ajax ({
                type: 'POST',
                url: ppy_object.ajax_url,
                data: data,
                success: function( response ) {
                    //drawMultSeries(response);
                    const response_array = [];
                    var object = jQuery.parseJSON( response );
                    let count = 0;
                    $.each(object, function(key, value) {
                        let row_data = [];
                        row_data[0] =value[0];
                        row_data[1] =value[1];
                        response_array[count] =row_data;
                        count++;
                    });
                    drawMultSeries(response_array);
                    $(".statistics-loading-graph-period").fadeOut();
                }
            });
        }
        function drawMultSeries(chart_data) {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Month');
            data.addColumn('number', 'Amount');
            data.addRows(chart_data);
            var options = {
                width: '100%',
                height: '100%',
                colors: ['#29C4A9', '#e6693e', '#ec8f6e', '#f3b49f', '#f6c7b6'],
                backgroundColor: '#f6f9fb',
                chartArea: {
                    backgroundColor: '#f6f9fb'
                },
            };

            var chart = new google.visualization.ColumnChart(
                document.getElementById('barchart'));
            var formatter = new google.visualization.NumberFormat({decimalSymbol: ',',groupingSymbol: '.', negativeColor: 'red', negativeParens: true, prefix: '€ '});
            formatter.format(data, 1);
            chart.draw(data, options);
        }

        function statistics_overdue_pie_chart() {
            $(".statistics-loading-overdue").fadeIn();
            var period = $('.filter-overdue').find(":selected").val();
            var data = {
                action: 'statistics_graph_data',
                security: ppy_object.ppy_ajax_nonce,
                graph_period : period,
                type: "overdue"
            };
            var options = {
                is3D: false,
                width: '100%',
                height: '100%',
                pieSliceText: 'percentage',
                colors: ['#29C4A9', '#EF974A', '#4387D4', '#EF5555', '#EF974A'],
                chartArea: {
                    left: "3%",
                    top: "3%",
                    height: "94%",
                    width: "94%",
                    backgroundColor: '#f6f9fb'
                },
                legend: {
                    position: 'right'
                },
                backgroundColor: '#f6f9fb',


            };
            //$(".statistics-loading-graph-period").fadeIn();
            $.ajax ({
                type: 'POST',
                url: ppy_object.ajax_url,
                data: data,
                success: function( response ) {
                    const response_array = [];
                    var object = jQuery.parseJSON( response );

                    if (object.data == "") {
                        $(".no-overdue-statistics").fadeIn();
                    } else {
                        var data2 = new google.visualization.DataTable();
                        data2.addColumn('string', 'Type');
                        data2.addColumn('number', 'Value');

                        data2.addRows(object.data);

                        overdue_chart(data2, options, 'piechart_3d3');
                    }
                    $(".statistics-loading-overdue").fadeOut();

                }
            });
        }

        function overdue_chart(data, options, id) {
            var chart = new google.visualization.PieChart(document.getElementById(id));
            var formatter = new google.visualization.NumberFormat({decimalSymbol: ',',groupingSymbol: '.', negativeColor: 'red', negativeParens: true, prefix: '€ '});
            formatter.format(data, 1);
            chart.draw(data, options);
        }

        function total_earnings(id) {
            $("#"+id).html("Loading...");
            var data = {
                action: 'total_earnings',
                security: ppy_object.ppy_ajax_nonce
            };
            $.ajax ({
                type: 'POST',
                url: ppy_object.ajax_url,
                data: data,
                success: function( response ) {
                    let euro = Intl.NumberFormat('de-DE', {
                    });
                    let extension = "";
                    if(Number.isInteger(response) == true) {
                        extension = ",-";
                    }
                    let num = Number(response).toFixed(2);
                    $("#"+id).html("€ "+euro.format(num)+extension);

                }
            });
        }

        function statistics_future_payments(reset = 0) {
            $(".statistics-loading-future-payments").fadeIn();
            var period = $('.filter-overdue').find(":selected").val();
            var payment_method = $("#payment_method").find(":selected").val();
            var from = $("#ppy-fp-from-date").val();
            var to = $("#ppy-fp-to-date").val();
            var data = {
                action: 'remaining_payments_statistics',
                security: ppy_object.ppy_ajax_nonce,
                graph_period : period,
                type: "future-payments",
                from: from,
                to:to,
                payment_method : payment_method,
                reset: reset
            };
            var options = {
                is3D: false,
                width: '100%',
                height: '100%',
                pieSliceText: 'percentage',
                colors: ['#29C4A9', '#EF974A', '#4387D4', '#EF5555', '#EF974A'],
                chartArea: {
                    left: "3%",
                    top: "3%",
                    height: "94%",
                    width: "94%",
                    backgroundColor: '#f6f9fb'
                },
                legend: {
                    position: 'right'
                },
                backgroundColor: '#f6f9fb',


            };
            //$(".statistics-loading-graph-period").fadeIn();
            $.ajax ({
                type: 'POST',
                url: ppy_object.ajax_url,
                data: data,
                success: function( response ) {
                    const response_array = [];
                    var object = jQuery.parseJSON( response );
                    drawLineChart(object.chart_data);
                    $("#total_future_payments_statistics").html(object.total_amount);
                    if (object.table_data == '') {
                        $('#fp_chart').hide();
                    }
                    if( reset == true ) {
                        $("#ppy-fp-from-date").val(object.from);
                        $("#ppy-fp-to-date").val(object.to);
                    }
                    $(".statistics-loading-future-payments").fadeOut();
                }
            });

        }

        //line chart
        setTimeout(function() {
            google.charts.load('current', {'packages': ['corechart']});
            google.charts.setOnLoadCallback(drawLineChart);
        }, 4000);

        //load the chart if the
        $("#future-payments-tab").click(function(){
            google.charts.load('current', {'packages': ['corechart']});
            google.charts.setOnLoadCallback(drawLineChart);
        });

        function drawLineChart(chart_data) {

            var data = google.visualization.arrayToDataTable(chart_data);
            var options = {
                title: 'Company Performance',
                curveType: 'function',
                legend: { position: 'bottom' }
            };

            var chart = new google.visualization.LineChart(document.getElementById('fp_chart'));
            var formatter = new google.visualization.NumberFormat({decimalSymbol: ',',groupingSymbol: '.', negativeColor: 'red', negativeParens: true, prefix: '€ '});
            formatter.format(data, 1);
            var formatter = new google.visualization.NumberFormat({decimalSymbol: ',',groupingSymbol: '.', negativeColor: 'red', negativeParens: true, prefix: '€ '});
            formatter.format(data, 2);

            chart.draw(data, options);
        }




        // $(".sales-list").click(function() {
        //     statistics_turnover("default");
        // });
        // var hash = location.hash.substr(1);
        // if (hash == "graph-period") {
        //     statistics_bar_graph();
        // }

        // $("#test").click(function(){
        //     statistics_overdue_pie_chart();
        // });

    });
})( jQuery );


/*
 * throttledresize: special jQuery event that happens at a reduced rate compared to "resize"
 *
 * latest version and complete README available on Github:
 * https://github.com/louisremi/jquery-smartresize
 *
 * Copyright 2012 @louis_remi
 * Licensed under the MIT license.
 *
 * This saved you an hour of work?
 * Send me music http://www.amazon.co.uk/wishlist/HNTU0468LQON
 */
(function($) {
    var $event = $.event,
        $special,
        dummy = {_:0},
        frame = 0,
        wasResized, animRunning;

    $special = $event.special.throttledresize = {
        setup: function() {
            $( this ).on( "resize", $special.handler );
        },
        teardown: function() {
            $( this ).off( "resize", $special.handler );
        },
        handler: function( event, execAsap ) {
            // Save the context
            var context = this,
                args = arguments;

            wasResized = true;

            if ( !animRunning ) {
                setInterval(function(){
                    frame++;

                    if ( frame > $special.threshold && wasResized || execAsap ) {
                        // set correct event type
                        event.type = "throttledresize";
                        $event.dispatch.apply( context, args );
                        wasResized = false;
                        frame = 0;
                    }
                    if ( frame > 9 ) {
                        $(dummy).stop();
                        animRunning = false;
                        frame = 0;
                    }
                }, 30);
                animRunning = true;
            }
        },
        threshold: 0
    };
    $( "#select-user-input" ).autocomplete({
        source: function( request, response ) {
            console.log(request);
            // Fetch data
            $.ajax({
                url: ppy_object.ajax_url,
                type: 'post',
                dataType: "json",
                data: {
                    search: request.term
                },
                success: function( data ) {
                    console.log(data);
                    response( data );
                }
            });
        },
        select: function (event, ui) {
            // Set selection
            $('#select-user-input').val(ui.item.label); // display the selected text
            $('#select-user-hidden').val(ui.item.value); // save selected id to input
            return false;
        },
        focus: function(event, ui){
            $( "#select-user-input" ).val( ui.item.label );
            $( "#select-user-hidden" ).val( ui.item.value );
            return false;
        },
    });
    $(".delete-product-image").click(function(e) {
        e.preventDefault();
        var additionalProductImage = $(this).attr('id');
        var attachmentId = $(this).attr('attachment-id');
        var postId = $(this).attr('post-id');

        var data = {
            action: 'delete_additiona_product_image',
            security: ppy_object.ppy_ajax_nonce,
            additonal_product_image: additionalProductImage,
            post_id : postId,
            attachment_id : attachmentId
        };
        console.log(data);
        $.ajax ({
            type: 'POST',
            url: ppy_object.ajax_url,
            data: data,
            success: function( response ) {
                $("#"+additionalProductImage+"_wrapper").fadeOut();
                $("#"+additionalProductImage).fadeOut();
            }
        });
    });

    $("input[name='ppy_tier_voluntary_contribution']").on('change', function() {
        var allowVoluntaryContribution = $("input[name='ppy_tier_voluntary_contribution']:checked").val();
        if (allowVoluntaryContribution == "yes") {
            $("#ppy-donation-title").css("display", "flex");
            $("#ppy-donation-description").fadeIn();
            $("#ppy-donation-fixed-amount").fadeIn();
            $("#ppy-donation-custom-amount").fadeIn();
            $("#tier_donation_image_url_wrapper").fadeIn();
            $("#ppy-donation-image").fadeIn();
            $("#ppy-donation-image-cropping").fadeIn();

        } else {
            $("#ppy-donation-title").fadeOut();
            $("#ppy-donation-description").fadeOut();
            $("#ppy-donation-fixed-amount").fadeOut();
            $("#ppy-donation-custom-amount").fadeOut();
            $("#tier_donation_image_url_wrapper").fadeOut();
            $("#ppy-donation-image").fadeOut();
            $("#ppy-donation-image-cropping").fadeOut();
        }
    });

    $("input[name='ppy_tier_schedule_voluntary_contribution']").on('change', function() {
        var allowTierVoluntaryContribution = $("input[name='ppy_tier_schedule_voluntary_contribution']:checked").val();
        if ( allowTierVoluntaryContribution == "yes" ) {
            $(".scheduled-voluntary-contribution").fadeIn();
        }
    });

    $("#ppy_tier_schedule_voluntary_contribution_disable").click(function() {
        if($('#ppy_tier_schedule_voluntary_contribution_disable').is(':checked')) {
            $(".scheduled-voluntary-contribution").fadeOut();
        }
    });
    $("#ppy_tier_schedule_voluntary_contribution_enable").click(function() {
        if($('#ppy_tier_schedule_voluntary_contribution_enable').is(':checked')) {
            $(".scheduled-voluntary-contribution").fadeIn();
        }
    });


    function image_resize() {
            $('#uploaded-image').fadeIn();
            $('.donation-image-crop-wrapper').fadeIn();
            var image_url = $("#donation-upload-image").val();
            resize.croppie('bind',{
                url: image_url
            }).then(function(){
                console.log('jQuery bind complete');
            });

    }
    var resize = $('#uploaded-image').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 200,
            height: 200,
            type: 'square' //square
        },
        boundary: {
            width: 432,
            height: 300
        }
    });
    $('.post-type-ppy_tier #publish').on('click', function (event) {
        var ppy_tier_voluntary_contribution_enable =  $("input[name='ppy_tier_voluntary_contribution']:checked").val();
        if ( ppy_tier_voluntary_contribution_enable == "yes") {
            var donation_upload_image = $("#donation-upload-image").val();
            if ( donation_upload_image != "" ) {
                var tierId = $("#tier_id").val();
                resize.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (img) {

                    var data = {
                        action: 'upload_donation_image',
                        security: ppy_object.ppy_ajax_nonce,
                        image_donation: img,
                        tier_id: tierId
                    };
                    console.log(data);
                    $.ajax ({
                        type: 'POST',
                        url: ppy_object.ajax_url,
                        data: data,
                        success: function( response ) {
                            //location.reload();
                            $("#post").submit();
                        }
                    });
                });
            }

        }

    });
    $('#upload-donation-image').on('click', function (event) {
        event.preventDefault();
        var tierId = $("#tier_id").val();
        resize.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {

            var data = {
                action: 'upload_donation_image',
                security: ppy_object.ppy_ajax_nonce,
                image_donation: img,
                tier_id: tierId
            };
            console.log(data);
            $.ajax ({
                type: 'POST',
                url: ppy_object.ajax_url,
                data: data,
                success: function( response ) {
                    location.reload();
                }
            });
        });
    });
    $('.post-type-ppyv_product #publish').on('click', function (event) {
        var ppy_tier_voluntary_contribution_enable =  $("input[name='ppy_tier_voluntary_contribution']:checked").val();
        if ( ppy_tier_voluntary_contribution_enable == "yes") {
            var donation_upload_image = $("#donation-upload-image").val();
            if ( donation_upload_image != "" ) {
                var tierId = $("#tier_id").val();
                resize.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (img) {

                    var data = {
                        action: 'upload_donation_image',
                        security: ppy_object.ppy_ajax_nonce,
                        image_donation: img,
                        tier_id: tierId
                    };
                    console.log(data);
                    $.ajax ({
                        type: 'POST',
                        url: ppy_object.ajax_url,
                        data: data,
                        success: function( response ) {
                            //location.reload();
                            //$("#post").submit();

                        }
                    });
                });
            }

        }

    });

    $(".delete-donation-image").click(function(e) {
        e.preventDefault();
        var additionalProductImage = $(this).attr('id');
        var attachmentId = $(this).attr('attachment-id');
        var postId = $(this).attr('post-id');

        var data = {
            action: 'delete_additiona_product_image',
            security: ppy_object.ppy_ajax_nonce,
            additonal_product_image: additionalProductImage,
            post_id : postId,
            attachment_id : attachmentId
        };
        console.log(data);
        $.ajax ({
            type: 'POST',
            url: ppy_object.ajax_url,
            data: data,
            success: function( response ) {
                $("#"+additionalProductImage+"_wrapper").fadeOut();
                $("#"+additionalProductImage).fadeOut();
                location.reload();
            }
        });
    });

    var custom_uploader;

    $('#upload_image_button').click(function(e) {
        e.preventDefault();
        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }

        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: false
        });

        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            $('#donation-upload-image').val(attachment.url);
            image_resize();
        });

        //Open the uploader dialog
        custom_uploader.open();
    });

    $(".rp-primary-row").click(function() {
        var id = $(this).attr('id');
        if ($("#"+id).hasClass("rp-row-close")) {
            $(".rp-row-"+id).fadeIn();
            $("#"+id).removeClass("rp-row-close");
            $("#"+id).addClass("rp-row-open");
            $("#"+id).addClass("rp-primary-row-active");
        } else {
                $(".rp-sub-row-" + id).fadeOut();
                $("#" + id).addClass("rp-row-close");
                $("#" + id).removeClass("rp-row-open");
            $("#"+id).removeClass("rp-primary-row-active");
        }
    });





})(jQuery);