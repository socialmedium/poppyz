<?php

class Poppyz_Statistics {

    public function __construct() {

    }

    //date format 'm/d/Y';
    public static function get_sales_by_date( $start_date, $end_date) {
        //check_ajax_referer( PPY_PREFIX . 'statistics', 'security' );
        global $ppy_lang;
        $ppy_invoice = new Poppyz_Invoice();
        /*if (  empty( $_POST['start_date'] )|| empty( $_POST['end_date'] ) ) {
            echo '<br/><p class="notice notice-error">' . __( 'Please fill in start and end dates' ) . '</p>';
            die();
        }*/



        $meta_query_args = array();
        if ( !empty($start_date . $end_date) ) {
            /*$d = DateTime::createFromFormat( 'Y-m-d', $start_date );
            if ( $d ) {
                $start_date = date( 'Y-m-d H:i:s', $d->getTimestamp() ) ;
            }
            $d = DateTime::createFromFormat( 'Y-m-d', $end_date );
            if ( $d ) {
                $end_date = date( 'Y-m-d H:i:s', $d->getTimestamp() ) ;
            }*/
            $meta_query_args = array(

				array (
					'relation' => 'OR',
					array (
						'key' => PPY_PREFIX .'invoice_mode',
						'value' => "Live",
						'compare' => 'LIKE',
					),
					array (
						'key' => PPY_PREFIX .'invoice_mode',
						'compare' => 'NOT EXISTS',
					)
				),
                array(
                        'relation' => 'AND',
					array(
						'key'     => PPY_PREFIX .'invoice_date_created',
						'value' => array( $start_date, $end_date ),
						'compare' => 'BETWEEN',
						'type' => 'CHAR'
					)
                )
            );
        } else {
			$meta_query_args = array (
					'relation' => 'OR',
					array (
						'key' => PPY_PREFIX .'invoice_mode',
						'value' => "Live",
						'compare' => 'LIKE',
					),
					array (
						'key' => PPY_PREFIX .'invoice_mode',
						'compare' => 'NOT EXISTS',
					)
				);

        }

        /*if ( $_POST['tier_id'] != '' && $_POST['tier_id'] != '-1' ) {
            $meta_query_args[] = array(
                'key'     => PPY_PREFIX .'invoice_tier_id',
                'value' => $_POST['tier_id'] ,
                'compare' => '='
            );
        }*/

        $args = array(
            'post_status' => 'paid',
            'posts_per_page' => '-1',
            'meta_query' => $meta_query_args

        );
        $invoices = Poppyz_Invoice::get_invoices( $args );
        return $invoices;
        /*if ( $invoices ) {
            $total = 0;

            echo '<table class="widefat fixed">';
            echo '<tr>';
            echo '<th>' . __( 'Invoice ID' ) . '</th>';
            echo '<th>' . __( 'Tier' ) . '</th>';
            echo '<th>' . __( 'Payment Date' ) . '</th>';
            echo '<th>' . __( 'Amount' ) . '</th>';
            echo '</tr>';

            foreach ( $invoices as $invoice ) {
                $id = $invoice->ID;
                $date_created = $ppy_invoice->get_invoice_field( $id, 'date_created' );
                $amount = $ppy_invoice->get_invoice_field( $id, 'amount' );
                $tax_rate = (int)$ppy_invoice->get_invoice_field( $id, 'tax_rate' );
                $tier_id = $ppy_invoice->get_invoice_field( $id, 'tier_id' );
                $tax_amount = 0;
                if ( $tax_rate > 0 ) {
                    $tax_amount = $amount * ( $tax_rate / 100 ) ;
                }

                $total_with_tax = $amount + $tax_amount;
                $total += $total_with_tax;

                echo '<tr>';
                echo '<td>';
                echo $invoice->post_title;
                echo '</td>';
                echo '<td>';
                echo get_the_title( $tier_id );
                echo '</td>';
                echo '<td>';
                echo $date_created;
                echo '</td>';
                echo '<td>';
                echo Poppyz_Core::format_price( $total_with_tax );
                echo '</td>';
                echo '</tr>';

            }
            echo '</table>';

            echo '<h3>' . __( 'Total Earnings: ' ) . Poppyz_Core::format_price( $total ) . '</h3>';
        }*/
        die();
    }

    public static function get_top_selling_tiers( $start_date = null, $end_date = null, $limit = 5 ) {

        global $wpdb;
        $table = $wpdb->prefix . "course_subscriptions";

        $sql_date = '';
        if ( $start_date . $end_date ) {
            $sql_date = ' AND ( subscription_date >= "'. $start_date . '" AND subscription_date <= "' . $end_date . '")';
        }

        $results =  $wpdb->get_results( "
        SELECT id, tier_id, count(*) as frequency
        FROM $table
        WHERE payment_status = 'paid'
        $sql_date
        AND (payment_mode IS NULL OR payment_mode = 'Live')
        GROUP BY tier_id
        ORDER BY count(*) desc
        LIMIT " . $limit . "
        " );

        return $results;
    }
    public static function get_top_buyers( $start_date = null, $end_date = null ) {
        global $wpdb;
        $table = $wpdb->prefix . "course_subscriptions";

        $sql_date = '';
        if ( $start_date . $end_date ) {
            $sql_date = ' AND ( subscription_date >= "'. $start_date . '" AND subscription_date <= "' . $end_date . '")';
        }

        $results =  $wpdb->get_results( "
        SELECT id, user_id, count(*) as frequency
        FROM $table
        WHERE payment_status = 'paid'
        $sql_date
        AND (payment_mode IS NULL OR payment_mode = 'Live')
        GROUP BY user_id
        ORDER BY count(*) desc
        LIMIT 5
        " );

        return $results;
    }


	public static function get_plan_automatic_buyers() {
		global $wpdb;
		$table = $wpdb->prefix . "course_subscriptions";
		$sql_date = "";
		if ( isset($_GET['select-tier']) && $_GET['select-tier'] > 1 ) {
			$sql_date .= ' AND tier_id = "'.$_GET['select-tier'].'"';
		} else if(isset($_GET['select-course']) && $_GET['select-course'] > 1) {
			$sql_date .= ' AND course_id = "'.$_GET['select-course'].'"';
        }
		$results =  $wpdb->get_results( "
        SELECT *
        FROM $table
        WHERE payment_method = 'plan'
        AND status = 'on'
        $sql_date
        " );

		return $results;
	}

	public static function get_plan_membership_automatic_buyers($payment_method = null) {
		global $wpdb;
		$table = $wpdb->prefix . "course_subscriptions";
		$sql_date = "";
		if ( isset($_GET['select-tier']) && $_GET['select-tier'] > 1 ) {
			$sql_date .= ' AND tier_id = "'.$_GET['select-tier'].'"';
		} else if(isset($_GET['select-course']) && $_GET['select-course'] > 1) {
			$sql_date .= ' AND course_id = "'.$_GET['select-course'].'"';
		}

        $sql_payment_method = "";
        if ($payment_method == "plan") {
			$sql_payment_method .= " AND payment_method = 'plan'";
        } else if ($payment_method == "membership") {
			$sql_payment_method .= " AND payment_method = 'membership'";
        } else {
			$sql_payment_method .= " AND (payment_method = 'plan' OR payment_method = 'membership' OR payment_method = 'standard')";
        }
		$results =  $wpdb->get_results( "
        SELECT *
        FROM $table
        WHERE amount != 0
        AND (payment_mode != 'Test' OR payment_mode IS NULL)
        AND payment_status  NOT IN ('admin', 'Open', 'Pending')
        $sql_date
        $sql_payment_method
        
        " );

		return $results;
	}

	public static function get_subscriptions_payments_details($sub_id, $start_date, $end_date, $all = false) {
		global $wpdb;
		$table = $wpdb->prefix . "course_subscriptions_payments";
		$results =  $wpdb->get_results( "
        SELECT *
        FROM $table
        WHERE status = 'paid' 
        AND subscription_id = $sub_id" );

        $sub_payments_data = [];
        $count = 1;
        foreach ( $results as $result) {
			$date = strtotime($result->created);
            if ($all == false && ($date >= $start_date && $date <= $end_date)) {
				$sub_payments_data[$count]['invoice_id'] = $result->invoice_id;
				$sub_payments_data[$count]['date_created'] = $result->created;
            } else if ($all == true) {
				$sub_payments_data[$count]['invoice_id'] = $result->invoice_id;
				$sub_payments_data[$count]['date_created'] = $result->created;
            }
			$count++;
        }
		return $sub_payments_data;
	}

    public static function getFirstInvoice($sub_id) {
		global $wpdb;
		$table = $wpdb->prefix . "course_subscriptions_payments";

		$result =  $wpdb->get_results( "
        SELECT *
        FROM $table
        WHERE subscription_id = $sub_id
        ORDER BY id ASC
        LIMIT 1");
        if ( !empty($result) ) {
			return $result[0];
        }

    }

    public static function filter_links( $filter, $selected = '', $class="" ) { ?>
        <?php
            global $ppy_lang;

        ?>
        <select class="short-2 filter-links <?php echo $class;?>" name="<?php echo $filter ?>" id="<?php echo $filter ?>"  >
            <option value="" <?php selected($selected, ''); ?>><?php echo __( 'View all' ,'poppyz'); ?></option>
            <option value="last_year" <?php selected($selected, 'last_year'); ?>><?php echo __( 'Last Year' ,'poppyz'); ?></option>
            <option value="this_year" <?php selected($selected, 'this_year'); ?>><?php echo __( 'This Year' ,'poppyz'); ?></option>
            <option value="last_month" <?php selected($selected, 'last_month'); ?>><?php echo __( 'Last Month' ,'poppyz'); ?></option>
        </select>
    <?php
    }

	public static function get_mollie_subscription($sub) {
		$payment = new Poppyz_Payment();
		$payment->_initialize_mollie();
		$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
		if ( empty( $mo_key ) )  {
			return false;
		}

        $customer_id = $sub->mollie_customer_id;
		$subscription_id = $sub->mollie_subscription_id;
        $customer = $payment->mollie->customers->get($customer_id);
		$subscription = $customer->getSubscription($subscription_id);

        return $subscription;
	}

    public static function get_overdue_payments($start_date, $end_date, $all = false) {
		$start_date = (strtotime($start_date) !== false && !is_int($start_date)) ? $start_date : date("Y-m-d",$start_date);
		$end_date = (strtotime($end_date) !== false && !is_int($end_date)) ? $end_date : date("Y-m-d",$end_date);
        if ( $all == false ) {
			$sql_range = "AND (s.subscription_date between '$start_date' AND '$end_date')";
		} else {
			$sql_range = "";
        }
		$total_remaining_payments = 0;
		global $wpdb;
		$meta_key = PPY_PREFIX . 'number_of_payments';
		$meta_key2 = PPY_PREFIX . 'payment_frequency';
		$meta_key3 = PPY_PREFIX . 'payment_timeframe';
        $sub_table = $wpdb->prefix . 'course_subscriptions';
		$output = "";


		$end_date = (empty($end_date)) ? date("Y-m-d H:i:s") : $end_date;
		$sql_date = "";
		if ( isset($_GET['select-tier']) && $_GET['select-tier'] > 1 ) {
			$sql_date .= ' AND s.tier_id = "'.$_GET['select-tier'].'"';
		} else if(isset($_GET['select-course']) && $_GET['select-course'] > 1) {
			$sql_date .= ' AND s.course_id = "'.$_GET['select-course'].'"';
		}
		$sql = "SELECT s.*, pm1.meta_value as tier_number_of_payments, pm2.meta_value as payment_frequency, pm3.meta_value as payment_timeframe
                FROM $sub_table s
                LEFT JOIN $wpdb->postmeta pm1
                ON (
                s.tier_id = pm1.post_id
                AND pm1.meta_key = '$meta_key'
                )
                 LEFT JOIN $wpdb->postmeta pm2
                ON (
                s.tier_id = pm2.post_id
                AND pm2.meta_key = '$meta_key2'
                )
                LEFT JOIN $wpdb->postmeta pm3
                ON (
                s.tier_id = pm3.post_id
                AND pm3.meta_key = '$meta_key3'
                )
                WHERE s.payment_method = 'plan'
                /*AND s.status ='on' */
                AND s.payment_status <> 'admin' 
                AND s.amount > 0
                AND s.payments_made < pm1.meta_value
                $sql_range
                AND (s.payment_mode != 'Test' OR s.payment_mode IS NULL)
                AND s.payment_status != 'Open'
                $sql_date";


		$subs = $wpdb->get_results( $sql );
        $overdue_array = [];


		foreach ( $subs as $sub ) {
			$subscription_date = $sub->subscription_date;
			$payment_timeframe = !empty( $sub->payment_timeframe ) ?  $sub->payment_timeframe : 'months';

			$payments_made = $sub->payments_made;
			$user_id = $sub->user_id;
			$times = $payments_made * $sub->payment_frequency;

			$due_date = strtotime( $subscription_date . ' + ' . $times . " $payment_timeframe" );
			$due_date_string =  date( get_option( 'date_format' ), $due_date );
			$data['due_date'] = $due_date_string;

			//add a 7 day grace period for processing before cancelling the subscription, unless the membership was cancelled in which case delete it promptly
			$delay = 604800;

			$ppy_payment = new Poppyz_Payment();
			//check if it is Test or live mode.
			if(!Poppyz_Core::column_exists( 'payment_mode' )) {
				$ppy_payment->_initialize_mollie();
			} else {
				$payment_mode = (!empty($sub->payment_mode)) ? $sub->payment_mode : "";
				$ppy_payment->_initialize_mollie( $payment_mode );
			}

			if ( time() > (  $due_date  + $delay ) &&  $sub->payment_status != 'cancelled') {
			//if (  get_user_meta( $user_id, PPY_PREFIX . $sub->id . '_overdue_notice', true ) == null && $sub->payment_status != 'cancelled') {
                $details = new stdClass();
				$details->id = $sub->id;
				$details->due_date = $due_date;
				$details->user_id = $sub->user_id;
				$details->course_id = $sub->course_id;
				$details->tier_id = $sub->tier_id;
				$details->amount = $sub->amount;
				$details->mollie_subscription_id = $sub->mollie_subscription_id;
				$details->subscription_date = $sub->subscription_date;
				$details->version = $sub->version;
				$details->mollie_customer_id = $sub->mollie_customer_id;
				$details->payments_made  = $sub->payments_made ;
				$details->payment_method  = $sub->payment_method ;
				$details->payment_type  = $sub->payment_type ;
				$overdue_array[] = $details;
			}
		}

        return $overdue_array;
	}
	/**
	 * @param $sub
	 * @return array|false
	 * Statistics remaining payments details.
	 */
	public static function statistics_remaining_payments_details($sub) {
		//user info
		$user_data = get_userdata($sub->user_id);
		if ($user_data) {
			$first_name = get_user_meta($sub->user_id, 'first_name', true);
			$last_name = get_user_meta($sub->user_id, 'last_name', true);
			$fullname = $first_name . ' ' . $last_name ;
		} else {
			$fullname = "<i>" . __('User Deleted', 'poppyz') . "</i>";
		}
		if ($sub->payment_method == "plan") {
			$times = (int)get_post_meta($sub->tier_id, PPY_PREFIX . 'number_of_payments', true);
			$interval = get_post_meta($sub->tier_id, PPY_PREFIX . 'payment_frequency', true);
			$timeframe = get_post_meta($sub->tier_id, PPY_PREFIX . 'payment_timeframe', true);
		} else {
			$times = 0;
			$interval = get_post_meta($sub->tier_id, PPY_PREFIX . 'membership_interval', true);
			$timeframe = get_post_meta($sub->tier_id, PPY_PREFIX . 'membership_timeframe', true);
		}


		if ( empty($interval) || empty($timeframe) ) {
			return false;
		}
		$payments_made = $sub->payments_made;
		$subscription_date = $sub->subscription_date;
		$number_of_paid = 0;
		$number_of_remaining = 0;
		$due_date = date_i18n(get_option('date_format'), strtotime($subscription_date . ' + ' . $payments_made * $interval . ' ' . $timeframe));
		$plan_status = (strtotime($due_date) < time()) ? '<br /><strong class="overdue error-message">' . __('Overdue') . '</strong>' : '';
		$amount = $sub->amount;
		$status = ( !empty($sub->status) ) ? $sub->status : "";
		$course = isset(get_post($sub->course_id)->post_title) ? get_post($sub->course_id)->post_title : "";
		$tier = isset(get_post($sub->tier_id)->post_title) ? get_post($sub->tier_id)->post_title : "";

		return [
			"fullname" => $fullname,
			"times" => $times,
			"interval" => $interval,
			"timeframe" => $timeframe,
			"payments_made" => $payments_made,
			"payment_type" => $sub->payment_type,
			"subscription_date" => $subscription_date,
			"number_of_paid" => $number_of_paid,
			"number_of_remaining" => $number_of_remaining,
			"due_date" => $due_date,
			"plan_status" => $plan_status,
			"amount" => $amount,
			"status" => $status,
			"course" => $course,
			"tier" => $tier
		];
	}

	/**
	 * @param $sub
	 * @return array|false
     * Statistics remaining payments details.
	 */
    public static function statistics_remaining_payments_details_v2($sub) {
		//user info
		$user_data = get_userdata($sub->user_id);
		if ($user_data) {
			$first_name = get_user_meta($sub->user_id, 'first_name', true);
			$last_name = get_user_meta($sub->user_id, 'last_name', true);
			$fullname = $first_name . ' ' . $last_name ;
		} else {
			$fullname = "<i>" . __('User Deleted', 'poppyz') . "</i>";
		}
        if ($sub->payment_method == "plan") {
			$times = (int)get_post_meta($sub->tier_id, PPY_PREFIX . 'number_of_payments', true);
			$interval = get_post_meta($sub->tier_id, PPY_PREFIX . 'payment_frequency', true);
			$timeframe = get_post_meta($sub->tier_id, PPY_PREFIX . 'payment_timeframe', true);
		} else {
			$times = 0;
			$interval = get_post_meta($sub->tier_id, PPY_PREFIX . 'membership_interval', true);
			$timeframe = get_post_meta($sub->tier_id, PPY_PREFIX . 'membership_timeframe', true);
		}


//        if ( empty($interval) || empty($timeframe) ) {
//            return false;
//		}
		$payments_made = $sub->payments_made;
		$subscription_date = $sub->subscription_date;
		$number_of_paid = 0;
		$number_of_remaining = 0;
        if ($sub->payment_method == "plan" || $sub->payment_method == "membership" ) {
            //error_log($sub->payment_method ." => ".$sub->course_id." => ".$sub->tier_id);
			$interval = (!empty($interval)) ? $interval : 1;
			$due_date = date_i18n(get_option('date_format'), strtotime($subscription_date . ' + ' . $payments_made * $interval . ' ' . $timeframe));
			$plan_status = (strtotime($due_date) < time()) ? '<br /><strong class="overdue error-message">' . __('Overdue') . '</strong>' : '';
		} else {
			$due_date = false;
			$plan_status = false;
		}


        $amount = $sub->amount;
		$status = ( !empty($sub->status) ) ? $sub->status : "";
		$course = isset(get_post($sub->course_id)->post_title) ? get_post($sub->course_id)->post_title : "";
		$tier = isset(get_post($sub->tier_id)->post_title) ? get_post($sub->tier_id)->post_title : "";

        return [
           "fullname" => $fullname,
           "times" => $times,
           "interval" => $interval,
           "timeframe" => $timeframe,
           "payments_made" => $payments_made,
			"payment_type" => $sub->payment_type,
           "subscription_date" => $subscription_date,
           "number_of_paid" => $number_of_paid,
           "number_of_remaining" => $number_of_remaining,
           "due_date" => $due_date,
           "plan_status" => $plan_status,
           "amount" => $amount,
           "status" => $status,
            "course" => $course,
            "tier" => $tier
        ];
	}

    public static function remaining_payments_table_row($data) {
		$output = "<tr id='".$data['primary_row']."' class='rp-row-".$data['parent_id']." ".$data['sub_row']." rp-row-close'>";
		$output .= "<td>" . $data['due_date_string'] . "</td>";
		$output .= "<td>" . $data['payment_details']['fullname'] . "</td>";
		$output .= "<td>" . $data['payment_details']['course'] . "</td>";
		$output .= "<td>" . $data['payment_details']['tier'] . "</td>";
		$output .= "<td>" . $data['amount'] . "</td>";
		$output .= "<td>" . $data['payment_details']['interval']." ". _n( rtrim($data['payment_details']['timeframe'], "s"), $data['payment_details']['timeframe'], $data['payment_details']['interval'] ). "</td>";
		$output .= "<td><span class='statistics_payment_method'>" .$data['sub']->payment_method. "</span></td>";
		$output .= "<td><div class='arrow-wrapper'>".$data['arrow_down']."</div></td>";
		$output .= "</tr>";

        return $output;
	}

	/**
	 * @param $start_date
	 * @param $end_date
	 * @param $payment_method
	 * @return array
     * Get the data of the remaining payments
	 */
    public static function get_remaining_payments( $start_date, $end_date, $payment_method = null, $all = false  ) {
		$start_date = (!empty(DateTime::createFromFormat('Y-m-d', $start_date))) ? strtotime($start_date) : $start_date;
		$end_date = (!empty(DateTime::createFromFormat('Y-m-d', $start_date))) ? strtotime($end_date) : $end_date;
		$ppy_payment = new Poppyz_Payment();
		$subs = Poppyz_Statistics::get_plan_membership_automatic_buyers($payment_method);
		$student = [];
        $output = "";
        $total_remaining_payments = 0;
        $count = 0;
        $membership = 0;
        $plan = 0;
        $month = "";
        $chart_data = array();
		$chart_data_label = array("Date", "Membership", "Plan" );
		$data_month = "";

        foreach ($subs as $sub) {
            $student[$sub->id]['tier_id'] = $sub->tier_id;
            $student[$sub->id]['user_id'] = $sub->user_id;
            $amount = 0;
            $payment_details = Poppyz_Statistics::statistics_remaining_payments_details($sub);
            if (empty($payment_details)) {continue;}
            if ( $sub->payment_method == "plan" ) {
                $times = $payment_details['times'];
            } elseif( $sub->payment_method == "membership" && $payment_details['timeframe'] == "months" ) {
                $times = 12;
            } elseif( $sub->payment_method == "membership" && $payment_details['timeframe'] == "days" ) {
                continue;
            }

            if ($payment_details != false) {
                $count_payment = 0;
                $parent_id = 0 ;
                for ($y = 0; $y < $times; $y++) {
                    $frequency = $payment_details['interval'] * $y;
                    $due_date = strtotime($payment_details['subscription_date'] . ' + ' . $frequency . ' ' . $payment_details['timeframe']);
                    $due_date_string = date_i18n(get_option('date_format'), $due_date);

                    $initial_interval = get_post_meta($sub->tier_id, PPY_PREFIX . 'initial_payment_frequency', true);
                    if ($sub->payment_method == "plan" && $y > 0 && $initial_interval) {
                        $initial_payment_timeframe = get_post_meta($sub->tier_id, PPY_PREFIX . 'initial_payment_timeframe', true);
                        $due_date = strtotime($sub->subscription_date . ' + ' . $initial_interval . ' ' . $initial_payment_timeframe);
                        $due_date_string = date_i18n(get_option('date_format'), $due_date);
                        if ($y > 1) {
                            $due_date = strtotime(' + ' . ($frequency - 1) . ' ' . $initial_payment_timeframe, $due_date);
                            $due_date_string = date_i18n(get_option('date_format'), $due_date);
                        }
                    }

                    $payment_data = null;
                    if ($sub->version == 2) {
                        $payment_data = $ppy_payment->get_payment_by_number($sub->id, $y + 1);
                        $payment_needed = $payment_data ? $payment_data->status != 'paid' && $payment_data->status != 'chargeback-paid' : true;
                        $x = true;
                    } else {
                        $payment_needed = $y == $payment_details['payments_made'];
                        $x = $y <= $payment_details['payments_made'] - 1;
                    }

                    if ($x && $payment_needed) {
                        $due_date_u = $due_date;
                        if (((isset($start_date) && $start_date <= $due_date_u) && (isset($end_date) && $end_date >= $due_date_u) ||
                                (isset($start_date) && $start_date <= $due_date_u) && (empty($end_date)) ||
                                (empty($start_date) && isset($end_date) && $end_date <= $due_date_u) ||
                                (empty($start_date) && empty($end_date))) && $payment_details['amount'] > 0 && $payment_details['status'] != "canceled" && $sub->status == "on" ) {

                            if ( $count_payment == 0 ) {
								$amount = Poppyz_Statistics::discount_computation( $sub->id, $sub->tier_id, $payment_details['amount'], $sub->user_id, "remaining_payments" );
							}
                            $primary_row = ($count_payment == 0) ? $sub->id : 0;
                            $parent_id = ($count_payment == 0) ? $sub->id : $parent_id;
                            $sub_row = ($count_payment != 0) ? "rp-sub-row-".$sub->id : "";
                            $sub_row .= ($count_payment != 0) ? " rp-sub-row" : " rp-primary-row";
                            $arrow_down = ($count_payment == 0) ? " <i class='arrow_down down'></i>" : "";
                            $table_row_data = array(
                                'sub'           => $sub,
								'primary_row'   => $primary_row,
                                'parent_id'     => $parent_id,
                                'sub_row'       => $sub_row,
                                'due_date_string' => $due_date_string,
                                'payment_details' => $payment_details,
                                'amount'        => Poppyz_Core::format_price($amount),
                                'arrow_down'    => $arrow_down,

                            );
                            $output .= Poppyz_Statistics::remaining_payments_table_row($table_row_data);

                            $total_remaining_payments = $total_remaining_payments + $amount;
                            $count_payment++;

                            if (empty($chart_data[date('Y-m',$due_date_u)][0])) {
							    if ( $sub->payment_method == "membership" ) {
									$membership = $amount;
                                    $plan = 0;
                                } else if ( $sub->payment_method == "plan" ) {
                                    $plan = $amount;
                                    $membership= 0;
                                }
								$chart_data[date('Y-m',$due_date_u)] = array(date('Y-m',$due_date_u), $membership, $plan );
							} else {
                                $data = $chart_data[date('Y-m',$due_date_u)];
                                if ( $sub->payment_method == "membership" ) {
                                    $membership = $data[1]+$amount;
									$plan = $data[2];
                                } else if ( $sub->payment_method == "plan" ) {
                                    $plan = $data[2]+$amount;
									$membership= $data[1];
                                }
                                $chart_data[date('Y-m',$due_date_u)][0] = date('Y-m',$due_date_u);
								$chart_data[date('Y-m',$due_date_u)][1] = $membership;
								$chart_data[date('Y-m',$due_date_u)][2] = $plan;
							}
                        }
                    }

                }
            }
            // Flush the output buffer to send data to the browser
//            flush();
//            if (ob_get_level()) {
//                ob_flush();
//            }
        }
		ksort($chart_data);
        //remove the key from the array
        $filtered_chart_data = array();
		$filtered_chart_data[0] = $chart_data_label;
        foreach ($chart_data as $value) {
			$filtered_chart_data[] = $value;
		}

        return array(
                "table_data"    => $output,
			    "total_amount"  => Poppyz_Core::format_price( $total_remaining_payments ),
                "chart_data"    => $filtered_chart_data
            );
    }

	/**
	 * @param $sub_id
	 * @param $tier_id
	 * @param $amount
	 * @return mixed
     * Compute discount.
	 */
    public static function discount_computation( $sub_id, $tier_id, $amount, $user_id, $type=false ) {
		$invoice = Poppyz_Statistics::getFirstInvoice($sub_id);
        if ( !empty($invoice) ) {
			$invoice_id = $invoice->invoice_id;
			$amount = Poppyz_Statistics::add_coupon_from_amount( $invoice_id, $amount );
			$amount = Poppyz_Statistics::discount_validation( $invoice_id, $tier_id, $amount, $type=false );
		}

		$amount = Poppyz_Core::price_with_tax( $amount, $tier_id, $user_id );

        return $amount;
	}

	/**
	 * @param $invoice_id
	 * @param $amount
	 * @return mixed|string
     * Remove the coupon discount it the amount.
	 */
    public static function add_coupon_from_amount( $invoice_id, $amount ) {
        //get coupon from invoice meta tag
        $coupon_code = get_post_meta( $invoice_id, PPY_PREFIX .'invoice_coupon_code', true);

        if ( empty($coupon_code) ) return $amount;

        $coupon = new Poppyz_Coupon;

        $coupon_data = $coupon->get_coupon_by_code( $coupon_code );
        if (!$coupon_data) return $amount;

        $coupon_amount = $coupon->get_coupon_amount($coupon_data->ID);
        if (!$coupon_amount) return $amount;

        $amount = $amount-$coupon_amount;

        return $amount;
	}

	/**
	 * @param $invoice_id
	 * @param $tier_id
	 * @param $amount
	 * @param $type
	 * @return mixed
     * Check if there is a discount and check if it is implemented to all of the payments.
	 */
	public static function discount_validation( $invoice_id, $tier_id, $amount, $type=false ) {
		$discount = get_post_meta( $invoice_id, PPY_PREFIX .'invoice_tier_discount', true);
		$implementation = get_post_meta( $tier_id, PPY_PREFIX . 'discount_implementation_option', true );
		if( ($type == "remaining_payments" || $type == "recurring" || $type == false ) && ($implementation == "all" || empty($implementation))  && !empty($discount) ) {
			$amount = $amount - $discount;
		} else if ($type=="recurring" && $implementation=="first_payment") {
			return $amount;
		}
		return $amount;
	}

	/**
	 * @param $sub
	 * @param $payment_details
	 * @return mixed
	 * Computation of the first payment.
	 */
	public static function first_payment_computation($sub, $payment_details) {
		//check if there is an initial price
		$initial_price = get_post_meta( $sub->tier_id, PPY_PREFIX . 'tier_initial_price',true);
		if ( !empty($initial_price) ) {
			$first_payment = Poppyz_Statistics::discount_computation( $sub->id, $sub->tier_id, $initial_price, $sub->user_id );
		} else {
			$first_payment = Poppyz_Statistics::discount_computation( $sub->id, $sub->tier_id, $payment_details['amount'], $sub->user_id );
		}
        if ( $first_payment < 0 ) {
            return 0;
		} else {
			return $first_payment;
        }
	}

	/**
	 * @param $start_date
	 * @param $end_date
	 * @param $data_by_months
	 * @param $data_by_days
	 * @return array
     * Get eproduct data and process.
	 */
	public static function get_eproduct_data($start_date, $end_date, $data_by_months, $data_by_days) {
        $turnover = [];
		$turnover['turnover'] = [];
		$turnover['total_amount'] = 0;
        $total_amount = 0;
		$query = new WP_Query(array(
			'post_type' => PPYV_PRODUCT_PT,
			'posts_per_page' => 999999,
			'post_status' => 'publish',
		));
		while ($query->have_posts()) {
			$query->the_post();
			$post_id = get_the_ID();
            $data = [
                "post_id" => $post_id,
                "turnover" => $turnover['turnover'],
                "total_amount" => $turnover['total_amount'],
                "start_date" => $start_date,
                "end_date" => $end_date,
                "data_by_months" => $data_by_months,
                "data_by_days" => $data_by_days
            ];
            $turnover = Poppyz_Statistics::get_eproduct_postmeta_data($data);
		}
		return $turnover;
	}

	public static function get_eproduct_postmeta_data($data) {
		$eproduct_id    = $data["post_id"];
		$turnover       = $data["turnover"];
		$total_amount   = $data["total_amount"];
		$start_date     = $data["start_date"];
		$end_date       = $data["end_date"];
		$data_by_months = $data["data_by_months"];
		$data_by_days   = $data["data_by_days"];

		global $wpdb;
		$sub_table = $wpdb->prefix . 'postmeta';
        $sql = "SELECT post_id  FROM $sub_table WHERE meta_value = $eproduct_id AND meta_key='ppy_invoice_product_id'";
		$post_ids = $wpdb->get_results( $sql );

        foreach($post_ids as $id) {
			$id = $id->post_id;
			$amount = get_post_meta($id, PPY_PREFIX . 'invoice_amount', true);
			$amount = Poppyz_Core::price_with_tax( $amount, $eproduct_id );
            $date_created = get_post_meta($id, PPY_PREFIX . 'invoice_date_created', true);
            $date_key = strtotime($date_created);
            if ((!empty($start_date) && $date_key < $start_date) || (!empty($end_date) && $date_key > $end_date) ) {
                continue;
			}
            if (!empty(get_the_title( $id ))) {
				$data_by_months = Poppyz_Statistics::save_date_and_total_to_array($data_by_months, "month", $date_created, $amount);
				$data_by_days = Poppyz_Statistics::save_date_and_total_to_array($data_by_days, "day", $date_created, $amount);
				$turnover[] =  [
					"invoice_id" => get_the_title( $id ),
					"subscription_id" => 'n/a',
					"tier_title" => get_post_meta($id, PPY_PREFIX . 'invoice_product', true),
					"payment_date" => $date_created,
					"amount" => $amount,
                    "u" => $date_key
				];
				$total_amount = $total_amount+$amount;
			}

		}
        return [
                "total_amount" => $total_amount,
                "turnover"  => $turnover,
                "data_by_days"      => $data_by_days,
                "data_by_months"    => $data_by_months
        ];
	}


	/**
	 * @param $start_date
	 * @param $end_date
	 * @return array
     * Get the total overdue data.
	 */
	public static function get_total_overdue( $start_date, $end_date, $all=false ) {
		$start_date = (strtotime($start_date) !== false) ? strtotime($start_date) : $start_date;
		$end_date = (strtotime($end_date) !== false) ? strtotime($end_date) : $end_date;
		$total_overdue = 0;
        $overdue_payments = Poppyz_Statistics::get_overdue_payments($start_date, $end_date, $all);
		$ppy_payment = new Poppyz_Payment();
		$total_remaining_payments = 0;
		$output = "";
        foreach ($overdue_payments as $sub) {
            $payment_details = Poppyz_Statistics::statistics_remaining_payments_details($sub);
            $count_payment = 0;
            $parent_id = 0;
            if ($payment_details != false) {
				$amount = 0;
				$total_overdue = $total_overdue + 1;
				for ($y = 0; $y < $payment_details['times']; $y++) {
                    $interval = get_post_meta($sub->tier_id, PPY_PREFIX . 'payment_frequency', true);
                    $frequency = $interval * $y;
                    $due_date = strtotime($payment_details['subscription_date'] . ' + ' . $frequency . ' ' . $payment_details['timeframe']);
                    $due_date_string = date_i18n(get_option('date_format'), $due_date);

                    $initial_interval = get_post_meta($sub->tier_id, PPY_PREFIX . 'initial_payment_frequency', true);
                    if ($y > 0 && $initial_interval) {
                        $initial_payment_timeframe = get_post_meta($sub->tier_id, PPY_PREFIX . 'initial_payment_timeframe', true);
                        $due_date = strtotime($sub->subscription_date . ' + ' . $initial_interval . ' ' . $initial_payment_timeframe);
                        $due_date_string = date_i18n(get_option('date_format'), $due_date);
                        if ($y > 1) {
                            $due_date = strtotime(' + ' . ($frequency - 1) . ' ' . $initial_payment_timeframe, $due_date);
                            $due_date_string = date_i18n(get_option('date_format'), $due_date);
                        }
                    }

                    $payment_data = null;
                    if ($sub->version == 2) {
                        $payment_data = $ppy_payment->get_payment_by_number($sub->id, $y + 1);
                        $payment_needed = $payment_data ? $payment_data->status != 'paid' && $payment_data->status != 'chargeback-paid' : true;
                        $x = true;
                    } else {
                        $payment_needed = $y == $payment_details['payments_made'];
                        $x = $y <= $payment_details['payments_made'] - 1;
                    }

                    if ($x && $payment_needed) {
                        $due_date_u = $due_date;
                        if (((isset($start_date) && $start_date <= $due_date_u) && (isset($end_date) && $end_date >= $due_date_u) ||
                                (isset($start_date) && $start_date <= $due_date_u) && (empty($end_date)) ||
                                (empty($start_date) && isset($end_date) && $end_date <= $due_date_u) ||
                                (empty($start_date) && empty($end_date))) && $payment_details['amount'] > 0) {

							if ( $count_payment == 0 ) {
								$amount = Poppyz_Statistics::discount_computation( $sub->id, $sub->tier_id, $payment_details['amount'], $sub->user_id );
							}

                            $total_remaining_payments = $total_remaining_payments + $amount;
							$primary_row = ($count_payment == 0) ? $sub->id : 0;
                            $parent_id = ($count_payment == 0) ? $sub->id : $parent_id;
                            $sub_row = ($count_payment != 0) ? "rp-sub-row-" . $sub->id : "";
                            $sub_row .= ($count_payment != 0) ? " rp-sub-row" : " rp-primary-row";
                            $arrow_down = ($count_payment == 0) ? " <i class='arrow_down down'></i>" : "";
							$table_row_data = array(
								'sub'           => $sub,
								'primary_row'   => $primary_row,
								'parent_id'     => $parent_id,
								'sub_row'       => $sub_row,
								'due_date_string' => $due_date_string,
								'payment_details' => $payment_details,
								'amount'        => Poppyz_Core::format_price($amount),
								'arrow_down'    => $arrow_down,
							);
							$output .= Poppyz_Statistics::remaining_payments_table_row($table_row_data);

							$count_payment++;
                        }
                    }
                }
            }
        }
		return array(
            "total_overdue"     => $total_overdue,
			"total_overdue_amount" => $total_remaining_payments,
            "output"            => $output
		);
	}

	/**
	 * @param $data
	 * @param $type
	 * @param $date_created
	 * @param $total
	 * @return mixed
	 * @throws Exception
     * Save date and total to an array.
	 */
    public static function save_date_and_total_to_array($data, $type, $date_created, $total) {
        if ($type == "month") {
			$array_key =  date_i18n( 'F', strtotime( $date_created )); //month is the array key
		} else if ($type == "day") {
            // if day, the array key is date format
			$date = new DateTime($date_created);
			$array_key = $date->format( 'j' );;
		}
		if (isset($data[$array_key])) {
			$data[$array_key] = $data[$array_key]+$total;
		} else {
			$data[$array_key] = $total;
		}
        return $data;
    }

	/**
	 * @param $sub
	 * @param $payment_details
	 * @param $sub_payments_data
	 * @param $turnover
	 * @param $amount
	 * @return array
	 */
    public static function save_turnover_to_array($sub, $payment_details, $sub_payments_data, $turnover, $amount) {
        $date_created = $sub_payments_data['date_created'];
        $date_key = strtotime($date_created);
        $get_invoice_id = get_the_title($sub_payments_data['invoice_id']);
		$turnover[] =  [
                "invoice_id" => $get_invoice_id,
                "subscription_id" => $sub->id,
                "tier_title" => get_the_title($sub->tier_id),
                "payment_date" => $sub_payments_data['date_created'],
                "amount" => $amount,
                "u" => $date_key
        ];
        return $turnover;
	}

	/**
	 * @param $sub
	 * @param $payment_details
	 * @param $start_date
	 * @param $end_date
	 * @return float|int|mixed
     * Get all paid payment.
     * Interval will be based on the invoice id from course_subscriptions_payments table.
	 */
    public static function get_all_paid_payment_details( $data, $type=null ) {
		$sub = $data["subscription"];
		$payment_details = $data["payment_details"];
		$payment_method = $data["payment_method"];
		$start_date = $data["start_date"];
		$end_date = $data["end_date"];
		$all = $data["all"];
		$months = $data["data_by_months"];
		$days = $data["data_by_days"];
		$turnover = $data["turnover"];

		$total = 0;
		$sub_payments_data = Poppyz_Statistics::get_subscriptions_payments_details($sub->id,  $start_date, $end_date, $all);
		$sub_payments_data_key = array_keys($sub_payments_data);
        if ( !empty($sub_payments_data_key[0]) ) {
            if ( $payment_method == 'standard' ) {
                $date_created = $sub_payments_data[$sub_payments_data_key[0]]['date_created'];
				$total =  Poppyz_Statistics::first_payment_computation($sub, $payment_details);
                if ($type == null) {
					$months = Poppyz_Statistics::save_date_and_total_to_array($months, "month", $date_created, $total);
					$days = Poppyz_Statistics::save_date_and_total_to_array($days, "day", $date_created, $total);
				} else if($type == "turnover") {
					$turnover =  Poppyz_Statistics::save_turnover_to_array($sub, $payment_details, $sub_payments_data[$sub_payments_data_key[0]], $turnover, $total);
				}

			} else {
				for ( $count=$sub_payments_data_key[0]; $count<=count($sub_payments_data); $count++ ) {
					if ($count == 1) {
						$amount =  Poppyz_Statistics::first_payment_computation($sub, $payment_details);
					} else {
						$invoice = Poppyz_Statistics::getFirstInvoice($sub->id);
						$invoice_id = $invoice->invoice_id;

						$amount = Poppyz_Statistics::discount_validation( $invoice_id, $sub->tier_id, $payment_details['amount'], "recurring" );
						$amount = Poppyz_Core::price_with_tax( $amount, $sub->tier_id, $sub->user_id );
					}
					if ($type == null) {
						$date_created = $sub_payments_data[$count]['date_created'];
						$months = Poppyz_Statistics::save_date_and_total_to_array($months, "month", $date_created, $amount);
						$days = Poppyz_Statistics::save_date_and_total_to_array($days, "day", $date_created, $amount);
					} else if($type == "turnover") {
						$turnover = Poppyz_Statistics::save_turnover_to_array($sub, $payment_details, $sub_payments_data[$count], $turnover, $amount);
					}
					$total = $total+$amount;
				}
			}
		}
		if ($type == "total_only") {
			$data = array(
				"total" => $total
			);
		} else if($type == "turnover") {
			$data = array(
				"total" => $total,
				"turnover" => $turnover
			);
		} else {
			$data = array(
				"total" => $total,
				"months" => $months,
				"days" => $days
			);
		}
        return $data;
	}

	/**
	 * @param $start_date
	 * @param $end_date
	 * @return array
     * Get the total statistics data.
	 */
	public static function get_total_statistics( $start_date, $end_date, $all=false, $overdue_included=false, $type=null, $is_json=false ) {
		$start_date = (strtotime($start_date) !== false) ? strtotime($start_date) : $start_date;
        $end_date = (strtotime($end_date) !== false) ? strtotime($end_date) : $end_date;
		$ppy_payment = new Poppyz_Payment();
		$subs = Poppyz_Statistics::get_plan_membership_automatic_buyers();
		$finished_payment = 0;
		$total_paid_plan = 0;
		$total_paid_membership = 0;
		$total_paid_standard = 0;
        $turnover = [];
		$eproduct_turnover_data = [];
		$data_by_months = [];
		$data_by_days = [];
        $data_by_months = [
            __("January", "poppyz") => 0,
			__("February", "poppyz") => 0,
			__("March", "poppyz") => 0,
			__("April", "poppyz") => 0,
			__("May", "poppyz") => 0,
			__("June", "poppyz") => 0,
			__("July", "poppyz") => 0,
			__("August", "poppyz") => 0,
			__("September", "poppyz") => 0,
			__("October", "poppyz") => 0,
			__("November", "poppyz") => 0,
			__("December", "poppyz") => 0,
        ];
		$data_by_days = [];
		$date = new DateTime();
		$this_month = $date->format( 'm' );
		$year = $date->format( 'Y' );
		$lm_days = Poppyz_Core::days_in_month( $this_month - 1, $year);
        for ($a=1; $a<=$lm_days; $a++) {
			$data_by_days[$a] = 0;
		}
        foreach ($subs as $sub) {
            $payment_details = Poppyz_Statistics::statistics_remaining_payments_details_v2($sub);
            if (empty($payment_details)) {continue;}

			$data = array(
				"subscription" => $sub,
				"payment_details" => $payment_details,
				"payment_method" => $sub->payment_method,
				"start_date" => $start_date,
				"end_date" => $end_date,
				"all" => $all,
				"data_by_months" => $data_by_months,
				"data_by_days" => $data_by_days,
                "turnover" => $turnover
			);
            if ($payment_details != false && $sub->payment_method == "plan") {
                $paid_last_payment = false;
				$times = $payment_details['times'];
				if ($sub->version == 2) {
					$payment_data = $ppy_payment->get_payment_by_number($sub->id, $times);
					$paid_last_payment = (!empty($payment_data) && $payment_data->status == 'paid') ? true : false ;
					$x = true;
				} else {
					$payment_needed = $times == $payment_details['payments_made'];
					$x = $times <= $payment_details['payments_made'] - 1;
				}
                $sub_date = strtotime($sub->subscription_date);
				if ( $sub->payment_method == "plan" && $paid_last_payment && $x && $start_date <= $sub_date && $end_date >= $sub_date) {
                    //count finished payments
					$finished_payment = $finished_payment +1;
				}
                $paid_plan = Poppyz_Statistics::get_all_paid_payment_details( $data, $type );
                $total_paid_plan = $total_paid_plan+$paid_plan['total'];
                if($type == null) {
					$data_by_months = $paid_plan['months'];
					$data_by_days = $paid_plan['days'];
				} else if ($type == "turnover") {
					$turnover = $paid_plan['turnover'];
				}
            } else if ( $payment_details != false && $sub->payment_method == "membership" ) {
				$paid_membership = Poppyz_Statistics::get_all_paid_payment_details( $data, $type );
                $total_paid_membership = $total_paid_membership+$paid_membership['total'];
				if($type == null) {
					$data_by_months = $paid_membership['months'];
					$data_by_days = $paid_membership['days'];
				} else if ($type == "turnover") {
					$turnover = $paid_membership['turnover'];
				}
			} else if ( $payment_details != false && $sub->payment_method == "standard" ) {
				$paid_standard = Poppyz_Statistics::get_all_paid_payment_details( $data, $type );
				$total_paid_standard = $total_paid_standard+$paid_standard['total'];
                if($type == null) {
                    $data_by_months = $paid_standard['months'];
                    $data_by_days = $paid_standard['days'];
                } else if ($type == "turnover") {
					$turnover = $paid_standard['turnover'];
				}
			}
        }
        //process eproduct data
		if (defined('PPYV_PREFIX')) {
			$eproduct_turnover_data = Poppyz_Statistics::get_eproduct_data($start_date, $end_date, $data_by_months, $data_by_days);
		}

        //merge data
        if ( !empty($eproduct_turnover_data) ) {
			$turnover = array_merge($turnover,$eproduct_turnover_data['turnover']);
			$total_paid_payments = $total_paid_plan+$total_paid_membership+$total_paid_standard+$eproduct_turnover_data['total_amount'];
		} else {
			$total_paid_payments = $total_paid_plan+$total_paid_membership+$total_paid_standard;
		}
		if ( $type == "total_only" ) {
			return [
				"total_paid_payments" =>  $total_paid_payments
			];
		}
		$key_values = array_column($turnover, 'u');
		array_multisort($key_values, SORT_DESC, $turnover);
		if ( $type == "turnover" ) {
			return [
                "start" => $start_date,
                "end" => $end_date,
				"turnovers" =>  $turnover,
				"total_paid_payments" => $total_paid_payments
			];
		}

        //check if there is a data on eproduct
        if ( !empty($eproduct_turnover_data['turnover']) ) {
			$data_by_months = $eproduct_turnover_data['data_by_months'];
			$data_by_days = $eproduct_turnover_data['data_by_days'];
		}


        if ($overdue_included == true) {
			$total_overdue = Poppyz_Statistics::get_total_overdue( $start_date, $end_date, $all );
			if ( !empty($total_overdue['total_overdue_amount']) ) {
				$overdue_percentage = ($total_overdue['total_overdue_amount']/$total_paid_payments)*100;
			}  else {
				$overdue_percentage = 0;
			}
			//array for pie chart
			$data_array = array();
			$data_array[] = array("Overdue", $total_overdue['total_overdue_amount']);
			$data_array[] = array("Paid", $total_paid_payments);
		}

        $data = array(
            "finished_payment"  => $finished_payment,
            "total_paid_plan"  => $total_paid_plan,
            "total_paid_membership"  => $total_paid_membership,
            "total_paid_payments"  => $total_paid_payments,
            "data_by_month"     => $data_by_months,
            "data_by_day"       => $data_by_days
        );

		if ($overdue_included == true) {
			$data["data"] = $data_array;
			$data["total_overdue"] = $total_overdue['total_overdue'];
			$data["total_overdue_amount"] = $total_overdue['total_overdue_amount'];
			$data["overdue_percentage"] = $overdue_percentage;
            $data['start_date']=$start_date;
            $data['end_date'] = $end_date;
			$data['all'] = $all;
		}
		return $data;
	}



}
