<?php

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}


/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 *
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 *
 * Our theme for this list table is going to be movies.
 */
class Poppyz_Invoice_List_Table extends WP_List_Table {

    /**
     * Number of results to show per page
     *
     * @var string
     * @since 0.8.9
     */
    public $per_page = 30;

    /**
     *
     * Total number of discounts
     * @var string
     * @since 0.8.9
     */
    public $total_count;

    /**
     * Paid number of discounts
     *
     * @var string
     * @since 0.8.9
     */
    public $unpaid_count;

    /**
     * Unpaid number of discounts
     *
     * @var string
     * @since 0.8.9
     */
    public $paid_count;


    public $invoice_object;

    public $ppy_lang;


    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'invoice',     //singular name of the listed records
            'plural'    => 'invoices',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

        $this->invoice_object = new Poppyz_Invoice();
        $this->ppy_lang = new Poppyz_i18n();

    }

    /**
     * Get invoice codes count
     *
     * @access public
     * @since 1.4
     * @return void
     */
    public function get_invoice_number_counts() {
        $invoice_count  = wp_count_posts( PPY_INVOICE_PT );
        $this->unpaid_count   = $invoice_count->paid;
        $this->paid_count = $invoice_count->unpaid;
        $this->total_count    = $invoice_count->paid + $invoice_count->unpaid;
    }

    /** ************************************************************************
     * Recommended. This method is called when the parent class can't find a method
     * specifically build for a given column. Generally, it's recommended to include
     * one method for each column you want to render, keeping your package class
     * neat and organized. For example, if the class needs to process a column
     * named 'title', it would first see if a method named $this->column_title()
     * exists - if it does, that method will be used. If it doesn't, this one will
     * be used. Generally, you should try to use custom column methods as much as
     * possible.
     *
     * Since we have defined a column_title() method later on, this method doesn't
     * need to concern itself with any column with a name of 'title'. Instead, it
     * needs to handle everything else.
     *
     * For more detailed insight into how columns are handled, take a look at
     * WP_List_Table::single_row_columns()
     *
     * @param array $item A singular item (one full row's worth of data)
     * @param array $column_name The name/slug of the column to be processed
     * @return string Text or HTML to be placed inside the column <td>
     **************************************************************************/
    function column_default($item, $column_name){
        switch($column_name){
            case 'product':
            case 'company':
            case 'firstname':
            case 'lastname':
            case 'status':
            case 'date_created':
            case 'amount':
            case 'actions':
                return $item[$column_name];
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }

    /*public function get_views() {
        $base           = admin_url('edit.php');

        $current        = isset( $_GET['status'] ) ? $_GET['status'] : '';
        $total_count    = '&nbsp;<span class="count">(' . $this->total_count    . ')</span>';
        $unpaid_count   = '&nbsp;<span class="count">(' . $this->unpaid_count . ')</span>';
        $paid_count = '&nbsp;<span class="count">(' . $this->paid_count  . ')</span>';

        $views = array(
            'all'		=> sprintf( '<a href="%s"%s>%s</a>', remove_query_arg( 'status', $base ), $current === 'all' || $current == '' ? ' class="current"' : '', __('All' ) . $total_count ),
            'paid'	=> sprintf( '<a href="%s"%s>%s</a>', add_query_arg( 'status', 'paid', $base ), $current === 'paid' ? ' class="current"' : '', __('Paid' ) . $unpaid_count ),
            'unpaid'	=> sprintf( '<a href="%s"%s>%s</a>', add_query_arg( 'status', 'unpaid', $base ), $current === 'unpaid' ? ' class="current"' : '', __('Unpaid') . $paid_count ),
        );

        return $views;
    }*/


    /** ************************************************************************
     * Recommended. This is a custom column method and is responsible for what
     * is rendered in any column with a name/slug of 'title'. Every time the class
     * needs to render a column, it first looks for a method named
     * column_{$column_title} - if it exists, that method is run. If it doesn't
     * exist, column_default() is called instead.
     *
     * This example also illustrates how to implement rollover actions. Actions
     * should be an associative array formatted as 'slug'=>'link html' - and you
     * will need to generate the URLs yourself. You could even ensure the links
     *
     *
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_title($item){

        $invoice     = get_post( $item['ID'] );
        $id = $invoice->ID;
        $row_actions  = array();

        //$row_actions['edit'] = '<a href="' . add_query_arg( array( 'ppy-action' => 'edit-invoice', 'invoice' => $id ) ) . '">' . __( 'Edit' ) . '</a>';

        if( strtolower( $item['status'] ) == 'paid' ) {
            $row_actions['unpay'] = '<a href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'unpay-invoice', 'invoice' => $id ) ), 'ppy-nonce' ) ) . '">' . __( 'Mark as unpaid' ) . '</a>';
        } elseif( strtolower( $item['status'] ) == 'unpaid' || strtolower( $item['status'] ) == 'expired' ) {
            $row_actions['pay'] = '<a href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'pay-invoice', 'invoice' => $id ) ), 'ppy-nonce' ) ) . '">' . __( 'Pay' ) . '</a>';
        }

        $row_actions['generate-pdf'] = '<a target="_blank" href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'generate-pdf', 'invoice' => $id ) ), 'ppy-nonce' ) ) . '">' . __( 'View PDF' ) . '</a>';

	    if ( strtolower( $item['status'] ) !== 'credit' ) {
		    $row_actions['duplicate-credit'] = '<a href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'duplicate-credit', 'invoice' => $id ) ), 'ppy-nonce' ) ) . '">' . __( 'Duplicate to credit invoice' ) . '</a>';
	    }

	    $row_actions['delete'] = '<a onclick="return confirm(\'Are you sure you want to delete this invoice?\')"" href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'delete-invoice', 'invoice' => $id ) ), 'ppy-nonce' ) ) . '">' . __( 'Delete' ) . '</a>';

        return stripslashes( $item['title'] ) . $this->row_actions( $row_actions );
    }



    /** ************************************************************************
     * REQUIRED if displaying checkboxes or using bulk actions! The 'cb' column
     * is given special treatment when columns are processed. It ALWAYS needs to
     * have it's own method.
     *
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item['ID']                //The value of the checkbox should be the record's id
        );
    }


    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     *
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     *
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
            'title'     => __( 'Invoice Number' ),
            'product'    => __( 'Product' ),
            //'company'    => __( 'Company' ),
            //'firstname'    => __( 'First Name' ),
            //'lastname'    => __( 'Last Name' ),
            //'status'  => __( 'Status' ),
            'date_created'  => __( 'Date Created' ),
            'amount'  => __( 'Amount' ),
            'actions'  => __( 'Actions' )
        );
        return $columns;
    }


    /** ************************************************************************
     * Optional. If you want one or more columns to be sortable (ASC/DESC toggle),
     * you will need to register it here. This should return an array where the
     * key is the column that needs to be sortable, and the value is db column to
     * sort by. Often, the key and value will be the same, but this is not always
     * the case (as the value is a column name from the database, not the list table).
     *
     * This method merely defines which columns should be sortable and makes them
     * clickable - it does not handle the actual sorting. You still need to detect
     * the ORDERBY and ORDER querystring variables within prepare_items() and sort
     * your data accordingly (usually by modifying your query).
     *
     * @return array An associative array containing all the columns that should be sortable: 'slugs'=>array('data_values',bool)
     **************************************************************************/
    function get_sortable_columns() {
        $sortable_columns = array(
            'title'     => array('title',false ),     //true means it's already sorted
            'product'    => array( 'product', false ),
            'company'    => array( 'company', false ),
            //'firstname'    => array( 'firstname', false ),
            //'lastname'    => array( 'lastname', false ),
            //'status'  => array( 'status', false ),
            'date_created'  => array( 'date_created', false ),
            'amount'  => array( 'amount', false )
        );
        return $sortable_columns;
    }


    /** ************************************************************************
     * Optional. If you need to include bulk actions in your list table, this is
     * the place to define them. Bulk actions are an associative array in the format
     * 'slug'=>'Visible Title'
     *
     * If this method returns an empty value, no bulk action will be rendered. If
     * you specify any bulk actions, the bulk actions box will be rendered with
     * the table automatically on display().
     *
     * Also note that list tables are not automatically wrapped in <form> elements,
     * so you will need to create those manually in order for bulk actions to function.
     *
     * @return array An associative array containing all the bulk actions: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_bulk_actions() {
        $actions = array(
            'pay'    => __( 'Mark as paid' ),
            'unpay'    => __( 'Mark as unpaid' ),
            /*'download'    => __( 'Download' ),
            'export-csv'    => __( 'Export CSV' ),
            'export-xml'    => __( 'Export XML' ),*/
            'delete'    => __( 'Delete' ),
        );
        return $actions;
    }


    /** ************************************************************************
     * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
     * For this example package, we will handle it in the class to keep things
     * clean and organized.
     *
     * @see $this->prepare_items()
     **************************************************************************/
    public function process_bulk_action() {

        // security check!
        /*if ( isset( $_POST['_wpnonce'] ) && ! empty( $_POST['_wpnonce'] ) ) {

            $nonce  = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );
            $action = 'bulk-' . $this->_args['plural'];

            if ( ! wp_verify_nonce( $nonce, $action ) )
                wp_die( 'Nope! Security check failed!' );
        }*/

        $ids = isset( $_POST['invoice'] ) ? $_POST['invoice'] : false;
        if ( ! is_array( $ids ) )
            $ids = array( $ids );


        foreach ( $ids as $id ) {
            if ( 'delete' === $this->current_action() ) {
                $this->invoice_object->remove_invoice( $id );
            }
            if ( 'pay' === $this->current_action() ) {
                $this->invoice_object->update_invoice_status( $id, 'paid' );
            }
            if ( 'unpay' === $this->current_action() ) {
                $this->invoice_object->update_invoice_status( $id, 'unpaid' );
            }
	        if ( 'duplicate-credit' === $this->current_action() ) {
		        $this->invoice_object->duplicate_to_credit( $id );
	        }
        }

        if ( 'download' === $this->current_action() ) {
			$ids = $this->remove_invoice_test_ids( $ids );
            $this->invoice_object->download( $ids );
        }
        if ( 'export-xml' === $this->current_action() ) {
			$ids = $this->remove_invoice_test_ids( $ids );
            $args['post__in'] = $ids;
            $this->invoice_object->export_xml( $args );
        }
        if ( 'export-csv' === $this->current_action() ) {
			$ids = $this->remove_invoice_test_ids( $ids );
            $args['post__in'] = $ids;
            $this->invoice_object->export_csv( $args );
        }

    }

    public function remove_invoice_test_ids( $ids = false ) {
        if ($ids) {
            $id_array = array();
            foreach( $ids as $id ) {
				$invoice_mode = get_post_meta($id, PPY_PREFIX .'invoice_mode', true);
				if ($invoice_mode == "Live" || $invoice_mode == null) {
					$id_array[] = $id;
				}
            }
            return $ids;
        }
    }

    public function get_data( $count = false) {
        $invoice_data = array();

        $per_page = $this->per_page;

        if ( isset( $_POST['download'] ) || isset( $_POST['export-xml'] ) || isset( $_POST['export-csv'] ) ) {
            $per_page = -1;
        }

        $orderby 		= isset( $_REQUEST['orderby'] )  ? $_REQUEST['orderby']                  : 'ID';
        $order 			= isset( $_REQUEST['order'] )    ? $_REQUEST['order']                    : 'DESC';
        $status 		= isset( $_REQUEST['status'] )   ? $_REQUEST['status']                   : 'any';
        $meta_key		= isset( $_REQUEST['meta_key'] ) ? $_REQUEST['meta_key']                 : null;
        $meta_value		= isset( $_REQUEST['meta_value'] ) ? $_REQUEST['meta_value']                 : null;
        $search         = isset( $_REQUEST['s'] )        ? sanitize_text_field( $_REQUEST['s'] ) : null;
		$invoice_mode	= isset( $_REQUEST['invoice_mode'] ) ? $_REQUEST['invoice_mode']          : null;

        $start_date         = isset( $_REQUEST['start-date'] ) ? sanitize_text_field( $_REQUEST['start-date'] ) : null;
        $end_date         = isset( $_REQUEST['end-date'] )  ? sanitize_text_field( $_REQUEST['end-date'] ) : null;

        $start_invoice_number        = isset( $_REQUEST['start-invoice-number'] )  ? sanitize_text_field( $_REQUEST['start-invoice-number'] ) : null;
	    $end_invoice_number         = isset( $_REQUEST['end-invoice-number'] )  ? sanitize_text_field( $_REQUEST['end-invoice-number'] ) : null;

	    $vat_shifted         = isset( $_REQUEST['vat-shifted'] )  ? sanitize_text_field( $_REQUEST['vat-shifted'] ) : null;


        $args = array(
            'posts_per_page' => $per_page,
            'paged'          => isset( $_REQUEST['paged'] ) ? $_REQUEST['paged'] : 1,
            'order'          => $order,
            'post_status'    => $status,
            's'              => $search
        );

        $meta_query_args = array();

        if ( $meta_key ) {
            $meta_query_args[] =  array (
                'key' => $meta_key,
                'value' => $meta_value,
                'compare' => 'LIKE',
            );
        }
        if ( $invoice_mode == "Live" ) {
			$meta_query_args[] =  array (
                    'relation' => 'OR',
                 array (
                    'key' => PPY_PREFIX .'invoice_mode',
                    'value' => $invoice_mode,
                    'compare' => 'LIKE',
			    ),
				array (
					'key' => PPY_PREFIX .'invoice_mode',
					'compare' => 'NOT EXISTS',
				)
            );
        }
		if ( $invoice_mode == "Test" ) {
			$meta_query_args[] =  array (
				'key' => PPY_PREFIX .'invoice_mode',
				'value' => $invoice_mode,
				'compare' => 'LIKE',
			);
		} else if( $invoice_mode != "All" ) {
			$meta_query_args[] =  array (
				'relation' => 'OR',
				array (
					'key' => PPY_PREFIX .'invoice_mode',
					'value' => "Live",
					'compare' => 'LIKE',
				),
				array (
					'key' => PPY_PREFIX .'invoice_mode',
					'compare' => 'NOT EXISTS',
				)
			);
        }

        if ( $vat_shifted ) {
            $meta_query_args[] =  array (
                'key' => PPY_PREFIX .'invoice_vatshifted',
                'value' => 1,
            );
        }

        if ( !empty( $start_date ) && !empty( $end_date ) ) {
            $meta_query_args[] = array(
                    'key'     => PPY_PREFIX .'invoice_date_created',
                    'value' => array( $start_date, $end_date ),
                    'compare' => 'BETWEEN',
                    'type' => 'DATE'
            );
        }

	    if ( !empty( $start_invoice_number ) && !empty( $end_invoice_number ) ) {
		    $meta_query_args[] = array(
			    'key'     => PPY_PREFIX .'invoice_number',
			    'value' => array( $start_invoice_number, $end_invoice_number ),
			    'compare' => 'BETWEEN',
			    'type' => 'NUMERIC'
		    );
	    }

        if ( $orderby == 'title' || $orderby == 'ID' ) {
            $args['orderby'] = $orderby;
        } else {
            $args['meta_key'] = PPY_PREFIX .'invoice_' . $orderby;
            $args['orderby'] = 'meta_value';
        }

        if (  $orderby == 'amount' ) {
            $args['orderby'] = 'meta_value_num';
        }

        $args['meta_query'] = $meta_query_args;

        $invoices = $this->invoice_object->get_invoices_query( $args );
        if ( $count === true ) {
            return $invoices->found_posts;
        }
        if ( $invoices->have_posts() ) {
            while ( $invoices->have_posts() ) {
                $invoices->the_post();
                $id =  get_the_ID();

                $date_created = $this->invoice_object->get_invoice_field( $id, 'date_created' );

                if ( ! empty( $date_created ) ) {
                    $invoice_date_created =  date_i18n( get_option( 'date_format' ), strtotime( $date_created ) );
                } else {
                    $invoice_date_created = __( 'No start date' );
                }

                if ( $this->invoice_object->get_invoice_field( $id, 'date_expired' ) ) {
                    $expiration = date_i18n( get_option( 'date_format' ), strtotime(  $this->invoice_object->get_invoice_field( $id, 'date_expired' )  ) );
                } else {
                    $expiration = __( 'No expiration' );
                }

                $price_fields = Poppyz_Invoice::get_price_fields( $id );
                $total_with_tax = $price_fields['total_with_tax'];

                $donation_amount = $this->invoice_object->get_invoice_field( $id, 'donation_amount' );
                if ( !empty($donation_amount) ) {
					$total_with_tax = $total_with_tax+$donation_amount;
                }

                $post_status = get_post_status( $id );
                if ( get_post_status( $id ) == 'paid' ) {
                    $post_status = 'paid';
                } elseif ( $this->invoice_object->is_invoice_expired( $id ) ) {
                    $post_status =  get_post_status( $id );
                }


                $row_actions = '<a class="has-icon"  target="_blank" title="' . __( 'View PDF' ) . '" href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'generate-pdf', 'invoice' => $id ) ), 'ppy-nonce' ) ) . '"><span class="dashicons dashicons-visibility"></span></a>';

                if ( strtolower( $post_status ) !== 'credit' ) {
                    $row_actions .= ' <a class="has-icon"  title="' . __( 'Duplicate to credit invoice' ) . '" href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'duplicate-credit', 'invoice' => $id ) ), 'ppy-nonce' ) ) . '"><span class="dashicons dashicons-image-rotate"></span></a>';
                }

                $row_actions .= ' <a class="has-icon" title="' . __( 'Delete' ) . '" onclick="return confirm(\'Are you sure you want to delete this invoice?\')"" href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'delete-invoice', 'invoice' => $id ) ), 'ppy-nonce' ) ) . '"><span class="dashicons dashicons-trash"></span></a>';


                $invoice_data[] = array(
                    'ID' 			=> $id,
                    'title' 		=> '<a target="_blank" href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'generate-pdf', 'invoice' => $id ) ), 'ppy-nonce' ) ) . '">' . get_the_title() . '</a>',
                    'product' 	    => $this->invoice_object->get_invoice_field( $id, 'product' ),
                    'company' 	    => $this->invoice_object->get_invoice_field( $id, 'company' ),
                    'firstname' 	    => $this->invoice_object->get_invoice_field( $id, 'firstname' ),
                    'lastname' 	    => $this->invoice_object->get_invoice_field( $id, 'lastname' ),
                    'amount' 		=> Poppyz_Core::format_price( $total_with_tax ),
                    'date_created' 	=> $invoice_date_created,
                    'expiration'	=> $expiration,
                    'status'		=> $post_status,
                    'actions'       => $row_actions
                );
            }
        }
        return $invoice_data;
    }

    /** ************************************************************************
     * REQUIRED! This is where you prepare your data for display. This method will
     * usually be used to query the database, sort and filter the data, and generally
     * get it ready to be displayed. At a minimum, we should set $this->items and
     * $this->set_pagination_args(), although the following properties and methods
     * are frequently interacted with here...
     *
     * @global WPDB $wpdb
     * @uses $this->_column_headers
     * @uses $this->items
     * @uses $this->get_columns()
     * @uses $this->get_sortable_columns()
     * @uses $this->get_pagenum()
     * @uses $this->set_pagination_args()
     **************************************************************************/
    function prepare_items() {
        global $wpdb; //This is used only if making any database queries

        /**
         * First, lets decide how many records per page to show
         */
        $per_page = 30;


        /**
         * REQUIRED. Now we need to define our column headers. This includes a complete
         * array of columns to be displayed (slugs & titles), a list of columns
         * to keep hidden, and a list of columns that are sortable. Each of these
         * can be defined in another method (as we've done here) before being
         * used to build the value for our _column_headers property.
         */
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();


        /**
         * REQUIRED. Finally, we build an array to be used by the class for column
         * headers. The $this->_column_headers property takes an array which contains
         * 3 other arrays. One for all columns, one for hidden columns, and one
         * for sortable columns.
         */
        $this->_column_headers = array($columns, $hidden, $sortable);





        /**
         * Instead of querying a database, we're going to fetch the example data
         * property we created for use in this plugin. This makes this example
         * package slightly different than one you might build on your own. In
         * this example, we'll be using array manipulation to sort and paginate
         * our data. In a real-world implementation, you will probably want to
         * use sort and pagination data to build a custom query instead, as you'll
         * be able to use your precisely-queried data immediately.
         */
        $data = $this->get_data();

        /**
         * This checks for sorting input and sorts the data in our array accordingly.
         *
         * In a real-world situation involving a database, you would probably want
         * to handle sorting by passing the 'orderby' and 'order' values directly
         * to a custom query. The returned data will be pre-sorted, and this array
         * sorting technique would be unnecessary.
         */
        /*function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'title'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcasecmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');*/


        /***********************************************************************
         * ---------------------------------------------------------------------
         * vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
         *
         * In a real-world situation, this is where you would place your query.
         *
         * For information on making queries in WordPress, see this Codex entry:
         * http://codex.wordpress.org/Class_Reference/wpdb
         *
         * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         * ---------------------------------------------------------------------
         **********************************************************************/


        /**
         * REQUIRED for pagination. Let's figure out what page the user is currently
         * looking at. We'll need this later, so you should always include it in
         * your own package classes.
         */
        $current_page = $this->get_pagenum();

        /**
         * REQUIRED for pagination. Let's check how many items are in our data array.
         * In real-world use, this would be the total number of items in your database,
         * without filtering. We'll need this later, so you should always include it
         * in your own package classes.
         */
        $total_items = $this->get_data(true);

        /**
         * The WP_List_Table class does not handle pagination for us, so we need
         * to ensure that the data is trimmed to only the current page. We can use
         * array_slice() to
         */
        //$data = array_slice($data,(($current_page-1)*$per_page),$per_page);



        /**
         * REQUIRED. Now we can add our *sorted* data to the items property, where
         * it can be used by the rest of the class.
         */
        $this->items = $data;

        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $_SERVER['REQUEST_URI'] = remove_query_arg( '_wp_http_referer', $_SERVER['REQUEST_URI'] );
        $pagination_args = array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page),   //WE have to calculate the total number of pages
        );
        if ( is_plugin_active( 'infinite-wp-list-tables/infinite-wp-list-tables.php' ) ) { //only enable infinite_scroll if this plugin is installed.
            $pagination_args['infinite_scroll'] = true;
        }
        $this->set_pagination_args( $pagination_args );


    }

    public function search_invoice( $text, $input_id ) {

        global $ppy_lang;
        $input_id = $input_id . '-search-input';

        if ( ! empty( $_REQUEST['orderby'] ) )
            echo '<input type="hidden" name="orderby" value="' . esc_attr( $_REQUEST['orderby'] ) . '" />';
        if ( ! empty( $_REQUEST['order'] ) )
            echo '<input type="hidden" name="order" value="' . esc_attr( $_REQUEST['order'] ) . '" />';
        if ( ! empty( $_REQUEST['post_mime_type'] ) )
            echo '<input type="hidden" name="post_mime_type" value="' . esc_attr( $_REQUEST['post_mime_type'] ) . '" />';
        if ( ! empty( $_REQUEST['detached'] ) )
            echo '<input type="hidden" name="detached" value="' . esc_attr( $_REQUEST['detached'] ) . '" />';

        $meta_key =  isset( $_REQUEST['meta_key'] ) ?  $_REQUEST['meta_key'] : '';
        $meta_value =  isset( $_REQUEST['meta_value'] ) ?  $_REQUEST['meta_value'] : '';
        ?>
        <?php
        $btn_attr = array('class' => 'ppy_button' );
        if ( !isset( $_REQUEST['meta_value'] ) ) {
            $btn_attr['disabled'] = 'disabled';
        }
        ?>

        <div class="ppy-invoice-tools-wrapper">
            <div class="ppy-invoice-tools-row"><a href="#" class="ppy-toggle"><?php echo __( 'Search and Select',"poppyz" ); ?></a></div>
            <input type="hidden" id="invoice-select-all" name="invoice-select-all" />
            <div class="ppy-invoice-tools-row"><span class="export-label">Export</span>
                <button  id="download" name="download" value="" class="inactive ppy_button has-icon "><span class="dashicons dashicons-download"></span><?php echo __( 'PDF' ,'poppyz'); ?></button>
                <button  id="export-xml" name="export-xml" class="inactive ppy_button has-icon "><span class="dashicons dashicons-download"></span><?php echo __( 'XML',"poppyz" ); ?></button>
                <button  id="export-csv" name="export-csv" class="inactive ppy_button has-icon "><span class="dashicons dashicons-download"></span><?php echo __( 'CSV',"poppyz" ); ?></button>
            </div>
        </div>
        <div class="ppy-toggle-wrapper">
            <div class="search-box">
                <fieldset>
                    <label>
                        <?php echo __( 'Select' ,'poppyz'); ?>
                    </label>
                    <div>
                        <label><?php echo __( 'From' ,'poppyz'); ?></label>
                        <input type="text" id="ppy-start-date" name="start-date" value="<?php if ( isset( $_REQUEST['start-date'] ) ) echo $_REQUEST['start-date']; ?>" class="date-picker ppy-input" placeholder="<?php echo __('Date','poppyz'); ?>" />
                    </div>

                    <div>
                        <label><?php echo __( 'To' ,'poppyz'); ?></label>
                        <input type="text" id="ppy-end-date" name="end-date" value="<?php if ( isset( $_REQUEST['end-date'] ) ) echo $_REQUEST['end-date']; ?>" class="date-picker ppy-input" placeholder="<?php echo __('Date'); ?>"  />
                    </div>
                    <label></label>
                    <div>
                        <label><?php echo __( 'From' ,'poppyz'); ?></label>
                        <input type="text" id="ppy-start-invoice-number" name="start-invoice-number" class="ppy-input" value="<?php if ( isset( $_REQUEST['start-invoice-number'] ) ) echo $_REQUEST['start-invoice-number']; ?>" placeholder="<?php echo  __('Invoice Number','poppyz'); ?>" />
                    </div>

                    <div>
                        <label><?php echo __( 'To',"poppyz" ); ?></label>
                        <input type="text" id="ppy-end-invoice-number" name="end-invoice-number" class="ppy-input" value="<?php if ( isset( $_REQUEST['end-invoice-number'] ) ) echo $_REQUEST['end-invoice-number']; ?>"  placeholder="<?php echo  __('Invoice Number','poppyz'); ?>"  />
                    </div>
                </fieldset>
                <fieldset>
                    <label>
                        <?php echo __( 'Vat Shifted',"poppyz" ); ?>
                    </label>
                    <div>
                        <input type="checkbox" name="vat-shifted" value="1" />
                    </div>
                </fieldset>
                <fieldset>
                    <label>
                        <?php echo __( 'Search',"poppyz" ); ?>
                    </label>
                    <div>
                        <select name="meta_key">
                            <option value="ppy_invoice_product" <?php selected( $meta_key, 'ppy_invoice_product' ); ?>><?php echo __( 'Product',"poppyz" ); ?></option>
                            <option value="ppy_invoice_company" <?php selected( $meta_key, 'ppy_invoice_company' ); ?>><?php echo __( 'Company' ,'poppyz'); ?></option>
                            <option value="ppy_invoice_firstname" <?php selected( $meta_key, 'ppy_invoice_firstname' ); ?>><?php echo __( 'First Name',"poppyz" ); ?></option>
                            <option value="ppy_invoice_lastname" <?php selected( $meta_key, 'ppy_invoice_lastname' ); ?>><?php echo __( 'Last Name',"poppyz" ); ?></option>
                            <option value="ppy_invoice_payment_id" <?php selected( $meta_key, 'ppy_invoice_payment_id' ); ?>><?php echo __( 'Payment ID',"poppyz" ); ?></option>
                        </select>
                    </div>

                    <div>
                        <input type="search" id="<?php echo $input_id ?>" name="meta_value" value="<?php echo $meta_value; ?>" placeholder="<?php echo __('Fill in search term if needed','poppyz'); ?>" class="ppy-input" />
                    </div>
                </fieldset>
                <fieldset>
                    <label>
						<?php echo __( 'Mode',"poppyz" ); ?>
                    </label>
                    <div>
                        <select name="invoice_mode">
                            <option>Live</option>
                            <option>Test</option>
                            <option>All</option>
                        </select>
                    </div>
                </fieldset>
                <fieldset>
                    <button id="search-submit" name="search-submit" class="ppy_button clear"> <?php echo $text; ?></button>
                </fieldset>
            </div>
        </div>

        <?php
    }


}
