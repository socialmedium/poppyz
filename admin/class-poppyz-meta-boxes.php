<?php
class Poppyz_Meta_Boxes {
	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    0.5.0
	 * @access   protected
	 * @var      string    $poppyz    The string used to uniquely identify this plugin.
	 */
	protected $poppyz;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the sM Courses and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the Dashboard and
	 * the public-facing side of the site.
	 *
	 * @since    0.5.0
	 */

	public function __construct( $poppyz ) {
		$this->poppyz = $poppyz;
	}

	public function add_meta_boxes() {
		add_meta_box( PPY_PREFIX . 'metabox_lessons', __( 'Lessons', $this->poppyz ), array( $this, 'metabox_lessons'), PPY_COURSE_PT, 'normal', 'high' );
		add_meta_box( PPY_PREFIX . 'metabox_modules', __( 'Lessoncluster', $this->poppyz ), array( $this, 'metabox_modules'), PPY_COURSE_PT, 'normal', 'high' );
		add_meta_box( PPY_PREFIX . 'metabox_tiers', __( 'Tiers', $this->poppyz ), array( $this, 'metabox_tiers'), PPY_COURSE_PT, 'normal', 'high' );
		add_meta_box( PPY_PREFIX . 'metabox_tier_options', __( 'Options', $this->poppyz ), array( $this, 'metabox_tier_options' ), PPY_TIER_PT, 'normal', 'high' );
		add_meta_box( PPY_PREFIX . 'metabox_tier_lessons_section', __( 'Lessons', $this->poppyz ), array( $this, 'metabox_tier_lessons' ), PPY_TIER_PT, 'normal', 'high' );
		add_meta_box( PPY_PREFIX . 'metabox_poppyz_lesson_links', __( 'Links', $this->poppyz ), array( $this, 'metabox_lesson_links' ), PPY_LESSON_PT, 'side', 'high' );
		add_meta_box( PPY_PREFIX . 'metabox_poppyz_tier_links', __( 'Course and program rounds', $this->poppyz ), array( $this, 'metabox_tier_links' ), PPY_TIER_PT, 'side', 'high' );
		//add_meta_box( PPY_PREFIX . 'metabox_tier_pricing', __( 'Pricing', $this->poppyz ), array( $this, 'metabox_tier_pricing' ), PPY_TIER_PT, 'side', 'core' );
	}

	public function register_metaboxes() {
		$meta_boxes = array();
		Poppyz_Meta_Boxes::build( $meta_boxes, PPY_PREFIX );

		if ( class_exists( 'RW_Meta_Box' ) )
			foreach ( $meta_boxes as $meta_box )
				new RW_Meta_Box( $meta_box );
	}

	public static function build( &$meta_boxes, $prefix ) {

		//declare variables so they don't output a PHP notice if empty
		$course_id = "";
		$course_title = "";
		global $ppy_lang;
		$ppy_core = new Poppyz_Core();
		$arr_lessons = array();

		$num_subs = '';
		$days_array = array();
		$post_id = 0;

		if ( isset( $_GET['post'] ) ) {
			$post_id =  $_GET['post'];

		}

		$meta_boxes[] = array (
			'id' => 'hide_main_menu',
			'title' => __( 'Main menu' ,'poppyz') ,
			'pages' => array (
				PPY_COURSE_PT,
				PPY_LESSON_PT,
				'post',
				'page'
			),
			'context' => 'side',
			'fields' => array (
				array(
					'name' => '',
					'id'   => "{$prefix}hide_main_menu",
					'type' => 'checkbox',
					'std'  => 0,
					'desc' => __( 'Hide the main menu on this page ',"poppyz" )
				),
			)
		);

		$meta_boxes = apply_filters( 'ppy_meta_boxes', $meta_boxes );

		wp_reset_query();
	}

	public function metabox_lessons() {
		global $ppy_lang;

		if ( !isset( $_GET['post'] ) ) {
			echo '<p>' . __( 'You must first publish the course to add lessons.' ,'poppyz') . '</p>';
			return;  //don't show this yet until the post has been published.
		}
		$ajax_nonce = wp_create_nonce(  PPY_PREFIX . "lessons_nonce" );
		$course_id = ( isset( $_GET["post"] ) ) ?  $_GET["post"] : 0 ;
		?>

        <div id="lessons-meta">
            <input type="text" class="ppy-course-input" name="ppy_post_title" id="ppy_post_title" placeholder="<?php echo __( 'Name',"poppyz" ); ?>" />
            <input type="hidden" name="ppy_order" id="ppy_order" />
            <input type="button" class="ppy_button" name="ppy_add_lesson" id="ppy_add_lesson" value="+  <?php echo __( 'Add' ,'poppyz');?>" />
            <!--<a href="<?php /*echo admin_url( 'edit-tags.php?taxonomy=' . PPY_LESSON_CAT . '&post_type=' . PPY_LESSON_PT ); */?>"><?php /*echo __( 'Categories' ); */?></a>-->
        </div>
        <ol id="admin-lesson-list" class="sortable ppy-admin-list">
			<?php
			$this->_get_lessons_by_course( $course_id );
			?>
        </ol>

        <div class="ppy-section">
            <h4><?php echo __( 'Add lesson\'s "Mark as Complete" button automatically?',"poppyz" ); ?></h4>
            <div class="ppy-field ppy-checkbox-wrapper">
                <label for="<?php echo PPY_PREFIX . 'lesson_mark_button'; ?>" class="switch">
                    <input <?php checked(1, get_post_meta($course_id, PPY_PREFIX . 'lesson_mark_button', true)); ?> type="checkbox" class="yes_no_button" name="<?php echo PPY_PREFIX . 'lesson_mark_button'; ?>" id="<?php echo PPY_PREFIX . 'lesson_mark_button'; ?>" value="1">
                    <div class="slider round dashicons-before"></div>
                </label>
                <p class="description"><?php echo __( 'If unchecked, you can manually add the button by using the shortcode <strong>[progress-button]</strong>',"poppyz" ); ?></p>
            </div>
        </div>

        <script>
            var lessons;
            jQuery( document ).ready(function( $ ) {

                $('#ppy_post_title').keypress(function (e) {
                    var key = e.which;
                    if(key == 13)  // the enter key code
                    {
                        $('#ppy_add_lesson').click();
                        return false;
                    }
                });

                lessons = $('#admin-lesson-list.sortable').nestedSortable({
                    disableNesting: 'no-nest',
                    forcePlaceholderSize: true,
                    handle: 'div',
                    items: 'li',
                    toleranceElement: '> div',
                    placeholder: 'placeholder',
                    opacity: 0.7,
                    connectWith: 'ol',
                    maxLevels: 1,
                    protectRoot: true,
                    excludeRoot: false,
                    update: function() {
                        var serialized =  $(this).nestedSortable('toArray');
                        console.log(serialized);
                        var data = {
                            action: 'update_lesson_order',
                            security: '<?php echo $ajax_nonce; ?>',
                            order: serialized
                        };
                        $.ajax ({
                            type: 'POST',
                            url: ajaxurl,
                            data: data,
                            success: function(response) {
                                lessons.nestedSortable('refresh');
                            }
                        });
                    }
                });

                $('#ppy_add_lesson').click(function(){
                    if ($('#ppy_post_title').val() == "") {
                        alert("<?php echo __( 'Lesson Name is required.',"poppyz" ); ?>");
                        return false;
                    }
                    var lesson_title = $('#ppy_post_title').val();
                    var data = {
                        action: 'add_new_lesson',
                        security: '<?php echo $ajax_nonce; ?>',
                        lesson_title: lesson_title,
                        course_id: '<?php echo $course_id; ?>',
                        count: $('.sortable li').length
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            $("#admin-lesson-list").empty().append(response);
                            lessons.nestedSortable('refresh');
                        }
                    });
                });


                $( document ).on( "click", ".delete-lesson.delete-link", function() {
                    event.stopPropagation();
                    if (!confirm('<?php echo __( 'Are you sure you want to delete this lesson?' ,'poppyz'); ?>')) return false;
                    var lesson_id = $( this ).attr("lesson-id");
                    var data = {
                        action: 'delete_lesson',
                        security: '<?php echo $ajax_nonce; ?>',
                        lesson_id: lesson_id,
                        course_id: '<?php echo $course_id; ?>'
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            $("#admin-lesson-list").empty().append(response);
                            lessons.nestedSortable('refresh');
                        }
                    });
                    return false;
                });


            });


        </script>
		<?php
	}

	public function metabox_modules() {
		global $ppy_lang;

		if ( !isset( $_GET['post'] ) ) {
			echo '<p>' . __( 'You must first publish the course to add modules.',"poppyz" ) . '</p>';
			return;  //don't show this yet until the post has been published.
		}

		$ajax_nonce = wp_create_nonce(  PPY_PREFIX . "modules_nonce" );
		$course_id = ( isset( $_GET["post"] ) ) ?  $_GET["post"] : 0 ;
		?>

        <div id="modules-meta">
            <input type="text" name="ppy_module_title" class="ppy-course-input" id="ppy_module_title" placeholder="<?php echo __( 'Name' ,'poppyz'); ?>" />
            <input type="hidden" name="ppy_order" id="ppy_order" />
            <input type="button" class="ppy_button" name="ppy_add_module" id="ppy_add_module" value="+ <?php echo __( 'Add' ,'poppyz');?>" />
        </div>

        <ol id="admin-module-list" class="sortable ppy-admin-list">
			<?php
			$this->_get_modules( $course_id );
			?>
        </ol>
        <script>
            var modules;
            jQuery( document ).ready(function( $ ) {

                $('#ppy_module_title').keypress(function (e) {
                    var key = e.which;
                    if(key == 13)  // the enter key code
                    {
                        $('#ppy_add_module').click();
                        return false;
                    }
                });

                modules = $('#admin-module-list.sortable').nestedSortable({
                    disableNesting: 'no-nest',
                    forcePlaceholderSize: true,
                    handle: 'div',
                    items: 'li',
                    toleranceElement: '> div',
                    placeholder: 'placeholder',
                    opacity: 0.7,
                    connectWith: 'ol',
                    maxLevels: 1,
                    protectRoot: true,
                    excludeRoot: false,
                    update: function() {
                        var serialized =  $(this).nestedSortable('toArray');
                        var data = {
                            action: 'update_module_order',
                            security: '<?php echo $ajax_nonce; ?>',
                            order: serialized
                        };
                        $.ajax ({
                            type: 'POST',
                            url: ajaxurl,
                            data: data,
                            success: function(response) {
                                modules.nestedSortable('refresh');
                            }
                        });
                    }
                });

                $('#ppy_add_module').click(function(){
                    if ($('#ppy_module_title').val() == "") {
                        alert("<?php echo __( 'Lessoncluster name is required.',"poppyz" ); ?>");
                        return false;
                    }
                    var module_title = $('#ppy_module_title').val();
                    var data = {
                        action: 'add_new_module',
                        security: '<?php echo $ajax_nonce; ?>',
                        module_title: module_title,
                        course_id: '<?php echo $course_id; ?>',
                        count: $('.sortable li').length
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            $("#admin-module-list").empty().append(response);
                            lessons.nestedSortable('refresh');
                        }
                    });
                });

                $( document ).on( "click", ".delete-module.delete-link", function() {
                    event.stopPropagation();
                    if (!confirm('<?php echo __( 'Are you sure you want to delete this lessoncluster?' ,'poppyz'); ?>')) return false;
                    var module_id = $( this ).attr("module-id");
                    var data = {
                        action: 'delete_module',
                        security: '<?php echo $ajax_nonce; ?>',
                        module_id: module_id,
                        course_id: '<?php echo $course_id; ?>'
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            $("#admin-module-list").empty().append(response);
                            lessons.nestedSortable('refresh');
                        }
                    });
                    return false;
                });

            });


        </script>
		<?php
	}

	//exclude modules in the lesson page that don't belong in the course
	function exclude_modules() {
		if( is_admin() && isset( $_GET['post'] ) && get_post_type( $_GET['post'] ) == PPY_LESSON_PT ) {
			add_filter( 'list_terms_exclusions', array( $this, 'exclude_terms' ), 10, 2 );
		}
	}
	function exclude_terms( $exclusions, $args ) {

		if ( !empty($args['taxonomy'][0]) && $args['taxonomy'][0] !== PPY_LESSON_CAT ) {
			return $exclusions;
		}
		$lesson_id = $_GET['post'];
		$course_id = Poppyz_Core::get_course_by_lesson( $lesson_id );
		global $wpdb;

		$terms = $wpdb->get_col(
			$wpdb->prepare(
				"
                SELECT DISTINCT     tm.term_id
                FROM                $wpdb->termmeta tm
                WHERE               tm.meta_key = %s 
                        AND         tm.meta_value = %s
                ",
				'ppy_module_course',
				$course_id
			)
		);
		//if no term has been found for this course/lesson, assign an invalid term to not display all terms
		if ( empty ( $terms ) ) {
			$terms = array( 0 );
		}
		$exclusions .= " AND ( t.term_id IN (" . implode(',', $terms ) . ") )";

		return $exclusions;
	}

	public function metabox_tiers(){

		global $ppy_lang;
		if ( !isset( $_GET['post'] ) ) {
			echo '<p>' . __( 'You must first publish the course to add tiers.',"poppyz" ) . '</p>';
			return;  //don't show this yet until the post has been published.
		}
		$course_id = ( isset( $_GET["post"] ) ) ?  $_GET["post"] : 0 ;
		$ajax_nonce = wp_create_nonce(  PPY_PREFIX . "ajax_tiers" );
		?>
        <script type="text/javascript">
            jQuery(document).ready(function($) {

                $('#ppy_tier_name, #ppy_tier_price').keypress(function (e) {
                    var key = e.which;
                    if(key == 13)  // the enter key code
                    {
                        $('#ppy_add_tier').click();
                        return false;
                    }
                });

                //only allow numbers and some symbols on the price
                $("#<?php echo PPY_PREFIX .  'tier_price' ?>").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter, comma, space, and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 188, 32, 190]) !== -1 ||
                        // Allow: Ctrl+A, Command+A
                        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
                        // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((/*e.shiftKey ||*/ (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });


                $('#ppy_add_tier').on('click', function() {
                    if ($('#ppy_tier_name').val() == "" || $('#ppy_tier_price').val() == "") {
                        alert("<?php echo __( 'Tier Name and Price are required.' ,'poppyz'); ?>");
                        return false;
                    }
                    var data = {
                        action: 'add_new_tier',
                        security: '<?php echo $ajax_nonce; ?>',
                        course_id: '<?php echo $course_id; ?>',
                        tier_name: $('#ppy_tier_name').val(),
                        tier_price: $('#ppy_tier_price').val()
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            if (response == 'below_minimum') {
                                alert("<?php echo __( 'Tier Price must be greater than 0.2.',"poppyz" ); ?>");
                            } else {
                                $("#admin-tier-list tbody").empty().append(response);
                            }
                        }
                    });

                    return false;
                });
                $('.remove-row').on('click', function() {
                    $(this).parents('tr').remove();
                    return false;
                });

                $( document ).on( "click", ".delete-tier", function() {
                    event.stopPropagation();
                    var tier_id = $( this ).attr("tier-id");
                    var data = {
                        action: 'get_tier_total_subscription',
                        security: '<?php echo $ajax_nonce; ?>',
                        tier_id: tier_id
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            var response_data = jQuery.parseJSON( response );

                            /*to continue tomorrow*/
                            if ( response_data.total_subscriber > 0 ) {
                               alert(response_data.message);
                            } else {
                                if (!confirm('<?php echo __('Are you sure you want to delete this tier?','poppyz'); ?>')) return false;
                                var data = {
                                    action: 'delete_tier',
                                    security: '<?php echo $ajax_nonce; ?>',
                                    course_id: '<?php echo $course_id; ?>',
                                    tier_id: tier_id
                                };
                                $.ajax ({
                                    type: 'POST',
                                    url: ajaxurl,
                                    data: data,
                                    success: function(response) {
                                        $("#admin-tier-list tbody").empty().append(response);
                                    }
                                });
                            }
                        }
                    });

                    return false;
                });

                $( document ).on( "click", ".clone-tier", function() {
                    event.stopPropagation();
                    var tier_id = $( this ).attr("tier-id");
                    var data = {
                        action: 'clone_tier',
                        security: '<?php echo $ajax_nonce; ?>',
                        course_id: '<?php echo $course_id; ?>',
                        tier_id: tier_id
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            $("#admin-tier-list tbody").empty().append(response);
                        }
                    });
                    return false;
                });
                $(".copy-tier-url").tooltip({
                    disabled: true,
                    close: function( event, ui ) { $(this).tooltip('disable'); }
                });
                $( document ).on( "click", ".copy-tier-url", function(e) {
                    e.preventDefault();
                    var $temp = $("<input>");
                    $("body").append($temp);
                    $temp.val($(this).prev('a').text()).select();
                    document.execCommand("copy");
                    $temp.remove();
                    $(this).tooltip('enable').tooltip('open');
                    return false;
                });

            });
        </script>

        <div id="tiers-meta">
            <input type="text" class="ppy-course-input" name="ppy_tier_name" id="ppy_tier_name" placeholder="<?php echo __( 'Name',"poppyz" ); ?>" />
            <input type="text" class="ppy-course-input" name="ppy_tier_price" id="ppy_tier_price" placeholder="<?php echo __( 'Price' ,'poppyz'); ?>" />
            <input type="button" class="ppy_button" name="ppy_add_tier" id="ppy_add_tier" value="+ <?php echo __( 'Add',"poppyz" ); ?>" />
        </div>


        <table id="admin-tier-list" class="ppy-admin-table widefat" cellspacing="0">
            <thead>
            <tr>
                <th><?php echo __( 'Name' ,'poppyz'); ?></th>
                <th><?php echo __( 'Price' ,'poppyz'); ?></th>
                <th><?php echo __( 'URL',"poppyz" ); ?></th>
                <th><?php echo __( 'Actions',"poppyz" ); ?></th>
            </tr>
            </thead>
            <tbody>
			<?php $this->_get_tiers_by_course( $course_id ); ?>
            </tbody>
        </table>

		<?php
	}

	public function metabox_tier_options() {
		global $ppy_lang;
		if ( !isset( $_GET['post'] ) ) {
			echo '<p>' . __( 'You must first publish the tier to use this section.',"poppyz" ) . '</p>';
			return;  //don't show this yet until the post has been published.
		}
		$post_id = $_GET['post'];

		?>
        <img class="ppy-loading" src="<?php echo admin_url() ?>/images/spinner-2x.gif " />
        <div id="ppy-form-settings" class="main-box poppyz-form" >
            <nav class="tabs">
                <ul class="ppy-vertical-nav-tabs">
                    <li><a href="#general" class="nav-tab dashicons-admin-settings"><?php echo __('General','poppyz'); ?></a></li>
                    <li><a href="#product-students" class="nav-tab dashicons-admin-settings"><?php echo __('Product Students','poppyz'); ?></a></li>
                    <li><a href="#pricing" class="nav-tab dashicons-money"><?php echo __('Pricing','poppyz'); ?></a></li>
                    <li><a href="#donations" id="donations_tab" class="nav-tab dashicons-money"><?php echo __('Donations','poppyz'); ?></a></li>
                    <li><a href="#upsells" class="nav-tab dashicons-money"><?php echo __('Upsells','poppyz'); ?></a></li>
                    <li><a href="#additional-products" class="nav-tab dashicons-money"><?php echo __('Additional Products','poppyz'); ?></a></li>
                    <li><a href="#thank-you-page" class="nav-tab dashicons-text-page"><?php echo __('Thank You Page','poppyz'); ?></a></li>
                    <li><a href="#emails" class="nav-tab dashicons-email-alt"><?php echo __('Emails','poppyz'); ?></a></li>
                    <li><a href="#newsletters" class="nav-tab dashicons-list-view"><?php echo __('Newsletters','poppyz'); ?></a></li>
					<?php do_action( 'ppy_meta_box_tier_options' ); ?>
                </ul>
            </nav>
            <div id="general">
                <h2><?php echo __( 'Subscriptions' ,'poppyz'); ?></h2>
				<?php
				echo Poppyz_Core::form_field_helper( $post_id, array(
						'id' => PPY_PREFIX . 'tier_subs_count',
						'name' =>  __( 'Current Count',"poppyz" ),
						'type' => 'html',
						'std' => '<strong>' . Poppyz_Subscription::get_subscription_total_by_tier( $_GET['post'] ) . '</strong> ' . __( 'subscriptions',"poppyz" ),
					)
				);
				echo Poppyz_Core::form_field_helper( $post_id, array(
						'id' => PPY_PREFIX . 'tier_subs_limit',
						'name' => __( 'Registration Limit' ,'poppyz'),
						'type' => 'number',
						'placeholder' => 0,
						'class' => 'short',
						'desc' => __('Leave it at 0 for unlimited subscriptions.' ,'poppyz')
					)
				);
				echo Poppyz_Core::form_field_helper( $post_id, array(
						'name' => __( 'Purchase expiration date' ,'poppyz'),
						'id'   => PPY_PREFIX . "tier_purchase_expiration",
						'type' => 'date',
						'desc' => __( 'Leave the date empty for no purchase expiration.' ,'poppyz'),
					)
				);
				?>
                <table class="form-table checkbox2-table allow_membership_cancellation" style="display:none;">
                    <tr>
                        <th>
                            <label for="allow_membership_cancellation"><?php echo __( 'Allow membership cancellation?'  ,'poppyz'); ?></label>
                        </th>
                        <td>
							<?php
							echo Poppyz_Core::form_field_helper( $post_id, array(
									'name' => __( 'Purchase expiration date' ,'poppyz'),
									'id'   => PPY_PREFIX . "allow_membership_cancellation",
									'type' => 'checkbox2',
									'desc' => __( 'Leave the date empty for no purchase expiration.' ,'poppyz'),
								)
							);

							?>


                        </td>
                    </tr>
                </table>

                <h2><?php echo __( 'Duration',"poppyz" ); ?></h2>
				<?php
				$days_array = array(
					'' =>  __( 'No specific day',"poppyz" ),
					'Monday' => __( 'Monday' ),
					'Tuesday' => __( 'Tuesday' ),
					'Wednesday' => __( 'Wednesday' ),
					'Thursday' => __( 'Thursday' ),
					'Friday' => __( 'Friday' ),
					'Saturday' => __( 'Saturday' ),
					'Sunday' => __( 'Sunday' )
				);

				echo Poppyz_Core::form_field_helper( $post_id, array(
						'name' => __( 'Closing Date' ,'poppyz'),
						'id'   => PPY_PREFIX . "tier_expire",
						'type' => 'date',
						'desc' => __( 'Leave the date empty for no expiration.' ,'poppyz'),
					)
				);
				echo Poppyz_Core::form_field_helper( $post_id, array(
						'name' => __( 'Closure after purchase' ,'poppyz'),
						'id'   => PPY_PREFIX . "tier_subs_expire",
						'type' => 'number',
						'placeholder' => 0,
						'desc' => __( 'Leave it at 0 for unlimited days.' ,'poppyz'),
						'class' => 'ppy-input short',
						'after_input' => __( 'days',"poppyz" )
					)
				);
				echo Poppyz_Core::form_field_helper( $post_id, array(
						'name' => __( 'Make lessons available on',"poppyz" ) . '</strong>',
						'id'   => PPY_PREFIX . "tier_subs_day_available",
						'type' => 'select',
						'options' => $days_array,
					)
				);

				echo Poppyz_Core::form_field_helper($post_id, array(
						'name' => __('Extra content','poppyz'),
						'id' => PPY_PREFIX . "tier_extra_content",
						'type' => 'editor',
						'wrapper_class' => 'full',
						'desc' => __('This content shows above the registration from.' ,'poppyz'),
					)
				);

				?>
            </div>
            <div id="product-students">
                <h2><?php echo __("Product Students", "poppyz");?></h2>
				<?php $this->meta_box_tier_students($post_id); ?>
            </div>
            <div id="pricing">
                <h2><?php echo __( 'Pricing',"poppyz" ); ?></h2>
				<?php $this->metabox_tier_pricing(); ?>
            </div>
            <div id="donations">
                <h2><?php echo __( 'Donation',"poppyz" ); ?></h2>
				<?php $this->metabox_tier_donation(); ?>
            </div>
            <div id="upsells">
                <h2><?php echo __( 'Upsells' ,'poppyz'); ?></h2>
				<?php
				echo '<div class="ppy-field-wrapper"><p>' . sprintf(__('<strong>NOTE</strong>: You must activate SEPA direct debit in your Mollie dashboard if you want to offer an upsell, see <a href="%s" target="_blank"><u>point 5</u></a> for more information','poppyz'), 'https://poppyz.nl/handleiding/payment-options/') . '</p></div>';
				/*if (get_post_meta( $post_id, PPY_PREFIX . "payment_method", true ) == 'plan') {*/
                $upsell_post_type = (defined('PPYV_VERSION') && version_compare(PPYV_VERSION, '1.4.8.4') >= 0) ? "all" : PPY_TIER_PT;
                for( $loop=0; $loop<=4; $loop++ ) {
                    echo Poppyz_Core::form_field_helper($post_id, array(
                            'name' => __('Upsell','poppyz'),
                            'id' => PPY_PREFIX . "tier_upsells",
                            'upsell_id' => PPY_PREFIX . "tier_upsells_".$loop,
                            'upsell_count' => $loop,
                            'type' => 'post',
                            'post_type' => $upsell_post_type,
                            'placeholder' => __('Select a tier','poppyz'),
                            'query_args' => array(
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                                'post__not_in' => [$post_id],
                            ),
                            'desc' => __('Select the tier that will be offered after the customer buys this tier.','poppyz'),
                        )
                    );
                    echo Poppyz_Core::form_field_helper($post_id, array(
                            'name' => __('Description','poppyz'),
                            'id' => PPY_PREFIX . "tier_upsell_description",
                            'upsell_id' => PPY_PREFIX . "tier_upsell_description_".$loop,
							'upsell_count' => $loop,
                            'type' => 'editor',
                            'wrapper_class' => 'full',
                            'desc' => __('When this tier has an upsell, show this text on top of the upsell product.' ,'poppyz'),
                        )
                    );
				}
				/*} else {
					echo '<h3>' . __('You must update this tier to a payment plan or membership to assign an upsell.') . '</h3>';
				}*/
				?>
            </div>
            <div id="additional-products">

                <?php $tier_bundled_products = get_post_meta( $post_id, PPY_PREFIX . "tier_bundled_products", true ) ? get_post_meta( $post_id, PPY_PREFIX . "tier_bundled_products", true ) :  'no' ; ?>
                <h2><?php echo __( 'Additional Products' ,'poppyz'); ?></h2>
                <div class="ppy-field-wrapper">
                    <label>
                        <?php _e('Enable bundled products?', 'poppyz'); ?>
                    </label>
                    <div class="ppy-checkbox-wrapper tier_bundled_products">
                        <div class="admin-option-radio">
                            <input type="radio" name="<?php echo PPY_PREFIX .  'tier_bundled_products';?>" value="yes" <?php checked('yes', $tier_bundled_products ) ;?> id="<?php echo PPY_PREFIX .  'tier_bundled_products';?>"> <?php echo __("Yes", "poppyz");?>
                        </div>
                        <div class="admin-option-radio">
                            <input type="radio" name="<?php echo PPY_PREFIX .  'tier_bundled_products';?>" value="no" <?php checked('no', $tier_bundled_products ) ;?> id="<?php echo PPY_PREFIX .  'tier_bundled_products';?>"> <?php echo __("No", "poppyz");?>
                        </div>
                        <small class="description">
                            <?php _e('Enabling this will make the additional products below bundled with this tier, the total price will only be the price of this tier. If this is disabled, the products below will be optional and will have their own prices.', 'poppyz'); ?>
                        </small>
                    </div>
                </div>
				<?php


				$upsell_post_type = (defined('PPYV_VERSION') && version_compare(PPYV_VERSION, '1.4.8.5') >= 0) ? "all" : PPY_TIER_PT;
				for($i=1;$i<6;$i++) {
					echo Poppyz_Core::form_field_helper($post_id, array(
							'name'        => __('Product' . ' ' . $i, 'poppyz'),
							'id'          => PPY_PREFIX . "tier_additional_product" . $i,
							'type'        => 'post',
							'post_type'   => $upsell_post_type,
							'placeholder' => __('Select a tier', 'poppyz'),
							'query_args'  => array(
								'post_status'    => 'publish',
								'posts_per_page' => -1,
								'post__not_in'   => [$post_id],
								'meta_query' => array(
									'relation' => 'OR',
                                    array(
                                        'relation' => 'AND',
                                        array(
                                            'key' => PPY_PREFIX . 'payment_method',
                                            'value' => "plan",
                                            'compare' => '!='
                                        ),
                                        array(
                                            'key' => PPY_PREFIX . 'payment_method',
                                            'value' => "membership",
                                            'compare' => '!='
                                        ),
                                    ),
									array(
										'key' => PPY_PREFIX . 'payment_method',
										'compare' => 'NOT EXISTS'
									)
								)
							),
						)
					);
					echo Poppyz_Core::form_field_helper($post_id, array(
							'name' => __('Product Image','poppyz'),
							'id' => PPY_PREFIX . "tier_additional_product_image" . $i,
							'type' => 'image',
							'wrapper_class' => 'full',
						)
					);
					$attachementId = get_post_meta($post_id, PPY_PREFIX . 'tier_additional_product_image' . $i, true);
					$attachment = wp_get_attachment_image_src( $attachementId );

					$imageUrl = get_post_meta($post_id, PPY_PREFIX . 'tier_additional_product_image_url' . $i, true);
					if ( isset($imageUrl) && $imageUrl != "") {
						echo "<div class='additional-products-image-wrapper' id='tier_additional_product_image_url".$i."_wrapper'>
                                <div class='products-image'>
                                    <img src='".$imageUrl."'/>
                                </div>
                                <div class='products-image-action'>
                                    <a href='#' post-id='".$post_id."' attachment-id='$attachementId' id='tier_additional_product_image_url".$i."' class='delete-product-image' >Delete</a>
                                </div>
                              </div>";
					}
					//attachment code here.

					echo Poppyz_Core::form_field_helper($post_id, array(
							'name' => __('Description','poppyz'),
							'id' => PPY_PREFIX . "tier_additional_product_description" . $i,
							'type' => 'editor',
							'wrapper_class' => 'full',
						)
					);
				}
				?>
            </div>
            <div id="thank-you-page">
                <h2><?php echo __('Thank You Page','poppyz'); ?></h2>
                <div class="ppy-field-wrapper">
                    <p><?php echo __('If you do not select a thank you page below, the default thank you page will be used. Below you can overwrite the content of the standard thank you e-mail for this program round. ','poppyz'); ?></p>
                </div>
				<?php
				echo Poppyz_Core::form_field_helper( $post_id, array(
						'name' =>  __( 'Thank You Page',"poppyz" ),
						'id'   => PPY_PREFIX . "tier_thank_you_page",
						'type'        => 'post',
						'post_type'  => array( 'page', 'leadpages_post' ),
						'placeholder' => __( 'Select a page',"poppyz" ),
						'query_args'  => array(
							'post_status'    => 'publish',
							'posts_per_page' => - 1,
						),
						'desc' => __( 'The user will get redirected to this page after purchase. If this is not set, they will be redirected to the default Return page set in the plugin settings.' ,'poppyz'),
					)
				);
				?>
                <div id="thank-you-content" style="display: none">
                    <h2><?php echo __('Default Thank You Page Content','poppyz'); ?></h2>

					<?php
					echo Poppyz_Core::form_field_helper( $post_id, array(
							'name' =>  __( 'Page Title' ,'poppyz'),
							'id'   => PPY_PREFIX . "tier_thank_you_title",
							'type'        => 'text',
						)
					);

					echo Poppyz_Core::form_field_helper( $post_id, array(
							'name' =>  __( 'Page Content',"poppyz" ),
							'id'   => PPY_PREFIX . "tier_thank_you_content",
							'type'        => 'editor',
							'wrapper_class' => 'full',
							'desc' => __( 'This content shows after a successful payment. Use the shortcode [course-link] to insert a link to the course that has been purchased and [name] for the buyer.',"poppyz" ),
						)
					);
					?>
                </div>
            </div>
            <div id="emails">
                <h2><?php echo __('Thank You Email' ,'poppyz'); ?></h2>
                <div class="ppy-field-wrapper">
                    <p><?php echo __('You can set the default e-mail messages on the Settings page. Below you can override the default thank you e-mail for this tier.',"poppyz" ); ?></p>
                </div>
				<?php
				echo Poppyz_Core::form_field_helper( $post_id, array(
						'name' =>  __( 'Email Subject' ,'poppyz'),
						'id'   => PPY_PREFIX . "tier_email_subject",
						'type'        => 'text',
					)
				);

				echo Poppyz_Core::form_field_helper( $post_id, array(
						'name' =>  __( 'Email Message' ,'poppyz'),
						'id'   => PPY_PREFIX . "tier_email_message",
						'type'        => 'editor',
						'wrapper_class' => 'full',
					)
				);
				?>
            </div>
            <div id="newsletters">
				<?php
				//get mailchimp list
				//require_once( PPY_DIR_PATH . 'includes/class-poppyz-newsletter.php' );
				//$ppy_nl = new Poppyz_Newsletter();
				$newsletter = new \Poppyz\Includes\Newsletters\Newsletter( new \Poppyz\Includes\Newsletters\Mailchimp());
				$mailchimp_list = $newsletter->getFormattedLists();

				$newsletter->setNewsletter(new \Poppyz\Includes\Newsletters\Aweber());
				$aweber_list = $newsletter->getFormattedLists();

				$newsletter->setNewsletter(new \Poppyz\Includes\Newsletters\Laposta());
				$la_list = $newsletter->getFormattedLists();

				$newsletter->setNewsletter(new \Poppyz\Includes\Newsletters\Mailerlite());
				$mailerlite_list = $newsletter->getFormattedLists();

				$newsletter->setNewsletter(new \Poppyz\Includes\Newsletters\Activecampaign());
				$ac_list = $newsletter->getFormattedLists();

				if ( is_object($mailchimp_list) || is_array($mailchimp_list) ) {
					echo '<h2>' . __('MailChimp','poppyz') . '</h2>';
					echo Poppyz_Core::form_field_helper( $post_id, array(
							'name'        =>  __( 'Subscribe the users to',"poppyz" ),
							'id'          => PPY_PREFIX . "mailchimp_list",
							'type'        => 'select',
							'options'     => $mailchimp_list,
							'std'         => '-1',
							'placeholder' =>  __( 'Select a list' ,'poppyz'),
						)
					);
					echo Poppyz_Core::form_field_helper( $post_id, array(
							'name'        =>  __( 'Tags',"poppyz" ),
							'id'          => PPY_PREFIX . "mailchimp_tags",
							'type'        => 'text',
							'placeholder' => __( 'Separate different tags with commas','poppyz'),
							'desc' => __('Example: tag1, tag2, tag3', 'poppyz')
						)
					);

				} else if (!empty($mailchimp_list)) {
					echo '<h2>' . __('MailChimp','poppyz') . '</h2>';
					echo  __("We can't connect to MailChimp. Please make sure you entered the correct credentials",'poppyz') ;
				}

				if ( is_object($mailerlite_list) || is_array($mailerlite_list) ) {
					echo '<h2>' . __('MailerLite','poppyz') . '</h2>';
					echo Poppyz_Core::form_field_helper( $post_id, array(
							'name'        =>  __( 'Subscribe the users to',"poppyz" ),
							'id'          => PPY_PREFIX . "mailerlite_list",
							'type'        => 'select',
							'options'     => $mailerlite_list,
							'std'         => '-1',
							'placeholder' =>  __( 'Select a list' ,'poppyz'),
						)
					);

				} else if (!empty($mailerlite_list)) {
					echo '<h2>' . __('MailerLite','poppyz') . '</h2>';
					echo  __("We can't connect to MailerLite. Please make sure you entered the correct credentials",'poppyz') ;
				}
				if ( $aweber_list ) {
					echo '<h2>' . __('Aweber','poppyz') . '</h2>';
					echo Poppyz_Core::form_field_helper( $post_id, array(
							'name'        =>  __( 'Subscribe the users to' ,'poppyz'),
							'id'          => PPY_PREFIX . "aweber_list",
							'type'        => 'select',
							'options'     => $aweber_list,
							'std'         => '-1',
							'placeholder' =>  __( 'Select a list',"poppyz" ),
						)
					);
				} else if( empty($aweber_list) ) {
					$awApiKey =  Poppyz_Core::get_option( 'aw_code' );
					if ( !empty($awApiKey) ) {
						echo '<h2>' . __('Aweber','poppyz') . '</h2>';
						echo __("We can't connect to Aweber. Please make sure you entered the correct credentials", 'poppyz');
					}
				}

				if ( is_object($ac_list) || is_array($ac_list)) {
					echo '<h2>' . __('ActiveCampaign','poppyz') . '</h2>';
					echo Poppyz_Core::form_field_helper( $post_id, array(
							'name'        =>  __( 'Subscribe the users to',"poppyz" ),
							'id'          => PPY_PREFIX . "av_list",
							'type'        => 'select',
							'options'     => $ac_list,
							'std'         => '-1',
							'placeholder' =>  __( 'Select a list' ,'poppyz'),
						)
					);
					echo Poppyz_Core::form_field_helper( $post_id, array(
							'name'        =>  __( 'Tags' ,'poppyz'),
							'id'          => PPY_PREFIX . "av_tags",
							'type'        => 'text',
							'placeholder' => __( 'Separate different tags with commas','poppyz'),
							'desc' => __('Example: tag1, tag2, tag3', 'poppyz')
						)
					);
				} else if ( empty($ac_list) ) {
					$acApiKey =  Poppyz_Core::get_option( 'ac_api_key' );
					if ( !empty($acApiKey) ) {
						echo '<h2>' . __('ActiveCampaign', 'poppyz') . '</h2>';
						echo __("We can't connect to ActiveCampaign. Please make sure you entered the correct credentials", 'poppyz');
					}

				}
				if ( is_object($la_list) || is_array($la_list) ) {
					echo '<h2>' . __('Laposta','poppyz') . '</h2>';
					echo Poppyz_Core::form_field_helper( $post_id, array(
							'name'        =>  __( 'Subscribe the users to',"poppyz" ),
							'id'          => PPY_PREFIX . "la_list",
							'type'        => 'select',
							'options'     => $la_list,
							'std'         => '-1',
							'placeholder' =>  __( 'Select a list' ,'poppyz'),
						)
					);
				}
				else if ( !empty($la_list) ) {
					echo '<h2>' . __('Laposta','poppyz') . '</h2>';
					echo  __("We can't connect to Laposta. Please make sure you entered the correct credentials",'poppyz') ;

				}
				?>
            </div>
        </div>
		<?php
	}

	public function metabox_tier_lessons(){
		?>
        <script>
            jQuery(document).ready(function($){
                var radios = $('#table-tier-lessons input[type="radio"]');

                //disable all inputs first
                radios.next().prop("readonly", true);
                //enable checked inputs
                radios.filter('input:checked').next().prop("readonly", false);

                $('.date-picker').datepicker({
                    dateFormat: 'dd-mm-yy'
                }).keyup(function(e) {
                    if(e.keyCode == 8 || e.keyCode == 46) {
                        $.datepicker._clearDate(this);
                    }
                });

                $('.date-picker, .ppy_tier_lesson_days, .date-picker, .ppy_tier_lesson_week').click(function(){
                    $(this).prev().click();
                });


                radios.click(function() {
                    $(this).next().focus();
                    //get the name of the radio button
                    $name = $(this).prop('id');

                    //remove _select string to get the row class
                    $rowClass = $name.replace('_select', '');
                    //disable inputs inside this row
                    $('tr.' + $rowClass + ' input[type="text"],tr.' + $rowClass + ' input[type="number"]').prop("readonly", true).val("");
                    //$('tr.' + $rowClass + ' input[type="checkbox"]').prop("checked", true).val("");

                    $('tr.' + $rowClass + ' select').prop("readonly", true).val("");

                    if($(this).prop('value') == 'day') {
                        $('input#' + $rowClass + '_day').prop("readonly", false);
                    } else if ($(this).prop('value') == 'date'){
                        //$('input#' + $rowClass + '_date').prop("readonly", false);
                    } else {
                        if ( $('#ppy_tier_subs_day_available').val() == '0' ) {
                            alert('You must first select a starting day in the sidebar');
                            $('#ppy_tier_subs_day_available').focus();
                            return false;
                        } else {
                            $('input#' + $rowClass + '_week').prop("readonly", false);
                        }
                    }
                });

                /*var exclude = $('#ppy_metabox_tier_lessons_section input[type="checkbox"]');
                exclude.each(function() {
                    toggle_tier_lessons($(this));
                });
                exclude.change(function() {
                    toggle_tier_lessons($(this));
                });*/

                function toggle_tier_lessons(el) {
                    $name = el.prop('id');
                    //remove _exclude string to get the row class
                    $rowClass = $name.replace('_exclude', '');
                    if (el.is(":checked")) {
                        $('tr.' + $rowClass + ' input[type="text"],tr.' + $rowClass + ' input[type="number"]').prop("readonly", false);
                    } else {
                        //disable inputs inside this row
                        $('tr.' + $rowClass + ' input[type="text"],tr.' + $rowClass + ' input[type="number"]').prop("readonly", true).val("");
                        $('tr.' + $rowClass + ' input[type="radio"]').prop("checked", false);
                    }
                }

            });


        </script>
		<?php

		$tier_id =  $_GET['post'];
		wp_nonce_field( 'metabox_tier_lessons', 'metabox_tier_lessons_nonce' );
		$course_id = get_post_meta( $tier_id , PPY_PREFIX . "tier_course", true );
		global $ppy_lang, $post;
		$ppy_core = new Poppyz_Core();
		$lessons = $ppy_core->get_lessons_by_course( $course_id );
		$temp_post = $post;
		if ( $lessons->have_posts() ) {
			echo '<table id="table-tier-lessons" class="widefat ppy-admin-table"><thead>';
			echo '<tr>';
			echo '<th >' . __( 'Include',"poppyz" ) .  '</th>';
			echo '<th>' . __( 'Lesson' ,'poppyz') .  '</th>';
			echo '<th>' . __( 'Days to start after purchase',"poppyz" ) .  '</th>';
			echo '<th>' . __( 'Starting week' ,'poppyz') .  ' <a class="dashicons dashicons-info-outline poppyz-tooltip" href="#" title="Vul een getal in: 0,1,2 etc. Als je niks in vult dan is de les direct toegankelijk."></a></th>';
			echo '<th>' . __( 'Start date' ,'poppyz') .  '</th>';
			echo '</tr></thead><tbody>';

			while ($lessons->have_posts()) : $lessons->the_post();
				$meta_name =  PPY_PREFIX . 'tier_lesson[' . get_the_ID() .  ']';
				$class_name =  PPY_PREFIX . 'tier_lesson_' . get_the_ID();

				$lesson = get_post_meta( $tier_id,  PPY_PREFIX . 'tier_lessons_' . get_the_ID(), true );
				$day = isset( $lesson['day'] ) ? $lesson['day'] : "";
				$date = isset( $lesson['date'] ) ? $lesson['date'] : "";
				$type = isset( $lesson['type'] ) ? $lesson['type'] : "";

				$week = isset( $lesson['week'] ) ? $lesson['week'] : "";

				$type_day = ($type == "day") ? "checked=checked" : "";
				$type_date = ($type == "date") ? "checked=checked" : "";
				$type_week = ($type == "week") ? "checked=checked" : "";

				$exclude = !empty( $lesson['exclude'] ) ? "" : "checked=checked";

				echo '<tr class="tier-row ' . $class_name . '">';
				$checkbox = '<div class="ppy-field ppy-checkbox-wrapper ">
                <label class="switch" for="' . $class_name . '">
                    <input type="checkbox" class="yes_no_button" name="' . $meta_name . '[exclude]"  id="' . $class_name . '" ' . $exclude .' >
                    <div class="slider round dashicons-before "></div>
                </label>';

				echo '<td data-label="' . __( 'Include?' ,'poppyz') .  '">' . $checkbox .'</td>';
				//<input type="checkbox" id="' . $class_name . '_exclude" name="' . $meta_name . '[exclude]" value="1" ' . $exclude . '  />

				echo '<td data-label="' . __( 'Lesson?' ,'poppyz') .  '" style="vertical-align: middle;"><strong>' . get_the_title() .  '</strong></td>';
				echo '<td data-label="' . __( 'Days to start after purchase' ,'poppyz') .  '"><input ' . $type_day . ' type="radio" id="' . $class_name . '_select" name="' . $meta_name . '[type]" value="day" /><input id="' . $class_name .'_day" class="short ppy_tier_lesson_days ppy-input" type="number" name="' . $meta_name . '[day]" value="' . $day . '" /></td>';
				echo '<td data-label="' . __( 'Starting week' ,'poppyz') .  '" ><input ' . $type_week . ' type="radio" id="' . $class_name . '_select" name="' . $meta_name . '[type]" value="week" /><input id="' . $class_name .'_week" class="short ppy_tier_lesson_week ppy-input" type="number" name="' . $meta_name . '[week]" value="' . $week . '" /></td>';
				echo '<td data-label="' . __( 'Start date' ,'poppyz') .  '" ><input ' . $type_date . ' type="radio" id="' . $class_name . '_select" name="' . $meta_name . '[type]" value="date" /><input  id="' . $class_name .'_date" class="short-2 date-picker ppy-input" type="text" readonly="true" name="' . $meta_name . '[date]" value="' . $date . '"  /></td>';


				/*echo '<td><input type="text" name="' . PPY_PREFIX . 'tier_start_days_lesson_' . get_the_ID() .  '" value="" /></td>';
				echo '<td><input type="text" name="' . PPY_PREFIX . 'tier_start_date_lesson_' . get_the_ID() .  '" value="" class="date-picker" /></td>';
				echo '<td><input type="checkbox" name="' . PPY_PREFIX . 'tier_exclude_lesson_' . get_the_ID() .  '" value="" /></td>';*/

				echo '</tr>';
			endwhile;

			echo '</tbody></table>';
			echo '<p><i>' . __( "Leave fields empty to make them accessible right away." ,'poppyz') . '</i></p>';
			$post = $temp_post;
		} else {
			echo __( "This tier's course has no lessons assigned to it yet." ,'poppyz');
		}

	}

	public function metabox_lesson_save( $post_id ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if( isset($_POST[PPY_PREFIX . 'lesson_progress_list']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'lesson_progress_list', $_POST[PPY_PREFIX .  'lesson_progress_list'] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . 'lesson_progress_list');
		}
		if( isset($_POST[PPY_PREFIX . 'lesson_mark_button']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'lesson_mark_button', $_POST[PPY_PREFIX .  'lesson_mark_button'] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . 'lesson_mark_button');
		}
	}

	public function metabox_tier_fields_save( $post_id ){
		// Check if our nonce is set.
		if ( ! isset( $_POST['metabox_tier_lessons_nonce'] ) ) {
			return;
		}

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $_POST['metabox_tier_lessons_nonce'], 'metabox_tier_lessons' ) ) {
			return;
		}

		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		// Check the user's permissions.
		if ( PPY_TIER_PT == $_POST['post_type'] ) {
			if ( ! current_user_can( 'manage_options', $post_id ) ) {
				return;
			}
		} else {
			return;
		}

		/* OK, it's safe for us to save the data now. */


		/* General Settings Save */

		if( isset($_POST[PPY_PREFIX .  'tier_subs_limit']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_subs_limit', $_POST[PPY_PREFIX .  'tier_subs_limit'] );
		}

		if( isset($_POST[PPY_PREFIX .  'tier_purchase_expiration']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_purchase_expiration', $_POST[PPY_PREFIX .  'tier_purchase_expiration'] );
		}
		if( isset($_POST[PPY_PREFIX .  'allow_membership_cancellation']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'allow_membership_cancellation', $_POST[PPY_PREFIX .  'allow_membership_cancellation'] );
		} else {
			update_post_meta( $post_id, PPY_PREFIX . 'allow_membership_cancellation', "off" );
		}
		if( isset($_POST[PPY_PREFIX .  'tier_expire']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_expire', $_POST[PPY_PREFIX .  'tier_expire'] );
		}
		if( isset($_POST[PPY_PREFIX .  'tier_subs_expire']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_subs_expire', $_POST[PPY_PREFIX .  'tier_subs_expire'] );
		}
		if( isset($_POST[PPY_PREFIX .  'tier_subs_day_available']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_subs_day_available', $_POST[PPY_PREFIX .  'tier_subs_day_available'] );
		}

		if( isset($_POST[PPY_PREFIX .  'tier_extra_content']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_extra_content', $_POST[PPY_PREFIX .  'tier_extra_content'] );
		}

		if( isset($_POST[PPY_PREFIX .  'tier_thank_you_page']) ) {
			if ($_POST[PPY_PREFIX .  'tier_thank_you_page'] > 0 ) {
				update_post_meta( $post_id, PPY_PREFIX . 'tier_thank_you_page', $_POST[PPY_PREFIX .  'tier_thank_you_page'] );
			} else {
				delete_post_meta( $post_id, PPY_PREFIX . 'tier_thank_you_page');
			}
		}

		if( !empty($_POST[PPY_PREFIX .  'tier_upsells']) ) {
			delete_post_meta( $post_id, PPY_PREFIX . 'tier_upsells' );
			foreach ($_POST[PPY_PREFIX .  'tier_upsells'] as $tier_upsells) {
				if (empty($tier_upsells)) continue;
				add_post_meta($post_id, PPY_PREFIX . 'tier_upsells', $tier_upsells);
			}
			$tiers = get_post_meta( $post_id, PPY_PREFIX . 'tier_upsells' );
		}
		if( isset($_POST[PPY_PREFIX .  'tier_upsell_description']) ) {
			delete_post_meta( $post_id, PPY_PREFIX . 'tier_upsell_description' );
			foreach ($_POST[PPY_PREFIX . 'tier_upsell_description'] as $tier_upsell_description) {
				if (empty($tier_upsell_description)) continue;
				add_post_meta($post_id, PPY_PREFIX . 'tier_upsell_description', $tier_upsell_description);
			}
		}


		for($i=1;$i<6;$i++) {
			if( isset($_POST[PPY_PREFIX .  'tier_additional_product' . $i]) ) {
				if ($_POST[PPY_PREFIX .  'tier_additional_product' . $i] > 0 ) {
					update_post_meta( $post_id, PPY_PREFIX . 'tier_additional_product' . $i, $_POST[PPY_PREFIX .  'tier_additional_product' . $i] );
				} else {
					delete_post_meta( $post_id, PPY_PREFIX . 'tier_additional_product' . $i);
				}
			}
			if( !empty($_POST[PPY_PREFIX .  'tier_additional_product_description' . $i]) ) {
				update_post_meta($post_id, PPY_PREFIX . 'tier_additional_product_description' . $i, $_POST[PPY_PREFIX . 'tier_additional_product_description' . $i]);
			} else {
				delete_post_meta( $post_id, PPY_PREFIX . 'tier_additional_product_description' . $i);
			}
			if( isset($_FILES[PPY_PREFIX .  'tier_additional_product_image' . $i]) &&  $_FILES[PPY_PREFIX .  'tier_additional_product_image' . $i]["name"] != "") {
				$upload = wp_handle_upload(
					$_FILES[ PPY_PREFIX .  'tier_additional_product_image' . $i ],
					array( 'test_form' => false )
				);
				if ( isset($upload[ 'file' ]) && !empty($upload[ 'file' ]) ) {
					$filename = $upload['file'];
					list($width, $height) = getimagesize($filename);
					$newheight = 104;
					$newwidth = 70;

					$thumb = imagecreatetruecolor($newwidth, $newheight);
					$resizeFileName = time();
					switch ($_FILES[PPY_PREFIX . 'tier_additional_product_image' . $i]['type']) {
						case "image/jpeg":
							$source = imagecreatefromjpeg($filename);
							imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
							imagejpeg($thumb, $filename);
							break;
						case "image/jpg":
							$source = imagecreatefromjpeg($filename);
							imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
							imagejpeg($thumb, $filename);
							break;
						case "image/png":
							$source = imagecreatefrompng($filename);
							imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
							imagepng($thumb, $filename);
							break;
						case "image/gif":
							$source = imagecreatefromgif($filename);
							imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
							imagegif($thumb, $filename);
							break;
					}

					if (!empty($upload['error'])) {
						wp_die($upload['error']);
					}
					// it is time to add our uploaded image into WordPress media library

					$attachment = array(
						'guid' => $upload['url'],
						'post_mime_type' => $upload['type'],
						'post_title' => preg_replace('/\.[^.]+$/', '', basename($upload['file'])),
						'post_content' => '',
						'post_status' => 'inherit'
					);
					$attachmentId = wp_insert_attachment($attachment, $filename, $post_id);

					if (is_wp_error($attachmentId) || !$attachmentId) {
						wp_die('Upload error.');
					}
					require_once(ABSPATH . 'wp-admin/includes/image.php');
					$attach_data = wp_generate_attachment_metadata($attachmentId, $filename);

					wp_update_attachment_metadata($attachmentId, $attach_data);
					//wp_safe_redirect( $upload[ 'url' ] );
					update_post_meta($post_id, PPY_PREFIX . 'tier_additional_product_image' . $i, $attachmentId);
					update_post_meta($post_id, PPY_PREFIX . 'tier_additional_product_image_url' . $i, $upload['url']);
				}

			}

		}

		if( isset($_POST[PPY_PREFIX .  'tier_thank_you_title']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_thank_you_title', $_POST[PPY_PREFIX .  'tier_thank_you_title'] );
		}
		if( isset($_POST[PPY_PREFIX .  'tier_thank_you_content']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_thank_you_content', $_POST[PPY_PREFIX .  'tier_thank_you_content'] );
		}
		if( isset($_POST[PPY_PREFIX .  'tier_email_subject']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_email_subject', $_POST[PPY_PREFIX .  'tier_email_subject'] );
		}
		if( isset($_POST[PPY_PREFIX .  'tier_email_message']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_email_message', $_POST[PPY_PREFIX .  'tier_email_message'] );
		}


		if( isset($_POST[PPY_PREFIX .  'mailchimp_list']) ) {
			if ( !empty( $_POST[PPY_PREFIX .  'mailchimp_list'] ) ) {
				update_post_meta( $post_id, PPY_PREFIX . 'mailchimp_list', $_POST[PPY_PREFIX .  'mailchimp_list'] );
			} else {
				delete_post_meta( $post_id, PPY_PREFIX . 'mailchimp_list');
			}
		}
		if( isset($_POST[PPY_PREFIX .  'mailchimp_tags']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'mailchimp_tags', $_POST[PPY_PREFIX .  'mailchimp_tags'] );
		}
		if( isset($_POST[PPY_PREFIX .  'mailerlite_list']) ) {
			if ( !empty( $_POST[PPY_PREFIX .  'mailerlite_list'] ) ) {
				update_post_meta( $post_id, PPY_PREFIX . 'mailerlite_list', $_POST[PPY_PREFIX .  'mailerlite_list'] );
			} else {
				delete_post_meta( $post_id, PPY_PREFIX . 'mailerlite_list');
			}
		}
		if( isset($_POST[PPY_PREFIX .  'aweber_list']) ) {
			if ( !empty( $_POST[PPY_PREFIX .  'aweber_list'] ) ) {
				update_post_meta( $post_id, PPY_PREFIX . 'aweber_list', $_POST[PPY_PREFIX .  'aweber_list'] );
			} else {
				delete_post_meta( $post_id, PPY_PREFIX . 'aweber_list');
			}
		}
		if( isset($_POST[PPY_PREFIX .  'av_list']) ) {
			if ( !empty( $_POST[PPY_PREFIX .  'av_list'] ) ) {
				update_post_meta( $post_id, PPY_PREFIX . 'av_list', $_POST[PPY_PREFIX .  'av_list'] );
			} else {
				delete_post_meta( $post_id, PPY_PREFIX . 'av_list');
			}
		}
		if( isset($_POST[PPY_PREFIX .  'av_tags']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'av_tags', $_POST[PPY_PREFIX .  'av_tags'] );
		}

		if( isset($_POST[PPY_PREFIX . 'la_list']) ) {
			if ( !empty( $_POST[PPY_PREFIX .  'la_list'] ) ) {
				update_post_meta( $post_id, PPY_PREFIX . 'la_list', $_POST[PPY_PREFIX .  'la_list'] );
			} else {
				delete_post_meta( $post_id, PPY_PREFIX . 'la_list');
			}
		}


		if ( isset( $_POST[ PPY_PREFIX . 'tier_lesson'] ) && count( $_POST[ PPY_PREFIX . 'tier_lesson'] ) > 0 ) {
			foreach ( $_POST[ PPY_PREFIX . 'tier_lesson'] as $course_id => $lesson ) {
				if (!empty($lesson['exclude']) ) {
					unset($lesson['exclude']);
				} else {
					$lesson['exclude'] = 'on';
				}

				if ( !empty( $lesson['day'] ) || !empty( $lesson['date'] ) || !empty( $lesson['week']  ) || !empty($lesson['exclude'] )) {
					update_post_meta( $post_id, PPY_PREFIX . 'tier_lessons_' . $course_id , $lesson );
				} else {
					delete_post_meta( $post_id, PPY_PREFIX . 'tier_lessons_' . $course_id );
				}

			}
		}
		if( isset($_POST[PPY_PREFIX .  'tier_tax_rate']) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_tax_rate', $_POST[PPY_PREFIX .  'tier_tax_rate'] );
		}

		if( isset($_POST[PPY_PREFIX .  'price_tax']) && $_POST[PPY_PREFIX .  'price_tax'] ) {
			update_post_meta( $post_id, PPY_PREFIX . 'price_tax', $_POST[PPY_PREFIX .  'price_tax'] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . 'price_tax' );
		}

		if( isset($_POST[PPY_PREFIX .  'tier_tax_rate_mb']) && $_POST[PPY_PREFIX .  'tier_tax_rate_mb'] ) {
			update_post_meta( $post_id, PPY_PREFIX .  'tier_tax_rate_mb', $_POST[PPY_PREFIX .  'tier_tax_rate_mb'] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX .  'tier_tax_rate_mb' );
		}

		if( isset($_POST[PPY_PREFIX . "tier_donation_title"]) && $_POST[PPY_PREFIX . "tier_donation_title"] ) {
			update_post_meta( $post_id, PPY_PREFIX . "tier_donation_title", $_POST[PPY_PREFIX . "tier_donation_title"] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . "tier_donation_title" );
		}

		if( isset($_POST[PPY_PREFIX . "tier_donation_description"]) && $_POST[PPY_PREFIX . "tier_donation_description"] ) {
			update_post_meta( $post_id, PPY_PREFIX . "tier_donation_description", $_POST[PPY_PREFIX . "tier_donation_description"] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . "tier_donation_description" );
		}




		if( isset($_FILES[PPY_PREFIX .  'donation_image']) &&  $_FILES[PPY_PREFIX .  'donation_image']["name"] != "") {
            /*
			$upload = wp_handle_upload(
				$_FILES[ PPY_PREFIX .  'donation_image' ],
				array( 'test_form' => false )
			);
			$filename = $upload[ 'file' ];

			if( ! empty( $upload[ 'error' ] ) ) {
				wp_die( $upload[ 'error' ] );
			}
			// it is time to add our uploaded image into WordPress media library

			$attachment = array(
				'guid'           => $upload[ 'url' ],
				'post_mime_type' => $upload[ 'type' ],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $upload[ 'file' ] ) ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);


			$attachmentId = wp_insert_attachment( $attachment, $filename, $post_id );


			if( is_wp_error( $attachmentId ) || ! $attachmentId ) {
				wp_die( 'Upload error.' );
			}
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			$attach_data = wp_generate_attachment_metadata( $attachmentId, $filename );

			wp_update_attachment_metadata( $attachmentId, $attach_data );
			//wp_safe_redirect( $upload[ 'url' ] );
			update_post_meta($post_id, PPY_PREFIX . 'tier_donation_image', $attachmentId);
			update_post_meta($post_id, PPY_PREFIX . 'tier_donation_image_url', $upload[ 'url' ]);
            */

		}


		$number_of_payments  =  !empty( $_POST[PPY_PREFIX . "number_of_payments"] ) && $_POST[PPY_PREFIX . "number_of_payments"] > 1 ? $_POST[PPY_PREFIX . "number_of_payments"] : 2;
		$payment_frequency =  !empty( $_POST[PPY_PREFIX . "payment_frequency"] ) ?  $_POST[PPY_PREFIX . "payment_frequency"] : 1;

		$price = Poppyz_Core::save_formatted_price( $_POST[PPY_PREFIX .  'tier_price'] );
		$tier_initial_price = Poppyz_Core::save_formatted_price($_POST[PPY_PREFIX .  'tier_initial_price']);

		$initial_payment_frequency = !empty($_POST[PPY_PREFIX . "initial_payment_frequency"]) ? $_POST[PPY_PREFIX . "initial_payment_frequency"] : 0;

		$price = (!empty($price)) ? $price : 0;
		update_post_meta( $post_id, PPY_PREFIX . 'tier_price', $price );


		$discount_price = Poppyz_Core::save_formatted_price( $_POST[PPY_PREFIX .  'tier_discount'] );
		$discount_price = (!empty($discount_price)) ? $discount_price : 0;
		update_post_meta( $post_id, PPY_PREFIX . 'tier_discount', $discount_price );


		$discount_implementation_option = $_POST[PPY_PREFIX .  'discount_implementation_option'];
		$discount_implementation_option = (!empty($discount_implementation_option)) ? $discount_implementation_option : 0;
		update_post_meta( $post_id, PPY_PREFIX . 'discount_implementation_option', $discount_implementation_option );


		$method = $_POST[PPY_PREFIX . "payment_method"];
		//$type = isset( $_POST[PPY_PREFIX . "payment_automatic"] ) ? $_POST[PPY_PREFIX . "payment_automatic"] : '' ;
		$type = 1;

		//we will no longer implement manual payment plans, change tier to automatic payment if there are no manual subscriptions
		/*if ( !Poppyz_Subscription::tier_has_manual_subscriptions( $post_id ) ) {
			$type = 1;
		}*/

		update_post_meta( $post_id, PPY_PREFIX . 'payment_method',  $method );
		update_post_meta( $post_id, PPY_PREFIX . 'payment_automatic',  $type );

		if ( $tier_initial_price > 0 )  {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_initial_price',  $tier_initial_price );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . 'tier_initial_price' );
		}

		if ( $initial_payment_frequency > 0 ) {
			update_post_meta( $post_id, PPY_PREFIX . 'initial_payment_frequency', $initial_payment_frequency );
			update_post_meta( $post_id, PPY_PREFIX . 'initial_payment_timeframe',  $_POST[PPY_PREFIX . "initial_payment_timeframe"] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . 'initial_payment_frequency' );
			delete_post_meta( $post_id, PPY_PREFIX . 'initial_payment_timeframe' );
		}

		if ( $method != 'donation' && !empty($_POST[PPY_PREFIX . "tier_voluntary_contribution"]) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_voluntary_contribution',  $_POST[PPY_PREFIX . "tier_voluntary_contribution"] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . 'tier_voluntary_contribution' );
		}

		if ( $method == 'plan' ) {
			update_post_meta( $post_id, PPY_PREFIX . 'payment_frequency', $payment_frequency );
			update_post_meta( $post_id, PPY_PREFIX . 'payment_timeframe',  $_POST[PPY_PREFIX . "payment_timeframe"] );
			update_post_meta( $post_id, PPY_PREFIX . 'number_of_payments',  $number_of_payments );
			delete_post_meta( $post_id, PPY_PREFIX . 'membership_interval' );
			//delete_post_meta( $post_id, PPY_PREFIX . 'membership_recurring' );
			delete_post_meta( $post_id, PPY_PREFIX . 'donation_allow_fixed_amounts' );
			delete_post_meta( $post_id, PPY_PREFIX . 'donation_fixed_amounts' );
		} elseif ( $method == 'membership') {
			delete_post_meta( $post_id, PPY_PREFIX . 'payment_frequency' );
			delete_post_meta( $post_id, PPY_PREFIX . 'payment_timeframe' );
			delete_post_meta( $post_id, PPY_PREFIX . 'number_of_payments' );
			if ( empty( $_POST[PPY_PREFIX . "membership_interval"] ) ) $_POST[PPY_PREFIX . "membership_interval"] = 1;
			update_post_meta( $post_id, PPY_PREFIX . 'membership_interval',  $_POST[PPY_PREFIX . "membership_interval"] );
			update_post_meta( $post_id, PPY_PREFIX . 'membership_timeframe',  $_POST[PPY_PREFIX . "membership_timeframe"] );
			//update_post_meta( $post_id, PPY_PREFIX . 'membership_recurring',  $_POST[PPY_PREFIX . "membership_recurring"] );
			delete_post_meta( $post_id, PPY_PREFIX . 'donation_allow_fixed_amounts' );
			delete_post_meta( $post_id, PPY_PREFIX . 'donation_fixed_amounts' );
		}  elseif ( $method == 'donation') {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_price', 0 );
			delete_post_meta( $post_id, PPY_PREFIX . 'payment_frequency' );
			delete_post_meta( $post_id, PPY_PREFIX . 'payment_timeframe' );
			delete_post_meta( $post_id, PPY_PREFIX . 'number_of_payments' );
			delete_post_meta( $post_id, PPY_PREFIX . 'membership_interval' );
			delete_post_meta( $post_id, PPY_PREFIX . 'membership_timeframe' );
			delete_post_meta( $post_id, PPY_PREFIX . 'membership_duration' );
			delete_post_meta( $post_id, PPY_PREFIX . 'payment_timeframe' );
			delete_post_meta( $post_id, PPY_PREFIX . 'tier_initial_price' );
			delete_post_meta( $post_id, PPY_PREFIX . 'initial_payment_frequency' );
			delete_post_meta( $post_id, PPY_PREFIX . 'payment_automatic' );
			if ( isset($_POST[PPY_PREFIX . "donation_fixed_amounts"]) && is_array($_POST[PPY_PREFIX . "donation_fixed_amounts"])) {
				$count = 0;
				$donationFixedAmountArray = [];
				foreach ( $_POST[PPY_PREFIX . "donation_fixed_amounts"] as $donationFixedAmount ) {
					$donationFixedAmount = Poppyz_Core::save_formatted_price( $donationFixedAmount );
					array_push($donationFixedAmountArray, $donationFixedAmount);
				}
				update_post_meta( $post_id, PPY_PREFIX . 'donation_fixed_amounts',$donationFixedAmountArray  );
			}
			update_post_meta( $post_id, PPY_PREFIX . 'donation_allow_custom_amount',  $_POST[PPY_PREFIX . "donation_allow_custom_amount"] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . 'payment_frequency' );
			delete_post_meta( $post_id, PPY_PREFIX . 'number_of_payments' );
			delete_post_meta( $post_id, PPY_PREFIX . 'membership_duration' );
			delete_post_meta( $post_id, PPY_PREFIX . 'payment_timeframe' );
			delete_post_meta( $post_id, PPY_PREFIX . 'tier_initial_price' );
			//delete_post_meta( $post_id, PPY_PREFIX . 'membership_recurring' );
			delete_post_meta( $post_id, PPY_PREFIX . 'donation_allow_fixed_amounts' );
			delete_post_meta( $post_id, PPY_PREFIX . 'donation_fixed_amounts' );
			delete_post_meta( $post_id, PPY_PREFIX . 'initial_payment_frequency' );
		}
		if (  !empty($_POST[PPY_PREFIX . "tier_donation_enabled"]) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_donation_enabled',  $_POST[PPY_PREFIX . "tier_donation_enabled"] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . 'tier_donation_enabled');
		}
        if (  !empty($_POST[PPY_PREFIX . "tier_bundled_products"]) ) {
			update_post_meta( $post_id, PPY_PREFIX . 'tier_bundled_products',  $_POST[PPY_PREFIX . "tier_bundled_products"] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . 'tier_bundled_products');
		}

		if ( isset($_POST[PPY_PREFIX . "donation_fixed_amounts"]) && is_array($_POST[PPY_PREFIX . "donation_fixed_amounts"])) {
			$count = 0;
			$donationFixedAmountArray = [];
			foreach ( $_POST[PPY_PREFIX . "donation_fixed_amounts"] as $donationFixedAmount ) {
				$donationFixedAmount = Poppyz_Core::save_formatted_price( $donationFixedAmount );
				array_push($donationFixedAmountArray, $donationFixedAmount);
			}
			update_post_meta( $post_id, PPY_PREFIX . 'donation_fixed_amounts',$donationFixedAmountArray  );
		}
        if ( isset($_POST[PPY_PREFIX . "donation_allow_custom_amount"]) && !empty($_POST[PPY_PREFIX . "donation_allow_custom_amount"]) ) {
			update_post_meta($post_id, PPY_PREFIX . 'donation_allow_custom_amount', $_POST[PPY_PREFIX . "donation_allow_custom_amount"]);
		}



		if (  !empty($_POST[PPY_PREFIX .  'tier_schedule_voluntary_contribution']) ) {
			update_post_meta( $post_id, PPY_PREFIX .  'tier_schedule_voluntary_contribution',  $_POST[PPY_PREFIX .  'tier_schedule_voluntary_contribution'] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX .  'tier_schedule_voluntary_contribution');
		}
		if (  !empty($_POST[PPY_PREFIX . "tier_donation_email_content"]) ) {
			update_post_meta( $post_id, PPY_PREFIX . "tier_donation_email_content",  $_POST[PPY_PREFIX . "tier_donation_email_content"] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX . "tier_donation_email_content");
		}
		if (  !empty($_POST[PPY_PREFIX .  'number_of_days_after_purchase']) ) {
			update_post_meta( $post_id, PPY_PREFIX .  'number_of_days_after_purchase',  $_POST[PPY_PREFIX .  'number_of_days_after_purchase'] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX .  'number_of_days_after_purchase');
		}

		if (  !empty($_POST[PPY_PREFIX .  'donation_email_subject']) ) {
			update_post_meta( $post_id, PPY_PREFIX .  'donation_email_subject',  $_POST[PPY_PREFIX .  'donation_email_subject'] );
		} else {
			delete_post_meta( $post_id, PPY_PREFIX .  'donation_email_subject');
		}


	}

	public function add_new_lesson(){
		check_ajax_referer(PPY_PREFIX . 'lessons_nonce', 'security');
		if ( empty( $_POST['lesson_title'] ) ) return;
		$post_arr = array (
			'post_title' =>  $_POST['lesson_title'],
			'post_status' => 'publish',
			'post_type' => PPY_LESSON_PT,
			'menu_order' => $_POST['count']
		);
		$post_id = wp_insert_post( $post_arr );
		add_post_meta( $post_id, PPY_PREFIX . 'lessons_course', $_POST['course_id'], true );
		$this->_get_lessons_by_course( $_POST['course_id'] );
		die;
	}

	public function add_new_module() {
		check_ajax_referer(PPY_PREFIX . 'modules_nonce', 'security');
		$term_id = wp_insert_term(
			$_POST['module_title'], // the term
			PPY_LESSON_CAT,
			array( 'slug' => Poppyz_Core::generate_term_name( $_POST['module_title'] ) )
		);

		$terms = get_terms( array(
			'taxonomy' => PPY_LESSON_CAT,
			'hide_empty' => false,
		) );
		$terms_count = count( $terms );

		update_term_meta($term_id['term_id'], 'ppy_module_course', $_POST['course_id'] );
		update_term_meta($term_id['term_id'], 'menu_order', $terms_count + 1 );
		$this->_get_modules( $_POST['course_id'] );
		die();
	}

	public function delete_lesson(){
		check_ajax_referer(PPY_PREFIX . 'lessons_nonce', 'security');
		if ( empty( $_POST['lesson_id'] ) ) return;
		wp_delete_post( $_POST['lesson_id'] );
		$this->_get_lessons_by_course( $_POST['course_id'] );
		die; //the AJAX script has to die or it will return exit(0)
	}

	public function delete_module() {
		check_ajax_referer(PPY_PREFIX . 'modules_nonce', 'security');
		if ( empty( $_POST['module_id'] ) ) return;
		wp_delete_term( $_POST['module_id'], PPY_LESSON_CAT );
		$this->_get_modules( $_POST['course_id'] );
		die();
	}

	public function add_new_tier(){
		check_ajax_referer(PPY_PREFIX . 'ajax_tiers', 'security');
		global $wpdb;
		$course_id = $_POST['course_id'];
		$slug_meta = PPY_PREFIX . 'tier_slug';

		$price = Poppyz_Core::save_formatted_price( $_POST['tier_price'] );

		if ( $price != 0 && $price < 0.2 ) {
			echo 'below_minimum';
			die();
		}

		//generate a unique slug for the tier, this will be used in registration
		$guid = "";
		$already_exists = true;
		do {
			$guid = substr ( md5( microtime() ),rand( 0, 26 ), 10 );
			$already_exists = (boolean) $wpdb->get_var("
              SELECT COUNT(meta_value) FROM $wpdb->postmeta WHERE meta_key = '$slug_meta' AND meta_value = '$guid'
            ");
		} while (true == $already_exists);

		$post_arr = array (
			'post_title' =>  $_POST['tier_name'],
			'post_status' => 'publish',
			'post_type' => PPY_TIER_PT,
		);
		$post_id = wp_insert_post( $post_arr );
		add_post_meta( $post_id, PPY_PREFIX . 'tier_course', $course_id, true );
        $price = (!empty($price)) ? $price : 0;
		add_post_meta( $post_id, PPY_PREFIX . 'tier_price', $price, true );
		add_post_meta( $post_id, $slug_meta, $guid, true );

		$this->_get_tiers_by_course( $course_id );
		die();
	}

	public function clone_tier() {
		check_ajax_referer(PPY_PREFIX . 'ajax_tiers', 'security');
		global $wpdb;
		if ( empty( $_POST['tier_id'] ) || empty( $_POST['course_id'] ) ) return;

		$post_id = $_POST['tier_id'];
		$course_id = $_POST['course_id'];
		$post = get_post( $post_id );

		$current_user = wp_get_current_user();
		$new_post_author = $current_user->ID;

		/*
		 * if post data exists, create the post duplicate
		 */
		if (isset($post) && $post != null) {

			/*
			 * new post data array
			 */
			$args = array(
				'comment_status' => $post->comment_status,
				'ping_status' => $post->ping_status,
				'post_author' => $new_post_author,
				'post_content' => $post->post_content,
				'post_excerpt' => $post->post_excerpt,
				'post_name' => $post->post_name,
				'post_password' => $post->post_password,
				'post_status' => 'publish',
				'post_title' => $post->post_title . ' - Copy',
				'post_type' => $post->post_type,
				'to_ping' => $post->to_ping,
				'menu_order' => $post->menu_order
			);


			/*
			 * insert the post by wp_insert_post() function
			 */
			$new_tier_id = wp_insert_post($args);

			$custom_fields = get_post_custom( $post_id );
			foreach ( $custom_fields as $key => $values ) {
				if ( in_array( $key, array( '_edit_lock', '_edit_last', 'ppy_tier_slug' ) ) ) continue;
				foreach ( $values as $v ) {
					//wp makes serialized data serialize again when saving, so it results in a nested serialization which we don't want. To avoid this make the serialized string an array first
					if ( is_serialized( $v ) ) {
						$v = maybe_unserialize( $v );
					}
					add_post_meta( $new_tier_id, $key, $v );
				}
			}

			//generate a unique slug for the tier, this will be used in registration
			$slug_meta = PPY_PREFIX . 'tier_slug';
			$guid = "";
			$already_exists = true;
			do {
				$guid = substr ( md5( microtime() ),rand( 0, 26 ), 10 );
				$already_exists = (boolean) $wpdb->get_var("
              SELECT COUNT(meta_value) FROM $wpdb->postmeta WHERE meta_key = '$slug_meta' AND meta_value = '$guid'
            ");
			} while (true == $already_exists);

			add_post_meta( $new_tier_id, $slug_meta, $guid, true );

			$this->_get_tiers_by_course( $_POST['course_id'] );
			die; //the AJAX script has to die or it will return exit(0)

		} else {
			wp_die('Post creation failed, could not find original post: ' . $post_id);
		}
	}

	function delete_tier(){
		check_ajax_referer(PPY_PREFIX . 'ajax_tiers', 'security');
		global $wpdb;
		if ( empty( $_POST['tier_id'] ) ) return;
		wp_delete_post( $_POST['tier_id'] );
		$this->_get_tiers_by_course( $_POST['course_id'] );

		$ppy_sub = new Poppyz_Subscription();
		$ppy_payment = new Poppyz_Payment();
		$subscribers = Poppyz_Subscription::get_subscriptions_by_tier( $_POST['tier_id'] );

		foreach ( $subscribers as $subscriber ) {
			$ppy_payment->cancel_membership( $subscriber->id );
			$ppy_sub->delete_subscription( $subscriber->id, false );
		}
		die; //the AJAX script has to die or it will return exit(0)
	}

	function get_tier_total_subscription(){
		check_ajax_referer(PPY_PREFIX . 'ajax_tiers', 'security');
		global $wpdb;
		$numberOfSubscribedUsers = Poppyz_Subscription::get_subscription_total_by_tier( $_POST['tier_id'] );

        if ( $numberOfSubscribedUsers > 0 ) {
			$message = sprintf(__("Sorry, you can't remove this tier because it has %s %s. Please unsubscribe these %s first.", "poppyz"), $numberOfSubscribedUsers, _n('subscriber', 'subscribers', $numberOfSubscribedUsers), _n('subscriber', 'subscribers', $numberOfSubscribedUsers));
		} else {
            $message = "";
		}

        $tier_total_subscription_array = array( "total_subscriber" => $numberOfSubscribedUsers, "message" => $message );

		echo json_encode($tier_total_subscription_array);
		die; //the AJAX script has to die or it will return exit(0)
	}

	private function _get_lessons_by_course( $course_id ) {
		$ppy_core = new Poppyz_Core();
		$lessons = $ppy_core->get_lessons_by_course( $course_id );
		while ($lessons->have_posts()) : $lessons->the_post();
			echo "<li id='item_" . get_the_ID() . "'><div class='handle'><a href='" . get_permalink() . "'><strong>";
			the_title();
			$terms =  wp_get_object_terms( get_the_ID(), PPY_LESSON_CAT);
			if ( ! empty( $terms ) ) {
				if ( ! is_wp_error( $terms ) ) {
					global $ppy_lang;
					$lesson_terms = array();
					foreach( $terms as $term ) {
						$lesson_terms[] = esc_html( $term->name );
					}
					echo " <small>(" . __( 'Lessoncluster' ,'poppyz') . ': ' . implode(', ', $lesson_terms) . ")</small>";
				}
			}

			echo "</strong></a><div class='ppy-actions'>";
			echo "<a class='has-icon' title='" .  __( 'View' )  . "' href='" . get_permalink() . "'><span class='dashicons dashicons-visibility'></span></a>";
			echo "<a class='has-icon' title='" .  __( 'Edit' )  . "' href='" . get_edit_post_link() . "'><span class='dashicons dashicons-edit-large'></span></a>";
			echo "<a title='" . __( 'Delete' ) .  "' href='" . get_delete_post_link() . "' class='has-icon delete-lesson delete-link' lesson-id='" . get_the_ID() . "'><span class='dashicons dashicons-trash'></span></a></div>";
			echo "</div></li>";
		endwhile;
	}

	private function _get_modules( $course_id = null) {

		$terms = Poppyz_Core::get_modules_by_course( $course_id );

		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
			foreach ( $terms as $term ) {
				$term_link = get_term_link($term->term_id);
				$term_edit_link =  get_edit_term_link( $term->term_id, PPY_LESSON_CAT) ;
				if (is_plugin_active('enhanced-category-pages/enhanced-category-pages.php')) {
					global $enhanced_category;
					$ecp_id = $enhanced_category->get_first_or_create_for_category($term->term_id, PPY_LESSON_CAT);
					$term_link = get_permalink($ecp_id);
					$term_edit_link = get_edit_post_link($ecp_id);
				}
				echo "<li id='item_" . $term->term_id . "'><div class='handle'><strong><a href='$term_edit_link'>$term->name</a>";

				echo "</strong><div class='ppy-actions'><a class='has-icon' title='" . __( 'View' ) . "' href='" . $term_link . "'><span class='dashicons dashicons-visibility'></span></a>";
				echo "</strong><div class='ppy-actions'><a class='has-icon' title='" . __( 'Edit' ) . "' href='" . $term_edit_link . "'><span class='dashicons dashicons-edit-large'></span></a>";
				//var_dump( get_term_meta( $term->term_id, 'menu_order',true ) );
				echo "<a href='#' title='" . __( 'Delete' )  ."' class='delete-module delete-link' module-id='" . $term->term_id . "'><span class='dashicons dashicons-trash'></span></a></div>";
				echo "</div></li>";
			}
		}
	}

	private function _get_tiers_by_course( $course_id ) {
		$ppy_core = new Poppyz_Core();
		$tiers = $ppy_core->get_tiers_by_course( $course_id );
		$tiers->posts = array_reverse( $tiers->posts );
		$purchase_url =  get_permalink( Poppyz_Core::get_option( 'purchase_page' ) );

		if (  get_option( 'permalink_structure' ) ) {
			$purchase_url .= 'order-id/';
		} else {
			$purchase_url .= '&order-id=';
		}
		global $ppy_lang;
		while ($tiers->have_posts()) : $tiers->the_post();
			$tier_id = get_the_ID();
			$price = Poppyz_Core::format_price( get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true ) ) ;
			if (Poppyz_Core::is_donation_enabled( $tier_id )) {
				$price = 'Donation';
			}

			$numberOfSubscribedUsers = Poppyz_Subscription::get_subscription_total_by_tier( $tier_id );
			echo "<tr>";
			echo "<td><strong><a href='" . get_edit_post_link() . "' >" . get_the_title(). "</strong></a></td>";

			//echo "<td>" . Poppyz_Core::format_price( get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true ) ) . " </td> ";
			echo "<td>" . $price . " </td> ";

			echo '<td><a class="url" href="' .  $purchase_url . get_post_meta( $tier_id, PPY_PREFIX . 'tier_slug', true )  . '">' . $purchase_url . get_post_meta( $tier_id, PPY_PREFIX . 'tier_slug', true ) . '</a> <button  href="#" style="padding: 0 5px" class="copy-tier-url ppy_button et_pb_button" title="' . __('Link copied to clipboard','poppyz') . '" >' .  __('Copy','poppyz') . '</button></td>';
			echo "<td><a class='has-icon' title='" . __( 'Edit' )  . "' href='" . get_edit_post_link() . "'><span class='dashicons dashicons-edit-large'></span></a>";
			echo '<a class="has-icon clone-tier" href="#" title="' . __( 'Clone' )  . '"  tier-id="' . $tier_id . '"><span class="dashicons dashicons-admin-page"></span></a></div>';
			echo '<a class="has-icon delete-tier" href="#" title="' . __( 'Delete' )  . '"  tier-id="' . $tier_id . '" data-sub="'.$numberOfSubscribedUsers.'"><span class="dashicons dashicons-trash"></span></a></div>';
			echo "</td>";
			echo "</tr>";
		endwhile;

		return false;
	}

	function update_lesson_order(){ //save tag sent via meta box
        //fix for Divi sometimes throwing out a missing function error, need to investigate further why this happens on some sites.
        if (file_exists(ET_BUILDER_DIR . 'ab-testing.php')) {
            include_once ET_BUILDER_DIR . 'ab-testing.php';
        }
		check_ajax_referer( PPY_PREFIX . 'lessons_nonce', 'security' );
		$i = 0;
		$test = [];
		foreach ( $_POST['order'] as $order) {
			$test[] = ($order);
			if ( $order['id'] != "" ) {
				$i++;
				$post_args = array (
					'ID'           =>  $order['id'],
					'menu_order' => $i
				);
				wp_update_post( $post_args );
			}
		}

		die; //the AJAX script has to die or it will return exit(0)
	}
	function update_module_order(){ //save tag sent via meta box
		check_ajax_referer( PPY_PREFIX . 'modules_nonce', 'security' );
		$i = 0;

		foreach ( $_POST['order'] as $order) {

			if ( $order['id'] != "" ) {
				$i++;
				update_term_meta( $order['id'], 'menu_order', $i );
			}
		}
		die; //the AJAX script has to die or it will return exit(0)
	}

	public function metabox_tier_pricing() {
		global $ppy_lang;
		$post_id = get_the_ID();
		$tier_price = get_post_meta( $post_id, PPY_PREFIX .  'tier_price', true );
		$tier_discount = get_post_meta( $post_id, PPY_PREFIX .  'tier_discount', true );
		$tier_discount = ( empty($tier_discount) ) ? 0 : $tier_discount;
		$tier_initial_price = get_post_meta( $post_id, PPY_PREFIX .  'tier_initial_price', true );
		if ( !$tier_price ) $tier_price = 0;
		if ( !$tier_initial_price ) $tier_initial_price = 0;
		$membership_interval = get_post_meta( $post_id, PPY_PREFIX .  'membership_interval', true );
		$ppy_membership_timeframe = get_post_meta( $post_id, PPY_PREFIX .  'membership_timeframe', true );
		$payment_frequency = get_post_meta( $post_id, PPY_PREFIX . 'payment_frequency', true );
		if ( !$payment_frequency ) {
			$payment_frequency = 1; //don't let it be empty / 0
		}

		$payment_timeframe = get_post_meta( $post_id, PPY_PREFIX . 'payment_timeframe', true );
		$number_of_payments = get_post_meta( $post_id, PPY_PREFIX . 'number_of_payments', true );

		$initial_payment_frequency = get_post_meta( $post_id, PPY_PREFIX . 'initial_payment_frequency', true );
		$initial_payment_timeframe = get_post_meta( $post_id, PPY_PREFIX . 'initial_payment_timeframe', true );

		$tier_tax_rate = get_post_meta( $post_id, PPY_PREFIX .  'tier_tax_rate', true );
		$price_tax = get_post_meta( $post_id, PPY_PREFIX .  'price_tax', true );

		$is_plan = get_post_meta( $post_id, PPY_PREFIX . "payment_method", true ) == 'plan' ;
		$is_membership = get_post_meta( $post_id, PPY_PREFIX . "payment_method", true ) == 'membership' ;
		$is_donation = get_post_meta( $post_id, PPY_PREFIX . "payment_method", true ) == 'donation' ;

		$automatic_payment = get_post_meta( $post_id, PPY_PREFIX . "payment_automatic", true );
        $tier_tax_rate_mb = get_post_meta( $post_id, PPY_PREFIX .  'tier_tax_rate_mb', true );
		$tierVoluntaryContributionEnabled = get_post_meta( $post_id, PPY_PREFIX . "tier_voluntary_contribution", true );

		$donationAllowCustomAmount = get_post_meta( $post_id, PPY_PREFIX .  'donation_fixed_amounts', true );
		$donationFixedAmounts = get_post_meta( $post_id, PPY_PREFIX .  'donation_fixed_amounts', true );
		$donationAllowCustomAmount = get_post_meta( $post_id, PPY_PREFIX .  'donation_allow_custom_amount', true );

		$donationtitle = get_post_meta( $post_id, PPY_PREFIX . "tier_donation_title", true );
        $donationDescription = get_post_meta( $post_id, PPY_PREFIX . "tier_donation_description", true );


		$payment = new Poppyz_Payment();
		$invoice_taxrates = $payment->get_invoice_taxrates();
		$options = get_option( 'ppy_settings' );
		?>
        <div id="ppy-tier-pricing">
            <div class="ppy-field-wrapper">
                <label for="payment-method"><?php echo __( 'Payment Method' ,'poppyz'); ?></label>
                <select id="payment-method" name="<?php echo PPY_PREFIX . "payment_method" ?>">
                    <option value="standard" <?php selected( get_post_meta( $post_id, PPY_PREFIX . "payment_method", true ), 'standard') ?>><?php echo __( 'Standard Payment' ,'poppyz'); ?></option>
                    <option value="plan" <?php selected( get_post_meta( $post_id, PPY_PREFIX . "payment_method", true), 'plan') ?>><?php echo __( 'Payment Plan' ,'poppyz'); ?></option>
                    <option value="membership" <?php selected( get_post_meta( $post_id, PPY_PREFIX . "payment_method", true), 'membership') ?>><?php echo __( 'Membership' ,'poppyz'); ?></option>
                </select>
            </div>

<!--            <div class="ppy-field-wrapper">-->
            <div id="tier_price" class="ppy-field-wrapper" >
                <label for="<?php echo PPY_PREFIX .  'tier_price' ?>" id="label_tier_price"><?php echo ( $is_plan ) ? __( 'Price per payment' ,'poppyz') : __( 'Price' ,'poppyz') . ' (' . Poppyz_Core::currency_symbol() . ')'; ?> </label>
                <div class="price-with-currency">
                    <div class="price-currency">€</div>
                    <input class="short input-price" type="text" name="<?php echo PPY_PREFIX .  'tier_price' ?>" id="<?php echo PPY_PREFIX .  'tier_price' ?>" placeholder="<?php echo Poppyz_Core::currency_symbol(); ?>0" value="<?php echo Poppyz_Core::format_price( $tier_price, '' ); ?>" />
                </div>

            </div>

			<div id="tier_discount" class="ppy-field-wrapper" >
				<label for="<?php echo PPY_PREFIX .  'tier_discount' ?>" id="label_tier_price"><?php echo __( 'Promotional Discount' ,'poppyz'); ?> </label>
				<div class="price-with-currency">
					<div class="price-currency">€</div>
					<input class="short input-price" type="text" name="<?php echo PPY_PREFIX .  'tier_discount' ?>" id="<?php echo PPY_PREFIX .  'tier_discount' ?>" placeholder="<?php echo Poppyz_Core::currency_symbol(); ?>0" value="<?php echo Poppyz_Core::format_price( $tier_discount, '' ); ?>" />
				</div>
			</div>
            <div id="ppy-initial-price" <?php if ( $is_plan || $is_membership ) echo 'style="display:block;"' ?>>
                <div id="tier_discount_implementation_option" class="ppy-field-wrapper">
                    <label for="<?php echo PPY_PREFIX .  'discount_implementation_option' ?>" id="label-discount-implementation_option"><?php echo __( 'Discount Implementation' ,'poppyz'); ?> </label>
                    <select id="discount-implementation-option" name="<?php echo PPY_PREFIX . "discount_implementation_option" ?>">
                        <option value="all" <?php selected( get_post_meta( $post_id, PPY_PREFIX . "discount_implementation_option", true ), 'all') ?>><?php echo __( 'All payments' ,'poppyz'); ?></option>
                        <option value="first_payment" <?php selected( get_post_meta( $post_id, PPY_PREFIX . "discount_implementation_option", true), 'first_payment') ?>><?php echo __( 'First payment only' ,'poppyz'); ?></option>
                    </select>
                </div>
                <div class="ppy-field-wrapper">
                    <label for="<?php echo PPY_PREFIX .  'tier_initial_price' ?>" id="label_tier_initial_price"><?php echo __( 'Initial price' ,'poppyz') . ' (' . Poppyz_Core::currency_symbol() . ')'; ?> </label>
                    <div class="price-with-currency">
                        <div class="price-currency">€</div>
                        <input class="short input-price" type="text" name="<?php echo PPY_PREFIX .  'tier_initial_price' ?>" id="<?php echo PPY_PREFIX .  'tier_initial_price' ?>" placeholder="<?php echo Poppyz_Core::currency_symbol(); ?>0" value="<?php echo Poppyz_Core::format_price( $tier_initial_price, '' ); ?>" />
                    </div>
                    <p class="description"><?php echo __('This will be used as the first price of your subscription plan. The succeeding payments will use the standard price above. Leave empty or 0 to be the same as the standard price above.','poppyz') ?></p>
                </div>
                <div class="ppy-field-wrapper">
                    <label for="ppy_initial_payment_frequency"><?php echo __( 'Second payment date' ,'poppyz'); ?></label>
					<?php echo __( 'At' ,"poppy"); ?>&nbsp; <input class="short" type="number" min="1" id="ppy_initial_payment_frequency" value="<?php echo $initial_payment_frequency; ?>" name="<?php echo PPY_PREFIX . "initial_payment_frequency" ?>" />
                    <select class="short-2" id="ppy_initial_payment_timeframe" name="<?php echo PPY_PREFIX . "initial_payment_timeframe" ?>">
                        <option value="months" <?php selected( 'months', $initial_payment_timeframe ) ?>><?php echo __( 'months' ,'poppyz'); ?></option>
                        <option value="days" <?php selected( 'days', $initial_payment_timeframe ) ?>><?php echo __( 'days' ,'poppyz'); ?></option>
                        <!--<option value="years" <?php /*selected( 'years', $initial_payment_timeframe ) */?>><?php /*echo __( 'years' ); */?></option>-->
                    </select>
                </div>
            </div>

            <div id="ppy-payment-plan" <?php if ( $is_plan ) echo 'style="display:block;"' ?>>
                <div class="ppy-field-wrapper">
                    <label for="ppy_payment_frequency"><?php echo __( 'Payment Frequency' ,'poppyz'); ?></label>
					<?php echo __( 'Every' ,'poppyz'); ?>&nbsp; <input class="short" type="number" min="1" id="ppy_payment_frequency" value="<?php echo $payment_frequency; ?>" name="<?php echo PPY_PREFIX . "payment_frequency" ?>" />
                    <select class="short-2" id="ppy_payment_timeframe" name="<?php echo PPY_PREFIX . "payment_timeframe" ?>">
                        <option value="months" <?php selected( 'months', $payment_timeframe ) ?>><?php echo __( 'months' ,'poppyz'); ?></option>
                        <option value="days" <?php selected( 'days', $payment_timeframe ) ?>><?php echo __( 'days' ,'poppyz'); ?></option>
                        <!--<option value="years" <?php /*selected( 'years', $payment_timeframe ) */?>><?php /*echo __( 'years' ); */?></option>-->
                    </select>
                </div>

                <div class="ppy-field-wrapper">
                    <label for="ppy_number_of_payments"><?php echo __( 'Number of payments' ,'poppyz'); ?></label>
                    <input class="short" type="number" id="ppy_number_of_payments" min="2"  name="<?php echo PPY_PREFIX . "number_of_payments" ?>" value="<?php echo $number_of_payments; ?>" />
                </div>

                <div id="total-price">
                    <div class="ppy-field-wrapper">
                        <label><?php echo __( 'Total Price:' ,'poppyz') ?></label>
						<?php
						$total_price = (float)$tier_price * (int)$number_of_payments;
						if ( $tier_initial_price ) {
							$total_price =  (float)$tier_initial_price + (float)$tier_price * ((int)$number_of_payments - 1);
						}
						?>
                        <strong> <?php echo Poppyz_Core::format_price( $total_price ) ?></strong>
                    </div>
                </div>
            </div>

            <div id="ppy-membership" <?php if ( $is_membership ) echo 'style="display:block;"' ?>>
                <div class="ppy-field-wrapper">
                    <p><label for="ppy_membership_duration"><?php echo __( 'Interval' ,'poppyz'); ?></label>
                        <input class="short" type="number" min="1" id="ppy_membership_interval" value="<?php echo $membership_interval; ?>" name="<?php echo PPY_PREFIX . "membership_interval" ?>" />
                        <select id="ppy_membership_timeframe" name="<?php echo PPY_PREFIX . "membership_timeframe" ?>">
                            <option value="months" <?php selected( 'months', $ppy_membership_timeframe ) ?>><?php echo __( 'months' ,'poppyz'); ?></option>
                            <option value="days" <?php selected( 'days', $ppy_membership_timeframe ) ?>><?php echo __( 'days' ,'poppyz'); ?></option>
                            <!--<option value="years" <?php /*selected( 'years', $ppy_membership_timeframe ) */?>><?php /*echo __( 'years' ); */?></option>-->
                        </select>
                        <!--<input type="checkbox" value="1" id="ppy_membership_recurring" <?php /*checked( 1, $membership_recurring ); */?> name="<?php /*echo PPY_PREFIX . "membership_recurring" */?>" /> --><?php /*echo __( 'Recurring?' ); */?>
                    </p>
                </div>
            </div>


            <!--<div id="ppy-payment-type" <?php /*if ( $is_plan  || $is_membership ) echo 'style="display:block;"' */?>>
                <p><?php /*echo __( 'Direct debit (SEPA) is enabled for this tier. Poppyz always uses SEPA when paying in installments and memberships.' ); */?></p>
            </div>-->

            <div class="ppy-field-wrapper tax-rate" >
                <label for="<?php echo PPY_PREFIX .  'tier_tax_rate' ?>" id="label_tier_tax_rate"><?php echo __( 'Tax Rate' ,'poppyz'); ?> </label>
                <div class="percentage-wrapper">
                    <input class="short input-percentage" type="number" name="<?php echo PPY_PREFIX .  'tier_tax_rate' ?>" id="<?php echo PPY_PREFIX .  'tier_tax_rate' ?>" placeholder="<?php echo Poppyz_Core::get_option( 'tax_rate' ); ?>" value="<?php echo $tier_tax_rate; ?>" /><div class="percentage-icon">%</div>
                </div>
            </div>



            <div class="ppy-field-wrapper price-with-tax" >

                <label for="<?php echo PPY_PREFIX .  'price_tax' ?>"><?php echo __( 'Prices entered with tax' ,'poppyz'); ?></label>
                <select id="<?php echo PPY_PREFIX .  'price_tax' ?>" name="<?php echo PPY_PREFIX .  'price_tax' ?>">
                    <option <?php selected( $price_tax, "" ); ?>  value=""><?php echo __( 'Default' ,'poppyz'); ?></option>
                    <option <?php selected( $price_tax, "inc" ); ?> value="inc"><?php echo __( 'Yes, I will enter prices inclusive of tax','poppyz'); ?></option>
                    <option <?php selected( $price_tax, "exc" ); ?> value="exc"><?php echo __( 'No, I will enter prices exclusive of tax' ,'poppyz'); ?></option>
                </select>
                <p class="description"><?php echo __( 'If the price is 10 euros, then the total amount on the invoice under "excluding VAT" is 10 + VAT = 12.10 (in the case of VAT 21%). And 10 euros in the case of "including VAT".' ,'poppyz'); ?></p>
            </div>
        </div>
        <?php if ( ( isset( $options['mb_email'] ) && $options['mb_email'] != '' ) || ( isset( $options['mb_api_key'] ) && $options['mb_api_key'] != '' ) && $invoice_taxrates  ) : ?>
            <h2><?php echo __( 'Moneybird',"poppyz" ); ?></h2>
            <div id="ppy-moneybird">
                    <?php
						$invoiceArray = array();
                        foreach ( $invoice_taxrates as $taxrate ) {
                            if ( $taxrate->active != true || ( $taxrate->taxRateType != 'sales_invoice' && $taxrate->taxRateType != 1 ) ) continue;
                            $invoiceArray[$taxrate->id] = $taxrate->name;
                        }
                    ?>
					<?php
					echo Poppyz_Core::form_field_helper( $post_id, array(
							'name'        =>  __( 'Taxrate','poppyz'),
							'id'          => PPY_PREFIX .  'tier_tax_rate_mb',
							'type'        => 'select',
							'options'     => $invoiceArray,
							'std'         => '-1',
							'placeholder' =>  __( 'Default' ,'poppyz'),
						)
					);
					?>
            </div>
		<?php endif; ?>


        <script>

            jQuery(document).ready(function($){

                //only allow numbers and some symbols on the price
                $("#<?php echo PPY_PREFIX .  'tier_price' ?>").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter, comma, space, and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 188, 32, 190]) !== -1 ||
                        // Allow: Ctrl+A, Command+A
                        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
                        // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((/*e.shiftKey || */(e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });

                $("#post").submit(function() {
                    var price_val = $("#<?php echo PPY_PREFIX .  'tier_price' ?>").val();
                    var isMatch = price_val.startsWith(",01") || price_val.startsWith(".01") ||  price_val.startsWith('0,01') && !price_val.startsWith('0,01-');
                    if ( isMatch ) {
                        alert("<?php echo __( 'Tier Price must be greater than 0.02' ,'poppyz'); ?>")
                        return false;
                    }
                });

                $('#payment-method').change(function(){
                    if (jQuery(this).val() == 'plan') {
                        $('#tier_price').slideDown();
                        $('#label_tier_price').slideDown();
                        $('#label_tier_price').html('<?php echo __( 'Price per payment' ,'poppyz')  ?>');
                        $('#ppy-membership').slideUp();
                        $('#ppy-donation-enabled').slideDown();
                        $('#ppy-payment-plan').slideDown();
                        $('#ppy-initial-price').slideDown();
                        $('#ppy-payment-type').slideDown();
                        $('#ppy-donation').slideUp();
                        $('.tax-rate').slideDown();
                        $('.price-with-tax').slideDown();
                    } else if (jQuery(this).val() == 'membership') {
                        $('#tier_price').slideDown();
                        $('#label_tier_price').slideDown();
                        $('#label_tier_price').html('<?php echo __( 'Price' ,'poppyz')  ?>');
                        $('#ppy-payment-plan').slideUp();
                        $('#ppy-donation-enabled').slideDown();
                        $('#ppy-initial-price').slideDown();
                        $('#ppy-membership').slideDown();
                        $('#ppy-payment-type').slideDown();
                        $('.tax-rate').slideDown();
                        $('.price-with-tax').slideDown();
                    } else if (jQuery(this).val() == 'donation') {
                        $('#tier_price').slideUp();
                        $('#label_tier_price').slideUp();
                        $('#ppy-payment-plan').slideUp();
                        $('#ppy-donation-enabled').slideUp();
                        $('#ppy-initial-price').slideUp();
                        $('#ppy-membership').slideUp();
                        $('#ppy-payment-type').slideUp();
                        $('.tax-rate').slideUp();
                        $('.price-with-tax').slideUp();
                    } else {
                        $('#tier_price').slideDown();
                        $('#label_tier_price').slideDown();
                        $('#label_tier_price').html('<?php echo __( 'Price' ,'poppyz')  ?>');
                        $('#ppy-payment-plan').slideUp();
                        $('#ppy-membership').slideUp();
                        $('#ppy-payment-type').slideUp();
                        $('#ppy-initial-price').slideUp();
                        $('#ppy-donation').slideUp();
                        $('#tier_discount_implementation_option').slideUp();
                        $('#ppy-donation-enabled').slideDown();
                        $('.tax-rate').slideDown();
                        $('.price-with-tax').slideDown();

                    }
                });

                $('#ppy_membership_timeframe, #ppy_payment_timeframe').change(function(){
                    var intervalInput = jQuery(this).prev();
                    if (jQuery(this).val() === 'days' && intervalInput.val() > 365 ) {
                        alert('Value cannot be more than 365 days');
                        intervalInput.val(1);
                        intervalInput.focus();
                        return false;
                    } else if (jQuery(this).val() === 'months' && intervalInput.val() > 12 ) {
                        intervalInput.val(1);
                        alert('Value cannot be more than 12 months');
                        intervalInput.focus();
                        return false;
                    }
                });
                $('#ppy_payment_frequency, #ppy_membership_interval').blur(function(){
                    var timeframeInput = jQuery(this).next();
                    if (timeframeInput.val() === 'days' && jQuery(this).val() > 365 ) {
                        jQuery(this).val(1);
                        alert('Value cannot be more than 365 days');
                        return false;
                    } else if (timeframeInput.val() === 'months' && jQuery(this).val() > 12 ) {
                        jQuery(this).val(1);
                        alert('Value cannot be more than 12 months');
                        return false;
                    }
                });
                $('#<?php echo PPY_PREFIX .  'tier_price' ?>, #<?php echo PPY_PREFIX .  'number_of_payments' ?>, #<?php echo PPY_PREFIX .  'tier_initial_price' ?>').bind('input', function() {
                    var price = $('#<?php echo PPY_PREFIX .  'tier_price' ?>').val();
                    var initial_price = $('#<?php echo PPY_PREFIX .  'tier_initial_price' ?>').val();
                    var number_of_payments = parseInt($('#<?php echo PPY_PREFIX .  'number_of_payments' ?>').val());

					<?php if (Poppyz_Core::get_currency() == 'EUR' ) : ?>
                    price = price.replace(/[,.]/g, function (x) { return x == "," ? "." : ","; });
                    initial_price = parseFloat(initial_price.replace(/[,.]/g, function (x) { return x == "," ? "." : ","; }));
					<?php endif; ?>
                    if (initial_price > 0) {
                        number_of_payments = number_of_payments - 1;
                    }

                    var total = parseFloat(price) * number_of_payments;

                    if (initial_price > 0) {
                        total = total + parseFloat(initial_price);
                        console.log( parseFloat(total));
                    }

                    $('#total-price strong').html(total.toFixed(2));
                });

                $('#ppy_automatic_plan').click(function(){
                    alert('You can\'t change to automatic payment because there is a user that\'s subscribed to a manual payment.');
                    $(this).attr("disabled", "disabled");
                    return false;
                });
                var payment_method = $("#payment-method").val();
                if (payment_method == "membership") {
                    $(".allow_membership_cancellation").show();
                }
                $('select').on('change', function() {
                    if (this.value == "membership") {
                        $(".allow_membership_cancellation").show();
                    } else {
                        $(".allow_membership_cancellation").hide();
                    }
                });

                $("#donations_tab").click(function() {

                    var tier_price = parseInt($("#ppy_tier_price").val());
                    if(tier_price <= 0) {
                        $("#schedule_donation_wrapper").show();
                    } else {
                        $("#schedule_donation_wrapper").hide();
                    }

                });
            });

        </script>
		<?php
	}

	public function metabox_tier_donation() {
		global $ppy_lang;
		$post_id = get_the_ID();

		$tierVoluntaryContributionEnabled = get_post_meta( $post_id, PPY_PREFIX . "tier_voluntary_contribution", true );
		$donationFixedAmounts = get_post_meta( $post_id, PPY_PREFIX .  'donation_fixed_amounts', true );
		$donationAllowCustomAmount = get_post_meta( $post_id, PPY_PREFIX .  'donation_allow_custom_amount', true );
		$donationtitle = get_post_meta( $post_id, PPY_PREFIX . "tier_donation_title", true );
		$donationDescription = get_post_meta( $post_id, PPY_PREFIX . "tier_donation_description", true );

		$scheduleVoluntaryContribution = get_post_meta( $post_id, PPY_PREFIX . "tier_schedule_voluntary_contribution", true );
		$donationEmailContent = get_post_meta( $post_id, PPY_PREFIX . "tier_donation_email_content", true );
		$numberOfDaysAfterPurchase = get_post_meta( $post_id, PPY_PREFIX . "number_of_days_after_purchase", true );
        $donationEmailSubject = get_post_meta( $post_id, PPY_PREFIX . "donation_email_subject", true );

		?>
        <div id="ppy-tier-donation">
            <div class="ppy-field-wrapper" id="ppy-donation-enabled" >
                <label for="<?php echo PPY_PREFIX .  'voluntary_contribution';?>" id="label_tier_voluntary_contribution_enabled">
					<?php echo __('Voluntary contribution', 'poppyz'); ?>
                </label>
                <div class="ppy-checkbox-wrapper voluntary_contribution">
                    <div class="admin-option-radio">
                        <input type="radio" name="<?php echo PPY_PREFIX .  'tier_voluntary_contribution';?>" value="yes" <?php checked('yes', $tierVoluntaryContributionEnabled ) ;?> id="<?php echo PPY_PREFIX .  'tier_voluntary_contribution_enable';?>"> <?php echo __("Yes", "poppyz");?>
                    </div>
                    <div class="admin-option-radio">
                        <input type="radio" name="<?php echo PPY_PREFIX .  'tier_voluntary_contribution';?>" value="no" <?php checked('no', $tierVoluntaryContributionEnabled ) ;?> id="<?php echo PPY_PREFIX .  'tier_voluntary_contribution_disable';?>"> <?php echo __("No", "poppyz");?>
                    </div>
                </div>
            </div>
            <div class="ppy-field-wrapper donation-title" id="ppy-donation-title" <?php if ( $tierVoluntaryContributionEnabled == "yes" ) echo 'style="display:flex;"' ?>>
                <label for="<?php echo PPY_PREFIX .  'tier_donation_title' ?>" id="label_tier_tax_rate"><?php echo __( 'Donation Title' ,'poppyz'); ?> </label>
                <div class="percentage-wrapper">
                    <input class="long " type="text" name="<?php echo PPY_PREFIX .  'tier_donation_title' ?>" id="<?php echo PPY_PREFIX .  'tier_donation_title' ?>"  value="<?php echo $donationtitle; ?>" />
                </div>
            </div>
            <div id="ppy-donation-description" class="donation-description" <?php if ( $tierVoluntaryContributionEnabled == "yes" ) echo 'style="display:block;"' ?>>
                <div class="ppy-field-wrapper">
					<?php
					$html = "";
					$settings_editor = array(
						'textarea_rows' => 20,
						'editor_height' => 425,
						'tabindex' => 1,
						'textarea_name' => PPY_PREFIX . "tier_donation_description"
					);
					$html .= '<label class="label">' . __('Description','poppyz') . '</label>';
					ob_start();                    // start buffer capture
					wp_editor( $donationDescription, PPY_PREFIX . "tier_donation_description", $settings_editor);
					$html .= ob_get_contents(); // put the buffer into a variable
					ob_end_clean();                // end capture
					$html .= '<p class="description full"></p>';
					echo $html;
					?>
                </div>
            </div>

            <div id="ppy-donation-fixed-amount" <?php if ( $tierVoluntaryContributionEnabled == "yes" ) echo 'style="display:block;"' ?>>
                <div class="ppy-field-wrapper">
                    <label for="ppy_fixed_amounts"><?php echo __( 'Fixed amounts' ,'poppyz'); ?></label>
                    <div class="price-with-currency">
						<?php $donationFixedAmounts1 = (is_array($donationFixedAmounts) && isset($donationFixedAmounts[0])) ? $donationFixedAmounts[0] : "0";?>
                        <div class="price-currency">€</div>
                        <input class="short input-price" type="text" name="<?php echo PPY_PREFIX .  'donation_fixed_amounts[]' ?>" id="<?php echo PPY_PREFIX .  'donation_fixed_amounts' ?>" placeholder="<?php echo Poppyz_Core::currency_symbol(); ?>0" value="<?php echo Poppyz_Core::format_price( $donationFixedAmounts1, '' ); ?>" />
                    </div>
                </div>
                <div class="ppy-field-wrapper">
                    <label for="ppy_fixed_amounts"></label>
                    <div class="price-with-currency">
						<?php $donationFixedAmounts2 = (is_array($donationFixedAmounts) && isset($donationFixedAmounts[1])) ? $donationFixedAmounts[1] : "0";?>
                        <div class="price-currency">€</div>
                        <input class="short input-price" type="text" name="<?php echo PPY_PREFIX .  'donation_fixed_amounts[]' ?>" id="<?php echo PPY_PREFIX .  'donation_fixed_amounts' ?>" placeholder="<?php echo Poppyz_Core::currency_symbol(); ?>0" value="<?php echo Poppyz_Core::format_price( $donationFixedAmounts2, '' ); ?>" />
                    </div>
                </div>
                <div class="ppy-field-wrapper">
                    <label for="ppy_fixed_amounts"></label>
                    <div class="price-with-currency">
						<?php $donationFixedAmounts3 = (is_array($donationFixedAmounts) && isset($donationFixedAmounts[2])) ? $donationFixedAmounts[2] : "0";?>
                        <div class="price-currency">€</div>
                        <input class="short input-price" type="text" name="<?php echo PPY_PREFIX .  'donation_fixed_amounts[]' ?>" id="<?php echo PPY_PREFIX .  'donation_fixed_amounts' ?>" placeholder="<?php echo Poppyz_Core::currency_symbol(); ?>0" value="<?php echo Poppyz_Core::format_price( $donationFixedAmounts3, '' ); ?>" />
                    </div>
                </div>
                <div class="ppy-field-wrapper">
                    <label for="ppy_fixed_amounts"></label>
                    <div class="price-with-currency">
						<?php $donationFixedAmounts3 = (is_array($donationFixedAmounts) && isset($donationFixedAmounts[3])) ? $donationFixedAmounts[3] : "0";?>
                        <div class="price-currency">€</div>
                        <input class="short input-price" type="text" name="<?php echo PPY_PREFIX .  'donation_fixed_amounts[]' ?>" id="<?php echo PPY_PREFIX .  'donation_fixed_amounts' ?>" placeholder="<?php echo Poppyz_Core::currency_symbol(); ?>0" value="<?php echo Poppyz_Core::format_price( $donationFixedAmounts3, '' ); ?>" />
                    </div>
                </div>
            </div>
            <div id="ppy-donation-custom-amount" <?php if ( $tierVoluntaryContributionEnabled == "yes" ) echo 'style="display:block;"' ?>>
                <div class="ppy-field-wrapper">
                    <label for="ppy_donation_custom_amount"><?php echo __( 'Allow custom amount' ,'poppyz'); ?></label>
                    <div class="ppy-checkbox-wrapper voluntary_contribution">
                        <div class="admin-option-radio">
                            <input type="radio" name="<?php echo PPY_PREFIX .  'donation_allow_custom_amount';?>" value="yes" <?php checked('yes', $donationAllowCustomAmount ) ;?> id="<?php echo PPY_PREFIX .  'donation_custom_amount';?>"> <?php echo __("Yes", "poppyz");?>
                        </div>
                        <div class="admin-option-radio">
                            <input type="radio" name="<?php echo PPY_PREFIX .  'donation_allow_custom_amount';?>" value="no" <?php checked('no', $donationAllowCustomAmount ) ;?> id="<?php echo PPY_PREFIX .  'donation_custom_amount';?>"> <?php echo __("No", "poppyz");?>
                        </div>
                    </div>
                </div>
            </div>

			<?php
			$imageUrl = get_post_meta($post_id, PPY_PREFIX . 'tier_donation_image_url', true);
			$attachementId = get_post_meta($post_id, PPY_PREFIX . 'tier_donation_image', true);

			?>
			<?php if ( empty($imageUrl)) :?>
                <div id="ppy-donation-image" <?php if ( $tierVoluntaryContributionEnabled == "yes" ) echo 'style="display:block;"' ?>>
                    <div class="ppy-field-wrapper">
                        <label for="ppy_donation_custom_amount"><?php echo __( 'Donation Image' ,'poppyz'); ?></label>
<!--                        <input type="file" name="--><?php //echo PPY_PREFIX .  'donation_image';?><!--"  id="donation-upload-image"/>-->
                        <input id="donation-upload-image" type="text" size="36" name="my_image" value="" />
                        <input id="upload_image_button" class="button" type="button" value="Upload Image" />
                    </div>
                </div>
                <div id="ppy-donation-image-cropping" <?php if ( $tierVoluntaryContributionEnabled == "yes" ) echo 'style="display:block;"' ?>>
                    <div class="ppy-field-wrapper">
                        <label for="ppy_donation_custom_amount"></label>
                        <input type="hidden" id="tier_id" value="<?php echo $post_id;?>" />
                        <div class="donation-image-crop-wrapper">
                            <div id="uploaded-image"></div>
                        </div>
                    </div>
                </div>

			<?php endif;?>
			<?php
			$imageUrl = get_post_meta($post_id, PPY_PREFIX . 'tier_donation_image_url', true);
			$attachementId = get_post_meta($post_id, PPY_PREFIX . 'tier_donation_image', true);
			?>
			<?php if ( isset($imageUrl) && !empty($imageUrl)) :?>
                <div class='products-image-wrapper' id='tier_donation_image_url_wrapper' <?php if ( $tierVoluntaryContributionEnabled == "yes" ) echo 'style="display:block;"' ?>>
                    <div class="ppy-field-wrapper">
                        <label for="ppy_donation_custom_amount"><?php echo __( 'Donation Image' ,'poppyz'); ?></label>
                        <div class='products-image'>
                            <img src='<?php echo $imageUrl;?>'/>
                        </div>
                        <div class='products-image-action'>
                            <a href='#' post-id='<?php echo $post_id;?>' attachment-id='<?php echo $attachementId;?>' id='tier_donation_image_url' class='delete-donation-image' >Delete</a>
                        </div>
                    </div>
                </div>
			<?php endif;?>
            <div id="schedule_donation_wrapper">
                <h2><?php echo __( 'Schedule sending tier donation link (For free tier only)',"poppyz" ); ?></h2>
                <div class="ppy-field-wrapper" id="ppy-scheduled-donation" >
                    <label for="<?php echo PPY_PREFIX .  'scheduled_donation';?>" id="label_tier_scheduled_donation_enabled">
                        <?php echo __('Schedule voluntary contribution', 'poppyz'); ?>
                    </label>
                    <div class="ppy-checkbox-wrapper voluntary_contribution">
                        <div class="admin-option-radio">
                            <input type="radio" name="<?php echo PPY_PREFIX .  'tier_schedule_voluntary_contribution';?>" value="yes" <?php checked('yes', $scheduleVoluntaryContribution ) ;?> id="<?php echo PPY_PREFIX .  'tier_schedule_voluntary_contribution_enable';?>"> <?php echo __("Yes", "poppyz");?>
                        </div>
                        <div class="admin-option-radio">
                            <input type="radio" name="<?php echo PPY_PREFIX .  'tier_schedule_voluntary_contribution';?>" value="no" <?php checked('no', $scheduleVoluntaryContribution ) ;?> id="<?php echo PPY_PREFIX .  'tier_schedule_voluntary_contribution_disable';?>"> <?php echo __("No", "poppyz");?>
                        </div>
                    </div>
                </div>
                <div class="scheduled-voluntary-contribution" <?php if ( $scheduleVoluntaryContribution == "yes" ) echo 'style="display:block;"' ?>>
                    <div class="ppy-field-wrapper donation-title" id="ppy-number-of-days-after-purchase" <?php if ( $tierVoluntaryContributionEnabled == "yes" ) echo 'style="display:flex;"' ?>>
                        <label for="<?php echo PPY_PREFIX .  'donation_email_subject' ?>" id="label_tier_tax_rate"><?php echo __( 'Email subject' ,'poppyz'); ?> </label>
                        <div class="percentage-wrapper">
                            <input class="long " type="text" name="<?php echo PPY_PREFIX .  'donation_email_subject' ?>" id="<?php echo PPY_PREFIX .  'donation_email_subject' ?>"  value="<?php echo $donationEmailSubject; ?>" />
                        </div>
                    </div>
                    <div id="ppy-donation-email-content" class="donation-email-content" >
                        <div class="ppy-field-wrapper">
                            <?php
                            $html = "";
                            $settings_editor = array(
                                'textarea_rows' => 20,
                                'editor_height' => 425,
                                'tabindex' => 1,
                                'textarea_name' => PPY_PREFIX . "tier_donation_email_content"
                            );
                            $html .= '<label class="label">' . __('Email content','poppyz') . '</label>';
                            $html .= '<div class="schedule-donation-editor">';
                            ob_start();                    // start buffer capture
                            wp_editor( $donationEmailContent, PPY_PREFIX . "tier_donation_email_content", $settings_editor);
                            $html .= ob_get_contents(); // put the buffer into a variable
                            ob_end_clean();                // end capture
                            $html .= '<p class="description full">Shortcodes: [account-link], [tier-link], [tier], [course], [course-link], [first-name], [last-name], [user-email], [customer-id],  [tier-purchase-url]</p>';
                            $html .= '</div>';
                            echo $html;
                            ?>
                        </div>
                    </div>
                    <div class="ppy-field-wrapper donation-title" id="ppy-number-of-days-after-purchase" <?php if ( $tierVoluntaryContributionEnabled == "yes" ) echo 'style="display:flex;"' ?>>
                        <label for="<?php echo PPY_PREFIX .  'number_of_days_after_purchase' ?>" id="label_tier_tax_rate"><?php echo __( 'Number of days after purchase' ,'poppyz'); ?> </label>
                        <div class="percentage-wrapper">
                            <input class="short " type="number" name="<?php echo PPY_PREFIX .  'number_of_days_after_purchase' ?>" id="<?php echo PPY_PREFIX .  'number_of_days_after_purchase' ?>"  value="<?php echo $numberOfDaysAfterPurchase; ?>" />
                        </div>
                        <p><?php echo __("Days after purchase that the tier will be sent for donation.", "poppyz");?></p>
                    </div>


                </div>
            </div>
        </div>




        <script>

            jQuery(document).ready(function($){



            });

        </script>
		<?php
	}

	public function metabox_lesson_links( $post_id ) {
		$post_id = isset($_GET["post"]) ? $_GET["post"] : '';
		$course_id = get_post_meta( $post_id, PPY_PREFIX . "lessons_course", true );

		//get the next lesson link
		$ppy_core = new Poppyz_Core();
		global $ppy_lang;
		$lessons = $ppy_core->get_lesson_ids_by_course( $course_id );
		if ( count( $lessons ) > 0 ) {
			echo '<ul class="poppyz-links">';
			echo "<li class='course-title'><strong>" . __('Course','poppyz') . ': ' .  get_the_title( $course_id ) . "</strong> <a href='" . get_edit_post_link( $course_id ) . "'>" . __('Edit','poppyz')  . " </a></li>";
			foreach ( $lessons as $lesson ) {
				if ( $lesson == $post_id ) continue;
				echo '<li><strong>' . get_the_title( $lesson ) . "</strong> <a href='" . get_edit_post_link( $lesson ) . "'>" . __('Edit','poppyz') . "</a></li>";
			}
			echo '</ul>';
		}
	}

	public function metabox_tier_links( $post_id ) {
		$post_id = isset($_GET["post"]) ? $_GET["post"] : '';
		$course_id = get_post_meta( $post_id, PPY_PREFIX .  "tier_course", true );

		$ppy_core = new Poppyz_Core();
		global $ppy_lang;
		$tiers = $ppy_core->get_tier_ids_by_course( $course_id );

		if ( count( $tiers ) > 0 ) {
			echo "<a class='button' href='" . Poppyz_Core::get_tier_purchase_url( $post_id ) . "'>" . __( 'View Tier' ,'poppyz') . "</a>";
			echo '<ul>';
			echo "<li class='course-title'><strong>" . __('Course','poppyz') . ': ' .  get_the_title( $course_id ) . "</strong> <a href='" . get_edit_post_link( $course_id ) . "'>" . __('Edit','poppyz')  . " </a></li>";
			foreach ( $tiers as $tier ) {
				if ( $tier == $_GET['post'] ) continue;
				echo '<li><strong>' . get_the_title( $tier ) . "</strong> <a href='" . get_edit_post_link( $tier ). "'>" . __('Edit','poppyz') . "</a></li>";

			}
			echo '</ul>';
		}
	}
	/**
	 * Get users by tiers
	 *
	 * @param $tierId
	 * @return void
	 */
	public function getUsersByTier($tierId) {
		global $wpdb;
		$subs_table = $wpdb->prefix . 'course_subscriptions';
		$sql = $wpdb->prepare( "SELECT user_id
                FROM $subs_table
                WHERE tier_id = %d
                AND status = %s",
			$tierId, 'on'
		);

		return $wpdb->get_results($sql);
	}

	/**
     * Owner window content.
     *
	 * @param $post_id
	 * @return false|void
	 */
	public function meta_box_tier_students($post_id) {
		$users = $this->getUsersByTier($post_id);
		if ( !isset($_GET['post']) ) return false;
		if ( $users ) {
			echo '<ul>';
			foreach ( $users as $user ) {
				$user = get_userdata( $user->user_id );
				if ( $user )
					echo '<li><a href="' . get_edit_user_link( $user->ID ) . '">' . $user->first_name . ' ' . $user->last_name . ' (' . $user->user_email . ')</a></li>';
			}
			echo '</ul>';
		}
	}

}