<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      0.5.0
 *
 * @package    Poppyz
 * @subpackage Poppyz/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the sM Courses, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Poppyz
 * @subpackage Poppyz/admin
 * @author     Socialmedium <info@socialmedium.nl>
 */
class Poppyz_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    0.5.0
     * @access   private
     * @var      string    $poppyz    The ID of this plugin.
     */
    private $poppyz;

    /**
     * The version of this plugin.
     *
     * @since    0.5.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    0.5.0
     * @var      string    $poppyz       The name of this plugin.
     * @var      string    $version    The version of this plugin.
     */
    public function __construct( $poppyz, $version ) {
        $this->poppyz = $poppyz;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the Dashboard.
     *
     * @since    0.5.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Poppyz_Admin_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Poppyz_Admin_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

		wp_enqueue_style ( 'jquery-ui-css', PPY_DIR_URL . 'admin/css/croppie.min.css' );
        wp_enqueue_style( $this->poppyz . '_global', PPY_DIR_URL . 'admin/css/poppyz-admin-global.css', array(), $this->version, 'all' );

        if (!$this->is_plugin_screen()) return;



        wp_enqueue_style ( 'jquery-ui-demo', PPY_DIR_URL . 'admin/css/jquery-ui-demo.css' );
        wp_enqueue_style ( 'open-sans', 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap' );

        if ( 'classic' == get_user_option( 'admin_color') )
            wp_enqueue_style ( 'jquery-ui-classic', PPY_DIR_URL . 'admin/css/jquery-ui-classic.css' );
        else
            wp_enqueue_style ( 'jquery-ui-fresh', PPY_DIR_URL . 'admin/css/jquery-ui-fresh.css' );

        if ( isset( $_GET['page']) && ($_GET['page'] == 'poppyz-students' ||  $_GET['page'] == 'poppyz-students-progress' || $_GET['page'] == 'poppyz-statistics' || $_GET['page'] == 'poppyz-overdue') ) {
            wp_enqueue_style( 'dataTables',  'https://cdn.datatables.net/v/dt/dt-1.10.24/r-2.2.7/datatables.min.css', array(), $this->version, 'all' );
            //wp_enqueue_style( 'dataTables-responsive',  PPY_DIR_URL . 'admin/css/responsive.dataTables.min.css', 'all' );
          //  wp_enqueue_style( 'tablesorter-scroller',  '//cdn.datatables.net/scroller/2.0.3/css/scroller.dataTables.min.css', array(), $this->version, 'all' );

        }

        if ( isset( $_GET['page']) && $_GET['page'] == 'poppyz-coupons' ) {
            wp_enqueue_style( 'select2-css', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css', array(), '4.1.0-rc.0');
            wp_enqueue_script( 'select2-js', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', 'jquery', '4.1.0-rc.0');
        }



        wp_enqueue_style( $this->poppyz, PPY_DIR_URL . 'admin/css/poppyz-admin.css', array(), $this->version, 'all' );
        wp_enqueue_style( "poppyz-admin-custom-css" , PPY_DIR_URL . 'admin/css/custom.css', array(), $this->version, 'all' );
  
    }


    /**
     * Register the JavaScript for the dashboard.
     *
     * @since    0.5.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Poppyz_Admin_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Poppyz_Admin_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        if (!self::is_plugin_screen()) return;

        wp_enqueue_script( 'jquery-ui-core' );
        wp_enqueue_script( 'jquery-ui-accordion' );
        wp_enqueue_script( 'jquery-ui-autocomplete' );
        wp_enqueue_script( 'jquery-ui-datepicker' );
        wp_enqueue_script( 'jquery-ui-dialog' );
        wp_enqueue_script( 'jquery-ui-progressbar' );
        wp_enqueue_script( 'jquery-ui-slider' );
        wp_enqueue_script( 'jquery-ui-tabs' );
        wp_enqueue_script( 'jquery-ui-sortable' );
        wp_enqueue_script( 'jquery-ui-draggable' );
        wp_enqueue_script( 'jquery-ui-tooltip' );

        wp_enqueue_script( 'jquery-ui-demo', PPY_DIR_URL . 'admin/js/jquery-ui-demo.js', array( 'jquery-ui-core' ) );

        wp_enqueue_script( 'nestedSortable', PPY_DIR_URL . 'admin/js/jquery.mjs.nestedSortable.js', array( 'jquery-ui-core' ), $this->version, true );

		wp_enqueue_script( 'croppie', PPY_DIR_URL . 'admin/js/croppie.js', array( 'jquery-ui-core' ), $this->version, true );

        wp_enqueue_script( $this->poppyz, PPY_DIR_URL . 'admin/js/poppyz-admin.js', array( 'jquery' ), $this->version, true );


        if ( isset( $_GET['page']) && ($_GET['page'] == 'poppyz-statistics' || $_GET['page'] == 'poppyz-overdue') )
            wp_enqueue_script ( 'google-charts', 'https://www.gstatic.com/charts/loader.js' );

        if ( isset( $_GET['page']) && ($_GET['page'] == 'poppyz-students' ||  $_GET['page'] == 'poppyz-students-progress' || $_GET['page'] == 'poppyz-statistics' || $_GET['page'] == 'poppyz-overdue') ) {
            wp_enqueue_script ( 'dataTables',  'https://cdn.datatables.net/v/dt/dt-1.10.24/r-2.2.7/datatables.min.js' );
            //wp_enqueue_script ( 'dataTables-scroller',  '//cdn.datatables.net/scroller/2.0.3/js/dataTables.scroller.min.js' );
            //wp_enqueue_script ( 'dataTables-responsive',  PPY_DIR_URL . 'admin/js/dataTables.responsive.min.js', array('jquery') );
            //wp_enqueue_script ( 'dataTables-responsive-jquery',  PPY_DIR_URL . 'admin/js/responsive.jqueryui.js', array('jquery') );
        }

        $scriptData = array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'plugin_url' => PPY_DIR_URL,
        );

        wp_localize_script($this->poppyz,'SMC',$scriptData );
        wp_enqueue_script($this->poppyz);

		global $ppy_lang;
		wp_localize_script( $this->poppyz, 'ppy_object',
			array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'prefix' => PPY_PREFIX,
				'site_url' => site_url(),
				'ppy_ajax_nonce' => wp_create_nonce( 'ppy_ajax_nonce'),
				'messages' => array (
					'coupon_required' => __( 'Coupon code is required.' ,'poppyz'),
					'no_coupon' => __( 'I don\'t have a coupon.' ,'poppyz'),
				)
			)
		);
    }

    function global_js_variables(){
        ?>
        <script type='text/javascript'>
            var ajaxurl = <?php echo json_encode(  admin_url( "admin-ajax.php" ) ) ?>;
            var ajaxnonce = <?php echo json_encode( wp_create_nonce( "itr_ajax_nonce" ) ) ?>;
            var smPoppyz = <?php echo json_encode( array(
                'site_url' => site_url(),
                'plugin_url' => PPY_DIR_URL,
             ) ) ?>;
        </script>

        <?php
    }

    public function admin_init() {
        add_image_size( 'invoice-logo', 250, 150, false );
    }

    /**
     * Determine if the admin screen belongs to this plugin
     *
     * @since 0.6.0
     * @var string    @screen    The screen to check
     * @return    bool    Returns true if on the plugin's admin screens.
     */
    public static function is_plugin_screen( $screen = null ) {
        if ( !$screen ) {
            $screen = get_current_screen();
            $screen = $screen->id;

        }

        $plugin_screens = array( PPY_COURSE_PT,  PPY_LESSON_PT, PPY_TIER_PT, 'ppyv_product', 'edit-ppyv_product' ,'toplevel_page_poppyz', 'profile', 'user-edit' );

        //we have to add this because the screen id changes per language
        $is_page = false;
        $is_post = false;
        if ( isset( $_GET['page'] ) ) {
            $is_page =  $_GET['page'] == 'poppyz-students' || $_GET['page'] == 'poppyz-options' || $_GET['page'] == 'poppyz-coupons' || $_GET['page'] == 'poppyz-invoices' || $_GET['page'] == 'poppyz-statistics' || $_GET['page'] == 'poppyz-overdue' || $_GET['page'] == 'poppyz-students-progress';
        }
        if ( isset ( $_GET['post_type'] ) ) {
            $is_post = $_GET['post_type'] == PPY_LESSON_PT;
        }
        return in_array( $screen, $plugin_screens ) || $is_page || $is_post;
    }


    public function admin_notices() {
        $options = get_option('ppy_settings');

        $license = isset($options['license_key']) ?  $options['license_key'] : '';
        $mollie = isset($options['mo_api_key']) ?  $options['mo_api_key'] : '';
		$mollieTestAccount = isset($options['mo_api_test_key']) ?  $options['mo_api_test_key'] : '';
        $autorespond = isset($options['secret_key']) ?  $options['secret_key'] : '';
		$return_page = isset($options['return_page']) ?  $options['return_page'] : '';


        $permission_failed = false;
        if ( isset( $options['permission_failed'] ) && $options['permission_failed'] == 1 ) {
            $permission_failed = true;
        }

        global $ppy_lang;

        //check moneybird
        $empty_mb = empty( $options['mb_admin_id'] ) || empty( $options['mb_api_key'] );

        if ( empty( $license ) || ( empty( $autorespond ) && ( empty( $mollie ) || $permission_failed ) ) || empty($mollieTestAccount) || (empty($return_page) || $return_page < 0) ) {
            $class = "error";
            $message = '';
            if (empty( $license ) ) {
                $message = '<strong>Poppyz</strong>: ' . sprintf( __( "To use all the plugin features you must first activate your license key <a href='%s'>here</a>.",'poppyz' ), admin_url( 'admin.php?page=poppyz-options#activation' ) );
            }
			if ( empty( $autorespond ) && empty( $mollie ) && empty( $mollieTestAccount )) {
				$message .=  '<strong>Poppyz</strong>: ' . sprintf( __( "To enable payment please enter your Mollie API Key <a href='%s'>here</a>.",'poppyz' ), admin_url( 'admin.php?page=poppyz-options#payment-options' ) );
			} else if ( empty( $mollie ) ) {
                $message .=  '<strong>Poppyz</strong>: ' . sprintf( __( "Add Mollie API key to enable payments. Click <a href='%s'>here</a>.",'poppyz' ), admin_url( 'admin.php?page=poppyz-options#payment-options' ) );
            } else if ( empty( $mollieTestAccount ) ) {
				$message .=  '<strong>Poppyz</strong>: ' . sprintf( __( "To enable test payment option for admins, please enter your Mollie API Test Key <a href='%s'>here</a>.",'poppyz' ), admin_url( 'admin.php?page=poppyz-options#payment-options' ) );
			}

            if ( $permission_failed ) {
                $message .=  '<strong>Poppyz</strong>: ' . sprintf( __( "Failed to create folder 'poppyz' inside the wp-content/uploads directory. Please make sure that you have the right permissions and click <a href='%s'>here</a> to try again." ,'poppyz'), admin_url( 'admin.php?page=poppyz-options&poppyz-create-folder' ) );
            }

            if ($message) echo "<div class='notice-" . $class . " poppyz-notice dashicons-before'>$message</div>";
        }

		if ( empty($return_page) || $return_page < 0 ) {
			$return_page_message =  '<strong>Poppyz</strong>: ' . sprintf( __( "Return Page is required. Please click <a href='%s'>here</a> to set the Rerturn Page." ,'poppyz'), admin_url( 'admin.php?page=poppyz-options#plugin-pages' ) );
			echo "<div class='notice-" . $class . " poppyz-notice dashicons-before'>$return_page_message</div>";

		}

        $screen = get_current_screen();
        //notify the admin when a the tier that's being edited has no mailchimp list assigned
        if( $screen->id == 'ppy_tier' ) {
            if ( isset( $_GET['post'] ) ) {
                $tier_id = $_GET['post'];

                if ( isset( $_GET['hide_tier_notice'] ) ) {
                    update_post_meta( $tier_id, PPY_PREFIX . 'hide_notice', true );
                }
                $hide_notice = get_post_meta( $tier_id, PPY_PREFIX . 'hide_notice', true );
                $mailchimp_list  = get_post_meta( $tier_id, PPY_PREFIX . 'mailchimp_list', true );
                $mailerlite_list  = get_post_meta( $tier_id, PPY_PREFIX . 'mailerlite_list', true );
                $aweber_list  = get_post_meta( $tier_id, PPY_PREFIX . 'aweber_list', true );
                $av_list  = get_post_meta( $tier_id, PPY_PREFIX . 'av_list', true );
                if ( empty ( $mailchimp_list ) && empty ( $mailerlite_list ) && empty( $aweber_list )  && empty( $av_list ) && !$hide_notice ) {
                    echo '<div class="poppyz-notice notice-info dashicons-before"><strong>Poppyz</strong>: ' . __( 'This tier does not have a list assigned. The students of this course won\'t be subscribed to any newsletter.','poppyz') . ' ' . sprintf( __( '<a href="%s"><small>Hide</small></a>' ,'poppyz'), admin_url( 'post.php?post='. $tier_id .'&action=edit&hide_tier_notice=1' ) ) . '</div>';
                }
            }
        }

        if ( isset( $_GET['hide_expiring_one_week_notice'] ) ) {
            Poppyz_Core::save_option( 'hide_expiring_one_week_notice', true );
        }
        $hide_expiring_one_week_notice = Poppyz_Core::get_option('hide_expiring_one_week_notice' );
        $interval = false;
        if ( false === ( $interval_transient = get_transient( 'expires_in_one_week' ) ) ) {
            $license_data = Poppyz_Core::get_license_data();
            if ( $license_data && $license_data['expiration_date'] != 'lifetime' ) {
                $target = new DateTime( $license_data['expiration_date'] );
                $origin  = new DateTime( date('Y-m-d' ) );
                $interval = $origin->diff($target);
                $interval = $interval->format('%R%a');
            }
            set_transient( 'expires_in_one_week', $interval, 12 * HOUR_IN_SECONDS );
        }
        //$interval_transient = get_transient( 'expires_in_one_week' );
        if (!$hide_expiring_one_week_notice && $interval_transient && $interval_transient <= 7 && $interval_transient >= 0) {

            global $wp;
            $query = '';
            if ($wp->query_vars) {
                $query = add_query_arg( $wp->query_vars );
            }
            echo '<div class="poppyz-notice notice-warning dashicons-before"><strong>Poppyz</strong>: ' . __( 'Your license is about to expire in 1 week. If you are subscribed to a recurring payment then this warning can be disregarded. You can prolong your license <a href="https://www.simonelevie.nl/membership/">here</a>.','poppyz') . ' ' . sprintf( __( '<a href="%s"><small>Hide</small></a>' ), $query) . '</div>';
        }

        $expired = false;
        if ( false === ( $expired_license_transient = get_transient( 'license_has_expired' ) ) ) {
            $license_data = Poppyz_Core::get_license_data();
            if (isset($license_data['status']) && $license_data['status'] == 'expired') {
               $expired = true;
               Poppyz_Core::delete_option( 'license_status' );
            }
            set_transient( 'license_has_expired', $expired, 0.5 * HOUR_IN_SECONDS );
        }

        if ( $expired_license_transient == true ) {
            echo '<div class="poppyz-notice notice-warning dashicons-before">
                    <strong>Poppyz</strong>: ' .
                __( 'Your Poppyz license has ended. <br />
                    You can no longer edit your online academy. Current subscriptions will remain active. <br />
                    Fortunately, you can easily renew your license. And you can always cancel your membership with a notice period of one month.  <br /> <br /> 
                    You can renew the Poppyz license <a href="https://www.simonelevie.nl/membership/">here</a>. <br /> <br /> 
                    You can choose to renew with or without access to MPOP.<br /> 
                    If you still see this message after renewing your license, make sure that the license is activated on the plugin settings.<br />
If you have any questions, please email matthijs@simonelevie.nl
                    ','poppyz') .
                '</div>';
        }

        if ( isset( $_GET['upgrade_poppyz_db'] ) ) {
            require_once (PPY_DIR_PATH . 'includes/class-poppyz-activator.php');
            Poppyz_Activator::_build_database();
            echo '<div class="poppyz-notice notice-success dashicons-before"><strong>Poppyz</strong>: ' .  sprintf( __( 'Database upgrade success.','poppyz' ) ) . '</div>';
        }

        if( $this->update_needed() ){
            echo '<div class="poppyz-notice notice-warning dashicons-before"><strong>Poppyz</strong>: ' .  sprintf( __( 'Database upgrade needed. <a href="%s">Click here to upgrade the database.</a>','poppyz' ), admin_url( 'admin.php?page=poppyz-options&upgrade_poppyz_db=1' ) ) . '</div>';
        }
		if ($screen->id == 'courses_page_poppyz-options') {
			$ppy_payment = new Poppyz_Payment();
			$ppy_payment->_initialize_mollie();
			$methods = $ppy_payment->mollie->methods->allActive();
            $methodArray = [];
            foreach ( $methods as $method ) {
				if( str_contains($method->id, "sepa") ) {
					array_push($methodArray, $method->id);
				}
			}
            if ( empty($methodArray) ) {
				echo '<div class="poppyz-notice notice-warning dashicons-before"><strong>Poppyz</strong>: ' .  sprintf( __( 'SEPA is not enabled.','poppyz' ) ) . '</div>';
			}
		}

    }

    /**
     * Add plugin pages
     *
     * @since 0.6.0
     */
    public function  add_menu_pages() {
        global $ppy_lang;
        global $submenu;
        add_menu_page( 'Poppyz', __( 'Poppyz','poppyz' ), 'manage_options', 'poppyz', array( $this, 'display_dashboard' ), '', 40 );

        add_submenu_page( 'poppyz', __( 'Students','poppyz' ),  __( 'Students','poppyz' ), 'manage_options', 'poppyz-students', array( $this, 'display_students') );
        //add_submenu_page( 'poppyz', __( 'Students Progress' ),  __( 'Students Progress' ), 'manage_options', 'poppyz-students-progress', array( $this, 'display_students_progress') );
        add_submenu_page( 'poppyz', __( 'Invoices','poppyz' ),  __( 'Invoices','poppyz' ), 'manage_options', 'poppyz-invoices', array( $this, 'display_invoices') );
		add_submenu_page( 'poppyz', __( 'Overdue','poppyz' ),  __( 'Overdue','poppyz' ), 'manage_options', 'poppyz-overdue', array( $this, 'display_overdue') );
        add_submenu_page( 'poppyz', __( 'Coupons','poppyz' ),  __( 'Coupons','poppyz' ), 'manage_options', 'poppyz-coupons', array( $this, 'display_coupons') );
        add_submenu_page( 'poppyz', __( 'Statistics','poppyz' ),  __( 'Statistics' ,'poppyz'), 'manage_options', 'poppyz-statistics', array( $this, 'display_statistics') );
        add_submenu_page( 'poppyz', __( 'Settings' ,'poppyz'), __( 'Settings' ,'poppyz'), 'manage_options', 'poppyz-options', array( $this, 'display_settings') );
        $submenu['poppyz'][0][0] =  __( 'Courses','poppyz' );
    }

    /**
     * Add Poppyz menu items to the admin bar
     *
     * @param $admin_bar
     */
    public function admin_bar_item( $admin_bar ) {

        // Only show to admins.
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }
        $admin_bar->add_node(
            array(
                'parent' => 'site-name',
                'id'     => 'ppy-poppyz',
                'title'  =>  __( 'Poppyz','poppyz' ),
                'href'  => admin_url('admin.php?page=poppyz'),
                'meta'   => array(
                    'title' =>  __( 'Poppyz','poppyz' ),
                ),
            )
        );

        $admin_bar->add_node(
            array(
                'parent' => 'ppy-poppyz',
                'id'     => 'ppy-courses',
                'title'  =>  __( 'Courses','poppyz' ),
                'href'  => admin_url('admin.php?page=poppyz'),
                'meta'   => array(
                    'title' =>  __( 'Courses','poppyz' ),
                ),
            )
        );

        $admin_bar->add_node(
            array(
                'parent' => 'ppy-poppyz',
                'id'     => 'ppy-students',
                'title'  =>  __( 'Students','poppyz' ),
                'href'  => admin_url('admin.php?page=poppyz-students'),
                'meta'   => array(
                    'title' =>  __( 'Students','poppyz' ),
                ),
            )
        );

        $admin_bar->add_node(
            array(
                'parent' => 'ppy-poppyz',
                'id'     => 'ppy-invoices',
                'title'  => __( 'Invoices','poppyz' ),
                'href'  => admin_url('admin.php?page=poppyz-invoices'),
                'meta'   => array(
                    'title' => __( 'Invoices','poppyz' ),
                ),
            )
        );

        $admin_bar->add_node(
            array(
                'parent' => 'ppy-poppyz',
                'id'     => 'ppy-poppyz-coupons',
                'title'  =>  __( 'Coupons','poppyz' ),
                'href'  => admin_url('admin.php?page=poppyz-coupons'),
                'meta'   => array(
                    'title' =>  __( 'Coupons','poppyz' ),
                ),
            )
        );

        $admin_bar->add_node(
            array(
                'parent' => 'ppy-poppyz',
                'id'     => 'ppy-poppyz-statistics',
                'title'  =>  __( 'Statistics','poppyz' ),
                'href'  => admin_url('admin.php?page=poppyz-statistics'),
                'meta'   => array(
                    'title' =>  __( 'Statistics','poppyz' ),
                ),
            )
        );

		$admin_bar->add_node(
			array(
				'parent' => 'ppy-poppyz',
				'id'     => 'ppy-poppyz-overdue',
				'title'  =>  __( 'Overdue','poppyz' ),
				'href'  => admin_url('admin.php?page=poppyz-overdue'),
				'meta'   => array(
					'title' =>  __( 'Statistics','poppyz' ),
				),
			)
		);

        $admin_bar->add_node(
            array(
                'parent' => 'ppy-poppyz',
                'id'     => 'ppy-poppyz-options',
                'title'  =>  __( 'Options','poppyz' ),
                'href'  => admin_url('admin.php?page=poppyz-options'),
                'meta'   => array(
                    'title' =>  __( 'Options','poppyz' ),
                ),
            )
        );
    }

    /**
     * Remove the default custom post type pages
     *
     * @since 0.6.0
     */
    public function remove_menu_pages() {
        remove_submenu_page( 'edit.php?post_type=' . PPY_COURSE_PT, 'post-new.php?post_type=' . PPY_COURSE_PT );
        remove_submenu_page( 'edit.php?post_type=' . PPY_INVOICE_PT, 'post-new.php?post_type=' . PPY_INVOICE_PT );
    }

    public static function display_dashboard() {
        Poppyz_Core::license_check();
        echo Poppyz_Template::get_template('admin/display.php');
    }
    public function display_students() {
        if (empty($_GET['student-id'])) {
            echo Poppyz_Template::get_template('admin/students.php');
        } else {
            echo Poppyz_Template::get_template('admin/student.php');
        }
    }
    /*public function display_students_progress() {
        require_once( PPY_DIR_PATH . 'admin/partials/poppyz-admin-students-progress.php' );
    }*/
    public function display_invoices() {
        require_once (PPY_DIR_PATH . 'admin/class-poppyz-invoice-table.php');
        echo Poppyz_Template::get_template('admin/invoices.php');
    }
    public function display_coupons() {
	    Poppyz_Core::license_check();
        echo Poppyz_Template::get_template('admin/coupon.php'); 
    }
    public function display_statistics() {
	    Poppyz_Core::license_check();
        echo Poppyz_Template::get_template('admin/statistics.php'); 
    }
	public function display_overdue() {
		Poppyz_Core::license_check();
		echo Poppyz_Template::get_template('admin/overdue.php');
	}
    public function display_settings() {
        echo Poppyz_Template::get_template('admin/settings.php'); 
    }

    public function register_settings(){
        //settings page
        register_setting('ppy_settings', 'ppy_settings', array( $this, 'settings_validate' ) );

        //flush rules on the settings page
        if ( get_site_transient( PPY_PREFIX . 'settings_visited' ) == true ) {
            flush_rewrite_rules();
            set_site_transient( PPY_PREFIX . 'settings_visited', false );
        }
        if ( isset( $_GET['poppyz-create-folder'] ) ) {
            $options_saved = get_option( 'ppy_settings' );
            if ( isset( $options_saved['permission_failed'] ) && Poppyz_Core::protect_attachments() ) {
                unset ( $options_saved['permission_failed'] );
                update_option( 'ppy_settings' , $options_saved );
            }
        }
    }

    public function process_settings_page() {
        check_ajax_referer(PPY_PREFIX . 'ajax_mc', 'security');
        $optionsClass = new \Poppyz\Includes\Options();
        $optionsClass->setDefaultTextOptions(true);
    }

    function settings_validate( $args ) {
        global $ppy_lang;

        //check license, if invalid dont save any data and mark license status as invalid
	    if ( empty( $args['license_key'] ) ) {
		    $args['license_status'] = '';
		    add_settings_error( 'ppy_settings', 'license_empty', __( 'Poppyz: Your license key is empty. ','poppyz'), 'error' );
		    return $args;
	    } else {

		    $response = Poppyz_Subscription::check_license( $args['license_key'] );

		    // make sure the response came back okay
		    if ( is_wp_error( $response ) ){
			    $args['license_status'] = 'invalid';
			    add_settings_error( 'ppy_settings', 'verify_license', __( 'Poppyz: Cannot verify license.','poppyz' ), 'error' );
			    return $args;
		    }

		    // decode the license data
		    $license_data = json_decode( wp_remote_retrieve_body( $response ) );

		    if ( !isset( $license_data->license ) || $license_data->license != 'valid' ) {
			    $args['license_status'] =  $license_data->license;
			    add_settings_error( 'ppy_settings', 'invalid_license', __( 'Poppyz: Your plugin license is invalid.' ,'poppyz'), 'error' );
			    return $args;
		    }

	    }

        if( !empty( $args['ty_email_from'] ) && !is_email( $args['ty_email_from'] ) ){
            //add a settings error because the email is invalid and make the form field blank, so that the user can enter again
            $args['ty_email_from'] = '';
            add_settings_error( 'ppy_settings', 'invalid_email', __( 'Please enter a valid email','poppyz' ), $type = 'error' );
        }

        //Validate mailchimp api key
        if ( isset( $args['mc_api_key'] ) ) {
            $api_key = $args['mc_api_key'];
            //only validate if the value changed
            if ( $api_key != Poppyz_Core::get_option( 'mc_api_key' ) ) {
                $data = array(
                    'apikey'        => $api_key
                );
                //get the string after the last dash on the api key to determine the datacenter
                $index = strrpos($api_key, "-");
                if( $index === false ) { //in case no dash was found
                    $args['mc_api_key'] = '';
                    add_settings_error( 'ppy_settings', 'invalid_api_key', __( 'MailChimp: API Key Invalid','poppyz' ), $type = 'error' );
                } else {
                    $dc = substr( $api_key, $index + 1  );
                    $url = 'https://' . $dc . '.api.mailchimp.com/3.0/';
                    $target = 'lists';
                    $auth = base64_encode( 'user:'. $api_key );
                    $json_data = json_encode( $data );

                    $ch = curl_init();

                    curl_setopt( $ch, CURLOPT_URL, $url . $target );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                        'Authorization: Basic '.$auth ) );
                    curl_setopt( $ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0' );
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                    curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );

                    //curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );

                    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data );

                    $result = curl_exec( $ch );
                    curl_close( $ch );

                    //check if the returned json is contains the lists or an error
                    $result_array = json_decode( $result );
                    if ( !isset( $result_array->lists ) ) {
                        add_settings_error( 'ppy_settings', 'invalid_api_key', __( 'MailChimp: API Key Invalid','poppyz' ), $type = 'error' );
                    }
                }
            }
        }

        //validate aweber authorization code
        if ( !empty( $args['aw_code'] ) && $args['aw_code'] != Poppyz_Core::get_option( 'aw_code' ) ) {

            try {
                require_once(PPY_DIR_PATH . 'includes/aweber_api/aweber_api.php');
                $auth = AWeberAPI::getDataFromAweberID( $args['aw_code'] );
                list($args['aw_consumer_key'], $args['aw_consumer_secret'], $args['aw_access_key'], $args['aw_access_secret']) = $auth;
                if ( empty($args['aw_consumer_key']) ||
                    empty($args['aw_consumer_secret']) ||
                    empty($args['aw_access_key']) ||
                    empty($args['aw_access_secret']) ) {
					add_settings_error( 'ppy_settings', 'invalid_api_key', 'AWeber: API Key Invalid'  );
				}
            }
            catch(AWeberOAuthDataMissing $e) {
                add_settings_error( 'ppy_settings', 'invalid_api_key', 'AWeber: ' . $e->getMessage() );
            }
        }
        //validate mollie api key
        if ( !empty( $args['mo_api_key'] ) && $args['mo_api_key'] != Poppyz_Core::get_option( 'mo_api_key' ) ) {
            if ( !preg_match( '/^(live|test)_\w+$/',   $args['mo_api_key'] ) )  {
                $args['mo_api_key'] = '';
                add_settings_error( 'ppy_settings', 'invalid_api_key', __( 'Mollie: API Key Invalid','poppyz' ), $type = 'error' );
            }
        }

		//validate mollie api test key
		if ( !empty( $args['mo_api_test_key'] ) && $args['mo_api_test_key'] != Poppyz_Core::get_option( 'mo_api_test_key' ) ) {
			if ( !preg_match( '/^(test)_\w+$/',   $args['mo_api_test_key'] ) )  {
				$args['mo_api_test_key'] = '';
				add_settings_error( 'ppy_settings', 'invalid_api_test_key', __( 'Mollie: API Test Key Invalid','poppyz' ), $type = 'error' );
			}
		}
        //validate moneybird credentials
        $mb_v2_active = Poppyz_Core::get_option( 'mb_v2_active' ) ;
        if (
            !$mb_v2_active &&
            !empty( $args['mb_client_name'] ) && $args['mb_client_name'] != Poppyz_Core::get_option( 'mb_client_name' )  ||
            !empty( $args['mb_email'] ) && $args['mb_email'] != Poppyz_Core::get_option( 'mb_email' )  ||
            !empty( $args['mb_password'] ) && $args['mb_password'] != Poppyz_Core::get_option( 'mb_password' )
        )
        {
            //if there is a change on the account, reset the invoice profile id to its default: 1
            //we do this to prevent saving of another mb account's profile ID to different mb account.
            $args['invoice_profile'] = '1';

            require_once (PPY_DIR_PATH . 'includes/Moneybird/ApiConnector.php');
            spl_autoload_register('Moneybird\ApiConnector::autoload');

            Moneybird\HttpClient::$verifyHostAndPeer = false;

            // Create a Transport
            $transport = new Moneybird\HttpClient();
            $transport->setAuth(
                $args['mb_email'],
                $args['mb_password']
            );

            try {
                $connector = new Moneybird\ApiConnector(
                    $args['mb_client_name'],
                    $transport,
                    new Moneybird\XmlMapper() // create a mapper
                );
                $connector->requestsLeft();
            } catch (\Moneybird\Exception $e) {
                $args['mb_client_name'] = '';
                $args['mb_email'] = '';
                $args['mb_password'] = '';
                add_settings_error( 'ppy_settings', 'invalid_credentials', __( 'Moneybird: Credentials are Invalid - API Error: ','poppyz' ) . $e->getMessage(), $type = 'error' );
            }
        }

        if (  ! empty( $args['delete_invoice_logo'] ) ) {
            Poppyz_Core::delete_option('invoice_logo');
            $args['invoice_logo'] = '';
        }

        //make sure you return the args
        return $args;
    }



    public function update_needed() {
        global $wpdb;
        return (
            !Poppyz_Core::column_exists( 'payment_type' )
            || !Poppyz_Core::column_exists( 'payments_made' )
            || !Poppyz_Core::column_exists( 'version' )
            || !Poppyz_Core::column_exists( 'vatshifted' )
            || !Poppyz_Core::table_exists( $wpdb->prefix . 'course_subscriptions_payments' )
            || !Poppyz_Core::column_exists( 'vat_out_of_scope' )
            || !Poppyz_Core::column_exists( 'initial_amount' )
            || !Poppyz_Core::column_exists( 'invoice_type', $wpdb->prefix . 'course_subscriptions_payments' )
            || !Poppyz_Core::column_exists( 'amount', $wpdb->prefix . 'course_subscriptions_payments' )
			|| !Poppyz_Core::column_exists( 'payment_mode' )
        );
    }


    /**
     * Hide the Add New button on the Lesson and Tier edit screens, since they should not be added directly.
     *
     * @since 0.6.0
     */
    function admin_head() {
        if( PPY_LESSON_PT == get_post_type() || PPY_TIER_PT == get_post_type() ) {
            echo '<style type="text/css">.add-new-h2,.page-title-action{display:none;}</style>';
        }
    }

    public function get_tiers_by_course_ajax() {
		$courseId = $_POST['course_id'];
		$ppy_core = new Poppyz_Core();
		$tiers = $ppy_core->get_tiers_by_course( $_POST['course_id'] );

		$tiers->posts = array_reverse( $tiers->posts );

        $tiersArray = [];
		foreach ( $tiers->posts as $tier) {
			$tier_id = $tier->ID;
			$tiersArray[] = array(
				"id" => $tier_id,
				"title" => $tier->post_title
			);
		}
        $data = array("tiers_array" =>  $tiersArray, "number_of_item" => count($tiers->posts));
		echo json_encode($data);
		wp_die();

	}

    /**
     * Add a plugin header on plugin admin pages
     *
     * @since 0.6.0
     */
    public function admin_header() {
        if (!$this->is_plugin_screen()) return;
        global $ppy_lang;
		global $pagenow;
		$ppy_invoice = new Poppyz_Invoice();

        $optionsAdditionalHeader = "";
        if ( isset($_GET['page']) && $_GET['page'] == "poppyz-options" ) {
			//for Settings page header.
			$titlePage = __('Settings', 'poppyz');
			$optionsAdditionalHeader = '
			<div class="ppy-banner-row">
                    <div class="ppy-banner-child-row"><h1>'.$titlePage.'</h1></div>
                    <div class="ppy-banner-child-row">
                        <label for="submit" id="ppy-settings-button-update" class="has-icon dashicons-before ppy_button">Save Changes</label>
                    </div>
                </div>
                ';
		} else if( isset($_GET['page']) && $_GET['page'] == "poppyz-students" ) {
			//for Students page header.
			$ppy_core = new Poppyz_Core();
			$courses = $ppy_core->get_courses();
            $selectedTier = "";
			if ( !empty($_GET['select-course']) && $_GET['select-course'] != "all") {
				$studentsPost = get_post( $_GET['select-course'] );
				$courseOptions = "<option value='".$_GET['select-course']."'>".$studentsPost->post_title."</option>";
				$courseOptions .= "<option value='all'>All Course</option>";

				if( isset( $_GET['select-tier'] ) && $_GET['select-tier'] != "all" ) {
					$tierDisable = "";
					$tierPost =  get_post( $_GET['select-tier'] );
					$selectedTier .= "<option value='".$_GET['select-tier']."'>".$tierPost->post_title."</option>";

					$tiers = $ppy_core->get_tiers_by_course( $_GET['select-course'] );
					foreach ( $tiers->posts as $tier) {
						$tier_id = $tier->ID;
						$selectedTier .= "<option value='".$tier_id."'>".$tier->post_title."</option>";
					}
				} else {
					$tierDisable = "";
					$selectedTier .= "<option value='all'>All Tiers</option>";

					$tiers = $ppy_core->get_tiers_by_course( $_GET['select-course'] );
					foreach ( $tiers->posts as $tier) {
						$tier_id = $tier->ID;
						$selectedTier .= "<option value='".$tier_id."'>".$tier->post_title."</option>";
					}
				}

			} else {
				$courseOptions = "<option value='all'>All Course</option>";
				$tierDisable = "disabled";
				$selectedTier .= "<option value='all'>All Tiers</option>";
			}
            foreach ( $courses->posts as $course) {
				$courseOptions .= "<option value='".$course->ID."'>".$course->post_title."</option>";
			}
			$ajax_nonce = wp_create_nonce(  PPY_PREFIX . "ajax_subscription" );
			$titlePage = __('Students', 'poppyz');
			$optionsAdditionalHeader = '
			<div class="ppy-banner-row">
                <div class="ppy-banner-child-row"><h1>'.$titlePage.'</h1></div>
            </div>
            <div class="ppy-banner-row banner-search-bar">
                <div class="ppy-banner-child-row">
                    <form action="" method="get">
                        <input type="hidden" name="page" value="poppyz-students" />
                        <input type="hidden" name="filter" value="1">
                        <select id="select-course" name="select-course" class="">
                            '.$courseOptions.'
                        </select>
                        <select id="select-tier" name="select-tier" class="" '.$tierDisable.'>
                            '.$selectedTier.'
                        </select>
                        
                        <input type="submit" value="'. __( 'Filter','poppyz' ).'">
                    </form>
                </div>
                <div class="ppy-banner-child-row">
                    <form action="#" method="get">
                        <input type="hidden" name="page" value="poppyz-students"/>
                        <input type="text" name="search_user" value="" id="student-search-by-name" placeholder="'. __( 'Search by name','poppyz' ).'" />
                        
                        <input type="submit" value="'. __( 'Search','poppyz' ).'">
                    </form>
                </div>
            </div>
            <script>
                //student filter ajax
                jQuery(document).ready(function($){
                    $("#select-course").on("change", function() {
                        var courseId = $("#select-course").val();
                       
                        var courseId = $(this).val();
                        var data = {
                            action: "get_tiers_by_course_ajax",
                            security: "'.$ajax_nonce.'",
                            course_id: courseId
                            };
                        if (courseId == "all") {
                            $("#select-tier").empty();
                            $("#select-tier").append($("<option>", {
                                                value: "all",
                                                text : "All"
                                            }));
                            $("#select-tier").prop("disabled", true);
                        } else {
                            $.ajax ({
                                type: "POST",
                                url: ajaxurl,
                                data: data,
                                success: function(response) {
                                    $("#select-tier").empty();
                                    $("#select-tier").append($("<option>", {
                                            value: "all",
                                            text : "All"
                                        }));
                                    var object = jQuery.parseJSON( response );
                                    if ( object.number_of_item != 0 ) {
                                        $("#select-tier").prop("disabled", false);
                                        $.each(object.tiers_array, function (i, object) {
                                            console.log(object);
                                            $("#select-tier").append($("<option>", {
                                                value: object.id,
                                                text : object.title
                                            }));
                                        });
                                        $(".select-tier").empty().append(response);
                                    } else {
                                        alert("Course empty");
                                    }
                                    
                                }
                            });
                        }
                        
                    });
                    
                    
                });
            </script>
                ';
		} else if ( isset($_GET['page']) && $_GET['page'] == "poppyz-invoices" ) {
			//for Invoices page header.
			$titlePage = __('Invoices', 'poppyz');
			$optionsAdditionalHeader = '
			<div class="ppy-banner-row">
                    <div class="ppy-banner-child-row"><h1>'.$titlePage.'</h1></div>
                    <div class="ppy-banner-child-row">
                        
                    </div>
                </div>
                ';
		} else if ( isset($_GET['page']) && $_GET['page'] == "poppyz-coupons" ) {
			//for Coupon page header.
			$titlePage = __('Coupons', 'poppyz');
			$optionsAdditionalHeader = '
			<div class="ppy-banner-row">
                    <div class="ppy-banner-child-row"><h2>'.$titlePage.'</h2></div>
                    <div class="ppy-banner-child-row">
                        <a href="'.esc_url(add_query_arg(array('ppy-action' => 'add-coupon'))).'" class="add-new-h2 ppy_button">'.__( 'Add New' ,'poppyz').'</a>
                    </div>
                </div>
                ';
		} else if ( isset($_GET['page'] ) && $_GET['page'] == "poppyz" ) {
			//for Courses page header.
			$titlePage = __('Courses', 'poppyz');
			$optionsAdditionalHeader = '
			<div class="ppy-banner-row">
                    <div class="ppy-banner-child-row"><h1>'.$titlePage.'</h1></div>
                    <div class="ppy-banner-child-row">
                        <a href="'.admin_url( 'post-new.php?post_type=' . PPY_COURSE_PT ).'" class="add-new-h2 ppy_button">Add New</a>
                    </div>
                </div>
                ';
		} else if ( isset($_GET['page']) && $_GET['page'] == "poppyz-statistics" ) {
            //for Statistics page header.
			$titlePage = __('Statistics', 'poppyz');

			$optionsAdditionalHeader = '
			<div class="ppy-banner-row">
                    <div class="ppy-banner-child-row"><h1>'.$titlePage.'</h1></div>
                    <div class="ppy-banner-child-row total_earnings topbar-total_earnings">
                        <span class="total">'. __( 'Earnings: ','poppyz'). '<span id="header_total_earnings"></span></span>
                    </div>
                </div>
                ';
		}else if ( isset($_GET['page']) && $_GET['page'] == "poppyz-overdue" ) {
			//for Statistics page header.
			$titlePage = __('Overdue', 'poppyz');
			$date_in_last_12_months = date('Y-m-d', strtotime('-12 month', strtotime(date('Y-m-d'))));
			$overdue_start_date = ( isset( $_GET['from_date'] ) ) ? strtotime($_GET['from_date']) : $date_in_last_12_months;
			$overdue_end_date = ( isset( $_GET['to_date'] ) ) ? strtotime($_GET['to_date']) : date('Y-m-d');
			$overdue = Poppyz_Statistics::get_total_overdue( $overdue_start_date, $overdue_end_date, true );
			$optionsAdditionalHeader = '
			<div class="ppy-banner-row">
                    <div class="ppy-banner-child-row"><h1>'.$titlePage.'</h1></div>
                    <div class="ppy-banner-child-row topbar-total_earnings">
                        <span class="total ">'. __( 'Total overdue: ','poppyz'). '<span>' . Poppyz_Core::format_price($overdue['total_overdue_amount']).'</span></span>
                    </div>
                    
                </div>
                ';
		}
		if ( $pagenow != 'user-edit.php' && $pagenow != 'profile.php' ) {
            echo '
            <div class="ppy-banner-wrapper">
                <div class="ppy-banner">
                    <div class="ppy-banner-row  logo-row">
                        <div class="ppy-banner-child-row">
                            <img class="logo" src="' . PPY_DIR_URL  . '/admin/images/poppyz-logo-2.0.png" alt="Poppyz" />
                            
                        </div>
                        <div class="ppy-banner-child-row">
                            <a class="second documentation-button" href="' . PPY_WEBSITE . '/documentation/" target="_blank">' . __( 'Documentation','poppyz' ).'</a>
                        </div>
                    </div>
                    '.$optionsAdditionalHeader.'
                </div>
            </div>';
		}
    }

    /**
     * Adds one or more classes to the body tag in the dashboard.
     *
     * @link https://wordpress.stackexchange.com/a/154951/17187
     * @param  String $classes Current body classes.
     * @return String          Altered body classes.
     */
    public function admin_body_class( $classes ) {
        if ($this->is_plugin_screen()) {
            $screen = get_current_screen();
            return "$classes poppyz-admin";
        }
        return $classes;
    }

    function admin_profile_fields( $user ) {
        if ( !current_user_can( 'manage_options' )  ) return;
        $ajax_nonce = wp_create_nonce(  PPY_PREFIX . "ajax_subscription" );
        $ppy_core = new Poppyz_Core();
        $ppy_subs = new Poppyz_Subscription();
        global $ppy_lang;
        $courses = $ppy_core->get_courses();
        $subs = $ppy_subs->get_subscriptions_by_user( $user->ID, 'on' );

        ?>
        <script>
            jQuery(document).ready(function($){
                $('.date-picker').datepicker({ dateFormat: 'dd-mm-yy' });

                //populate the tier based on the selected course
                $( document ).on( 'change', 'select.select-course', function() {
                    var course_id = $(this).val();
                    var data = {
                        action: 'populate_tiers',
                        security: '<?php echo $ajax_nonce; ?>',
                        course_id: course_id
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            $(".select-tier").empty().append(response);
                        }
                    });
                });

                $('.select-courses').each(function() {
                    $courses = $(this); // memorize $(this)
                    $tiers = $courses.next('select'); // find a sibling to $this.
                    $courses.change(function($courses) {
                        return function() {
                            var course_id = $courses.val();
                            var data = {
                                action: 'populate_tiers',
                                security: '<?php echo $ajax_nonce; ?>',
                                course_id: course_id
                            };
                            $.ajax ({
                                type: 'POST',
                                url: ajaxurl,
                                data: data,
                                success: function(response) {
                                    $tiers.empty().append(response);
                                }
                            });
                        }
                    }($courses));
                });

            });
        </script>
        <h3>Poppyz Plugin</h3>

       <div id="subscription-form-wrapper" class="admin-form">
        <table class="form-table">

            <tr>
                <th><label><?php echo __( 'Tiers' ,'poppyz'); ?></label></th>
                <td>
                    <?php if ($subs) {
                        echo '<table class="assign-subscription poppyz-form">';
                        echo '<tr>
                                <th> ' . __( 'Course' ,'poppyz') . ' </th>
                                <th> ' . __( 'Tier','poppyz' ) . ' </th>
                                <th> ' . __( 'Subscription date','poppyz' ) . ' </th>
                                <th> ' . __( 'Payments made','poppyz' ) . ' </th>
                                <th> ' . __( 'Actions' ,'poppyz') . ' </th>
                            </tr>';
                        foreach ($subs as $s)  {

                            echo '<tr><td><select name="assign-tier[' . $s->id . '][course]" class="select-courses">';
                            echo '<option value="-1">' . __('Select...','poppyz') .  '</option>';
                            //get select data
                            if ( $courses->have_posts() ) {
                                while ( $courses->have_posts() ) {
                                    $courses->the_post();
                                    echo '<option ' . selected($s->course_id, get_the_ID()) . ' value="' . get_the_ID() . '">' . get_the_title() . '</option>';
                                }
                            }
                            echo '</select></td>';

                            $tiers = $ppy_core->get_tiers_by_course( $s->course_id  );

                            if ( $tiers->have_posts() ) {
                                echo '<td><select name="assign-tier[' . $s->id . '][tier]" class="select-tiers">';
                                while ($tiers->have_posts()) {
                                    $tiers->the_post();
                                    echo '<option ' . selected($s->tier_id, get_the_ID()) . ' value="' . get_the_ID() . '">' . get_the_title() . '</option>';
                                }
                                echo '</select></td>';
                            }
                            $date = strtotime( $s->subscription_date );
                            $payments_made =  $s->payments_made ;
                            $date = date( 'd-m-Y', $date );
                            echo '<td><input type="text" class="date-picker" name="assign-tier[' . $s->id . '][date]" value="' . $date . '" /></td>';
                            echo '<td><input type="text"  name="assign-tier[' . $s->id . '][payments_made]" value="' . $payments_made . '" /></td>';
                            echo '<td><input type="checkbox" name="assign-tier[' . $s->id . '][delete]" value="1" /> ' . __('Delete','poppyz') . '</td>';
                            echo '<td><input type="hidden" name="assign-tier[' . $s->id . '][id]" value="' . $s->tier_id . '" /></td></tr>';
                        }
                        echo "<hr />";
                        echo '</table>';
                    }

                    echo '<div class="poppyz-form">';
                    $this->subscription_form(false);
                    echo '</div>';
                    ?>
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            jQuery("#subscription-form-wrapper.admin-form #add_subscription").click(function(e) {
                // Check if the select element is empty
                if (jQuery(".select-tier").val() === "" || jQuery(".select-tier").val() === null || jQuery(".select-tier").val() === "-1") {
                    // If the select is empty, show an alert and prevent form submission
                    alert("Please select a value for the course and tier.");
                    // Prevent the default form submission
                    e.preventDefault();
                } else {
                    // If the select is filled, submit the form
                    return true;
                }
            });
        </script>
       </div>
        <?php

        require_once( PPY_DIR_PATH . 'public/class-poppyz-user.php' );
        $custom_fields = Poppyz_User::custom_fields();
        global $ppy_lang;
        $html = '<div class="poppyz-admin-profile-fields form-table">';
        foreach ( $custom_fields as $name => $value ) {

            $exclude = apply_filters( 'ppy_custom_fields_exclude', array() );
            $field_value =  get_user_meta( $user->ID, PPY_PREFIX . $value, true );
            if ( $value == 'country' ) {
                $html .= '<fieldset><label for="reg_country">'. __('Country','poppyz') . '*</label>' . Poppyz_Core::country_select( 'reg_country', $field_value ) . '</fieldset>';
                continue;
            }
            if ( $value == 'company' ) { //company should not be required
                $html .= ' <fieldset><label for="reg-' . $value .'">' . $name . '</label><input name="reg_' . $value .'" type="text"
                                   value="' . $field_value . '"
                                   id="reg-' . $value .'"/>
                               </fieldset>';
                continue;
            }
            if ( !in_array( $value, $exclude) ) {
                $html .= ' <fieldset><label for="reg-' . $value .'">' . $name  . '</label><input name="reg_' . $value .'" type="text" 
	                                   value="' . $field_value . '"
	                                   id="reg-' . $value .'"/>
	                               </fieldset>';
            }
            $html = apply_filters( 'ppy_custom_fields_html', $html, $field_value, $name, $value );
        }
        $html .= '</div>';
        echo $html;
    }

	public function getUserByKeyword()
	{
		check_ajax_referer(PPY_PREFIX . 'ajax_subscription', 'security');
        $keyword = $_POST['search'];
		$args = array (
			'search'         => '*' . esc_attr( $keyword ) . '*',
			'search_columns' => array( 'user_login', 'user_email', 'user_nicename', 'display_name' )
		);
		$user_query = new WP_User_Query( $args );
		$users = $user_query->get_results();
		$userArray = [];
		foreach ( $users as $user ) {
			$userArray[] = array(
				"value" => 	$user->ID,
				"label" => $user->first_name." ".$user->last_name
			);
		}

		echo  json_encode($userArray);
        wp_die();

	}

    public static function subscription_form( $users = true, $search = false, $add = true ) {
        global $ppy_lang;

        $courses = Poppyz_Core::get_courses();

        if ( $users ) {
            $users = get_users( array( 'fields' => array( 'ID', 'user_login') ) );
			echo '<div class="field-wrapper"><input type="text" name="select-user-input" id="select-user-input" class=" ppy-input" placeholder="Select user" >';
            echo '<input type="hidden" id="select-user-hidden" name="select-user">';
			echo '</div>';
        }

        echo '<div class="field-wrapper"><select name="select-course" class="select-course ppy-input" >';
        echo '<option value="-1">' . __('Select course','poppyz') .  '</option>';
        //get select data
        if ( $courses->have_posts() ) {
            while ( $courses->have_posts() ) {
                $courses->the_post();
                echo '<option value="' . get_the_ID() . '">' . get_the_title() . '</option>';
            }
        }
		echo '</select></div>';

		echo '<div class="field-wrapper"><select name="select-tier" class="select-tier ppy-input">';
		echo '<option value="-1">' . __('Select tier','poppyz') . '</option>';
		echo '</select></div>';

		echo '<div class="field-wrapper"><input type="text" class="date-picker ppy-input" name="subscribe-course-date" autocomplete="off" placeholder="' . __('Subscription date','poppyz') . '" /></div>';
		$current_screen = get_current_screen();
		if ( $search ) echo '<input type="submit" id="search-student" class="ppy_button" name="filter" value="' . __('Search','poppyz') . '" id="filter_subscription" />';
		if ( isset( $_GET['page'] ) && $_GET['page'] == 'poppyz-students' ) echo '<div class="field-wrapper label ppy-field ppy-checkbox-wrapper add-student-nav-wrapper">
    <label for="yes-ty-email" class="yes-ty-email switch">
        <input type="checkbox" value="1" name="yes-ty-email" id="yes-ty-email" class="yes_no_button" checked="checked" /><div class="slider round dashicons-before "></div></label>' . __( 'Send Thank You mail?','poppyz' ) . '</div>';
		if ( $add ) echo '<input class="ppy_button" type="submit" name="add" value="+ ' . __('Add','poppyz' ) . '" id="add_subscription" />';
		?>

        <?php
        $ajax_nonce = wp_create_nonce(  PPY_PREFIX . "ajax_subscription" );
        ?>

        <script>
            jQuery(document).ready(function($){
                $('.date-picker').datepicker({ dateFormat: 'yy-mm-dd' });

                //populate the tier based on the selected course
                $( document ).on( 'change', 'select.select-course', function() {
                    var selectCourse = $(this);
                    var course_id = $(this).val();
                    var data = {
                        action: 'populate_tiers',
                        security: '<?php echo $ajax_nonce; ?>',
                        course_id: course_id
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            selectCourse.next(".select-tier").empty().append(response);
                        }
                    });
                });

                $('.select-courses').each(function() {
                    $courses = $(this); // memorize $(this)
                    $tiers = $courses.next('select'); // find a sibling to $this.
                    $courses.change(function($courses) {
                        return function() {
                            var course_id = $courses.val();
                            var data = {
                                action: 'populate_tiers',
                                security: '<?php echo $ajax_nonce; ?>',
                                course_id: course_id
                            };
                            $.ajax ({
                                type: 'POST',
                                url: ajaxurl,
                                data: data,
                                success: function(response) {
                                    $tiers.empty().append(response);
                                }
                            });
                        }
                    }($courses));
                });

                $( '#select-user-input' ).autocomplete({
                    source: function( request, response ) {
                        var data = {
                            action: 'get_users_by_keyword',
                            security: '<?php echo $ajax_nonce; ?>',
                            search: request.term
                        };
                        // Fetch data
                        $.ajax({
                            type: 'post',
                            url: ajaxurl,
                            dataType: "json",
                            data: data,
                            success: function( data ) {
                                response( data );
                            }
                        });
                    },
                    select: function (event, ui) {
                        // Set selection
                        $('#select-user-input').val(ui.item.label); // display the selected text
                        $('#select-user-hidden').val(ui.item.value); // save selected id to input
                        return false;
                    },
                    focus: function(event, ui){
                        $('#select-user-input').val(ui.item.label); // display the selected text
                        $('#select-user-hidden').val(ui.item.value); // save selected id to input
                        return false;
                    },
                });

            });
        </script>

        <?php
    }

	/**
	 * add class to body tag to identify the name of the page.
	 * @param $classes
	 * @return mixed|string
	 */
	public function addAdminBodyClass( $classes ) {
		if ( isset($_GET['page']) ) {
			$classes .= " ".$_GET['page']."-body";
		} else if ( isset($_GET['post_type']) ) {
			$classes .= " ".$_GET['post_type']."-body";
		}
		return $classes;
	}
    public function populate_tiers() {
        check_ajax_referer(PPY_PREFIX . 'ajax_subscription', 'security');

        if ( empty( $_POST['course_id'] ) ) return;

        $ppy_core = new Poppyz_Core();
        $tiers = $ppy_core->get_tiers_by_course( $_POST['course_id'] );

        if ( $tiers->have_posts() ) {
            global $ppy_lang;
	        echo '<option value="-1">' . __('None','poppyz') . '</option>';
            while ($tiers->have_posts()) {
                $tiers->the_post();
                echo '<option value="' . get_the_ID() . '">' . get_the_title() . '</option>';
            }
        }
        die;
    }

    public function populate_user_subscriptions() {
        check_ajax_referer(PPY_PREFIX . 'ajax_subscription', 'security');
        if ( empty( $_POST['user_id'] ) ) return;
        $user_id = $_POST['user_id'];
        $ppy_sub = new Poppyz_Subscription();
        $subs = $ppy_sub->get_subscriptions_by_user( $user_id );
        if ( $subs ) {
            foreach ( $subs as $sub ) {
                if ( $sub->version == 2 ) continue;
                echo '<option value="' . $sub->id . '">' . get_the_title( $sub->tier_id ) . '</option>';
            }
        } else {
            echo '<option value="-1">' . __('None') . '</option>';
        }

        wp_die();
    }

    public function admin_save_profile_fields( $user_id ) {
        if ( !current_user_can( 'manage_options', $user_id ) )
            return false;

        if ( $_POST['select-tier'] != -1 && $_POST['select-course'] != -1 ) {
            $send_mail = !isset( $_POST['yes-ty-email'] );
	        Poppyz_Subscription::add_subscription( $user_id, $_POST['select-tier'], '', $_POST['subscribe-course-date'], $send_mail, 'admin' );
        }

        if ( isset( $_POST['assign-tier'] ) && count( $_POST['assign-tier'] ) > 0 ) {
            foreach ( $_POST['assign-tier'] as $id => $tier) {
                if ( isset( $tier['delete'] ) && $tier['delete'] == 1) {
                    Poppyz_Subscription::update_subscription( 'delete', $id );
                } elseif ( $tier['course'] != -1 && $tier['tier'] != -1 && !empty( $tier['date'] ) ){
                    $data['course_id'] = $tier['course'];
                    $data['tier_id'] = $tier['tier'];
                    $data['date'] = $tier['date'];
                    $data['payments_made'] = $tier['payments_made'];
                    //$data['payment_status'] = 'admin';
                    Poppyz_Subscription::update_subscription( 'update', $id, $data );
                }

            }
        }

        require_once( PPY_DIR_PATH . 'public/class-poppyz-user.php' );
        $custom_fields = Poppyz_User::custom_fields();
        foreach ( $custom_fields as $cf ) {
            if ( isset( $_POST['reg_' . $cf] ) ) {
                $value = $_POST['reg_' . $cf];
                $value = apply_filters( 'ppy_custom_fields_value', $value, $cf );
                update_user_meta( $user_id, PPY_PREFIX . $cf, $value );
                do_action( 'ppy_custom_fields_save', $value, $cf, $user_id );
            }
        }

    }

    public function populate_invoice_data() {
        check_ajax_referer( PPY_PREFIX . 'ajax_subscription', 'security' );
        if (  empty( $_POST['user_id'] )|| empty( $_POST['tier_id'] ) ) return;
        $user_id = $_POST['user_id'];
        $tier_id = $_POST['tier_id'];
        $user = get_user_by( 'id', $user_id );
        $data = array();
        $data['product'] = get_the_title( $tier_id );

        $data['amount'] = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true );
        $data['amount'] =  !empty( $data['amount'] ) ?  $data['amount'] : 0 ;
        $data['first_name'] = $user->first_name;
        $data['last_name'] = $user->last_name;
        $data['company'] = get_user_meta( $user_id, PPY_PREFIX . 'company', true );
        $data['address'] = get_user_meta( $user_id, PPY_PREFIX . 'address', true );
        $data['zipcode'] = get_user_meta( $user_id, PPY_PREFIX . 'zipcode', true );
        $data['city'] = get_user_meta( $user_id, PPY_PREFIX . 'city', true );
        $data['country'] = get_user_meta( $user_id, PPY_PREFIX . 'country', true );

        if ( !empty( $_POST['start_date'] ) ) {
            $d = DateTime::createFromFormat( 'Y-m-d', $_POST['start_date'] );
            $date = date( 'Y-m-d H:i:s', $d->getTimestamp() ) ;
            $data['start_date'] = $_POST['start_date'];
        } else {
            $date = date( 'Y-m-d' );
            $data['start_date']  = $date;
        }


        $exp_days = Poppyz_Core::get_option( 'invoice_expiration_days' );
        if ( empty( $exp_days ) ) $exp_days = 7;
        $data['end_date'] = date( 'Y-m-d', strtotime( $date . ' + ' . $exp_days . ' days' ) );

        //get the user's subscriptions
        $ppy_subs = new Poppyz_Subscription();
        $subscriptions = $ppy_subs->get_subscriptions_by_user( $_POST['user_id'] );

        if ( $subscriptions) {
            global $ppy_lang;
            $data['subscriptions'] .= '<option value="-1">' . __('None','poppyz') . '</option>';
            foreach ( $subscriptions as $sub ) {
                $data['subscriptions'] .= '<option value="' . $sub->id . '">' . get_the_title( $sub->tier_id ) . '</option>';
            }
        }

        wp_send_json( $data );
    }

    /*public function populate_mc_list() {
        check_ajax_referer(PPY_PREFIX . 'ajax_mc', 'security');
        global $ppy_lang;

        // MAILCHIMP SUBSCRIPTION
        $api_key = $_POST['api_key'];

        $data = array(
            'apikey'        => $api_key
        );

        //get the string after the last dash on the api key to determine the datacenter
        $index = strrpos($api_key, "-");
        if( $index === false ) { //in case no dash was found
            echo json_encode( array( 'error' => __( 'MailChimp: API Key Invalid' ) ) );
            die;
        } else {

            $dc = substr( $api_key, $index + 1  );
            $url = 'https://' . $dc . '.api.mailchimp.com/3.0/';
            $target = 'lists';
            $auth = base64_encode( 'user:'. $api_key );
            $json_data = json_encode( $data );

            $ch = curl_init();

            curl_setopt( $ch, CURLOPT_URL, $url . $target );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                'Authorization: Basic '.$auth ) );
            curl_setopt( $ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0' );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );

            //curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );

            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data );

            $result = curl_exec( $ch );
            curl_close( $ch );

            //check if the returned json is contains the lists or an error
            $result_array = json_decode( $result );
            if ( isset( $result_array->lists ) ) {
                echo $result;
            } elseif ( $result == false ){
                echo json_encode( array( 'error' => __( 'MailChimp: API Key Invalid' ) ) );
            } else {
                echo json_encode( array( 'error' => 'MailChimp: ' . $result_array->title ) );
            }
            die;
        }

    }*/



    public function generate_secret_key() {
        check_ajax_referer(PPY_PREFIX . 'ajax_mc', 'security');
        echo $token = bin2hex(openssl_random_pseudo_bytes(10));
        die;
    }

    public function update_license() {

        // run a quick security check
        check_ajax_referer(PPY_PREFIX . 'ajax_mc', 'security');

        // retrieve the license from the database
        $license = trim( $_POST['license_key'] );

        $process = $_POST['process'];
        $this->_update_license( $license, $process );
    }

    private function _update_license( $license, $process ) {

        $action = 'activate_license';

        if ( $process == 'deactivate' ) {
            $action = 'deactivate_license';
        }

        // data to send in our API request
        $api_params = array(
            'edd_action'=> $action,
            'license' 	=> $license,
            'item_id' => PPY_ITEM_ID, // the name of our product in EDD
            'url'       => home_url()
        );

        // Call the custom API.
        $response = wp_remote_post( PPY_WEBSITE, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

        // make sure the response came back okay
        if ( is_wp_error( $response ) )
            return false;

        // decode the license data
        $license_data = json_decode( wp_remote_retrieve_body( $response ) );

        // $license_data->license will be either "valid" or "invalid"
        if ( $process == 'activate' ) {
            Poppyz_Core::save_option( 'license_status', $license_data->license );
            delete_transient( 'license_has_expired' );
        } else {
            Poppyz_Core::delete_option( 'license_status' );
        }

        echo wp_remote_retrieve_body( $response ) ;

        die;
    }

    public function change_upload_dir( $args ){

        if( !isset( $_REQUEST['post_id'] ) ) return $args;

        $id = (int)$_REQUEST['post_id'];

        if ( !in_array( get_post_type( $id ) , array( PPY_COURSE_PT, PPY_LESSON_PT ) ) )
            return $args;

        $new_dir = '/poppyz/attachments';

        $args['path']    = str_replace( $args['subdir'], '', $args['path'] );
        $args['url']     = str_replace( $args['subdir'], '', $args['url'] );

        $args['subdir']  = $new_dir;


        $args['path']   .= $new_dir;
        $args['url']    .= $new_dir;

        return $args;
    }


    /**
     * Use this to add the ID of the uploaded image to the media upload popup
     *
     * @since    0.5.0
     */

    public function image_to_editor($html, $id, $caption, $title, $align, $url, $size, $alt){

        $dom = new DOMDocument();
        @$dom->loadHTML($html);

        $x = new DOMXPath($dom);
        foreach($x->query("//img") as $node){
            $node->setAttribute("data-id", $id);
        }

        if($dom->getElementsByTagName("a")->length == 0){
            $newHtml = $dom->saveXML($dom->getElementsByTagName('img')->item(0));
        }else{
            $newHtml = $dom->saveXML($dom->getElementsByTagName('a')->item(0));
        }

        return $newHtml;
    }


    public function clone_course() {
        global $wpdb;
        if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'ppy_clone_course' == $_REQUEST['action'] ) ) ) {
            wp_die('No post to duplicate has been supplied!');
        }

        /*
         * get the original post id
         */
        $post_id = (isset($_GET['post']) ? $_GET['post'] : $_POST['post']);
        /*
         * and all the original post data then
         */
        $post = get_post( $post_id );

        /*
         * if you don't want current user to be the new post author,
         * then change next couple of lines to this: $new_post_author = $post->post_author;
         */
        $current_user = wp_get_current_user();
        $new_post_author = $current_user->ID;

        /*
         * if post data exists, create the post duplicate
         */
        if (isset( $post ) && $post != null) {

            /*
             * new post data array
             */
            $args = array(
                'comment_status' => $post->comment_status,
                'ping_status'    => $post->ping_status,
                'post_author'    => $new_post_author,
                'post_content'   => $post->post_content,
                'post_excerpt'   => $post->post_excerpt,
                'post_name'      => $post->post_name,
                'post_password'  => $post->post_password,
                'post_status'    => 'draft',
                'post_title'     => $post->post_title,
                'post_type'      => $post->post_type,
                'to_ping'        => $post->to_ping,
                'menu_order'     => $post->menu_order
            );

            //template, this relies on the Libre Poppyz theme.
            $page_template = get_post_meta($post->ID, PPY_PREFIX . 'page_template', true);
            $auto_add_list = get_post_meta($post->ID, PPY_PREFIX . 'lesson_list', true);

            //get featured image
            $thumbnail_id = get_post_thumbnail_id( $post->ID );

            /*
             * insert the post by wp_insert_post() function
             */
            $new_course_id = wp_insert_post( $args );

            //save post metas
            add_post_meta( $new_course_id, PPY_PREFIX . 'page_template', $page_template );
            add_post_meta( $new_course_id, PPY_PREFIX . 'lesson_list', $auto_add_list );

            //save post thumbnail
            set_post_thumbnail( $new_course_id, $thumbnail_id );

            //profit builder clone fix
            if ( class_exists( 'ProfitBuilder' ) ) {
                global $pbuilder;
                $pbuilder->copy_post_pb_data( $new_course_id, $post );
            }

            //divi builder
            $this->clone_divi_content($post, $new_course_id);

            //save / recreate modules
            $args = array(
                'taxonomy' => PPY_LESSON_CAT,
                'orderby' =>  'menu_order',
                'order' =>  'ASC',
                'hide_empty' =>  false,
                'hierarchical' =>  false,
                'parent' =>  0,
            );

            if ( $post_id ) {
                $args['meta_query'] = array(
                    array(
                        'key' => 'ppy_module_course',
                        'value' => $post_id,
                        'compare'		=> '='
                    )
                );
            }
            $terms = get_terms( $args );

            $old_terms = array();
            if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                $i = 0;
                foreach ( $terms as $term ) {
                    $i++;
                    $term_id = wp_insert_term(
                        $term->name, // the term
                        PPY_LESSON_CAT,
                        array( 'slug' => Poppyz_Core::generate_term_name( $term->name ) )
                    );

                    update_term_meta( $term_id['term_id'], 'ppy_module_course', $new_course_id );
                    update_term_meta( $term_id['term_id'], 'menu_order', $i );
                    //save the old term associations so we can reassign the new terms properly to new lessons later

                    $old_terms[$term->term_id] = $term_id['term_id'];

                    if (is_plugin_active('enhanced-category-pages/enhanced-category-pages.php') ) {
                        global $enhanced_category;
                        $posts_array = $enhanced_category->get_by_category($term->term_id);

                        if (isset($posts_array[0])) {
                            $original_post_thumbnail_id = get_post_thumbnail_id( $posts_array[0] );
                            $ecp_post_id = $enhanced_category->get_first_or_create_for_category($term_id['term_id'], PPY_LESSON_CAT);
                            if ($ecp_post_id) {
                                set_post_thumbnail($ecp_post_id, $original_post_thumbnail_id);
                                $this->clone_divi_content($posts_array[0] , $ecp_post_id);
                                $data = array(
                                    'ID' => $ecp_post_id,
                                    'post_content' => get_post_field('post_content', $posts_array[0]),
                                );

                                wp_update_post( $data );
                            }
                        }
                    }
                }
            }
            //get the lessons associated with this course
            $ppy_core = new Poppyz_Core();
            $lessons = $ppy_core->get_lessons_by_course( $post_id );
            //clone each lesson
            foreach($lessons->posts as $post) :

                $args = array(
                    'comment_status' => $post->comment_status,
                    'ping_status'    => $post->ping_status,
                    'post_author'    => $new_post_author,
                    'post_content'   => $post->post_content,
                    'post_excerpt'   => $post->post_excerpt,
                    'post_name'      => $post->post_name,
                    'post_password'  => $post->post_password,
                    'post_status'    => 'publish',
                    'post_title'     => $post->post_title,
                    'post_type'      => $post->post_type,
                    'to_ping'        => $post->to_ping,
                    'menu_order'     => $post->menu_order
                );

                //template, this relies on the Libre Poppyz theme.
                $page_template = get_post_meta($post->ID, PPY_PREFIX . 'page_template', true);
                $auto_add_list = get_post_meta($post->ID, PPY_PREFIX . 'lesson_list', true);

                $thumbnail_id = get_post_thumbnail_id( $post->ID );

                $the_old_terms = get_the_terms( $post->ID, PPY_LESSON_CAT );
                /*
                 * insert the post by wp_insert_post() function
                 */
                $new_lesson_id = wp_insert_post( $args );

                add_post_meta( $new_lesson_id, PPY_PREFIX . 'lessons_course', $new_course_id, true );
                add_post_meta( $new_lesson_id, PPY_PREFIX . 'page_template', $page_template );
                add_post_meta( $new_lesson_id, PPY_PREFIX . 'lesson_list', $auto_add_list );

                set_post_thumbnail( $new_lesson_id, $thumbnail_id );

                //resave modules
                if ( $the_old_terms && ! is_wp_error( $the_old_terms ) ) {
                    foreach ( $the_old_terms as $old_term ) {
                        wp_set_post_terms( $new_lesson_id, $old_terms[$old_term->term_id], PPY_LESSON_CAT );
                    }
                }
                //check if Thrive Content Editor Plugin is activated, if yes copy its fields as well - one of the fields  holds the main content of posts.
                if ( defined('TVE_TCB_CORE_INCLUDED') ) {
                    global $wpdb;
                    $tve_fields = $wpdb->get_results(
                        "
                        SELECT meta_key, meta_value
                        FROM $wpdb->postmeta
                        WHERE meta_key LIKE  'tve_%'
                        AND post_id = $post->ID
                        "
                    );

                    if ( $tve_fields ) {
                        foreach ( $tve_fields as $field ) {
                            add_post_meta( $new_lesson_id, $field->meta_key, $field->meta_value, true );
                        }
                    }
                }

                //divi builder
                global $wpdb;
                $divi_fields = $wpdb->get_results(
                    "
                    SELECT meta_key, meta_value
                    FROM $wpdb->postmeta
                    WHERE meta_key LIKE  '_et_pb_%'
                    AND post_id = $post->ID
                    "
                );

                if ( $divi_fields ) {
                    foreach ( $divi_fields as $field ) {
                        add_post_meta( $new_lesson_id, $field->meta_key, $field->meta_value, true );
                    }
                }

                //profit builder clone fix
                if ( class_exists( 'ProfitBuilder' ) ) {
                    global $pbuilder;
                    $pbuilder->copy_post_pb_data( $new_lesson_id, $post );
                }

            endforeach;

            /*
             * finally, redirect to the edit post screen for the new draft
             */
            wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_course_id ) );
            exit;
        } else {
            wp_die('Post creation failed, could not find original post: ' . $post_id);
        }
    }

    public function delete_course() {
        if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'ppy_delete_course' == $_REQUEST['action'] ) ) ) {
            wp_die('No post to delete has been supplied!');
        }


        $post_id = (isset($_GET['post']) ? $_GET['post'] : $_POST['post']);

        $ppy_core = new Poppyz_Core();
        $lessons = $ppy_core->get_lessons_by_course( $post_id );
        $tiers = $ppy_core->get_tiers_by_course( $post_id );

        //loop through each lesson and tier to delete them
        while ($lessons->have_posts()) : $lessons->the_post();
            wp_delete_post( get_the_ID(), true );
        endwhile;

        while ($tiers->have_posts()) : $tiers->the_post();
            wp_delete_post( get_the_ID(), true );
        endwhile;

        //delete all lesson modules associated to this course
        Poppyz_Core::delete_modules_by_course( $post_id );

        wp_delete_post( $post_id, true );

        wp_redirect( admin_url( 'admin.php?page=poppyz' ) );
        exit;

    }

	/**
     * initialize the TCPDF library
	 * @return void
	 */
	public function initialize_tcpdf() {
		require_once PPY_DIR_PATH . 'includes/tcpdf_min/config/tcpdf_config.php' ;
		require_once PPY_DIR_PATH . 'includes/tcpdf_min/tcpdf.php' ;
	}

	/**
     * generate pdf file for student list
     *
	 * @param $studentList
	 * @return void
	 */
	public function generate_students_pdf( $studentList ) {
		$this->initialize_tcpdf();
		global $ppy_lang;

		$user = wp_get_current_user();


		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor( $user->data->user_nicename);
		$pdf->SetTitle( __('Student List', 'poppyz') );
		$pdf->SetSubject( __('Student List','poppyz') );

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);


		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->setFont( 'helvetica', '', 8);

		$pdf->setFontSubsetting(true);

		$pdf->setCellPaddings(1, 1, 1, 1);

		$pdf->setCellMargins(1, 1, 1, 1);

		$pdf->SetFillColor(255, 255, 255);

		$pdf->AddPage();



		//$invoice_info .=  __( 'Client Number: ' ) . $client_number .  '<br />';

		$studentTable = '<style>
            table {
                width: 100%;
                
            }
            td {
                padding: 10px;
            }
            th {
                font-size:14px;
            }
            
            tr.headings th {
                background-color: #eeeeee;
                color: #8d8d8d;
            }
            tr.first-row td {
                height: 200px;
                border-bottom: 1px solid #ccc;
            }
            th.tax-amount {
                border-bottom: 1px solid #ccc;
            }

            td.empty {
                border-bottom: none !important;
            }
            .student-name {
                width:30%;
            }
            .date-created {
                font-weight:normal;
                font-style: italic;
            }
            .status {
                font-weight:normal;
            }
            .student-row {
                border-bottom:1px solid #D7E0E8;
            }
            .status-row {
                border-bottom:1px solid #D7E0E8;
            }
            h3 {
                font-weight:500;
            }
          </style>';


		$studentTable .= '<table cellpadding="10" width="100%">';
		$studentTable .= '<tr class="headings">';
		$studentTable .= '<th class="student-name">';
		$studentTable .= __('Name (username)', 'poppyz');
		$studentTable .= '</th>';
		$studentTable .= '<th class="course">';
		$studentTable .= __('Course', 'poppyz');
		$studentTable .= '</th>';
		$studentTable .= '<th class="status">';
		$studentTable .= __('Status', 'poppyz');
		$studentTable .= '</th>';
		$studentTable .= '</tr>';

        foreach ( $studentList as $student ){
			$studentTable .= '<tr ><td class="student-name student-row">'.$student['fullname'];
			$studentTable .= '</td>';
			$studentTable .= '<td class="course student-row">';
			$studentTable .= '<strong>Course:</strong> '.$student['course'].'<br/>';
            $studentTable .= '<strong>Tier:</strong> '.$student['tier'].'</td> ';
			$studentTable .= '<td  class="status student-row"><span class="status">'.$student['subscription']['status'].'</span><br/>';
            $studentTable .= '<span class="date-created">'.$student['subscription']['subscription_date'].'</span></td></tr>';
		}

		$studentTable .= '</table>';

		$pdf->MultiCell( 55, 40, '<h1>' .  __( 'Student' ,'poppyz')  . '</h1>', 0, 'L', 0, 1, 15, 20, true, 0, true, true, 60 ,'T', true );


		$pdf->writeHTMLCell(0, 0, '', '', $studentTable, 0, 1, 0, true, '', true );
		$pdf->Ln( 5 );

		$pdf->Ln( 5 );

		$upload_dir = wp_upload_dir();
		$student_dir = $upload_dir['basedir'].'/poppyz/student/pdf/';
		if ( !is_dir( $student_dir ) ) {
			mkdir( $student_dir, 0755, true );
		}
		$pdf->Output($student_dir . date( "U" ) . '.pdf', 'F');

	}

	/**
     * download student list in PDF file
     *
	 * @param $students
	 * @return void
	 */
	public function students_download_pdf( $students = false ) {

		$this->generate_students_pdf( $students );

		$upload_dir = wp_upload_dir();
		$student_dir = $upload_dir['basedir'].'/poppyz/student/';
		$zip_file =  $student_dir . 'students.zip';
		$this::students_zip( $student_dir . '/pdf', $zip_file);
		$this::delete_students_directory( $student_dir . '/pdf' );

		if( $zip_file ){
			//Set Headers:
			header('Pragma: public');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($zip_file)) . ' GMT');
			header('Content-Type: application/force-download');
			header('Content-Disposition: inline; filename="students.zip"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . filesize($zip_file));
			header('Connection: close');
			ob_clean();
			flush();
			if (readfile($zip_file)) {
				$this::delete_students_directory( $student_dir );
			}
			exit();
		}

	}

	/**
     * zip the PDF file of the student list.
     *
	 * @param $source
	 * @param $destination
	 * @return bool
	 */
	public static function students_zip($source, $destination) {
		if (!extension_loaded('zip') || !file_exists($source)) {
			return false;
		}

		$zip = new ZipArchive();
		if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
			return false;
		}

		$source = str_replace('\\', '/', realpath($source));

		if (is_dir($source) === true)
		{
			$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

			foreach ($files as $file)
			{
				$file = str_replace('\\', '/', realpath($file));

				if (is_dir($file) === true)
				{
					$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
				}
				else if (is_file($file) === true)
				{
					$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
				}
			}
		}
		else if (is_file($source) === true)
		{
			$zip->addFromString(basename($source), file_get_contents($source));
		}

		return $zip->close();
	}

	/**
     *  delete student list directory
	 * @param $dirname
	 * @return bool
	 */
	public static function delete_students_directory($dirname) {
		if (is_dir($dirname))
			$dir_handle = opendir($dirname);
		if (!$dir_handle)
			return false;
		while($file = readdir($dir_handle))
		{
			if ($file != "." && $file != "..")
			{
				if (!is_dir($dirname."/".$file))
					unlink($dirname."/".$file);
				else
					delete_directory($dirname.'/'.$file);
			}
		}
		closedir($dir_handle);
		rmdir($dirname);
		return true;
	}

	/**
     * Export student list in XML format.
     *
	 * @param $args
	 * @return void
	 */
	public function export_students_xml( $students ) {
		$host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
		$students_xml = new SimpleXMLElement('<source></source>');
		$students_xml->addChild('publisher', $_SERVER['HTTP_HOST']);
		$students_xml->addChild('publisherurl', $host);
		$students_xml->addChild('lastBuildDate', date('r'));
		$students_xml = $students_xml->addChild('students');


		if ( !$students ) return;
		$fields = array( 'name', 'course', 'tier', 'subscription_date', 'status' );
		foreach ( $students as $student ) {
			$student_xml = $students_xml->addChild('students');
			//$student_xml->addChild('name', $student['student_info']->data->display_name );
			$student_xml->addChild('name', $student['fullname'] );
			$student_xml->addChild('course', $student['course'] );
			$student_xml->addChild('tier', $student['tier'] );
			$student_xml->addChild('status', $student['subscription']['status'] );
			$student_xml->addChild('subscription_date', $student['subscription']['subscription_date'] );
		}
		ob_end_clean();
		header_remove();
		header('Content-disposition: attachment; filename=students.xml');
		header ("Content-Type:text/xml");
		echo $students_xml->asXML();
		exit();
	}

    public function process_entities() {
        if (isset($_GET['page']) && $_GET['page'] == 'poppyz-students' ) {
			if ( isset($_POST['subscription_id'])) {
                $subscriptionIdArray = $_POST['subscription_id'];
                $studentsArray = [];

                if (isset($_POST['check_all']) && $_POST['check_all'] == "on") {

					$search = "";
					$filter = array();
                    $_SERVER['REQUEST_URI'] = remove_query_arg( '_wp_http_referer', $_SERVER['REQUEST_URI'] );
                    if ( !empty( $_REQUEST['u_user_id'] ) && $_REQUEST['u_user_id'] != '-1' ) {
                        $filter['u.user_id'] =  $_REQUEST['u_user_id'];
                        $search .= "&user_id=" .$_REQUEST['u_user_id'] ;
                    }
                    if ( !empty( $_REQUEST['u_course_id'] ) && $_REQUEST['u_course_id'] != '-1' && $_REQUEST['u_course_id'] != 'all') {
                        $filter['u.course_id'] =  $_REQUEST['u_course_id'];
                        $search .= "&course_id=" .$_REQUEST['u_course_id'];
                    }
                    if ( !empty( $_REQUEST['u_tier_id'] ) && $_REQUEST['u_tier_id'] != '-1'  && $_REQUEST['u_tier_id'] != "all") {

                        $filter['u.tier_id'] =  $_REQUEST['u_tier_id'];
                        $search .= "&tier_id=" .$_REQUEST['u_tier_id'];
                    }
                    if ( !empty( $_REQUEST['subscription_date'] ) && $_REQUEST['subscription_date'] != '-1' ) {
                        $d = DateTime::createFromFormat( 'Y-m-d', $_REQUEST['subscription_date'] );
                        if ( $d !== false ) {
                            $date = date( 'Y-m-d', $d->getTimestamp() ) ;
                            $filter['subscription_date'] = $date;
                            $search .= "&subscription_date=" . $date . "&sub=0";
                        } else {
                            $message = __( 'Invalid Date' );
                            $filter['subscription_date'] = null;
                        }
                    }
					$students = Poppyz_Core::get_students( $filter );
					foreach ( $students as $student ) {
						array_push($studentsArray, $student);
					}
				} else {
					foreach ( $subscriptionIdArray as $subscriptionId ) {
						$filter['id'] =  $subscriptionId;
						$students = Poppyz_Core::get_students( $filter );
						foreach ( $students as $student ) {
							array_push($studentsArray, $student);
						}
					}
				}

                if ( $_POST['export_type'] == "export-pdf") {
                    $this->students_download_pdf($studentsArray);
					$upload_dir = wp_upload_dir();
					$student_dir = $upload_dir['basedir'].'/poppyz/student/';
					$zip_file =  $student_dir . 'students.zip';

				} else if ( $_POST['export_type'] == "export-xml") {
                    $this->export_students_xml($studentsArray);
				} else if ( $_POST['export_type'] == "export-csv") {
					Poppyz_Core::export_students( $studentsArray );
				}
			}
            if ( isset($_POST['export-students'])) {
               $filter = array();
                if ( !empty( $_POST['select-user'] ) && $_POST['select-user'] != '-1' ) {
                    $filter['user_id'] =  $_POST['select-user'];
                }
                if ( !empty( $_POST['select-course'] ) && $_POST['select-course'] != '-1' ) {
                    $filter['course_id'] =  $_POST['select-course'];
                }
                if ( !empty( $_POST['select-tier'] ) && $_POST['select-tier'] != '-1' ) {
                    $filter['tier_id'] =  $_POST['select-tier'];
                }
                if ( !empty( $_POST['subscribe-course-date'] ) && $_POST['subscribe-course-date'] != '-1' ) {

                    $date = Date('Y-m-d', strtotime($_POST['subscribe-course-date']));
                    $filter['subscription_date'] = $date;
                }


            }
        }
        if ( isset( $_GET['page'] ) && ( $_GET['page'] == 'poppyz-coupons'  || $_GET['page'] == 'poppyz-invoices' ) ) {

            $ppy_coupon = new Poppyz_Coupon();
            $ppy_invoice = new Poppyz_Invoice();

            if ( $_SERVER['REQUEST_METHOD'] == 'POST' && !isset( $_POST['bulk'] ) ) {
                $data = $_POST;
                if ( ! isset( $data['_wpnonce'] ) || ! wp_verify_nonce( $data['_wpnonce'], 'ppy-nonce' ) ) {
                    wp_die( __( 'Trying to cheat or something?' ), __( 'Error' ), array( 'response' => 403 ) );
                }
                $action = $data['ppy-action'];

                unset( $data['ppy-action'] );
                unset($data['_wpnonce']);

                if ($action == 'add-coupon') {
                    if (!$ppy_coupon->get_coupon_by_code($data['code'])) {
                        // Set the discount code's default status to active

                        $data['status'] = 'active';
                        if ($ppy_coupon->save_coupon($data)) {
                            wp_redirect(add_query_arg('ppy-message', 'coupon_added', 'admin.php?page=poppyz-coupons'));
                        } else {
                            wp_redirect(add_query_arg('ppy-message', 'coupon_add_failed', 'admin.php?page=poppyz-coupons&ppy-action=add-coupon'));
                        }
                    } else {
                        wp_redirect(add_query_arg('ppy-message', 'coupon_exists', 'admin.php?page=poppyz-coupons&ppy-action=add-coupon'));
                    }

                } elseif ($action == 'update-coupon') {
                    $coupon = $ppy_coupon->get_coupon_by_code($data['code']);
                    $id = $data['id'];
                    $redirect = 'admin.php?page=poppyz-coupons&ppy-action=edit-coupon&coupon=' . $id;
                    if (!$coupon || ($coupon && $coupon->ID == $id)) {
                        if ($coupon_result = $ppy_coupon->save_coupon($data, $id)) {
                            if (!is_wp_error( $coupon_result ) && is_numeric($coupon_result)) {
                                wp_redirect(add_query_arg('ppy-message', 'coupon_updated', $redirect));
                            } else {
                                wp_redirect(add_query_arg('ppy-message', $coupon_result->get_error_code(), $redirect ) );
                            }
                        }
                    } else {
                        wp_redirect(add_query_arg('ppy-message', 'coupon_exists', $redirect));
                    }
                } elseif ($action == 'add-invoice' && !empty($_POST['select-subscription']) && $_POST['select-subscription'] > 0) {
                    if ($ppy_invoice->save_invoice($_POST['select-subscription'], $data)) {
                        if (isset($data['set_payment_number']) && $data['set_payment_number'] > 0) {
                            Poppyz_Subscription::update_subscription('update', $_POST['select-subscription'], array('payments_made' => $data['set_payment_number']));
                        }
                        wp_redirect(add_query_arg('ppy-message', 'invoice_added', 'admin.php?page=poppyz-invoices'));
                    } else {
                        wp_redirect(add_query_arg('ppy-message', 'invoice_add_failed', 'admin.php?page=poppyz-invoices&ppy-action=add-invoice'));
                    }
                } elseif ($action == 'update-invoice') {
                    $id = $data['id'];
                    $redirect = 'admin.php?page=poppyz-invoices&ppy-action=edit-invoice&invoice=' . $id;
                    if ($ppy_invoice->save_invoice($id, $data)) {
                        wp_redirect(add_query_arg('ppy-message', 'invoice_updated', $redirect));
                    } else {
                        wp_redirect(add_query_arg('ppy-message', 'invoice_updated_failed', $redirect));
                    }
                }

            } elseif ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $data = $_GET;

                if (isset ($_GET['ppy-action']) && $data['ppy-action'] != 'add-coupon' && $data['ppy-action'] != 'edit-coupon' && $data['ppy-action'] != 'add-invoice' && $data['ppy-action'] != 'edit-invoice') {
                    $action = $data['ppy-action'];
                    if (!isset($data['_wpnonce']) || !wp_verify_nonce($data['_wpnonce'], 'ppy-nonce')) {
                        wp_die(__('Trying to cheat or something?!'), __('Error'), array('response' => 403));
                    }

                    if ($action == 'deactivate-coupon') {
                        $ppy_coupon->update_coupon_status($data['coupon'], 'inactive');
                    } elseif ($action == 'activate-coupon') {
                        $ppy_coupon->update_coupon_status($data['coupon']);
                    } elseif ($action == 'delete-coupon') {
                        $ppy_coupon->remove_coupon($data['coupon']);
                    } elseif ($action == 'pay-invoice') {
                        $ppy_invoice->update_invoice_status($data['invoice'], 'paid');
                    } elseif ($action == 'unpay-invoice') {
                        $ppy_invoice->update_invoice_status($data['invoice'], 'unpaid');
                    } elseif ( $action == 'duplicate-credit' ) {
	                    $ppy_invoice->duplicate_to_credit( $data['invoice'] );
                    } elseif ($action == 'delete-invoice') {
                        $ppy_invoice->remove_invoice($data['invoice']);
                    } elseif ($action == 'generate-pdf') {
                        $format = isset( $_GET['download'] ) ? "F" : "I";
                        if ( $format == "F" ) {
                            $ppy_invoice->download();
                        } else {
                            $preview = isset( $_GET['preview'] ) ? true : false;
                            $ppy_invoice->generate_pdf($data['invoice'], 'I', $preview);
                        }
                        exit();
                    } elseif ($action == 'export-xml') {
                        $ppy_invoice->export_xml();
                    } elseif ($action == 'export-csv') {
                        $ppy_invoice->export_csv();
                    }
                } else {
                    require_once PPY_DIR_PATH . 'admin/class-poppyz-invoice-table.php';
                    $ppy_invoice_table = new Poppyz_Invoice_List_Table();
                    if ( isset( $_GET['export-xml'] ) || isset( $_GET['export-csv'] ) ) {
                        $args = array();
                        if ( !empty ( $_GET['invoice'] ) ) {
                            $args['post__in'] = $_GET['invoice'];
                            if ( isset( $_GET['export-xml'] ) ) {
                                $ppy_invoice->export_xml( $args );
                            } else {
                                $ppy_invoice->export_csv( $args );
                            }
                        }
                    } elseif ( isset( $_GET['download'] ) && !empty ( $_GET['invoice'] ) ) {
                        $ppy_invoice->download( $_GET['invoice'] );
                    } else {
                        $ppy_invoice_table->process_bulk_action();
                    }
                }
            } elseif( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

            }

        }
    }

    //allow divi builder editor to poppyz's post types
    function et_builder_post_types( $post_types ) {
        $post_types[] = PPY_COURSE_PT;
        $post_types[] = PPY_LESSON_PT;
        $post_types[] = 'enhancedcategory';

        return $post_types;
    }
    //add divi library to poppyz's post types
	function et_pb_show_all_layouts_built_for_post_type() {
        return get_post_types();
	}

    public function load_media() {
        if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != PPY_LESSON_CAT ) {
            return;
        }
        wp_enqueue_media();
    }

    /**
     * Add a form field in the new category page
     * @since 1.0.0
     */

    public function add_category_image( $taxonomy ) { ?>
        <?php if ( is_plugin_active('enhanced-category-pages/enhanced-category-pages.php')) : ?>
            <h4>It looks like you have Enhanced Category Plugin installed. Please use this link to edit the Category fields</h4>
        <?php else: ?>
            <div class="form-field term-group">
                <label for="ppy-lesson-category-image-id"><?php _e( 'Image', 'showcase' ); ?></label>
                <input type="hidden" id="ppy-lesson-category-image-id" name="ppy-lesson-category-image-id" class="custom_media_url" value="">
                <div id="category-image-wrapper"></div>
                <p>
                    <input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Add Image', 'showcase' ); ?>" />
                    <input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remove Image', 'showcase' ); ?>" />
                </p>
            </div>
        <?php endif ?>
    <?php }

    /**
     * Save the form field
     * @since 1.0.0
     */
    public function save_category_image( $term_id, $tt_id ) {
        if( isset( $_POST['ppy-lesson-category-image-id'] ) && '' !== $_POST['ppy-lesson-category-image-id'] ){
            add_term_meta( $term_id, 'ppy-lesson-category-image-id', absint( $_POST['ppy-lesson-category-image-id'] ), true );
        }
    }

    /**
     * Edit the form field
     * @since 1.0.0
     */
    public function update_category_image( $term, $taxonomy ) { ?>
        <?php if ( is_plugin_active('enhanced-category-pages/enhanced-category-pages.php')) : ?>
            <?php
                global $enhanced_category;
                $enchanced_id = $enhanced_category->get_first_or_create_for_category($term->term_id, PPY_LESSON_CAT);
            ?>
            <tr><td></td><td><h4>It looks like you have Enchance Category Plugin installed. Please use this <a href="<?php echo get_edit_post_link($enchanced_id); ?>">link</a> to edit the Category fields</h4></td></tr>
        <?php else: ?>
            <tr class="form-field term-group-wrap">
                <th scope="row">
                    <label for="ppy-lesson-category-image-id"><?php _e( 'Image', 'showcase' ); ?></label>
                </th>
                <td>
                    <?php $image_id = get_term_meta( $term->term_id, 'ppy-lesson-category-image-id', true ); ?>
                    <input type="hidden" id="ppy-lesson-category-image-id" name="ppy-lesson-category-image-id" value="<?php echo esc_attr( $image_id ); ?>">
                    <div id="category-image-wrapper">
                        <?php if( $image_id ) { ?>
                            <?php echo wp_get_attachment_image( $image_id, 'thumbnail' ); ?>
                        <?php } ?>
                    </div>
                    <p>
                        <input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Add Image', 'showcase' ); ?>" />
                        <input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remove Image', 'showcase' ); ?>" />
                    </p>
                </td>
            </tr>
        <?php endif; ?>
    <?php }

    /**
     * Update the form field value
     * @since 1.0.0
     */
    public function updated_category_image( $term_id, $tt_id ) {
        if( isset( $_POST['ppy-lesson-category-image-id'] ) && '' !== $_POST['ppy-lesson-category-image-id'] ){
            update_term_meta( $term_id, 'ppy-lesson-category-image-id', absint( $_POST['ppy-lesson-category-image-id'] ) );
        } else {
            update_term_meta( $term_id, 'ppy-lesson-category-image-id', '' );
        }
    }

    /**
     * Enqueue styles and scripts
     * @since 1.0.0
     */
    public function add_script() {
        if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != PPY_LESSON_CAT ) {
            return;
        } ?>
        <script> jQuery(document).ready( function($) {

                _wpMediaViewsL10n.insertIntoPost = '<?php  _e( "Insert", "showcase" ); ?>';
                function ct_media_upload(button_class) {
                    var _custom_media = true, _orig_send_attachment = wp.media.editor.send.attachment;
                    $('body').on('click', button_class, function(e) {
                        var button_id = '#'+$(this).attr('id');
                        var send_attachment_bkp = wp.media.editor.send.attachment;
                        var button = $(button_id);
                        _custom_media = true;
                        wp.media.editor.send.attachment = function(props, attachment){
                            if( _custom_media ) {
                                $('#ppy-lesson-category-image-id').val(attachment.id);
                                $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
                                $( '#category-image-wrapper .custom_media_image' ).attr( 'src',attachment.url ).css( 'display','block' );
                            } else {
                                return _orig_send_attachment.apply( button_id, [props, attachment] );
                            }
                        }
                        wp.media.editor.open(button); return false;
                    });
                }
                ct_media_upload('.showcase_tax_media_button.button');
                $('body').on('click','.showcase_tax_media_remove',function(){
                    $('#ppy-lesson-category-image-id').val('');
                    $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
                });
                // Thanks: http://stackoverflow.com/questions/15281995/wordpress-create-category-ajax-response
                $(document).ajaxComplete(function(event, xhr, settings) {
                    var queryStringArr = settings.data.split('&');
                    if( $.inArray('action=add-tag', queryStringArr) !== -1 ){
                        var xml = xhr.responseXML;
                        $response = $(xml).find('term_id').text();
                        if($response!=""){
                            // Clear the thumb image
                            $('#category-image-wrapper').html('');
                        }
                    }
                });
            });
        </script>
    <?php }
    public function change_featured_image_text( $content, $post_id ) {
        global $ppy_lang;
        $description = (get_post_type($post_id) == PPY_COURSE_PT || get_post_type($post_id) == PPY_LESSON_PT )
            ? '<small>' .
            sprintf( __( "We recommend using the same size for all courses and lessons for better display on overviews. Minimum recommended size is 300x300px. Click <a href='%s'>here</a> for more info.",'poppyz' ), 'https://poppyz.nl/handleiding/resizing-images/' )
            . '</small>' : '';

        return $content . $description;
    }

    public function user_can_richedit( $default ) {
        global $post;
        if( $post->post_type === PPY_TIER_PT )  return false;
        return $default;
    }

    /**
     * @param $post
     * @param $new_course_id
     * @return array|bool
     */
    public function clone_divi_content($post, $new_course_id)
    {
        global $wpdb;
        $divi_fields = $wpdb->get_results(
            "
                    SELECT meta_key, meta_value
                    FROM $wpdb->postmeta
                    WHERE meta_key LIKE  '_et_pb_%'
                    AND post_id = $post->ID
                    "
        );

        if ($divi_fields) {
            foreach ($divi_fields as $field) {
                add_post_meta($new_course_id, $field->meta_key, $field->meta_value, true);
            }
        }
        return array($wpdb, $divi_fields, $field);
    }

	public function add_post_enctype() {
		$current_screen = get_current_screen();
		if ($current_screen->post_type == "ppy_tier") {
			echo ' enctype="multipart/form-data"';
		}
	}

	public function deleteTierDonationImage() {
		$id = $_POST['additonal_product_image'];
		$postId = $_POST['post_id'];
		$attachmentId = $_POST['attachment_id'];

		delete_post_meta($postId, PPY_PREFIX . $id);
		wp_delete_attachment( $attachmentId, true );

		die();

	}

    public function uploadTierDonationImage () {
		$image = $_POST['image_donation'];
        $tierId = $_POST['tier_id'];

		list($type, $image) = explode(';',$image);
		list(, $image) = explode(',',$image);

        $userId = get_current_user_id();

        $imageName = "donation_image_".$userId."_".$tierId."_".time();
		$fileName = "donation_image_".$userId."_".$tierId."_".time().'.png';

		$image = base64_decode($image);
        $uploadDir = wp_get_upload_dir();
        $uploadPath = $uploadDir['path'].'/donation_image/';

        $this->checkAndCreateDirectory($uploadPath);

        $imagePath = $uploadPath.$fileName;
		$imageUrl = $uploadDir['url'].'/donation_image/'.$fileName;

		file_put_contents($imagePath, $image);

		$attachment = array(
			'guid'           => $imagePath,
			'post_mime_type' => 'image/png',
			'post_title'     => $fileName,
			'post_content'   => '',
			'post_status'    => 'inherit'
		);

		$attachmentId = wp_insert_attachment( $attachment, $imagePath, $tierId );
		if( is_wp_error( $attachmentId ) || ! $attachmentId ) {
			error_log( 'Donation image upload error.' );
		}
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		$attach_data = wp_generate_attachment_metadata( $attachmentId, $fileName );

		wp_update_attachment_metadata( $attachmentId, $attach_data );

		update_post_meta($tierId, PPY_PREFIX . 'tier_donation_image', $attachmentId);
		update_post_meta($tierId, PPY_PREFIX . 'tier_donation_image_url', $imageUrl);

        die();

	}
	public function checkAndCreateDirectory($directory) {
		try {
			if (!is_dir($directory)) {
				if (!mkdir($directory, 0755, true))
				{
					throw new Exception("Error creating the directory.");
				}
			}
		}
		catch (Exception $e)
		{
			error_log("An error occurred in creating directory for donation image: " . $e->getMessage());
		}
	}

	function deleteAdditionalProductImage() {
		$id = $_POST['additonal_product_image'];
		$postId = $_POST['post_id'];
		$attachmentId = $_POST['attachment_id'];

		delete_post_meta($postId, PPY_PREFIX . $id);
		wp_delete_attachment( $attachmentId, true );

		die();

	}

	public function statistics_turnover_json() {

		$date = new DateTime();
		$date_now = $date->format( get_option('date_format') );
		$month = $date->format( 'F' );
		$this_month = $date->format( 'm' );
		$year = $date->format( 'Y' );
		$last_year =  (int)$year - 1;
		$last_month = (int)$this_month - 1;
		$next_month = (int)$this_month + 1;

		//get last year
		$ly_start = mktime(0, 0, 0, 1, 1, $last_year);
		$ly_end = mktime(0, 0, 0, 12, 31, $last_year);
		$ly_start_date  = date( 'Y-m-d 00:00:00', $ly_start );
		$ly_end_date  = date( 'Y-m-d 12:59:59', $ly_end );

		//get last month

		//if current month is January so last month is 12
		if ( $last_month == 0  ) {
			$last_month = 12;
			$year = $year - 1;
		}


		//get last month
		$lm_start = mktime(0, 0, 0, $last_month, 1, $year);
		$lm_end = mktime(0, 0, 0, $last_month, cal_days_in_month(CAL_GREGORIAN, $last_month, $year), $year);
		$lm_start_date  = date( 'Y-m-d 00:00:00', $lm_start );
		$lm_end_date  = date( 'Y-m-d 12:59:59', $lm_end );

		//get this year
		$ty_start = mktime(0, 0, 0, 1, 1, $year);
		$ty_end = mktime(0, 0, 0, 12, 31, $year);
		$ty_start_date  = date( 'Y-m-d 00:00:00', $ty_start );
		$ty_end_date  = date( 'Y-m-d 12:59:59', $ty_end );

		//get this month
		$tm_start = mktime(0, 0, 0, $this_month, 1, $year);
		$tm_end = mktime(0, 0, 0, $this_month, cal_days_in_month(CAL_GREGORIAN, $this_month, $year), $year);
		$tm_start_date  = date( 'Y-m-d 00:00:00', $tm_start );
		$tm_end_date  = date( 'Y-m-d 12:59:59', $tm_end );

		$start_date = ( !empty( $_POST['start_date'] ) )  ? $_POST['start_date'] . ' 00:00:00' : null;
		$end_date = ( !empty( $_POST['end_date'] ) )  ? $_POST['end_date'] . ' 12:59:59' : null;


		//for future payments


		$view_all = false;
		$period = '';
		if (  isset( $_POST['period']  ) ) {
			$period =  $_POST['period'];
			if ( $period == 'last_year' ) {
				$start_date =  $ly_start_date;
				$end_date =  $ly_end_date;
			} elseif ( $period == 'this_year' ) {
				$start_date =  $ty_start_date;
				$end_date =  $ty_end_date;
			} elseif ( $period == 'this_month' ) {
				$start_date =  $tm_start_date;
				$end_date =  $tm_end_date;
			} elseif ( $period == 'last_month') {
				$start_date =  $lm_start_date;
				$end_date =  $lm_end_date;
			} elseif ( empty($period) && empty($start_date) && empty($start_date) ) {
				$view_all = true;
			}
		} else {
			$view_all = true;
		}

		$turnover = Poppyz_Statistics::get_total_statistics( $start_date, $end_date, $view_all, false, "turnover", true );
        $data = array(
                "start"=>$start_date,
                "end"=>$end_date,
			    "period"=>$period,
                "turnovers" => $turnover,
        );
		echo json_encode($data);
		die();
	}

    public function get_statistics_graph_data() {

		$date = new DateTime();
		$date_now = $date->format( get_option('date_format') );
		$month = $date->format( 'F' );
		$this_month = $date->format( 'm' );
		$year = $date->format( 'Y' );
		$last_year =  (int)$year - 1;
		$last_month = (int)$this_month - 1;
		$next_month = (int)$this_month + 1;

		//get last year
		$ly_start = mktime(0, 0, 0, 1, 1, $last_year);
		$ly_end = mktime(0, 0, 0, 12, 31, $last_year);
		$ly_start_date  = date( 'Y-m-d 00:00:00', $ly_start );
		$ly_end_date  = date( 'Y-m-d 12:59:59', $ly_end );

		//get last month

		//if current month is January so last month is 12
		if ( $last_month == 0  ) {
			$last_month = 12;
			$year = $year - 1;
		}

		//if current month is December so next month is 1
		if ( $next_month == 13  ) {
			$next_month = 1;
			$nm_year = $year + 1;
		} else {
			$nm_year = $year;
		}

		//get last month
		$lm_start = mktime(0, 0, 0, $last_month, 1, $year);
		$lm_end = mktime(0, 0, 0, $last_month, cal_days_in_month(CAL_GREGORIAN, $last_month, $year), $year);
		$lm_start_date  = date( 'Y-m-d 00:00:00', $lm_start );
		$lm_end_date  = date( 'Y-m-d 12:59:59', $lm_end );

		//get this year
		$ty_start = mktime(0, 0, 0, 1, 1, $year);
		$ty_end = mktime(0, 0, 0, 12, 31, $year);
		$ty_start_date  = date( 'Y-m-d 00:00:00', $ty_start );
		$ty_end_date  = date( 'Y-m-d 12:59:59', $ty_end );

		//get this month
		$tm_start = mktime(0, 0, 0, $this_month, 1, $year);
		$tm_end = mktime(0, 0, 0, $this_month, cal_days_in_month(CAL_GREGORIAN, $this_month, $year), $year);
		$tm_start_date  = date( 'Y-m-d 00:00:00', $tm_start );
		$tm_end_date  = date( 'Y-m-d 12:59:59', $tm_end );

		$start_date = ( !empty( $_GET['start-date'] ) )  ? $_GET['start-date'] . ' 00:00:00' : null;
		$end_date = ( !empty( $_GET['end-date'] ) )  ? $_GET['end-date'] . ' 12:59:59' : null;


		//for future payments

		//get up to next month
		$nm_start = mktime(0, 0, 0, $next_month, 1, $nm_year);
		$nm_end = mktime(0, 0, 0, $next_month, 31, $nm_year);
		$nm_start_date  = date( 'Y-m-d 00:00:00', $nm_start );
		$nm_end_date  = date( 'Y-m-d 12:59:59', $nm_end );

		//get up to next year
		$ny_start = mktime(0, 0, 0, $this_month, 1, $year);
		$ny_start_date  = date( 'Y-m-d 00:00:00', $ny_start );
		$ny_end_date = date("Y-m-d 12:59:59", strtotime(date("Y-m-d", $ny_start) . " + 1 year" ) );
		$view_all = false;
		$period = '';
		if (  isset( $_POST['graph_period']  ) ) {
			$period =  $_POST['graph_period'];
			if ( $period == 'last_year' ) {
				$start_date =  $ly_start_date;
				$end_date =  $ly_end_date;
			} elseif ( $period == 'this_year' ) {
				$start_date =  $ty_start_date;
				$end_date =  $ty_end_date;
			} elseif ( $period == 'this_month' ) {
				$start_date =  $tm_start_date;
				$end_date =  $tm_end_date;
			} elseif ( $period == 'last_month') {
				$start_date =  $lm_start_date;
				$end_date =  $lm_end_date;
			} elseif ( $period == 'next_month') {
				$start_date =  $nm_start_date;
				$end_date =  $nm_end_date;
			} elseif ( empty($period) && empty($start_date) && empty($start_date) ) {
				$view_all = true;
			}
		} else {
			$view_all = true;
		}
		$data = array(
			"start" => $start_date,
			"end" => $end_date
		);
		$total_statistics = Poppyz_Statistics::get_total_statistics($start_date, $end_date, $view_all, true, null, true);
		$total_statistics["start"] = $start_date;
		$total_statistics["end"] = $end_date;
		$total_statistics["post"] = $_POST;
		$total_statistics["view_all"] = $view_all;


        if ( isset($_POST['type']) && $_POST['type'] == "overdue" ) {
			echo json_encode($total_statistics);
			die();
		} else {
			if ( $period == 'last_month') {
				foreach ( $total_statistics['data_by_day'] as $d => $v ) {
					$bar_data[] = array( strval($d), intval( $v ) );
				}
			} else {
				foreach ( $total_statistics['data_by_month'] as $m => $v ) {
					$bar_data[] = array( $m, intval( $v ) );
				}
			}
			echo json_encode($bar_data);
			die();
		}
	}

    public function get_total_earnings() {
		$total_statistics = Poppyz_Statistics::get_total_statistics(false, false, true, false);
		$total_earnings = $total_statistics['total_paid_payments'];
        echo $total_earnings;
        die();
	}

    public function get_remaining_payments_statistics() {
		$payment_method = (!empty($_POST['payment_method'])) ? $_POST['payment_method'] : null;
		$date_in_12_months = date('Y-m-d', strtotime('+12 month', strtotime(date('Y-m-d'))));
		$start_date = ( isset( $_POST['from'] ) && (empty( $_POST['reset'] ) && $_POST['reset'] == false ) ) ? strtotime($_POST['from']) : strtotime(date('Y-m-d'));
		$end_date = ( isset( $_POST['to'] ) && (empty( $_POST['reset'] ) && $_POST['reset'] == false ) ) ? strtotime($_POST['to']) : strtotime($date_in_12_months);
		$all = false;
		$remaining_payments = Poppyz_Statistics::get_remaining_payments( $start_date, $end_date, $payment_method, $all );
		$remaining_payments['post'] = $_POST;
        if ( isset( $_POST['reset']) && $_POST['reset'] == true ) {
			$remaining_payments['from'] = date('Y-m-d');
			$remaining_payments['to'] = $date_in_12_months;
		}

        echo json_encode($remaining_payments);
        die();
	}
}
