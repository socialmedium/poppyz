<?php

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}


/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 *
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 *
 * Our theme for this list table is going to be movies.
 */
class Poppyz_Coupon_List_Table extends WP_List_Table {

    /**
     * Number of results to show per page
     *
     * @var string
     * @since 0.8.9
     */
    public $per_page = 30;

    /**
     *
     * Total number of discounts
     * @var string
     * @since 0.8.9
     */
    public $total_count;

    /**
     * Active number of discounts
     *
     * @var string
     * @since 0.8.9
     */
    public $active_count;

    /**
     * Inactive number of discounts
     *
     * @var string
     * @since 0.8.9
     */
    public $inactive_count;


    public $coupon_object;


    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'coupon',     //singular name of the listed records
            'plural'    => 'coupons',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

        $this->coupon_object = new Poppyz_Coupon();

    }

    /**
     * Get coupon codes count
     *
     * @access public
     * @since 1.4
     * @return void
     */
    public function get_coupon_code_counts() {
        $coupon_count  = wp_count_posts( PPY_COUPON_PT );
        $this->active_count   = $coupon_count->active;
        $this->inactive_count = $coupon_count->inactive;
        $this->total_count    = $coupon_count->active + $coupon_count->inactive;
    }

    /** ************************************************************************
     * Recommended. This method is called when the parent class can't find a method
     * specifically build for a given column. Generally, it's recommended to include
     * one method for each column you want to render, keeping your package class
     * neat and organized. For example, if the class needs to process a column
     * named 'title', it would first see if a method named $this->column_title()
     * exists - if it does, that method will be used. If it doesn't, this one will
     * be used. Generally, you should try to use custom column methods as much as
     * possible.
     *
     * Since we have defined a column_title() method later on, this method doesn't
     * need to concern itself with any column with a name of 'title'. Instead, it
     * needs to handle everything else.
     *
     * For more detailed insight into how columns are handled, take a look at
     * WP_List_Table::single_row_columns()
     *
     * @param array $item A singular item (one full row's worth of data)
     * @param array $column_name The name/slug of the column to be processed
     * @return string Text or HTML to be placed inside the column <td>
     **************************************************************************/
    function column_default($item, $column_name){
        switch($column_name){
            case 'code':
            case 'status':
            case 'amount':
            case 'coupon_usages':
                return $item[$column_name];
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }

    /*public function get_views() {
        $base           = admin_url('edit.php');

        $current        = isset( $_GET['status'] ) ? $_GET['status'] : '';
        $total_count    = '&nbsp;<span class="count">(' . $this->total_count    . ')</span>';
        $active_count   = '&nbsp;<span class="count">(' . $this->active_count . ')</span>';
        $inactive_count = '&nbsp;<span class="count">(' . $this->inactive_count  . ')</span>';

        $views = array(
            'all'		=> sprintf( '<a href="%s"%s>%s</a>', remove_query_arg( 'status', $base ), $current === 'all' || $current == '' ? ' class="current"' : '', __('All' ) . $total_count ),
            'active'	=> sprintf( '<a href="%s"%s>%s</a>', add_query_arg( 'status', 'active', $base ), $current === 'active' ? ' class="current"' : '', __('Active' ) . $active_count ),
            'inactive'	=> sprintf( '<a href="%s"%s>%s</a>', add_query_arg( 'status', 'inactive', $base ), $current === 'inactive' ? ' class="current"' : '', __('Inactive') . $inactive_count ),
        );

        return $views;
    }*/


    /** ************************************************************************
     * Recommended. This is a custom column method and is responsible for what
     * is rendered in any column with a name/slug of 'title'. Every time the class
     * needs to render a column, it first looks for a method named
     * column_{$column_title} - if it exists, that method is run. If it doesn't
     * exist, column_default() is called instead.
     *
     * This example also illustrates how to implement rollover actions. Actions
     * should be an associative array formatted as 'slug'=>'link html' - and you
     * will need to generate the URLs yourself. You could even ensure the links
     *
     *
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_title($item){

        $coupon     = get_post( $item['ID'] );
        $id = $coupon->ID;
        $row_actions  = array();

        $row_actions['edit'] = '<a href="' . add_query_arg( array( 'ppy-action' => 'edit-coupon', 'coupon' => $id ) ) . '">' . __( 'Edit' ) . '</a>';

        if( strtolower( $item['status'] ) == 'active' ) {
            $row_actions['deactivate'] = '<a href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'deactivate-coupon', 'coupon' => $id ) ), 'ppy-nonce' ) ) . '">' . __( 'Deactivate' ) . '</a>';
        } elseif( strtolower( $item['status'] ) == 'inactive' ) {
            $row_actions['activate'] = '<a href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'activate-coupon', 'coupon' => $id ) ), 'ppy-nonce' ) ) . '">' . __( 'Activate' ) . '</a>';
        }

        $row_actions['delete'] = '<a href="' . esc_url( wp_nonce_url( add_query_arg( array( 'ppy-action' => 'delete-coupon', 'coupon' => $id ) ), 'ppy-nonce' ) ) . '">' . __( 'Delete' ) . '</a>';

        return stripslashes( $item['title'] ) . $this->row_actions( $row_actions );
    }



    /** ************************************************************************
     * REQUIRED if displaying checkboxes or using bulk actions! The 'cb' column
     * is given special treatment when columns are processed. It ALWAYS needs to
     * have it's own method.
     *
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item['ID']                //The value of the checkbox should be the record's id
        );
    }


    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     *
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     *
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_columns(){
        global $ppy_lang;
        $columns = array(
            'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
            'title'     => __('Title'),
            'code'    => __('Code','poppyz'),
            'status'  => __('Status','poppyz'),
            'amount'  => __('Amount','poppyz'),
            'coupon_usages'  => __('Usage','poppyz')
        );
        return $columns;
    }


    /** ************************************************************************
     * Optional. If you want one or more columns to be sortable (ASC/DESC toggle),
     * you will need to register it here. This should return an array where the
     * key is the column that needs to be sortable, and the value is db column to
     * sort by. Often, the key and value will be the same, but this is not always
     * the case (as the value is a column name from the database, not the list table).
     *
     * This method merely defines which columns should be sortable and makes them
     * clickable - it does not handle the actual sorting. You still need to detect
     * the ORDERBY and ORDER querystring variables within prepare_items() and sort
     * your data accordingly (usually by modifying your query).
     *
     * @return array An associative array containing all the columns that should be sortable: 'slugs'=>array('data_values',bool)
     **************************************************************************/
    function get_sortable_columns() {
        $sortable_columns = array(
            'title'     => array('title',false ),     //true means it's already sorted
            'code'    => array( 'code', false ),
            'status'  => array( 'status', false ),
            'amount'  => array( 'amount', false ),
        );
        return $sortable_columns;
    }


    /** ************************************************************************
     * Optional. If you need to include bulk actions in your list table, this is
     * the place to define them. Bulk actions are an associative array in the format
     * 'slug'=>'Visible Title'
     *
     * If this method returns an empty value, no bulk action will be rendered. If
     * you specify any bulk actions, the bulk actions box will be rendered with
     * the table automatically on display().
     *
     * Also note that list tables are not automatically wrapped in <form> elements,
     * so you will need to create those manually in order for bulk actions to function.
     *
     * @return array An associative array containing all the bulk actions: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_bulk_actions() {
        $actions = array(
            'activate'    => __( 'Activate' ),
            'deactivate'    => __( 'Deactivate' ),
            'delete'    => __( 'Delete' ),
        );
        return $actions;
    }


    /** ************************************************************************
     * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
     * For this example package, we will handle it in the class to keep things
     * clean and organized.
     *
     * @see $this->prepare_items()
     **************************************************************************/
    public function process_bulk_action() {

        // security check!
        if ( isset( $_POST['_wpnonce'] ) && ! empty( $_POST['_wpnonce'] ) ) {

            $nonce  = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );
            $action = 'bulk-' . $this->_args['plural'];

            if ( ! wp_verify_nonce( $nonce, $action ) )
                wp_die( 'Nope! Security check failed!' );

        }


        $ids = isset( $_POST['coupon'] ) ? $_POST['coupon'] : false;
        if ( ! is_array( $ids ) )
            $ids = array( $ids );


        foreach ( $ids as $id ) {
            if ( 'delete' === $this->current_action() ) {
                $this->coupon_object->remove_coupon( $id );
            }
            if ( 'activate' === $this->current_action() ) {
                $this->coupon_object->update_coupon_status( $id, 'active' );
            }
            if ( 'deactivate' === $this->current_action() ) {
                $this->coupon_object->update_coupon_status( $id, 'inactive' );
            }
        }

    }

    public function get_data() {
        $coupon_data = array();

        $per_page = $this->per_page;

        $orderby 		= isset( $_GET['orderby'] )  ? $_GET['orderby']                  : 'ID';
        $order 			= isset( $_GET['order'] )    ? $_GET['order']                    : 'DESC';
        $status 		= isset( $_GET['status'] )   ? $_GET['status']                   : array( 'active', 'inactive' );
        $meta_key		= isset( $_GET['meta_key'] ) ? $_GET['meta_key']                 : null;
        $search         = isset( $_GET['s'] )        ? sanitize_text_field( $_GET['s'] ) : null;

        $coupons = $this->coupon_object->get_coupons( array(
            'posts_per_page' => $per_page,
            'paged'          => isset( $_GET['paged'] ) ? $_GET['paged'] : 1,
            'orderby'        => $orderby,
            'order'          => $order,
            'post_status'    => $status,
            'meta_key'       => $meta_key,
            's'              => $search
        ) );

        if ( $coupons ) {
            foreach ( $coupons as $coupon ) {
                $id =  $coupon->ID;

                $start_date = $this->coupon_object->get_coupon_start_date( $id );

                if ( ! empty( $start_date ) ) {
                    $coupon_start_date =  date_i18n( get_option( 'date_format' ), strtotime( $start_date ) );
                } else {
                    $coupon_start_date = __( 'No start date', 'easy-digital-downloads' );
                }

                if ( $this->coupon_object->get_coupon_end_date( $id ) ) {
                    $expiration = date_i18n( get_option( 'date_format' ), strtotime(  $this->coupon_object->get_coupon_end_date( $id )  ) );
                } else {
                    $expiration = __( 'No expiration' );
                }

                $coupon_data[] = array(
                    'ID' 			=> $id,
                    'title' 	    => '<a href="' . add_query_arg( array( 'ppy-action' => 'edit-coupon', 'coupon' => $id ) ) . '">' . get_the_title( $id ) . '</a>',
                    'code' 			=> $this->coupon_object->get_coupon_code( $id ),
                    'amount' 		=> $this->coupon_object->format_coupon_rate( $this->coupon_object->get_coupon_type( $id ), $this->coupon_object->get_coupon_amount( $id ) ),
                    'start_date' 	=> $coupon_start_date,
                    'expiration'	=> $expiration,
                    'status'		=> $this->coupon_object->is_coupon_expired( $id ) ? 'expired' : $coupon->post_status,
                    'coupon_usages' => $this->coupon_object->get_coupon_usages( $id )
                );
            }
        }

        return $coupon_data;
    }

    /** ************************************************************************
     * REQUIRED! This is where you prepare your data for display. This method will
     * usually be used to query the database, sort and filter the data, and generally
     * get it ready to be displayed. At a minimum, we should set $this->items and
     * $this->set_pagination_args(), although the following properties and methods
     * are frequently interacted with here...
     *
     * @global WPDB $wpdb
     * @uses $this->_column_headers
     * @uses $this->items
     * @uses $this->get_columns()
     * @uses $this->get_sortable_columns()
     * @uses $this->get_pagenum()
     * @uses $this->set_pagination_args()
     **************************************************************************/
    function prepare_items() {
        global $wpdb; //This is used only if making any database queries

        /**
         * First, lets decide how many records per page to show
         */
        $per_page = 30;


        /**
         * REQUIRED. Now we need to define our column headers. This includes a complete
         * array of columns to be displayed (slugs & titles), a list of columns
         * to keep hidden, and a list of columns that are sortable. Each of these
         * can be defined in another method (as we've done here) before being
         * used to build the value for our _column_headers property.
         */
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();


        /**
         * REQUIRED. Finally, we build an array to be used by the class for column
         * headers. The $this->_column_headers property takes an array which contains
         * 3 other arrays. One for all columns, one for hidden columns, and one
         * for sortable columns.
         */
        $this->_column_headers = array($columns, $hidden, $sortable);


        /**
         * Optional. You can handle your bulk actions however you see fit. In this
         * case, we'll handle them within our package just to keep things clean.
         */
        $this->process_bulk_action();


        /**
         * Instead of querying a database, we're going to fetch the example data
         * property we created for use in this plugin. This makes this example
         * package slightly different than one you might build on your own. In
         * this example, we'll be using array manipulation to sort and paginate
         * our data. In a real-world implementation, you will probably want to
         * use sort and pagination data to build a custom query instead, as you'll
         * be able to use your precisely-queried data immediately.
         */
        $data = $this->get_data();


        /**
         * This checks for sorting input and sorts the data in our array accordingly.
         *
         * In a real-world situation involving a database, you would probably want
         * to handle sorting by passing the 'orderby' and 'order' values directly
         * to a custom query. The returned data will be pre-sorted, and this array
         * sorting technique would be unnecessary.
         */
        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'title'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');


        /***********************************************************************
         * ---------------------------------------------------------------------
         * vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
         *
         * In a real-world situation, this is where you would place your query.
         *
         * For information on making queries in WordPress, see this Codex entry:
         * http://codex.wordpress.org/Class_Reference/wpdb
         *
         * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         * ---------------------------------------------------------------------
         **********************************************************************/


        /**
         * REQUIRED for pagination. Let's figure out what page the user is currently
         * looking at. We'll need this later, so you should always include it in
         * your own package classes.
         */
        $current_page = $this->get_pagenum();

        /**
         * REQUIRED for pagination. Let's check how many items are in our data array.
         * In real-world use, this would be the total number of items in your database,
         * without filtering. We'll need this later, so you should always include it
         * in your own package classes.
         */
        $total_items = count($data);


        /**
         * The WP_List_Table class does not handle pagination for us, so we need
         * to ensure that the data is trimmed to only the current page. We can use
         * array_slice() to
         */
        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);



        /**
         * REQUIRED. Now we can add our *sorted* data to the items property, where
         * it can be used by the rest of the class.
         */
        $this->items = $data;


        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }




}



/** *************************** RENDER TEST PAGE ********************************
 *******************************************************************************
 * This function renders the admin page and the example list table. Although it's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and
 * it's the way the list tables are used in the WordPress core.
 */
function tt_render_list_page(){

    //Create an instance of our package class...
    $testListTable = new Poppyz_Coupon_List_Table();
    //Fetch, prepare, sort, and filter our data...
    $testListTable->prepare_items();

    global $ppy_lang;

    ?>
    <div class="wrap white">

        <div id="icon-users" class="icon32"><br/></div>
        <h2>List Table Test</h2>

        <div style="background:#ECECEC;border:1px solid #CCC;padding:0 10px;margin-top:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;">
            <p>This page demonstrates the use of the <tt><a href="http://codex.wordpress.org/Class_Reference/WP_List_Table" target="_blank" style="text-decoration:none;">WP_List_Table</a></tt> class in plugins.</p>
            <p>For a detailed explanation of using the <tt><a href="http://codex.wordpress.org/Class_Reference/WP_List_Table" target="_blank" style="text-decoration:none;">WP_List_Table</a></tt>
                class in your own plugins, you can view this file <a href="<?php echo admin_url( 'plugin-editor.php?plugin='.plugin_basename(__FILE__) ); ?>" style="text-decoration:none;">in the Plugin Editor</a> or simply open <tt style="color:gray;"><?php echo __FILE__ ?></tt> in the PHP editor of your choice.</p>
            <p>Additional class details are available on the <a href="http://codex.wordpress.org/Class_Reference/WP_List_Table" target="_blank" style="text-decoration:none;">WordPress Codex</a>.</p>
        </div>

        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
        <form id="coupon-filter" method="get">
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <!-- Now we can render the completed list table -->
            <?php $testListTable->display() ?>
        </form>


    </div>
    <?php
}