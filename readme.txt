=== Poppyz ===
Contributors: jomsky
Tags: course, subscriptions, ideal, mollie
Requires at least: 3.0
Tested up to: 5.9.2
License: Commercial

A course plugin with paid subscriptions. Uses MoneyBird, Mollie, MailChimp, Wishlist Member and Autorespond.nl.

== Installation ==
* Download the zip file
* Upload via WordPress admin: Plugins -> Add New -> Upload Plugin.
* Activate the plugin
* Go to the settings page to configure the plugin.

== Changelog ==

= 1.9.4.9 19/02/25
* Fixed invoices not displaying on certain websites

= 1.9.4.8 17/02/25
* Fixed return page issue for test payments

= 1.9.4.7 05/02/25
* Fixed referral URL in WP affiliate when used in the order page.

= 1.9.4.6 26/01/25
* Fixed logout redirect URL not working.

= 1.9.4.5 09/12/24
* Fixed statistics page's loading time.
* Fixed PHP 7 errors.
* Make Return Page required.

= 1.9.4.4 22/11/24
* Fixed donations adding .02 to the total amount in Mollie.
* Fixed errors on the statistics page.

= 1.9.4.3 21/11/24
* Fixed saving of upsell issues.

= 1.9.4.2 20/11/24
* Fixed invoice search.
* Fixed upsell description issue.
* Fixed turnover data.

= 1.9.4.1 19/11/24
* Fixed payment method not available issue.

= 1.9.4.0 12/11/24
* Added new sections in the Statistics page and updated the code to have correct amount values
* Fixed donation issues when trying to donate after purchasing the tier without an address.

= 1.9.3.6 1/11/24
* Fix not being able to buy a free tier again for donation
* Fix lesson list displaying all lessons when use has no access via tier rules
* Remove free products from being selected as an upsell product

= 1.9.3.5 25/10/24
* Allow 5 upsells to be sold
* Test invoices are now separate from live invoices. The letter "T" is added on the start of their invoice numbers.
* Add a new feature to add additional content on the purchase page called "Extra Content". This can be found on plugin settings and on each tier.

= 1.9.3.4 17/10/24
* Fixed double content on the purchase page
* Fixed lesson sorting
* Fixed wrong amount without tax in the invoice when vat is shifted

= 1.9.3.3 11/10/24
* Output a notification when no tier has been selected when adding a subscription in the user's profile page.
* Fix coupon form alignment
* Fix promotional discount issue when price gets to negative
* Removed promotional discount in the invoice of additional products

= 1.9.3.2 02/10/24
* When clicking on the mark as read button on lessons, scroll back to the button after page reloads

= 1.9.3.1 24/09/24
* Include students added by the admin in the students metabox section of a tier
* Students page - Prevent mollie from executing when a student was only added manually by an admin so there are no actual mollie subscriptions to cancel
* Fixed upsell issues with additional products
* Fixed vat % formatting in the purchase page


= 1.9.3 26/08/24
* Added Poppyz menu items to the admin bar
* Add PlugandPay API integration. API Key should be added on plugin settings to use this feature.
* Fixed invoice not displaying on additional products when upsell is enabled
* Scheduled donation fix
* Fixed additional product and donation issues
* Change the donation button's image upload to use media upload
* Fixed layout issues
* CSS clean up

= 1.9.2.2 30/07/24
* Add discount implementation (first payment/all payments) feature
* Fix credit invoice showing when price of invoice is 0
* Remove orders table
* Donation - send admin notification
* Fix PHP warnings
* Translation fixes

= 1.9.2.1 10/07/24
* Prevent the mollie subscription from failing when the webhook initially fails. This issue resulted in some Poppyz subscriptions not being created after payment.

= 1.9.2 30/06/24
* Add discount feature for tiers
* Remove Universal Analytics and replace it with GA 4

= 1.9.1.2 25/06/24
* Fixed progress list not appearing on the lesson widget
* Added a class to the menu item of current lesson being viewed in the lesson widget. This allows us to style it via custom css.

= 1.9.1.1 19/06/24
* Added fix when eproducts are selected for additional product (requires E-product v1.4.8.5)
* Fixed coupons' total payments calculations

= 1.9.1 10/06/24
* Fixed coupons - european notation is now accepted
* Fixed prices in the checkout page

= 1.9.0 21/05/24
* Added new features: Kassakoopje and bundle products
* Added donation feature
* Added tier discount
* UI enhancements

= 1.8.1.2.1 09/04/24
* Remove / appearing on the purchase page

= 1.8.1.2 09/04/24
* Fix manual payment for memberships that had chargebacks

= 1.8.1.1 17/01/24
* Fix alignment of invoices on the account page

= 1.8.1.0 08/01/24
* Fixes to the test mode payments
* Poppyz now allows e-products to be added as upsells (requires the latest E-product plugin)
* Recurring payment fixes

= 1.8.0.6 21/11/23
* Added export all for invoices page
* Activate test api key to users if the live api key is blank.
* Remove the course dropdown to Lesson widget.
* E-products can now be applied to tier upsells.
* Fixed clone course feature.

= 1.8.0.5 14/11/23
* Lowered the minimum payment amount to 0,20 EUR.

= 1.8.0.4 10/11/23
* MoneyBird users can now set the MoneyBird tax rate per tier.
* Corrected the layout of the login message of the account page when a user is not logged in.
* See subscribers in the tier edit page.
* Fixed the Export (all) button on the students page.

= 1.8.0.3 31/10/23
* Fixed export buttons of the invoices page
* Added a feature to allow admins to use a test payment for Mollie on the checkout page. This feature has to be enabled on the plugin settings page by adding a test API key.

= 1.8.0.2 25/10/23
* Fixed wrong amount charged in Mollie for recurring payments when initial amount has been set - the tax amount is not being added if price is entered exclusive of tax.
* Fixed the dates in payment plans on the student page
* Hide cancel membership button on the account page if membership is already cancelled.

= 1.8.0.1 26/09/23
* Adjust future payment to outstanding payment
* Only show the reminder button to students who have at least one payment

= 1.8.0.0 22/09/2023
* New Admin UI.
* Bulk actions added on the students page
* The same tier can now be purchase multiple times by a user.

= 1.7.3.9 31/08/2023
* Fixed missing payments in the students page

= 1.7.3.8 26/08/2023
* Added sort by status on the students page
* Fixed Facebook Pixel error when the plugin's directory is deleted.

= 1.7.3.7 07/07/2023
* Sort students page by newest first

= 1.7.3.6 03/07/2023
* Fixed the download buttons on the invoices page
* Fixed translations on the purchase page
* Added new translations for expiry texts
* Added additional ajax call on delete tier to make sure that the warning is accurate and up to date.
* Added an option for admins to allow cancellation of memberships in the account page
* Fixed PHP 8.0 issues
* Fixed coupon users list
* Tiers with subscriptions can no longer be deleted.


= 1.7.3.5 26/06/2023
* The course/lesson content no longer displays publicly when it's been assigned a layout via Divi Theme Builder.
* Deleting a tier now displays a warning, all subscribers will get unsubscribed as well

= 1.7.3.4 13/06/2023
* Made shortcodes work on the tier thank you message
* Fixed invoice filters when using pagination
* Added purchase link expiration feature for tiers
* Added newsletter connection warnings when wrong keys are used.
* Fixed the login link of the registration form for e-products.

= 1.7.3.3 20/05/2023
* Added a reset default texts feature under plugin settings -> extras tab.
* Fixed P&P's Kassakoopje feature. The multiple webhook call per order should be checked in P&P settings https://bit.ly/3oFfOxK
* Fixed missing translations like the second payment text.
* Added coupon users to edit coupon page
* Fixed statistics page date picker

= 1.7.3.2 11/05/2023
* Fixed students page display "User Deleted" on users that have no first names.
* Add new cancel button for each membership subscription in the user's account page.

= 1.7.3.1 19/04/2023
* Global Divi Headers are now only affected on course and lessons pages

= 1.7.3 12/04/2023
* A shortcode [purchase-button tier_id=<tier id>]can now be used to place a program's purchase button on any page. Clicking on it will redirect the user to the order page.
* Global Divi Headers are now hidden when hide menu is enabled in the admin.

= 1.7.2 13/03/2023
* Fixed the bug that does not translate the registration fields to Dutch.

= 1.7.1 09/03/2023
* Fixed Divi content redirecting to edit profile page.

= 1.7.0 16/02/2023
* Fixed tier bug where it can't be edited
* Remove automatic invoice link.

= 1.6.9.9.9 13/02/2023
* Added sort on the students page when using the experimental paging feature.

= 1.6.9.9.8 05/02/2023
* Fixed bug where the payment pending email can't be turned on
* Fix Vat text translation on the invoice

= 1.6.9.9.7 - 16/01/2023
* Added new shortcode for welcome mail - [title-password-link]
* Fixed membership price not showing on the overview page.

= 1.6.9.9.6 - 12/01/2023
* Fixed upsells not showing
* Fixed PHP8 issues on TCPDF
* Fixed limit reached text not showing

= 1.6.9.9.5 - 23/12/2022
* Reworked the students page to allow pagination. This feature has to be enabled for now on plugin settings: Extras -> Turn on paging for the students page
* Fixed translations

= 1.6.9.9.4 - 14/12/2022
* The date of each payment made is now recorded.
* Updated translations

= 1.6.9.9.3 - 14/12/2022
* Added filter to change invoice column titles
* Added setting for logout URL.
* Fixed Redirect URL saving as 'poppyz'

= 1.6.9.9.2 - 09/12/2022
* Cloning of lessonclusters fix.
* Refactored all translations.
* Hide lessoncluster sections in the admin that are not needed.

= 1.6.9.9.1 - 07/12/2022
* Fix mollie error when no key is supplied and a student is deleted on the backend.

= 1.6.9.9.0 - 02/12/2022
* Fixed register page overwriting other pages.
* Added new plugin setting to change "tier has expired" default text.

= 1.6.9.8.9 - 02/12/2022
* Prevent P&P payments from double posting.
* Fixed no access message alignment - again.

= 1.6.9.8.8 - 23/11/2022
* Fixed translations
* Fixed no access message alignment
* Remove register page in plugin settings.

= 1.6.9.8.7 - 21/11/2022
* Change translations on the purchase page

= 1.6.9.8.6 - 14/11/2022
* Admin templates can now be overwritten by a child theme/plugin.

= 1.6.9.8.5 - 14/11/2022
* Fix price shown when coupon is applied.
* Center no access messages.
* Add show all lessons option on the lesson widget.

= 1.6.9.8.4 - 04/11/2022
* Make the new MoneyBird API Keys compatible with Poppyz

= 1.6.9.8.3 - 31/10/2022
* Fixed tier not loading properly because of a weird case error in the Newsletters directory.

= 1.6.9.8.2 - 26/10/2022
* Pending order email notification is now disabled by default (only works for new installs)
* Layout fixes for progress module
* Text fixes for the statistics page

= 1.6.9.8.1 - 26/09/2022
* Removed the progress list checkbox. It is now recommended using the Poppyz Progress List module.
* Added Poppyz prefix for Poppyz Divi modules
* Added a simple validation for MoneyBird credentials.

= 1.6.9.8 - 23/09/2022
* Removed the use of PHP Sessions.
* Fixed PlugandPay payments not subscribing the user when the coupon used leads to a zero price.

= 1.6.9.7 - 15/09/2022
* The subscription date of users now affects their access. Date set in the future (via admin) will block access to the customer's course.
* The welcome mail is now sent as well for Plugandpay purchases.
* The templates files of the frontend can now be overriden by another plugin or theme.
* The admin notification email now sends on free purchases as well.
* The students page now lists the newest students at the top.
* Fixed wrong tier and course name displaying on the thank you email of free tiers.

= 1.6.9.6 - 16/08/2022
* Added a way to set the interval of the second payment.

= 1.6.9.5 - 15/08/2022
* Added a way to set the interval of the second payment.
* Fixed missing price fields of tiers for standard payments.

= 1.6.9.4 - 08/08/2022
* Added a way to set the first payment amount for payment plans

= 1.6.9.3 - 03/06/2022
* Bug fixes on the account page
* Update translations

= 1.6.9.2 - 03/06/2022
* The admin can now set an interval before sending reminder emails for pending transactions.
* Added order id and payment id on hover of the subscription date in the Students page.

= 1.6.9.1 - 02/06/2022
* Fixed javascript errors in the admin

= 1.6.9 - 31/05/2022
* Improved coupon page:
* Made it easier to select tiers and e-products (needs e-products plugin update) which the coupon will be applied to.
* Added a check if the amount entered is a number (bug fix)
* List users of a coupon

= 1.6.8 - 30/05/2022
* Code improvements for newsletters that will make it easier to add new ones in the future.

= 1.6.7 - 18/05/2022
* Added upsell feature for Standard payment tiers

= 1.6.6 - 02/05/2022
* For multisites/Wordpress Networks: a user that exists on a subsite now gets added automatically to another subsite if they try to register.
* Version update fix.

= 1.6.5 - 20/04/2022
* Fixed possible bug with chargebacks.

= 1.6.4 - 20/04/2022
* Fixed a bug where multiple same payments are posted on Poppyz.

= 1.6.3.1 - 18/04/2022
* Fixed lesson category page layout. The newest version of Divi Poppyz theme should be installed as well.

= 1.6.3 - 01/04/2022
* Fixed manual payment of chargebacks not registering.
* Fixed invoice link being automatically added on other emails that don't require invoice

= 1.6.2 - 22/03/2022
* Fixed sorting of lessons and modules.

= 1.6.1
* Fixed iDeal payment not showing on payment methods.

= 1.6.0
* Re-added the Upsell feature. Only works for tiers with payment plans for now.

= 1.5.9.7
* Fixed lesson widget title display, it is now hidden when a user has no access to the course.
* Changed translations on plugin settings
* Fixed PHP warning for AffiliateWP on PHP 8

= 1.5.9.6
* Fixed lesson sorting feature.
* Added coupon used for each subscription in the students page https://i.imgur.com/aSsdVv8.png
* Added payment ID to invoice. Only new invoices after this update will have this feature.
* Payment ID can now be searched on invoices. This search function won't work with previous invoices since they don't have Payment ID's saved.
* Fixed error in mailchimp subscription when first name or last name are missing.
* Multiple subscriptions now takes into account the last subscription for lesson rules.

= 1.5.9.5
* Fixed [tier-link] shortcode displaying the wrong link in emails

* Re-added export function for Students

= 1.5.9.4
* Added popup box on when clicking on send reminder in the students page

= 1.5.9.3
* Removed year option for membership plans because it's no longer supported. Use 12 months instead for every year.

= 1.5.9.2
* Fixed inactive coupons still applying a discount.

= 1.5.9.1
* Fixed invoice link not displaying on invoice emails
* Temporarily disable all upsells while fixing the on demand payment issue.

= 1.5.9
* Refactored email settings
* Added new button for each email that can be used to enable/disable sending
* Payment reminder now sends 6 days before payment date.
* Fixed payment plan text on purchase page.
* Increased the width of the container of the purchase page for loggedin users.

= 1.5.8
* Fixed invoice not showing customer's invoice number
* Added a way to disable sending of new user email to the customer in plugin settings
* Added a way to change the purchase button text

= 1.5.7
* Removed upsell for standard payments fow now while the bug is being fixed.
* Run an update that cancels mollie subscriptions that were improperly created for standard payments

= 1.5.6
* Added delete key for date inputs
* Added a new filter for vat shifted invoices.
* Standard payments can now have upsells.

= 1.5.5
* Added all shortcodes to the invoice email

= 1.5.4
* Added [tier] and [tier-link] shortcodes in the invoice email.
* Remove tax display when tier's price is 0 due to using a coupon.

= 1.5.3
* Fixed invoice logo error
* Added new design support for Divi progress list module.

= 1.5.2
* Fixed free programs via coupon not dispaying the correct thank you content.

= 1.5.1
* Users that don't have a company won't be applied the vat shifted status.
* Users that belong in the same country as the one set in Poppyz settings won't have a vatshifted status.

= 1.5.0
* Fixed tax percentages not displaying decimal points.
* Added a new Poppyz extension support: Poppyz Tax Rate by Country
* Reworked how taxes work.
* Fixed ui issues in the admin
* Fixed required fields checkbox on registration form settings
* Added tax conditions for non EU countries
* Translation fixes
* Removed logout link on the Edit Profile page

= 1.4.8
* Added Vat Shifted for MoneyBird. The MoneyBird account should have a tax rate with 0% rate.
* Added a new field "Country" in plugin settings under the Payment tab. This will be used for tax purposes.
* Fixed filter bug on the MoneyBird SDK.
* Remove popup error when pressing enter on Invoice search.

= 1.4.7.1
* UI fixes for Poppyz fields in the admin Profile page
* Fixed error when wrong expiry date is entered on tiers.
* Disable manual typing of data for date input types in the admin

= 1.4.7
* Styling fixes
* Added edit links to lessons' and tiers' titles in the backend.

= 1.4.6
* Fixed lesson list not appearing on module pages
* Fixed wrong confirmation text when trying to delete a lesson in the backend
* Fixed button display.
* Changed plugin author name.

= 1.4.5
* Clicking on a tab in the settings no longer scrolls the page.
* Fixed 1.4.4 tab issue.
* Fixed layout in the purchase page when it has custom content.
* Added leadpages to the selection of Thank You page.
* Fixed missing course edit link in edit tier page.
* Styled lesson links.

= 1.4.4
* Fixed ActiveCampaign tags not saving
* When saving, the currently viewed tab in settings is now retained.

= 1.4.3
* Added debug feature for developers
* Fixed Coupon bulk actions not working
* Change status texts on the students page to Active and Inactive instead of Subscribed and Pending.

= 1.4.2
* Tiers with Payment plans can now be selected as upsells.
* Ui fixes.

= 1.4.1
* Added Upsell feature
* Fixed translations
* Style the View Tier button in the backend.
* Fixed backend styles.

= 1.4.0
* Applied new frontend UI. It is recommended make a backup of your database before updating to this version.

= 1.3.0.4
* Fixed students page only showing 10 rows.

= 1.3.0.3
* Fixed invoice number not displaying properly in the PDF
* Fixed UI issues
* In the Invoices list in the admin, the download buttons now require a selection before downloading.


= 1.3.0.2
* Added ReCAPTCHA V3 on the Poppyz Registration form

= 1.3.0.1
* Fixed ActiveCampaign list not displaying. Some users might have had their lists unselected when they updated a tier because of this bug. Please check your list if you were recently in version 1.3.0 and updated a tier.

= 1.3.0
* Implemented new backend UI. Requires Poppyz Divi Integration 2.6.5, Poppyz E-Products 1.4.1, and Divi Poppyz Theme 1.6.1

= 1.2.3.0
* Fixed Hide Menu settings hiding it non poppyz pages as well
* Fixed admins not being able too see lessons that are excluded in a tier

= 1.2.2.9
* Fixed invoice link in the email
* Fixed purchase page layout, tier information is now on top when viewing on smaller screens.
* Fixed tier's tax inc/exc options not saving when default is used.

= 1.2.2.8
* Fixed lesson progress enable/disable selection not saving correctly.

= 1.2.2.7
* Added course id specification on the lesson list, it can now be used outside lesson and courses by specifying a course in the module settings

= 1.2.2.6
* Added a function that runs a fix on Mollie charges after activating the plugin.

= 1.2.2.5
* Fixed Dutch translation of Thank You email default text with wrong shortcode.

= 1.2.2.4
* Fixed invoice amount when price has a decimal point.

= 1.2.2.3
* Important Update - Fixed Mollie recurring payments having an additional 1 payment.

= 1.2.2.2
* Emergency Update - Fix payment errors for servers with PHP version below 7.3.

= 1.2.2.1
* Fixed custom fields not saving on tiers.

= 1.2.2.0
* Fixed update nag about poppyz license about to expire even if the license is not

= 1.2.1.9
* Fixed invoice amount warnings
* Fixed invoice logo alignment
* Statistics page: section now remains focused and open even if the page reloads
* Statistics page: fixed progress list table
* Update Mollie SDK
* Fixed bug with Facebook Pixel plugin.

= 1.2.1.8
* Lesson list now no longer display lessons that are not available.
* Clicking on an unavailable lesson will display the availability date

= 1.2.1.7
* Updated invoice layout to a cleaner look

= 1.2.1.6
* Add paging and search function on the students page

= 1.2.1.5
* Refactored the payment process
* Added additional filters and parameters on payment completion
* Added notification in the WP dashboard when license is about to expire
* Added IBAN to invoice
* Fixed E-product custom message when purchase limit has been reached
* Added upsell feature after tier payment (in development, not yet available for the public)

= 1.2.1.4
* Fixed wrong description in mailchimp list

= 1.2.1.3
* Fixed a bug where default text are in English even if the site is using another language
* Updated and added new default content for Poppyz Emails
* Poppyz shortcodes now work with emails' subject

= 1.2.1.2
* Fixed MailChimp tags not being updated when the user is already subscribed.

= 1.2.1.1
* Added default country for the registration form in Poppyz settings.
* Fixed notice errors regarding newsletter lists.
* Fixed coupon count not incrementing when tier becomes free due to a coupon.

= 1.2.1.0
* Added descriptive text when uploading featured images on courses and lessons.

= 1.2.0.9
* Major bug: Fixed select box in tiers not saving.

= 1.2.0.8
* MoneyBird: Added Vat Shifted text on the invoice.

= 1.2.0.7
* Fixed Divi not loading because of Facebook Pixel update.

= 1.2.0.6
* Added VAT Number in MoneyBird Invoice (requires adding vat field in a profile of the moneybird account)

= 1.2.0.5
* Removed Vat Shifted field display on the account page
* Fixed purchase page layout for tablets. It now displays the same as desktop.
* Fixed image upload of content editors in plugin settings.
* Added features in the content editors in plugin settings.
* Updated invoice upload logo to use the latest media upload of WP
* Updated meta boxes source code.

= 1.2.0.4
* Fixed a long term bug where customers are marked "paid by admin" incorrectly. This was due to a bug in the backend's edit profile page.
* Vat included/excluded options are now available on per tier basis.

= 1.2.0.3
* Implemented custom thank you page redirects for free tiers because of coupons.

= 1.2.0.2
* Fixed AffiliateWP integration. AffiliateWP plugin should now be version 2.6+ to work.

= 1.2.0.1
* Enchanced category fixes in the admin
* Added featured image in lesson modules if Enchanced Category plugin is not installed.

= 1.2.0.0
* Added Laposta Newsletter integration
* Fixed alignment on the backend's tier lesson list (contributed by: Michael ten Den)

= 1.1.9.9
* Fixed bug on the registration form where phone number is being required even if it's disabled.

= 1.1.9.8
* Changed pending reminder email to 5 days instead of 2
* Fixed reset password link
* Added e-product moneybird support
* Plug&Pay integration: Added first name and last name.

= 1.1.9.7
* Added required phone field for free tiers.

= 1.1.9.6
* Fixed Rest API error on Site Health tool
* Added Extras tab on plugin settings
* Centered notification texts
* Additional translations

= 1.1.9.5
* Added new [company] shortcode for Payment Default Texts

= 1.1.9.4
* Reworked thank you emails. Poppyz E-products plugin users should update Poppyz as well for the emails to work.
* Fixed all shortcodes and created a documentation page on https://poppyz.nl/en/documentation/shortcodes/
* Added a way to change the registration form text in Poppyz Settings -> Registration form

= 1.1.9.3
* Fixed access unavailable layout.

= 1.1.9.2
* Added Dutch - Belgium language
* Remove fix for wrong edit lesson link when lesson list is used, it was hiding the next and previous lesson links. Fix it on Divi Poppyz Theme instead.
* Added a shortcode [user-info] which by default displays the current login users first name. To display the last name just use [user-info field='last_name']

= 1.1.9.1
* Fixed failed payment email - admin sending to the user instead of admin

= 1.1.9.0
* Fixed wrong edit lesson link when lesson list is used
* Added translations

= 1.1.8.9
* Added a way in plugin settings to change the Payment Failed message.
* Free tiers can now be assigned their own Thank You pages and Thank You content (contributed by: Michael ten Den).
* Fixed invoice display for PHP 7.4

= 1.1.8.8
* Added custom text for the invoices of Plug&Pay purchases
* Added a new shortcode to display the remaining slots of a tier. Use [tier-slots id="<id of tier here>"]

= 1.1.8.7
* Fixed some UI bugs for WordPress 5.5

= 1.1.8.6
* Fixed mobile menu layout for Divi's vertical navigation with lesson list. Needs Divi-Poppyz theme update as well.
* Added text on the students page if the subscription is manual or automatic payment.
* Improved statistics page

= 1.1.8.5
* Fix registration form layout
* Added Plugandpay.nl Support
* Enchanced Category support implemented. See documentation for more info. https://poppyz.nl/en/documentation/module-pages/

= 1.1.8.4
* If a payment has failed due to insufficient funds, the Mollie subscription now still continues and the customer is asked to pay the missed payment manually.
* Added new translations

= 1.1.8.3
* Fixed USD currency not being able to save free tiers.

= 1.1.8.2
* Fixed coupons not working on Mollie
* The latest subscription is now taken into account in case of multiple tier subscriptions to a single course.

= 1.1.8.1
* Previous and Next links for Divi Poppyz theme (needs theme update)
* Translation fixes.

= 1.1.8.0
* Google Analytics fix.
* Added new fields on Students export like address and company
* Added thank you page selection for tiers.

= 1.1.7.9 (2020-04-27)
* Fixed interval text for Membership payment plans on the admin
* Fixed payment failed text being out of the main content in Divi (needs divi-poppyz theme update)
* Add module page options in plugin settings (needs divi-poppyz theme update)

= 1.1.7.8 (2020-04-020)
* Fixed loggedin users not being able to purchase

= 1.1.7.7 (2020-04-07)
* Changed loggedin users' initial purchase page styling
* Added new text and validations on vat shifted fields.

= 1.1.7.6 (2020-04-07)
* Fixed not being able to update free tiers.
* Show lesson module slug

= 1.1.7.5 (2020-04-06)
* Added logout link to account and edit profile pages

= 1.1.7.4 (2020-04-02)
* Added a new add invoice screen, experimental only. Do not use unless you know what you are doing.
* Fixed issues when no Mollie API key is set

= 1.1.7.3 (2020-04-01)
* Fixed adding subscription by admin when subscription date is set

= 1.1.7.3 (2020-03-31)
* Fixed free products when Mollie is not set

= 1.1.7.2 (2020-03-31)
* Fixed closing div tag on registration form
* Added phone number to MoneyBird contact

= 1.1.7.1 (2020-03-27)
* Added password reset link using the shortcode [password-link] on Poppyz's Thank You e-mail.

= 1.1.7.0 (2020-03-25)
* Fixed issue preventing users from registering

= 1.1.6.9 (2020-03-24)
* Admin bar will now be hidden to regular users by default. This can be changed via plugin settings.

= 1.1.6.8 (2020-03-23)
* Added an option to disable coupons for customers
* Fixed logout link of Divi's login module when no login page is selected in plugin settings.

= 1.1.6.7 (2020-03-19)
* Add MailerLite Integration
* Fixed issue: Client can't purchase a tier with a payment plan if their first payment is cancelled / does not push thru.
* Added error when Mollie has not payment method enabled
* Fixed translations

= 1.1.6.6 (2020-03-17)
* Fixed pagination of invoices in the admin

= 1.1.6.5 (2020-03-17)
* Fixed free button text

= 1.1.6.4 (2020-03-16)
* Fixed clone function not applying course nad lesson featured images

= 1.1.6.3 (2020-03-16)
* Fixed issue on free tiers when not logged in

= 1.1.6.2
* Added hooks for additional plugin settings like for Poppyz E-Products
* Fixed main menu being hidden on course pages


= 1.1.6.1
* Registration and payment our now combined on the purchase process.
* Make vat shifted text editable

= 1.1.6.0 (2020-02-22)
* Fixed coupons not applying the date restrictions
* Added specific coupon errors.
* Added module shortcode [lesson-modules]
* Added MailChimp tag field for each tier
* Allowed shift key when entering prices to fix issues with other keyboard formats
* Made the tier url a link and added a copy button for easier copying of the tier url

= 1.1.5.7 (2020-02-20)
* Prevent customers from entering prices lower than 1.
* Translation fixes
* Fixed MoneyBird's wrong tax implementation when vat exclusive pricing is selected.

= 1.1.5.6 (2020-02-06)
* Fixed a bug that results to duplicate invoices when Mollie retries to post a payment to the website due to a server issue
* Fixed Overdue notice on the Students list for memberships
* Plugin settings translation texts

= 1.1.5.5 (2020-01-31)
* The new feature since WP 5.3 that needs admin to confirm conflicts with login redirections for admins. So for now it has been disabled.

= 1.1.5.4 (2020-01-31)
* Remove disable autologin option on plugin settings

= 1.1.5.3 (2020-01-28)
* Fix notice in the Progress section of the Statistics page.
* Remove auto login option in the plugin settings

= 1.1.5.2 (2020-01-28)
* Fixed thank you emails not sending

= 1.1.5.2 (2020-01-23)
* Fixed login credentials not sending on registration

= 1.1.5.1 (2020-01-22)
* Changed Charged Back text in the admin to "Chargeback"
* When a chargeback happens for the first payment, client now has to pay via the account page instead of going to the purchase process again

= 1.1.5.0 (2020-01-14)
* Added no VAT option in plugin settings
* Code cleanup
* Add module archive page
* MoneyBird now requires the account's email to be verified before being able to send an invoice, make sure your email is verified.

= 1.1.4.9 (2020-01-08)
* Translation fixes

= 1.1.4.8 (2020-01-08)
* Added a new plugin setting to enable module pages

= 1.1.4.7 (2020-01-06)
* Fixed chargebacks for manual payments
* Fixed wrong invoice displaying for paid chargedbacks

= 1.1.4.6 (2020-01-03)
* Payments for chargedbacks has been fixed
* Fixed chargeback notifications not sending to the admin

= 1.1.4.5 (2019-12-18)
* Added Mollie payment ID integration to MoneyBird

= 1.1.4.4 (2019-12-18)
* Removed MoneyBird tax included option, MoneyBird now gets the option on the Payment Tab.
* Fixed invoice display on the students page

= 1.1.4.3 (2019-12-17)
* Fixed invoices of old subscriptions

= 1.1.4.2 =
* Added add coupon code link on the purchase page instead of showing the input box right away
* Removed last two breadcrumbs

= 1.1.4.1 =
* Convert all tiers to automatic payment, even if they were set to manual before.

= 1.1.4.0 =
* Show MoneyBird links on the account page
* Added a way to change the labels and disable some registration fields
* Remove username and password fields in the registration process. The email will be used for login and the password will be generated.

= 1.1.3.8 =
* Added Student Progress section in the Statistics page
* Changed the use of localization class
* Change default login url to use the selected login page of Poppyz
* Show MoneyBird invoice on the success page when it's activated
* Hide poppyz invoices on the account page when moneybird invoice is activated

= 1.1.3.7 =
* Added future payments amount in the Statistics page
* Fixed AutoRespond purchase when amount is 0.
* Fixed database upgrade

= 1.1.3.6 =
* Unsubscribe users to Mollie subscription when their subscription is deleted on the admin user profile page
* Fixed chargebacks for first payments
* Fixed price having wrong amount on payment plans

= 1.1.3.5 =
* Fixed error in the admin when there are no invoices
* Added a temporary filter for earnings in the invoice page (will transfer to the statistics page on the next update)

= 1.1.3.4 =
* Add reminder for unpaid items in the "cart"
* Change plugin name to Poppyz
* Add a cron job that automatically searches for duplicate invoices and change them
* Fixed module order

= 1.1.3.3 =
* Add reminder for unpaid items in the "cart"

= 1.1.3.2 =
* Fixed wrong tranche number for MoneyBird when using payment plants

= 1.1.3.1 =
* Fixed poppyz invoice sending with MoneyBird invoice for automatic payments
* Remove debugging message on the account page

= 1.1.3.0 =
* Added international support
* Added MoneyBird integration

= 1.1.2.8 =
* Removed debug info on the students page

= 1.1.2.7 =
* Fixed coupon code not working when clicked

= 1.1.2.6 =
* Added feature to add tags for Active campaign and Mailblue per tier

= 1.1.2.5 =
* Fixed the download invoice buttons getting only the first 30 invoices
* Change the invoice number fields to text instead of date

= 1.1.2.4 =
* Added subscription manager tool to enable mass assigning/deleting of subscribers to selected tiers.

= 1.1.2.3 =
* Change Vat Shifted translation

= 1.1.2.2 =
* Fixed a bug where multiple payment buttons are shown for manual payments
* Fixed a bug that duplicates payment for payment plans when Mollie tries to re-ping our webhook because of a prior failure
* Removed loading of script inclusion twice, this fixes all duplicate alert bugs
* Fixed lesson widget opening and closing immediately when clicked

= 1.1.2.1 =
* Fixed error when activating plugin

= 1.1.2.0 =
* Added hooks for Facebook Pixel. This requires Official Facebook Pixel plugin.

= 1.1.1.9 =
* Added course not available text on plugin settings

= 1.1.1.8 =
* Added temporary Facebook Pixel integration hooks

= 1.1.1.7 =
* Grouped email by sections in plugin settings
* Invalid dates on the students page no longer outputs a PHP error, but instead a validation message
* Fixed students page dates column not being translated to the correct language.

= 1.1.1.6 =
* Fixed subscribe button on the students page not working when dutch language is used.

= 1.1.1.5 =
* Grouped email sections on the Emails tab of the plugin settings page.
* Fixed invoice displays of membership plans in the account page
* Fixed membership being recognized as manual payment instead of automatic payment
* Reworked reminder emails on the students page
* Refreshing the actions on the students no longer duplicates the action
* Made actions links to buttons

= 1.1.1.4 =
* Fixed students not being subscribed when admin does it manually if their subscription already expired.

= 1.1.1.3 =
* Upgraded Mollie PHP SDK

= 1.1.1.2 =
* Added a way to export invoices by starting and ending invoice number.
* Added a filter for hiding invoices in the account page

= 1.1.1.1 =
* Fixed a bug where the admin cannot add subscriptions manually on the backend
* Fixed subscriptions that were manually added by admin not appearing on the backend user profile

= 1.1.1.0 =
* Added new payments table to be able to track each payment of a payment plan / membership
* Chargeback fixes
* Subscription that have not been paid no longer appear on the account page.
* Fixed users not being recognized as admin if their subscriptions where manually added from their profile page.
* Fixed plans showing "Paid - Deleted" even if no payment has been made yet.
* Fixed number of payments made being maxed even if it's only the first payment
* Manually added subscriptions now default to manual payment instead of automatic
* Fixed Future payment singular translation
* Fixed vatshifted applying to payment plans even if first payment is non vatshifted.
* Fixed duplicate invoice numbers

= 1.1.0.9 =
* Fixed automatic payments not converting to manual
* Fixed "added by admin" status of subscription not applying
* Fixed return page error when a payment has been converted to manual

= 1.1.0.8 =
* When chargeback happens, convert automatic payments to manual payments immediately.

= 1.1.0.7 =
* Add cron job to check for license and automatically disable the plugin if license is invalid

= 1.1.0.6 =
* Added email column field on student exports

= 1.1.0.5 =
* Fixed a major bug for payment plans and membership subscriptions by disabling purchase of a tier if it has pending manual payments.
* If a license is not activated plugin features are now restricted.

= 1.1.0.4 =
* Fixed invalid amount value in Mollie when the price is in thousands

= 1.1.0.3 =
* Added phone number field for users
* Added phone number integration for ActiveCampaign

= 1.1.0.2 =
* Fixed user profile page not saving because of previous version
* Fixed invoice numbers alignment
* Added a blank (...) selection for the invoice country
* Added translations for country select.


= 1.1.0.1 =
* Add CSV Export for subscribers

= 1.1.0.0 =
* Upgraded Mollie's PHP SDK
* Fixed chargedbacks being run as paid.
* Reprogrammed chargedback and refunds logic, a credit invoice should be made manually by the admin upon getting a chargeback.
* Fixed reminder email's amount value not displaying
* Fixed error on Students page when payment does not exist
* Applied styling to the button for manual payments
* Remove confirm refund button since this is now done automatically via the Mollie API
* Fixed invalid amount error on Mollie API.

= 1.0.9.8 =
* Fixed styling for progress list
* Translation fixes

= 1.0.9.7 =
* Changed how added by admin subscriptions are detected
* Previously added by admin subscriptions before this update will have to be fixed by us to display as "subscribed by admin"
* Prevent double subscriptions for free tiers

= 1.0.9.6 =
* Changed default text reminder mail, added new shortcodes
* Reminder mail now only sends to membership and payment plans that have been paid at least once
* Added admin text for standard payments on the students and account page

= 1.0.9.5 =
* Removed extra reminder fields

= 1.0.9.4 =
* Added new links on the edit tier page for easier access
* Fixed repeated tier links on edit tier page
* Fix manually added subscriptions with payment plans
* Displayed a different text on the status of the subscription in the account page if the subscription was done by the admin

= 1.0.9.3 =
* Displayed a different text on the status of the subscription in the students page if the subscription was done by the admin
* Added default value for Interval to avoid wrong future payment dates in the Students page

= 1.0.9.2 =
* Add chargeback logic
* Fixed failed subscriptions having marked as "Paid" in the Students page
* Added links to courses and tiers in the Students page
* Added a link to view the tier's purchase url in the Edit Tier page.

= 1.0.9.1 =
* Fixed bulk invoice downloads in the admin only downloading 1 invoice
* Update TCPDF to latest version to avoid further PHP 7.2 errors

= 1.0.9.0 =
* Fixed PDF errors for PHP 7.2
* Fixed membership plans which have not been paid on time not appearing on the account page

= 1.0.8.8 =
* Fixed progress list styling when lesson modules are not being used.

= 1.0.8.7 =
* Fixed invoice having the wrong tax percentage when created manually

= 1.0.8.6 =
* Added a message replacing the automatic payment checkbox

= 1.0.8.5
* Added a limit on coupons' usage
* Removed paid text in the invoice if the invoice has not been paid yet
* Fixed a bug with the payment amount for pending orders remaining the same even if the tier price has changed.
* Removed automatic payment checkbox completely since all plans will be automatic

= 1.0.8.4
* Fixed vat shifted field outputting wrong the price.

= 1.0.8.3
* Removed manual payments for payment plans and memberships
* Fixed future payment dates for manual payments in the account page
* Added a way for coupons to include Poppyz e-products.
* Fixed the way Vat Shifted and Belgium settings work.
* Added "Already paid" mark on the invoice.
* Fixed doubled 'invalid coupon' message.
* Fixed terms checkboxes having default value even if they're deleted.

= 1.0.8.2
* Changed layout of invoices - removed unnecessary information
* Fixed purchase title of tiers with Membership plans showing the wrong intervals and text.
* Removed yearly interval on payment plans and memberships because Mollie has a limit of 365 days.

= 1.0.8.1
* Made invoice expiration date always based on the days set in the plugin settings.
* Remove duplicate send reminder link
* Change credit invoice title to Creditfactuur
* Fixed invoice expiration date for payment plans and memberships

= 1.0.8
* If a lesson list is collapsible, the module is now opened when the current lesson being viewed belongs to it.
* Allowed invoices to have negative values for creating credit bills/invoices
* Made a function to duplicate an invoice and make it a credit invoice
* Fixed users not being subscribed to the newsletter and not having a success message when the tier becomes free because of a coupon.
* Updated lessons widget to include progress list

= 1.0.7.1
* Fixed invoices email from field in plugin settings not applying to the send invoice.

= 1.0.7
* Fixed automatic payments invoices not having the right number of payments made

= 1.0.6.2
* Additional translations

= 1.0.6.1
* Translation fixes

= 1.0.6
* Added progress list and mark as read button on courses and lessons.

= 1.0.5 =
* Fixed invoice link in the email
* Added coupon code on the invoices if used
* Fixed account page's invoice links displays

= 1.0.4 =
* Fixed invoice title of automatic recurring payments
* Remove invoices as email attachments. Now there is just a link to the invoice in the email to avoid being labeled as spam. In addition, a new shortcode can be used in the invoice email: [invoice-link]
* Hide email address field of the invoice copy in plugin settings until the checkbox to allow sending is checked.

= 1.0.3 =
* Added invoices in the account and students page
* Fixed thank you mail not sending when no From Email has been supplied in the plugin settings

= 1.0.2 =
* Added sorter for the students page
* Added support for Poppyz E-Products

= 1.0.1.3 =
* Fixed a bug where users that were subscribed manually from the admin dont have access to lessons.

= 1.0.1.2 =
* Added BTW Field activation for Belgium in the plugin settings. Once activated, a btw field will be added on the registration form and invoice.
* Added a way to make custom user fields required or not required through the use of third party plugins

= 1.0.1.1 =
* Fixed tax rate (tax included pricing) displaying wrong when coupon is applied.

= 1.0.1 =
* Fixed overdue warning appearing wrongly on the students page

= 1.0.0 =
* Payment plans and memberships are now supported with  the use of Automatic payments

= 0.9.9.0 =
* If a user is already subscribed to a tier, they will no longer be able to purchase it again unless it was cancelled or has expired.

= 0.9.8.9 =
* Rewrote invoice sorting function
* Add a filter form in the students page
* Add a link to the users in the students page

= 0.9.8.8 =
* Add invoice id to Mollie's payment description
* Remove send reminder link
* Fix invoice sorting function
* Fixed send reminder email in the students page of the admin
* Fixed account page showing all subscriptions as expired
* Add a way in the Students page to rollback last payment if the client refunded.

= 0.9.8.7 =
* Reworked ordering of lesson clusters
* Fixed the error when adding a lesson cluster with the same name as an existing one
* Fixed clone error that occurs when cloning a course twice

= 0.9.8.6 =
* Fixed lesson clusters now being ordered properly with the sorting function

= 0.9.8.5 =
* Fix Statistics page calculations
* Fixed update issue preventing the plugin to see new versions

= 0.9.8.4 =
* Add Export functions to Invoice bulk actions

= 0.9.8.3 =
* Fix account page displaying multiple payment plan subscriptions
* Remove Membership email settings

= 0.9.8.2 =
* Fixed a bug when saving the tax rate amount of 0 in a specific tier.
* Fix translations

= 0.9.8.1 =
* Removed the limit of items when exporting invoices
* Remove listed subscriptions in the admin profile page that aren't active.
* Add download CSV format for Acumulus
* Fix Vat Shifted issues
* Automatically unsubscribe a user if tier is still not paid after the its due date.
* Fixed invoice table displaying wrong prices
* Allow payment of overdue payments

= 0.9.8.0 =
* Add a way for admins to edit the Poppyz profile fields of users in the admin edit profile page
* Fixed a bug that outputs the wrong error in the lesson list when accessing a course that has no available lessons.

= 0.9.7.9 =
* Fixed invoice email not attaching file

= 0.9.7.8 =
* Added FROM fields for emails in the plugin settings
* Add tier name on the review purchase page
* Fix invoice email not applying proper formatting and sender information
* Fixed a bug that doubles the price when tax rate is set to 0 with exclusive to price settings

= 0.9.7.7 =
* Add CSV download for invoices
* Remove email from field in the plugin settings
* Fix admin translations

= 0.9.7.6 =
* Fix text AVR to AVG

= 0.9.7.5 =
* Fix plugin settings box UI
* Fix plugin settings link for the payments page

= 0.9.7.4 =
* Add download invoices as PDF with filters

= 0.9.7.2 =
* Add GDPR compliance
* Add an option in the Student page to not send a thank you email message when manually subscribing a user.

= 0.9.7.1 =
* Fixed Divi Layouts not loading for Lessons.

= 0.9.7.0 =
* Added excerpt on courses and lessons
* Change lesson modules name to lessencluster

= 0.9.6.9 =
* Added more flexibility on how the Purchase Steps are displayed to be more compatible with Divi theme

= 0.9.6.8 =
* Added displays options in the Poppyz Lesson Widget
* Fixed clone course function to include modules and their lesson assignments

= 0.9.6.6 =
* Added Modules - a way to group lessons
* Removed the automatic listing of the lesson list inside the course page, the shortcode [lesson-list] is now required to display the lesson list
* A Divi plugin module can now be installed as a separated plugin to add a "Lesson" module that can be customized.
* Fix invoice pagination in the admin
* Courses no longer display their content if the logged in user has no access.


= 0.9.6.5 =
* Coupons can now be tier specific

= 0.9.6.4
* Made tier price required.

= 0.9.6.3
* Fix the issue of users being able to access some lessons that they are not subscribed to.

= 0.9.6.2
* Add Divi Library to custom post types
* Fix AffiliateWP error

= 0.9.6.1
* Increase width of text boxes in the plugin settings.

= 0.9.6.0.9
* In case a user is subscribed multiple times to the same tier, the last subscribed tier will overwrite the lesson rules of all previous ones.

= 0.9.6.0.8
* Remove dollar currency
* Temporarily remove membership payment method

= 0.9.6.0.7
* Fix tax rate per tier when switching from Poppyz to Poppyz Invoice

= 0.9.6.0.6
* Fix saving of tier when payment details are empty
* Fix saving of tier when a lesson rule is empty
* Change column order of the lesson rules

= 0.9.6.0.5
* Add payment method: Membership

= 0.9.6.0.4
* Fix Invoice computation
* Fix wrong computation in the statistics page
* Convert payment plan method to standard payment method of all tiers when switching between Poppyz Invoice and Poppyz.

= 0.9.6.0.3
* Fix register form not sending.
* Fix tiers not saving when the course it belongs to don't have lessons.

= 0.9.6.0.2 (27/09/2017) =
* Added a feature that makes it possible to add additional fields of any type in the registration process.
* Fix notices and warnings triggered by PHP

= 0.9.6.0.1 (04/08/2017) =
* Fix for USD currency having an invalid format.

= 0.9.6.0.0 (23/05/2017) =
* Added translations
* Fixed a bug in tier rules when setting the week to 0. Setting 0 will now get the next nearest day within the current week.

= 0.9.5.9.9 (23/05/2017) =
* Add starting weekday functionality to tiers admin
* Fix tier rules layout admin

= 0.9.5.9.8 (10/05/2017) =
* Added Visual Builder support for Divi

= 0.9.5.9.7 (19/04/2017) =
* If the price of the tier reaches 0 after a coupon is applied, subscribe the user automatically.

= 0.9.5.9.6 (03/4/2017) =
* Fixed the start time of a lesson using days.

= 0.9.5.9.5 (29/3/2017) =
* Fixed double content showing on the payment process' register page.

= 0.9.5.9.4 (8/3/2017) =
* Added description text for Lesson rules in tier admin.
* Added shortcode support for tiers.

= 0.9.5.9.3 (24/2/2017) =
* Added support for Divi plugin.

= 0.9.5.9.2 (24/2/2017) =
* Fixed a bug that does not display the register page content in the purchase process.


= 0.9.5.9.1 =
* Fixed a bug when cloning courses and lessons which are using Profit Builder.

= 0.9.5.9 =
* Changed layout of registration page notifications
* Fixed a bug that prevents adding of free tiers.

= 0.9.5.8 =
* Change notification texts for email duplicates in the registration form.
* Updated layout of the registration form.
* Do not allow prices to be lower than 1 euro.

= 0.9.5.7 =
* Change some translations for default texts.

= 0.9.5.6 =
* Don't require the terms and conditions anymore.

= 0.9.5.5 =
* Added terms and conditions
* Added subscription expiration dates for tiers.

= 0.9.5.4 =
* Added settings to change the limit reach text.

= 0.9.5.3 =
* Added a limit to the number of subscriptions per tier.
* Fix login redirect not working properly on the frontend login page.
* Fix course content display not logged in text even if the course does not display lessons automatically.
* Added coupon usage count on the coupon admin page.

= 0.9.5.2 =
* Fixed an error that was occuring on old PHP versions.

= 0.9.5.1 =
* Fixed MailChimp subscription.

= 0.9.5.0 =
* Removed MoneyBird Workflow selections in the plugin settings that are not for Invoices.

= 0.9.4.9 =
* Admins can now select a Workflow for MoneyBird in the plugin settings.
* Fixed Thank You Mail's email address not being used.

= 0.9.4.8 =
* Yoast SEO plugin compatibility fix

= 0.9.4.7 =
* Purchase text can now be changed through child plugin or by using Libre Poppyz Theme's customizer.
* Fixed order URL for non SEO friendly permalinks.

= 0.9.4.6 =
* Add a shortcode [lessons-widget] that can be used anywhere in the content to show the list of lessons available to the subscription.

= 0.9.4.5 =
* Fixed the Thank You email not sending when the email field is empty on the plugin settings.

= 0.9.4.4 =
* Added a way for another plugin to add custom MailChimp fields
* Fixed wrong redirect after login when auto login is disabled in the plugin settings

= 0.9.4.3 =
* Remove the no list assigned notication in the Tier admin when Active Campaign list had already been assigned.

= 0.9.4.2 =
* Removed sending of user notifications twice on Autoresponder API
* Removed sending of Thank You mail on Autoresponder API

= 0.9.4.1 =
* Fix newly created users not receiving their login info

= 0.9.4.0 =
* Added filters for custom profile fields and metaboxes. Additional fields and metaboxes can now be added through a plugin.
* Added a restriction filter for tier purchases. A plugin can now be created to restrict access to tier purchase depending on a condition - i.e, address, email, age, etc.

= 0.9.3.8 =
* Fix order links not working when pretty permalink is not turned on
* Hide admin notice when autorespond is being used instead of Mollie and MoneyBird.

= 0.9.3.7 =
* Fix missing arrows of date fields

= 0.9.3.6 =
* Fix a PHP error when activating the plugin

= 0.9.3.5 =
* The thank you message can now be set per tier. If no thank you message is set on a tier it will use the one on the plugin settings page.

= 0.9.3.4 =
* Added new settings tab for custom notifications text
* Increase MailChimp list display to 100


= 0.9.3.3 =
* Fixed email messages not being displayed properly.

= 0.9.3.2 =
* Fixed Active Campaign subscribing users with a test email address.

= 0.9.3.1 =
* When scheduling lessons by day/s, the lesson will be displayed at the beginning of the start day regardless of what time the user subscribed.
* Removed reminder and overdue emails temporarily
* Fixed double content bug when Libre Poppyz is not being used

= 0.9.3.0 =
* Multisite support!
* Updated layout, make sure to update Libre Poppyz theme to version 1.1.6 as well

= 0.9.2.6 =
* Added an option to include tax on tier prices
* Textual changes
* Purchase button change

= 0.9.2.5 =
* Fixed blank page after login

= 0.9.2.4 =
* Added shortcodes for the thank you email message.

= 0.9.2.3 =
* Fixed login redirects

= 0.9.2.2 =
* Removed debug messages

= 0.9.2.1 =
* Fixed the manual subscriptions on the User Profile page not having the correct subscription date.
* Changed the way the plugin handles refunded payment status

= 0.9.2.0 =
* Added Active Campaign newsletter subscription.

= 0.9.1.9 =
* Added default text for the success message and a shortcode.

= 0.9.1.8 =
* Fixed duplicating content
* Fixed the default login redirect not working when the admin leave it blank
* Added new settings for a successful payment.
* Added a new tab in the settings to be able to edit the texts displayed on the purchase process.

= 0.9.1.7 =
* Changed the way our plugin integrates with AffiliateWP. Now the integration happens on the plugin itself.
* Started supporting PHP version 5.3 for MoneyBird v2 - some issues still left.
* The course title in the Purchase page is now changeable by install the Poppyz Custom plugin
* Same with the Free payment button.

= 0.9.1.6 =
* Added support for Thrive Content Editor plugin when cloning a course or a lesson.
* Fixed repeating text notification when switching MoneyBird version.

= 0.9.1.5 =
* Fixed date on lessons that have the 'not yet available' message.
* Fixed user subscription on the WP admin's User page.
* Fixed Tiers label on the User page.


= 0.9.1.4 =
* MoneyBird Version 2 fix for first name not appearing on the MoneyBird Contact.
* Additional dutch translations.

= 0.9.1.3 =
* Coupon code entered is now removed when a user logs out.
* Fixed date translation for lessons.

= 0.9.1.2 =
* Tiers can now be cloned from the edit course page.

= 0.9.1.1 =
* Added fix for Thrive plugin conflict, invoice is not displayed and payment does not go through.
* Fixed notice error when activating the plugin and WP_DEBUG is set to true.
* Fixed login redirect not working on the default wp login (wp-login.php).
* Fixed tier price formatting when saving for the Add Tier form on the course page.
* Added user friendly messages when encountering errors in the payment process instead of a blank page/exception message.
* The register page's content no longer shows after a successful registration.
* Improved how the selection works on the tier page's lesson list.

= 0.9.1 =
* Added AffiliateWP support
* Fixed tier price bug to remove duplicate symbols.
* Fixed dropdown not displaying selected items on the settings page.
* The invoice profile ID now resets when users change they MoneyBird account credentials.
* When purchasing a course using accounts with no names, a prompt will display instead of a blank/error page.
* Changed max amount for payments to 10 digits.


= 0.9.0.1 =
* Fixed country select when user fields are missing on the purchase page.

= 0.9.0.0 =
* Coupons are now available
* Added currency options: USD and Euro.
* Added European notation for dates and prices.

= 0.8.9.5 =
* The price of a tier now only accepts numbers. This is to avoid users adding a currency symbol.

= 0.8.9.4 =
* Users can now choose a Moneybird invoice profile

= 0.8.9.3 =
* Fix redirects happening on the admin page

= 0.8.9.2 =
* Bug fix for courses not showing on the dashboard that are set to private, password protected, or future post.

= 0.8.9.1 =
* Soooo tired of the login problems. Removing login redirect. Frontend and wp-login.php login now both works.

= 0.8.9 =
* Added MoneyBird v2
* The user info: country is now a dropdown field.

= 0.8.8.5 =
* Remove login redirects, use the default wp login page instead

= 0.8.8.4 =
* Small bug fixes

= 0.8.8.3 =
* Fix newsletter subscription on free courses

= 0.8.8.2 =
* Fixed user registration errors
* Backend subscriptions now sends mail as well

= 0.8.8.1 =
* Fixed login redirect errors

= 0.8.8 =
* Added AWeber subscription
* Created a Newsletter class to combine MailChimp and AWeber

= 0.8.7.6 =
* Deleting a course now also delete all its lessons
* Cloning a course also clones its lessons and all meta data

= 0.8.7.5 =
* Added a default image for lesson's without featured posts

= 0.8.7.4 =
* Added the shortcode [lesson-list-image] for courses to show the featured images of lessons instead of a text list
* Added clone function for courses, lessons are not yet duplicated

= 0.8.7.3 =
* Fixed content displaying on the Profit Builder plugin even if the user has no accesss

= 0.8.7.2 =
* Removed Planned Payments

= 0.8.7.1 =
* Added a reminder link on the Students page admin that points directly to the invoice

= 0.8.7 =
* A new invoice is now automatically created after the payment of a subscription with payment plan
* Fixed invoice product name adding payment plan text for standard payments

= 0.8.6.5 =
* Bug fixes

= 0.8.6.4 =
* Invoice text fixes
* Translation fixes

= 0.8.6.3 =
* Added notice when a tier does not have a MailChimp list assigned

= 0.8.6.2 =
* For Autorespond.nl purchases, the user is now sent a link to be able to set his/her password
* Fixed MailChimp bug

= 0.8.6.1 =
* Bug fixes

= 0.8.6 =
* Removed the MailChimp list selection in the settings page
* MailChimp list can now be set per tier
* The settings page can no longer be saved if a license is not supplied.
* Fixed new user notification for Autorespond.nl

= 0.8.5.4 =
* Fixed due dates
* Improved account page
* Improved invoice page

= 0.8.5.3 =
* Modified the account page to make it more clear

= 0.8.5.2 =
* Changed the payment plan interface on the backend
* Due date of invoice is now the due date of the next payment for payment plans, otherwise it's 14 days for standard payments.
* Added a payment link shortcode in reminder emails for direct payment.
* Added tier name and payment number on the invoice
* Remove manual sending of reminders

= 0.8.5.1 =
* Payment Plans: Notify users that have an overdue payment or nearing payment due date
* Manual sending of payment reminders on the admin Students page
* New settings for email messages of payment reminder and overdue notification.

= 0.8.5 =
* Added another method for paying: Payment Plans

= 0.8.4 =
* Changed the dutch translation of tier to 'programmaronde'.

= 0.8.3 =
* Added admin notice when the attachments folder can't be created.
* Fixed login not redirecting properly.

= 0.8.2 =
* More bug fixes

= 0.8.1 =
* Minor bug fixes

= 0.8 =
* Added Payment Plans

= 0.7.7 =
* Removed code that were incompatible with PHP versions < 5.5
* Instead of saving invoices on the server, they are now streamed on the fly.
* Added the account page

= 0.7.6 =
* Added lessons widget

= 0.7.5 =
* Added free tiers - just set the tier price to 0

= 0.7.4 =
* Several bug fixes
* Added admin notices

= 0.7.3 =
* New translations

= 0.7.2 =
* Changed plugin name to Poppyz
* Updated documentation to include dutch video tutorials

= 0.7.1 =
* Code optimizations.
* Added admin notices

= 0.7 =
* Code optimizations.

= 0.6 =
* Created API for Autorespond.nl

= 0.5 =
* Plugin alpha version.