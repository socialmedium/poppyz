<?php

class Poppyz_Analytics_JS {

    /** @var object Class Instance */
    private static $instance;

    /** @var array Inherited Analytics options */
    private static $options;

    /**
     * Get the class instance
     */
    public static function get_instance($options = array()) {
        return null === self::$instance ? (self::$instance = new self($options)) : self::$instance;
    }

    /**
     * Constructor
     * Takes our options from the parent class so we can later use them in the JS snippets
     */
    public function __construct($options = array()) {
        self::$options = $options;
    }

    /**
     * Return one of our options
     * @param  string $option Key/name for the option
     * @return string         Value of the option
     */
    public static function get($option) {
        return self::$options[$option];
    }

    /**
     * Returns the tracker variable this integration should use
     */
    public static function tracker_var() {
        return 'gtag';
    }

    /**
     * Generic GA4 / header snippet for opt out
     */
    public static function header() {
        return "<script async src='https://www.googletagmanager.com/gtag/js?id=" . esc_js(self::get('ga_id')) . "'></script>
                <script type='text/javascript'>
                    window.dataLayer = window.dataLayer || [];
                    function gtag() {
                        dataLayer.push(arguments);
                    }
                    gtag('js', new Date());
                    gtag('config', '" . esc_js(self::get('ga_id')) . "', {
                        'anonymize_ip': " . (self::get('ga_anonymize_enabled') === 'on' ? 'true' : 'false') . "
                    });
                </script>";
    }

    /**
     * Loads the GA4 analytics code
     * @return string         Analytics loading code
     */
    public static function load_analytics() {
        return self::header();
    }

    /**
     * Adds e-commerce transaction to GA4
     * @param array $order
     * @return string Add Transaction Code
     */
    public static function add_transaction($order) {
        $currency = Poppyz_Core::get_option('currency');
        if (!$currency) {
            $currency = 'eur';
        }
        $code = "gtag('event', 'purchase', {
            'transaction_id': '" . esc_js($order['id']) . "',
            'affiliation': '" . esc_js(get_bloginfo('name')) . "',
            'value': '" . esc_js($order['total']) . "',
            'currency': '" . esc_js(strtoupper($currency)) . "',
            'items': " . json_encode(self::get_items($order)) . "
        });";
        return $code;
    }

    /**
     * Retrieves order items for GA4
     * @param array $order
     * @return array List of items
     */
    private static function get_items($order) {
        $items = array(
            array(
                'id' => esc_js($order['order_id']),
                'name' => esc_js($order['name']),
                'category' => esc_js(get_the_title($order['course_id'])),
                'price' => esc_js($order['total']),
                'quantity' => 1
            )
        );
        return $items;
    }
}
