<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      0.5.0
 *
 * @package    Poppyz
 * @subpackage Poppyz/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.5.0
 * @package    Poppyz
 * @subpackage Poppyz/includes
 * @author     Socialmedium <info@socialmedium.nl>
 */
class Poppyz_Activator {


    public static function activate( $networkwide ) {
        global $wpdb;

        if ( function_exists( 'is_multisite' ) && is_multisite() ) {
            // check if it is a network activation - if so, run the activation function for each blog id
            if ( $networkwide ) {
                $old_blog = $wpdb->blogid;
                // Get all blog ids
                $blogids = $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs" );
                foreach ($blogids as $blog_id) {
                    switch_to_blog($blog_id);
                    Poppyz_Activator::_activate();
                }
                switch_to_blog($old_blog);
                return;
            }
        }
        Poppyz_Activator::_activate();
    }

    /**
     * Run SQL queries.
     *
     * Add database tables.
     *
     * @since    0.5.0
     */
    public static function _activate() {
        global $wpdb;
        $table_subscriptions = $wpdb->prefix . "course_subscriptions";

        $charset_collate = $wpdb->get_charset_collate();

        $options = array();
        $options_saved = get_option( 'ppy_settings' );

        Poppyz_Activator::_build_database();

        //run version specific database structure updates

        if ( isset($options_saved['version']) ) {
            $version_now = $options_saved['version'];
            if ( $version_now < 1.0 ) {
                $wpdb->query( "ALTER TABLE $table_subscriptions DROP COLUMN number_of_payments" );
            }
        }

        //create pages required by the plugin if they don't exist yet
        $ppy_lang = new Poppyz_i18n();

        $pages = array(
            //__( 'Register' ) =>  'register',
            __( 'Registreer' ,'poppyz') =>  'register', //requested dutch by default wutface
            __( 'Edit Profile' ,'poppyz') =>  'edit-profile',
            __( 'Login' ,'poppyz') =>  'login',
            __( 'Aanschaf' ,'poppyz') => 'purchase',
            __( 'Account' ,'poppyz') => 'account',
            __( 'Return Page' ,'poppyz') => 'return',
        );


        foreach ( $pages as $p => $s ) {
            $page_id = null;

            /*if (  $options_saved[$s . "_page"] != null && get_post( $options_saved[$s . "_page"] )  ) {
                //a page is already assigned and the page exists, skip page creation
                continue;
            } elseif ( get_page_by_path( $s ) == NULL ||  get_post( $options_saved[$s . "_page"] == null ) )  {*/


            $current_page =  get_page_by_path( $s );

            //if a page is already assigned
            if ( isset( $options_saved[$s . "_page"] ) && $options_saved[$s . "_page"] != null ) {
                if (  $current_page != null && $current_page->post_status != 'trash' ) {
                    //if that page exists, don't do anything
                    continue;
                } else {
                    //if that page does not exist (most probably deleted) empty the value
                    $options_saved[$s . "_page"] = null;
                }
            }

            //no page has been assigned yet
            if ( !isset($options_saved[$s . "_page"]) || $options_saved[$s . "_page"] == null ) {
                if ( $current_page == null || $current_page->post_status == 'trash' )  {
                    //the page does not yet exist and no other pages are assigned yet
                    $create_page = array(
                        'post_title'    => $p,
                        'post_name'     => $s,
                        'post_content'  => '',
                        'post_status'   => 'publish',
                        'post_type'     => 'page'
                    );
                    // Insert the post into the database
                    $page_id = wp_insert_post( $create_page );
                } else {
                    //the page exists, but is not assigned
                    $page = get_page_by_path( $s );
                    $page_id = $page->ID;
                }
            }


            if ( $page_id ) $options_saved[$s . "_page"] = $page_id;

        }

        $optionsClass = new \Poppyz\Includes\Options($options_saved);
        $options_saved = $optionsClass->setDefaultTextOptions();

        if ( empty( $options_saved['currency'] ) ) {
            $options_saved['currency'] = 'eur';
        }

        if ( !isset( $options_saved['pending_email_disabled'] ) ) {
            $options_saved['pending_email_disabled'] = 'on';
        }

        Poppyz_Core::run_updates();

        $options_saved['version'] = PPY_VERSION;
        update_option( 'ppy_settings' , $options_saved );

        /*require_once PPY_DIR_PATH . 'includes/class-poppyz-type.php';
        $plugin_type = new Poppyz_Type( 'poppyz' );
        $plugin_type->register_post_type();
        add_rewrite_endpoint( 'order-id',  EP_PERMALINK | EP_PAGES );*/
        //new Poppyz_API();
        //flush_rewrite_rules();

        Poppyz_Core::protect_attachments();
        Poppyz_Core::setup_payment_reminders();
    }

    public static function _build_database() {
        global $wpdb;
        $table_subscriptions = $wpdb->prefix . "course_subscriptions";
        $table_payments = $wpdb->prefix . "course_subscriptions_payments";

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_subscriptions (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          course_id mediumint(9) NOT NULL,
          user_id mediumint(9) NOT NULL,
          tier_id mediumint(9) NOT NULL,
          status varchar(55) NOT NULL,
          order_id varchar(20) NOT NULL,
          payment_method varchar(55) NOT NULL,
          payment_type varchar(55) NOT NULL,
          payment_status varchar(55) NOT NULL,
          payment_id varchar(55) NOT NULL,
          payment_mode varchar(55) NULL, 
          invoice_id varchar(55) NOT NULL,
          amount decimal(10,2) NOT NULL,
          initial_amount decimal(10,2) NULL,
          discount_amount decimal(10,2) NOT NULL,
          donation_amount decimal(10,2) NOT NULL,
          payments_made tinyint(3) NOT NULL,
          subscription_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          mollie_subscription_id varchar(55) NOT NULL,
          mollie_customer_id varchar(55) NOT NULL,
          vatshifted BOOLEAN,
          vat_out_of_scope BOOLEAN,
          version mediumint(9) NOT NULL,
          UNIQUE KEY id (id)
        ) $charset_collate;";

        $sql .= "CREATE TABLE $table_payments (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          subscription_id mediumint(9) NOT NULL,
          payment_id varchar(55) NOT NULL,
          amount decimal(10,2) NOT NULL,
          invoice_id mediumint(9) NOT NULL,
          invoice_type varchar(55) NOT NULL,
          invoice_id_other varchar(55) NOT NULL,
          customer_id mediumint(9) NOT NULL,
          status varchar(55) NOT NULL,
          number_of_payment mediumint(9) NOT NULL, 
          created timestamp default '0000-00-00 00:00:00',
          updated timestamp null on update current_timestamp,
          UNIQUE KEY id (id)
        ) $charset_collate;";


        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );

    }


    public static function new_blog( $blog_id ) {
        global $wpdb;

        if ( is_plugin_active_for_network( 'poppyz/poppyz.php' ) ) {
            $old_blog = $wpdb->blogid;
            switch_to_blog( $blog_id );
            Poppyz_Activator::_activate();
            switch_to_blog( $old_blog );
        }
    }


}
