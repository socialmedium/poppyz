<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://example.com
 * @since      0.5.0
 *
 * @package    Poppyz
 * @subpackage Poppyz/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      0.5.0
 * @package    Poppyz
 * @subpackage Poppyz/includes
 * @author     Socialmedium <info@socialmedium.nl>
 */
class Poppyz {

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    0.5.0
     * @access   protected
     * @var      Poppyz_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    0.5.0
     * @access   protected
     * @var      string    $poppyz    The string used to uniquely identify this plugin.
     */
    protected $poppyz;

    /**
     * The current version of the plugin.
     *
     * @since    0.5.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the sM Courses and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the Dashboard and
     * the public-facing side of the site.
     *
     * @since    0.5.0
     */


    public function __construct() {

        $this->poppyz = 'poppyz';
        $this->version = PPY_VERSION;

        $this->load_dependencies();
        $this->set_locale();

        $this->register_post_type();
        $this->register_widgets();
        $this->run_plugin_updates();

        $this->define_admin_hooks();
        $this->define_public_hooks();

    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Poppyz_Loader. Orchestrates the hooks of the plugin.
     * - Poppyz_i18n. Defines internationalization functionality.
     * - Poppyz_Admin. Defines all hooks for the dashboard.
     * - Poppyz_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    0.5.0
     * @access   private
     */
    private function load_dependencies() {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once PPY_DIR_PATH . 'includes/class-poppyz-loader.php';

        /**
         * The class responsible for creating post types and taxonomies
         */
        require_once PPY_DIR_PATH . 'includes/class-poppyz-type.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once PPY_DIR_PATH . 'includes/class-poppyz-i18n.php';

        /**
         * The class that registers all widgets
         */
        require_once PPY_DIR_PATH . 'includes/class-poppyz-widgets.php';


        /**
         * The plugin class that handles all subscriptions and payments
         */
        require_once PPY_DIR_PATH . 'includes/class-poppyz-subscription.php';

        /**
         * The class responsible for defining all actions that occur in the Dashboard.
         */
        require_once PPY_DIR_PATH . 'admin/class-poppyz-admin.php';


        /**
         * The class responsible for defining all actions that occur in the Dashboard.
         */
        require_once PPY_DIR_PATH . 'admin/class-poppyz-meta-boxes.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once PPY_DIR_PATH . 'public/class-poppyz-public.php';

        /**
         * The class responsible for Google Analytics tracking
         */
        require_once PPY_DIR_PATH . 'includes/class-poppyz-analytics.php';

        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        if ( is_plugin_active('official-facebook-pixel/facebook-for-wordpress.php') && class_exists('\FacebookPixelPlugin\Integration\FacebookWordpressIntegrationBase'))
            require_once PPY_DIR_PATH . 'includes/facebook-pixel/facebook-pixel.php';

        $this->loader = new Poppyz_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Poppyz_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    0.5.0
     * @access   private
     */
    private function set_locale() {

        $plugin_i18n = new Poppyz_i18n();
        $plugin_i18n->set_domain( $this->get_poppyz() );

        $this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
    }


    /**
     * Register the plugin post types
     *
     * @since    0.5.0
     * @access   private
     */
    private function register_post_type() {
        $plugin_type = new Poppyz_Type( $this->get_poppyz() );
        $this->loader->add_action( 'init', $plugin_type, 'register_post_type' );
    }

    /**
     * Register the plugin widgets
     *
     * @since    0.7.5
     * @access   private
     */
    private function register_widgets() {
        $plugin_widgets = new Poppyz_Widgets();
        $this->loader->add_action( 'widgets_init', $plugin_widgets, 'ppy_load_widgets' );
    }

    private function run_plugin_updates() {
        $this->loader->add_action( 'plugins_loaded', new Poppyz_Core(), 'run_updates' );
    }

    /**
     * Register all of the hooks related to the dashboard functionality
     * of the plugin.
     *
     * @since    0.5.0
     * @access   private
     */
    private function define_admin_hooks() {

        $plugin_admin = new Poppyz_Admin( $this->get_poppyz(), $this->get_version() );

        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
        $this->loader->add_filter( 'in_admin_header', $plugin_admin, 'admin_header');
        $this->loader->add_filter( 'admin_body_class', $plugin_admin, 'admin_body_class');
        $this->loader->add_filter( 'admin_head', $plugin_admin, 'admin_head');

        $this->loader->add_action( 'admin_menu', $plugin_admin, 'add_menu_pages', 5 );
        $this->loader->add_action( 'admin_bar_menu', $plugin_admin, 'admin_bar_item', 99 );
        $this->loader->add_action( 'admin_notices', $plugin_admin, 'admin_notices' );

        $this->loader->add_action( 'admin_post_thumbnail_html', $plugin_admin, 'change_featured_image_text', 11,2 );

        // Add additional custom profile fields that only the admin can edit, e.g Manual Subscriptions
        $this->loader->add_action( 'show_user_profile', $plugin_admin, 'admin_profile_fields' );
        $this->loader->add_action( 'edit_user_profile', $plugin_admin, 'admin_profile_fields' );
        $this->loader->add_action( 'personal_options_update', $plugin_admin, 'admin_save_profile_fields' );
        $this->loader->add_action( 'edit_user_profile_update', $plugin_admin, 'admin_save_profile_fields' );
        $this->loader->add_action( 'edit_user_profile_update', $plugin_admin, 'admin_save_profile_fields' );

        $this->loader->add_action('wp_ajax_populate_tiers', $plugin_admin, 'populate_tiers');
		$this->loader->add_action('wp_ajax_get_users_by_keyword', $plugin_admin, 'getUserByKeyword');
        $this->loader->add_action('wp_ajax_add_subscription', $plugin_admin, 'add_subscription');
        $this->loader->add_action('wp_ajax_delete_subscription', $plugin_admin, 'delete_subscription');
		$this->loader->add_filter('admin_body_class', $plugin_admin, 'addAdminBodyClass');


		//student filter ajax
		$this->loader->add_action('wp_ajax_get_tiers_by_course_ajax', $plugin_admin, 'get_tiers_by_course_ajax');

        //Plugin settings
        $this->loader->add_action('admin_init', $plugin_admin, 'register_settings');
        //$this->loader->add_action('admin_init', $plugin_admin, 'process_settings_page', 999, 0);
        $this->loader->add_action('wp_ajax_process_settings_page', $plugin_admin, 'process_settings_page');

        $this->loader->add_action('wp_ajax_generate_secret_key', $plugin_admin, 'generate_secret_key');
        $this->loader->add_action('wp_ajax_update_license', $plugin_admin, 'update_license');
        $this->loader->add_action('upload_dir', $plugin_admin, 'change_upload_dir');

        $this->loader->add_action('admin_action_ppy_delete_course', $plugin_admin, 'delete_course');
        $this->loader->add_action('admin_action_ppy_clone_course', $plugin_admin, 'clone_course');

        //$this->loader->add_action('user_can_richedit', $plugin_admin, 'user_can_richedit');

        $this->loader->add_action('et_builder_post_types', $plugin_admin, 'et_builder_post_types');
        $this->loader->add_action('et_fb_post_types', $plugin_admin, 'et_builder_post_types');
	    $this->loader->add_filter('et_pb_show_all_layouts_built_for_post_type', $plugin_admin, 'et_pb_show_all_layouts_built_for_post_type');

        //add an ID attribute to the upload media popup
        $this->loader->add_action('init', $plugin_admin, 'admin_init');
        $this->loader->add_action('image_send_to_editor', $plugin_admin, 'image_to_editor', 1, 8);

        //populate invoice data on the Add/Edit Invoice admin page
        $this->loader->add_action('wp_ajax_populate_invoice_data', $plugin_admin, 'populate_invoice_data');
        $this->loader->add_action('wp_ajax_populate_user_subscriptions', $plugin_admin, 'populate_user_subscriptions');

        // Handles the add, update, and delete of coupons and invoices
        $this->loader->add_action('admin_init', $plugin_admin, 'process_entities');

		// Add enctype to tier form
		$this->loader->add_action('post_edit_form_tag', $plugin_admin, 'add_post_enctype');
		//delete image of donation in tier page
		$this->loader->add_action('wp_ajax_delete_additiona_product_image', $plugin_admin, 'deleteTierDonationImage');


		$this->loader->add_action('wp_ajax_upload_donation_image', $plugin_admin, 'uploadTierDonationImage');

		//statistics turnover
		$this->loader->add_action('wp_ajax_statistics_turover', $plugin_admin, 'statistics_turnover_json');

		$this->loader->add_action('wp_ajax_statistics_graph_data', $plugin_admin, 'get_statistics_graph_data');

		$this->loader->add_action('wp_ajax_total_earnings', $plugin_admin, 'get_total_earnings');

		$this->loader->add_action('wp_ajax_remaining_payments_statistics', $plugin_admin, 'get_remaining_payments_statistics');

		//remove additional product image
		$this->loader->add_action('wp_ajax_delete_additiona_product_image', $plugin_admin, 'deleteAdditionalProductImage');


        //register custom meta boxes
        $plugin_metaboxes = new Poppyz_Meta_Boxes( $this->get_poppyz() );

        $this->loader->add_action( 'admin_init', $plugin_metaboxes, 'register_metaboxes', 13 );
        $this->loader->add_action( 'add_meta_boxes', $plugin_metaboxes, 'add_meta_boxes',12 );

        $this->loader->add_action( 'save_post_' . PPY_COURSE_PT, $plugin_metaboxes, 'metabox_lesson_save' );
        $this->loader->add_action( 'save_post', $plugin_metaboxes, 'metabox_tier_fields_save' );

        //AJAX call to create a new lesson
        $this->loader->add_action('wp_ajax_add_new_lesson', $plugin_metaboxes, 'add_new_lesson');
        //AJAX call to delete a lesson
        $this->loader->add_action('wp_ajax_delete_lesson', $plugin_metaboxes, 'delete_lesson');
        //AJAX call to save the order of the lessons
        $this->loader->add_action('wp_ajax_update_lesson_order', $plugin_metaboxes, 'update_lesson_order');
        //AJAX call to save the order of the modules
        $this->loader->add_action('wp_ajax_update_module_order', $plugin_metaboxes, 'update_module_order');

        //AJAX call to add a tier
        $this->loader->add_action('wp_ajax_add_new_tier', $plugin_metaboxes, 'add_new_tier');
        //AJAX call to delete a tier
        $this->loader->add_action('wp_ajax_delete_tier', $plugin_metaboxes, 'delete_tier');
		//AJAX call to get total subscriber of tier
		$this->loader->add_action('wp_ajax_get_tier_total_subscription', $plugin_metaboxes, 'get_tier_total_subscription');
        //AJAX call to clone a tier
        $this->loader->add_action('wp_ajax_clone_tier', $plugin_metaboxes, 'clone_tier');

        $this->loader->add_action('wp_ajax_add_new_module', $plugin_metaboxes, 'add_new_module');
        $this->loader->add_action('wp_ajax_delete_module', $plugin_metaboxes, 'delete_module');
        $this->loader->add_action('admin_init', $plugin_metaboxes, 'exclude_modules');




        //add image field on lesson module


        $this->loader->add_action(PPY_LESSON_CAT . '_add_form_fields', $plugin_admin, 'add_category_image', 10, 2);
        $this->loader->add_action('created_' . PPY_LESSON_CAT, $plugin_admin, 'save_category_image', 10, 2);
        $this->loader->add_action(PPY_LESSON_CAT . '_edit_form_fields', $plugin_admin, 'update_category_image', 10, 2);
        $this->loader->add_action('edited_' . PPY_LESSON_CAT, $plugin_admin, 'updated_category_image', 10, 2);

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'load_media');
        $this->loader->add_action('admin_footer', $plugin_admin, 'add_script');


    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    0.5.0
     * @access   private
     */
    private function define_public_hooks() {
        global $plugin_public; //have to make this global to be able to remove filters on certain instances
        $plugin_public = new Poppyz_Public( $this->get_poppyz(), $this->get_version() );

        //$this->loader->add_action( 'template_redirect', $plugin_public, 'session_start' );
        //$this->loader->add_action( 'wp_loaded', $plugin_public, 'session_close' );

        //add rewrite rules for the purchase order
        $this->loader->add_action( 'init', $plugin_public, 'endpoint' );
        $this->loader->add_action( 'query_vars', $plugin_public, 'query_vars' );

        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );


        $this->loader->add_filter( 'affwp_extended_integrations', $plugin_public, 'affwp_extended_integrations' );
        $this->loader->add_action( 'affwp_integrations_load', $plugin_public, 'affwp_integrations_load' );


        $this->loader->add_action( 'pre_get_posts', $plugin_public, 'lesson_category_archive' );

        //change the text of the return page according to the payment status
        $this->loader->add_filter( 'the_title', $plugin_public, 'change_wp_title' );
        $this->loader->add_filter( 'the_content', $plugin_public, 'return_content', 999999 );

        //sort the next / post links according to their order on the edit course page
        $this->loader->add_filter( 'get_next_post_where', $plugin_public, 'adjacent_post_where' );
        $this->loader->add_filter( 'get_previous_post_where', $plugin_public,  'adjacent_post_where' );
        $this->loader->add_filter( 'get_next_post_sort', $plugin_public,  'adjacent_post_sort' );
        $this->loader->add_filter( 'get_previous_post_sort', $plugin_public,  'adjacent_post_sort' );


        // Redirects wp-login to custom login with some custom error query vars when needed
        $this->loader->add_filter( 'login_url', $plugin_public, 'login_url', 10, 2 );
        // Redirects wp-login to custom login with some custom error query vars when needed
        //$this->loader->add_filter( 'login_init', $plugin_public, 'custom_redirect_login', 2, 2 );
        // Updates login failed to send user back to the custom form with a query var
        //$this->loader->add_filter( 'wp_login_failed', $plugin_public, 'custom_login_failed', 10, 2 );
        // Updates authentication to return an error when one field or both are blank
        //$this->loader->add_filter( 'authenticate', $plugin_public, 'custom_authenticate_username_password', 30, 3);
        // Automatically adds the login form to "login" page
        //$this->loader->add_filter( 'the_content', $plugin_public, 'custom_login_form_to_login_page', 9999 );

        //redirect the registration link to our own registration page
        $this->loader->add_filter( 'login_redirect', $plugin_public, 'login_redirect', 10, 3 );
        $this->loader->add_filter( 'register_url', $plugin_public, 'register_url' );
        $this->loader->add_filter( 'edit_profile_url', $plugin_public, 'edit_profile_url', 10, 3 );
        //process registration and login
        $this->loader->add_filter( 'wp', $plugin_public, 'process_user' );

        //process file access restriction
        $this->loader->add_filter( 'init', $plugin_public, 'process_file' );

        //remove the coupon session upon logout
        $this->loader->add_filter( 'clear_auth_cookie', $plugin_public, 'logout' );

        //ajax process for coupon application
        $this->loader->add_filter( 'wp_ajax_apply_coupon', $plugin_public, 'apply_coupon' );
        //$this->loader->add_filter( 'wp_ajax_nopriv_apply_coupon', $plugin_public, 'apply_coupon' );
        $this->loader->add_filter( 'wp_ajax_remove_coupon', $plugin_public, 'remove_coupon' );

		//ajax check if payment is successful, validation in return page
		$this->loader->add_filter( 'wp_ajax_is_payment_successful', $plugin_public, 'is_payment_successful' );


        $this->loader->add_filter( 'ppy_tier_title', $plugin_public, 'tier_title', 10, 3 );
        $this->loader->add_filter( 'ppy_mailchimp_custom_fields', $plugin_public, 'mailchimp_custom_fields', 10, 3 );

        //decide to show or hide the WP admin bar
        $this->loader->add_filter( 'show_admin_bar', $plugin_public, 'show_admin_bar', 10, 3 );

        $this->loader->add_action( 'wp_logout', $plugin_public, 'logout_redirect', 1 );

        $plugin_payment = new Poppyz_Payment();
        $this->loader->add_action( 'template_redirect', $plugin_payment, 'process_payment', 1 );

        $plugin_invoice = new Poppyz_Invoice();
        $this->loader->add_action( 'init', $plugin_invoice, 'display_invoice', 1 );
        $this->loader->add_action( 'init', $plugin_invoice, 'register_status', 1 );

	    $this->loader->add_filter( 'ppy_custom_fields', $plugin_invoice, 'reg_invoice_fields' );
	    $this->loader->add_filter( 'ppy_custom_fields_html', $plugin_invoice, 'reg_invoice_fields_html', 11, 4 );
	    $this->loader->add_filter( 'ppy_custom_fields_exclude', $plugin_invoice, 'reg_invoice_exclude', 11, 1 );
	    $this->loader->add_filter( 'ppy_exclude_custom_fields_validation', $plugin_invoice, 'reg_invoice_fields_exclude_validation' );
	    $this->loader->add_action( 'ppy_custom_fields_save', $plugin_invoice, 'reg_invoice_fields_save', 11, 3 );
	    $this->loader->add_action( 'ppy_custom_fields_required', $plugin_invoice, 'reg_invoice_required', 11, 2 );

        $this->loader->add_filter( 'ppy_custom_fields', $plugin_public, 'custom_user_fields', 11, 9999 );

        //trick divi into thinking it's a WC Shop page to get the right POST ID for the Divi Builder Edit link in the frontend
        //$this->loader->add_filter( 'et_builder_used_in_wc_shop', $plugin_public, 'fix_divi_edit_link', 11 );

        //$this->loader->add_action( 'admin_bar_menu', $plugin_public, 'fix_divi_edit_link', 1 );


        add_shortcode('lesson-list', array(new Poppyz_Core(), 'lesson_list')); //display the lesson list of a course
        add_shortcode('lesson-modules', array(new Poppyz_Core(), 'lesson_modules')); //display the module/s of a lesson
        add_shortcode('tier-slots', array(new Poppyz_Core(), 'tier_slots_remaining')); //display the module/s of a lesson
        add_shortcode('user-info', array(new Poppyz_Core(), 'user_info')); //display user
        add_shortcode('purchase-button', array(new Poppyz_Core(), 'purchase_button')); //display user

        //remove admin email check
        add_filter( 'admin_email_check_interval', '__return_false' );

        //load our cron job that sends reminders for due payments on payment plans
        $this->loader->add_action( 'ppy_send_payment_reminders', new Poppyz_Core(), 'send_payment_reminders' );


    }


    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    0.5.0
     */
    public function run() {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_poppyz() {
        return $this->poppyz;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Poppyz_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader() {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version() {
        return $this->version;
    }

}
