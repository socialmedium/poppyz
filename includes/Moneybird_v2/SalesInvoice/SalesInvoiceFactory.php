<?php

namespace emilebons\moneybird\SalesInvoice;

use emilebons\moneybird\BaseFactory;

class SalesInvoiceFactory extends BaseFactory
{
    /**
     * @inheritdoc
     */
    public function __construct($administrationId, $client = null, $apiToken = null)
    {
        parent::__construct($administrationId, $client, $apiToken);
        $this->converter = new SalesInvoiceConverter();
        $this->apiSubUrl = 'sales_invoices';
    }

    public function setData($params, $method = 'POST') {
        $this->method = $method;
        $this->params = $params;
        $this->apiSubUrl = 'sales_invoices';
    }

    public function getInvoiceById($invoiceId) {
        $this->apiSubUrl = 'sales_invoices/' . $invoiceId;
    }

    public function registerPayment($invoiceId, $params) {
        $this->converter = null;
        $this->apiSubUrl = 'sales_invoices/' . $invoiceId . '/payments';
        $this->method = 'POST';
        $this->params = $params;
    }

}