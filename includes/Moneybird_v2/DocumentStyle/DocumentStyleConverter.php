<?php

namespace emilebons\moneybird\DocumentStyle;

use emilebons\moneybird\ConverterInterface;

class DocumentStyleConverter implements ConverterInterface
{

    public function convert($object)
    {
        // TODO: Implement convert() method.
    }

    public function parse(array $array)
    {
        $documentStyle = new DocumentStyle();
        $documentStyle->id = $array['id'];
        $documentStyle->name = $array['name'];
        return $documentStyle;
    }
}