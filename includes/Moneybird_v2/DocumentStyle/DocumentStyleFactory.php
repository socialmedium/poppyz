<?php

namespace emilebons\moneybird\DocumentStyle;

use emilebons\moneybird\BaseFactory;

class DocumentStyleFactory extends BaseFactory
{
    /**
     * @inheritdoc
     */
    public function __construct($administrationId, $client = null, $apiToken = null)
    {
        parent::__construct($administrationId, $client, $apiToken);
        $this->converter = new DocumentStyleConverter();
        $this->apiSubUrl = 'document_styles';
    }
}