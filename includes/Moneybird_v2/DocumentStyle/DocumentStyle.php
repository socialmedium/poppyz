<?php

namespace emilebons\moneybird\DocumentStyle;

//use emilebons\moneybird\Contact;

class DocumentStyle
{
    /**
     * @var integer the identifier of the document style
     */
    public $id;

    /**
     * @var string the name of the document style
     */
    public $name;

}