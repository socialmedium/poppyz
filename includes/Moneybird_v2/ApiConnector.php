<?php

/**
 * Communicates with Moneybird through REST API
 * For more information about the MoneyBird API, please visit
 * http://www.moneybird.nl/help/api
 *
 * @author Sjors van der Pluijm <sjors@youngguns.nl>; @sjorsvdp
 */
namespace emilebons\moneybird;

use \DateTime;
use emilebons\moneybird\Contact;
use emilebons\moneybird\SalesInvoice;
use emilebons\moneybird\DocumentStyle;

/**
 * Service for connecting with Moneybird
 */
class ApiConnector
{
    const API_VERSION = '1.0';

    /**
     * First part of Url to connect to
     *
     * @access protected
     * @var string
     */
    protected $baseUri;

    /**
     * HttpRequest object
     *
     * @access protected
     * @var Transport
     */
    protected $transport;

    /**
     * Mapper object
     *
     * @access proteced
     * @var Mapper
     */
    protected $mapper;

    /**
     * @var CurrentSession
     * @access protected
     */
    protected $currentSession;

    /**
     * Last error messages
     * @var Array
     */
    protected $errors = array();

    /**
     * Array of created service objects
     * @var Array
     */
    protected $services = array();

    /**
     * Array of available filters
     * @var Array
     */
    protected $filters = array();

    /**
     * Array for mapping named id's to objects
     * @var Array
     */
    protected $namedId = array();

    /**
     * Indicates if the login credentials where tested
     * @var bool
     */
    protected $loginTested = false;

    /**
     * Show debug information
     * @var bool
     * @static
     */
    public static $debug = false;

    /**
     * Autoloader
     * @static
     * @param string $classname
     */
    static public function autoload($classname)
    {
        if (strpos($classname, __NAMESPACE__) === 0) {
            $classname = substr($classname, strlen(__NAMESPACE__) + 1);
            if (file_exists(__DIR__ . '/' . str_replace('\\', '/', $classname) . '.php')) {
                require_once __DIR__ . '/' . str_replace('\\', '/', $classname) . '.php';
            }
        }
    }
}