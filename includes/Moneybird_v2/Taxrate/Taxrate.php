<?php

namespace emilebons\moneybird\Taxrate;

//use emilebons\moneybird\Contact;

class Taxrate
{
    /**
     * @var integer the identifier of the taxrate
     */
    public $id;

    /**
     * @var string the name of the taxrate
     */
    public $name;


    /**
     * @var string the type of the taxrate
     */
    public $taxRateType;


    /**
     * @var string the state of the taxrate
     */
    public $active;


}