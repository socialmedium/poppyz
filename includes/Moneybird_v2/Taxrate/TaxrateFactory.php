<?php

namespace emilebons\moneybird\Taxrate;

use emilebons\moneybird\BaseFactory;
use emilebons\moneybird\Taxrate\TaxrateConverter;

class TaxrateFactory extends BaseFactory
{
    /**
     * @inheritdoc
     */
    public function __construct($administrationId, $client = null, $apiToken = null)
    {
        parent::__construct($administrationId, $client, $apiToken);
        $this->converter = new TaxrateConverter();
        $this->apiSubUrl = 'tax_rates';
    }
}