<?php

namespace emilebons\moneybird\Taxrate;

use emilebons\moneybird\ConverterInterface;

class TaxrateConverter implements ConverterInterface
{

    public function convert($object)
    {
        // TODO: Implement convert() method.
    }

    public function parse(array $array)
    {
        $Taxrate = new Taxrate();
	    $Taxrate->id = $array['id'];
	    $Taxrate->name = $array['name'];
	    $Taxrate->taxRateType = $array['tax_rate_type'];
	    $Taxrate->active = $array['active'];
        return $Taxrate;
    }
}