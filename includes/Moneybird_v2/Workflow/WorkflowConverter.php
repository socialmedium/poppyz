<?php

namespace emilebons\moneybird\Workflow;

use emilebons\moneybird\ConverterInterface;

class WorkflowConverter implements ConverterInterface
{

    public function convert($object)
    {
        // TODO: Implement convert() method.
    }

    public function parse(array $array)
    {
        $Workflow = new Workflow();
        $Workflow->id = $array['id'];
        $Workflow->name = $array['name'];
        $Workflow->type = $array['type'];
        $Workflow->active = $array['active'];
        return $Workflow;
    }
}