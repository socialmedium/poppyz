<?php

namespace emilebons\moneybird\Workflow;

//use emilebons\moneybird\Contact;

class Workflow
{
    /**
     * @var integer the identifier of the workflow
     */
    public $id;

    /**
     * @var string the name of the workflow
     */
    public $name;


    /**
     * @var string the type of the workflow
     */
    public $type;


    /**
     * @var string the state of the workflow
     */
    public $active;


}