<?php

namespace emilebons\moneybird\Workflow;

use emilebons\moneybird\BaseFactory;

class WorkflowFactory extends BaseFactory
{
    /**
     * @inheritdoc
     */
    public function __construct($administrationId, $client = null, $apiToken = null)
    {
        parent::__construct($administrationId, $client, $apiToken);
        $this->converter = new WorkflowConverter();
        $this->apiSubUrl = 'workflows';
    }
}