<?php

namespace emilebons\moneybird\LedgerAccount;

use emilebons\moneybird\ConverterInterface;

class LedgerAccountConverter implements ConverterInterface
{

    public function convert($object)
    {
        // TODO: Implement convert() method.
    }

    public function parse(array $array)
    {
        $LedgerAccount = new LedgerAccount();
        $LedgerAccount->id = $array['id'];
        $LedgerAccount->name = $array['name'];
        return $LedgerAccount;
    }
}