<?php

namespace emilebons\moneybird\LedgerAccount;

//use emilebons\moneybird\Contact;

class LedgerAccount
{
    /**
     * @var integer the identifier of the ledger account
     */
    public $id;

    /**
     * @var string the name of the ledger account
     */
    public $name;

}