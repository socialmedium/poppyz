<?php

namespace emilebons\moneybird\LedgerAccount;

use emilebons\moneybird\BaseFactory;

class LedgerAccountFactory extends BaseFactory
{
    /**
     * @inheritdoc
     */
    public function __construct($administrationId, $client = null, $apiToken = null)
    {
        parent::__construct($administrationId, $client, $apiToken);
        $this->converter = new LedgerAccountConverter();
        $this->apiSubUrl = 'ledger_accounts';
    }
}