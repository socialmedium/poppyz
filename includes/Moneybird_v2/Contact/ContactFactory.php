<?php

namespace emilebons\moneybird\Contact;

use emilebons\moneybird\BaseFactory;

class ContactFactory extends BaseFactory
{
    public function __construct($administrationId, $client = null, $apiToken = null)
    {
        parent::__construct($administrationId, $client, $apiToken);
        $this->converter = new ContactConverter();
    }


    public function setData($params) {
        $this->params = $params;
        if (isset($this->params['id'])) {
            $this->method = 'PATCH';
            $this->apiSubUrl = 'contacts/' . $this->params['id'];
            unset($this->params['id']);
        } else {
            $this->method = 'POST';
            $this->apiSubUrl = 'contacts';
        }

    }
}