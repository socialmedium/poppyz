<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      0.5.0
 *
 * @package    Poppyz
 * @subpackage Poppyz/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.5.0
 * @package    Poppyz
 * @subpackage Poppyz/includes
 * @author     Socialmedium <info@socialmedium.nl>
 */
class Poppyz_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.5.0
	 */
	public static function deactivate() {
		wp_clear_scheduled_hook( 'ppy_send_payment_reminders' );
	}

}
