<?php
class Poppyz_Lesson_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'ppy_lesson_widget',
            __('Poppyz Lessons Widget', 'poppyz'),
            array( 'description' => __( 'Displays the available lessons of a course. It will only display on course and lesson pages', 'poppyz' ))
        );
        add_shortcode( 'lessons-widget', array( $this, 'shortcode' ) );
    }

    public function widget( $args, $instance ) {
        global $ppy_lang;
        if ( get_post_type() != PPY_COURSE_PT && get_post_type() != PPY_LESSON_PT && ( empty( $instance['course'] ) || $instance['course'] == '-1') ) {
            $is_enhanced_category = is_plugin_active('enhanced-category-pages/enhanced-category-pages.php');
            if (!$is_enhanced_category) return false;
            global $enhanced_category;

            //we have to add a check here since the enhanced category plugin returns an error when category is empty
            global $wpdb;
            $category = $wpdb->get_row($wpdb->prepare(
                "
				SELECT category_id, taxonomy
				FROM {$wpdb->prefix}ecp_x_category
				WHERE post_id = %d
			",
                get_the_ID()
            ));

            if ($category) {
                $term_id = $enhanced_category->get_by_post(get_the_ID());
                if ($term_id) {
                    $instance['course'] = get_term_meta($term_id->category_id, 'ppy_module_course', true);
                }
            }
            if (empty($instance['course']) || $instance['course'] == -1 ) return false;
        }

        if ( empty( $instance['hide_title'] ) ) {
            if ( !isset( $instance['title'] ) )  $instance['title'] = __( 'Course and Lessons','poppyz' );
            $title = apply_filters( 'widget_title', $instance['title'] );
            echo $args['before_widget'];
            if ( ! empty( $title ) )
                echo $args['before_title'] . $title . $args['after_title'];
        }

        $show_image = !empty( $instance['show_images'] ) &&  $instance['show_images'] == 1 ? ' display="image"' : '';
        $show_course_title = !empty( $instance['show_course_title'] ) &&  $instance['show_course_title'] == 1 ? ' show_course_title="true"' : '';
        $collapsible = !empty( $instance['collapsible'] ) &&  $instance['collapsible'] == 1 ? ' collapsible="true"' : '';
        $progress_list = !empty( $instance['progress_list'] ) &&  $instance['progress_list'] == 1 ? ' display="progress"' : '';
        $course = !empty( $instance['course'] ) ? ' course="' . $instance['course'] .'"' : '';
        $show_all = !empty( $instance['show_all'] ) ? ' all="1"' : '';

        $lesson_list = do_shortcode('[lesson-list ' .  $show_image . $show_course_title . $collapsible . $progress_list . $course . $show_all . ']');
        //check if what's return here is a list or a no access message. If it's an no access message then don't display it
        if ( strpos($lesson_list, 'class="lesson-list') == true || strpos($lesson_list, 'class="progress-list') == true) {
            //add lesson-list class if it's a progress-list to inherit its default styles
            $lesson_list = str_replace('class="progress-list', 'class="progress-list lesson-list"', $lesson_list );
            echo $lesson_list;
        }

        echo $args['after_widget'];
    }

    public function form( $instance ) {
        $title =   $instance[ 'title' ] ?? __( 'Course and Lessons', 'poppyz' );
        $show_images =   $instance[ 'show_images' ] ?? '';
        $show_course_title =   $instance[ 'show_course_title' ] ?? '';
        $collapsible =   $instance[ 'collapsible' ] ?? '';
        $progress_list  =  $instance[ 'progress_list' ] ?? '';
        $course  =  $instance[ 'course' ] ?? '';
        $show_all  =  $instance[ 'show_all' ] ?? '';
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'show_images' ); ?>"><?php _e( 'Display featured images?' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'show_images' ); ?>" name="<?php echo $this->get_field_name( 'show_images' ); ?>" type="checkbox" value="1" <?php checked('1', $show_images ) ?> />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'show_course_title' ); ?>"><?php _e( 'Display course title?' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'show_course_title' ); ?>" name="<?php echo $this->get_field_name( 'show_course_title' ); ?>" type="checkbox" value="1" <?php checked('1', $show_course_title ) ?> />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'collapsible' ); ?>"><?php _e( 'Collapsible?' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'collapsible' ); ?>" name="<?php echo $this->get_field_name( 'collapsible' ); ?>" type="checkbox" value="1" <?php checked('1', $collapsible ) ?> />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'progress_list' ); ?>"><?php _e( 'Progress List?' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'progress_list' ); ?>" name="<?php echo $this->get_field_name( 'progress_list' ); ?>" type="checkbox" value="1" <?php checked('1', $progress_list ) ?> />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'show_all' ); ?>"><?php _e( 'Show all lessons?' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'show_all' ); ?>" name="<?php echo $this->get_field_name( 'show_all' ); ?>" type="checkbox" value="1" <?php checked('1', $show_all ) ?> />
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['show_images'] = ( ! empty( $new_instance['show_images'] ) ) ? strip_tags( $new_instance['show_images'] ) : '';
        $instance['show_course_title'] = ( ! empty( $new_instance['show_course_title'] ) ) ? strip_tags( $new_instance['show_course_title'] ) : '';
        $instance['collapsible'] = ( ! empty( $new_instance['collapsible'] ) ) ? strip_tags( $new_instance['collapsible'] ) : '';
        $instance['progress_list'] = ( ! empty( $new_instance['progress_list'] ) ) ? strip_tags( $new_instance['progress_list'] ) : '';
        $instance['course'] = ( ! empty( $new_instance['course'] ) ) ? strip_tags( $new_instance['course'] ) : '';
        $instance['show_all'] = ( ! empty( $new_instance['show_all'] ) ) ? strip_tags( $new_instance['show_all'] ) : '';
        return $instance;
    }

    public function shortcode( $atts ) {
        extract( shortcode_atts(
            array(
                'hide_course' => '0',
                'hide_title' => '0',
            ),
            $atts
        ));
        $instance['hide_course'] = $hide_course;
        $instance['hide_title'] = $hide_title;
        $instance['all'] = $show_all;

        ob_start();
        the_widget( 'Poppyz_Lesson_Widget', $instance );
        $output = ob_get_clean();

        return $output;
    }
}