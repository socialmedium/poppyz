<?php
/**
 * The subscription functionality of the plugin.
 *
 * @since      0.5.0
 *
 * @package    Poppyz
 * @subpackage Poppyz/includes
 */

class Poppyz_Subscription {

	/**
	 * The subscription table
	 *
	 * @since    0.5.0
	 * @access   private
	 * @var      string    $subs_table    The subscription table.
	 */
	private $subs_table;


	/**
	 * Initialize the class properties
	 *
	 * @since    0.5.0
	 */
	public function __construct() {
		global $wpdb;
		$this->subs_table = $wpdb->prefix . 'course_subscriptions';
	}

	/**
	 * Update the user's subscription
	 *
	 * @since     0.6.0
	 * var      bool    $subscribe  True to subscribe the user and false to unsubscribe
	 * @return    bool    Return true on success
	 */
	public static function update_subscription( $action, $id, $data = array() ) {
		global $wpdb;

		global $ppy_lang;
		$ppy_sub = new Poppyz_Subscription();

		$subs_table = $wpdb->prefix . 'course_subscriptions';
		$sub = $ppy_sub->get_subscription_by_id( $id );
		$message = false;

		switch ( $action ) {

			case "update" :
				$course_id = isset( $data['course_id'] ) ? $data['course_id'] : '' ;
				$tier_id = isset( $data['tier_id'] ) ? $data['tier_id'] : '' ;
				$date = isset( $data['date'] ) ? $data['date'] : '' ;
				$price = isset( $data['price'] ) ? $data['price'] : '' ;
				$payments_made = isset( $data['payments_made'] ) ? $data['payments_made'] : '' ;
				$payment_status = isset( $data['payment_status'] ) ? $data['payment_status'] : '' ;
				$payment_type = isset( $data['payment_type'] ) ? $data['payment_type'] : null ;
				$mollie_customer_id = isset( $data['mollie_customer_id'] ) ? $data['mollie_customer_id'] : null ;
				$payment_mode = isset( $data['payment_mode'] ) ? $data['payment_mode'] : null ;


				$update_array = array();
				$update_type_array = array();

				if ( $course_id ) {
					$update_array['course_id'] = $course_id;
					$update_type_array[] = '%d';
				}
				if ( $tier_id ) {
					$update_array['tier_id'] = $tier_id;
					$update_type_array[] = '%d';
				}
				if ( $price ) {
					$update_array['amount'] = $price;
					$update_type_array[] = '%f';
				}

				if ( $mollie_customer_id ) {
					$update_array['mollie_customer_id'] = $mollie_customer_id;
					$update_type_array[] = '%s';
				}

				if ( $date ) {
					$d = DateTime::createFromFormat( 'd-m-Y', $date );
					if ( $d ) {
                        $date = date( 'Y-m-d H:i:s', $d->getTimestamp() ) ;
                        $fields['subscription_date'] = $date;
                        $update_array['subscription_date'] = $date;
                        $update_type_array[] = '%s';
                    }
				}
				if ( $payments_made ) {
					$update_array['payments_made'] = $payments_made;
					$update_type_array[] = '%d';
				}

				if ( $payment_status ) {
					$update_array['payment_status'] = $payment_status;
					$update_type_array[] = '%s';
				}


                if ( $payment_type !== null) {
                    $update_array['payment_type'] = $payment_type;
                    $update_type_array[] = '%f';
                }


                if ( isset($data['initial_amount']) ) {
                    $update_array['initial_amount'] = (float)$data['initial_amount'];
                    $update_type_array[] = '%f';
                }

				if ( $payment_mode && Poppyz_Core::column_exists( 'payment_mode' ) ) {
					$update_array['payment_mode'] = $payment_mode;
					$update_type_array[] = '%s';
				}

				if ( count($update_array) > 0 ) {
					$wpdb->update(
						$subs_table,
						$update_array,
						array('id' => $id),
						$update_type_array,
						array('%d')
					);
				}



				break;

			case "subscribe":
				$tier_id = $sub->tier_id;
				$user_id = $sub->user_id;

				$payments_made = $sub->payments_made;

				$number_of_payments = 1;

				$payment_status = isset( $data['payment_status'] ) ? $data['payment_status'] : 'paid';

				$update_array = array(
					'status' => "on",
					'payment_status' => $payment_status,
				);

				$update_type_array = array(
					'%s',
					'%s',
				);

				$wpdb->update(
					$subs_table,
					$update_array,
					array('id' => $id),
					$update_type_array,
					array('%d')
				);

				Poppyz_Subscription::send_thank_you_message( $id, $user_id );

				$message = __( 'Subscription successful.','poppyz');
				break;

			case "cancel":
				$wpdb->update(
					$subs_table,
					array(
						'status' => "off",
						'payment_status' => 'pending',
						'payment_type' => 'manual',
					),
					array( 'id' => $id ),
					array(
						'%s',
						'%s',
						'%s'
					),
					array( '%s' )
				);
				$message = __( 'The subscription has been cancelled.' ,'poppyz');
				break;

			case "refund":

				$number_of_payments = $sub->payments_made - 1;

				$wpdb->update(
					$subs_table,
					array(
						'payments_made' => $number_of_payments,
					),
					array( 'id' => $id ),
					array(
						'%d'
					),
					array( '%s' )
				);
				$message = __( 'The subscription has been cancelled and the refund was acknowledged.','poppyz');
				break;

			case "remind" :

				$tier_id = $sub->tier_id;
				$payment_method = $sub->payment_method;
				$timeframe = 'months';
				if ( $payment_method == 'membership' ) {
					$interval = get_post_meta( $tier_id,  PPY_PREFIX . 'membership_interval', true );
					$timeframe = get_post_meta( $tier_id,  PPY_PREFIX . 'membership_timeframe', true );
				} elseif ( $payment_method == 'plan' ) {
					$interval = get_post_meta( $tier_id, PPY_PREFIX . 'payment_frequency', true );
					$timeframe = get_post_meta( $tier_id, PPY_PREFIX . 'payment_timeframe', true );
				}

				$due_date =  date( get_option( 'date_format' ), strtotime( $sub->subscription_date . ' + ' . $interval * $sub->payments_made . ' ' . $timeframe ));
				$has_passed = ( time() > strtotime( $due_date ) );
				$data['due_date'] = $has_passed ? __('ASAP','poppyz') : $due_date;
				$type = $has_passed ? 'overdue' : 'reminder';
				$content = Poppyz_Core::send_notification_email( $sub->id, $sub->user_id, $data, $type );
				$user = get_user_by( 'ID', $sub->user_id );
				$message = __( 'A reminder email was successfully sent to ' . $user->user_email ,'poppyz') . ': <br />' . $content;

				break;

			case "delete":
				do_action( 'ppy_payment_cancelled', $id );

                $payment_method = $sub->payment_method;
                $payment_type = $sub->payment_type;

                if ( $payment_method == 'membership' || $payment_method == 'plan' &&  $payment_type != 'manual' ) {
                    $ppy_payment = new Poppyz_Payment();
                    $ppy_payment->cancel_membership( $id );
                }

                $wpdb->delete( $subs_table, array( 'id' => $id ) );
				$message = __( 'The subscription has been deleted.' ,'poppyz');
                do_action( 'ppy_subscription_deleted', $id );
				break;
		}
		return $message;

	}


	/**
	 * Add a user's subscription
	 *
	 * @since     1.0
	 * var      bool    $user_id  True to subscribe the user and false to unsubscribe
	 * var      bool    $tier_id  The user id to subscribe
	 * var      bool    $price  The price of the tier
	 * var      bool    $date  The date of subscription
	 * @return    int    return the newly created subscription's id
	 */
	public static function add_subscription( $user_id, $tier_id, $price = '', $date = false, $send_mail = true, $payment_status = 'paid') {
		global $wpdb;
		$ppy_sub = new Poppyz_Subscription();
		$payment_method = $ppy_sub->get_payment_method( $tier_id );
		if ( $payment_status == 'admin' ){
			$payment_method['type'] = 'manual';
		}

		$fields =  array(
			'course_id' => Poppyz_Core::get_course_by_tier( $tier_id ),
			'user_id' => $user_id,
			'tier_id' => $tier_id,
			'status' => 'on',
			'payment_status' => $payment_status,
			'payment_type' => $payment_method['type'],
			'payment_method' => $payment_method['method'],
			'amount' => $price,
			'payments_made' => 1,
			'version' => 2,
		);

		$values = array(
			'%d',
			'%d',
			'%d',
			'%s',
			'%s',
			'%s',
			'%s',
			'%f',
			'%d',
			'%d',
		);

		if ( $date ) {
			$d = DateTime::createFromFormat( 'd-m-Y', $date );
			if ( $d == false ) {
				$d = DateTime::createFromFormat( 'Y-m-d', $date );
			}
			$date = date( 'Y-m-d H:i:s', $d->getTimestamp() ) ;
			$fields['subscription_date'] = $date;
		} else {
			$fields['subscription_date'] = date('Y-m-d H:i:s');
		}
		$values[] = '%s';

		if ( $payment_method['method'] == 'membership' || $payment_method['method'] == 'plan' ) {
			$fields['payments_made'] = 1;
			$values[] = '%d';
		}

		$wpdb->insert(
			$wpdb->prefix . 'course_subscriptions',
			$fields,
			$values
		);

		$sub_id = $wpdb->insert_id;

		if ( $send_mail ) {
			Poppyz_Subscription::send_thank_you_message( $sub_id, $user_id );
		}

		Poppyz_Core::subscribe_newsletter( $user_id, $tier_id );
		return $sub_id;
	}
    public static function remove_user_subscription_by_tier( $tier_id, $user_id ) {
	    $sub_id = self::is_subscribed_tier( $tier_id, $user_id );
	    $pending = self::has_pending_subscription_for_tier( $tier_id, $user_id );
	    if ( $sub_id ) {
            Poppyz_Subscription::update_subscription( 'cancel', $sub_id );
        } elseif ( $pending ) {
            Poppyz_Subscription::update_subscription( 'delete', $sub_id );
        }
    }

	public static function send_thank_you_message( $id, $user_id ) {

        $ppy_sub = new Poppyz_Subscription();
        $sub = $ppy_sub->get_subscription_by_id( $id );
        if (!$sub) {
            error_log('Thank you email not sent, subscription not found: ' . $id);
            return false;
        }
        $tier_id = $sub->tier_id;

		$message = get_post_meta( $tier_id, PPY_PREFIX . 'tier_email_message', true );
        $subject =  get_post_meta(  $tier_id, PPY_PREFIX . 'tier_email_subject', true );
        $data = [];
		if ($message) $data['email_content'] = $message;
		if ($subject) $data['email_subject'] = $subject;
        Poppyz_Core::send_notification_email( $id, $user_id, $data, 'ty');
	}


	/**
	 * Used to create the initial subscription data, i.e when the user initially clicks on the Buy button.
	 *
	 * @since     1.0
	 * var      bool    $user_id  True to subscribe the user and false to unsubscribe
	 * var      bool    $tier_id  The user id to subscribe
	 * var      bool    $price  The price of the tier
	 * var      bool    $date  The date of subscription
	 * var      int    $initial_price  The date of subscription
	 * @return    int    return the newly created subscription's id
	 */
	public static function add_pending_subscription( $user_id, $tier_id, $price, $date = '', $initial_price = 0 ) {

		//first check if there's an existing pending subscription for this tier to avoid data duplicates
		$id = Poppyz_Subscription::has_pending_subscription_for_tier( $user_id, $tier_id );
		if ( $id ) {
			return $id;
		}

		$payment_method = get_post_meta( $tier_id, PPY_PREFIX . 'payment_method', true );

		if ( empty( $payment_method ) ) $payment_method = 'standard';

		global $wpdb;
		$subs_table = $wpdb->prefix . 'course_subscriptions';

		$fields =  array(
			'course_id' => Poppyz_Core::get_course_by_tier( $tier_id ),
			'user_id' => $user_id,
			'tier_id' => $tier_id,
			'payment_status' => 'pending',
			'payment_method' => $payment_method,
			'amount' => $price,
			'payments_made' => 0,
			'version' => 2 //add version column to determine that this subscription will use the new database structure of payments
		);

		$values = array(
			'%d',
			'%d',
			'%d',
			'%s',
			'%s',
			'%f',
			'%d',
			'%d',
		);

		$tierDonation = get_user_meta( $user_id, PPY_PREFIX . 'donation_amount', true);
		if ( isset($tierDonation) && !empty($tierDonation)) {
			$fields['donation_amount'] = $tierDonation;
			array_push($values, "%f");
		}

		if ( $date ) {
			$d = DateTime::createFromFormat( 'd-m-Y', $date );
			$date = date( 'Y-m-d H:i:s', $d->getTimestamp() ) ;
			$fields['subscription_date'] = $date;
		} else {
			$fields['subscription_date'] = date('Y-m-d H:i:s');
		}

		$values[] = '%s';

        if ( $initial_price > 0 ) {
            $fields['initial_amount'] = $initial_price;
            $values[] = '%f';
        }

		$wpdb->insert(
			$subs_table,
			$fields,
			$values
		);


		$subs_id = $wpdb->insert_id;

		do_action( 'ppy_order_started', $subs_id, $price );

		return $subs_id;
	}

	/**
	 * Checks whether the current user has a pending subscription with the tier to be purchased
	 *
	 * @since 1.0.0
	 * @var int    $user_id    The user who's purchasing
	 * @var int    $tier_id    The tier to check against
	 * @return    int|null    Returns id if has pending
	 */
	public static function has_pending_subscription_for_tier( $user_id, $tier_id ) {
		global $wpdb;
		$subs_table = $wpdb->prefix . 'course_subscriptions';
		$sql = $wpdb->prepare( "SELECT id
                FROM $subs_table
                WHERE user_id = %d
                AND tier_id = %d
                AND payment_status = %s",
			$user_id, $tier_id, 'pending'
		);

		return $wpdb->get_var( $sql );

	}

	public static function get_subscription_by_order( $order_id, $user_id  ) {

		global $wpdb;
		$table = $wpdb->prefix . "course_subscriptions";

		return $wpdb->get_row( $wpdb->prepare(
			"
                    SELECT *
                    FROM $table
                    WHERE order_id = %s
                    AND user_id = %d
                    ",
			$order_id,
			$user_id
		) );
	}

    public static function get_subscription_by_payment_id( $payment_id ) {
        global $wpdb;
        $table = $wpdb->prefix . "course_subscriptions";

        return $wpdb->get_row( $wpdb->prepare(
            "
                    SELECT *
                    FROM $table
                    WHERE payment_id = %s
                    ",
            $payment_id
        ) );
    }

	/**
	 * Add or update a subscription via admin's user page
	 *
	 * @since 0.6.0
	 * @var int    $tier_id    The tier to subscribe to
	 * @var int    $user_id    The user to subscribe
	 * @var int    $course_id    The course which the tier belongs
	 * @var string    $status    The status of the subscription, uses the value 'on' to subscribe a user manually via admin
	 * @var int    $subscription_id   If this has a value other than 0, we are performing an update of the subscription
	 * @return    bool    Returns true if insert/update is successful.
	 */
	public function update_subscription2( $tier_id, $user_id, $course_id, $payment_date, $enable = false, $paid = false, $subscription_id = 0  ) {
		global $wpdb;
		$status = ( $enable ) ? "on" : "off";
		$payment_status = ( $paid ) ? "paid" : "pending";

		if ( $payment_date ) {
			$d = DateTime::createFromFormat( 'd-m-Y', $payment_date );
			$date = date( 'Y-m-d H:i:s', $d->getTimestamp() ) ;
		} else {
			$date = date('Y-m-d H:i:s');
		}

		if ( $subscription_id == 0 ) {
			$wpdb->insert(
				$this->subs_table,
				array(
					'course_id' => $course_id,
					'user_id' => $user_id,
					'tier_id' => $tier_id,
					'status' => $status,
					'payment_method' => 'standard',
					'payment_status' => $payment_status,
					'subscription_date' => $date,
					'version' => 2,
				),
				array(
					'%d',
					'%d',
					'%d',
					'%s',
					'%s',
					'%s',
					'%s',
					'%d',
				)
			);
		} else {
			return $wpdb->update(
				$this->subs_table,
				array(
					'course_id' => $course_id,
					'user_id' => $user_id,
					'tier_id' => $tier_id,
					'status' => $status,
					'payment_status' => $payment_status,
					'subscription_date' => $date,
				),
				array( 'id' => $subscription_id ),
				array(
					'%d',
					'%d',
					'%d',
					'%s',
					'%s',
					'%s',
					'%s',
				),
				array( '%d' )
			);
		}

	}

	/**
	 * Delete a subscription
	 *
	 * @since 0.6.0
	 * @var int    $subscription_id   If this has a value other than 0, we are performing an update of the subscription
	 * @return    bool    Returns true if delete is successful.
	 */
	public function delete_subscription( $subscription_id, $permanent = true ) {
		global $wpdb;


        $return = $this->has_membership_ended($subscription_id, $wpdb);

        do_action( 'ppy_payment_cancelled', $subscription_id );
		if ( $permanent ) {
			$return = $wpdb->delete( $this->subs_table, array( 'id' => $subscription_id ) );
		} else {
            $updateFields = ['payment_status' => 'cancelled'];
            $updateFormat = ['%s'];
            if ($this->has_membership_ended($subscription_id)) {
                $updateFields['status'] = 'off';
                $updateFormat[] = '%s';
            }

			$return = $wpdb->update(
				$this->subs_table,
				$updateFields,
				array( 'id' => $subscription_id ),
                $updateFormat,
				array( '%d' )
			);
		}
		return $return;
	}

    /**
     * @param int $subscription_id
     * @return bool
     */
    private function has_membership_ended(int $subscription_id)
    {
        global $wpdb;
        $sub = $this->get_subscription_by_id($subscription_id);
        $subscription_date = $sub->subscription_date;
        $payments_made = $sub->payments_made;
        $times = $payments_made * $sub->payment_frequency;

        $payment_timeframe = !empty($sub->payment_timeframe) ? $sub->payment_timeframe : 'months';
        $due_date = strtotime($subscription_date . ' + ' . $times . " $payment_timeframe");


        //check if the subscription of the cancelled member has been ended.  Change the status in to off;
        if (time() > ($due_date) && $sub->payment_status == 'cancelled' && $sub->status == 'on') {
            return true;
        }
        return false;
    }

	/**
	 * Subscribe a user after payment
	 *
	 * @since 0.6.0
	 * @var int    $subscription_id    The id to subscribe to
	 * @return    bool    Returns true if the update is successful.
	 */
	public function subscribe_user( $subscription_id ) {
		global $wpdb;
		return $wpdb->update(
			$this->subs_table,
			array(
				'status' => 'on',
				'payment_status' => 'paid',
			),
			array( 'id' => $subscription_id ),
			array(
				'%s',
				'%s',
			),
			array( '%d' )
		);
	}

	/**
	 * Returns the subscriptions of a user
	 *
	 * @since 0.6.0
	 * @var int    $user    The id of the user
	 * @return array|null Database query results
	 */
	public function get_subscriptions_by_user( $user_id, $status = 'all', $method ='all', $tier_id = null ) {
		global $wpdb;
		$user_id = esc_sql( $user_id );
		$sql = "SELECT *
                FROM $this->subs_table
                WHERE user_id = $user_id";

		if ( $status == 'on' ) {
			$sql .= " AND status = 'on' AND payment_status IN ('paid','admin')";
		} elseif ( $status == 'pending' )  {
			$sql .= " AND payment_status = 'invoice_sent'";
		}
		if ( $method != 'all' ) {
			if ( $method == 'standard' ) {
				$sql .= " AND (payment_method = 'standard' OR payment_method = '')";
			} else {
				$sql .= " AND payment_method = '" . esc_sql( $method ) . "'";
			}
		}
		if ( $tier_id != null ) {
			$sql .= " AND tier_id = '" . esc_sql( $tier_id ) . "'";
		}

		return $wpdb->get_results( $sql );
	}

	public function get_subscriptions_by_customer_id( $customer_id ) {
        global $wpdb;

        $sql = $wpdb->prepare(  "SELECT *
                FROM $this->subs_table
                WHERE mollie_customer_id = %s", $customer_id);

        return $wpdb->get_results( $sql );
    }

	public function get_planned_subscriptions_by_user2( $user_id ) {
		global $wpdb;

		$meta_key = PPY_PREFIX . 'number_of_payments';
		$meta_key2 = PPY_PREFIX . 'payment_frequency';
		$sql = "SELECT s.*, pm1.meta_value as tier_number_of_payments, pm2.meta_value as payment_frequency
                FROM $this->subs_table s
                LEFT JOIN $wpdb->postmeta pm1
                ON (
                s.tier_id = pm1.post_id
                AND pm1.meta_key = '$meta_key'
                )
                 LEFT JOIN $wpdb->postmeta pm2
                ON (
                s.tier_id = pm2.post_id
                AND pm2.meta_key = '$meta_key2'
                )
                WHERE s.user_id = $user_id
                AND s.payment_method = 'plan'
                AND s.status ='on'
                AND s.payments_made < pm1.meta_value";

		return $wpdb->get_results( $sql );
	}

	public function get_planned_subscriptions_by_user( $user_id ) {
		global $wpdb;

		$meta_key = PPY_PREFIX . 'number_of_payments';
		$meta_key2 = PPY_PREFIX . 'payment_frequency';
		$meta_key3 = PPY_PREFIX . 'payment_timeframe';

		$sql = "SELECT s.*, pm1.meta_value as tier_number_of_payments, pm2.meta_value as payment_frequency, pm3.meta_value as payment_timeframe
                FROM $this->subs_table s
                LEFT JOIN $wpdb->postmeta pm1
                ON (
                s.tier_id = pm1.post_id
                AND pm1.meta_key = '$meta_key'
                )
                 LEFT JOIN $wpdb->postmeta pm2
                ON (
                s.tier_id = pm2.post_id
                AND pm2.meta_key = '$meta_key2'
                )
                LEFT JOIN $wpdb->postmeta pm3
                ON (
                s.tier_id = pm3.post_id
                AND pm3.meta_key = '$meta_key3'
                )
                WHERE s.user_id = $user_id
                AND s.payment_method = 'plan'
                ";

		return $wpdb->get_results( $sql );
	}

	public function get_membership_subscriptions_by_user( $user_id ) {
		global $wpdb;

		$meta_key = PPY_PREFIX . 'membership_interval';
		$meta_key2 = PPY_PREFIX . 'membership_timeframe';
		$sql = "SELECT s.*, pm1.meta_value as membership_interval, pm2.meta_value as membership_timeframe
                FROM $this->subs_table s
                LEFT JOIN $wpdb->postmeta pm1
                ON (
                s.tier_id = pm1.post_id
                AND pm1.meta_key = '$meta_key'
                )
                LEFT JOIN $wpdb->postmeta pm2
                ON (
                s.tier_id = pm2.post_id
                AND pm2.meta_key = '$meta_key2'
                )
                WHERE s.user_id = $user_id
                AND s.payment_method = 'membership'
				";
		return $wpdb->get_results( $sql );
	}

	/**
	 * Returns the subscription by id
	 *
	 * @since 0.6.0
	 * @var int    $subscription_id    The id of the subscription
	 * @return object|null Database query results
	 */
	public function get_subscription_by_id( $subscription_id ) {
		global $wpdb;

		$sql = $wpdb->prepare(  "SELECT *
                FROM $this->subs_table
                WHERE id = %s", $subscription_id);


		return $wpdb->get_row( $sql );
	}

	/**
	 * Returns the invoice id of the subscription
	 *
	 * @since 0.6.0
	 * @var int    $subscription_id    The id of the subscription
	 * @return object|null Database query results
	 */
	public function get_invoice_id_by_subscription( $subscription_id ) {
		global $wpdb;

		$sql = $wpdb->prepare(  "SELECT invoice_id
                FROM $this->subs_table
                WHERE id = %s", $subscription_id);

		return $wpdb->get_var( $sql );
	}

	/**
	 * Returns the payment method used together with the frequency and number of payments of a tier
	 *
	 * @since 0.6.0
	 * @var int    $tier_id    The tier id of the subscription
	 * @return array The payment method, frequency, and number of payments
	 */
	public function get_payment_method( $tier_id ) {
		$payment = array();
		$payment['method'] =  get_post_meta( $tier_id, PPY_PREFIX . 'payment_method', true );
		if ( $payment['method']  == 'plan' ) {
			$payment['frequency'] = get_post_meta( $tier_id, PPY_PREFIX . 'payment_frequency', true );
			$payment['number'] = get_post_meta( $tier_id, PPY_PREFIX . 'payment_number', true);
		} elseif ( $payment['method'] == 'membership' ) {
			$payment['membership_interval'] = get_post_meta( $tier_id, PPY_PREFIX . 'membership_interval', true );
		}
		if ( empty ( $payment['method'] ) ) {
			$payment['method'] = 'standard';
		}
		$is_automatic = get_post_meta( $tier_id, PPY_PREFIX . 'payment_automatic', true );
		$payment['type'] = $is_automatic ? 'automatic' : 'manual';
		return $payment;
	}

	public function get_number_of_payments( $subscription_id ) {
		global $wpdb;

		$sql = $wpdb->prepare(  "SELECT payments_made
                FROM $this->subs_table
                WHERE id = %s", $subscription_id);

		return $wpdb->get_var( $sql );
	}

	/**
	 * Check if the user has access to the given course or lesson
	 *
	 * @since 0.6.0
	 * @var int    $post_id    Can either be a course id or a lesson id
	 * @var int    $user_id    The id of the current user
	 * @return    bool|WP_Error    Returns true the user has access or WP_Error otherwise
	 */
	public function is_subscribed ( $post_id, $user_id = null ) {
		global $wpdb;
		if ( !$user_id ) {
			$current_user = wp_get_current_user();
			$user_id = $current_user->ID;
		}

		//if admin, show course regardless
		if (  current_user_can( 'manage_options' ) ) return true;

		global $ppy_lang;
		$post_type = get_post_type( $post_id );

		if ( $post_type == PPY_COURSE_PT ) {

			$subscription = $wpdb->get_row( $wpdb->prepare(
				"
                    SELECT tier_id, subscription_date
                    FROM $this->subs_table
                    WHERE user_id = %d
                    AND course_id = %d
                    AND status = %s
                    ORDER BY subscription_date DESC
                    ",
				$user_id,
				$post_id,
				'on'
			) );
            $tier_id = (isset($subscription->tier_id)) ? $subscription->tier_id : "";
			$tier_status = get_post_status($tier_id);
            $subscription_date = (isset($subscription->subscription_date)) ? $subscription->subscription_date : "";

			if ( empty( $tier_id ) ) {
                $course_not_accessible_text = __( 'Sorry, you do not have access to this course.' ,'poppyz');
                $course_not_accessible_text2 = Poppyz_Core::get_option( 'course_not_accessible_text' );
                if ( !empty( $course_not_accessible_text2 ) ) {
                    $course_not_accessible_text = $course_not_accessible_text2;
                }
                return new WP_Error( 'no_access',  $course_not_accessible_text );
			} else {
				if (  $this->tier_expired( $tier_id, $user_id ) ) {
                    $tier_expired_text = !empty(Poppyz_Core::get_option( 'tier_expired_text' )) ? Poppyz_Core::get_option( 'tier_expired_text' ) : __( 'Sorry, this tier already expired.' ,'poppyz');
					return new WP_Error( 'tier_expired',  $tier_expired_text);
				}
				if (  empty($tier_status) ) {
					return new WP_Error( 'tier_removed',  __( 'Sorry, this tier has already been deleted.' ,'poppyz'));
				}
                if ( strtotime($subscription_date) > time() ) {
                    return new WP_Error( 'user_registered_future',  sprintf( __( 'Sorry, you do not have access to this course yet. You will gain access on %s.' ,'poppyz'), date_i18n(get_option('date_format'), $subscription_date ) )  );
                }
			}
			return true;
		} elseif ( $post_type == PPY_LESSON_PT ) {
			// get the lesson's course
			$course_id = get_post_meta( $post_id, PPY_PREFIX . "lessons_course", true );
			// use the course to find out which tier that user is subscribed too
			$result = $wpdb->get_row( $wpdb->prepare(
				"
                    SELECT tier_id, subscription_date
                    FROM $this->subs_table
                    WHERE user_id = %d
                    AND course_id = %d
                    AND status = %s
                    ORDER BY subscription_date DESC
                    ",
				$user_id,
				$course_id,
				'on'
			) );
			// if the user is subscribed to a tier check if the given tier has access to lessons
			if ( $result ) {

				if (  $this->tier_expired( $result->tier_id, $user_id ) ) {
					$tier_expired_text = !empty(Poppyz_Core::get_option( 'tier_expired_text' )) ? Poppyz_Core::get_option( 'tier_expired_text' ) : __( 'Sorry, this tier already expired.' ,'poppyz');
					return new WP_Error( 'tier_expired',  $tier_expired_text);
				}
				$tier_status = get_post_status($result->tier_id);
				if (  empty($tier_status) ) {
					return new WP_Error( 'tier_removed',  __( 'Sorry, this tier has already been deleted.' ,'poppyz'));
				}
				if ( strtotime($result->subscription_date) > time() ) {
					return new WP_Error( 'user_registered_future',  sprintf( __( 'Sorry, you do not have access to this course yet. You will gain access on %s.' ), date_i18n(get_option('date_format'), $result->subscription_date ) )  );
				}
				$ppy_core = new Poppyz_Core();
				$lessons = $ppy_core->get_lessons_by_course( $course_id );

				$day_name = get_post_meta( $result->tier_id, PPY_PREFIX . 'tier_subs_day_available', true );

				if ( $lessons->have_posts() ) {
					while ( $lessons->have_posts() ) {
						$lessons->the_post();
						if ( get_the_ID() == $post_id) {
							$lesson = get_post_meta( $result->tier_id,  PPY_PREFIX . 'tier_lessons_' . get_the_ID(), true );

							//if there is no written rule for the lesson
							if (empty($lesson)) return true;

							//if the exclude checkbox is not checked on the tier's lessons meta box
							if ( !isset( $lesson['exclude'] ) ) {
								if ( isset( $lesson['type'] ) && $lesson['type'] == "day" ) {
									$start_date = strtotime( '+ ' . $lesson['day'] . " days", strtotime( $result->subscription_date ) );
									//reset to start of day
									$start_date = strtotime( date('Y-m-d 00:00:00', $start_date ) );
									if ($start_date <= time())
										return true;
								}
								elseif ( isset( $lesson['type'] ) && $lesson['type'] == 'date' ) {
									$start_date = DateTime::createFromFormat('d-m-Y', $lesson['date']);
									$start_date = $start_date->getTimestamp();
									if ($start_date <= time())
										return true;
								} else {
									$start_date = strtotime( $result->subscription_date );
									if ( !empty( $lesson['week'] ) && $lesson['week'] != 0  ) {
										$start_date = strtotime( '+ ' . $lesson['week'] . " weeks", strtotime( $result->subscription_date ) );
									}

									if ( !empty( $day_name ) ) {
										$start_date = strtotime( '-4 days', $start_date );
										$start_date = strtotime( 'next ' . $day_name, $start_date);
									}

									$start_date = strtotime( date('Y-m-d 00:00:00', $start_date ) );
									if ($start_date <= time())
										return true;
								}
								$not_yet_available = __( 'Sorry, this lesson isn\'t available yet. It will be available on: ','poppyz') . date_i18n(  get_option( 'date_format'  ), $start_date );
								$not_yet_available_text = Poppyz_Core::get_option( 'lesson_not_yet_available_text' );
								if ( !empty( $not_yet_available_text ) ) {
									$not_yet_available = str_replace( '[start-date]', date_i18n(  get_option( 'date_format'  ), $start_date ), $not_yet_available_text );
								}
								return new WP_Error( 'no_access',  $not_yet_available );

							} else {
								$lesson_excluded_text = __( 'Sorry, this lesson is excluded from the course that you\'re subscribed to.' ,'poppyz');
								$lesson_excluded_text2 = Poppyz_Core::get_option( 'lesson_excluded_text' ) ;
								if ( !empty( $lesson_excluded_text2  )  ) {
									$lesson_excluded_text =  $lesson_excluded_text2;
								}
								return new WP_Error( 'no_access',  $lesson_excluded_text );
							}
						}

					}
				}

			}
			$lesson_not_accessible_text = __( 'Sorry, you do not have access to this lesson.','poppyz');
			$lesson_not_accessible_text2 = Poppyz_Core::get_option( 'lesson_not_accessible_text' );
			if ( !empty( $lesson_not_accessible_text2 ) ) {
				$lesson_not_accessible_text = $lesson_not_accessible_text2;
			}
			return new WP_Error( 'no_access',  $lesson_not_accessible_text );
		}

	}

	public function tier_expired( $tier_id, $user_id ) {
		$tier_expiry = get_post_meta( $tier_id, PPY_PREFIX . 'tier_expire', true );
		$tier_subs_expiry = get_post_meta( $tier_id, PPY_PREFIX . 'tier_subs_expire', true );
		$return = false;
		if ( $tier_expiry ) {
			$tier_expiry = DateTime::createFromFormat('d-m-Y', $tier_expiry);
			if ( $tier_expiry ) {
                $tier_expiry = $tier_expiry->getTimestamp();

                if ( $tier_expiry < time() ) {
                    $return = true;
                }
            }
		}
		if ( $tier_subs_expiry && $tier_subs_expiry > 0 ) {

			$subs = $this->get_subscriptions_by_user( $user_id, 'on', 'all', $tier_id );

			if  ( $subs ) {
				$subs = end($subs); //if multiple subscriptions, get the last one
				$subs_date = strtotime( '+ ' . $tier_subs_expiry . " days", strtotime( $subs->subscription_date ) );

				if ( $subs_date < time() ) {
					$return = true;
				}
			}
		}

		$return = apply_filters( 'ppy_tier_expired', $return, $tier_id );

		return $return;
	}


	public function tier_purchase_expired( $tier_id ) {
		$tier_purchase_expiry = get_post_meta( $tier_id, PPY_PREFIX . 'tier_purchase_expiration', true );
		$return = false;
		if ( $tier_purchase_expiry ) {
			$tier_purchase_expiry = DateTime::createFromFormat('d-m-Y', $tier_purchase_expiry);
			if ( $tier_purchase_expiry ) {
				$tier_purchase_expiry = $tier_purchase_expiry->getTimestamp();

				if ( $tier_purchase_expiry < time() ) {
					$return = true;
				}
			}
		}
		$return = apply_filters( 'ppy_tier_purchase_expired', $return, $tier_id );

		return $return;
	}

    public function member_has_future_register_date( $user ) {
        return new DateTime("01/02/2016");
    }

	public function membership_expired( $user_id, $course_id, $tier_id = null ) {
		global $wpdb;
		$tier_id = $wpdb->get_var( $wpdb->prepare(
			"
                    SELECT tier_id
                    FROM $this->subs_table
                    WHERE user_id = %d
                    AND course_id = %d
                    AND status = %s
                    AND payment_status = %s
                    ",
			$user_id,
			$course_id,
			'expired',
			'paid'
		) );
	}

	public static function check_license( $license_key ) {
		$api_params = array(
			'edd_action'=>  'check_license',
			'license' 	=> $license_key,
			'item_id' => PPY_ITEM_ID, // the name of our product in EDD
			'url'       => home_url()
		);

		return wp_remote_post( PPY_WEBSITE, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
	}

	public function update_license() {
		$options = get_option( 'ppy_settings');
		$license_key = $options['license_key'];
		$response = self::check_license( $license_key );

		// make sure the response came back okay
        if ( !is_wp_error( $response ) ){
            $license_data = json_decode( wp_remote_retrieve_body( $response ) );
            $status = ( $license_data->license == 'valid' ||  $license_data->license == 'site_inactive') ? 'valid' : 'invalid';
            $options['license_status'] =  $status;
            update_option( 'ppy_settings' , $options );
        }
	}

	public function notify_overdue_subscriptions() {
		global $wpdb;

		self::remind_pending_subscriptions();

		$meta_key = PPY_PREFIX . 'number_of_payments';
		$meta_key2 = PPY_PREFIX . 'payment_frequency';
		$meta_key3 = PPY_PREFIX . 'payment_timeframe';

		$sql = "SELECT s.*, pm1.meta_value as tier_number_of_payments, pm2.meta_value as payment_frequency, pm3.meta_value as payment_timeframe
                FROM $this->subs_table s
                LEFT JOIN $wpdb->postmeta pm1
                ON (
                s.tier_id = pm1.post_id
                AND pm1.meta_key = '$meta_key'
                )
                 LEFT JOIN $wpdb->postmeta pm2
                ON (
                s.tier_id = pm2.post_id
                AND pm2.meta_key = '$meta_key2'
                )
                LEFT JOIN $wpdb->postmeta pm3
                ON (
                s.tier_id = pm3.post_id
                AND pm3.meta_key = '$meta_key3'
                )
                WHERE s.payment_method = 'plan'
                AND s.status ='on' 
                AND s.payment_status <> 'admin' 
                AND s.amount > 0
                AND s.payments_made < pm1.meta_value";

		$subs = $wpdb->get_results( $sql );

		foreach ( $subs as $sub ) {
			$subscription_date = $sub->subscription_date;
			$payment_timeframe = !empty( $sub->payment_timeframe ) ?  $sub->payment_timeframe : 'months';

			$payments_made = $sub->payments_made;
			$user_id = $sub->user_id;
			$times = $payments_made * $sub->payment_frequency;

            //skip subscriptions that are already marked as completed - right now this is only the case for the API's payment plans
            if ( get_user_meta($user_id, 'poppyz_installments_done_' . $sub->id) == 1 ) continue;

			$due_date = strtotime( $subscription_date . ' + ' . $times . " $payment_timeframe" );
			$due_date_string =  date( get_option( 'date_format' ), $due_date );
			$data['due_date'] = $due_date_string;

			//add a 7 day grace period for processing before cancelling the subscription, unless the membership was cancelled in which case delete it promptly
			$delay = 604800;
			//if it's a bank transfer extend it to 14 days since it make take awhile to clear transfers
			if (  $sub->payment_id ) {
				try {
					$ppy_payment = new Poppyz_Payment();

					//check if it is Test or live mode.
					if(!Poppyz_Core::column_exists( 'payment_mode' )) {
						$ppy_payment->_initialize_mollie();
					} else {
						$payment_mode = (!empty($sub->payment_mode)) ? $sub->payment_mode : "";
						$ppy_payment->_initialize_mollie( $payment_mode );
					}

					$payment = $ppy_payment->mollie->payments->get( $sub->payment_id );
					if ( $payment && $payment->method == 'banktransfer') {
						$delay = 1036800;
					}
				} catch ( Exception $ex ) {
					error_log( 'get payment method error: ' . $ex->getMessage());
				}
			}
		
			if ( $sub->payment_status == 'cancelled' ) $delay = 0;
			if ( time() > (  $due_date  + $delay ) && get_user_meta( $user_id, PPY_PREFIX . $sub->id . '_overdue_notice', true ) == null && $sub->payment_status != 'cancelled') {
				Poppyz_Core::send_notification_email( $sub->id, $user_id, $data, 'overdue' );
				$this::update_subscription( 'cancel', $sub->id );
				$ppy_payment = new Poppyz_Payment();
				$ppy_payment->cancel_membership( $sub->id );
				
				//flag this user so the notice won't send again
				update_user_meta( $user_id, PPY_PREFIX . $sub->id . '_overdue_notice', 1 );
			} else if (time() > ( $due_date + $delay ) && $sub->payment_status == 'cancelled') {
				$this->delete_subscription( $sub->id, false );
			} else {
				//check if payment is 6 days away from being overdue
				$difference = $due_date - time();
				if ( $difference > 0 && $difference < 518400 && get_user_meta( $user_id, PPY_PREFIX . $sub->id . '_reminder_notice_' . $sub->payments_made, true ) == null && $sub->amount > 0 && $sub->payments_made > 0 && $sub->payment_status != 'cancelled') {
					if ( $sub->payment_type == 'manual' ) {
						$prefix = 'reminder_manual';
					} else {
						$prefix = 'reminder';
					}
					Poppyz_Core::send_notification_email(  $sub->id, $user_id, $data, $prefix );
					update_user_meta( $user_id, PPY_PREFIX . $sub->id . '_reminder_notice_' . $sub->payments_made, 1 );
				}
			}
		}

		// check memberships

		$meta_key = PPY_PREFIX . 'membership_interval';
		$meta_key2 = PPY_PREFIX . 'membership_timeframe';

		$sql = "SELECT s.*, pm1.meta_value as membership_interval, pm2.meta_value as membership_timeframe
                FROM $this->subs_table s
                LEFT JOIN $wpdb->postmeta pm1
                ON (
                s.tier_id = pm1.post_id
                AND pm1.meta_key = '$meta_key'
                )
                 LEFT JOIN $wpdb->postmeta pm2
                ON (
                s.tier_id = pm2.post_id
                AND pm2.meta_key = '$meta_key2'
                )
                WHERE s.payment_method = 'membership'
                AND s.amount > 0
                AND s.payment_status <> 'admin'
                AND s.status ='on'";

		$subs = $wpdb->get_results( $sql );


		/*ob_start();
		echo "get all membership payments:";
		var_dump($subs);
		$contents = ob_get_contents();
		ob_end_clean();
		error_log($contents);*/


		foreach ( $subs as $sub ) {
			$subscription_date = $sub->subscription_date;

			$membership_interval = $sub->membership_interval;
			$membership_timeframe = $sub->membership_timeframe;
			$payments_made = $sub->payments_made;

			$user_id = $sub->user_id;

			$times = $payments_made * $membership_interval;

			$due_date =  strtotime( $subscription_date . ' + ' . $times . ' ' . $membership_timeframe );
			$due_date_string = date( get_option( 'date_format' ), $due_date);

			$data['due_date'] = $due_date_string;
			$data['subs_id'] = $sub->id;
			$data['tier_link'] = Poppyz_Core::get_tier_purchase_url( $sub->tier_id );

			//if no payment has been made after 7 days of expiration, cancel the subscription

			/*ob_start();
			echo "due_date:";
			echo $due_date_string;
			echo 'current_time:';
			echo(date( get_option( 'date_format' ), time()));
			echo 'is_expired:';
			var_dump(time() > ( $due_date  + 604800 ));
			$contents = ob_get_contents();
			ob_end_clean();
			error_log($contents);*/


			//add a 7 day grace period for processing before cancelling the subscription, unless the membership was cancelled in which case delete it promptly
			$delay = 604800;
			//if it's a bank transfer extend it to 14 days since it make take awhile to clear transfers
			if ( $sub->payment_id ){
				try {
					$ppy_payment = new Poppyz_Payment();

					//check if it is Test or live mode.
					if(!Poppyz_Core::column_exists( 'payment_mode' )) {
						$ppy_payment->_initialize_mollie();
					} else {
						$payment_mode = (!empty($sub->payment_mode)) ? $sub->payment_mode : "";
						$ppy_payment->_initialize_mollie( $payment_mode );
					}

					$payment = $ppy_payment->mollie->payments->get( $sub->payment_id );
					if ( $payment && $payment->method == 'banktransfer') {
						$delay = 1036800;
					}
				} catch ( Exception $ex ) {
					error_log( 'get payment method error: ' . $ex->getMessage());
				}
			}

			if ( $sub->payment_status == 'cancelled' ) $delay = 0;
			if ( time() > ( $due_date + $delay ) && $sub->payment_status != 'cancelled') {
				$this::update_subscription( 'cancel', $sub->id );
				$ppy_payment = new Poppyz_Payment();
				$ppy_payment->cancel_membership( $sub->id );
				if ( get_user_meta( $user_id, PPY_PREFIX . $sub->id. '_overdue_notice', true ) == null ) {
					Poppyz_Core::send_notification_email( $sub->id, $user_id, $data, 'overdue' );
					update_user_meta( $user_id, PPY_PREFIX . $sub->id . '_overdue_notice', 1 );
				}
			} else if (time() > ( $due_date + $delay ) && $sub->payment_status == 'cancelled' && $sub->status == 'on') {
				
				$this->delete_subscription( $sub->id, false );
			} else {
				//check if membership is 2 days away from being expired
				$difference =  $due_date - time();

				if ( $difference > 0 && $difference < 172800 && get_user_meta( $user_id, PPY_PREFIX . $sub->id. '_reminder_notice', true ) == null && $sub->amount > 0 && $sub->payments_made > 0 ) {
					$data['due_date'] = $due_date_string;
					if ( $sub->payment_type == 'manual' ) {
						$prefix = 'reminder_manual';
					} else {
						$prefix = 'reminder';
					}
					Poppyz_Core::send_notification_email(  $sub->id, $user_id, $data, $prefix );
					update_user_meta( $user_id, PPY_PREFIX . $sub->id . '_reminder_notice', 1 );
				}
			}
		}
	}


	public static function remind_pending_subscriptions() {
        global $wpdb;

        $subs_table = $wpdb->prefix . 'course_subscriptions';


        if ( Poppyz_Core::get_option('pending_email_disabled') == 'on' ) {
            return false;
        }

        $interval = Poppyz_Core::get_option('pending_mail_waiting_hours') ? Poppyz_Core::get_option('pending_mail_waiting_hours') : 48;

        $sql = $wpdb->prepare(
            "
                    SELECT s.id, s.user_id
                    FROM $subs_table s
                    WHERE NOT EXISTS (
                        SELECT * FROM {$wpdb->prefix}usermeta um 
                        WHERE s.user_id = um.user_id
                        AND ( um.meta_key = CONCAT('ppy_', s.id, '_reminder_pending_notice') AND um.meta_value = 1)
                    )
                    AND s.payment_status = %s
                    AND s.payments_made = %d
                    AND s.amount > %d
                    AND NOW() > DATE_ADD(s.subscription_date, INTERVAL %d HOUR) GROUP BY s.id 
                    ",
            'pending',
            0,
            0,
            $interval
        ) ;

        $subs = $wpdb->get_results( $sql );

        if ( $subs ) {
            foreach ( $subs as $sub ) {
                if ( get_user_meta( $sub->user_id, PPY_PREFIX . $sub->id. '_reminder_pending_notice', true ) != null ) continue;

                Poppyz_Core::send_notification_email(  $sub->id, $sub->user_id, array(), 'pending' );
                update_user_meta( $sub->user_id, PPY_PREFIX . $sub->id . '_reminder_pending_notice', 1 );
            }
        }

        return $subs;
    }

	public function get_lessons_by_subscription( $course_id, $user_id, $all = false ) {
		global $wpdb;
		$ppy_core = new Poppyz_Core();

		//if admin, just return all the lessons id's belonging to the course
		if ( current_user_can( 'manage_options' ) || $all ) {
			$lessons =  $ppy_core->get_lesson_ids_by_course( $course_id );
			if ( count ( $lessons ) > 0 ) {
				return array( $lessons );
			} else {
				return false;
			}
		}

		//get the user's subscription/s
		$subs = $wpdb->get_results( $wpdb->prepare(
			"
                SELECT tier_id, subscription_date
                FROM $this->subs_table
                WHERE user_id = %d
                AND course_id = %d
                AND status = %s          
                ORDER BY id DESC LIMIT 1
            ",
			$user_id,
			$course_id,
			'on'
		) );


		//get the lessons that are accessible
		//we only need the id's here because we still have to sort them later on via WP_Query
		if ( $subs ) {
			//store available lessons per tier/subscription
			$a_lessons = array();
			//there can be more than one subscriptions per course, so loop through them
			foreach ($subs as $s) {
				$ppy_core = new Poppyz_Core();
				$lessons = $ppy_core->get_lessons_by_course( $course_id );
				$day_name = get_post_meta( $s->tier_id, PPY_PREFIX . 'tier_subs_day_available', true );
				if ( $lessons->have_posts() ) {
					while ( $lessons->have_posts() ) {
						$lessons->the_post();
						$lesson_id = get_the_ID();
						$lesson = get_post_meta( $s->tier_id,  PPY_PREFIX . 'tier_lessons_' . $lesson_id, true );

						//if there is no written rule for the lesson, it's accessible
						if (empty($lesson)){
							$a_lessons[$s->tier_id][] =  $lesson_id; //separate the lessons by tier
							continue;
						}

						//if the exclude checkbox is not checked on the tier's lessons meta box
						if ( !isset( $lesson['exclude'] ) ) {
							if ( isset( $lesson['type'] ) && $lesson['type'] == "day" ) {
								//reset the start date to start at the beginning of the day
								$start_date = strtotime( date( 'm/d/Y', strtotime( $s->subscription_date ) ) . ' 00:00:00' );
								$start_date = strtotime( '+ ' . $lesson['day'] . " days", $start_date );
								if ($start_date <= time()){
									$a_lessons[$s->tier_id][] =  $lesson_id;
									continue;
								}
							} elseif ( isset( $lesson['type'] ) && $lesson['type'] == 'date') {
								$start_date = DateTime::createFromFormat('d-m-Y', $lesson['date']);
								$start_date = $start_date->getTimestamp();
								if ($start_date <= time()) {
									$a_lessons[$s->tier_id][] =  $lesson_id;
									continue;
								}
							}
							else {
								$start_date = strtotime( $s->subscription_date );
								if ( !empty( $lesson['week'] ) && $lesson['week'] != 0 ) {
									$start_date = strtotime( '+ ' . $lesson['week'] . " weeks", strtotime( $s->subscription_date ) );
								}
								if ( !empty( $day_name ) ) {
									$start_date = strtotime( '-4 days', $start_date );
									$start_date = strtotime( 'next ' . $day_name, $start_date);
								}

								$start_date = strtotime( date('Y-m-d 00:00:00', $start_date ) );
								if ($start_date <= time()) {
									$a_lessons[$s->tier_id][] =  $lesson_id;
									continue;
								}

							}
						}
					}
				}

			}
			return $a_lessons;
		}
		return false;
	}

	public function compute_discounted_price( $subs_id, $price = 0 ) {
		$sub = $this->get_subscription_by_id( $subs_id );


        //price is sometimes coming from somewhere else, like initial price
        if ($price > 0) {
            $amount = $price;
        } else {
            $amount = $sub->amount;
        }

//		$tier_discount = get_post_meta( $sub->tier_id, PPY_PREFIX .  'tier_discount', true );
//		if ( !empty($tier_discount) ) {
//			return $amount - $tier_discount;
//		}
		if ( $sub->discount_amount > 0 ) {
			return $amount - $sub->discount_amount;
		} else {
			return $amount;
		}

	}

	public static function get_subscription_total_by_tier( $tier_id ) {
		global $wpdb;
		$subs_table = $wpdb->prefix . 'course_subscriptions';

		$subs = $wpdb->get_var( $wpdb->prepare(
			"
                SELECT count(*)
                FROM $subs_table
                WHERE tier_id = %d
                AND status = %s
            ",
			$tier_id,
			'on'
		) );

		if ( $subs ) return $subs;
		return 0;
	}

	public static function get_subscriptions_by_tier( $tier_id ) {
		global $wpdb;

		$subs_table = $wpdb->prefix . 'course_subscriptions';
		$tier_id = esc_sql( $tier_id );
		$sql = "SELECT *
                FROM $subs_table
                WHERE tier_id = $tier_id
		 		AND status = 'on'";


		return $wpdb->get_results( $sql );
	}

	public static function is_subscribed_tier( $user_id, $tier_id ) {
		global $wpdb;
		$subs_table = $wpdb->prefix . 'course_subscriptions';

		$subs = $wpdb->get_var( $wpdb->prepare(
			"
                SELECT id
                FROM $subs_table
                WHERE tier_id = %d
                AND user_id = %d
                AND status = %s                    
            ",
			$tier_id,
			$user_id,
			'on'
		) );


		if ( $subs ) return $subs;
		return 0;
	}

	public static function get_users_with_subscription(){
        global $wpdb;
        $subs_table = $wpdb->prefix . 'course_subscriptions';

        $subs = $wpdb->get_col(
            "
                SELECT DISTINCT user_id
                FROM $subs_table
                GROUP BY user_id
            "
         );


        if ( $subs ) return $subs;
        return 0;
    }

    public static function has_subscription( $user_id ) {
        global $wpdb;
        $subs_table = $wpdb->prefix . 'course_subscriptions';

        $subs = $wpdb->get_var( $wpdb->prepare(
            "
                SELECT id
                FROM $subs_table
                WHERE user_id = %d
            ",
            $user_id
        ) );


        if ( $subs ) return $subs;
        return 0;
    }

	public static function is_pending_plan( $user_id, $tier_id ) {
		global $wpdb;
		$subs_table = $wpdb->prefix . 'course_subscriptions';
        $payments_table = $wpdb->prefix . "course_subscriptions_payments";

		$subs = $wpdb->get_var( $wpdb->prepare(
			"
                    SELECT s.id
                    FROM $subs_table s
                    WHERE s.tier_id = %d
                    AND s.user_id = %d
                    AND (s.payment_method = 'plan' OR s.payment_method = 'membership')
                    AND s.payment_status = 'pending'
                    AND EXISTS (SELECT 1 FROM $payments_table p WHERE p.subscription_id = s.id)
                    ",
			$tier_id,
			$user_id
		) );  //if a payment record exists it means it has already been paid before but a chargeback happend. When this is the case the payment should happen on the account page.

		if ( $subs ) return $subs;
		return 0;
	}

	public static function is_pending_tier( $user_id, $tier_id ) {
		global $wpdb;
		$subs_table = $wpdb->prefix . 'course_subscriptions';

		$subs = $wpdb->get_var( $wpdb->prepare(
			"
                    SELECT id
                    FROM $subs_table
                    WHERE tier_id = %d
                    AND user_id = %d
                    AND ( payment_status = %s OR payment_status = %s )
                    ",
			$tier_id,
			$user_id,
			'pending',
			'open'
		) );

		if ( $subs ) return $subs;
		return 0;
	}

	public static function tier_has_manual_subscriptions( $tier_id ) {
		global $wpdb;
		$subs_table = $wpdb->prefix . 'course_subscriptions';

		$subs = $wpdb->get_var( $wpdb->prepare(
			"
                    SELECT count(*)
                    FROM $subs_table
                    WHERE tier_id = %d
                    AND payment_type <> %s
                    ",
			$tier_id,
			'automatic'
		) );

		if ( $subs ) return $subs;
		return 0;
	}

	public static function show_upsell_form( $content, $subscription ) {
        $upsells = Poppyz_Core::get_tier_upsells( $subscription->tier_id );
        global $ppy_lang;
        if ($upsells) {
            $content = '';
            $can_buy = false; //check if user has not bought at least one of the upsell yet
            foreach ( $upsells as $upsell_id ) { //check if the client already has subscribed, if he's subscribed to all the tiers already dont show the form
                if (Poppyz_Subscription::is_subscribed_tier( $subscription->user_id, $upsell_id ) ) continue;
                $can_buy = true;
            }

//            if ( !$can_buy ) {
//                return false;
//            }

			$content .= '<div class="upsell-wrapper upsells">';
            $content .= '<div class="upsells-steps">
                            <div class="step step1">
                                <div class="icon">
                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M13.331 0.401478C13.0763 0.146773 12.6634 0.146773 12.4087 0.401478L4.59522 8.21501L1.59186 5.21164C1.33718 4.95694 0.924274 4.95696 0.669544 5.21164C0.414839 5.46632 0.414839 5.87923 0.669544 6.13393L4.13407 9.5984C4.38867 9.85308 4.80188 9.85291 5.05638 9.5984L13.331 1.32379C13.5857 1.06912 13.5857 0.656182 13.331 0.401478Z" fill="white"/>
                                    </svg>
                                </div>
                                
                                '. __('Checkout' ,'poppyz').'
                            </div>
                            <div class="line"></div>
                            <div class="step step2">
                                <div class="icon">
                                    <svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9.86427 9.64888C10.1562 9.3424 10.6157 9.3424 10.9076 9.64888C11.1896 9.95536 11.1896 10.4379 10.9076 10.7443L6.53584 15.3354C6.40042 15.4776 6.2124 15.5652 6.004 15.5652C5.7956 15.5652 5.60759 15.4776 5.47151 15.3354L1.09977 10.7443C0.807888 10.4379 0.807888 9.95536 1.09977 9.64888C1.39166 9.3424 1.85119 9.3424 2.14307 9.64888L5.26311 12.936V1.26689C5.26245 0.839614 5.59641 0.5 6.00334 0.5C6.41028 0.5 6.73372 0.839614 6.73372 1.26689V12.9353L9.86427 9.64888Z" fill="white"/>
                                    </svg>
                                </div>
                                '. __('Great chance','poppyz') .'
                            </div>
                            <div class="line"></div>
                            <div class="step step3">
                                <div class="icon">
                                    <svg width="4" height="4" viewBox="0 0 4 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="2" cy="2" r="2" fill="white"/>
                                    </svg>
                                </div>
                                '. __('Confirmation','poppyz') .'
                            </div>
                        </div>';

			$count = 0;
			$description = get_post_meta($subscription->tier_id, PPY_PREFIX . 'tier_upsell_description');
			$description = (is_array($description[0])) ? $description[0] : $description;
            foreach ( $upsells as $upsell_id ) {
                //if (Poppyz_Subscription::is_subscribed_tier( $subscription->user_id, $upsell_id ) ) continue; //skip if already subscribed to the tier
				if ( empty($upsell_id) ) {
					continue;
				}
                $tier_id = $upsell_id;
                $price = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true );
                $price = Poppyz_Core::price_with_tax( $price, $tier_id, $subscription->user_id );

                $included_text = '';
                if ( Poppyz_Invoice::is_vatshifted() ) {
                    $final_tax = __( '0 (VAT shifted.)' ,'poppyz');
                } else if ( Poppyz_Invoice::is_vat_out_of_scope() ) {
                    $final_tax = __( '0 (VAT out of scope.)' ,'poppyz');
                } else if ( Poppyz_Invoice::is_vat_disabled() ) {
                    $final_tax = __( '0 (No VAT.)' );
                } else {
                    //if (  Poppyz_Core::get_price_tax( $tier_id ) == 'inc' ) {
                    $included_text = __( 'Price with tax: ' ,'poppyz');
                    //}
                }

                //hide title, show the upsell text instead
                $content .= '<style>
                .main_title {display:none;}

                </style>';
                    $content .= "<script>
                    jQuery( document ).ready(function($) {
                        $('#poppyz-confirm-" . $tier_id . "').confirm({
                            title: '<h3>" . sprintf( __( 'Add ‘%s‘ to your order?' ,'poppyz'),  get_the_title($upsell_id) ) . "</h3>',
                            content:
                             '<div class=\'confirm-content\'>" . __('By clicking the button below you add this product to your order and agree to our terms and conditions. The payment will then be debited via the payment method with which you just paid.','poppyz'). "</div>' +
                              '<div class=\'confirm-price\'>" . $included_text . '<span>' .str_replace(':','', Poppyz_Core::get_price_description( $upsell_id, $price)) . "</span></div>',
                            theme: 'poppyz',
                            useBootstrap: false,
                            closeIcon: true,
                            buttons: {
                                confirm: {
                                    text: '" . __('Yes, proceed to order','poppyz') . "',
                                    action: function () {
                                       $('#ppy-form.upsells_".$count."').submit();
                                    },
                                    btnClass: 'et_pb_button primary'
                                },
                                cancel: {
                                    text: '" . __('No, thank you','poppyz'). "',
                                    action: function () {
                                        location.href = $('.actions a.cancel').attr('href');
                                    },
                                    btnClass: 'et_pb_button '
                                }
                            }
                        });
                    });
                </script>";

                $course_id = Poppyz_Core::get_course_by_tier( $tier_id );
                $content .= '<div class="upsells-content">';
                $content .= Poppyz_Core::the_content( $description[$count]);
                $content .= '<div class="white-box">';

                    $content .='<div class="upsell upsell-content upsell-' . $tier_id . '">';
					$content .= '<div class="upsell-checkbox">';
					$content .= '<label class="checkbox-container">';
                    $content .= '<input type="checkbox" value="' . $tier_id . '" name="upsell[]">';
					$content .= '<span class="checkmark"></span></label>';
					$content .= '</div>';
                    $content .= '<input type="hidden" value="' . $subscription->id . '" name="subscription_id">';
                    $content .= '<input type="hidden" value="' . $subscription->mollie_customer_id . '" name="' . PPY_PREFIX . 'mollie_customer_id" />';
					$content .= (isset($_REQUEST['testmode']) && $_REQUEST['testmode'] == 1) ? '<input type="hidden" value="1" name="mollieTestMode" />' : '';
					if ( isset($_GET['additional_products-sold']) ) {
						$content .= '<input type="hidden" value="1" name="additional_products-sold" />';
					}
                    $content .= '<input type="hidden" value="1" name="' . PPY_PREFIX . 'purchase" />';
                    $content .= wp_nonce_field(PPY_PREFIX . 'process_payment', PPY_PREFIX . 'pay_nonce', false);
                    $content .= '<div class="tier-info">';
                    $content .= get_the_post_thumbnail( $tier_id, array(200,0));
                    $content .= '<div><h3>' . get_the_title( $course_id ) . ' - ' . get_the_title( $tier_id ) . '</h3>';
                    $content .= '<h4>' . __( 'Now for just' ,'poppyz') . ' <strong> ' .  str_replace(':','', Poppyz_Core::get_price_description( $upsell_id, $price)) . '</strong></h4></div>';
                    $content .= '</div>';
                    $content .= '</div>';
//                    $content .= '<div class="actions"><button name="' . PPY_PREFIX . 'purchase" class="proceed et_pb_button poppyz-confirm" id="poppyz-confirm-' . $tier_id . '" style="margin-bottom: 10px;">' . __('Add to order' ,'poppyz') . '</button><br />';
//                    $content .= '<a class="cancel" href="?return&upsell-pass&order_id='. $_GET['order_id']. '">' . __('No, thank you','poppyz') . '</a></div>';
                $content .= '</div>';
                $content .= '</div>';
				$count++;
            }

            $content .= '</div>';
			$content .= '</div>';


            return $content;
        }
        return false;
    }


}