<?php
/*
 * Copyright (C) 2017-present, Facebook, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/**
 * @package FacebookPixelPlugin
 */

namespace FacebookPixelPlugin\Integration;

defined('ABSPATH') or die('Direct access not allowed');

use FacebookPixelPlugin\Core\FacebookPixel;
use FacebookPixelPlugin\Core\FacebookPluginUtils;

class FacebookWordpressPoppyz extends FacebookWordpressIntegrationBase {
    const PLUGIN_FILE = 'poppyz/poppyz.php';
    const TRACKING_NAME = 'poppyz';

    private static $postID;
    private static $subscription;

    public static function injectPixelCode() {

        // Purchase
        add_action(
            'ppy_after_purchase_content',
            array(__CLASS__, 'injectPurchaseEventHook'),
            11);

        // ViewContent
        add_action(
            'ppy_view_content',
            array(__CLASS__, 'injectViewContentEventHook'),
            11);
    }

    public static function injectPurchaseEventHook($subscription) {
        static::$subscription = $subscription;

        add_action(
            'wp_footer',
            array(__CLASS__, 'injectPurchaseEvent'),
            11);
    }

    public static function injectPurchaseEvent() {
        if ( current_user_can('install_plugins') || empty(static::$subscription)) {
            return;
        }

        $subscription = static::$subscription;

        $param = array(
            'content_ids' => array($subscription->id),
            'content_type' => 'product',
            'currency' => \Poppyz_Core::get_currency(),
            'value' => $subscription->amount,
            'content_name' => get_the_title( $subscription->course_id ) . ' - ' . get_the_title($subscription->tier_id)
        );
        $code = FacebookPixel::getPixelPurchaseCode($param, self::TRACKING_NAME, true);

        printf("
<!-- Facebook Pixel Event Code -->
%s
<!-- End Facebook Pixel Event Code -->
      ",
            $code);
    }

    public static function injectViewContentEventHook($postID) {
        static::$postID = $postID;
        add_action(
            'wp_footer',
            array(__CLASS__, 'injectViewContentEvent'),
            11);
    }

    public static function injectViewContentEvent() {
        if (current_user_can('install_plugins') || empty(static::$postID)) {
            return;
        }

        $param = array(
            'content_ids' => array(static::$postID),
            'content_name' => get_the_title(static::$postID),
        );
        $code = FacebookPixel::getPixelViewContentCode($param, self::TRACKING_NAME);

        printf("
<!-- Facebook Pixel Event Code -->
%s
<!-- End Facebook Pixel Event Code -->
      ",
            $code);
    }
}
FacebookWordpressPoppyz::injectPixelCode();