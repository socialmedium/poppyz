<?php

/**
 * This registers the course post type and its taxonomies as well as its actions
 *
 * @link       http://socialmedium.nl
 * @since      0.5.0
 *
 * @package    poppyz
 * @subpackage poppyz/public
 */


class Poppyz_Type {

    /**
     * The ID of this plugin.
     *
     * @since    0.5.0
     * @access   private
     * @var      string    $poppyz    The ID of this plugin.
     */
    private $poppyz;

    /**
     * The post type name
     *
     * @since    0.5.0
     * @access   public
     * @var      string    $post_type    The name of the post type
     */
    public $post_type;

    /**
     * Initialize the class and set its properties.
     *
     * @since    0.5.0
     * @var      string    $poppyz       The name of the plugin.
     * @var      string    $version    The version of this plugin.
     */
    public function __construct( $poppyz ) {
        $this->poppyz = $poppyz;
    }

    /**
     * Register the course post type
     *
     * @since    0.5.0
     */
    function register_post_type() {

        $labels = array(
            'name'                => _x( 'Courses', 'Post Type General Name', $this->poppyz ),
            'singular_name'       => _x( 'Course', 'Post Type Singular Name', $this->poppyz ),
            'menu_name'           => __( 'Courses', $this->poppyz ),
            'parent_item_colon'   => __( 'Parent Item:', $this->poppyz ),
            'all_items'           => __( 'All Courses', $this->poppyz ),
            'view_item'           => __( 'View Course', $this->poppyz ),
            'add_new_item'        => __( 'Add New Course', $this->poppyz ),
            'add_new'             => __( 'Add New', $this->poppyz ),
            'edit_item'           => __( 'Edit Course', $this->poppyz ),
            'update_item'         => __( 'Update Course', $this->poppyz ),
            'search_items'        => __( 'Search Course', $this->poppyz ),
            'not_found'           => __( 'Not found', $this->poppyz ),
            'not_found_in_trash'  => __( 'Not found in Trash', $this->poppyz ),
        );
        $rewrite = array(
            'slug'                => 'course',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => __( PPY_COURSE_PT, $this->poppyz ),
            'description'         => __( 'Course Post Type', $this->poppyz ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'excerpt', 'editor', 'thumbnail', 'revisions', 'page-attributes', 'comments'  ),
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => false,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'page',
        );
        register_post_type( PPY_COURSE_PT, $args );
        register_taxonomy(
            PPY_LESSON_CAT,
            PPY_LESSON_PT,
            array(
                'labels' => array(
                    // Add new taxonomy, make it hierarchical (like categories)
                    'name'              => _x( 'Lessonclusters', $this->poppyz ),
                    'singular_name'     => _x( 'Lessencluster', $this->poppyz ),
                    'menu_name'         => __( 'Lessonclusters', $this->poppyz ),
                ),
                'public' => true,
                'rewrite' => array(
                    'slug' => 'lesson-category'
                ),
                'hierarchical' => true,
            )
        );
        $labels = array(
            'name'                => _x( 'Lessons', 'Post Type General Name', $this->poppyz ),
            'singular_name'       => _x( 'Lesson', 'Post Type Singular Name', $this->poppyz ),
            'menu_name'           => __( 'Lessons', $this->poppyz ),
            'parent_item_colon'   => __( 'Parent Item:', $this->poppyz ),
            'all_items'           => __( 'All Lessons', $this->poppyz ),
            'view_item'           => __( 'View Lesson', $this->poppyz ),
            'add_new_item'        => __( 'Add New Lesson', $this->poppyz ),
            'add_new'             => __( 'Add New', $this->poppyz ),
            'edit_item'           => __( 'Edit Lesson', $this->poppyz ),
            'update_item'         => __( 'Update Lesson', $this->poppyz ),
            'search_items'        => __( 'Search Lesson', $this->poppyz ),
            'not_found'           => __( 'Not found', $this->poppyz ),
            'not_found_in_trash'  => __( 'Not found in Trash', $this->poppyz ),
        );
        $rewrite = array(
            'slug'                => 'lesson',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => __( PPY_LESSON_PT, $this->poppyz ),
            'description'         => __( 'Lesson Post Type', $this->poppyz ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'excerpt', 'editor', 'thumbnail', 'revisions', 'post-thumbnail', 'comments' ),
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => false,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'page',
        );

        register_post_type( PPY_LESSON_PT, $args );



        $labels = array(
            'name'                => _x( 'Tiers', 'Post Type General Name', $this->poppyz ),
            'singular_name'       => _x( 'Tier', 'Post Type Singular Name', $this->poppyz ),
            'menu_name'           => __( 'Tiers', $this->poppyz ),
            'parent_item_colon'   => __( 'Parent Item:', $this->poppyz ),
            'all_items'           => __( 'All Tiers', $this->poppyz ),
            'view_item'           => __( 'View Tier', $this->poppyz ),
            'add_new_item'        => __( 'Add New Tier', $this->poppyz ),
            'add_new'             => __( 'Add New', $this->poppyz ),
            'edit_item'           => __( 'Edit Tier', $this->poppyz ),
            'update_item'         => __( 'Update Tier', $this->poppyz ),
            'search_items'        => __( 'Search Tier', $this->poppyz ),
            'not_found'           => __( 'Not found', $this->poppyz ),
            'not_found_in_trash'  => __( 'Not found in Trash', $this->poppyz ),
        );
        $args = array(
            'label'               => __( PPY_TIER_PT, $this->poppyz ),
            'description'         => __( 'Tier Post Type', $this->poppyz ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', ),
            'hierarchical'        => true,
            'public'              => false,
            'show_ui'             => true,
            'show_in_menu'        => false,
            'show_in_nav_menus'   => false,
            'show_in_admin_bar'   => false,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
        );
        register_post_type( PPY_TIER_PT, $args );

        $labels = array(
            'name'                => _x( 'Invoices', 'Post Type General Name', $this->poppyz ),
            'singular_name'       => _x( 'Invoice', 'Post Type Singular Name', $this->poppyz ),
            'menu_name'           => __( 'Invoices', $this->poppyz ),
            'parent_item_colon'   => __( 'Parent Invoice:', $this->poppyz ),
            'all_items'           => __( 'All Invoices', $this->poppyz ),
            'view_item'           => __( 'View Invoice', $this->poppyz ),
            'add_new_item'        => __( 'Add New Invoice', $this->poppyz ),
            'add_new'             => __( 'Add New', $this->poppyz ),
            'edit_item'           => __( 'Edit Invoice', $this->poppyz ),
            'update_item'         => __( 'Update Invoice', $this->poppyz ),
            'search_items'        => __( 'Search Invoice', $this->poppyz ),
            'not_found'           => __( 'Not found', $this->poppyz ),
            'not_found_in_trash'  => __( 'Not found in Trash', $this->poppyz ),
        );
        $args = array(
            'label'               => __( PPY_INVOICE_PT, $this->poppyz ),
            'description'         => __( 'Invoice Post Type', $this->poppyz ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor' ),
            'hierarchical'        => true,
            'public'              => false,
            'show_ui'             => true,
            'show_in_menu'        => false,
            'show_in_nav_menus'   => false,
            'show_in_admin_bar'   => false,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
        );
        register_post_type( PPY_INVOICE_PT, $args );

        $labels = array(
            'name'                => _x( 'Coupons', 'Post Type General Name', $this->poppyz ),
            'singular_name'       => _x( 'Coupon', 'Post Type Singular Name', $this->poppyz ),
            'menu_name'           => __( 'Coupons', $this->poppyz ),
            'parent_item_colon'   => __( 'Parent Item:', $this->poppyz ),
            'all_items'           => __( 'All Coupons', $this->poppyz ),
            'view_item'           => __( 'View Coupon', $this->poppyz ),
            'add_new_item'        => __( 'Add New Coupon', $this->poppyz ),
            'add_new'             => __( 'Add New', $this->poppyz ),
            'edit_item'           => __( 'Edit Coupon', $this->poppyz ),
            'update_item'         => __( 'Update Coupon', $this->poppyz ),
            'search_items'        => __( 'Search Coupon', $this->poppyz ),
            'not_found'           => __( 'Not found', $this->poppyz ),
            'not_found_in_trash'  => __( 'Not found in Trash', $this->poppyz ),
        );
        $args = array(
            'label'               => __( PPY_COUPON_PT, $this->poppyz ),
            'description'         => __( 'Coupon Post Type', $this->poppyz ),
            'labels'              => $labels,
            'supports'            => array( 'title' ),
            'hierarchical'        => true,
            'public'              => false,
            'show_ui'             => true,
            'show_in_menu'        => false,
            'show_in_nav_menus'   => false,
            'show_in_admin_bar'   => false,
            'menu_position'       => 6,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
        );
        register_post_type( PPY_COUPON_PT, $args );

        //add menu order to lesson custom post type
        add_post_type_support( PPY_LESSON_PT, 'page-attributes' );

    }


}
