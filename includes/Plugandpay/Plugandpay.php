<?php

class Plugandpay {
    public $client;
    public $token;

    public function __construct()
    {
        $this->token = Poppyz_Core::get_option('plugandpay_token');
    }

    function getOrder($orderId) {

        $url = 'https://api.plugandpay.nl/v2/orders/' . $orderId . '?include=subscriptions';

        // The arguments for the request
        $args = array(
            'headers' => array(
                'Authorization' => 'Bearer ' . $this->token,
                'Content-Type'  => 'application/json',
            ),
        );

        // Make the POST request
        $response = wp_remote_get($url, $args);

        // Check for errors
        if (is_wp_error($response)) {
            $error_message = $response->get_error_message();
            error_log($error_message);
            return false;
        }
        // Handle the successful response
        $response_body = wp_remote_retrieve_body($response);
        $response_code = wp_remote_retrieve_response_code($response);
        if ($response_code == 200) {
            return json_decode($response_body);
        } else {
            error_log('Invalid response: ' . $response_code);
        }
        return false;
    }

}

