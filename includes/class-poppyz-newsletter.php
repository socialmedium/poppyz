<?php

/**
 * This handles all newsletter subscriptions like MailChimp and AWeber
 *
 * @link       http://socialmedium.nl
 * @since      0.8.8
 *
 * @package    poppyz
 * @subpackage poppyz/public
 */


class Poppyz_Newsletter {


    /**
     * The MailChimp API Key
     *
     * @since    0.8.8
     * @access   protected
     * @var      string    $mc_api_key  The MailChimp API key on the plugin settings page
     */
    protected $mc_api_key;

    /**
     * The AWeber API Object
     *
     * @since    0.8.8
     * @access   protected
     * @var      object    $aweber  The AWeber API Object
     */
    protected $aweber;

    /**
     * The AWeber Account Object
     *
     * @since    0.8.8
     * @access   protected
     * @var      object    $aweber  The AWeber Account Object
     */
    protected $aw_account;

    /**
     * The AWeber App ID
     *
     * @since    0.8.8
     * @access   protected
     * @var      string    $aw_app_id  The AWeber App ID
     */
    protected $aw_app_id;

    /**
     * The AWeber Authorization code
     *
     * @since    0.8.8
     * @access   protected
     * @var      string    $mc_api_key  The AWeber Authorization code on the plugin settings page
     */
    protected $aw_code;

    /**
     * The Laposta Authorization code
     *
     * @since    0.8.8
     * @access   protected
     * @var      string    $mc_api_key  The AWeber Authorization code on the plugin settings page
     */
    protected $la_code;

    /**
     * Initialize the class and set its properties.
     *
     * @since    0.8.8
     */
    public function __construct() {
        $this->aw_app_id = 'da214c21';
    }

    public static function mc_get_lists() {
        $api_key =  Poppyz_Core::get_option( 'mc_api_key' );

        $index = strrpos($api_key, "-");
        if( $index === false ) { //in case no dash was found
            return false;
        } else {
            $dc = substr( $api_key, $index + 1  );
            $url = 'https://' . $dc . '.api.mailchimp.com/3.0/';
            $target = 'lists?count=100';

            $response = wp_remote_get(  $url . $target ,
                array(
                    'timeout' => 10,
                    'user-agent' => 'PHP-MCAPI/2.0',
                    'headers' => array(
                        'Content-Type' => 'application/json',
                        'Authorization' => 'apikey ' . $api_key
                    ),
                    'sslverify' => false
                )
            );

            if ( is_wp_error( $response ) ) {
                return $response->get_error_message();
            }

            return json_decode( $response['body'] );
        }
    }

    public function mc_subscribe( $user_id, $tier_id ) {
        $user = get_user_by( 'id', $user_id );
        $api_key = Poppyz_Core::get_option( 'mc_api_key' );
        if ( !$api_key ) return false;

        $list_id = get_post_meta( $tier_id, PPY_PREFIX . 'mailchimp_list', true );
        $tags = get_post_meta( $tier_id, PPY_PREFIX . 'mailchimp_tags', true );

        $index = strrpos($api_key, "-");

        if( $index !== false && !empty( $list_id ) && $list_id !== '-1' ) {

            $dc = substr( $api_key, $index + 1  );
            $data = array(
                'apikey'        => $api_key,
                'email_address' => $user->user_email,
                'status'        => 'subscribed'
            );
            if ( !empty($user->first_name) ) {
                $data['merge_fields']['FNAME'] = $user->first_name;
            }
            if ( !empty($user->first_name) ) {
                $data['merge_fields']['LNAME'] = $user->last_name;
            }

            if ($tags) {
                $tags = preg_replace('/\s*,\s*/', ',', $tags ); //remove spaces before and after commas
                $tags = trim($tags, ',');
                $tags = explode(',', $tags);
                $data['tags'] = $tags;
            }

            $data = apply_filters( 'ppy_mailchimp_custom_fields', $user_id, $list_id, $data);

            $url = 'https://' . $dc . '.api.mailchimp.com/3.0/';

            $auth = base64_encode( 'user:'. $api_key );
            $json_data = json_encode( $data );

            $user_email = md5($user->user_email);
            $target = 'lists/' . $list_id .  '/members/' . $user_email;

            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url . $target );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                'Authorization: Basic '.$auth ) );
            curl_setopt( $ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0' );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );

            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );

            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );

            $result = curl_exec( $ch );
            curl_close( $ch );

            $result = json_decode( $result );

            if ( $result->status != '404' || $result->status == 'subscribed' ) {
                //if subscribed, add tag to the member
                if (isset($data['tags']) && is_array($data['tags'])) {
                    $formatted_tags = array();
                    foreach ( $data['tags'] as $tag) {
                        $formatted_tags[] = [
                            "name" => $tag,
                            "status" => 'active'
                        ];
                    }
                    $body = [
                        'tags' => $formatted_tags,
                    ];
                    $body = wp_json_encode( $body );
                    $response = wp_remote_post(  $url . $target . '/tags',
                        array(
                            'timeout' => 10,
                            'headers' => array(
                                'User-Agent' => 'PHP-MCAPI/2.0',
                                'Content-Type' => 'application/json',
                                'Authorization' => 'Basic '.$auth
                            ),
                            'sslverify' => false,
                            'body' => $body,
                            'data_format' => 'body',
                        )
                    );
                }

                //if subscribed, just update the user information on mailchimp
                $type = 'PATCH';
                $target = 'lists/' . $list_id .  '/members/' . $result->id;

            } else {
                //if not, add a new mailchimp subscriber
                $type = 'POST';
                $target = 'lists/' . $list_id . '/members/';
            }

            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url . $target );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                'Authorization: Basic '.$auth ) );
            curl_setopt( $ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0' );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );

            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $type );

            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data );

            $result = curl_exec( $ch );
            curl_close( $ch );

            $result = json_decode( $result );

            return $result;
        }
    }

    /**
     * Initialize AWeber API
     *
     * @since    0.8.8
     */
    public function aw_initialize() {

        $this->aw_code = Poppyz_Core::get_option( 'aw_code' );

        if ( empty( $this->aw_code )  ) {
            return false;
        }
        $consumer_key = Poppyz_Core::get_option( 'aw_consumer_key' );
        $consumer_secret = Poppyz_Core::get_option( 'aw_consumer_secret' );
        $access_key = Poppyz_Core::get_option( 'aw_access_key' );
        $access_secret = Poppyz_Core::get_option( 'aw_access_secret' );

        if ( !class_exists('AWeberAPI') ) {
            require_once(PPY_DIR_PATH . 'includes/aweber_api/aweber_api.php');
        }

        try {
            $this->aweber = new AWeberAPI($consumer_key, $consumer_secret);
            $this->aw_account = $this->aweber->getAccount($access_key, $access_secret);
        } catch ( AWeberAPIException $exc ) {
            error_log($exc->message);
            return false;
        }
        return true;
    }

    public function aw_get_authorization_link() {
        return 'https://auth.aweber.com/1.0/oauth/authorize_app/' . $this->aw_app_id;
    }
    public function aw_get_lists() {
        if (! $this->aw_account ) return false;
        try {
            $lists = array();
            foreach ($this->aw_account->lists as $list) {
                $lists[$list->id] = $list->name;
            }
            return $lists;

        } catch(AWeberAPIException $exc) {
            error_log($exc->message);
            return false;
        }
    }

    public function aw_subscribe( $user_id, $tier_id ) {
        try {
            $user = get_user_by( 'id', $user_id );
            $list_id = get_post_meta( $tier_id, PPY_PREFIX . 'aweber_list', true);

            if ( !$list_id ) return;
            $listURL = '/accounts/' . $this->aw_account->data['id'] .  '/lists/' . $list_id;
            $list = $this->aw_account->loadFromUrl($listURL);
            $name = $user->first_name . ' ' . $user->last_name;

            $subscribers = $list->subscribers;

            //if already subscribed, just update the name
            /*$params = array( 'status' => 'subscribed', 'email' => $user->user_email );
            $found_subscribers = $subscribers->find( $params );
            foreach($found_subscribers as $subscriber) {
                if( $subscriber->email == $user->user_email ) {
                    return false; //the user is already subscribed to this list, don't continue
                }
            }*/

            # create a subscriber
            $params = array(
                'email' => $user->user_email,
                'name' => $name,
            );
            $new_subscriber = $subscribers->create( $params );
        } catch( AWeberAPIException $exc ) {
	        ob_start();
	        echo 'ActiveCampaign error: ';
	        var_dump($exc);
	        $contents = ob_get_contents();
	        ob_end_clean();
	        error_log($contents);
	        return false;
        }
    }

    public static function av_get_lists() {
        $api_key =  Poppyz_Core::get_option( 'ac_api_key' );
        $api_url =  Poppyz_Core::get_option( 'ac_api_url' );
        if ( !$api_key || !$api_url ) return false;
        $params = array(
            'api_key'      => $api_key,
            'api_action'   => 'list_list',
            'api_output'   => 'json',
            'ids' => 'all'
        );

        $query = "";
        foreach( $params as $key => $value ) $query .= $key . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        // clean up the url
        $api_url = rtrim($api_url, '/ ');

	    //set_time_limit(30);

        // define a final API request - GET
        $api_url = $api_url . '/admin/api.php?' . $query;
        $response = wp_remote_get(  $api_url,
            array(
                'timeout' => 10,
                'user-agent' => 'PHP-acAPI/2.0',
                'sslverify' => false
            )
        );

        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }

        return json_decode( $response['body'] );

    }

    public function av_subscribe( $user_id, $tier_id ) {
        $api_key =  Poppyz_Core::get_option( 'ac_api_key' );
        $api_url =  Poppyz_Core::get_option( 'ac_api_url' );

        if ( !$api_key || !$api_url ) return false;

        $user = get_user_by( 'id', $user_id );
        $company = get_user_meta( $user_id, PPY_PREFIX . 'company', true);
        $phone = get_user_meta( $user_id, PPY_PREFIX . 'phone', true);
        $list_id = get_post_meta( $tier_id, PPY_PREFIX . 'av_list', true);

        $tags = get_post_meta( $tier_id, PPY_PREFIX . 'av_tags', true);
        if ( !$tags ) {
            $tags =  Poppyz_Core::get_option( 'nl_tags' );
        }

        if ( !$list_id ) return;

        $params = array(
            'api_key'      => $api_key,
            'api_action'   => 'contact_sync',
            'api_output'   => 'json',
        );

        $post = array(
            'email'                    => $user->user_email,
            'first_name'               => $user->first_name,
            'last_name'                => $user->last_name,
            'p[' . $list_id .']'                   => $list_id, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
            'status[' . $list_id . ']'              => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 1)
        );


        if ( !empty( $company ) ) {
            $post['orgname'] = $company;
        }
        if ( !empty( $phone ) ) {
            $post['phone'] = $phone;
        }

        if ( !empty( $tags ) ) {
            $post['tags'] = rtrim( $tags, ',' );
        }

        // This section takes the input fields and converts them to the proper format
        $query = "";
        foreach( $params as $key => $value ) $query .= $key . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        $data = "";
        foreach( $post as $key => $value ) $data .= $key . '=' . urlencode($value) . '&';
        $data = rtrim($data, '& ');

        $api_url = rtrim($api_url, '/ ');

        $api_url = $api_url . '/admin/api.php?' . $query;

        $response = wp_remote_post( $api_url, array(
                'method' => 'POST',
                'httpversion' => '1.0',
                'sslverify' => false,
                'body' => $data
            )
        );

	    //error_log( $response['body'] . 'xxx' );

        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }

        return json_decode( $response['body'] );
    }

    public static function ml_get_groups(){
        $api_key =  Poppyz_Core::get_option( 'ml_api_key' );
        if ( !$api_key ) return false;

        $url = 'http://api.mailerlite.com/api/v2/';
        $target = 'groups';

        $response = wp_remote_get(  $url . $target ,
            array(
                'timeout' => 10,
                'headers' => array(
                    'User-Agent' => 'MailerLite PHP SDK/2.0',
                    'Content-Type' => 'application/json',
                    'X-MailerLite-ApiKey' => $api_key
                ),
                'sslverify' => false
            )
        );

        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }

        return json_decode( $response['body'] );

    }

    public static function ml_subscribe($user_id, $tier_id){
        $api_key =  Poppyz_Core::get_option( 'ml_api_key' );
        if ( !$api_key ) return false;
        $url = 'http://api.mailerlite.com/api/v2/';

        $list_id = get_post_meta( $tier_id, PPY_PREFIX . 'mailerlite_list', true );

        $target = (!empty( $list_id ) && $list_id !== '-1' ) ? 'groups/' . $list_id . '/subscribers' : 'subscribers';

        $user = get_user_by( 'id', $user_id );
        require_once( PPY_DIR_PATH . 'public/class-poppyz-user.php' );
        $custom_fields = Poppyz_User::custom_fields();
        foreach ( $custom_fields as $name => $value) {
            $$value = get_user_meta( $user->ID, PPY_PREFIX . $value, true );
        }
        $data = [
            'email' => $user->user_email,
            'fields' => [
                'name' => $user->first_name,
                'last_name' =>  $user->last_name,
                'company' => $company,
                'city' => $city,
                'country' => $country,
                'phone' => $phone,
                'zip' => $zipcode,
            ]
        ];

        $data = wp_json_encode( $data );
        $response = wp_remote_post(  $url . $target ,
            array(
                'timeout' => 10,
                'headers' => array(
                    'User-Agent' => 'MailerLite PHP SDK/2.0',
                    'Content-Type' => 'application/json',
                    'X-MailerLite-ApiKey' => $api_key
                ),
                'sslverify' => false,
                'body' => $data,
                'data_format' => 'body',
            )
        );

        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }

        return json_decode( $response['body'] );

    }

    public function la_get_lists() {
        $api_key =  Poppyz_Core::get_option( 'la_api_key' );
        if ( !$api_key ) return false;

        $url = 'https://api.laposta.nl/';
        $version = 'v2';
        $target = 'list';

        $response = wp_remote_get(  $url . $version .'/' . $target ,
            array(
                'timeout' => 10,
                'headers' => array(
                    'Authorization' => 'Basic ' . base64_encode( $api_key . ':'),
                ),
                'sslverify' => false
            )
        );

        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }
        return json_decode( $response['body'] );

    }

    public function la_subscribe( $user_id, $tier_id ) {
        $api_key =  Poppyz_Core::get_option( 'la_api_key' );
        if ( !$api_key ) return false;

        $url = 'https://api.laposta.nl/';
        $version = 'v2';
        $target = 'member';

        $list_id = get_post_meta( $tier_id, PPY_PREFIX . 'la_list', true );

        if ( empty($list_id) || $list_id == '-1') {
            return false;
        }
        $user = get_user_by( 'id', $user_id );
        require_once( PPY_DIR_PATH . 'public/class-poppyz-user.php' );
        $custom_fields = Poppyz_User::custom_fields();
        foreach ( $custom_fields as $name => $value) {
            $$value = get_user_meta( $user->ID, PPY_PREFIX . $value, true );
        }

        $data = [
            'email' => $user->user_email,
            'list_id' => $list_id,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'custom_fields' => [
                'name' => $user->first_name . ' ' . $user->last_name,
                'company' => $company,
                'city' => $city,
                'country' => $country,
                'phone' => $phone,
                'zip' => $zipcode,
            ]
        ];

        $data = http_build_query( $data );
        $response = wp_remote_post(  $url . $version . '/' . $target ,
            array(
                'timeout' => 10,
                'headers' => array(
                    'Authorization' => 'Basic ' . base64_encode( $api_key . ':'),
                ),
                'sslverify' => false,
                'body' => $data,
                'data_format' => 'body',
            )
        );


        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }

        return json_decode( $response['body'] );

    }


}
