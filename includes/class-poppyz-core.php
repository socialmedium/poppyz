<?php

class Poppyz_Core {
	/**
	 * Retrieve all courses
	 *
	 * @since     0.6.0
	 * @return    WP_Query    Return WP Query object or false on empty
	 */
	public static function get_courses( $status = 'publish' ) {
		return new WP_Query( array( 'post_type' => PPY_COURSE_PT, 'posts_per_page' => -1, 'post_status' => $status ) );
	}

	/**
	 * Retrieve the lessons of a course
	 *
	 * @since     0.6.0
	 * @var       string    $course_id  The id of the course to retrieve lessons from
	 * @return    WP_Query    Return WP Query object or false on empty
	 */
	public function get_lessons_by_course( $course_id ) {
		$args = array(
			'post_type' =>  PPY_LESSON_PT,
			'posts_per_page' => -1,
			'order' => 'ASC',
			'orderby'  =>  'menu_order',
			'meta_query' => array(
				array(
					'key' => PPY_PREFIX . 'lessons_course',
					'value' => array($course_id),
					'compare' => 'IN',
				),

			));

		return new WP_Query( $args );

	}

	/**
	 * Retrieve the lessons by their ids
	 *
	 * @since     0.6.0
	 * @var       array    $lessons_ids  The id's of the lessons to retrieve.
	 * @return    WP_Query    Return WP Query object or false on empty
	 */
	public function get_lessons_by_ids( $lesson_ids, $category = null ) {
		$args = array(
			'post_type' =>  PPY_LESSON_PT,
			'posts_per_page' => -1,
			'order' => 'ASC',
			'orderby'  =>  'menu_order',
			'post__in' => $lesson_ids
		);

		if ( $category ) {
			$args['tax_query'] = array(
				array(
					'taxonomy' => PPY_LESSON_CAT,
					'field'    => 'term_id',
					'terms'    => $category,
				),
			);
		}

		return new WP_Query( $args );

	}

	public function get_lesson_ids_by_course( $course_id ) {
		global $wpdb;

		$lessons = $wpdb->get_col( $wpdb->prepare(
			"
                    SELECT p.ID
                    FROM $wpdb->posts p
                    INNER JOIN $wpdb->postmeta pm
                    ON ( p.ID = pm.post_id )
                    WHERE 1=1
                    AND p.post_type = %s
                    AND (p.post_status = 'publish'
                    OR p.post_status = 'private')
                    AND ( ( pm.meta_key = %s
                    AND CAST(pm.meta_value AS CHAR) IN (%s) ) )
                    GROUP BY p.ID
                    ORDER BY p.menu_order ASC
                    ",
			PPY_LESSON_PT,
			PPY_PREFIX . 'lessons_course',
			$course_id
		)
		);

		return $lessons;
	}

	public function get_tier_ids_by_course( $course_id ) {
		global $wpdb;

		$lessons = $wpdb->get_col( $wpdb->prepare(
			"
                    SELECT p.ID
                    FROM $wpdb->posts p
                    INNER JOIN $wpdb->postmeta pm
                    ON ( p.ID = pm.post_id )
                    WHERE 1=1
                    AND p.post_type = %s
                    AND (p.post_status = 'publish'
                    OR p.post_status = 'private')
                    AND ( ( pm.meta_key = %s
                    AND CAST(pm.meta_value AS CHAR) IN (%s) ) )
                    GROUP BY p.ID
                    ORDER BY p.menu_order ASC
                    ",
			PPY_TIER_PT,
			PPY_PREFIX . 'tier_course',
			$course_id
		)
		);

		return $lessons;
	}

	public static function get_all_tiers() {
		$args = array(
			'post_type' =>  PPY_TIER_PT,
			'posts_per_page' => -1,
			'order' => 'ASC',
		);

		return new WP_Query( $args );
	}

	/**
	 * Retrieve all the tiers of a course
	 *
	 * @since     0.6.0
	 * @var       int    $course_id  The id of the course to retrieve lessons from
	 * @return    Object    Return an object or NULL on empty
	 */
	public function get_tiers_by_course( $course_id ) {
		$args = array(
			'post_type' =>  PPY_TIER_PT,
			'posts_per_page' => -1,
			'order' => 'ASC',
			'orderby'  =>  'menu_order',
			'meta_query' => array(
				array(
					'key' => PPY_PREFIX . 'tier_course',
					'value' => array($course_id),
					'compare' => 'IN',
				),

			));
		return new WP_Query( $args );
	}

	/**
	 * Retrieve the tier's course
	 *
	 * @since     0.6.0
	 * @var       int    $tier_id  The id of the tier
	 * @return    int    Return the ID of the course
	 */
	public static function get_course_by_tier( $tier_id ) {
		return get_post_meta( $tier_id, PPY_PREFIX . "tier_course", true );
	}

	/**
	 * Retrieve the lessons's course
	 *
	 * @since     0.6.0
	 * @var       int    $lesson_id  The id of the lesson
	 * @return    int    Return the ID of the course
	 */
	public static function get_course_by_lesson( $lesson_id ) {
		return get_post_meta( $lesson_id, PPY_PREFIX . "lessons_course", true );
	}


	/**
	 * Check if the user has access to the course
	 *
	 * @since     0.6.0
	 * @var       int    $course_id  The id of the course to check access to
	 * @return    bool    Return true if the user has access to the post
	 */
	public function has_access( $course_id ) {
		global $wpdb;
	}

	/*
	 * Return the start date of the course.
	 *
	*  @var       int    $course_id  The id of the course to check access from
	 * @return    bool    Return true if the user has access to the post
	 * */
	public function get_course_start_date() {

	}

	/**
	 * Get a tier by its slug
	 *
	 * @since     0.6.0
	 * @var       string    $slug  The slug string
	 * @return    int    Return result or return false if empty
	 */
	public function get_tier_by_slug( $slug ) {
		global $wpdb;
		$slug_meta = PPY_PREFIX . "tier_slug";

		return $wpdb->get_var( $wpdb->prepare(
			"
                    SELECT post_id
                    FROM $wpdb->postmeta
                    WHERE meta_key = %s
                    AND meta_value = %s
                    ",
			$slug_meta,
			$slug
		) );

	}

	/**
	 * Get plugin options
	 *
	 * @since     0.6.0
	 * var       string    $name  The option name
	 * @return    string    Return option value
	 */
	public static function get_option( $name ) {
		$options = get_option( 'ppy_settings');
		if ( isset( $options[$name] ) )
			return $options[$name];
		else return false;
	}

	/**
	 * Save plugin options
	 *
	 * @since     0.6.0
	 * var       string    $name  The option name
	 * var       string    $value  The option value
	 */
	public static function save_option( $name, $value ) {
		$options_saved = get_option( 'ppy_settings' );
		$options_saved[$name] = $value;
		update_option( 'ppy_settings' , $options_saved );
	}

	/**
	 * Delete plugin options
	 *
	 * @since     0.6.0
	 * var       string    $name  The option name
	 */
	public static function delete_option( $name  ) {
		$options_saved = get_option( 'ppy_settings' );
		unset( $options_saved[$name] );
		update_option( 'ppy_settings' , $options_saved );
	}

	public function sort(){
		$arrowPosition = "poppyz-down";
		if (isset($_GET['sort'])) {
			$sort = (int) $_GET['sort'];
			switch($sort) {
				case 1:
					return [
						"arrowPosition" =>$arrowPosition,
						"field"=>"fullname",
						"extra" =>  isset($_GET['query']) ? sanitize_text_field($_GET['query']) : "",
					];
					break;
				case 2:
					return [
						"arrowPosition" =>$arrowPosition,
						"field"=>"course",
						"extra" =>  isset($_GET['query']) ? sanitize_text_field($_GET['query']) : "",
					];
					break;

				case 3:
					return [
						"arrowPosition" =>$arrowPosition,
						"field"=>"tier",
						"extra" =>   isset($_GET['query']) ? sanitize_text_field($_GET['query']) : "",
					];
					break;
				case 4:
					return [
						"arrowPosition" =>$arrowPosition,
						"field"=>"subscription_date",
						"extra" =>  isset($_GET['query']) ? sanitize_text_field($_GET['query']) : "",
					];
					break;
                case 5:
                    return [
                        "arrowPosition" =>$arrowPosition,
                        "field"=>"payment_status",
                        "extra" =>  isset($_GET['query']) ? sanitize_text_field($_GET['query']) : "",
                    ];
                    break;
				default:
					return [];

			}

		}
		return [];
	}
	/**
	 * Get students
	 *
	 * @since     0.6.0
	 * var      Array    $name  The option name
	 * @return    Array    Return students
	 */
	public static function get_students( $filter = array(), $pagination_options = null, $searchStudent = null ) {
		global $wpdb;
		$student_data = [];
		$current_url = '?page=poppyz-students';
		$subs_table = $wpdb->prefix . 'course_subscriptions';
		$where = "";
		$ppy_payment = new Poppyz_Payment();
		$payment_type_description = "";

        if ( !empty($filter) ) {
			foreach ( $filter as $k => $v ) {

				if ( $k == 'subscription_date' ) {
					$where .= " AND $k LIKE '$v%'"; //since we don't need to know the exact time
				} else {
					$where .= " AND $k = $v";
				}
			}
        }

		$pagination = '';
		if ($pagination_options && !empty($pagination_options['page'])) {
			$items_per_page = $pagination_options['records_per_page'];
			$offset = ($pagination_options['page'] - 1) * $items_per_page;
			$pagination = 'LIMIT ' . $offset . ',' . $items_per_page;
		}
		$field = "";
		$sort = "asc";
		$SQL = "SELECT * FROM $subs_table u WHERE 1 = 1 $where ORDER BY u.subscription_date DESC $pagination";
		if(isset($pagination_options['sort_request']) && count($pagination_options['sort_request'])){
			$field = $pagination_options['sort_request']['field'];
			$sort = $pagination_options['sort_request']['extra'];

			if($field =="fullname"){
				$SQL = "SELECT DISTINCT * FROM $subs_table u INNER JOIN " . $wpdb->prefix . "usermeta um ON um.user_id = u.user_id WHERE um.meta_key='first_name' $where ORDER BY um.meta_value $sort $pagination";
			}
			if($field =="course"){
				$SQL = "SELECT DISTINCT * FROM $subs_table u INNER JOIN " . $wpdb->prefix . "posts p ON p.ID = u.course_id $where ORDER BY p.post_title $sort $pagination";

			}
			if( $field =="tier"){
				$SQL = "SELECT DISTINCT * FROM $subs_table u INNER JOIN " . $wpdb->prefix . "posts p ON p.ID = u.tier_id $where ORDER BY p.post_title $sort $pagination";

			}
			if($field =="subscription_date"){
				$SQL = "SELECT * FROM $subs_table u WHERE 1 = 1 $where ORDER BY subscription_date $sort $pagination";

			}
            if($field =="payment_status"){
                $SQL = "SELECT * FROM $subs_table u WHERE 1 = 1 $where ORDER BY payment_status $sort $pagination";
            }

		}

		$res = $wpdb->get_results($SQL, ARRAY_A);
		$searchUserResultId = [];
        if ( $searchStudent != null) {
			$searchStudentArgs = array(
				'search'         => '*' . esc_attr( $searchStudent ) . '*',
				'search_columns' => array( 'user_login', 'user_email', 'user_nicename', 'display_name' )
			);
			$user_query = new WP_User_Query( $searchStudentArgs );
			$searchUserResultId = [];
			foreach ( $user_query->get_results() as $user ) {
				$searchUserResultId[] = $user->ID;
			}
        }

		for($i = 0; $i < count($res); $i++){
            $userId = $res[$i]['user_id'];
            if ( !empty($searchUserResultId) && !in_array($userId, $searchUserResultId)) {
                continue;
            }
			$payment_method = $res[$i]['payment_method'];
			$payments_made  = $res[$i]['payments_made'];
			$timeframe = 'months';
			$is_automatic = get_post_meta(  $res[$i]['tier_id'], PPY_PREFIX . 'payment_automatic', true );
			if ( $res[$i]['payment_type'] == 'automatic' ) {
				$is_automatic = true;
			}
			if ( $res[$i]['payment_type']== 'automatic' || $res[$i]['payment_type'] == '' ) {
				$payment_type_description = '<br />'  . __( 'Automatic Payment' ,'poppyz');
			}
			if ($res[$i]['payment_type'] == 'manual' ) {
				$is_automatic = false;
				$payment_type_description =  '<br />'  . __( 'Manual Payment','poppyz' );
			}
			if ( $payment_method == 'membership' ) {
				$method_name =  __( 'Membership' );
				$interval = get_post_meta( $res[$i]['tier_id'],  PPY_PREFIX . 'membership_interval', true );
				$timeframe = get_post_meta( $res[$i]['tier_id'],  PPY_PREFIX . 'membership_timeframe', true );
			} elseif ( $payment_method == 'plan' ) {
				$method_name =  __( 'Payment Plan' );
				$times = (int)get_post_meta($res[$i]['tier_id'], PPY_PREFIX .  'number_of_payments', true );
				$interval = get_post_meta( $res[$i]['tier_id'], PPY_PREFIX . 'payment_frequency', true );
				$timeframe = get_post_meta($res[$i]['tier_id'], PPY_PREFIX . 'payment_timeframe', true );
			}

			if ( empty( $interval ) ) {
				$interval = 1;
			}
			if ( !$timeframe ) {
				$timeframe = 'months';
			}

			$user_data = get_userdata($res[$i]['user_id']);
			if ($user_data)  {
				$first_name = get_user_meta($res[$i]['user_id'],'first_name',true);
				$last_name = get_user_meta($res[$i]['user_id'], 'last_name', true);
				$fullname = $first_name . ' ' . $last_name . '(' . $user_data->user_login .')';
			} else {
				$fullname = "<i>" . __( 'User Deleted' ,'poppyz') . "</i>";
			}

			$payment_status = $res[$i]['payment_status'];
			$invoices =  Poppyz_Invoice::get_subscription_invoices( $res[$i]['id'] );
			$coupon = '';
			$subUrl = "";
			if(isset($_GET['pagination'])){
				$subUrl .= '&pagination='.(int)$_GET['pagination'];
			}

			if(isset($_GET['sort'])){
				$subUrl .='&sort='.(int)$_GET['sort'];
			}

			if(isset($_GET['query'])){
				$subUrl .='&query='.$_GET['query'];
			}
			if(isset($_GET['tier'])){
				$subUrl .= '&tier=' . (int) $_GET['tier'];
			}

			if ( $res[$i]['status'] == 'on' && ( $payment_status == 'paid' || $payment_status == 'admin' ) )  {
				$status = __( 'Active' );


				$actions = '<form class="btn-link-form" action="' . $current_url . $subUrl .'" method="post">
                        <input type="hidden" name="id" value="' .  $res[$i]['id']  . '" />
                        <button name="action" value="cancel" class="btn-link cancel" type="submit" title="' . __('Cancel','poppyz') .  '"><span class="dashicons dashicons-remove"></span></button>
                        </form>';
			} else {
				$status = __( 'Inactive' );
				if ( $payment_status == 'refund' ) {
					$status = __( 'Refunded' );
				}
				if ( $res[$i]['payments_made']== 1 && $res[$i]['payment_method'] == 'membership' ) {
					$status = __( 'Expired' );
				}

				$actions =
					'<form class="btn-link-form" action="' . $current_url .  $subUrl. '" method="post">
                        <input type="hidden" name="id" value="' .  $res[$i]['id']  . '" />
                        <button name="action" value="subscribe" class="btn-link subscribe" type="submit" title="' . __('Subscribe','poppyz') .  '"><span class="dashicons dashicons-admin-users"></span></button>
                        <button name="action" value="delete" class="btn-link delete" type="submit" title="' . __('Delete','poppyz') .  '"><span class="dashicons dashicons-trash"></span></button>
                    </form>';
			}

			$invoices_html = [];
			$invoice = [];

			if ( $res[$i]['discount_amount'] > 0 ) {
				$invoices = Poppyz_Invoice::get_invoices_by_subscription($res[$i]['id']);
				if ( isset( $invoices[0]->ID ) ) {
					$ppy_invoice = new Poppyz_Invoice();
					$coupon_code = $ppy_invoice->get_invoice_field( $invoices[0]->ID, 'coupon_code' );
					if ( $coupon_code ) {
						$coupon = '<br />' . __( 'Coupon used' ,'poppyz')  . ': ' . $coupon_code;;
					}
				}
			}

			if ( $invoices ) {
				foreach ( $invoices as $invoice ) {
					if ( isset( $invoice->invoice_id ) ) { //this is from the payments table and not from the custom post type invoice id
						$invoice_id = $invoice->invoice_id;
					} else {
						$invoice_id = $invoice->ID; //from post table
					}
					$ppy_payment = new Poppyz_Payment();
					$payment_data = $ppy_payment->get_payment_by_invoice( $invoice_id );


					$title = get_the_title( $invoice_id );
					if ( $payment_data ) {
						$is_chargedback = ( $payment_data->status == 'chargedback')  ? ' - ' . __( 'Chargeback' ) : '';
						$invoice_payments_made = $payment_data->number_of_payment;
						if ($payment_data->invoice_type == 'moneybird') {
							$title = $payment_data->invoice_id_other;
						}
					} else {
						$invoice_payments_made = get_post_meta( $invoice_id, PPY_PREFIX . 'invoice_payments_made', true );
						$is_chargedback = Poppyz_Invoice::has_credit_invoice( $res[$i]['id'], $invoice_payments_made ) ? ' - ' . __( 'Chargeback' ,'poppyz') : '';
					}
					$invoices_html[$invoice_payments_made] = '<a href="' . site_url('?process_invoice=' .  $invoice_id ) . '">' . $title . '</a>' . $is_chargedback;
				}
			}

			$subscription_date = $res[$i]['subscription_date'];
			$plan_status = '';

			if ( $res[$i]['payment_method'] == 'plan' ) {
				$payment_method =  __( 'Payment Plan' ,'poppyz');
				$payments_made = $res[$i]['payments_made'];
				$payment_type =$res[$i]['payments_made'];

				$payments_left = $times - $payments_made;

				//if no payment has been made yet, the allowance is 14 days
				if ( $payments_made == 0 ) {
					$days = 14;
				}

				if ( $res[$i]['payment_status'] != 'admin' ) { //this subscription was not manually created by the admin
					for ( $y=0; $y < $times; $y++ ) {
						$frequency = $interval * $y ;
						$due_date =  strtotime( $subscription_date . ' + ' . $frequency . ' ' . $timeframe );
						$due_date_string =  date_i18n( get_option( 'date_format' ), $due_date );

						$initial_interval = get_post_meta($res[$i]['tier_id'], PPY_PREFIX . 'initial_payment_frequency', true);
						if ( $y > 0 && $initial_interval ) {
							$initial_payment_timeframe = get_post_meta($res[$i]['tier_id'], PPY_PREFIX . 'initial_payment_timeframe', true);
							$due_date =  strtotime( $res[$i]['subscription_date'] . ' + ' . $initial_interval . ' ' . $initial_payment_timeframe);
							$due_date_string =  date_i18n( get_option( 'date_format' ), $due_date );
							if ( $y > 1 ) {
								$due_date =  strtotime(  ' + ' .  ( $frequency - 1 ) . ' ' . $initial_payment_timeframe, $due_date);
								$due_date_string =  date_i18n( get_option( 'date_format' ), $due_date );
							}
						}

						$payment_data = null;
						if ( $res[$i]['version'] == 2 ) {
							$payment_data = $ppy_payment->get_payment_by_number( $res[$i]['id'], $y+1 );
							$payment_needed = $payment_data ? $payment_data->status != 'paid' && $payment_data->status != 'chargeback-paid' : true;
							$x = true;
						} else {
							$payment_needed =  $y == $payments_made;
							$x = $y <= $payments_made - 1;
						}

						if (  $x && !$payment_needed  ) {
							$plan_status  .= ' <br />' . $due_date_string . ' - <strong class="green">' . __( 'Paid','poppyz' ) . '</strong>';
							$plan_status .= ( $invoices && isset( $invoices_html[$y+1] ) ) ? ' - ' . $invoices_html[$y+1] : ' - ' . __( 'None' ,'poppyz');
							//$plan_status .= ' : <form class="btn-link-form" action="' . $current_url . '" method="post"><input type="hidden" name="id" value="' .  $s->id  . '" /><input type="hidden" name="action" value="refund" /><button class="btn-link confirm-refund" type="submit">' . __('Confirm Refund') . '</button></form>';
						} else {
							$plan_status  .=  ' <br/> ' . $due_date_string . ' - ' . __( 'Remaining payment','poppyz' );
						}
					}
					if ($payments_made > 0) $actions .= ' <a class="remind btn-link" href="' . $res[$i]['id'] . '"><span class="dashicons dashicons-email-alt send-reminder-icon"></span></a>';
				} else {
					$plan_status .=  ' - ' . __( 'Subscribed by the admin' );
				}

				/*if ( $payments_left > 0 ) {
					$due_date = strtotime( $subscription_date . ' + ' . $payments_made * $interval  . ' ' . $timeframe );
					$due_date_string =  date_i18n( get_option( 'date_format' ), $due_date );



					$plan_status .= ( $due_date < time() ) ?  '<br /><strong class="overdue error-message">' . __( 'Overdue' ) . '</strong>' : '';

					if ( $payments_made == 0 ) {
						$plan_status .=  ' - ' . __( 'Unpaid' );
					} else {
						$plan_status .= ' - ' . $payments_made . '/' . $times;
					}

					$plan_status .= ' <br /> ' . __( 'Payment due on: ' ) . ' ' . $due_date_string;

					$actions .= ' / <a class="remind" href="' . $s->id . '">' . __( 'Send Reminder' ) . '</a>';

				} else {
					$plan_status .= ': <strong>' . __( 'Fully Paid' ) . '</strong>' ;
				}*/

			} else if ( $payment_method == 'membership'  ) {
				$payments_made = $res[$i]['payments_made'];
				$due_date =  date_i18n( get_option( 'date_format' ), strtotime( $subscription_date . ' + ' . $payments_made * $interval  . ' ' . $timeframe ));
				//$plan_status = ( strtotime( $due_date ) < time() ) ?  '<br /><strong class="overdue error-message">' . __( 'Overdue' ) . '</strong>' : '';
				$plan_status = ' <br />';
				$plan_status .= __('Payments' ) . ': ' . $payments_made;
				$plan_status .= ' <br /> ' . __( 'Payment due on: ' ,'poppyz') .  $due_date . '<br />';
				$invoices_html = '';
				$invoices =  Poppyz_Invoice::get_subscription_invoices(  $res[$i]['id'] );
				if ( $invoices ) {
					foreach ( $invoices as $invoice ) {
						if (isset($invoice->invoice_id)) { //this is from the payments table and not from the custom post type invoice id
							$invoice_id = $invoice->invoice_id;
						} else {
							$invoice_id = $invoice->ID;
						}
						$payment_data = $ppy_payment->get_payment_by_invoice( $invoice_id );
						if ( $payment_data ) {
							if ($payment_data->invoice_type == 'moneybird') {
								$title = $payment_data->invoice_id_other;
							}
						}
						$invoices_html .= ' <a href="' . site_url('?process_invoice=' .  $invoice_id ) . '">' . get_the_title( $invoice_id ) . '</a>, ';
					}
				}

				if ( $res[$i]['payment_status'] != 'admin' ) {
					$plan_status .= rtrim( $invoices_html  , ', ' );
                    if ($payments_made > 0) $actions .= ' <a class="remind btn-link" href="' . $res[$i]['id'] . '"><span class="dashicons dashicons-email-alt send-reminder-icon"></span></a>';
				} else {
					$plan_status =  ' - ' . __( 'Subscribed by the admin' ,'poppyz');
				}

			} else {
				$payment_type_description = '';
				$payment_method =  __( 'Standard','poppyz','poppyz' );
				//$plan_status .= '<br />' . date_i18n( get_option( 'date_format' ), strtotime( $subscription_date ) );

				if ( $res[$i]['payment_status'] != 'admin' ) {
					$plan_status .=  ($res[$i]['payment_status'] == 'paid' ) ? ' <strong>' . __( 'Paid' ,'poppyz') . '</strong><br/>' : '';
					$plan_status .=  reset($invoices_html);
				} else {
					$plan_status .=  __( 'Subscribed by the admin','poppyz' );
				}
			}
			if ( $payments_made > 0 ) { //only show refund link when at least 1 payment has been done
				//$actions .= ' /<form class="btn-link-form" action="' . $current_url . '" method="post"><input type="hidden" name="id" value="' .  $s->id  . '" /><input type="hidden" name="action" value="refund" /><button class="btn-link confirm-refund" type="submit" />' . __('Confirm Refund') . '</form>';

			}

			$order_id = $res[$i]['order_id'] ? 'Order ID: ' . $res[$i]['order_id'] : '';
			$payment_id = $res[$i]['payment_id'] ? ', Payment ID: ' . $res[$i]['payment_id'] : '';

			$student_data[] = array(
				"user_id" => $res[$i]['user_id'],
				"student_info" => get_userdata($res[$i]['user_id']),
				"student_profile" => get_edit_user_link($res[$i]['user_id']),
				"fullname" => $fullname,
				"course_link" => get_edit_post_link( $res[$i]['course_id']),
				"tier_link" => get_edit_post_link($res[$i]['tier_id']),
				"course"=>  isset(get_post( $res[$i]['course_id'])->post_title) ? get_post( $res[$i]['course_id'])->post_title : "",
				"tier"=> isset(get_post( $res[$i]['tier_id'])->post_title) ? get_post( $res[$i]['tier_id'])->post_title : "",
				"subscription" => array(
					"subscription_id" => $res[$i]['id'],
					"sub_status" =>$res[$i]['status'],
					"tier_id" =>$res[$i]['tier_id'],
					"course_id" =>$res[$i]['course_id'],
					"payment_status" => $res[$i]['payment_status'],
					"payment_type" =>$res[$i]['payment_type'],
					"payments_made" => $res[$i]['payments_made'],
					"payment_type_description" =>$payment_type_description,
					"subscription_date" =>  $subscription_date,
					"plan_status" => $plan_status,
					"status" => $status,
					"coupon" =>$coupon,
					"actions" => $actions
				),

			);

		}

		return $student_data; //wp_list_sort($student_data, $field,$sort);
	}


	public static function students_count( $filter = array(), $searchStudent = null ) {
		global $wpdb;

		$subs_table = $wpdb->prefix . 'course_subscriptions';

		$where = "";

		foreach ( $filter as $k => $v ) {
			if ( $k == 'subscription_date' ) {
				$where .= " AND $k LIKE '$v%'"; //since we don't need to know the exact time
			} else {
				$where .= " AND $k = $v";
			}
		}

        if ( $searchStudent != null ) {

			$searchStudentArgs = array(
				'search'         => '*'.esc_attr( $searchStudent ).'*',
			);
			$user_query = new WP_User_Query( $searchStudentArgs );
			$searchUserResultId = [];
			foreach ( $user_query->get_results() as $user ) {
				$searchUserResultId[] = $user->ID;
			}

			$res = $wpdb->get_results(
				"
                SELECT *
                FROM $subs_table u
                WHERE 1 = 1
                $where
                ORDER BY subscription_date
                DESC
            "
			);
            $userArray = [];
            foreach ( $res as $user ) {
                if ( in_array($user->user_id, $searchUserResultId ) && !in_array($user->user_id, $userArray) ) {
					$userArray[] = $user->user_id;
                }
            }
            return count($userArray);
        } else {
			$res = $wpdb->get_var(
				"
                SELECT COUNT(*) 
                FROM $subs_table u
                WHERE 1 = 1
                $where
                ORDER BY subscription_date
                DESC
            "
			);
			return $res;
        }



	}
	public static function export_students( $students ) {

		if ( !$students ) return false;

		header('Content-Type: text/csv; charset=ISO-8859-15');
		header('Content-Disposition: attachment; filename=data.csv');

		$fp = fopen('php://output', 'w');
		ob_clean(); // clean slate

		global $ppy_lang;

		$columns = array(
			__( 'First Name' ,'poppyz'),
			__( 'Last Name' ,'poppyz'),
			__( 'Email' ,'poppyz'),
			__( 'Phone' ,'poppyz'),
			__( 'Company' ,'poppyz'),
			__( 'Address' ,'poppyz'),
			__( 'Zipcode' ,'poppyz'),
			__( 'City' ,'poppyz'),
			__( 'Country' ,'poppyz'),
			__( 'Course Name' ,'poppyz'),
			__( 'Tier' ,'poppyz'),
			__( 'Subscription Date' ,'poppyz'),
			__( 'Status' ,'poppyz'),
		);

		fputcsv($fp, $columns);

		for ($i = 0; $i<count($students); $i++ ) {
			$user = get_user_by( 'id', $students[$i]['user_id'] );

			if ( $students[$i]['subscription']['status'] == 'on' && $students[$i]['subscription']['payment_status'] == 'paid' )  {
				$status = __( 'Subscribed' ,'poppyz');
			} elseif (  $students[$i]['subscription']['payment_status'] == 'admin' ) {
				$status = __( 'Subscribed' ,'poppyz');
			} else {
				$status = __( 'Pending' ,'poppyz');
			}

			$name = $email = $first_name = $last_name = __( 'User Deleted' ,'poppyz');
			if ( $user ) {
				$first_name = $user->first_name;
				$last_name =  $user->last_name;
				$name = $user->first_name . ' ' . $user->last_name . ' (' . $user->user_login  .')';
				$email = $user->user_email;
				require_once (PPY_DIR_PATH . 'public/class-poppyz-user.php');
				$custom_fields = Poppyz_User::custom_fields();
				foreach ( $custom_fields as $name => $value) {
					$$value = get_user_meta( $user->ID, PPY_PREFIX . $value, true );
				}
				$fields = array(
					$first_name,
					$last_name,
					$email,
					$phone,
					$company,
					$address,
					$zipcode,
					$city,
					$country,
					get_the_title( $students[$i]['subscription']['course_id'] ),
					get_the_title( $students[$i]['subscription']['tier_id'] ),
					date( get_option( 'date_format' ), strtotime( $students[$i]['subscription']['subscription_date']) ),
					$status,
				);
				fputcsv($fp, $fields);
			}

		}

		ob_flush(); // dump buffer

		fclose($fp);
		die();
	}

	public static function subscribe_newsletter_old( $user_id, $id ) {

		require_once( PPY_DIR_PATH . 'includes/class-poppyz-newsletter.php' );
		$ppy_nl = new Poppyz_Newsletter();

		$ppy_nl->mc_subscribe( $user_id, $id );

		$ppy_nl->ml_subscribe( $user_id, $id );

		if ( $ppy_nl->aw_initialize() ) {
			$ppy_nl->aw_subscribe( $user_id, $id );
		}

		$ppy_nl->av_subscribe( $user_id, $id );

		$ppy_nl->la_subscribe( $user_id, $id );

	}

	public static function subscribe_newsletter($user_id, $tier_id ) {
		\Poppyz\Includes\Newsletters\Newsletter::subscribeAll($user_id, $tier_id );
	}


	public static function save_formatted_price( $price ) {
		//before saving the price, if the currency is in euro then convert to US format before saving to the database
		$currency = Poppyz_Core::get_option( 'currency' );
		//remove consecutive commas
		$price = preg_replace('/,+/', ',', $price);
		if ( $currency == 'usd' ) {
			$price = str_replace( ',', '', $price ); //remove commas
			$price = str_replace( ',00', ',-', $price ); //if last decimals are 00, remove and replace with ,-.
			$price = str_replace( ',-', '', $price );
			$price = str_replace( '.-', '', $price );
		} else {
			$price = str_replace( '.', '', $price ); // remove fullstop
			$price = str_replace( ' ', '', $price ); // remove spaces
			$price = str_replace( ',', '.', $price ); // change comma to fullstop
			$price = str_replace( ',00', ',-', $price ); //if last decimals are 00, remove and replace with ,-.
			$price = str_replace( ',-', '', $price );
			$price = str_replace( '.-', '', $price );
		}
		$price = preg_replace("/[^0-9.]/", "", $price); //only allow numbers and decimal point
		return $price;
	}

	public static function protect_attachments( $remove = false ) {

		// Create a directory in the uploads folder to upload the attachments in our courses and lessons.
		$upload_dir = wp_upload_dir();
		$upload_path = $upload_dir['basedir'] . '/poppyz/';

		if( !file_exists( $upload_path ) ) {
			if ( !wp_mkdir_p( $upload_path ) ) {
				$options_saved = get_option( 'ppy_settings' );
				$options_saved['permission_failed'] = 1;
				update_option( 'ppy_settings' , $options_saved );
				return false;
			}
		}

		//add a htaccess file to protect files from direct access
		if ( file_exists( $upload_path ) ) {

			//add index.php to disable directory listing
			file_put_contents($upload_path . "/index.php", "");

			$htaccess_start = '# BEGIN Poppyz File Protection';
			$htaccess_end = '# END Poppyz File Protection';

			$htaccess_file =  $upload_path . '/.htaccess';

			if( !file_exists( $htaccess_file ) )
				file_put_contents($htaccess_file, $htaccess_start . $htaccess_end );

			// read it
			$htaccess = (string) @file_get_contents($htaccess_file);

			// remove it
			list($start) = explode($htaccess_start, $htaccess);
			list($x, $end) = explode($htaccess_end, $htaccess);

			$htaccess = trim($start) . "\n" . trim($end);

			if ( ! $remove ) {
				$ignorelist = array( 'jpg', 'jpeg', 'png', 'gif', 'bmp', 'css','js' );

				foreach ($ignorelist AS $i => $ext) {
					$ext = preg_replace('/[^A-Za-z0-9]/', '', trim($ext));
					$ignorelist[$i] = $ext;
				}
				$ignorelist = implode('|', $ignorelist);

				// add it
				$siteurl = parse_url( get_option('home') );
				$siteurl['path'] = isset( $siteurl['path']  ) ? $siteurl['path'] : '';
				$siteurl = $siteurl['path'] . '/index.php';
				$htaccess.="\n{$htaccess_start}\nOptions -Indexes\nRewriteEngine on\nRewriteCond %{REQUEST_URI} !\.({$ignorelist})\$\nRewriteRule ^attachments/(.*)\$ {$siteurl}?smcfile=\$1 [L]\n{$htaccess_end}";
			}

			// write it
			$f = fopen($htaccess_file, 'w');
			fwrite($f, trim($htaccess));
			fclose($f);

			return true;
		}

		return false;
	}

	public static function read_file( $file ) {

		$upload     = wp_upload_dir();
		$the_file   = $file;
		$file       = $upload[ 'basedir' ] . '/poppyz/attachments/' . $file;

		//this function only gets called on the images requested that are from uploads/poppyz/attachments,
		// which is used to save media uploaded from courses and lessons. So make this require login.
		if ( !is_user_logged_in() ) {
			wp_redirect( wp_login_url( $upload[ 'baseurl' ] . '/poppyz/attachments/' . $the_file) );
			die();
		}

		if ( !is_file( $file ) ) {
			status_header( 404 );
			die( '404 &#8212; File not found.' );
		}
		else {

			$image = get_posts( array( 'post_type' => 'attachment', 'meta_query' => array( array( 'key' => '_wp_attached_file', 'value' => 'poppyz/attachments/' . $the_file ) ) ) );

			if ( 0 < count( $image ) && 0 < $image[0] -> post_parent ) { // attachment found and parent available
				if ( post_password_required( $image[0] -> post_parent ) ) { // password for the post is not available
					wp_die( get_the_password_form() );// show the password form
				}

				$parent_id = $image[0]->post_parent;
				$ppy_subs = new Poppyz_Subscription();
				$access = $ppy_subs->is_subscribed( $parent_id );

				if ( is_wp_error ( $access ) ) {
					die( $access->get_error_message() );
				}
			}
			else {

				//this file does not belong to any course and lesson, so do not allow access to all except admin
				if ( !current_user_can( 'manage_options' ) ) {
					wp_die( 'This file is not attached to any course or lesson so access is forbidden.' );
				}

				// not a normal attachment check for thumbnail
				$filename   = pathinfo( $the_file );

				$images     = get_posts( array( 'post_type' => 'attachment', 'meta_query' => array( array( 'key' => '_wp_attachment_metadata', 'compare' => 'LIKE', 'value' => $filename[ 'filename' ] . '.' . $filename[ 'extension' ] ) ) ) );
				if ( 0 < count( $images ) ) {
					foreach ( $images as $SINGLEimage ) {
						$meta = wp_get_attachment_metadata( $SINGLEimage -> ID );
						if ( 0 < count( $meta[ 'sizes' ] ) ) {
							$filepath   = pathinfo( $meta[ 'file' ] );
							if ( $filepath[ 'dirname' ] == $filename[ 'dirname' ] ) {// current path of the thumbnail
								foreach ( $meta[ 'sizes' ] as $SINGLEsize ) {
									if ( $filename[ 'filename' ] . '.' . $filename[ 'extension' ] == $SINGLEsize[ 'file' ] ) {
										if ( post_password_required( $SINGLEimage -> post_parent ) ) { // password for the post is not available
											wp_die( get_the_password_form() );// show the password form
										}

									}
								}
							}
						}
					}
				}
			}
		}
		$mime = wp_check_filetype($file);
		if( false === $mime[ 'type' ] && function_exists( 'mime_content_type' ) )
			$mime[ 'type' ] = mime_content_type( $file );
		if( $mime[ 'type' ] )
			$mimetype = $mime[ 'type' ];
		else
			$mimetype = 'image/' . substr( $file, strrpos( $file, '.' ) + 1 );
		header( 'Content-Type: ' . $mimetype ); // always send this

		if ( false === strpos( $_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS' ) )
			header( 'Content-Length: ' . filesize( $file ) );
		if ( function_exists( 'apache_get_modules' ) && in_array( 'mod_xsendfile', apache_get_modules() ) ) {
			header("X-Sendfile: $file");
		} elseif ( stristr( getenv( 'SERVER_SOFTWARE' ), 'lighttpd' ) ) {
			header( "X-LIGHTTPD-send-file: $file" );
		} elseif (  stristr( getenv( 'SERVER_SOFTWARE' ), 'nginx' ) || stristr( getenv( 'SERVER_SOFTWARE' ), 'cherokee' ) ) {
			$file = str_ireplace( $_SERVER['DOCUMENT_ROOT'], '', $file );
			header( "X-Accel-Redirect: /$file" );
		}

		$last_modified = gmdate( 'D, d M Y H:i:s', filemtime( $file ) );
		$etag = '"' . md5( $last_modified ) . '"';
		header( "Last-Modified: $last_modified GMT" );
		header( 'ETag: ' . $etag );
		header( 'Expires: ' . gmdate( 'D, d M Y H:i:s', time() + 100000000 ) . ' GMT' );
		// Support for Conditional GET
		$client_etag = isset( $_SERVER['HTTP_IF_NONE_MATCH'] ) ? stripslashes( $_SERVER['HTTP_IF_NONE_MATCH'] ) : false;
		if( ! isset( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) )
			$_SERVER['HTTP_IF_MODIFIED_SINCE'] = false;
		$client_last_modified = trim( $_SERVER['HTTP_IF_MODIFIED_SINCE'] );
		// If string is empty, return 0. If not, attempt to parse into a timestamp
		$client_modified_timestamp = $client_last_modified ? strtotime( $client_last_modified ) : 0;
		// Make a timestamp for our most recent modification...
		$modified_timestamp = strtotime($last_modified);
		if ( ( $client_last_modified && $client_etag )
			? ( ( $client_modified_timestamp >= $modified_timestamp) && ( $client_etag == $etag ) )
			: ( ( $client_modified_timestamp >= $modified_timestamp) || ( $client_etag == $etag ) )
		) {
			status_header( 304 );
			exit;
		}
		$page = ob_get_contents();
		ob_end_clean();
		// If we made it this far, just serve the file
		readfile( $file );
	}

	public static function send_notification_email( $subs_id, $user_id, $data, $type ) {
		$options = get_option( 'ppy_settings' );
		$prefix =  $type . '_';
		$message = $options[$prefix . 'email_message'];
		global $ppy_lang;
		if ( isset( $data['email_content'] ) ) {
			$message = $data['email_content'];
		}

		if ( isset($options[$prefix . 'email_disabled']) && $options[$prefix . 'email_disabled'] == 'on' ) {
			return false;
		}

		if ( !empty( $message ) ) {


			$account_link =  get_permalink( Poppyz_Core::get_option( 'account_page' ) );

			$ppy_sub = new Poppyz_Subscription();
			$sub = $ppy_sub->get_subscription_by_id( $subs_id );
			$tier_link = (isset($sub->course_id)) ? get_permalink( $sub->course_id ) : "";
			$tier_id = (isset($sub->tier_id)) ?  $sub->tier_id : "";
			$tier_name = ($tier_id) ? get_the_title( $tier_id ) : "";
			$course_name = (isset($sub->course_id)) ? get_the_title( $sub->course_id ) : "";
			$course_link = (isset($sub->course_id)) ? get_permalink( $sub->course_id) : "";
			$payments_made =  (isset($sub->payments_made)) ? $sub->payments_made ?: 1 : "";
			$invoice_id = Poppyz_Invoice::get_invoice_by_subscription( $subs_id, $payments_made );
			$invoice_link = ( $invoice_id ) ? Poppyz_Invoice::get_invoice_url( $invoice_id ) . '&download' : '';
			$user_info = get_userdata( $user_id );
			if($user_id > 0 && !empty($user_info))
			{
				$user_info = get_userdata( $user_id );
                $email = $user_info->user_email;
                $amount = (isset($sub->amount)) ? $sub->amount : "";
                $customer_id = (isset($sub->mollie_customer_id)) ? $sub->mollie_customer_id : "";
                $rp_link = '';
                //generate password link if the shortcode [password-link] has been used
                if ( strpos($message, '[password-link]') !== false ) {
                    $adt_rp_key = get_password_reset_key( $user_info );
                    $user_login = $user_info->user_login;
                    if ( !is_wp_error( $adt_rp_key ) ) {
                        $password_link_text = ( Poppyz_Core::get_option( 'password_link_text') ) ? Poppyz_Core::get_option( 'password_link_text') : network_site_url("wp-login.php?action=rp&key=$adt_rp_key&login=" . rawurlencode($user_login), 'login');
                        $rp_link = '<a href="' . network_site_url("wp-login.php?action=rp&key=$adt_rp_key&login=" . rawurlencode($user_login), 'login') . '">' . $password_link_text . '</a>';
                    }
                }

                //automatically add invoice link if the shortcode has not been used.
                /*if ( ($type == 'ty' || $type == 'invoice') &&  strpos( $message, '[invoice-link]') === false ) {
                    $message = $message . '<br /><br /><br />' . __( 'Invoice' ,'poppyz') . ': [invoice-link]';
                }*/

                if ( isset($sub->amount) && isset($sub->payments_made) && Poppyz_Core::get_option( 'mo_api_key' ) && $sub->amount > 0 && $sub->payments_made > 0 && !empty( $sub->payment_id ) ) {
                    $ppy_payment = new Poppyz_Payment();
                    if(!Poppyz_Core::column_exists( 'payment_mode' )) {
                        $ppy_payment->_initialize_mollie();
                    } else {
                        $payment_mode = (!empty($sub->payment_mode)) ? $sub->payment_mode : "";
                        $ppy_payment->_initialize_mollie( $payment_mode );
                    }
                    try {
                        $payment = $ppy_payment->mollie->payments->get( $sub->payment_id );
                        $amount = Poppyz_Core::format_price( $payment->amount->value );
                        $customer_id = $payment->customerId;
                    } catch (Exception $e)  {
                        error_log( "Error: " . htmlspecialchars($e->getMessage()) );
                    }
                }
                $due_date = isset($data['due_date']) ? $data['due_date'] : '';
                $is_for_admin = !empty($data['admin']) ? true : false;
                $tier_purchase_url = Poppyz_Core::get_tier_purchase_url($tier_id);
                $search = array( '[due-date]', '[account-link]', '[payment-link]', '[invoice-link]', '[tier-link]', '[tier]', '[course]', '[course-link]', '[first-name]', '[last-name]', '[user-email]','[customer-id]', '[amount]', '[password-link]', '[tier-purchase-url]' );
                $replace = array( $due_date, $account_link, $account_link, $invoice_link, $tier_link, $tier_name, $course_name, $course_link, $user_info->first_name, $user_info->last_name, $user_info->user_email, $customer_id, $amount, $rp_link, $tier_purchase_url );
                $message = str_replace( $search, $replace, $message );

                global $ppy_lang;

                if ( isset( $data['email_subject'] ) ) {
                    $subject = $data['email_subject'];
                } else {
                    $subject = ( isset( $options[$prefix . 'email_subject'] ) ) ? $options[$prefix . 'email_subject'] : '';
                }

                if ( empty( $subject ) ) {
                    if ( $type == 'reminder' ) {
                        $subject = __( 'Payment Reminder' ,'poppyz');
                    } elseif ( $type == 'overdue' ) {
                        $subject = __( 'Payment Overdue' ,'poppyz');
                    }  elseif ( $type == 'failed_payment' || $type == 'failed_payment_admin' ) {
                        $subject = __( 'Important: SEPA direct debit collection failed.' ,'poppyz');
                    }  elseif ( $type == 'pending' ) {
                        $subject = __( 'Did something go wrong during your purchase?' ,'poppyz');
                    }  elseif ( $type == 'scheduled_donation_notification_email' ) {
                        $subject = $data['subject'];
                    }
                }
                $subject = str_replace( $search, $replace, $subject );

                if ( $is_for_admin ) {
                    $email = get_option('admin_email');
                }

				if ( $is_for_admin && !empty($data['donation']) ) {
                    $donation_message = sprintf(__('This user also donated %s with this purchase.', 'poppyz'), Poppyz_Core::format_price( $data['donation'] ));
                    $message .= "\r\n\r\n".$donation_message;
				}

                $headers = "";
                if ( isset( $options[$prefix . 'email_from'] ) && !empty( $options[$prefix . 'email_from'] ) ) {
                    $headers = 'From: ' . $options[$prefix . 'email_from_name'] . ' <' . $options[$prefix . 'email_from']  . '>' . "\r\n";
                }

                $message = Poppyz_Core::the_content( $message );
                //error_log('email_sent!' . $message );
                add_filter( 'wp_mail_content_type', function( $content_type ) {
                    return 'text/html';
                });

                wp_mail( $email, $subject, $message, $headers );

                return $message;
			}
		}
	}

	public static function setup_payment_reminders() {
		$timestamp = wp_next_scheduled( 'ppy_send_payment_reminders' );

		//If $timestamp == false setup payment reminders since it hasn't been done previously
		if( $timestamp == false ){
			//Schedule the event for right now, then to repeat daily using the hook 'wi_create_daily_backup'
			wp_schedule_event( time(), 'twicedaily', 'ppy_send_payment_reminders' );
		}
	}

	public static function get_tier_purchase_url( $id ) {
		$purchase_url =  get_permalink( Poppyz_Core::get_option( 'purchase_page' ) );
		if (  get_option( 'permalink_structure' ) ) {
			$purchase_url .= 'order-id/';
		} else {
			$purchase_url .= '&order-id=';
		}
		$purchase_url .= get_post_meta( $id, PPY_PREFIX . 'tier_slug', true );

		return $purchase_url;
	}

	public function send_payment_reminders() {
		$ppy_sub = new Poppyz_Subscription();
		$ppy_sub->notify_overdue_subscriptions();
		$ppy_sub->update_license();
		Poppyz_Invoice::rename_duplicate_titles();
		$this->sendEmailDonation();
	}

	public static function countries() {
		global $ppy_lang;
		return array(
			"GB" => "United Kingdom",
			"US" => "United States",
			"AF" => "Afghanistan",
			"AL" => "Albania",
			"DZ" => "Algeria",
			"AS" => "American Samoa",
			"AD" => "Andorra",
			"AO" => "Angola",
			"AI" => "Anguilla",
			"AQ" => "Antarctica",
			"AG" => "Antigua And Barbuda",
			"AR" => "Argentina",
			"AM" => "Armenia",
			"AW" => "Aruba",
			"AU" => "Australia",
			"AT" => "Austria",
			"AZ" => "Azerbaijan",
			"BS" => "Bahamas",
			"BH" => "Bahrain",
			"BD" => "Bangladesh",
			"BB" => "Barbados",
			"BY" => "Belarus",
			"BE" => __("Belgium",'poppyz'),
			"BZ" => "Belize",
			"BJ" => "Benin",
			"BM" => "Bermuda",
			"BT" => "Bhutan",
			"BO" => "Bolivia",
			"BA" => "Bosnia And Herzegowina",
			"BW" => "Botswana",
			"BV" => "Bouvet Island",
			"BR" => "Brazil",
			"IO" => "British Indian Ocean Territory",
			"BN" => "Brunei Darussalam",
			"BG" => "Bulgaria",
			"BF" => "Burkina Faso",
			"BI" => "Burundi",
			"KH" => "Cambodia",
			"CM" => "Cameroon",
			"CA" => "Canada",
			"CV" => "Cape Verde",
			"KY" => "Cayman Islands",
			"CF" => "Central African Republic",
			"TD" => "Chad",
			"CL" => "Chile",
			"CN" => "China",
			"CX" => "Christmas Island",
			"CC" => "Cocos (Keeling) Islands",
			"CO" => "Colombia",
			"KM" => "Comoros",
			"CG" => "Congo",
			"CD" => "Congo, The Democratic Republic Of The",
			"CK" => "Cook Islands",
			"CR" => "Costa Rica",
			"CI" => "Cote D'Ivoire",
			"HR" => "Croatia (Local Name: Hrvatska)",
			"CU" => "Cuba",
			"CY" => "Cyprus",
			"CZ" => "Czech Republic",
			"DK" => "Denmark",
			"DJ" => "Djibouti",
			"DM" => "Dominica",
			"DO" => "Dominican Republic",
			"TP" => "East Timor",
			"EC" => "Ecuador",
			"EG" => "Egypt",
			"SV" => "El Salvador",
			"GQ" => "Equatorial Guinea",
			"ER" => "Eritrea",
			"EE" => "Estonia",
			"ET" => "Ethiopia",
			"FK" => "Falkland Islands (Malvinas)",
			"FO" => "Faroe Islands",
			"FJ" => "Fiji",
			"FI" => "Finland",
			"FR" => "France",
			"FX" => "France, Metropolitan",
			"GF" => "French Guiana",
			"PF" => "French Polynesia",
			"TF" => "French Southern Territories",
			"GA" => "Gabon",
			"GM" => "Gambia",
			"GE" => "Georgia",
			"DE" => "Germany",
			"GH" => "Ghana",
			"GI" => "Gibraltar",
			"GR" => "Greece",
			"GL" => "Greenland",
			"GD" => "Grenada",
			"GP" => "Guadeloupe",
			"GU" => "Guam",
			"GT" => "Guatemala",
			"GN" => "Guinea",
			"GW" => "Guinea-Bissau",
			"GY" => "Guyana",
			"HT" => "Haiti",
			"HM" => "Heard And Mc Donald Islands",
			"VA" => "Holy See (Vatican City State)",
			"HN" => "Honduras",
			"HK" => "Hong Kong",
			"HU" => "Hungary",
			"IS" => "Iceland",
			"IN" => "India",
			"ID" => "Indonesia",
			"IR" => "Iran (Islamic Republic Of)",
			"IQ" => "Iraq",
			"IE" => "Ireland",
			"IL" => "Israel",
			"IT" => "Italy",
			"JM" => "Jamaica",
			"JP" => "Japan",
			"JO" => "Jordan",
			"KZ" => "Kazakhstan",
			"KE" => "Kenya",
			"KI" => "Kiribati",
			"KP" => "Korea, Democratic People's Republic Of",
			"KR" => "Korea, Republic Of",
			"KW" => "Kuwait",
			"KG" => "Kyrgyzstan",
			"LA" => "Lao People's Democratic Republic",
			"LV" => "Latvia",
			"LB" => "Lebanon",
			"LS" => "Lesotho",
			"LR" => "Liberia",
			"LY" => "Libyan Arab Jamahiriya",
			"LI" => "Liechtenstein",
			"LT" => "Lithuania",
			"LU" => "Luxembourg",
			"MO" => "Macau",
			"MK" => "Macedonia, Former Yugoslav Republic Of",
			"MG" => "Madagascar",
			"MW" => "Malawi",
			"MY" => "Malaysia",
			"MV" => "Maldives",
			"ML" => "Mali",
			"MT" => "Malta",
			"MH" => "Marshall Islands",
			"MQ" => "Martinique",
			"MR" => "Mauritania",
			"MU" => "Mauritius",
			"YT" => "Mayotte",
			"MX" => "Mexico",
			"FM" => "Micronesia, Federated States Of",
			"MD" => "Moldova, Republic Of",
			"MC" => "Monaco",
			"MN" => "Mongolia",
			"MS" => "Montserrat",
			"MA" => "Morocco",
			"MZ" => "Mozambique",
			"MM" => "Myanmar",
			"NA" => "Namibia",
			"NR" => "Nauru",
			"NP" => "Nepal",
			"NL" => __("Netherlands",'poppyz'),
			"AN" => "Netherlands Antilles",
			"NC" => "New Caledonia",
			"NZ" => "New Zealand",
			"NI" => "Nicaragua",
			"NE" => "Niger",
			"NG" => "Nigeria",
			"NU" => "Niue",
			"NF" => "Norfolk Island",
			"MP" => "Northern Mariana Islands",
			"NO" => "Norway",
			"OM" => "Oman",
			"PK" => "Pakistan",
			"PW" => "Palau",
			"PA" => "Panama",
			"PG" => "Papua New Guinea",
			"PY" => "Paraguay",
			"PE" => "Peru",
			"PH" => "Philippines",
			"PN" => "Pitcairn",
			"PL" => "Poland",
			"PT" => "Portugal",
			"PR" => "Puerto Rico",
			"QA" => "Qatar",
			"RE" => "Reunion",
			"RO" => "Romania",
			"RU" => "Russian Federation",
			"RW" => "Rwanda",
			"KN" => "Saint Kitts And Nevis",
			"LC" => "Saint Lucia",
			"VC" => "Saint Vincent And The Grenadines",
			"WS" => "Samoa",
			"SM" => "San Marino",
			"ST" => "Sao Tome And Principe",
			"SA" => "Saudi Arabia",
			"SN" => "Senegal",
			"SC" => "Seychelles",
			"SL" => "Sierra Leone",
			"SG" => "Singapore",
			"SK" => "Slovakia (Slovak Republic)",
			"SI" => "Slovenia",
			"SB" => "Solomon Islands",
			"SO" => "Somalia",
			"ZA" => "South Africa",
			"GS" => "South Georgia, South Sandwich Islands",
			"ES" => "Spain",
			"LK" => "Sri Lanka",
			"SH" => "St. Helena",
			"PM" => "St. Pierre And Miquelon",
			"SD" => "Sudan",
			"SR" => "Suriname",
			"SJ" => "Svalbard And Jan Mayen Islands",
			"SZ" => "Swaziland",
			"SE" => "Sweden",
			"CH" => "Switzerland",
			"SY" => "Syrian Arab Republic",
			"TW" => "Taiwan",
			"TJ" => "Tajikistan",
			"TZ" => "Tanzania, United Republic Of",
			"TH" => "Thailand",
			"TG" => "Togo",
			"TK" => "Tokelau",
			"TO" => "Tonga",
			"TT" => "Trinidad And Tobago",
			"TN" => "Tunisia",
			"TR" => "Turkey",
			"TM" => "Turkmenistan",
			"TC" => "Turks And Caicos Islands",
			"TV" => "Tuvalu",
			"UG" => "Uganda",
			"UA" => "Ukraine",
			"AE" => "United Arab Emirates",
			"UM" => "United States Minor Outlying Islands",
			"UY" => "Uruguay",
			"UZ" => "Uzbekistan",
			"VU" => "Vanuatu",
			"VE" => "Venezuela",
			"VN" => "Viet Nam",
			"VG" => "Virgin Islands (British)",
			"VI" => "Virgin Islands (U.S.)",
			"WF" => "Wallis And Futuna Islands",
			"EH" => "Western Sahara",
			"YE" => "Yemen",
			"YU" => "Yugoslavia",
			"ZM" => "Zambia",
			"ZW" => "Zimbabwe"
		);
	}

	public static function country_select($name, $selected = 'NL', $placeholder = '', $required = '')
	{
		if ( empty( $selected )  ) {
			$selected = ( Poppyz_Core::get_option('default_country')  ) ? Poppyz_Core::get_option('default_country')  : 'NL';
		}

		$_countries = Poppyz_Core::countries();
		$select = '';
		$select .= '<select name="' . $name . '"' . $required .'>';
		if ( $placeholder )  $select .= '<option value="-1"> ' . $placeholder .  '</option>';
		foreach ($_countries as $k => $v) {
			$active = ( $k == $selected ) ? 'selected=selected' : '';
			$select .= '<option ' . $active . ' value="' . $k .'">' . $v . '</option>';
		}
		$select .= '</select>';

		return $select;
	}


	public static function form_settings_helper( $type, $name, $data, $default = '', $id = null, $placeholder = '', $class = 'ppy-input', $tooltip = false ) {

		$html = '';
		if ( $id == null ) {
			$id = str_replace( '_', '-', $name );
		}

		if ( $data != null ) {
			$value = ( isset( $data[$name] ) && $data[$name]  != '') ?  $data[$name] : $default;
		}


		if ( $type == 'text' || $type == 'number' || $type == 'hidden' ) {
			$class = ( $class ) ? ' class="' . $class . '"' : '';
			$placeholder = ( $placeholder ) ? ' placeholder="' . $placeholder . '"' : '';
			$html .= '<input type="' . $type .'" id="' . $id . '" name="ppy_settings[' . $name .']"  value="' . $value . '" ' . $placeholder . $class . '  />';
			if ($tooltip) $html .= '<span class="dashicons dashicons-info-outline poppyz-tooltip" title="' . $tooltip . '"></span>';

		} else if ( $type == 'select' ) {

		} else if ( $type == 'checkbox' ) {
			if ( $data != null ) {
				$checked = ( isset( $data[$name] ) && $data[$name]  == 'on') ?  ' checked="checked" ': '';
			}
			if ( !isset( $data[$name] ) ) {
				$checked = $default == 'on' ? ' checked="checked" ': '';
			}
			$tooltip_html = ($tooltip)  ?  '<span class="dashicons dashicons-info-outline poppyz-tooltip" title="' . $tooltip . '"></span>' : '';
			$html .= '<div class="ppy-field ppy-checkbox-wrapper ' . $class . '">
                <label class="switch" for="' . $id . '">
                    <input type="checkbox" class="yes_no_button"  name="ppy_settings[' . $name .']"   id="' . $id . '"' . $checked .' >
                    <div class="slider round dashicons-before "></div>
                </label>
                <p class="description">' . $placeholder .  '</p>
                ' . $tooltip_html . '
            </div>';

		}
		return $html;
	}

	public static function form_field_helper( $post_id, $field_data ) {
		//$post_id, $type, $name, $default, $id, $placeholder, $description, $class = 'ppy-input'
		$default = array(
			'desc' => '',
			'type'        => 'text',
			'options'     => array(),
			'std'         => '',
			'placeholder' =>  '',
			'class' => '',
			'wrapper_class' => '',
			'disabled' => false,
			'value' => null

		);
		$field_data = array_merge( $default, $field_data );

		$type = $field_data['type'];
		$name = $field_data['name'];
		$description = $field_data['desc'];
		$default = $field_data['std'];
		$placeholder = $field_data['placeholder'];
		$id = $field_data['id'];
		$class = $field_data['class'];
		$wrapper_class = $field_data['wrapper_class'] ? $field_data['wrapper_class'] : '';
		$disabled = $field_data['disabled'] ? ' disabled=disabled'  : '';


		$html = ($type != "checkbox2") ? '<div class="ppy-field-wrapper '. $wrapper_class . '">' : '';
		$value = !empty( $field_data['value'] ) ? $field_data['value'] : get_post_meta( $post_id, $id, true );
        if ( $id == PPY_PREFIX . "tier_extra_content" ) {
            $post_meta_value = get_post_meta( $post_id, $id, true );

            if ( empty($post_meta_value) ) {
				$bullets_default_content = "
								<ul>
									<li>Lorem ipsum dolor sit amet consectetur adipiscing</li>
									<li>Praesent elementum augue id nunc efficitur</li>
									<li>Cras porttitor eros venenatis aliquet tortor at faucibus</li>
									<li>Pellentesque dui nisl, suscipit id nunc ac</li>
								</ul>
								";
            }

			$value = empty( $post_meta_value ) ? $bullets_default_content : get_post_meta( $post_id, $id, true );
        }

		if ( $type == 'text' || $type == 'number' || $type == 'hidden' ) {
			$class = ( $class ) ? ' class="' . $class . '"' : '';
			$placeholder = ( $placeholder ) ? ' placeholder="' . $placeholder . '"' : '';
			$html .= '<label class="label">' . $name . '</label>';
			$html .= '<input type="' . $type .'" id="' . $id . '" name="' . $id .'"  value="' . $value . '" ' . $placeholder . $class . $disabled .'  />';
			if (isset($field_data['after_input']))
				$html .= ' <p>' . $field_data['after_input'] . '</p>';
			if ($description)
				$html .= '<p class="description">' . $description . '</p>';

		} elseif ( $type == 'date' ) {
			$class = 'class="date-picker short-2 ' . $class . '"';
			$html .= '<label class="label">' . $name . '</label>';
			$placeholder = ( $placeholder ) ? ' placeholder="' . $placeholder . '"' : '';
			$html .= '<input type="text" readonly="true" id="' . $id . '" name="' . $id .'"  value="' . $value . '" ' . $placeholder . $class . '  />';
			$html .= '<span class="dashicons dashicons-calendar-alt"></span>';
			if ($description)
				$html .= '<p class="description">' . $description . '</p>';
		} elseif ( $type == 'select' ) {
			$html .= '<label class="label">' . $name . '</label>';
			if (count($field_data['options']) > 0) {
				$html .= '<select id="' . $id . '" name="' . $id . '" >';
				if ($placeholder) $html .= '<option value="">' . $placeholder . '</option>';
				foreach ($field_data['options'] as $k => $v) {
					$html .= '<option ' . selected($k, $value, false) . ' value="' . $k . '">' . $v . '</option>';
				}
				$html .= '</select>';
				if ($description)
					$html .= '<p class="description">' . $description . '</p>';
			}
		} elseif ( $type == 'post' ) {
			$html .= '<label class="label">' . $name . '</label>';
			$args = $field_data['query_args'];
            if ( $field_data['post_type'] == "all" ) {
				$args['post_type'] = PPY_TIER_PT;
				$select_name = ( $id == PPY_PREFIX . "tier_upsells" ) ? PPY_PREFIX . "tier_upsells[]" : $id; // add [] if it is a upsell field.
				$select_id = ( $id == PPY_PREFIX . "tier_upsells" ) ? $field_data['upsell_id'] : $id; // change id if it is an upsell to make the upsell select unique to each other.
				$posts = new WP_Query( $args );
				$additional_products_select_class = (!empty($id) && str_contains($id, 'additional_product')) ? 'class="'.PPY_PREFIX .'tier_additional_product"' : '';
				$html .= '<select id="' . $select_id . '" '.$additional_products_select_class.' name="' . $select_name . '" >';
				//check if the post is upsell
				//then extract the upsell array
				if ( $id == PPY_PREFIX . "tier_upsells" ) {
					$value_upsell = !empty( $field_data['value'] ) ? $field_data['value'] : get_post_meta( $post_id, PPY_PREFIX . 'tier_upsells' );
					if (  isset($value_upsell[$field_data['upsell_count']]) && is_array($value_upsell)){
						$option_value = $value_upsell[$field_data['upsell_count']];
					} else if ( !is_array($value_upsell) && $field_data['upsell_count'] == 0 ) {
						$option_value = $value ;
					} else if ( $id == PPY_PREFIX . "tier_upsells" ) {
						$option_value = "" ;
					} else {
						$option_value = $value ;
					}
                } else {
					$option_value = $value ;
                }

				if ( $posts->have_posts() ) {
					$html .= '<option value="">' . $placeholder . '</option>';
					while ( $posts->have_posts() ) {
						$posts->the_post();
						$field_post_id = get_the_ID();
						$title =  get_the_title();
						if (get_post_type($field_post_id) == PPY_TIER_PT ) {
							$title =  get_the_title(self::get_course_by_tier($field_post_id)) . ' - ' . $title;
						}
						$additional_products_options = (!empty($id) && str_contains($id, 'additional_product')) ? ' class="'.$field_post_id.'"' : '';
						$option_tier_price = get_post_meta( $field_post_id, PPY_PREFIX .  'tier_price', true );

						if ( empty( $option_tier_price ) ) continue;

						$html .= '<option id="'.$field_post_id.'" '.$additional_products_options.' ' . selected( $field_post_id, $option_value, false ) . ' value="' . $field_post_id . '">' . $title . '</option>';
					}

					if ($description)
						$html .= '<p class="description">' . $description . '</p>';
				}
				$args['post_type'] = PPYV_PRODUCT_PT;
				$posts = new WP_Query( $args );
				if ( $posts->have_posts() ) {
					while ( $posts->have_posts() ) {
						$posts->the_post();
						$field_post_id = get_the_ID();
						$title =  get_the_title();
						$option_tier_price = get_post_meta( $field_post_id, PPY_PREFIX .  'tier_price', true );

						if ( empty( $option_tier_price ) ) continue;

						$html .= '<option ' . selected( $field_post_id, $option_value, false ) . ' value="' . $field_post_id . '">' . $title . '</option>';
					}

					if ($description)
						$html .= '<p class="description">' . $description . '</p>';
				}
				$html .= '</select>';
            } else {
				$args['post_type'] = $field_data['post_type'];

				$select_name = ( $id == PPY_PREFIX . "tier_upsells" ) ? PPY_PREFIX . "tier_upsells[]" : $id; // add [] if it is a upsell field.
				$select_id = ( $id == PPY_PREFIX . "tier_upsells" ) ? $field_data['upsell_id'] : $id; // change id if it is an upsell to make the upsell select unique to each other.


				if ( $id == PPY_PREFIX . "tier_upsells" ) {
					$value_upsell = !empty( $field_data['value'] ) ? $field_data['value'] : get_post_meta( $post_id, PPY_PREFIX . 'tier_upsells' );
					if (  isset($value_upsell[$field_data['upsell_count']]) && is_array($value_upsell)){
						$option_value = $value_upsell[$field_data['upsell_count']];
					} else if ( !is_array($value_upsell) && $field_data['upsell_count'] == 0 ) {
						$option_value = $value ;
					} else if ( $id == PPY_PREFIX . "tier_upsells" ) {
						$option_value = "" ;
					} else {
						$option_value = $value ;
					}
				} else {
					$option_value = $value ;
				}

				$posts = new WP_Query( $args );
				if ( $posts->have_posts() ) {
					$html .= '<select id="' . $select_id . '" name="' . $select_name . '" >';
					$html .= '<option value="">' . $placeholder . '</option>';
					while ( $posts->have_posts() ) {
						$posts->the_post();
						$field_post_id = get_the_ID();
						$title =  get_the_title();
						if (get_post_type($field_post_id) == PPY_TIER_PT ) {
							$title =  get_the_title(self::get_course_by_tier($field_post_id)) . ' - ' . $title;
						}
						$html .= '<option ' . selected( $field_post_id, $option_value, false ) . ' value="' . $field_post_id . '">' . $title . '</option>';
					}
					$html .= '</select>';
					if ($description)
						$html .= '<p class="description">' . $description . '</p>';
				}
            }

		} elseif ( $type == 'date' ) {
			$html .= '<label class="label">' . $name . '</label>';
			$html .= '<input type="' . $type .'" id="' . $id . '" name="' . $id .'"  value="' . $value . '" ' . $placeholder . $class . '  />';
		} elseif ( $type == 'checkbox' ) {
			$checked = ( $value  == 'on' || $value == '1' ) ? ' checked="checked" ': '';
			$html .= '<div class="ppy-field ppy-checkbox-wrapper ' . $class . '">
                <label class="switch" for="' . $id . '">
                    <input type="checkbox" class="yes_no_button" name="' . $id .'"  id="' . $id . '"' . $checked .' >
                    <div class="slider round dashicons-before "></div>
                </label>
                <p class="description">' . $placeholder .  '</p>
            </div>';
		}  elseif ( $type == 'checkbox2' ) {
			$checked = ($value == 'on' || $value == '1') ? ' checked="checked" ' : '';
			$html .= '<div class="ppy-field ppy-checkbox-wrapper ' . $class . '">
                <label class="switch" for="' . $id . '">
                    <input type="checkbox" class="yes_no_button" name="' . $id . '"  id="' . $id . '"' . $checked . ' >
                    <div class="slider round dashicons-before "></div>
                </label>
                <p class="description">' . $placeholder . '</p>
            </div>';
		} elseif ( $type == 'html' ) {
			$html .= '<label class="label">' . $name . '</label>';
			$html .= '<div>'.  $default . '</div>';
		} elseif ( $type == 'editor' ) {
			$editor_name = ( $id == PPY_PREFIX . "tier_upsell_description" ) ? PPY_PREFIX . "tier_upsell_description[]" : $id; // add [] if it is a upsell field.

            // change editor ID if it is an upsell to make it unique to other upsell editor.
            $editor_id = ( $id == PPY_PREFIX . "tier_upsell_description" ) ? $field_data['upsell_id'] : $id;
			$settings_editor = array(
				'textarea_rows' => 20,
				'editor_height' => 425,
				'tabindex' => 1,
				'textarea_name' => $editor_name
			);
			$html .= '<label class="label">' . $name . '</label>';
			ob_start();                    // start buffer capture

			//check if the post is upsell
			//then extract the upsell array
			if ( $id == PPY_PREFIX . "tier_upsell_description" && isset($value[$field_data['upsell_count']]) ){
                $tier_upsell_description = get_post_meta( $post_id, $id );
				$tier_upsell_description = (is_array($tier_upsell_description[0])) ? $tier_upsell_description[0] : $tier_upsell_description;
				$editor_value = $tier_upsell_description[$field_data['upsell_count']];
            } else if ( $id == PPY_PREFIX . "tier_upsell_description" && !is_array($value) && $field_data['upsell_count'] == 0 ) {
				$editor_value = $value;

			}else if ( $id == PPY_PREFIX . "tier_upsell_description" ) {
				$editor_value = "" ;
            } else {
				$editor_value = $value ;
            }
			wp_editor( $editor_value, $editor_id, $settings_editor);
			$html .= ob_get_contents(); // put the buffer into a variable
			ob_end_clean();                // end capture
			if ($description) {
				$html .= '<div class="break"></div>';
				$html .= '<label></label>';
				$html .= '<p class="description full">' . $description . '</p>';
			}

		}  elseif ( $type == 'image' ) {
			$html .= '<label class="label">' . $name . '</label>';
			$html .= '<input type="file" name="'.$id.'" />';
			$html .= '<p class="description full additional-product-image-size-requirement">' . __('Requirements: Height: 104px & Width: 70px') . '</p>';
		}
		$html .= ($type != "checkbox2") ? '</div>' : '';
		return $html;
	}

	public static function has_session() {
		return is_page( array('purchase') );
	}

    public static function format_number( $number ) {
        $currency = Poppyz_Core::get_option( 'currency' );
        if ( $currency == 'usd' ) {
            $number = number_format( $number, 2 );
        } else {
            //use euro notation
            $number = number_format( $number, 2, ',', '.' );
            $number = str_replace( ',00', '', $number );
        }

        return $number;
    }

	public static function format_price( $price, $symbol = '&euro; ' ) {
		$currency = Poppyz_Core::get_option( 'currency' );
		if ( $currency == 'usd' ) {
			$symbol = ( $symbol == '' ) ? '' : '$ ';
			$price = number_format( $price, 2 );
		} else {
			//use euro
			$price = number_format( $price, 2, ',', '.' );
			//if last decimals are 00, remove and replace with ,-.
			$price = str_replace( ',00', ',-', $price );
		}

		return $symbol . $price;
	}

	public static function currency_symbol() {
		$currency = Poppyz_Core::get_option( 'currency' );
		if ( $currency == 'usd' ) {
			$symbol = '$';
		} else {
			//use euro
			$symbol = '&euro;';
		}
		return $symbol;
	}

	public static function get_currency() {
		$currency = Poppyz_Core::get_option( 'currency' );
		if ( !$currency ) {
			$currency = 'EUR';
		}
		return strtoupper($currency);
	}

	public static function get_tax_rate( $tier_id = null, $user_id = null, $single = false ) {

		if ( $user_id ) {
			if ( !self::is_same_country( $user_id ) ) {
				$user_country = get_user_meta($user_id, 'ppy_country', true) ?: 'NL';
				$trc_data = get_option( 'trc_data' );
				if ($trc_data) {
					if ( isset( $trc_data[$user_country] ) ) {
						return $trc_data[$user_country];
					}
				}
			}
			//$single variable tells us that this call gets the tax rate regardless if the person does not have tax, used to subtract tax amount for "inc" vat shifted individuals below on get_fixed_tax_amount
			if ( !$single && self::has_no_tax( $user_id, $tier_id )) {
				return 0;
			}
		}

		if ( $tier_id ) {
			$tax_rate = get_post_meta( $tier_id, 'ppy_tier_tax_rate', true );
			if ($tax_rate !== false && $tax_rate !== '') {
				return get_post_meta( $tier_id, 'ppy_tier_tax_rate', true ) ;
			}
		}

		return (int)Poppyz_Core::get_option( 'tax_rate' );

	}

	public static function get_fixed_tax_amount( $amount, $tier_id = null, $price_tax = 'exc', $user_id = false, $single = false ) {
		if (!$user_id) {
			$user_id =  get_current_user_id();
		}
		if ( empty( $amount ) ) {
			$amount = 0;
		}
		$tax_rate = Poppyz_Core::get_tax_rate( $tier_id, $user_id, $single );

		if ( $tax_rate > 0 ) {
			if ( $price_tax  == 'inc' ) {
				$amount = round( $amount - ( $amount / ( $tax_rate / 100 + 1 ) ), 3 ) ;
			} else {
				$amount = round( $amount * ( $tax_rate / 100 ), 3 ) ;
			}
		} else {
			$amount = 0;
		}

		return $amount;
	}

	public static function get_price_tax( $tier_id = null) {
		if ( $tier_id && get_post_meta( $tier_id, PPY_PREFIX . 'price_tax', true ) ) {
			return get_post_meta( $tier_id, PPY_PREFIX . 'price_tax', true );
		} else {
			if ( Poppyz_Core::get_option( 'price_tax' ) == 'exc' ) {
				return 'exc';
			} else {
				return 'inc';
			}
		}
	}

	public static function is_same_country( $user_id ) {
		$user_country = get_user_meta($user_id, 'ppy_country', true) ?: 'NL';
		$admin_country = Poppyz_Core::get_option('country') ?: 'NL';

		return $user_country == $admin_country;
	}

	public static function price_with_tax( $price, $tier_id = null, $user_id = null ) {
		$price_tax =  Poppyz_Core::get_price_tax( $tier_id );
		$tax_amount = Poppyz_Core::get_fixed_tax_amount( $price, $tier_id, $price_tax, $user_id );
		if ( empty( $price ) ) {
			$price = 0;
		}
		if ( Poppyz_Core::is_donation_enabled( $tier_id ) ) {
			return $price;
		}
		if ( $price_tax == 'exc' && !self::has_no_tax( $user_id ) ) {
			return $price + $tax_amount;
		} elseif ( $price_tax == 'inc' && ( self::has_no_tax( $user_id )  || Poppyz_Invoice::is_chargedback( $user_id ) ) ) {
			return $price - Poppyz_Core::get_fixed_tax_amount( $price, $tier_id, $price_tax, $user_id, true );
		} else {
			return $price;
		}
	}
	public static function is_donation_enabled( $tier_id ) {
		return get_post_meta( $tier_id, PPY_PREFIX . 'tier_donation_enabled', true );
	}
	public static function has_no_tax( $user_id, $tier_id = null ) {
		return Poppyz_Invoice::is_vatshifted( $user_id ) || Poppyz_Invoice::is_vat_out_of_scope( $user_id ) || Poppyz_Invoice::is_vat_disabled() ;
	}

	//since using the_content filter does not work as we intend it to, only choose the filters that we need to format content
	public static function the_content( $content ) {
		$content = wptexturize( $content );
		$content = wpautop( $content );
		$content = do_shortcode( $content, true );
		return $content;
	}

	public static function breadcrumbs( $step = 1, $show_steps = true ) {
		global $ppy_lang;

		$html =  (  $show_steps == true ) ? __( 'You are here: ' ,'poppyz') . ' ' : '';

		$html .= '<ul class="steps">';

		$title[] = __( 'Order' ,'poppyz');
		//$title[] = __( 'Register' ,'poppyz');
		$title[] = __( 'Payment' ,'poppyz');
		/* $title[] = __( 'Confirmation' );
		 $title[] = __( 'Payment' );*/

		for ( $i = 0; $i < 2; $i++ ) {
			$number =  $i + 1;
			$step_number = ($show_steps == true) ? __( 'Step' ,'poppyz') . ' ' . $number . '. ' : "";
			$steps[$i] = '<li>' . $step_number . ' ' . $title[$i] . ' <span>></span> </li>';
		}

		for ( $i = 0; $i < 2; $i++ ){
			if ( ( $step - 1 ) == $i ) {
				$steps[$i] = str_replace( '<li', '<li class="current"', $steps[$i] );
			}
			$html .= $steps[$i];
		}

		$html = substr( $html, 0, -20);
		$html .= '</li>';
		$html .= '</ul>';
		return $html;
	}

	public static function terms_condition() {
		$terms_condition_title = Poppyz_Core::get_option( 'terms_condition_title' );
		$terms_condition = Poppyz_Core::get_option( 'terms_condition' );
		$content = "";

		if ( !empty( $terms_condition_title ) ) {
			$content .= '<h3>' . $terms_condition_title . '</h3>';
		}
		if ( !empty( $terms_condition ) ) {
			$content .= Poppyz_Core::the_content( $terms_condition );
		}

		return $content;
	}

	public static function terms_checkboxes() {

		$terms_checkboxes = Poppyz_Core::get_option( 'terms_checkbox' );
		if ( !$terms_checkboxes ) return;
		$content = "";
		$script = "";
		$i = 0;
		foreach ( $terms_checkboxes as $checkbox_text ) {
			if ( !empty( $checkbox_text ) ) {
				$i++;
				$checkbox_text = str_replace( '[company]',  Poppyz_Core::get_option('invoice_company'), $checkbox_text );
				$content .= '<p><input type="checkbox" value="yes" name="terms_checkbox[]" class="terms-checkbox" id="terms_checkbox_' . $i . '" />  <label for="terms_checkbox_' . $i . '">' . $checkbox_text . '</label></p>';
			}
		}
		return  $content;
	}

	public static function terms_alert() {
		global $ppy_lang;
		$terms_required = Poppyz_Core::get_option( 'terms_checkbox_required' );
		if ( !$terms_required ) {
			__( 'You must first tick all check boxes to continue.' ,'poppyz');
		}
		ob_start();
		?>
        <script>
            (function( $ ) {
                'use strict';

                $(function() {
                    //don't submit form if the terms checkboxes are not checked
                    $("#ppy-form").submit(function (e) {
                        $('input[type=checkbox].terms-checkbox').each(function () {
                            var pass = true;
                            if (!this.checked) {
                                alert('<?php echo $terms_required; ?>');
                                pass = false;
                                e.preventDefault();
                                return false;
                            }
                        });
                    });
                    $("#ppy-eproduct-form").submit(function (e) {
                        $('input[type=checkbox].terms-checkbox').each(function () {
                            var pass = true;
                            if (!this.checked) {
                                alert('<?php echo $terms_required; ?>');
                                pass = false;
                                e.preventDefault();
                                return false;
                            }
                        });
                    });
                });
            })( jQuery );
        </script>
		<?php
		$script = ob_get_contents();
		ob_end_clean();
		return $script;
	}

	public static function days_in_month($month, $year) {
		return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
	}

	public static function is_poppyz_page() {
		return ( is_page( Poppyz_Core::get_option( 'login_page' ) ) || is_page( Poppyz_Core::get_option( 'register_page' ) ) || is_page( Poppyz_Core::get_option( 'edit-profile_page' ) ) || is_page( Poppyz_Core::get_option( 'purchase_page' ) ) || is_page( Poppyz_Core::get_option( 'return_page' ) ) );
	}

	public static function lesson_list( $atts = "" ) {

		//only show the lesson list inside courses and lessons
		if ( !is_user_logged_in() || ( get_post_type() != PPY_COURSE_PT && get_post_type() != PPY_LESSON_PT && !isset($atts['course']) ) ) return '';

		$atts = shortcode_atts( array(
			'display' => 'list',
			'show_course_title' => true,
			'collapsible' => false,
			'all' => false,
			'course' => false,
			'content' => false,
		), $atts, 'lesson-list' );

		$current_user = wp_get_current_user();
		$user_id = $current_user->ID;
		$course_id = get_the_ID();
		$ppy_core = new Poppyz_Core();
		$ppy_subs = new Poppyz_Subscription();
		global $ppy_lang;

		if ( get_post_type() == PPY_LESSON_PT ) {
			$course_id = $ppy_core->get_course_by_lesson( get_the_ID() );
		}

		if ( $atts['course'] && $atts['course'] != '-1' ) {
			$course_id = $atts['course'];
		}

		$show_all_lessons = $atts['all'];
		$a_lessons = $ppy_subs->get_lessons_by_subscription( $course_id, $user_id, $show_all_lessons );
		$return = "";
		$access = $ppy_subs->is_subscribed( $course_id, $user_id );

		$module_page_enabled = Poppyz_Core::get_option('enable-module-page') == 'on';
		//get all the lessons in this course that are accessible by the user
		//keep in mind that the lessons are grouped by tier, since a user can subscribe more than once on a given course

		if ( is_array( $a_lessons ) ) {
			//get lesson categories
			$args = array(
				'taxonomy' => PPY_LESSON_CAT,
				'orderby' =>  'meta_value_num',
				'meta_key' =>  'menu_order',
				'order' =>  'ASC',
				'hide_empty' =>  false,
				'hierarchical' =>  false,
				'parent' =>  0,
			);

			$terms = get_terms( $args );
			$has_categories =  ( ! empty( $terms ) && ! is_wp_error( $terms ) );

			$is_divi_active = function_exists('ppy_body_classes'); //check if activated theme is divi poppyz

			$current_term = false;
			if ( is_tax() ) {
				$queried_object = get_queried_object();
				$current_term = $queried_object->name;
			}

			$sub_for_exclusion = false;
			if ($show_all_lessons == '1') {
				global $wpdb;
				$subs = $wpdb->get_results( $wpdb->prepare(
					"
                        SELECT tier_id, subscription_date
                        FROM {$wpdb->prefix}course_subscriptions
                        WHERE user_id = %d
                        AND course_id = %d
                        AND status = %s                    
                    ",
					$user_id,
					$course_id,
					'on'
				) );
				$sub_for_exclusion = ( $subs && isset($subs[0])) ? end($subs) : false;
			}

			foreach ( $a_lessons as $tier => $lessons ) {
				if ( !is_wp_error( $access ) ) {

					if ( $sub_for_exclusion && !current_user_can( 'manage_options' ) ) {
						$excluded = [];
						foreach ($lessons as $lesson_id  ) {
							$lesson = get_post_meta( $sub_for_exclusion->tier_id,  PPY_PREFIX . 'tier_lessons_' . $lesson_id, true );
							if ( !empty( $lesson['exclude'] ) ) {
								$excluded[] = $lesson_id;
							}
						}
						$lessons = array_diff( $lessons, $excluded );
					}

					if ( $tier ) {
						$tier_name = ' - ' . get_the_title( $tier );
					}  else {
						$tier_name = "";
					}
					$collapsible = ( $atts['collapsible'] == true ) ? ' collapsible' : '';
					if ( $atts['display'] == 'progress' ) {
						$return .= '<div class="progress-list ' . $collapsible . '">';
					} else {
						$return .= '<div class="lesson-list ' . $collapsible . '">';
					}
					$return .= '<div class="inner">';
					$return .= '<div class="inner-2">';
					if ( $atts['show_course_title'] )
						$return .= '<h4><a href="' . get_permalink( $course_id ) . '">' . get_the_title( $course_id ) . '</a>' . $tier_name . '</h4>';

					if ( $has_categories ) {

						$lesson_list_output = '';
						foreach ( $terms as $term ) {
							$active = ( has_term($term->term_id, PPY_LESSON_CAT ) || $current_term == $term->name ) ? 'current' : '';
							$output = '<div class="module ' . $active . ' ">';
							if ( $module_page_enabled ) {
								$output .= '<h3><a href="' . get_term_link($term) . '">' . $term->name . '</a></h3>';
							} else {
								$icon = ( !$is_divi_active ) ? '<i class="icon"></i>' : ''; //only add the icon if the activated theme is not divi poppyz
								$output .= '<h3>' . $term->name . $icon .'</h3>';
							}
							$lessons_filtered = $ppy_core->get_lessons_by_ids( $lessons, $term->term_id );
							$lesson_list = Poppyz_Core::lesson_list_output( $lessons_filtered, $atts['display'], $show_all_lessons, $atts['content']);
							if ( $lesson_list ) {
								$lesson_list_output .= $output . $lesson_list .  '</div>';
							}
						}
						$return .= $lesson_list_output;
						//if there is no output, it means that categories were created but the current course's lessons are not assigned to any category.
						if ( empty( $lesson_list_output ) ) {
							$lessons_filtered = $ppy_core->get_lessons_by_ids( $lessons );
							$return .= Poppyz_Core::lesson_list_output( $lessons_filtered, $atts['display'], $show_all_lessons, $atts['content'] );
						}
					} else {
                        $lessons_filtered = false;
                        if ( !empty($lessons) ) {
                            $lessons_filtered = $ppy_core->get_lessons_by_ids( $lessons );
                        }
						if (  $atts['display'] == 'progress' ) {
							$output = '<div class="module">';
							$output .= '<h3>' . __( 'Lessons' ,'poppyz') . '</h3>';
							$return .= $output . Poppyz_Core::lesson_list_output( $lessons_filtered, $atts['display'], $show_all_lessons, $atts['content'] ) . '</div>';
						} else {
							$return .= Poppyz_Core::lesson_list_output( $lessons_filtered, $atts['display'], $show_all_lessons, $atts['content'] );
						}


					}
					$return .= '</div>';
					$return .= '</div>';
					$return .= '</div>';
				}
			}
			wp_reset_query();
			wp_reset_postdata();
			return $return;
		} elseif ( is_wp_error( $access )  ) {
			return  $access->get_error_message();
		} else {
			return '';
		}
	}

	public static function lesson_list_output( $lessons, $display, $all, $content = false ) {
		$lesson_list = "";
        $current_id = get_the_ID();
		if ( $lessons && $lessons->have_posts() ) {
			$lesson_list .= "<ul class='" . $display ."'>";
			$i = 0;

			while ($lessons->have_posts()) : $lessons->the_post();
				$i++;
				$class = (Poppyz_Core::has_read_lesson( get_the_ID(), get_current_user_id() ) ) ? 'has_read' : '';
				$link = get_permalink();
				$message = '';
				if ($all) {
					$ppy_sub = new Poppyz_Subscription();
					$subscribed = $ppy_sub->is_subscribed( get_the_ID(), get_current_user_id() );
					if (is_wp_error($subscribed)) {
						$noclick = empty( $custom_link ) ? " onclick='return false;'" : '';
						$error =  isset($subscribed->errors['no_access'][0]) ? $subscribed->errors['no_access'][0] : '';
						$message = $error ? 'title="' . $error . '"' . $noclick : '';
						$message = str_replace('return false;', 'alert("' . esc_attr($error) . '"); return false;', $message );
						$class .= ' no-access';
						$link = '#';
					}
				}
                $class .= (get_the_ID() == $current_id) ? 'current' : '';
				$class = $class ? ' class=' . $class : '';
				$lesson_list .= "<li" . $class . ">";
				if ( $display == 'image' ) {
					$image = ( has_post_thumbnail() ) ? get_the_post_thumbnail( null, 'thumbnail', array( 'class' => 'alignleft size-thumbnail' ) ) : '<img class="alignleft size-thumbnail" src="' . PPY_DIR_URL . '/public/images/default_thumb.jpg"  width="' . get_option('thumbnail_size_w'). '" height="' . get_option('thumbnail_size_h'). '" />';
					$lesson_list .= "<a $message href='" . $link . "'>" . $image . "</a>";
				}
                elseif ( $display == 'progress' ){
					$lesson_list .= "<span class='lesson-number'>" . $i . "</span><h4 class='lesson-title'><a href='" . $link . "'>" . get_the_title() . "</h4></a>";
					$lesson_list .= "<span class='status'></span>";
				}
				else {
					$lesson_list .= "<a $message href='" . $link . "'>" . get_the_title() . "</a>";
				}
				if ( $content ) $lesson_list .= '<span class="flex-break"></span><span class="lesson-excerpt">' . get_the_excerpt() . '</span>';

				$lesson_list .=  "</li>";
			endwhile;
			$lesson_list .= "</ul>";
			wp_reset_query();
			wp_reset_postdata();
			return $lesson_list;
		} else {
			return false;
		}
	}
	public static function getTierTotalPrice($tier_id) {
		$initial_amount = get_post_meta( $tier_id,  PPY_PREFIX . 'tier_initial_price', true );
		$price = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true );
		$price = ( !empty($initial_amount) ) ? $initial_amount : $price;

		$tier_discount = get_post_meta( $tier_id, PPY_PREFIX .  'tier_discount', true );
		if ( !empty($tier_discount) && $tier_discount < $price ) {
			$price = $price - $tier_discount;
		} else if ( !empty($tier_discount) && $tier_discount >= $price ) {
			$price = 0;
		}

		$additionalProducts = (isset($_POST['additional_products'])) ? $_POST['additional_products'] : Poppyz_Core::get_additional_products_cookie();
		$bundledProducts = get_post_meta( $tier_id, PPY_PREFIX . 'tier_bundled_products',  true );
		if ( !empty($additionalProducts) && (empty($bundledProducts) || $bundledProducts == "no") ) {
			foreach ( $additionalProducts as $additionalProductId) {
				$additionalProductPrice = get_post_meta($additionalProductId, PPY_PREFIX . 'tier_price', true);
				$price = $price + $additionalProductPrice;
			}
		}
		return $price;
	}

	public static function getTotalTax($tier_id) {
		$initial_amount = get_post_meta( $tier_id,  PPY_PREFIX . 'tier_initial_price', true );
		$price = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true );
		$price = ( !empty($initial_amount) ) ? $initial_amount : $price;
		$tax_amount = 0;

		$tier_discount = get_post_meta( $tier_id, PPY_PREFIX .  'tier_discount', true );
		if (!empty($tier_discount) && $price > $tier_discount ) {
			$price = $price - $tier_discount;
		} else if (!empty($tier_discount) && $price <= $tier_discount ) {
			$price = 0;
		}

		if ( Poppyz_Core::get_price_tax( $tier_id ) != 'inc' ) {
			$tax_amount = Poppyz_Core::get_fixed_tax_amount($price, $tier_id, Poppyz_Core::get_price_tax($tier_id));
		}
		$additionalProducts = (isset($_POST['additional_products'])) ? $_POST['additional_products'] : Poppyz_Core::get_additional_products_cookie();
		$bundledProducts = get_post_meta( $tier_id, PPY_PREFIX . 'tier_bundled_products',  true );

		if ( !empty($additionalProducts) && (empty($bundledProducts) || $bundledProducts == "no") ) {
			foreach ( $additionalProducts as $additionalProductId ) {
				$price = get_post_meta($additionalProductId, PPY_PREFIX . 'tier_price', true);
				$tax_amount_additional_product = 0;
				if ( Poppyz_Core::get_price_tax( $additionalProductId ) != 'inc' ) {
					$tax_amount_additional_product = Poppyz_Core::get_fixed_tax_amount($price, $additionalProductId, Poppyz_Core::get_price_tax($additionalProductId));
				}
				$tax_amount = $tax_amount + $tax_amount_additional_product;

			}
		}
		return $tax_amount;
	}

	public static function getUnfilteredTotalTax($tier_id) {
		$initial_amount = get_post_meta( $tier_id,  PPY_PREFIX . 'tier_initial_price', true );
		$price = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true );
		$price = ( !empty($initial_amount) ) ? $initial_amount : $price;
		$tax_amount = 0;

		$tier_discount = get_post_meta( $tier_id, PPY_PREFIX .  'tier_discount', true );
		if ( !empty($tier_discount) && $tier_discount < $price ) {
			$price = $price - $tier_discount;
		} else if ( !empty($tier_discount) && $tier_discount >= $price ) {
			$price = 0;
		}
		$tax_amount = Poppyz_Core::get_fixed_tax_amount($price, $tier_id, Poppyz_Core::get_price_tax($tier_id));
		$additionalProducts = (isset($_POST['additional_products'])) ? $_POST['additional_products'] : Poppyz_Core::get_additional_products_cookie();
		$bundledProducts = get_post_meta( $tier_id, PPY_PREFIX . 'tier_bundled_products',  true );

		if ( !empty($additionalProducts) && (empty($bundledProducts) || $bundledProducts == "no") ) {
			foreach ( $additionalProducts as $additionalProductId ) {
				$price = get_post_meta($additionalProductId, PPY_PREFIX . 'tier_price', true);
				$tax_amount_additional_product = Poppyz_Core::get_fixed_tax_amount($price, $additionalProductId, Poppyz_Core::get_price_tax($additionalProductId));
				$tax_amount = $tax_amount + $tax_amount_additional_product;

			}
		}
		return $tax_amount;
	}

	/**
	 * @param int $tier_id
	 * @param $price
	 * @param string $content
	 * @param string $current_url
	 * @return string
	 */
	public static function purchase_button($atts = '')
	{
		$atts = shortcode_atts( array(
			'tier_id' => 0,
			'content' => false,
		), $atts, 'purchase-button' );

		$tier_id = $atts['tier_id'];
		$content = $atts['content'];
		$current_url = self::get_tier_purchase_url($tier_id);
		$has_additional_product = false;

		$content = '<form id="ppy-form" method="post" action="' . $current_url . '">';

		$mainProductPrice = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true );
        //$price = Poppyz_Core::getTierTotalPrice($tier_id);
		$initial_amount = get_post_meta( $tier_id,  PPY_PREFIX . 'tier_initial_price', true );
		$price = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true );
		$price = ( !empty($initial_amount) ) ? $initial_amount : $price;
		for($i=1;$i<6;$i++) {
			$additionalProductId = get_post_meta($tier_id, PPY_PREFIX . 'tier_additional_product' . $i, true);
            if ( !empty($additionalProductId) ) {
                $has_additional_product = true;
                break;
            }
		}


		$donation_enabled = get_post_meta( $tier_id, PPY_PREFIX . 'tier_donation_enabled', true );

		$vat =  Poppyz_Core::get_fixed_tax_amount( $price, $tier_id, Poppyz_Core::get_price_tax( $tier_id ) );


		$step = 1;
		$return = 'full';
		if ($donation_enabled) {
			$return = 'title_only';
		}
		$tier_title = apply_filters('ppy_tier_title', $tier_id, $mainProductPrice, $return);

		$mo_api_key = Poppyz_Core::get_option('mo_api_key');
		$mo_test_api_key = Poppyz_Core::get_option('mo_api_test_key');
		$return_page = Poppyz_Core::get_option('return_page');
		if ($price == 0) {
			$free_button_text = Poppyz_Core::get_option('free_purchase_button_text');
			if (!$free_button_text) {
				$free_button_text = __('Free - Subscribe now!', 'poppyz');
			}
			$button_text = apply_filters('ppy_free_tier_button_text', $free_button_text);
		} else {
			$tier_button_text = Poppyz_Core::get_option('purchase_button_text');
			if (!$tier_button_text) {
				$tier_button_text = __('Checkout', 'poppyz');
			}
			$button_text = apply_filters('ppy_tier_button_text', $tier_button_text);
		}

		$button_name = (is_user_logged_in()) ? 'buy' : 'reg_submit';

		$buy_button = '<input type="submit" class="full-width et_pb_button buy-button" name="' . $button_name . '" value="' . $button_text . '"/>';
		$buy_button_mobile = str_replace('full-width', 'full-width et_pb_button buy-button', $buy_button);

        $content = "";
		if (($price == 0 || !empty($mo_api_key) || !empty($mo_test_api_key)) && (!empty($return_page) &&  $return_page > 0)) {
			if (is_user_logged_in()) {
				$content .= apply_filters('step1_content_after', $content);
			} else {
				$content .= "<div id='registration-form'>";
				$register_id = Poppyz_Core::get_option('register_page');
                if ($register_id) {
                    $content .= Poppyz_Core::the_content(get_post_field('post_content', $register_id));
                }
				require_once(PPY_DIR_PATH . 'public/class-poppyz-user.php');
				$user = new Poppyz_User();

                $buyQueryString = '?buy';
                //if tier has donation enabled, do not redirect the user to buy when logging in because they need to select a donation first. This also prevents not being able to donate after already purchasing the tier.
                $tierVoluntaryContributionEnabled = get_post_meta( $tier_id, PPY_PREFIX . "tier_voluntary_contribution", true );
                if ($tierVoluntaryContributionEnabled == 'yes') {
                    $buyQueryString = '';
                }
                $login_link = sprintf(__('Already a member?&nbsp;&nbsp; <a class="et_pb_button white" href="%s">Login</a>', 'poppyz'), wp_login_url($current_url . $buyQueryString));

				$content .= '<p class="complete-fields">' . Poppyz_Core::get_option('registration_form_description') . '</p>';
				$content .= '<p class="login-here">' . $login_link . '</p>';
				$content .= '<h3 class="">' . apply_filters('ppy_required_information_text', __('Account and billing information', 'poppyz')) . '</h3>';

				//if it's a free tier, there's no need to display the additional invoice fields so make it a quick registration form
				$payment_method = get_post_meta( $tier_id, PPY_PREFIX . 'payment_method', true );
				$donationStatus = get_post_meta( $tier_id, PPY_PREFIX . 'tier_voluntary_contribution', true );
				$quick = ( $price == 0 && $donationStatus != "yes" && $has_additional_product != true ) ? true : false;
				$content .= $user->registration_form($quick);
				//$content .= $buy_button_mobile;
				$content = apply_filters('step2_content_after', $content);
				$content .= "</div>";
			}
		} else if ( $return_page <= 0 ) {
			$content .= __('The Return Page is not set.', 'poppyz');
		} else {
			$content .= __('Payment is not enabled.', 'poppyz');
		}

		$paymentMethod = get_post_meta( $tier_id, PPY_PREFIX . 'payment_method', true );
		$donationStatus = get_post_meta( $tier_id, PPY_PREFIX . 'tier_voluntary_contribution', true );
		$donationDescription = get_post_meta( $tier_id, PPY_PREFIX . "tier_donation_description", true );
		$donationtitle = get_post_meta( $tier_id, PPY_PREFIX . "tier_donation_title", true );

		$tier_extra_content = get_post_meta( $tier_id, PPY_PREFIX . 'tier_extra_content', true );
        $settings_extra_content = Poppyz_Core::get_option('registration_form_extra_content');
        if ( !empty($tier_extra_content)  && strpos($tier_extra_content, "Lorem ipsum dolor sit amet consectetur") == false ) {
			$registration_form_extra_content = $tier_extra_content;
		} else if ( !empty($settings_extra_content) && strpos($settings_extra_content, "Lorem ipsum dolor sit amet consectetur") == false ) {
			$registration_form_extra_content = $settings_extra_content;
		} else  {
			$registration_form_extra_content = "";
		}
        $class = '';
		$class .= (is_user_logged_in()) ? 'logged-in' : '';
        $class = 'class="'.$class.'"';
		$content .= "<div id='tier-information' $class>";
		$content .= "<div class='tier-bullets' >";
		$content .= $registration_form_extra_content ;
		$content .= "</div>";
		$content .= "<div class='tier-details' >";
        $content .= "<div class='tier-section'>";
        $content .= "<h3 class='your-order'>" . __('Your Order', 'poppyz') . "</h3>";
        $content .= "<h3 class='tier-title tier-title-h3'>" . $tier_title . "</h3>";
        $content .= "<input type='hidden' class='vat' value='".$vat."'/>";


        $tier_discount = get_post_meta( $tier_id, PPY_PREFIX . "tier_discount", true );
        $tier_discount = ( !empty($tier_discount) ) ? $tier_discount: 0;
		$content .= "<input type='hidden' name='tier_discount' value='".$tier_discount."'/>";
        if ( $tier_discount != 0 && $price > $tier_discount ) {
			$content .= "<div class='tier-discount-container' >";
            $content .= "<span>" . __('Promotional discount', 'poppyz') . "</span>";
			$content .= "<span> -" . Poppyz_Core::format_price( $tier_discount ) . "</span>";
            $content .= "</div>";
            $price = $price - $tier_discount;
		} else if ( $tier_discount != 0 && $price < $tier_discount ) {
			$content .= "<div class='tier-discount-container' >";
			$content .= "<span>" . __('Promotional discount', 'poppyz') . "</span>";
			$content .= "<span> -" . Poppyz_Core::format_price( $tier_discount ) . "</span>";
			$content .= "</div>";
			$price = 0;
		}
		$price_with_tax = Poppyz_Core::price_with_tax( $price, $tier_id, null );
		$content .= "<input type='hidden' class='price_with_tax' id='main_tier_price_with_tax' value='".$price_with_tax."'/>";

		$purchase_page_setting = Poppyz_Core::get_option('purchase_page_content');
		if (!empty($purchase_page_setting)) {
			$content .= Poppyz_Core::the_content($purchase_page_setting);
		}
		if ( $donationStatus == "yes" ) {
			$content .= '<div class="donation-hr"></div>';
			if (!empty($donationtitle)) {
				$content .= "<h3>" . $donationtitle . "</h3>";

			} else {
				$content .= "<h3>" . __('Your Order', 'poppyz') . "</h3>";
			}

			if (!empty($donationDescription)) {
				$content .= '<div class="donation-description">';
				$content .= $donationDescription;
				$content .= '</div>';
			}
			$imageUrl = get_post_meta($tier_id, PPY_PREFIX . 'tier_donation_image_url', true);
			$attachementId = get_post_meta($tier_id, PPY_PREFIX . 'tier_donation_image', true);
			if (!empty($imageUrl)) {
				$content .= '<div class="donation-image">';
				$content .= '<img src="' . $imageUrl . '">';
				$content .= '</div>';
			}
		} else {
			$content .= '<div class="purchase-hr"></div>';
		}

		$content_post = get_post($tier_id);
		$tier_content = $content_post->post_content;
		if (!empty($tier_content)) {
			$tier_content = Poppyz_Core::the_content($tier_content);
			$tier_content = '<div class="tier-content">' . do_shortcode($tier_content) . '</div>';
			$content .= $tier_content;
		}


		wp_reset_postdata();
		if ( $donationStatus == "yes" ) {
			$content .= "<div class='tier-section'>";
			$donationFixedAmounts = get_post_meta( $tier_id, PPY_PREFIX . 'donation_fixed_amounts' );
			$donationFixedAmounts = get_post_meta( $tier_id, PPY_PREFIX . 'donation_fixed_amounts' );
			$content .= '<div class="donation-row">';
			$content .= '<label class="donation-amount-label" for="'.PPY_PREFIX.'donation_amount">'.__("Select an amount", "poppyz").'</label>';
			$donationRadioButton = '<div class="donation-radio-button-wrapper">';
			foreach ( $donationFixedAmounts[0] as $amount ) {
				if ( !empty($amount) && $amount > 0 ) {
					$donationRadioButton .= '<div class="donation-radio-button-container">';
					$donationRadioButton .= '<input type="radio" name="'.PPY_PREFIX.'donation_amount" class="'.PPY_PREFIX.'donation_amount" value="'.$amount.'"/> '.Poppyz_Core::format_price( $amount, '€' );
					$donationRadioButton .= '</div>';
				}

			}
			$donationRadioButton .= "</div>";
			$donationAllowCustomAmount = get_post_meta( $tier_id, PPY_PREFIX . 'donation_allow_custom_amount');
            $content .= '';
			$content .= $donationRadioButton;
			$content .= "</div>";

			$donationAllowCustomAmount = get_post_meta( $tier_id, PPY_PREFIX . 'donation_allow_custom_amount', true);
			if ( !empty($donationAllowCustomAmount) && $donationAllowCustomAmount == "yes" ) {
                $content .= '<div class="donation-row custom-amount-row">';
                $content .= '<div class="custom-amount-label">'.__("Choose an amount yourself","poppyz").'</div>';
				$content .= '<div class="custom-amount-input-radio-button"><input type="radio" name="'.PPY_PREFIX.'donation_amount" class="'.PPY_PREFIX.'donation_amount" id="donation-custom-amount-radio-button" value="others"/></div>';
                $content .= '<div class="custom-amount-input"><div class="price-with-currency"><div class="price-currency">€</div><input type="textbox" name="'.PPY_PREFIX .'donation_amount_custom" id="donation-custom-amount" class="short input-price" placeholder="0,-"></div></div>';

                $content .= "</div>";
			}
		}
		$content .= '<div id="purchase-total" >';
		$content .= '<div class="purchase-total-label"><strong>'.__("Total","poppyz").'</strong></div>';
		$content .= '<input type="hidden" name="tier_price" id="tier_price" value="'.$price.'"/>';
		$content .= '<input type="hidden" name="total" id="total" value="'.$price.'"/>';
		$content .= '<div class="purchase-total-value" id="purchase-total-value">'.Poppyz_Core::format_price($price).'</div>';
		$content .= "</div>";
		$included_text = '';

		$tax_amount =  Poppyz_Core::get_fixed_tax_amount( $price, $tier_id, Poppyz_Core::get_price_tax( $tier_id ) );
		$tax_rate = (float)Poppyz_Core::get_tax_rate( $tier_id, null );
		$total_with_tax = Poppyz_Core::price_with_tax( $price, $tier_id, null );
		$final_tax = Poppyz_Core::format_number( $tax_amount );
        $vat_info = Poppyz_Core::format_number($tax_rate) .'%';


		if ( Poppyz_Invoice::is_vatshifted() ) {
			$final_tax = __( '0 (VAT shifted.)','poppyz' );
		} else if ( Poppyz_Invoice::is_vat_out_of_scope() ) {
			$final_tax = __( '0 (VAT out of scope.)' ,'poppyz');
		} else {
			$included_text = __( ' - VAT is included' ,'poppyz');
		}

		$bij =  ( Poppyz_Core::get_price_tax( $tier_id ) == 'inc' ) ? $included_text : '';

		$content .= "<div class='total-with-tax-container' >";
		$content .= "<span id='total-with-tax-label' >".__("Amount incl. ", "poppyz") . $vat_info."  ". __('VAT', 'poppyz'). "</span>";
		$content .= "<span id='total-with-tax' > " .Poppyz_Core::format_price($price_with_tax) . "</span>";
		$content .= "</div>";
		$content .= "</div>";

		$userId = is_user_logged_in() ? get_current_user_id() : null;
		$bundledProducts = get_post_meta( $tier_id, PPY_PREFIX . 'tier_bundled_products',  true );

        $hideCheckbox = ( $bundledProducts == "yes" ) ? "hide-additional-product-checkbox" : "" ;
		$expandContent = ( $bundledProducts == "yes" ) ? "expand-additional-product-content" : "" ;


        $content .= "<div class='additional-product-container'>";

		for($i=1;$i<6;$i++) {
			$additionalProductId = get_post_meta($tier_id, PPY_PREFIX . 'tier_additional_product' . $i, true);
			$eproductVersionValid = (defined('PPYV_VERSION') && version_compare(PPYV_VERSION, '1.4.8.5') >= 0) ? true : false;
			$productType = get_post_type( $additionalProductId );
			$allowEproducts =(  $productType == "ppyv_product" && $eproductVersionValid == false ) ? false : true;

			if ($additionalProductId && $allowEproducts == true) {
				$productTitle = get_the_title($additionalProductId);
				$additionalProductPrice = get_post_meta($additionalProductId, PPY_PREFIX . 'tier_price', true);
				$tax_amount_additional_product =  Poppyz_Core::get_fixed_tax_amount( $additionalProductPrice, $additionalProductId, Poppyz_Core::get_price_tax( $additionalProductId ) );

				$totalWithTax = Poppyz_Core::price_with_tax($additionalProductPrice, $additionalProductId, $userId);
				$imageUrl = get_post_meta($tier_id, PPY_PREFIX . 'tier_additional_product_image_url' . $i, true);
				$hasImage = (isset($imageUrl) && $imageUrl != "") ? "has-image" : "no-image";
                $content .= '<div class="additional-product-row-'.$i.' additional-product-row" id="additional-product-row-'.$additionalProductId.'">';
                $content .= '<div class="additional-product-checkbox '.$hideCheckbox.'"><label class="checkbox-container">';
                $content .= '<input type="checkbox" name="additional_products[]" class="child-additional-product additional-product-checkbox-'.$i.'" value="'.$additionalProductId.'" id="'.$additionalProductId.'" data-price="'.$additionalProductPrice.'" data-price-with-tax="'.$totalWithTax.'" />';
				$content .= "<input type='hidden' class='vat' value='".$tax_amount_additional_product."'/>";

				$price_with_tax_additional_product = Poppyz_Core::price_with_tax( $additionalProductPrice, $additionalProductId, null );
				$price_with_tax_additional_product = ( Poppyz_Core::get_price_tax( $additionalProductId ) != 'inc' ) ? $price_with_tax_additional_product : $additionalProductPrice; // if tax is included, it will display the tier price only, but if not, it will display the price with tax.
				$content .= "<input type='hidden' class='price_with_tax' id='price_with_tax_".$additionalProductId."' value='".$price_with_tax_additional_product."'/>";
                $content .= '<span class="checkmark"></span></label></div>';
                $content .= '<div class="additional-product additional-product-'.$i.' '.$hasImage.' '.$expandContent.'" >';
                $content .= '<div class="fields">';
				$additionalProductPrice = ( $bundledProducts != "yes" ) ? ' (' .Poppyz_Core::format_price($additionalProductPrice) . ')' : "";
                $content .= '<h5>'. $productTitle . $additionalProductPrice .'</h5>';
                $content .= get_post_meta($tier_id,  PPY_PREFIX . 'tier_additional_product_description' . $i, true);
                $content .= '</div></div>';

                if (isset($imageUrl) && $imageUrl != "") {
                    $content .= '<div class="additional-product-image-wrapper">';
                    $content .= '<img src="'.$imageUrl.'" class="additional-product-image">';
                    $content .= '</div>';
                }
                $content .= '</div>';
                                                        ;
			}
		}
		$content .= "</div>";
		$content .= $buy_button;
		$content .= "</div></div></form>";
		return $content; //tier-section
	}



	public function lesson_modules( $lesson_id = null ) {
		if ($lesson_id == null) {
			$lesson_id = get_the_ID();
		}
		$modules = $this->get_modules_by_lesson( $lesson_id );
		if ( empty( $modules ) ) return false;
		return implode(', ', $modules );
	}

	public static function get_lessons_read( $user_id ) {
		return get_user_meta( $user_id,  PPY_PREFIX . 'lessons_read', true );
	}

	public static function get_lessons_read_with_course( $user_id ) {
		$read_lessons = [];

		$lessons_read = Poppyz_Core::get_lessons_read( $user_id );
		if (!$lessons_read) return $read_lessons;

		$ppy_sub = new Poppyz_Subscription();
		$subs = $ppy_sub->get_subscriptions_by_user( $user_id );

		foreach ( $subs as $sub ) {
			$course_id = $sub->course_id;
			$lessons = $ppy_sub->get_lessons_by_subscription($course_id, $user_id);
			foreach ($lessons as $lesson) {
				foreach ($lesson as $v) {
					if ( !in_array( $v, $lessons_read ) ) {
						$lesson = array_diff( $lesson, [$v] ); //remove unread lesson
					}
				}
				if (isset($read_lessons[$course_id]))
					array_merge($read_lessons[$course_id], $lesson);
				else
					$read_lessons[$course_id] = $lesson;
			}

		}
		return $read_lessons;
	}

	public static function has_read_lesson( $lesson_id,  $user_id ) {
		$lessons_read = Poppyz_Core::get_lessons_read( get_current_user_id() );
		return is_array( $lessons_read ) && in_array( $lesson_id, $lessons_read );
	}

	public static function read_lesson( $lesson_id, $user_id ) {
		$meta = get_user_meta( $user_id,  PPY_PREFIX . 'lessons_read', true );
		// Do some defensive coding - if it's not an array, set it up
		if ( ! is_array( $meta ) ) {
			$meta = array();
		}
		if ( !in_array( $lesson_id, $meta ) ){
			$meta[] = $lesson_id;
			update_user_meta( $user_id, PPY_PREFIX . 'lessons_read',  $meta );
		}
	}
	public static function unread_lesson( $lesson_id, $user_id ) {
		$meta = get_user_meta( $user_id, PPY_PREFIX . 'lessons_read', true );
		// Find the index of the value to remove, if it exists
		$index = array_search( $lesson_id, $meta );
		// If an index was found, then remove the value
		if ($index !== FALSE) {
			unset( $meta[ $index ] );
			// Write the user meta record with the removed value
			update_user_meta( $user_id, PPY_PREFIX . 'lessons_read', $meta );
		}
	}

	public static function generate_payment_title($subscription_id, $price, $invoice = true) {
		$ppy_subs = new Poppyz_Subscription();
		$subs = $ppy_subs->get_subscription_by_id( $subscription_id );
		$course_id = $subs->course_id;
		$tier_id = $subs->tier_id;
		$payment_method = $subs->payment_method;
		$user_id = $subs->user_id;
		$initial_price = (float) $subs->initial_amount;
		$title = get_the_title( $course_id ) . ' - ' . get_the_title( $tier_id );
		global $ppy_lang;
		if ( $initial_price > 0) {
			$initial_amount_with_tax = Poppyz_Core::price_with_tax( $initial_price, $tier_id, $user_id );
			$title .= ' ' . sprintf( __( "Initially %s, and then " ,'poppyz'),  Poppyz_Core::format_price( $initial_amount_with_tax ) );
		}
		$title .= self::get_payment_method_text( $tier_id, $payment_method, $price, $invoice);
		return $title;
	}

	public static function get_payment_method_text($tier_id, $payment_method, $price, $invoice = true) {
		$text = '';
		global $ppy_lang;
		if ( $payment_method == 'plan' ) {
			$payment_frequency =  get_post_meta( $tier_id, PPY_PREFIX . 'payment_frequency', true );
			$payment_number =  get_post_meta( $tier_id, PPY_PREFIX . 'number_of_payments', true );
			$payment_timeframe =  get_post_meta( $tier_id, PPY_PREFIX . 'payment_timeframe', true );

			$initial_price =  (float)get_post_meta( $tier_id, PPY_PREFIX . 'tier_initial_price', true );
			if ($initial_price > 1) {
				$payment_number = $payment_number - 1;
			}

			if ( $payment_timeframe == 'days' ) {
				$payment_timeframe_text =  __('day','poppyz');
				if ( $payment_frequency > 1 ) {
					$payment_timeframe_text =  __('days','poppyz');
				}
			} elseif ( $payment_timeframe == 'years' ) {
				$payment_timeframe_text =  __('year','poppyz');
				if ( $payment_frequency > 1 ) {
					$payment_timeframe_text =  __('years','poppyz');
				}
			} else {
				$payment_timeframe_text =  __('month','poppyz');
				if ( $payment_frequency > 1 ) {
					$payment_timeframe_text =  __('months','poppyz');
				}
			}
			$payment_frequency_text = ( $payment_frequency == 1 ) ? '' : $payment_frequency;

			$text .= ' : ';

			if ( !$invoice ) {
				$text .= __( 'Payment Plan' ,'poppyz') . ': ';
			}

			$every =  sprintf( __( 'per %s' ,'poppyz') , $payment_frequency_text ) . ' ' . $payment_timeframe_text;

			$text .= Poppyz_Core::format_price( $price, '€' ) . ' ' . $every . ' (' . $payment_number . 'x)';
		} elseif ( $payment_method == 'membership' ) {
			$membership_interval =  get_post_meta( $tier_id, PPY_PREFIX . 'membership_interval', true );
			$membership_timeframe =  get_post_meta( $tier_id, PPY_PREFIX . 'membership_timeframe', true );

			if  ( $membership_timeframe == 'months' ) {
				$membership_timeframe =  __( 'month' ,'poppyz');
				if ( $membership_interval > 1 ) {
					$membership_timeframe =  __( 'months' ,'poppyz');
				}
			}
			else if  ( $membership_timeframe == 'days' ) {
				$membership_timeframe =  __( 'day' ,'poppyz');
				if ( $membership_interval > 1 ) {
					$membership_timeframe =  __( 'days' ,'poppyz');
				}
			}
			else if  ( $membership_timeframe == 'years' ) {
				$membership_timeframe =  __( 'year' ,'poppyz');
				if ( $membership_interval > 1 ) {
					$membership_timeframe =  __( 'years' ,'poppyz');
				}
			}
			$membership_interval_text = ( $membership_interval == 1 ) ? '' : $membership_interval;

			$text .= ' : ';
			if ( !$invoice ) {
				$text .= __( 'Membership' ,'poppyz') .  ': ' ;
			}
			$every =  sprintf( __( 'per %s' ,'poppyz') , $membership_interval_text ) . ' ' . $membership_timeframe;
			$text .= Poppyz_Core::format_price( $price, '€' ) . ' ' . $every;
		} else {
			$text = ' ' . Poppyz_Core::format_price( $price, '€' );
		}
		return $text;
	}

	public static function get_price_description( $tier_id, $price ) {
		$title = '';
		$payment_method = get_post_meta( $tier_id, PPY_PREFIX . 'payment_method', true);
		$title .= self::get_payment_method_text( $tier_id, $payment_method, $price );
		return $title;
	}
	public static function generate_term_name( $term_name ) {
		/*$term = term_exists( $term_name, PPY_LESSON_CAT );
		if ( 0 !== $term && null !== $term ) {
			$term_name = $term_name . '-' . $count;
			$count++;
			$term_name = Poppyz_Core::generate_term_name( $term_name, $count );
		}*/
		$term_exists = term_exists( $term_name, PPY_LESSON_CAT );
		$slug = $term_name;
		if (  0 !== $term_exists && null !== $term_exists ) {
			$num = 2;
			do {
				$alt_slug = $slug . "-$num";
				$num++;
				$term = term_exists( $alt_slug, PPY_LESSON_CAT );
			} while (  0 !== $term && null !== $term );
			$slug = $alt_slug;
		}

		return $slug;
	}

	public static function get_modules_by_course( $course_id = null ) {
		$args = array(
			'taxonomy' => PPY_LESSON_CAT,
			'orderby' =>  'meta_value_num',
			'meta_key' =>  'menu_order',
			'order' =>  'ASC',
			'hide_empty' =>  false,
			'hierarchical' =>  false,
			'parent' =>  0,
		);

		if ( $course_id ) {
			$args['meta_query'] = array(
				array(
					'key' => 'ppy_module_course',
					'value' => $course_id,
					'compare'		=> '='
				)
			);
		}

		return get_terms( $args );
	}

	public static function delete_modules_by_course ( $course_id ) {
		$terms = Poppyz_Core::get_modules_by_course( $course_id );
		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
			foreach ( $terms as $term ) {
				wp_delete_term( $term->term_id, PPY_LESSON_CAT );
			}
		}
	}

	public static function get_modules_by_lesson( $lesson_id = null ) {
		return wp_get_post_terms( $lesson_id, PPY_LESSON_CAT, array( 'fields' => 'names' ) );
	}

	public static function progress_button( $lesson_id = null, $user_id = null, $content = '' ) {
		global $ppy_lang;
		if ( !$lesson_id ) {
			$lesson_id = get_the_ID();
		}
		if ( !$user_id ) {
			$user_id = get_current_user_id();
		}
		if ( isset( $_GET['read'] )  ) Poppyz_Core::read_lesson( $lesson_id,$user_id );
		if ( isset( $_GET['unread'] )  ) Poppyz_Core::unread_lesson( $lesson_id, $user_id );
		if ( isset( $_GET['read']) || isset( $_GET['unread']) ) {
			?>
            <script>
                $(window).bind('load', function()
                {
                    $('html, body').animate({
                        scrollTop: $("#progress-button").offset().top
                    }, 2000);
                });
            </script>
			<?php
		}
		if ( !Poppyz_Core::has_read_lesson( $lesson_id, $user_id ) ) {
			$link = '<a id="progress-button" href="' . get_permalink() . '/?read=1#progress-button" class="mark-read et_pb_button">' . __( "Mark as completed" ,'poppyz') .  '</a>';
		} else {
			$link = '<a id="progress-button" href="' . get_permalink(). '/?unread=1#progress-button" class="mark-read et_pb_button">' . __( "Mark as not completed" ,'poppyz') .  '</a>';
		}
		$content = str_replace( '[progress-button]', $link, $content );
		$course_id = Poppyz_Core::get_course_by_lesson( $lesson_id );

		if ( get_post_meta( $course_id, PPY_PREFIX . 'lesson_mark_button', true ) === '1' ) {
			$content .=  '<div id="progress-button" class="et_pb_row">' . $link . '</div>';
		}
		return $content;
	}

	public static function progress_list( $course_id ) {
		if ( get_post_meta( $course_id, PPY_PREFIX . 'lesson_progress_list', true ) === '1' ) {
			return '<div class="et_pb_row">' . do_shortcode('[lesson-list display="progress"]') . '</div>';
		}
	}

	public static function get_license_data() {
		$license_key = Poppyz_Core::get_option('license_key');
		if (!$license_key) return false;
		$response = Poppyz_Subscription::check_license( $license_key );
		// make sure the response came back okay
		if ( is_wp_error( $response ) ){
			return false;
		}

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		if ( isset( $license_data->license ) ) {
			return [
				'status' => $license_data->license,
				'expiration_date' =>  ( $license_data->expires == 'lifetime')  ? 'lifetime' : date('Y-m-d', strtotime( $license_data->expires ) ),
			];
		}
		return false;
	}
	public static function get_license_status() {
		return Poppyz_Core::get_option('license_status');
	}
	public static function license_check() {
		$license_status = Poppyz_Core::get_license_status();
		global $ppy_lang;
		if ( $license_status == 'valid' ) return 'valid';
		if ( $license_status == 'expired' ) {
			if ( is_admin() ) {
				die( __( 'Poppyz\'s license has expired. Please renew and re-activate the license in Poppyz Settings or contact support@simonelevie.nl.' ,'poppyz') );
			} else {
				die( __( 'Unfortunately it is currently not possible to purchase the product. Contact the owner of the site.' ,'poppyz') );
			}
		} else {
			die( __( 'Poppyz\'s license is invalid. Please activate the license in Poppyz Settings' ,'poppyz') );
		}
	}
	public static function column_exists( $column_name, $table = null) {
		global $wpdb;
		$table_name = ($table) ? $table : $wpdb->prefix . "course_subscriptions";

		$column = $wpdb->get_results( $wpdb->prepare(
			"SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s AND COLUMN_NAME = %s ",
			DB_NAME, $table_name, $column_name
		) );
		if ( ! empty( $column ) ) {
			return true;
		}
		return false;
	}

	public static function table_exists( $table_name ) {
		global $wpdb;
		$query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );

		return ( strtolower( $wpdb->get_var( $query ) ) == strtolower( $table_name ) );
	}

	public static function return_action( $action, $value ) {
		ob_start();
		do_action( $action, $value );
		$return = ob_get_contents();
		ob_end_clean();
		return $return;
	}

	public static function get_remaining_payments_amount ( $tier = null, $start_date = null, $end_date = null ) {
		global $wpdb;
		$ppy_sub = new Poppyz_Subscription();

		$tier_sql =  ( $tier ) ? ' AND tier_id =' . $tier : '';
		$subscriptions = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}course_subscriptions WHERE payment_method = 'plan' AND payments_made > 0 AND payment_type <> 'manual' AND (payment_mode = 'Live' OR payment_mode IS NULL)" . $tier_sql );
		$total_future_earnings = 0;
		foreach ( $subscriptions as $subs ){

			$subs_id = $subs->id;
			$tier_id = $subs->tier_id;
			$user_id = $subs->user_id;
			$subscription_date = $subs->subscription_date;

			$payments_made = $subs->payments_made;
			$number_of_payments = get_post_meta( $tier_id, PPY_PREFIX . 'number_of_payments', true );

			if ($payments_made >= $number_of_payments) continue; //fully paid


			$interval = get_post_meta( $tier_id, PPY_PREFIX . 'payment_frequency', true );
			$timeframe = get_post_meta( $tier_id, PPY_PREFIX . 'payment_timeframe', true );

			$remaining_payment = $number_of_payments - $payments_made;
			$price = $ppy_sub->compute_discounted_price( $subs_id );
			$total_with_tax = Poppyz_Core::price_with_tax( $price, $tier_id, $user_id );
			for ($i=0; $i <= $remaining_payment; $i++) {
				$frequency = $interval * $i;
				$target_date =  strtotime( $subscription_date . ' + ' . $frequency . ' ' . $timeframe );
				/*echo ($start_date) . ' - '.  date('Y-m-d', $target_date) .' - ' .  ($end_date) ;
				echo'<br /><br />';*/
				if ( ( $start_date && strtotime($start_date) > $target_date )|| ( $end_date && strtotime($end_date) < $target_date ) ) continue;
				$total_future_earnings += $total_with_tax;

			}


		}

		return Poppyz_Core::format_price( $total_future_earnings );
	}

	public function tier_slots_remaining( $atts = '' ) {
		$atts = shortcode_atts( array(
			'id' => false,
		), $atts, 'tier-slots' );
		if ( !$atts['id'] ) return null;
		$num_subs = Poppyz_Subscription::get_subscription_total_by_tier( $atts['id'] );
		$subs_limit = get_post_meta( $atts['id'], PPY_PREFIX . 'tier_subs_limit', true );
		if ( !$subs_limit ) return '&infin;'; //no limit
		$remaining = $subs_limit - $num_subs;
		return ( $remaining > 0 ) ? $remaining : 0;
	}

	public function user_info( $atts = '' ) {
		$atts = shortcode_atts( array(
			'field' => 'first_name',
			'id' => false
		), $atts, 'user-info' );
		if ( !$atts['id'] ) {
			$id = get_current_user_id();
		} else {
			$id = $atts['id'];
		}
		$user = get_userdata($id);
		if ($user) {
			return $user->{$atts['field']};
		}
		return null;
	}

	public static function get_tier_upsells( $tier_id ) {
		$upsells =  get_post_meta( $tier_id, PPY_PREFIX . 'tier_upsells' );
		if ($upsells){
			return self::check_empty_array($upsells);
		}
		return false;
	}

	public static function check_empty_array( $array ) {
		// Filter the array to keep only non-empty values
		$nonEmptyValues = array_filter($array, function ($value) {
			return !empty($value);
		});

		// Return the array if it's not empty, otherwise return false
		return empty($nonEmptyValues) ? false : $nonEmptyValues;
	}

	public static function get_tier_upsell_description( $tier_id ) {
		return get_post_meta( $tier_id, PPY_PREFIX . 'tier_upsell_description', true );
	}

    public static function get_tier_additional_products( $tier_id, $ids_only = false ) {
        $additional_products = array();
        for($i=1;$i<6;$i++) {
            if (empty(get_post_meta( $tier_id, PPY_PREFIX . 'tier_additional_product' . $i, true ))) continue;
            $additional_products[] = array(
                'tier' => get_post_meta( $tier_id, PPY_PREFIX . 'tier_additional_product' . $i, true ),
                'image' => get_post_meta( $tier_id, PPY_PREFIX . 'tier_additional_product_image' . $i, true ),
                'description' => get_post_meta( $tier_id, PPY_PREFIX . 'tier_additional_product_description' . $i, true ),
            );
        }
        if ($ids_only) {
            return array_unique(array_column($additional_products, 'tier'));
        }
        return $additional_products;
    }

	public static function run_updates() {

		if ( Poppyz_Core::get_option('version') <= '1.2.2.5') {
			//ob_start();                    // start buffer capture
			//echo 'start_var_log';
			Poppyz_Core::ppy_fix_mollie_charges();
			Poppyz_Core::save_option('version', PPY_VERSION );
			//echo 'end_varlog';
			//$contents = ob_get_contents(); // put the buffer into a variable
			//ob_end_clean();                // end capture
			//error_log( $contents );
		}
		if ( Poppyz_Core::get_option('version') <= '1.6.0') {
			Poppyz_Core::ppy_remove_standard_payments_subscriptions();
			Poppyz_Core::save_option('version', PPY_VERSION );
		}
	}

	public static function ppy_fix_mollie_charges() {
		//if (!isset( $_GET[ 'jl2FRf*safkx22' ] ) || !is_super_admin() ) return;

		global $wpdb;
		$sql = $wpdb->prepare(  "SELECT *
                FROM {$wpdb->prefix}course_subscriptions
                WHERE subscription_date > '%s'
                AND payment_method = '%s'
                AND mollie_subscription_id  <> ''
                AND mollie_customer_id  <> ''
                ", '2020-12-27 00:00', 'plan'
		);
		$results =  $wpdb->get_results( $sql );
		if (!$results) return false;

		$payment = new Poppyz_Payment();
		$payment->_initialize_mollie();
		$mo_key = Poppyz_Core::get_option( 'mo_api_key' );

		if ( empty( $mo_key ) )  {
			return false;
		}
		foreach ( $results as $sub ) {
			//echo $sub->subscription_date . ' -- ' . $sub->id . ':' . $sub->tier_id;
			//echo '<hr />';
			self::process_mollie_fix($sub->mollie_customer_id, $sub->mollie_subscription_id, $sub->tier_id, $payment );
		}
		//process_mollie_fix('cst_VjNUePmeqD', 'sub_udJrgeQQDw', '2044' );

	}

	public static function process_mollie_fix($customer_id, $subscription_id, $tier_id, $payment) {

		$times = (int)get_post_meta( $tier_id, PPY_PREFIX .  'number_of_payments', true );

		try {
			$customer = $payment->mollie->customers->get($customer_id);
			$subscription = $customer->getSubscription($subscription_id);
			$sub_times = $subscription->times;

			//echo " <p>Tier Number of Payments: $times </p><p>Subscription Number of Payments: $sub_times </p>";

			if ( $times == $sub_times && $subscription->status == 'active' ) {
				$subscription->times = $times - 1;
				$updatedSubscription = $subscription->update();
				//echo "<p>Changed sub: <strong> $subscription->id </strong> to new charges: <strong>$subscription->times</strong></p>";
			} else {
				//echo "<p> Skipped </p>";
			}
			//echo '<hr />';
		} catch (Exception $e) {
			//echo $e->getMessage();
		}
	}


	//function to fix the bug that created mollie subscriptions for standard payments
	public static function ppy_remove_standard_payments_subscriptions() {
		global $wpdb;
		$sql = $wpdb->prepare(  "SELECT *
                FROM {$wpdb->prefix}course_subscriptions
                WHERE payment_method = '%s'
                AND mollie_subscription_id  <> ''
                AND mollie_customer_id  <> ''
                ", 'standard'
		);
		$results =  $wpdb->get_results( $sql );
		if (!$results) return false;

		$payment = new Poppyz_Payment();
		$payment->_initialize_mollie();
		$mo_key = Poppyz_Core::get_option( 'mo_api_key' );

		if ( empty( $mo_key ) )  {
			return false;
		}
		foreach ( $results as $sub ) {
			//echo "<p>Changed sub: <strong> $sub->id </strong> customer: <strong> $sub->mollie_customer_id </strong> / mollie_sub: <strong>$sub->mollie_subscription_id</strong></p>";
			try {
				$customer = $payment->mollie->customers->get( $sub->mollie_customer_id );
				$subscription = $customer->getSubscription($sub->mollie_subscription_id);
				if ( $subscription && $subscription->isActive()) {
					$customer->cancelSubscription($sub->mollie_subscription_id);
				}
				//echo "<p>Cancelled: $sub->mollie_subscription_id</p>";
			} catch ( Exception $e ) {
				//echo $e->getMessage();
				error_log( $e->getMessage() );
			}

			//delete the mollie subscription id
			$wpdb->update(
				$wpdb->prefix . 'course_subscriptions',
				array('mollie_subscription_id' => '' ),
				array('id' => $sub->id),
				array('%s'),
				array('%d')
			);
			//error_log("<p>Cancelled mollie_sub: $sub->mollie_subscription_id</p>");
			//error_log("<p>Cancelled customer sub: $sub->mollie_customer_id</p>");
		}
	}

	public static function log($message, $type = 'notify', $error = false) {
		$debug_enabled = Poppyz_Core::get_option('enable_debugging');

		//if it's an error and debug is disabled, write to the error log of PHP
		if ($error && !$debug_enabled ) {
			error_log($message);
			return;
		}
		//return if not an error on logging is disabled
		if (!$debug_enabled) return false;

		//proceed with plugin log
		$upload_dir = wp_upload_dir();
		$upload_path = $upload_dir['basedir'] . '/poppyz/';

		if( !file_exists( $upload_path ) ) return false;

		$type = ( $type == 'notify') ? '' : '<' . $type . '> ';
		error_log("[" . date("F j, Y, g:i a") . "]: " . $type . $message . "\n", 3, $upload_path . 'debug.log' );
	}

	public static function isEU($user_id) {
		$user_country = get_user_meta($user_id, 'ppy_country', true) ?: 'NL';
		$eu_countrycodes = array(
			'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'EL',
			'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV',
			'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'
		);
		return ( in_array( $user_country, $eu_countrycodes ) );
	}

	public static function  ppy_get_invoice_mb( $product_id, $user_id ) {
		return get_user_meta( $user_id, PPYV_PREFIX . 'mb_invoice_' . $product_id , true );
	}
	public static function ppy_get_invoice( $product_id, $user_id ) {
		$args = array(
			'meta_query' => array(
				array(
					'key' => PPY_PREFIX . 'invoice_product_id',
					'value' => $product_id
				),
				array(
					'key' => PPY_PREFIX . 'invoice_client_number',
					'value' => $user_id
				)
			),
			'post_type' => PPY_INVOICE_PT,
			'orderby' => 'ID', //get the latest invoice for payment plans
			'order' => 'DESC',
			'post_status' => 'paid',
			'posts_per_page' => 1
		);

		$invoice = get_posts($args);
		if ( !empty( $invoice ) ) {
			foreach ( $invoice as $post ) {
				setup_postdata( $post );
				return $post->ID;
			}
		}
		wp_reset_postdata();
		return false;
	}



	/**
	 * @param $tier_id
	 * @param $user_id
	 */
	public static function setScheduledDonation($tier_id, $user_id)
	{
		$scheduleVoluntaryContribution = get_post_meta($tier_id, PPY_PREFIX . "tier_schedule_voluntary_contribution", true);
		if (!empty($scheduleVoluntaryContribution) && $scheduleVoluntaryContribution == "yes") {
			$userScheduledDonation = get_user_meta($user_id, PPY_PREFIX . "schedule_voluntary_contribution_tier_id", true);
			if (!empty($userScheduledDonation)) {
				array_push($userScheduledDonation, $tier_id);
			} else {
				$userScheduledDonation = array($tier_id);
			}
			update_user_meta($user_id, PPY_PREFIX . "schedule_voluntary_contribution_tier_id", $userScheduledDonation);
			update_user_meta($user_id, PPY_PREFIX . "tier_date_purchased".$tier_id, date("Y/m/d"));
		}
		//return $userScheduledDonation;
	}

    public static function getTotalSubscription( $tierId, $userId ) {
		global $wpdb;
		$sql = $wpdb->prepare(  "SELECT id
                FROM {$wpdb->prefix}course_subscriptions 
                WHERE tier_id  = %s and user_id = %s",
			$tierId,
			$userId
		);

		$wpdb->get_results( $sql );

        return $wpdb->num_rows;
	}

    public static function sendEmailDonation() {
		global $wpdb;

		$sql = $wpdb->prepare(  "SELECT *
                FROM {$wpdb->prefix}course_subscriptions s 
                INNER JOIN {$wpdb->prefix}postmeta pm ON (s.tier_id = pm.post_id)
                WHERE pm.meta_key  = %s and pm.meta_value = %s",
                'ppy_tier_schedule_voluntary_contribution',
                'yes'
		);
		$subscription =  $wpdb->get_results( $sql );

        foreach( $subscription as $sub) {
            $tierId = $sub->tier_id;
			$userId = $sub->user_id;
			$subCount = Poppyz_Core::getTotalSubscription( $tierId, $userId );

			$tierIds = get_user_meta($userId, PPY_PREFIX . "schedule_voluntary_contribution_tier_id", true);  // all the tier ID of the schedule voluntary contribution under the user.

			$numberOfDaysAfterPurchase = get_post_meta( $tierId, PPY_PREFIX .  'number_of_days_after_purchase',  true );
			$dateToSend = strtotime(date('Y-m-d', strtotime($sub->subscription_date. ' + '.$numberOfDaysAfterPurchase.' days')));
            $today = strtotime(date('Y-m-d'));

			$tierExist = ( !empty($tierIds) && in_array($sub->tier_id, $tierIds) ) ? true : false;

            //for free tier only
            if ( $today >= $dateToSend && $tierExist == true && $sub->amount == 0 && $subCount < 2) {
				if ( empty(get_user_meta( $userId, PPY_PREFIX . $sub->id. '_schedule_donation_email_sent', true )) ) {

					$subject = get_post_meta( $tierId, PPY_PREFIX .  'donation_email_subject',  true );
					$purchase_url =  get_permalink( Poppyz_Core::get_option( 'purchase_page' ) );
					if (  get_option( 'permalink_structure' ) ) {
						$purchase_url .= 'order-id/';
					} else {
						$purchase_url .= '&order-id=';
					}
					$tierLink = '<a class="url" href="' .  $purchase_url . get_post_meta( $tierId, PPY_PREFIX . 'tier_slug', true )  . '">' . $purchase_url . get_post_meta( $tierId, PPY_PREFIX . 'tier_slug', true ) . '</a>';
					$tierLink = __("Please click this link to proceed with the donation. " .$tierLink, "poppyz");

					//get the content message
					$message = get_post_meta( $tierId, PPY_PREFIX . "tier_donation_email_content",  true );

                    if ( !empty($message) ) {
						$message = Poppyz_Core::the_content( $message );
					}

					$tier_link = (isset($sub->course_id)) ? get_permalink( $sub->course_id ) : "";
					$tier_name = (isset($sub->tier_id)) ? get_the_title( $sub->tier_id ) : "";
					$course_name = (isset($sub->course_id)) ? get_the_title( $sub->course_id ) : "";
					$course_link = (isset($sub->course_id)) ? get_permalink( $sub->course_id) : "";
					$payments_made =  (isset($sub->payments_made)) ? $sub->payments_made ?: 1 : "";
					$invoice_id = Poppyz_Invoice::get_invoice_by_subscription( $sub->user_id, $payments_made );
					$invoice_link = ( $invoice_id ) ? Poppyz_Invoice::get_invoice_url( $invoice_id ) . '&download' : '';
					$user_info = get_userdata( $sub->user_id );
					$tier_purchase_url = Poppyz_Core::get_tier_purchase_url($sub->tier_id);
					$account_link =  get_permalink( Poppyz_Core::get_option( 'account_page' ) );

					$search = array( '[account-link]', '[tier-link]', '[tier]', '[course]', '[course-link]', '[first-name]', '[last-name]', '[user-email]','[customer-id]',  '[tier-purchase-url]' );
					$replace = array( $account_link,  $tier_link, $tier_name, $course_name, $course_link, $user_info->first_name, $user_info->last_name, $user_info->user_email, $sub->user_id,   $tier_purchase_url );
					$message = str_replace( $search, $replace, $message );

                    $data = array("subject" => $subject, "email_content" => $message );

					Poppyz_Core::send_notification_email( $sub->id, $userId, $data, 'scheduled_donation_notification_email' );
					update_user_meta( $userId, PPY_PREFIX . $sub->id . '_schedule_donation_email_sent', 1 );
				}
			}
		}

        //return $tierIdArray;
	}

    public static function set_additional_products_cookie( $products ) {
		if ( !empty($products) ) {
			setcookie('ppy_additional_products', implode(',', $products), time() + 3600, '/');
		} else {
			Poppyz_Core::remove_additional_products_cookie();
		}

    }

	public static function set_donation_amount_cookie( $donation_amount ) {
		if ( !empty($donation_amount) ) {
			setcookie('ppy_donation_amount', $donation_amount, time() + 3600, '/');
		} else {
			Poppyz_Core::remove_donation_amount_cookie();
		}
	}

	public static function set_donation_amount_custom_cookie( $donation_amount_custom ) {
		if ( !empty($donation_amount_custom) ) {
			setcookie('ppy_donation_amount_custom', $donation_amount_custom, time() + 3600, '/');
		} else {
			Poppyz_Core::remove_donation_amount_custom_cookie();
		}
	}

    public static function get_additional_products_cookie() {
        if ( isset($_COOKIE['ppy_additional_products']) ) {
			return explode(',', $_COOKIE['ppy_additional_products']);
		} else {
            return false;
		}
    }

    public static function remove_additional_products_cookie(){
        if ( isset( $_COOKIE['ppy_additional_products' ]) ) {
            unset( $_COOKIE['ppy_additional_products'] ) ;
            setcookie('ppy_additional_products', '', time() - 3600, '/');
        }
    }

	public static function get_donation_amount_cookie() {
		if ( isset($_COOKIE['ppy_donation_amount']) ) {
			return $_COOKIE['ppy_donation_amount'];
		} else {
			return false;
		}
	}
	public static function get_donation_amount_custom_cookie() {
		if ( isset($_COOKIE['ppy_donation_amount_custom']) ) {
			return $_COOKIE['ppy_donation_amount_custom'];
		} else {
			return false;
		}
	}
	public static function remove_donation_amount_cookie(){
		if ( isset( $_COOKIE['ppy_donation_amount' ]) ) {
			unset( $_COOKIE['ppy_donation_amount'] ) ;
			setcookie('ppy_donation_amount', '', time() - 3600, '/');
		}
	}
	public static function remove_donation_amount_custom_cookie(){
		if ( isset( $_COOKIE['ppy_donation_amount_custom' ]) ) {
			unset( $_COOKIE['ppy_donation_amount_custom'] ) ;
			setcookie('ppy_donation_amount_custom', '', time() - 3600, '/');
		}
	}

    public static function is_tier_bundled_products( $tier_id ) {
        return get_post_meta( $tier_id, PPY_PREFIX . "tier_bundled_products", true ) === 'yes';
    }
	public static function get_overdue_list( $start_date, $end_date, $all=false ) {
		$ppy_payment = new Poppyz_Payment();
		$output = "";
		$total_remaining_payments = 0;
		$overdue_payments = Poppyz_Statistics::get_overdue_payments($start_date, $end_date, $all);
        $overdue_data = array();
		foreach ($overdue_payments as $sub) {
			$payment_details = Poppyz_Statistics::statistics_remaining_payments_details($sub);
			$count_payment = 0;
			$parent_id = 0;

			if ( $sub->payment_method == "plan" ) {
				$times = $payment_details['times'];
			} elseif( $sub->payment_method == "membership" && $payment_details['timeframe'] == "months" ) {
				$times = 12;
			} elseif( $sub->payment_method == "membership" && $payment_details['timeframe'] == "days" ) {
				$times = 360;
			}

			if ($payment_details != false) {
				$amount = 0;
				for ($y = 0; $y < $times; $y++) {
					$interval = get_post_meta($sub->tier_id, PPY_PREFIX . 'payment_frequency', true);
					$frequency = $payment_details['interval'] * $y;
					$due_date = strtotime($payment_details['subscription_date'] . ' + ' . $frequency . ' ' . $payment_details['timeframe']);
					$due_date_string = date_i18n(get_option('date_format'), $due_date);

					$initial_interval = get_post_meta($sub->tier_id, PPY_PREFIX . 'initial_payment_frequency', true);
					if ($y > 0 && $initial_interval) {
						$initial_payment_timeframe = get_post_meta($sub->tier_id, PPY_PREFIX . 'initial_payment_timeframe', true);
						$due_date = strtotime($sub->subscription_date . ' + ' . $initial_interval . ' ' . $initial_payment_timeframe);
						$due_date_string = date_i18n(get_option('date_format'), $due_date);
						if ($y > 1) {
							$due_date = strtotime(' + ' . ($frequency - 1) . ' ' . $initial_payment_timeframe, $due_date);
							$due_date_string = date_i18n(get_option('date_format'), $due_date);
						}
					}
					$payment_data = null;
					if ($sub->version == 2) {
						$payment_data = $ppy_payment->get_payment_by_number($sub->id, $y + 1);
						$payment_needed = $payment_data ? $payment_data->status != 'paid' && $payment_data->status != 'chargeback-paid' : true;
						$x = true;
					} else {
						$payment_needed = $y == $payment_details['payments_made'];
						$x = $y <= $payment_details['payments_made'] - 1;
					}


                    if ($x && $payment_needed) {
                        $due_date_u = $due_date;
                        if (((isset($start_date) && $start_date <= $due_date_u) && (isset($end_date) && $end_date >= $due_date_u) ||
                                (isset($start_date) && $start_date <= $due_date_u) && (empty($end_date)) ||
                                (empty($start_date) && isset($end_date) && $end_date <= $due_date_u) ||
                                (empty($start_date) && empty($end_date))) && $payment_details['amount'] > 0) {
                            $primary_row = ($count_payment == 0) ? $sub->id : 0;

                            $payments_amount = Poppyz_Statistics::discount_computation( $sub->id, $sub->tier_id, $payment_details['amount'], $sub->user_id );
							$amount = $amount + $payments_amount;
                            $parent_id = ($count_payment == 0) ? $sub->id : $parent_id;
                            $sub_row = ($count_payment != 0) ? "rp-sub-row-" . $sub->id : "";
                            $arrow_down = ($count_payment == 0) ? " <i class='arrow_down down'></i>" : "";
							$loop_var_2 = $y + 1;
							if( $times == $loop_var_2 ) {
                                $output .= "<tr id='" . $primary_row . "' class='rp-row-" . $parent_id . " '>";
                                $output .= "<td data-order='".$due_date_u."'>" . $due_date_string . "</td>";
                                $output .= "<td data-order='".$payment_details['fullname']."'>" . $payment_details['fullname'] . "</td>";
                                $output .= "<td >" . $payment_details['course'] . "</td>";
                                $output .= "<td >" . $payment_details['tier'] . "</td>";
                                $output .= "<td data-order='".$amount."'>" . Poppyz_Core::format_price($amount) . "</td>";
                                $output .= "<td >" . $payment_details['interval']." ". _n( rtrim($payment_details['timeframe'], "s"), $payment_details['timeframe'], $payment_details['interval'] ). "</td>";
                                $output .= "<td> <a class='remind btn-link' href='" . $sub->id . "'><span class='dashicons dashicons-email-alt send-reminder-icon'></span></a></td>";
                                $output .= "</tr>";
								$total_remaining_payments = $total_remaining_payments + $amount;
							}


                            $count_payment++;
//                            if( $times == $loop_var_2 ) {
//								$overdue_data[] = array(
//                                    "due_date_string" => $due_date_string,
//                                    "fullname" => $payment_details['fullname'],
//                                    "course" => $payment_details['course'],
//                                    "tier" => $payment_details['tier'],
//                                    "amount" => Poppyz_Core::format_price($amount),
//                                    "interval" => $payment_details['interval']." ". _n( rtrim($payment_details['timeframe'], "s"), '%s '.$payment_details['timeframe'], $payment_details['interval'] ),
//                                    "action" => "<a class='remind btn-link' href='" . $sub->id . "'><span class='dashicons dashicons-email-alt send-reminder-icon'></span></a>"
//                                );
//							}
                        }
                    }
				}
			}
		}
		return array(
			"table_data" => $output,
			"total_amount" => Poppyz_Core::format_price( $total_remaining_payments)
		);
	}

}
