<?php

/**
 * This handles all invoice methods
 *
 * @link       http://socialmedium.nl
 * @since      2.0
 *
 * @package    poppyz
 * @subpackage poppyz/includes
 */


class Poppyz_Invoice {

    /**
     * Get invoices
     *
     * Get all invoices
     *
     * @since 1.0
     * @param array $args Query arguments
     * @return mixed array if discounts exist, false otherwise
     */
    public static function get_invoices( $args = array() ) {
        $defaults = array(
            'post_type'      => PPY_INVOICE_PT,
            'posts_per_page' => 30,
            'paged'          => null,
        );

        $args = wp_parse_args( $args, $defaults );

        $invoices = get_posts( $args );
        /*if( ! $invoices && ! empty( $args['s'] ) ) {
            // If no invoices are found and we are searching, re-query with a meta key to find discounts by code
            $args['meta_key']     = PPY_PREFIX . 'invoice_client_first_name';
            $args['meta_value']   = $args['s'];
            $args['meta_compare'] = 'LIKE';
            unset( $args['s'] );
            $invoices = get_posts( $args );
        }*/

        if( $invoices ) {
            return $invoices;
        }
        return false;
    }

    public static function get_invoices_query( $args = array() ) {
        $defaults = array(
            'post_type'      => PPY_INVOICE_PT,
            'posts_per_page' => 30,
            'paged'          => null,
        );

        $args = wp_parse_args( $args, $defaults );

        $invoices = new WP_Query( $args );

        if( $invoices ) {
            return $invoices;
        }
        return false;
    }

    public static function get_invoices_by_id( $id, $args = array() ) {
        $defaults = array(
            'post_type'      => PPY_INVOICE_PT,
            'posts_per_page' => -1,
            'paged'          => null,
            'order'        => 'ASC',
            'orderby'        => 'post_title',
            'meta_query' => array(
                array(
                    'key'     => PPY_PREFIX . 'invoice_subs_id',
                    'value'   => $id,
                    'compare' => '=',
                ),
            ),
            'post_status' => 'paid'
        );

        $args = wp_parse_args( $args, $defaults );

        $invoices = get_posts( $args );
        /*if( ! $invoices && ! empty( $args['s'] ) ) {
            // If no invoices are found and we are searching, re-query with a meta key to find discounts by code
            $args['meta_key']     = PPY_PREFIX . 'invoice_client_first_name';
            $args['meta_value']   = $args['s'];
            $args['meta_compare'] = 'LIKE';
            unset( $args['s'] );
            $invoices = get_posts( $args );
        }*/

        if( $invoices ) {
            return $invoices;
        }
        return false;
    }

    public static function get_subscription_invoices( $id, $args = array() ) {
        global $wpdb;
        $ppy_sub = new Poppyz_Subscription();
        $sub = $ppy_sub->get_subscription_by_id( $id );
        if ( $sub->version != 2) {
            return self::get_invoices_by_id($id, $args);
        }

        $sql = $wpdb->prepare(
            "
	            SELECT *
	            FROM {$wpdb->prefix}course_subscriptions_payments
	            WHERE subscription_id = %d
	      	",
            $id
        );
        return $wpdb->get_results(
            $sql
        );
    }

    public static function has_credit_invoice( $id, $payments_made ) {
        $args = array(
            'post_type'      => PPY_INVOICE_PT,
            'posts_per_page' => 1,
            'paged'          => null,
            'order'        => 'ASC',
            'orderby'        => 'post_title',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key'     => PPY_PREFIX . 'invoice_subs_id',
                    'value'   => $id,
                    'compare' => '=',
                ),
                array(
                    'key'     => PPY_PREFIX . 'invoice_payments_made',
                    'value'   => $payments_made,
                    'compare' => '=',
                ),
            ),
            'post_status' => 'credit'
        );

        $invoices = new WP_Query( $args );

        if( $invoices->found_posts > 0 ) {
            return true;
        }
        return false;
    }

    public static function is_chargedback( $invoice_id ) {
        global $wpdb;
        if ( !Poppyz_Core::table_exists( $wpdb->prefix . 'course_subscriptions_payments' ) ) return false;
        $sql = $wpdb->prepare(
            "
	            SELECT id
	            FROM {$wpdb->prefix}course_subscriptions_payments
	            WHERE invoice_id = %d
	            AND status = %s
	      	",
            $invoice_id,
            'chargedback'
        );
        return $wpdb->get_var(
            $sql
        );
    }
    /**
     * Get invoice
     *
     * Retrieves a invoice by its ID.
     *
     * @since 1.0
     * @param integer $invoice_id Invoice ID
     * @return array
     */
    public function get_invoice( $invoice_id = 0 ) {

        if( empty( $invoice_id ) ) {
            return false;
        }

        $invoice = get_post( $invoice_id );

        if ( get_post_type( $invoice_id ) != PPY_INVOICE_PT ) {
            return false;
        }

        return $invoice;
    }

    /**
     * Get invoice by code
     *
     * @param string $code
     *
     * @since       1.0
     * @return      mixed
     */
    public function get_invoice_by_code( $code = '' ) {

        if( empty( $code ) || ! is_string( $code ) ) {
            return false;
        }

        return $this->get_invoice_by( 'invoice_number', $code );

    }

    /**
     * Retrieve invoice by a given field
     *
     * @since       2.0
     * @param       string $field The field to retrieve the invoice with
     * @param       mixed $value The value for $field
     * @return      mixed
     */
    public function get_invoice_by( $field = '', $value = '' ) {

        if( empty( $field ) || empty( $value ) ) {
            return false;
        }

        if( ! is_string( $field ) ) {
            return false;
        }

        switch( strtolower( $field ) ) {

            case 'invoice_number':
                $invoice = $this::get_invoices( array(
                    'meta_key'       => PPY_PREFIX . 'invoice_number',
                    'meta_value'     => $value,
                    'posts_per_page' => 1,
                    'post_status'    => 'any'
                ) );

                if( $invoice ) {
                    $invoice = $invoice[0];
                }

                break;

            case 'id':
                $invoice = $this->get_invoice( $value );

                break;

            case 'name':
                $invoice = get_posts( array(
                    'post_type'      => PPY_INVOICE_PT,
                    'name'           => $value,
                    'posts_per_page' => 1,
                    'post_status'    => 'any'
                ) );

                if( $invoice ) {
                    $invoice = $invoice[0];
                }

                break;

            case 'subs_id':
                $invoice = $this::get_invoices( array(
                    'meta_key'       => PPY_PREFIX . 'subs_id',
                    'meta_value'     => $value,
                    'posts_per_page' => 1,
                    'post_status'    => 'any'
                ) );

                if( $invoice ) {
                    $invoice = $invoice[0];
                }

                break;
                break;

            default:
                return false;
        }

        if( ! empty( $invoice ) ) {
            return $invoice;
        }

        return false;
    }

    function get_all_invoice_ids( $status = 'any') {
        global $wpdb;

        $all = new WP_Query( array (
            'posts_per_page'        => -1,
            'fields' => 'ids',
            'post_type'      => PPY_INVOICE_PT,
            'post_status'    => $status,
			'meta_query' => array (
				'relation' => 'OR',
				array (
					'key' => PPY_PREFIX .'invoice_mode',
					'value' => "Live",
					'compare' => '='
				),
				array (
					'key' => PPY_PREFIX .'invoice_mode',
					'compare' => 'NOT EXISTS',
				)
			)
        ));
        return $all->posts;
    }

    public function register_status() {
        register_post_status( 'paid', array(
            'label'                     => __( 'Paid','poppyz'),
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
        ) );
        register_post_status( 'unpaid', array(
            'label'                     => __( 'Unpaid' ,'poppyz'),
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
        ) );
        register_post_status( 'credit', array(
            'label'                     => __( 'Credit' ,'poppyz'),
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
        ) );
    }


    /**
     * Add an invoice using an existing subscription
     * @since 1.0
     * @param int $subs_id
     * @param array $data
     */
    function save_invoice( $subs_id, $data = array() ) {

        $meta = array();

        $ppy_subs = new Poppyz_Subscription();
        $subs = $ppy_subs->get_subscription_by_id( $subs_id );

        $user_id = $subs->user_id;
        $tier_id = $subs->tier_id;
        $payment_method = $subs->payment_method;
        $payments_made = $subs->payments_made;
        $payment_number = isset( $data['payment_number'] ) ? $data['payment_number'] : false;

        $user = get_userdata( $user_id );
        $course_id = Poppyz_Core::get_course_by_tier( $tier_id );

        require_once( PPY_DIR_PATH . 'public/class-poppyz-user.php' );
        $custom_fields = Poppyz_User::custom_fields();
        foreach ( $custom_fields as $name => $value) {
            $meta[$value] = get_user_meta( $user_id, PPY_PREFIX . $value, true );
        }

        $contact_name =  $user->first_name . ' ' . $user->last_name;

        if ( empty( $company ) ) {
            $user_meta['company'] = $contact_name;
        }

        $meta['product'] = get_the_title( $course_id ) . ' - ' . get_the_title( $tier_id );

        $discounted_amount = $ppy_subs->compute_discounted_price( $subs_id );
        $meta['amount'] = $discounted_amount;
        $meta['currency'] = Poppyz_Core::get_currency();

        $meta['tax_rate'] = Poppyz_Core::get_tax_rate( $tier_id, $user_id );
        $meta['tax_rate_single'] =  Poppyz_Core::get_tax_rate( $tier_id, $user_id, true );
        $meta['price_tax'] = Poppyz_Core::get_price_tax( $tier_id );

        $meta['firstname'] = $user->first_name;
        $meta['lastname'] = $user->last_name;
        $meta['client_number'] = $user_id;
        $meta['email'] = $user->user_email;
        $meta['status'] = 'paid';
        $meta['subs_id'] = $subs_id;
        $meta['tier_id'] = $tier_id;
        $meta['course_id'] = $course_id;

        //save btw number of customer (if any)
        $vat_number =  get_user_meta( $user_id, PPY_PREFIX . 'vatnumber', true );
        $vatshifted =  get_user_meta( $user_id, PPY_PREFIX . 'vatshifted', true );

        $vat_out_of_scope = false;
        if ( $subs->version == 2 ) {
            $vatshifted = isset( $subs->vatshifted ) ? $subs->vatshifted : 0;
            $vat_out_of_scope = isset( $subs->vat_out_of_scope ) ? $subs->vat_out_of_scope : 0;
        }
        $meta['vat_number'] = $vat_number;
        if ( $vatshifted && Poppyz_Invoice::is_vatshifted( $user_id ) ) {
            $meta['vatshifted'] = $vatshifted;
        } else {
            $meta['vatshifted'] = 0;
        }

        if ( $vat_out_of_scope && Poppyz_Invoice::is_vat_out_of_scope( $user_id ) ) {
            $meta['vat_out_of_scope'] = 1;
        }
		if ( Poppyz_Invoice::is_vat_disabled() || Poppyz_Core::is_donation_enabled( $tier_id ) ) {
            $meta['vat_disabled'] = 1;
        }
        $meta['date_created'] = date( 'Y-m-d H:i:s' );

        $exp_days = Poppyz_Core::get_option( 'invoice_expiration_days' );
        if ( empty( $exp_days ) ) $exp_days = 7;
        $meta['date_expired'] = date( 'Y-m-d', strtotime( $meta['date_created'] . ' + ' . $exp_days . ' days' ) );

        $payment_frequency = get_post_meta( $tier_id, PPY_PREFIX .  'payment_frequency', true );
        $number_of_payments = get_post_meta( $tier_id, PPY_PREFIX .  'number_of_payments', true );

        if ( $payment_method == 'plan' ) {
            global $ppy_lang;

            if ( $subs_id ) {

                $payments_left = $number_of_payments - $payments_made;
                $days = $payments_made * $payment_frequency * 30;

                if ( $payment_number ) {
                    $current_payment_number = $payment_number;
                } else {
                    $current_payment_number = $payments_made;
                }

                $payment_plan_text =  ' - ' . sprintf( __( 'Payment Plan: %d of %d ' ,'poppyz') , $current_payment_number, $number_of_payments);
                $meta['payments_made'] = $current_payment_number;

                if ( $payments_made > 0 ) {
                    if ($payments_left > 0) {
                        $due_date =  strtotime( $subs->subscription_date . ' + ' . $days . ' days' );
                        $diff = abs( $due_date - time() );
                        $invoice_due_days = floor( $diff /( 60*60*24 ) );
                        //$meta['date_expired'] = date( 'Y-m-d', $due_date );
                    }
                } else {
                    $invoice_due_days = $payment_frequency * 30;
                }
            } else {

                $payment_plan_text = ' - ' . sprintf( __( 'Payment Plan: 1 of %d' ,'poppyz'), $number_of_payments);
            }

            $meta['product'] .= $payment_plan_text;

        }

        if ( ( $payment_method == 'plan' || $payment_method == 'membership' ) && $subs->initial_amount > 0 && (int)$payments_made < 2 ) {
            $discounted_initial_amount = $ppy_subs->compute_discounted_price( $subs_id, $subs->initial_amount );
            $meta['amount'] = $discounted_initial_amount;
        }

        if ( count($data) > 0 ) {
            foreach ( $data as $key => $value ) {
                $meta[$key] = $value;
            }
        }
        $number = $meta['number'] = $this->generate_invoice_number($subs->payment_mode);
        $invoice_id = wp_insert_post( array(
            'post_type'   => PPY_INVOICE_PT,
            'post_title'  => $number,
            'post_status' => $meta['status']
        ) );


		$tierDonation = get_user_meta( $user_id, PPY_PREFIX . 'donation_amount_'.$subs_id, true);
		if( isset($tierDonation) && !empty($tierDonation) ) {
			$meta['donation_amount'] = $tierDonation;
			//delete_user_meta( $user_id, PPY_PREFIX . 'donation_amount_'.$subs_id);
			delete_user_meta( $user_id, PPY_PREFIX . 'donation_exist');
		}

        foreach( $meta as $key => $value ) {
            update_post_meta( $invoice_id, PPY_PREFIX .'invoice_'  . $key, $value );
        }

        return $invoice_id;

    }

    function save_invoice_admin( $details ) {
        $meta = array(
            'product' => isset($details['product']) ? $details['product'] : '',
            'amount' => isset($details['amount']) ? $details['amount'] : '',
            'client_number' => isset($details['select-user']) ? $details['select-user'] : '',
            'company' => isset($details['company']) ? $details['company'] : '',
            'firstname' => isset($details['firstname']) ? $details['firstname'] : '',
            'lastname' => isset($details['lastname']) ? $details['lastname'] : '',
            'address' => isset($details['address']) ? $details['address'] : '',
            'zipcode' => isset($details['zipcode']) ? $details['zipcode'] : '',
            'city' => isset($details['city']) ? $details['city'] : '',
            'country' => isset($details['country']) ? $details['country'] : '',
            'date_created' => isset($details['date_created']) ? $details['date_created'] : '',
            'date_expired' => isset($details['date_expired']) ? $details['date_expired'] : '',
            'status' => isset($details['status']) ? $details['status'] : 'unpaid',
            'create_subscription' => isset($details['create_subscription']) ? 1 : '',
            'payments_made' => isset($details['payments_made']) ? $details['payments_made'] : 1,
        );

        if (  $meta['product'] == '' ||  $meta['amount'] == '' || $meta['firstname'] == '' || $meta['lastname'] == '' || $meta['address'] == '' ) {
            return false;
        }

        //add a subscription for the invoice
        $meta['amount'] = Poppyz_Core::save_formatted_price(  $meta['amount'] );

        if ( $meta['create_subscription'] == 1 &&  $meta['amount'] >= 0 ) {
            $subs_id = Poppyz_Subscription::add_subscription( $details['select-user'], $details['select-tier'], $meta['amount'] , $meta['date_created'] );
            $meta['subs_id'] = $subs_id ? $subs_id : 0;
        }
        $meta['tier_id'] = $details['select-tier'];
        //dont save this
        unset($meta['create_subscription']);

        $start_timestamp = strtotime( $meta['date_created'] );

        if( ! empty( $meta['date_created'] ) ) {
            $meta['date_created']  = date( 'Y-m-d H:i:s', $start_timestamp );
        } else {
            $meta['date_created']  = date( 'Y-m-d H:i:s' );
        }
        $meta['date_created_ts'] = strtotime( $meta['date_created'] );

        if( ! empty( $meta['date_expired'] ) ) {
            $meta['date_expired'] = date( 'Y-m-d H:i:s', strtotime( date( 'Y-m-d', strtotime( $meta['date_expired'] ) ) . ' 23:59:59' ) );
            $end_timestamp  = strtotime( $meta['date_expired'] );
            if( ! empty( $meta['date_created'] ) && $start_timestamp > $end_timestamp ) {
                // Set the date_expired date to the start date if start is later than date_expired
                $meta['date_expired'] = $meta['date_created'];
            }
        } else {
            $exp_days = Poppyz_Core::get_option( 'invoice_expiration_days' );
            if ( empty( $exp_days ) ) $exp_days = 7;
            $meta['date_expired'] = date( 'Y-m-d', strtotime( $meta['date_created'] . ' + ' . $exp_days . ' days' ) );
        }

        $meta['tax_rate'] = Poppyz_Core::get_tax_rate( $meta['tier_id'], $details['select-user'] );
        $meta['price_tax'] = Poppyz_Core::get_price_tax( $meta['tier_id'] );
        $meta['currency'] = Poppyz_Core::get_currency();

        //save btw number of customer (if any)
        $vat_number =  get_user_meta( $details['select-user'], PPY_PREFIX . 'vatnumber', true );
        if ( !empty( $vat_number ) ){
            $meta['vat_number'] = $vat_number;
        }

        $number = $meta['number'] = $this->generate_invoice_number();

        $invoice_id = wp_insert_post( array(
            'post_type'   => PPY_INVOICE_PT,
            'post_title'  => $number,
            'post_status' => $meta['status']
        ) );

        foreach( $meta as $key => $value ) {
            update_post_meta( $invoice_id, PPY_PREFIX .'invoice_'  . $key, $value );
        }

        return $invoice_id;

    }


    /**
     * Generates a unique sequential number by year - format: 20160001
     *
     * @since 1.0
     * @return string
     */
	public function generate_invoice_number($mode = null) {
		global $wpdb;

		/*$max = $wpdb->get_var
		(
			'SELECT MAX(cast(meta_value as unsigned)) AS max FROM ' . $wpdb->prefix . 'postmeta AS pm, ' . $wpdb->prefix .'posts AS po
			 WHERE pm.post_id = po.ID AND po.post_status IN ("paid", "unpaid", "expired", "credit") AND po.post_type = "' . PPY_INVOICE_PT .'" AND pm.meta_key = "' . PPY_PREFIX . 'invoice_number"'
		);*/
		$mode_prefix = "";
		if ( empty($mode) || $mode == "Live") {
			$max = $wpdb->get_var
			(
				'SELECT MAX(cast(post_title as unsigned)) AS max FROM ' . $wpdb->prefix .'posts AS po
             WHERE po.post_status IN ("paid", "unpaid", "expired", "credit") AND po.post_type = "' . PPY_INVOICE_PT .'"'
			);
		} else {
			$query = 'SELECT MAX(cast(TRIM(LEADING "T" FROM post_title) as unsigned)) AS max FROM ' . $wpdb->prefix .'posts AS po INNER JOIN '.$wpdb->prefix.'postmeta as pm ON po.ID=pm.post_id
             WHERE po.post_status IN ("paid", "unpaid", "expired", "credit") AND po.post_type = "' . PPY_INVOICE_PT .'" AND pm.meta_key="ppy_invoice_mode" AND pm.meta_value="Test"';
			$max = $wpdb->get_var($query);
			$mode_prefix = "T";
		}
		$year_now = date( 'Y' );
		$year = substr( $max, 0, 4 );
		//if no meta value yet or it's a new year , start at the beginning

		if ( $max == null ||  $year < $year_now  ) {
			$start_num = ($mode == "Test") ? Poppyz_Core::get_option( 'test_invoice_starting_number' ) :Poppyz_Core::get_option( 'invoice_starting_number' );
			//check if the Starting Number is not set in the plugin settings
			$start_num = ( !empty( $start_num ) ) ? $start_num : '0001';
			$invoice_number = $year_now . $start_num;
		} else {
			//check if it's the current year
			$invoice_number = $max + 1;
		}
		return $mode_prefix.$invoice_number;
	}

    /**
     * Deletes a invoice.
     *
     * @since 1.0
     * @param int $invoice_id Invoice ID (default: 0)
     * @return void
     */
    function remove_invoice( $invoice_id = 0 ) {
        wp_delete_post( $invoice_id, true );

    }

    function update_invoice_status( $invoice_id = 0, $new_status = 'active' ) {
        $invoice = $this->get_invoice(  $invoice_id );

        if ( $invoice ) {
            wp_update_post( array( 'ID' => $invoice_id, 'post_status' => $new_status ) );
            return true;
        }

        return false;
    }

    function update_invoice( $invoice_id, $fields = array() ) {
        $invoice = $this->get_invoice(  $invoice_id );

        if ( $invoice && is_array( $fields ) ) {
            foreach ( $fields as $field => $value ) {
                update_post_meta( $invoice_id, PPY_PREFIX . $field, $value );
            }
            return true;
        }

        return false;
    }

    function duplicate_to_credit( $invoice_id ) {
        $invoice = $this->get_invoice(  $invoice_id );
        global $ppy_lang;
        if ( $invoice ) {
            $number =  $this->generate_invoice_number();
            $post    = array(
                'post_title' => $number,
                'post_status' => 'credit',
                'post_type' => $invoice->post_type,
                'post_author' => 1
            );
            $new_post_id = wp_insert_post($post);
            $data = get_post_custom( $invoice_id );
            foreach ( $data as $key => $values) {
                foreach ($values as $value) {

                    if ( $key == PPY_PREFIX . 'invoice_product' ) {
                        $value = __( 'Credit Invoice' ,'poppyz'). ' ' . $value;
                    }
                    if ( $key == PPY_PREFIX . 'invoice_amount' ) {
                        $value = -1 * abs( $value );
                    }
                    if ( $key == PPY_PREFIX . 'invoice_number' ) {
                        $value = $number;
                    }
                    add_post_meta( $new_post_id, $key, $value );
                }
            }
        }

        return false;
    }

    public function get_bulk_actions() {
        $actions = array(
            'pay'   => __( 'Pay' ,'poppyz'),
            'unpay' => __( 'Mark as unpaid','poppyz'),
            'download' => __( 'Download','poppyz'),
            'export-csv'     => __( 'Export CSV','poppyz'),
            'export-xml'     => __( 'Export XML','poppyz'),
            'duplicate-credit'     => __( 'Duplicate to credit invoice','poppyz'),
            'delete'     => __( 'Delete' ,'poppyz')
        );

        return $actions;
    }


    function invoice_exists( $invoice_id ) {
        if ( $this->get_invoice(  $invoice_id ) ) {
            return true;
        }

        return false;
    }


    function is_invoice_expired( $invoice_id = null ) {
        $invoice = $this->get_invoice(  $invoice_id );
        $return   = false;

        if ( $invoice ) {
            $expiration = $this->get_invoice_field( $invoice_id, 'date_expired' );
            $status = get_post_status( $invoice_id );
            if ( $status != 'paid' && $expiration ) {
                $expiration = strtotime( $expiration );
                if ( $expiration < current_time( 'timestamp' ) ) {
                    $this->update_invoice_status( $invoice_id, 'expired' );
                    $return = true;
                }
            }
        }

        return $return;
    }

    function is_invoice_valid( $invoice_id ) {
        $invoice = $this->get_invoice(  $invoice_id );
        $return = true;

        if ( $invoice ) {
            $date_created = $this->get_invoice_field( $invoice_id, 'date_created' );
            $expiration = $this->get_invoice_field( $invoice_id, 'date_expired' );

            if ( $date_created ) {
                $date_created = strtotime( $date_created );
                if ( $date_created > current_time( 'timestamp' ) ) {
                    $return = false;
                }
            }
            if ( $expiration ) {
                $expiration = strtotime( $expiration );
                if ( $expiration < current_time( 'timestamp' ) ) {
                    $return = false;
                }
            }
        }

        return $return;
    }

    function get_invoice_field( $invoice_id, $field  ) {
        return get_post_meta( $invoice_id,  PPY_PREFIX . 'invoice_' . $field, true );

    }

    function get_invoice_number( $invoice_id = null ) {
        return get_the_title( $invoice_id );
    }

    function format_invoice_rate( $type, $amount ) {
        if ( $type == 'flat' ) {
            return Poppyz_Core::format_price( $amount );
        } else {
            return $amount . '%';
        }
    }

    public static function is_vatshifted( $user_id = null ) {
        if ( !$user_id && is_user_logged_in() ) {
            $user_id = get_current_user_id();
        }
        //if on the same country, vatshifted should not work
        if ( Poppyz_Core::is_same_country( $user_id ) ) {
            return false;
        }

        $vatshifted = get_user_meta( $user_id,  PPY_PREFIX . 'vatshifted', true );
        $vatnumber = get_user_meta( $user_id,  PPY_PREFIX . 'vatnumber', true );
        $company = get_user_meta( $user_id, PPY_PREFIX . 'company', true);
        $show_vat_shifted = Poppyz_Core::get_option( 'show_vat_shifted' );

        return ( $user_id && $show_vat_shifted == 'on' && ( !empty( $vatshifted ) && !empty( $vatnumber ) && !empty($company) ) );
    }

    public static function is_vat_out_of_scope( $user_id = null ) {
        if ( !$user_id && is_user_logged_in() ) {
            $user_id = get_current_user_id();
        }
        //if  on the same country, vat_out_of_scope should not work
        if ( Poppyz_Core::is_same_country( $user_id ) ) {
            return false;
        }
        //if country is inside EU, vat_out_of_scope should not work
        if ( Poppyz_Core::isEU( $user_id ) ) {
            return false;
        }
        $vatoutscope = get_user_meta( $user_id,  PPY_PREFIX . 'vat_out_of_scope', true );
        $show_vatoutscope = Poppyz_Core::get_option( 'vat_out_of_scope' );
        return ( $user_id && $show_vatoutscope == 'on' && ( !empty( $vatoutscope )));
    }

    public static function is_vat_disabled() {
        $vat_disabled = Poppyz_Core::get_option( 'vat_disabled' );
        return ($vat_disabled == 'on');
    }

    public static function get_invoice_by_subscription( $subs_id, $payments_made = 1) {
        $args = array(
            'meta_query' => array(
                array(
                    'key' => PPY_PREFIX . 'invoice_subs_id',
                    'value' => $subs_id
                )
            ),
            'post_type' => PPY_INVOICE_PT,
            'orderby' => 'ID', //get the latest invoice for payment plans
            'order' => 'DESC',
            'post_status' => 'paid',
            'posts_per_page' => 1

        );

        if ( $payments_made > 1 ) {
            $args['meta_query'] = array(
                'key' => PPY_PREFIX . 'invoice_payments_made',
                'value' => $payments_made
            );
        }
        $invoice = get_posts($args);
        if ( !empty( $invoice ) ) {
            foreach ( $invoice as $post ) {
                setup_postdata( $post );
                return $post->ID;
            }
        }
        wp_reset_postdata();
        return false;
    }

    public static function get_invoices_by_subscription( $subs_id ) {
        $args = array(
            'meta_query' => array(
                array(
                    'key' => PPY_PREFIX . 'invoice_subs_id',
                    'value' => $subs_id
                )
            ),
            'post_type' => PPY_INVOICE_PT,
            'posts_per_page' => -1,
            'post_status' => 'paid',
            'orderby' => 'ID',
            'order' => 'DESC'
        );

        $invoices = get_posts($args);
        wp_reset_postdata();
        return $invoices;
    }

    public static function get_invoices_by_tier( $tier_id, $date = false ) {
        $args = array(
            'meta_query' => array(
                array(
                    'key' => PPY_PREFIX . 'invoice_tier_id',
                    'value' => $tier_id
                )
            ),
            'post_type' => PPY_INVOICE_PT,
            'posts_per_page' => -1,
            'post_status' => 'paid',
            'orderby' => 'ID', //get the latest invoice for payment plans
            'order' => 'DESC'
        );
        if ( !empty($date) ) {
            $args['meta_query'][] = array(
                'key'     => PPY_PREFIX . 'invoice_date_created',
                'value' => array( $date[0], $date[1] ),
                'compare' => 'BETWEEN',
                'type' => 'CHAR'
            );
        }

        $invoices = get_posts($args);

        wp_reset_postdata();
        return $invoices;
    }

    public function is_invoice_owner( $invoice_id, $user_id ) {
        $subs_id = get_post_meta( $invoice_id, PPY_PREFIX . 'invoice_subs_id', true );
        global $wpdb;
        $table = $wpdb->prefix . "course_subscriptions";

        return $wpdb->get_var( $wpdb->prepare(
            "
                    SELECT id
                    FROM $table
                    WHERE id = %d
                    AND user_id = %d
                    ",
            $subs_id,
            $user_id
        ) );
    }

    public function initialize_tcpdf() {
        require_once PPY_DIR_PATH . 'includes/tcpdf_min/config/tcpdf_config.php' ;
        require_once PPY_DIR_PATH . 'includes/tcpdf_min/tcpdf.php' ;
    }

    public function generate_pdf( $id, $format = "I", $sample = false ) {
        $this->initialize_tcpdf();
        $ppy_subs = new Poppyz_Subscription();
        $ppy_invoice = new Poppyz_Invoice();
        global $ppy_lang;
        $countries = Poppyz_Core::countries();

        if ( !$sample ) {
            $options = get_option( 'ppy_settings' );
            $invoice_company = Poppyz_Core::get_option( 'invoice_company' );
            $invoice_logo = Poppyz_Core::get_option( 'invoice_logo' );
            $invoice_contact_person = Poppyz_Core::get_option( 'invoice_contact_person' );
            $invoice_phone = Poppyz_Core::get_option( 'invoice_phone' );
            $invoice_email = Poppyz_Core::get_option( 'invoice_email' );
            $invoice_address = Poppyz_Core::get_option( 'invoice_address' );
            $invoice_zipcode = Poppyz_Core::get_option( 'invoice_zipcode' );
            $invoice_city = Poppyz_Core::get_option( 'invoice_city' );
            $invoice_iban = Poppyz_Core::get_option( 'invoice_iban' );
            $invoice_kvk = Poppyz_Core::get_option( 'invoice_kvk' );
            $invoice_btw = Poppyz_Core::get_option( 'invoice_btw' );
            $footer_text = Poppyz_Core::get_option( 'invoice_footer' );

            //$invoice_country_abr = Poppyz_Core::get_option( 'invoice_country' );
            //$invoice_country = isset( $countries[$invoice_country_abr] ) ?  $countries[$invoice_country_abr] : '';
            $invoice_country = Poppyz_Core::get_option( 'invoice_country' );


            $invoice = $ppy_invoice->get_invoice( $id );

            $invoice_number = $ppy_invoice->get_invoice_number( $id );
            $product = $ppy_invoice->get_invoice_field( $id, 'product' );

            $company = $ppy_invoice->get_invoice_field( $id, 'company' );
            $firstname = $ppy_invoice->get_invoice_field( $id, 'firstname' );
            $lastname = $ppy_invoice->get_invoice_field( $id, 'lastname' );
            $address = $ppy_invoice->get_invoice_field( $id, 'address' );
            $zipcode = $ppy_invoice->get_invoice_field( $id, 'zipcode' );
            $city = $ppy_invoice->get_invoice_field( $id, 'city' );
            $btw = $ppy_invoice->get_invoice_field( $id, 'vat_number' );
            $payment_id = $ppy_invoice->get_invoice_field( $id, 'payment_id' );
            $additional_products = $ppy_invoice->get_invoice_field( $id, 'additional_product_subscriptions' );
			$donation_amount = $ppy_invoice->get_invoice_field( $id, 'donation_amount' );

            //backwards compatibility for old invoices
            if (!$btw) {
                $btw = $ppy_invoice->get_invoice_field( $id, 'vatnumber' );
            }
            $vatshifted = $ppy_invoice->get_invoice_field( $id, 'vatshifted' );

            $coupon_code = $ppy_invoice->get_invoice_field( $id, 'coupon_code' );
            if ( $coupon_code ) {
                $product .= '<br />' . __( 'Coupon used' ,'poppyz') . ': ' . $coupon_code;;
            }

            $is_additional_product = false;
            if (!empty($additional_products)) {
                $additional_product_ids = explode(',', $additional_products);
                $is_additional_product = in_array($ppy_invoice->get_invoice_field( $id, 'subs_id' ), $additional_product_ids);
            }

			$tier_discount = $ppy_invoice->get_invoice_field( $id, 'tier_discount' );
			if ( !empty($tier_discount) && !$is_additional_product) {
				$product .= '<br />' . __( 'Promotional Discount' ,'poppyz') . ': -' . Poppyz_Core::format_price( $tier_discount );
			}
            $date_created = $ppy_invoice->get_invoice_field( $id, 'date_created' );
            $date_expired = $ppy_invoice->get_invoice_field( $id, 'date_expired' );

            $return = $this->get_price_fields( $id );

            extract($return);
            /*$tax_rate = $ppy_invoice->get_invoice_field( $id, 'tax_rate' );
            $price_tax = $ppy_invoice->get_invoice_field( $id, 'price_tax' );*/

            if ( !empty( $date_created ) ) {
                $start_timestamp = strtotime( $date_created );
                $date_created = date( 'd-m-Y', $start_timestamp );
            }

            if ( !empty( $date_expired ) ) {
                $end_timestamp = strtotime( $date_expired );
                $date_expired = date( 'd-m-Y', $end_timestamp );
            }

            $country_abr = $ppy_invoice->get_invoice_field( $id, 'country' );
            $country = isset( $countries[$country_abr] ) ?  $countries[$country_abr] : '';
        } else {
            //show default texts for PDF preview
            $invoice_company = Poppyz_Core::get_option( 'invoice_company' );
            $invoice_logo = Poppyz_Core::get_option( 'invoice_logo' );
            $invoice_contact_person = Poppyz_Core::get_option( 'invoice_contact_person' );
            $invoice_phone = Poppyz_Core::get_option( 'invoice_phone' );
            $invoice_email = Poppyz_Core::get_option( 'invoice_email' );
            $invoice_address = Poppyz_Core::get_option( 'invoice_address' );
            $invoice_zipcode = Poppyz_Core::get_option( 'invoice_zipcode' );
            $invoice_city = Poppyz_Core::get_option( 'invoice_city' );
            $invoice_iban = Poppyz_Core::get_option( 'invoice_iban' );
            $invoice_kvk = Poppyz_Core::get_option( 'invoice_kvk' );
            $invoice_btw = Poppyz_Core::get_option( 'invoice_btw' );
            $footer_text = Poppyz_Core::get_option( 'invoice_footer' );

            $invoice_country = Poppyz_Core::get_option( 'invoice_country' );
            //$invoice_country_abr = Poppyz_Core::get_option( 'invoice_country' );
            //$invoice_country = isset( $countries[$invoice_country_abr] ) ?  $countries[$invoice_country_abr] : '';

            $invoice_number = $this->generate_invoice_number();
            $product = __( 'PRODUCT','poppyz');
            //$client_number = $ppy_invoice->get_invoice_field( $id, 'client_number' );
            $company = __( 'COMPANY' ,'poppyz');
            $firstname = __( 'FIRST_NAME' ,'poppyz');
            $lastname = __( 'LAST_NAME' ,'poppyz');
            $address = __( 'ADDRESS' ,'poppyz');
            $zipcode = __( 'ZIP' ,'poppyz');
            $city = __( 'CITY' ,'poppyz');
            $btw = __( 'VAT NUMBER' ,'poppyz');

            $date_created = __( 'DATE_CREATED' ,'poppyz');
            $date_expired = __( 'DATE_EXPIRED' ,'poppyz');

            $amount = 100;

            $tax_rate = Poppyz_Core::get_option( 'tax_rate' );
            $price_tax = Poppyz_Core::get_option( 'price_tax' );

            $tax_amount = 0;
            if ( $tax_rate > 0 ) {
                $tax_amount = $amount * ( $tax_rate / 100 ) ;
            }

            if ( $price_tax == 'inc' ) {
                $amount = $amount - $tax_amount;
            }
            $tax_amount_formatted = Poppyz_Core::format_price( $tax_amount );

            $total_with_tax = $amount + $tax_amount;
            $country = __('INVOICE_COUNTRY', 'poppyz');
        }

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor( $invoice_contact_person );
        $pdf->SetTitle( $invoice_company );
        $pdf->SetSubject( __('Invoice','poppyz') );

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);


        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->setFont( 'helvetica', '', 8);

        $pdf->setFontSubsetting(true);

        $pdf->setCellPaddings(1, 1, 1, 1);

        $pdf->setCellMargins(1, 1, 1, 1);

        $pdf->SetFillColor(255, 255, 255);

        $pdf->AddPage();

        $company_info =  "<strong>" . $invoice_company . "</strong><br />";
        $company_info .=  $invoice_address . "<br />";
        $company_info .=  $invoice_zipcode . ' ';
        $company_info .=  $invoice_city . "<br />";
        $company_info .=  $invoice_country . "<br />";
        $company_info .=  $invoice_iban . "<br />";

        $company_info2 =  $invoice_phone . "<br />";
        $company_info2 .=  $invoice_email . "<br />";

        $company_info3 = '';
        if ( $invoice_kvk ) $company_info3 .=  'KvK nr: ' . $invoice_kvk . "<br />";
        if ( $invoice_btw ) $company_info3 .=  __('VAT nr','poppyz') .': ' . $invoice_btw . "<br />";


        $user_info = "<strong>" . $company . "</strong><br />";
        $user_info .=  $firstname . ' ' . $lastname . "<br />";
        $user_info .=  $address . "<br />";
        $user_info .=  $zipcode . ' ';
        $user_info .=  $city . "<br />";
        $user_info .=  $country . "<br />";
		if ( $btw ) $user_info .=  __( 'Vat nr' ,'poppyz') . ': ' . $btw . "<br />";


		$invoice_info = __( 'Date Created:','poppyz') . ' ' . $date_created . '<br />';
		if ( $invoice_number ) $invoice_info .=  __( 'Invoice Number:' ,'poppyz') . ' ' .  $invoice_number .  '<br />';
        //$invoice_info .= __( 'Date Expired:' ) . ' ' . $date_expired ;
        if ( $payment_id ) $invoice_info .=  __( 'Payment ID','poppyz') . ': ' . $payment_id . "<br />";
        $invoice_status_text = '';
        if ( get_post_status( $id ) == 'paid' ) {
            $invoice_status_text = '<h2 style="color:green;">' . __( 'Already paid' ,'poppyz') . '</h2>';
        }

        //$invoice_info .=  __( 'Client Number: ' ) . $client_number .  '<br />';

        $invoice_product = '<style>
            table {
                width: 100%;
                
            }
            td {
                padding: 10px;
            }
            th {
                font-weight: bold;
            }
            
            tr.headings th {
                background-color: #eeeeee;
                color: #8d8d8d;
            }
            tr.first-row td {
                height: 200px;
                border-bottom: 1px solid #ccc;
            }
            th.tax-amount {
                border-bottom: 1px solid #ccc;
            }

            td.empty {
                border-bottom: none !important;
            }
            h2 {
            	color: green;
            }
          </style>';

        $invoice_column_titles = array(
            'product' => __( 'Product' ,'poppyz'),
            'number' => __( 'Number' ,'poppyz'),
            'price_unit' => __( 'Price / unit' ,'poppyz'),
            'total' => __( 'Total' ,'poppyz'),
            'total_excluding_vat' => __( 'Total (excl VAT)' ,'poppyz'),
            'vat' => __( 'VAT' ,'poppyz'),
            'total_including_vat' => __( 'Total (incl VAT)' ,'poppyz'),
        );

        $invoice_column_titles = apply_filters( 'ppy_invoice_column_titles', $invoice_column_titles );

        $invoice_product .= '<table cellpadding="10" width="100%">';

        $invoice_product .= '<tr class="headings">';
        $invoice_product .= '<th width="30%">';
        $invoice_product .= $invoice_column_titles['product'];
        $invoice_product .= '</th>';
        $invoice_product .= '<th width="15%">';
        $invoice_product .= $invoice_column_titles['number'];
        $invoice_product .= '</th>';
        $invoice_product .= '<th width="15%">';
        $invoice_product .= $invoice_column_titles['price_unit'];
        $invoice_product .= '</th>';
        $invoice_product .= '<th width="10%">';
        $invoice_product .= ' ';
        $invoice_product .= '</th>';
        $invoice_product .= '<th width="10%">';
        $invoice_product .= ' ';
        $invoice_product .= '</th>';
        $invoice_product .= '<th width="15%">';
        $invoice_product .= $invoice_column_titles['total'];
        $invoice_product .= '</th>';
        $invoice_product .= '</tr>';

        $invoice_product .= '<tr>';
        $invoice_product .= '<td>';
        $invoice_product .= $product;
        $invoice_product .= '</td>';
        $invoice_product .= '<td>';
        $invoice_product .= '1';
        $invoice_product .= '</td>';
        $invoice_product .= '<td>';
        $invoice_product .= Poppyz_Core::format_price( $amount );
        $invoice_product .= '</td>';
        $invoice_product .= '<th colspan="2">';
        $invoice_product .=  $invoice_column_titles['total_excluding_vat'];
        $invoice_product .= '</th>';
        $invoice_product .= '<td align="right">';
        $invoice_product .= Poppyz_Core::format_price( $amount );
        $invoice_product .= '</td>';
        $invoice_product .= '</tr>';

        /*foreach ( $additional_products as $additional_product ) {
            $additional_subscription = $ppy_subs->get_subscription_by_id( $additional_product );
            $invoice_product .= '<tr>';
            $invoice_product .= '<td>';
            $invoice_product .=  get_the_title( $additional_subscription->course_id ) . ' - ' . get_the_title( $additional_subscription->tier_id );
            $invoice_product .= '</td>';
            $invoice_product .= '<td>';
            $invoice_product .= '1';
            $invoice_product .= '</td>';
            $invoice_product .= '<td>';
            $invoice_product .= Poppyz_Core::format_price( $additional_subscription->amount );
            $invoice_product .= '</td>';
            $invoice_product .= '<th colspan="2">';
            $invoice_product .=  $invoice_column_titles['total_excluding_vat'];
            $invoice_product .= '</th>';
            $invoice_product .= '<td align="right">';
            $invoice_product .= Poppyz_Core::format_price( $additional_subscription->amount );
            $invoice_product .= '</td>';
            $invoice_product .= '</tr>';
        }*/

		if( isset($donation_amount) && !empty($donation_amount) ){
			$invoice_product .= '<tr>';
			$invoice_product .= '<td>';
			$invoice_product .= __('Donation', 'poppyz');
			$invoice_product .= '</td>';
			$invoice_product .= '<td>';
			$invoice_product .= '';
			$invoice_product .= '</td>';
			$invoice_product .= '<td>';
			$invoice_product .= Poppyz_Core::format_price( $donation_amount );
			$invoice_product .= '</td>';
			$invoice_product .= '<th colspan="2">';
			$invoice_product .=  $invoice_column_titles['total_excluding_vat'];
			$invoice_product .= '</th>';
			$invoice_product .= '<td align="right">';
			$invoice_product .= Poppyz_Core::format_price( $donation_amount );
			$invoice_product .= '</td>';
			$invoice_product .= '</tr>';
		}

        /*$invoice_product .= '<tr class="row">';
        $invoice_product .= '<th colspan="5" align="right">';
        $invoice_product .= __( 'Total (excl BTW)' );
        $invoice_product .= '</th>';
        $invoice_product .= '<th>';
        $invoice_product .= Poppyz_Core::format_price( $amount );
        $invoice_product .= '</th>';
        $invoice_product .= '</tr>';*/
		$tax_rate = ( empty($total_with_tax) || $total_with_tax == 0 ) ? 0 : $tax_rate;

        $invoice_product .= '<tr class="row">';
        $invoice_product .= '<th colspan="5" align="right">';
        $invoice_product .= $tax_rate . '% ' . $invoice_column_titles['vat'];
        $invoice_product .= '</th>';
        $invoice_product .= '<th align="right" class="tax-amount">';
        $invoice_product .= '<span>' . $tax_amount_formatted . '</span>';
        $invoice_product .= '</th>';
        $invoice_product .= '</tr>';

        $invoice_product .= '<tr class="row">';
        $invoice_product .= '<th colspan="5" align="right">';
        $invoice_product .= $invoice_column_titles['total_including_vat'];
        $invoice_product .= '</th>';
        $invoice_product .= '<th align="right">';
		if (isset($donation_amount) && !empty($donation_amount)) {
			$invoice_product .= Poppyz_Core::format_price( $total_with_tax + $donation_amount );
		} else {
			$invoice_product .= Poppyz_Core::format_price( $total_with_tax );
		}

        $invoice_product .= '</th>';
        $invoice_product .= '</tr>';


        $invoice_product .= '</table>';

        /*$invoice_product .= '<table cellpadding="10" width="100%">';

        $invoice_product .= '<tr class="first-row">';
        $invoice_product .= '<td width="10%" class="border-right">';
        $invoice_product .= '1';
        $invoice_product .= '</td>';
        $invoice_product .= '<td width="70%">';
        $invoice_product .= $product;
        $invoice_product .= '</td>';
        $invoice_product .= '<td width="15%">';
        $invoice_product .= Poppyz_Core::format_price( $amount );;
        $invoice_product .= '</td>';
        $invoice_product .= '</tr>';

        $invoice_product .= '<tr class="row">';
        $invoice_product .= '<td>';
        $invoice_product .= '</td>';
        $invoice_product .= '<th>';
        $invoice_product .= __( 'Total (excl BTW)' );
        $invoice_product .= '</th>';
        $invoice_product .= '<th>';
        $invoice_product .= Poppyz_Core::format_price( $amount );
        $invoice_product .= '</th>';
        $invoice_product .= '</tr>';

        $invoice_product .= '<tr class="row">';
        $invoice_product .= '<td>';
        $invoice_product .= '</td>';
        $invoice_product .= '<th>';
        $invoice_product .= __( 'BTW' );
        $invoice_product .= '</th>';
        $invoice_product .= '<th class="tax-amount">';
        $invoice_product .= '<span>' . Poppyz_Core::format_price( $tax_amount ) . '</span>';
        $invoice_product .= '</th>';
        $invoice_product .= '</tr>';

        $invoice_product .= '<tr class="row">';
        $invoice_product .= '<td>';
        $invoice_product .= '</td>';
        $invoice_product .= '<th>';
        $invoice_product .= __( 'Total (incl BTW)' );
        $invoice_product .= '</th>';
        $invoice_product .= '<th>';
        $invoice_product .= Poppyz_Core::format_price( $total_with_tax );
        $invoice_product .= '</th>';
        $invoice_product .= '</tr>';


        $invoice_product .= '</table>';*/
        $invoice_logo = wp_get_attachment_image_src( $invoice_logo, 'invoice-logo' );
        $invoice_footer = Poppyz_Core::the_content( $footer_text );
		if( isset($donation_amount) && !empty($donation_amount) ) {
			$invoice_heading = __('Invoice', 'poppyz');
		} else {
            $invoice_heading = ($amount < 0) ? __('Credit Invoice', 'poppyz') : __('Invoice', 'poppyz') ;
		}

        $pdf->MultiCell( 55, 40, '<h1>' .  $invoice_heading  . '</h1>', 0, 'L', 0, 1, 15, 20, true, 0, true, true, 60 ,'T', true );

        if ( !empty($invoice_logo[0]) && getimagesize( $invoice_logo[0] ) ) {
            $pdf->Image( $invoice_logo[0], 0, 15, 0, 0, '', '', 'T', true, 300, 'R' );
        }

        $pdf->MultiCell( 55, 40, $user_info . $invoice_info , 0, 'L', 0, 1, 15, 70, true, 0, true, true, 60 ,'T', true );


        $pdf->MultiCell( 55, 40, $company_info . $company_info2 . $company_info3 , 0, 'R', 0, 1, 130, 70, true, 0, true, true, 60 ,'T', true );

        //$pdf->MultiCell( 70, 5, '', 0, 'L', 1, 0, , 70, true, 0, true);


        $pdf->MultiCell( 100, 5, $invoice_status_text, 0, 'L', 0, 1, '', '', true, 0, true, true, 60, 'T', true );

        $pdf->MultiCell( 70, 5, '', 0, 'L', 1, 0, '', '', true, 0, true);
        $pdf->MultiCell( 100, 5, '', 0, 'L', 0, 1, '', '', true, 0, true, true, 60, 'T', true );

        $pdf->Ln( 5 );

        $pdf->writeHTMLCell(0, 0, '', '', $invoice_product, 0, 1, 0, true, '', true );
        $pdf->Ln( 5 );
        $pdf->writeHTMLCell(0, 0, '', '', $invoice_footer, 0, 1, 0, true, '', true );

        $pdf->Ln( 5 );



        if ( $format == "I" ) {
            $pdf->Output( $invoice_number . '.pdf', 'I' );

        } elseif ( $format == "E" ) {

            if ( !Poppyz_Core::get_option( 'send_invoice' ) ) return;

            $to = Poppyz_Core::get_option( 'invoice_email' );
            if ( empty( $to ) ) {
                $to = get_option( 'admin_email' );
            }
            $invoice_link = $this::get_invoice_url( $id ) . '&download';
            $message = __( 'An invoice from Poppyz has been created.' ,'poppyz') . '<br /><br /><br />' . __( 'Invoice' ,'poppyz') . ': ' .  $invoice_link;
            add_filter( 'wp_mail_content_type', function( $content_type ) {
                return 'text/html';
            });
            /*$pdfdoc = $pdf->Output('', 'S');
            $attachment = chunk_split(base64_encode($pdfdoc));
            $filename = $invoice_number . '.pdf';
            $from = "poppyz@no-reply";

            $separator = md5(time());
            // carriage return type (we use a PHP end of line constant)
            $eol = PHP_EOL;
            // main header (multipart mandatory)
            $headers = "From: " . get_bloginfo( 'name' ) . " <".$from.'>'.$eol;
            $headers .= "MIME-Version: 1.0".$eol;
            $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;

            // message
            $body = "--".$separator.$eol;
            $body .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
            $body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
            $body .= $message.$eol;

            // attachment
            $body .= "--".$separator.$eol;
            $body .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol;
            $body .= "Content-Transfer-Encoding: base64".$eol;
            $body .= "Content-Disposition: attachment".$eol.$eol;
            $body .= $attachment.$eol;
            $body .= "--".$separator."--";*/

            wp_mail( $to, 'Poppyz - ' . __( 'Invoice' ,'poppyz'), $message );


        } elseif ( $format == "EU" ) {
            $user_id = $ppy_invoice->get_invoice_field( $id, 'client_number' );
            $subs_id = $ppy_invoice->get_invoice_field( $id, 'subs_id' );
            Poppyz_Core::send_notification_email( $subs_id, $user_id, [], 'invoice');
        } elseif ( $format == "D" ) {
            /*$upload_dir = wp_upload_dir();
            $invoice_dir = $upload_dir['basedir'].'/poppyz/invoices/pdf/';

            if ( !is_dir( $invoice_dir ) ) {
                mkdir( $invoice_dir, 0755, true );
            }

            $pdf->Output($invoice_dir . get_the_title( $id ) . '.pdf', 'F');*/
            $pdf->Output(  $invoice_number . '.pdf', 'D' );
        }
        elseif ( $format == "F")  {
            $upload_dir = wp_upload_dir();
            $invoice_dir = $upload_dir['basedir'].'/poppyz/invoices/pdf/';

            if ( !is_dir( $invoice_dir ) ) {
                mkdir( $invoice_dir, 0755, true );
            }

            $pdf->Output($invoice_dir . get_the_title( $id ) . '.pdf', 'F');
        }

    }

	/**
	 * Request fields from invoice search/filtered box.
	 * @return void
	 */
	public function searchArgs() {
		$orderby = isset($_REQUEST['orderby']) ? $_REQUEST['orderby'] : 'ID';
		$order = isset($_REQUEST['order']) ? $_REQUEST['order'] : 'DESC';
		$status = isset($_REQUEST['status']) ? $_REQUEST['status'] : 'any';
		$meta_key = isset($_REQUEST['meta_key']) ? $_REQUEST['meta_key'] : null;
		$meta_value = isset($_REQUEST['meta_value']) ? $_REQUEST['meta_value'] : null;
		$search = isset($_REQUEST['s']) ? sanitize_text_field($_REQUEST['s']) : null;

		$start_date = isset($_REQUEST['start-date']) ? sanitize_text_field($_REQUEST['start-date']) : null;
		$end_date = isset($_REQUEST['end-date']) ? sanitize_text_field($_REQUEST['end-date']) : null;

		$start_invoice_number = isset($_REQUEST['start-invoice-number']) ? sanitize_text_field($_REQUEST['start-invoice-number']) : null;
		$end_invoice_number = isset($_REQUEST['end-invoice-number']) ? sanitize_text_field($_REQUEST['end-invoice-number']) : null;

		$vat_shifted = isset($_REQUEST['vat-shifted']) ? sanitize_text_field($_REQUEST['vat-shifted']) : null;


		$args = array(
			'order' => $order,
			'post_status' => $status,
			's' => $search
		);

		$meta_query_args = array();

		if ($meta_key) {
			$meta_query_args[] = array(
				'key' => $meta_key,
				'value' => $meta_value,
				'compare' => 'LIKE',
			);
		}

		if ($vat_shifted) {
			$meta_query_args[] = array(
				'key' => PPY_PREFIX . 'invoice_vatshifted',
				'value' => 1,
			);
		}

		if (!empty($start_date) && !empty($end_date)) {
			$meta_query_args[] = array(
				'key' => PPY_PREFIX . 'invoice_date_created',
				'value' => array($start_date, $end_date),
				'compare' => 'BETWEEN',
				'type' => 'DATE'
			);
		}

		if (!empty($start_invoice_number) && !empty($end_invoice_number)) {
			$meta_query_args[] = array(
				'key' => PPY_PREFIX . 'invoice_number',
				'value' => array($start_invoice_number, $end_invoice_number),
				'compare' => 'BETWEEN',
				'type' => 'NUMERIC'
			);
		}

		if ($orderby == 'title' || $orderby == 'ID') {
			$args['orderby'] = $orderby;
		} else {
			$args['meta_key'] = PPY_PREFIX . 'invoice_' . $orderby;
			$args['orderby'] = 'meta_value';
		}

		if ($orderby == 'amount') {
			$args['orderby'] = 'meta_value_num';
		}

		$meta_query_args[] =  array (
			'relation' => 'OR',
			array (
				'key' => PPY_PREFIX .'invoice_mode',
				'value' => "Live",
				'compare' => 'LIKE',
			),
			array (
				'key' => PPY_PREFIX .'invoice_mode',
				'compare' => 'NOT EXISTS',
			)
		);

		$args['posts_per_page'] =	-1;
		$args['meta_query'] = $meta_query_args;

		return $args;
	}

	/**
	 * get filtered Invoice IDs
	 * @param $count
	 * @return array
	 */
	public function getFilteredInvoiceIds( $count = false)
	{
		$args = $this->searchArgs();
		$invoices = $this->get_invoices_query($args);
		if ($count === true) {
			return $invoices->found_posts;
		}
		$ids = array();
		if ($invoices->have_posts()) {
			while ($invoices->have_posts()) {
				$invoices->the_post();
				$ids[] = get_the_ID();
			}
		}

		return $ids;
	}

    public function download( $ids = false ) {

        if ( !$ids || (isset($_REQUEST['invoice-select-all']) && $_REQUEST['invoice-select-all'] == true ) ) {
			$ids = $this->getFilteredInvoiceIds();
        }
        foreach  ( $ids as $id ) {
			$invoice_mode = get_post_meta($id, PPY_PREFIX .'invoice_mode', true);
			if ($invoice_mode == "Test") {
				continue;
			}
            $this->generate_pdf( $id, 'F' );
        }

        $upload_dir = wp_upload_dir();
        $invoice_dir = $upload_dir['basedir'].'/poppyz/invoices/';
        $zip_file =  $invoice_dir . 'invoices.zip';
        $this::zip( $invoice_dir . '/pdf', $zip_file);
        $this::delete_directory( $invoice_dir . '/pdf' );

        if( $zip_file ){
            //Set Headers:
            header('Pragma: public');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($zip_file)) . ' GMT');
            header('Content-Type: application/force-download');
            header('Content-Disposition: inline; filename="invoices.zip"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . filesize($zip_file));
            header('Connection: close');
            ob_clean();
            flush();
            if (readfile($zip_file)) {
                $this::delete_directory( $invoice_dir );
            }

            exit();
        }

    }

    public function display_invoice() {
        if ( isset( $_GET['process_invoice'] ) && !is_admin()) {
            $invoice_id =  $_GET['process_invoice'];
            if ( is_user_logged_in() ) {
                $current_user = wp_get_current_user();
                $user_id = $current_user->ID;
                $ppy_payment = new Poppyz_Payment();
                $payment = $ppy_payment->get_payment_by_invoice($invoice_id);
                if ( isset( $_GET['invoice'] ) || ( $payment && $payment->invoice_type == 'moneybird')) {
                    if ($payment && !empty($payment->invoice_id_other)) {
                        $invoice_id = $payment->invoice_id_other;
                    }
                    if ( isset( $_GET['product'] ) && defined('PPYV_PREFIX') ) {
                        $invoice_id = get_user_meta( $user_id, PPYV_PREFIX . 'mb_invoice_' . $_GET['product'] , true );
                    }

                    return $ppy_payment->process_invoice_mb( $invoice_id, isset( $_GET['product'] ) );
                } else {
                    //here process_invoice is the subs_id
                    if ( current_user_can( 'manage_options' ) || $this->is_invoice_owner( $invoice_id, $user_id ) ) {
                        $type = "I";
                        if ( isset( $_GET['download'] ) ) {
                            $type = "D";
                        }
                        $this->generate_pdf( $invoice_id, $type );
                    }
                }

            } else {
                $current_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                wp_die( 'Je dient eerst <a href="' . wp_login_url( $current_link ) . '">in te loggen</a> om je factuur te downloaden.' );
            }

        }
    }

    public static function get_price_fields( $id ) {
        $ppy_invoice = new Poppyz_Invoice();
        global $ppy_lang;
        $return = array();
        $return['tax_rate'] = (float)$ppy_invoice->get_invoice_field( $id, 'tax_rate' );
        $return['tax_rate_single'] = $ppy_invoice->get_invoice_field( $id, 'tax_rate_single' );
        $return['price_tax'] = $ppy_invoice->get_invoice_field( $id, 'price_tax' );
        $return['vat_number'] = $ppy_invoice->get_invoice_field( $id, 'vat_number' );
        $return['vatshifted'] = $ppy_invoice->get_invoice_field( $id, 'vatshifted' );
        $return['vat_out_of_scope'] = $ppy_invoice->get_invoice_field( $id, 'vat_out_of_scope' );
        $return['vat_disabled'] = $ppy_invoice->get_invoice_field( $id, 'vat_disabled' );


        $amount = (float) $ppy_invoice->get_invoice_field( $id, 'amount' );

		$tier_discount = $ppy_invoice->get_invoice_field( $id, 'tier_discount' );
		if ( !empty($tier_discount) && $tier_discount > $amount ) {
			$return['amount'] = 0;
		} else {
			$return['amount'] = $amount;
		}


        $return['tax_amount'] = 0;

        if ( $return['tax_rate'] > 0 ) {
            if ( $return['price_tax'] == 'exc' ) {
                $return['tax_amount'] =  ( round( $return['amount'] * ( $return['tax_rate'] / 100 ), 2 ) ) ;
            } else {
                $return['tax_amount'] = $return['amount'] - ( round( $return['amount'] / ( $return['tax_rate'] / 100 + 1), 2 ) ) ;
            }
        }


        if ( $return['price_tax']  == 'inc' ) {
            $return['amount'] = $return['amount'] - $return['tax_amount'];
        }

        $return['tax_amount_formatted'] = Poppyz_Core::format_price( $return['tax_amount']  );

        // if the invoice says its vat shifted, remove tax
        if ( isset( $return['vatshifted'] ) && $return['vatshifted'] == 1 ) {
            $return['total_with_tax'] = $return['amount'];
            $return['tax_amount_formatted'] = __( '0 (VAT shifted.)' ,'poppyz');
            $return['tax_rate'] = 0;
            $return['tax_amount'] = 0;
        } elseif ( isset( $return['vat_out_of_scope'] ) && $return['vat_out_of_scope'] == 1 ) {
            $return['total_with_tax'] = $return['amount'];
            $return['tax_amount_formatted'] = __( '0 (VAT out of scope.)' ,'poppyz');
            $return['tax_rate']  = 0;
            $return['tax_amount'] = 0;
        } elseif ( isset( $return['vat_disabled'] ) && $return['vat_disabled'] == 1 ) {
            $return['total_with_tax'] = $return['amount'];
            $return['tax_amount_formatted'] = __( '0 (No VAT.)' ,'poppyz');
            $return['tax_rate']  = 0;
            $return['tax_amount'] = 0;
        } else {
            $return['total_with_tax'] = $return['amount'] + $return['tax_amount'];
        }


        if ( ( !empty($return['vatshifted']) || !empty($return['vat_out_of_scope']) || !empty($return['vat_disabled']) ) && $return['price_tax'] == 'inc' && $return['tax_rate_single'] > 0 ) {
            $tax_amount = $return['amount'] - ( round( $return['amount'] / ( $return['tax_rate_single'] / 100 + 1), 2 ) ) ;
            $return['total_with_tax'] = $return['amount'] = $return['amount'] - $tax_amount;
        }
        return $return;

    }

    public function get_all_earnings( $tier = null ) {
        $ids = $this->get_all_invoice_ids( "paid" );
        $total_earnings= 0;
        foreach  ( $ids as $id ) {

            if ( $tier ) {
                $tier_id = $this->get_invoice_field( $id, 'tier_id' );
                if ( $tier != $tier_id ) continue;
            }

            $price_fields = $this->get_price_fields( $id );
            $total_earnings += $price_fields['total_with_tax'];
        }
        return Poppyz_Core::format_price( $total_earnings );
    }

    public static function zip($source, $destination) {
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true)
        {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file)
            {
                $file = str_replace('\\', '/', realpath($file));

                if (is_dir($file) === true)
                {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }
                else if (is_file($file) === true)
                {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        }
        else if (is_file($source) === true)
        {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }

    public static function delete_directory($dirname) {
        if (is_dir($dirname))
            $dir_handle = opendir($dirname);
        if (!$dir_handle)
            return false;
        while($file = readdir($dir_handle))
        {
            if ($file != "." && $file != "..")
            {
                if (!is_dir($dirname."/".$file))
                    unlink($dirname."/".$file);
                else
                    delete_directory($dirname.'/'.$file);
            }
        }
        closedir($dir_handle);
        rmdir($dirname);
        return true;
    }

    public static function get_invoice_url( $id ) {
        return site_url( '?process_invoice=' . $id );
    }


    //add additional fields in the registration and edit user pages
    function reg_invoice_fields( $fields ) {
        global $ppy_lang;
        $show_vat_shifted = Poppyz_Core::get_option( 'show_vat_shifted' );
        $show_vat_belgium = Poppyz_Core::get_option( 'vat_belgium' );
        $vat_out_of_scope = Poppyz_Core::get_option( 'vat_out_of_scope' );
        $vat_shifted_text = Poppyz_Core::get_option( 'vat_shifted_text' );
        if ( !$vat_shifted_text ) {
            $vat_shifted_text = __( 'VAT Shifted? Only check this if your company is registered outside the Netherlands and within the EU.' ,'poppyz');
        }
        if ( $show_vat_shifted == 'on' ) {
            $fields[$vat_shifted_text] = 'vatshifted';
            $fields[__( 'VAT Number' ,'poppyz')] = 'vatnumber';
        }
        if ( $show_vat_belgium == 'on' ) {
            $fields[__( 'VAT Number' ,'poppyz')] = 'vatnumber';
        }
        if ( $vat_out_of_scope == 'on' ) {
            $fields[__( 'VAT out of scope? Only check this field if your company is registered outside the EU.' ,'poppyz')] = 'vat_out_of_scope';
        }
        return $fields;
    }

    function reg_invoice_fields_html( $html, $field, $name, $value ) {
        if ( $value == 'vatshifted' ) {
            global $ppy_lang;
            $vatshifted = null;
            $show_vat_belgium = Poppyz_Core::get_option( 'vat_belgium' );
            if ( is_admin() && isset($_GET['user_id'])) { //in edit profile page
                $vatshifted = get_user_meta( $_GET['user_id'], 'ppy_vatshifted', true );
            }  else if ( is_user_logged_in() ) {
                $vatshifted = get_user_meta( get_current_user_id(), 'ppy_vatshifted', true );
            }
            if ( $show_vat_belgium == 'on' ) {
                $html .= '<input type="hidden" name="ppy_belgium" id="ppy_belgium" value="1" />';
            }
            $html .= '<fieldset><label for="reg-' . $value .'">' . __('VAT shifted','poppyz') . '</label><div class="input-wrapper checkbox"><input name="reg_' . $value .'" type="checkbox" 
                                   value="1" ' . checked('1', $vatshifted, false ) . '
                                   id="reg-' . $value .'"/><small class="cdesc" for="reg-' . $value .'">' . $name . '</small></div> 
                               </fieldset>';
        }
        if ( $value == 'vat_out_of_scope' ) {
            $vat_out_of_scope = '';
            if ( is_admin() && isset($_GET['user_id'])) { //in edit profile page
                $vat_out_of_scope = get_user_meta( $_GET['user_id'], 'ppy_vat_out_of_scope', true );
            }  else if ( is_user_logged_in() ) {
                $vat_out_of_scope = get_user_meta( get_current_user_id(), 'ppy_vat_out_of_scope', true );
            }
            $html .= '<fieldset><label for="reg-' . $value .'"></label><div class="input-wrapper checkbox"><input name="reg_' . $value .'" type="checkbox" 
                                   value="1" ' . checked('1', $vat_out_of_scope, false ) . '
                                   id="reg-' . $value .'"/><small class="cdesc" for="reg-' . $value .'">' . $name . '</small></div> 
                               </fieldset>';
        }
        return $html;
    }

    function reg_invoice_exclude( $arr ) {
        $arr[] = 'vatshifted';
        $arr[] = 'vat_out_of_scope';
        return $arr;
    }

    function reg_invoice_required( $required, $value ) {
        if ( $value == 'vatnumber' ) {
            return false;
        }
        return $required;
    }

    //delete the vatshifted boolean when vat number is empty
    function reg_invoice_fields_save( $value, $field, $user_id ) {

        if ( ( is_user_logged_in() && !is_admin() ) && $field == 'vatnumber' && $value == '' ) { //frontend
            delete_user_meta( get_current_user_id(), 'ppy_vatshifted' );
        }
        if ( ( is_user_logged_in() && !is_admin() ) && $field == 'vatshifted' && $value == 1 && empty( $_POST['reg_company'] )) { //no company means vat shifted cannot be used
            delete_user_meta( get_current_user_id(), 'ppy_vatshifted' );
        }
        if ( is_admin() && $field == 'vatnumber' && !isset( $_POST['reg_vatshifted'] ) ) { //in edit profile page, and vatshifted hasn't been checked
            delete_user_meta( $user_id, 'ppy_vatshifted' );
        }
        if ( ( is_user_logged_in() && !is_admin() ) && $field == 'vat_out_of_scope' && $value == '' ) { //frontend
            delete_user_meta( get_current_user_id(), 'ppy_vat_out_of_scope' );
        }
    }

    function reg_invoice_fields_exclude_validation( $exclude ) {
        $exclude[] = 'vatshifted';
        $exclude[] = 'vatnumber';
        $exclude[] = 'vat_out_of_scope';
        return $exclude;
    }

    public function export_xml( $args = array() ) {
        $defaults = array(
            'post_status' => 'all',
            'posts_per_page' => -1,
			'meta_query' => array (
				'relation' => 'OR',
				array (
					'key' => PPY_PREFIX .'invoice_mode',
					'value' => "Live",
					'compare' => 'LIKE',
				),
				array (
					'key' => PPY_PREFIX .'invoice_mode',
					'compare' => 'NOT EXISTS',
				)
			)
        );
		if ( isset($_REQUEST['invoice-select-all']) && $_REQUEST['invoice-select-all'] == true ) {
			$args = $this->searchArgs();
		}
        $args = wp_parse_args( $args, $defaults );
        $invoices = Poppyz_Invoice::get_invoices( $args );
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        $invoices_xml = new SimpleXMLElement('<source></source>');
        $invoices_xml->addChild('publisher', $_SERVER['HTTP_HOST']);
        $invoices_xml->addChild('publisherurl', $host);
        $invoices_xml->addChild('lastBuildDate', date('r'));


        if ( !$invoices ) return;
        $fields = array( 'product', 'company', 'firstname', 'lastname', 'address', 'zipcode', 'city', 'date_created', 'date_expired', 'tax_rate', 'price_tax', 'vat_number', 'amount' );
        foreach ( $invoices as $invoice ) {
            $id = $invoice->ID;
            $price_fields = Poppyz_Invoice::get_price_fields( $id );
            $invoice_xml = $invoices_xml->addChild('invoice');
            $invoice_xml->addChild('number', $invoice->post_title );
            $invoice_xml->addChild('status', $invoice->post_status );
            $invoice_xml->addChild('created', $invoice->post_modified );
            foreach ( $fields as $field ) {
                $invoice_xml->addChild( $field, get_post_meta( $id,  PPY_PREFIX . 'invoice_' . $field, true ) );
            }
            foreach ( $price_fields as $price_field => $value ) {
                if ( $price_field == 'tax_amount_formatted' ) $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');

                $invoice_xml->addChild( $price_field, $value );
            }
        }
        ob_end_clean();
        header_remove();
        header('Content-disposition: attachment; filename=invoices.xml');
        header ("Content-Type:text/xml");
        echo $invoices_xml->asXML();
        exit();
    }

    public function export_csv_poppyz( $args = array() ) {
        $defaults = array(
            'post_status' => 'all',
            'posts_per_page' => -1,
			'meta_query' => array (
				'relation' => 'OR',
				array (
					'key' => PPY_PREFIX .'invoice_mode',
					'value' => "Live",
					'compare' => 'LIKE',
				),
				array (
					'key' => PPY_PREFIX .'invoice_mode',
					'compare' => 'NOT EXISTS',
				)
			)
        );
        $args = wp_parse_args( $args, $defaults );
        $invoices = Poppyz_Invoice::get_invoices( $args );

        if ( !$invoices ) return;
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="invoices.csv"');
        $fp = fopen('php://output', 'w');

        //header
        $headers = array( 'number', 'status', 'created', 'product', 'company', 'firstname', 'lastname', 'address', 'zipcode', 'city', 'date_created', 'date_expired', 'tax_rate', 'price_tax', 'vat_number', 'amount', 'tax_amount', 'total_with_tax', 'currency' );
        fputcsv($fp, $headers, ',');

        $fields = array( 'product', 'company', 'firstname', 'lastname', 'address', 'zipcode', 'city', 'date_created', 'date_expired', 'tax_rate', 'price_tax', 'vat_number', 'amount' );
        foreach ( $invoices as $invoice ) {
            $id = $invoice->ID;
            $price_fields = Poppyz_Invoice::get_price_fields( $id );
            $invoice_data =  array( $invoice->post_title, $invoice->post_status, $invoice->post_modified ) ;
            foreach ( $fields as $field ) {
                $inv_array = get_post_meta( $id,  PPY_PREFIX . 'invoice_' . $field, true );
                array_push( $invoice_data, $inv_array );
            }
            foreach ( $price_fields as $price_field => $value ) {
                if ( in_array( $price_field, array( 'tax_amount', 'total_with_tax' ) ) ){
                    //if ( $price_field == 'tax_amount_formatted' ) $value = html_entity_decode ($value);
                    array_push($invoice_data, $value);
                }
            }
            array_push( $invoice_data, $currency = Poppyz_Core::get_option( 'currency' ) );
            //$data[] = $invoice_data;
            fputcsv($fp, $invoice_data, ',');
        }
        fclose($fp);
        exit();
    }

    public function export_csv( $args = array() ) {
        $defaults = array(
            'post_status' => 'all',
            'posts_per_page' => -1,
			'meta_query' => array (
				'relation' => 'OR',
				array (
					'key' => PPY_PREFIX .'invoice_mode',
					'value' => "Live",
					'compare' => 'LIKE',
				),
				array (
					'key' => PPY_PREFIX .'invoice_mode',
					'compare' => 'NOT EXISTS',
				)
			)
        );
		if ( isset($_REQUEST['invoice-select-all']) && $_REQUEST['invoice-select-all'] == true ) {
			$args = $this->searchArgs();
		}
        $args = wp_parse_args( $args, $defaults );
        $invoices = Poppyz_Invoice::get_invoices( $args );

        if ( !$invoices ) return;
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="invoices.csv"');
        $fp = fopen('php://output', 'w');

        //header
        $headers = array( 'invoice_date', 'debtor_name1', 'debtor_name2', 'debtor_address1', 'debtor_address2', 'debtor_zipcode', 'debtor_city', 'debtor_email', 'invoice_id', 'invoice_number', 'product_description', 'product_unit_price_ex_vat', 'product_vat_rate', 'product_quantity', 'product_subtotal_ex_vat', 'product_subtotaal_in_vat', 'invoice_type', 'debtor_country', 'debtor_vat_number', 'product_goods_or_services', 'invoice_pay_date' );
        $this::fputcsv2($fp, $headers, ';', '"');

        $fields = array( 'name', 'name2', 'address', 'address2', 'zipcode', 'city', 'email', 'invoice_id', 'invoice_number', 'product', 'amount', 'tax_rate' );
        foreach ( $invoices as $invoice ) {
            $id = $invoice->ID;
            $price_fields = Poppyz_Invoice::get_price_fields( $id );
            $date_created = date( 'Y-m-d', strtotime( $this->get_invoice_field( $id, 'date_created' ) ) );
            $invoice_data = array( $date_created );

            foreach ( $fields as $field ) {
                if ( $field == 'name' ) {
                    $first_name = $this->get_invoice_field( $id, 'firstname' );
                    $last_name = $this->get_invoice_field( $id, 'lastname' );
                    $inv_val = $first_name . ' ' . $last_name;
                } else if ( $field == 'invoice_id' || $field == 'invoice_number' ) {
                    $inv_val = $this->get_invoice_number( $id );
                } elseif ( $field == 'email') {
                    $inv_val = $this->get_invoice_field( $id, 'email' );
                    if ( $inv_val == "" ) {
                        $user_id = $this->get_invoice_field( $id, 'client_number' );
                        $user = get_userdata ( $user_id );
                        $inv_val = $user->user_email;
                    }
                } else {
                    $inv_val = get_post_meta( $id,  PPY_PREFIX . 'invoice_' . $field, true );
                }
                array_push( $invoice_data, $inv_val );
            }

            //product quantity
            array_push( $invoice_data, 1 );

            //subtotal exvat
            array_push( $invoice_data, $price_fields['amount'] );

            //subtotal with tax
            array_push( $invoice_data, $price_fields['total_with_tax'] );

            //invoice type
            array_push( $invoice_data, 1 );

            //debtoor country
            array_push( $invoice_data, $this->get_invoice_field( $id, 'country' ) );

            //vat number
            array_push( $invoice_data, $price_fields['vat_number'] );

            //product or services
            array_push( $invoice_data, 'Product' );

            //invoice pay date | same as date created
            array_push ( $invoice_data,  $date_created );

            //array_push( $invoice_data, $currency = Poppyz_Core::get_option( 'currency' ) );
            //$data[] = $invoice_data;
            $this::fputcsv2($fp, $invoice_data, ';');
        }
        fclose($fp);
        exit();
    }

    public static function fputcsv2($fp, $fields, $delimiter = ';', $enclosure = '"', $mysql_null = false) {
        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');
        $output = array();
        foreach ($fields as $field) {
            if ($field === null && $mysql_null) {
                $output[] = 'NULL';
                continue;
            }
            if ( is_numeric( $field ) && !is_float( $field ) ) {
                $output[] = $field;
                continue;
            }
            // original
            // $output[] = preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field) ? (
            //     $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure
            // ) : $field;
            $output[] = preg_match("/(?:{$delimiter_esc}|{$enclosure_esc}|\s)/", $field) ? (
                $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure
            ) : "\"{$field}\"";
        }
        fwrite($fp, join($delimiter, $output) . "\n");
    }

    public static function rename_duplicate_titles() {
        do {
            global $wpdb;
            $invoices = $wpdb->get_results( $wpdb->prepare( "SELECT ID, post_title FROM {$wpdb->posts} WHERE post_type = %s", PPY_INVOICE_PT ), ARRAY_A );

            $arr = array();

            if (!$invoices) return false;

            foreach ( $invoices as $invoice ) {
                $arr[$invoice['ID']] = $invoice['post_title'];
                $titles[] = $invoice['post_title'];
            }

            $has_duplicates = count($titles) !== count(array_unique($titles));

            if ( $has_duplicates ) {
                $arr_val = array();
                foreach ( $arr as $key => $val ) {
                    if ( isset( $arr_val[$val] ) ) {
                        if ( ( $pos = strpos( $val, "-") ) !== FALSE ) { //has been changed already, add + 1
                            $index = substr($val, $pos + 1);
                            $new_index = $index + 1;
                            $post_title = str_replace("-" . $index, "-" . $new_index, $val);
                        } else {
                            $index = 2;
                            $post_title = $val . '-' . $index;
                        }
                        $my_post = array(
                            'ID' => $key,
                            'post_title' => $post_title,
                        );
                        wp_update_post($my_post);
                    } else {
                        $arr_val[$val] = 0;
                    }
                }
                error_log('renamed duplicate titles');
                $titles = $wpdb->get_col($wpdb->prepare("SELECT post_title FROM {$wpdb->posts} WHERE post_type = %s", PPY_INVOICE_PT));
                $has_duplicates = count($titles) !== count(array_unique($titles));
            }
        } while ( $has_duplicates );
    }
}
