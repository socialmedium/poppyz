<?php

namespace Poppyz\Includes;

class Options {

    public $options;
    public $reset;

    public function __construct($options = null) {
        if ($options == null) {
            $options =  get_option( 'ppy_settings' );
        }
        $this->options = $options;
        $this->reset = false;
    }

    /**
     * @param $this->options
     * @return mixed
     */
    public function setDefaultTextOptions($reset = false) {
        $this->reset = $reset;

        if (self::shouldSaveDefault($this->options['ty_email_subject'])) {
            $this->options['ty_email_subject'] = __('Thank you for your purchase.', 'poppyz');
        }

        if (self::shouldSaveDefault($this->options['ty_email_message'])) {
            $this->options['ty_email_message'] = __(
                'Hi [first-name],
            <p>Welcome to the program [tier]. Click on this link to view it directly: [tier-link].</p>
            <p>You will receive the login details in another email. If you have not received it, you can create a password here: [password-link].</p>
            <p>You will also receive the invoice in another email or view it here: [invoice-link]</p>
            <br />Regards,'
                , 'poppyz');
        }

        if (self::shouldSaveDefault($this->options['new_purchase_email_subject'])) {
            $this->options['new_purchase_email_subject'] = __('Hurray! Someone bought from you!', 'poppyz');
        }
        if (self::shouldSaveDefault($this->options['new_purchase_email_message'])) {
            $this->options['new_purchase_email_message'] = __(
                'Customer: [first-name] [last-name], [user-email] <br />
                    Product: [course] [tier] <br />
                    Price: [amount] <br />
                    Invoice: [invoice-link]'
                , 'poppyz');
        }

        /*if ( self::shouldSaveDefault( $this->options['reminder_email_message'] ) ) {
            $this->options['reminder_email_message'] = __(
                'Hoi [first-name],
				<br />
				<p>Op [due-date] worden de kosten van [tier] geïncasseerd d.m.v. SEPA incasso.</p>
				<p>Het bedrag dat zal worden afgeschreven is [amount].</p>
				<p>Je unieke machtigingskenmerk door ons toegekend is [customer-id].</p>
				<br />
				Hartelijke groet,'
            );
        }*/

        if (self::shouldSaveDefault($this->options['reminder_email_subject'])) {
            $this->options['reminder_email_subject'] = __('Collection reminder for [tier]', 'poppyz');
        }
        if (self::shouldSaveDefault($this->options['reminder_email_message'])) {
            $this->options['reminder_email_message'] = __(
                'Hi [first-name],
                    <br />
                    <p> On [due-date], the costs of [tier] will be collected by means of SEPA direct debit. </p>
                    <p> The amount that will be debited is [amount]. </p>
                    <p> Your unique customer identifier is: [customer-id]. </p>
                    <p> You do not have to take any further action yourself. </p>
                    <br />
                    Sincerely,'
                , 'poppyz');
        }

        if (self::shouldSaveDefault($this->options['reminder_manual_email_subject'])) {
            $this->options['reminder_manual_email_subject'] = __('Your invoice is ready to be paid', 'poppyz');
        }
        if (self::shouldSaveDefault($this->options['reminder_manual_email_message'])) {
            $this->options['reminder_manual_email_message'] = __(
                'Hi [first-name],
                <br />
                <p>There is an invoice for [tier] and the amount is [amount]. This expires on [due-date]. Please go to your account page to view and pay for it. I kindly request you to pay the invoice.</p>
                <p>For questions you can contact us by e-mail.</p>
                <br />
                Sincerely,'
                , 'poppyz');
        }


        if (self::shouldSaveDefault($this->options['overdue_email_subject'])) {
            $this->options['overdue_email_subject'] = __('Your payment term has expired', 'poppyz');
        }
        if (self::shouldSaveDefault($this->options['overdue_email_message'])) {
            $this->options['overdue_email_message'] = __(
                'Hi [first-name],
                     <br />
                     <p> Your payment term has expired on [due-date]. Please pay this as soon as possible via this link: [payment-link]. </p>
                     <p> To view your subscriptions and possibly other payments, please visit this link: [account-link] </p>
                     <br />
                     Sincerely,'
                , 'poppyz');
        }


        if (self::shouldSaveDefault($this->options['invoice_email_subject'])) {
            $this->options['invoice_email_subject'] = __('Your invoice', 'poppyz');
        }
        if (self::shouldSaveDefault($this->options['invoice_email_message'])) {
            $this->options['invoice_email_message'] = __(
                'Hi [first-name],
                    <br />
                    <p>Hereby your invoice: [invoice-link]</p>
                    <br />
                    Sincerely,'
                , 'poppyz');
        }

		if (self::shouldSaveDefault($this->options['scheduled_donation_notification_email_message'])) {
			$this->options['scheduled_donation_notification_email_message'] = __(
				'Hi [first-name],
                    <br />
                    <p>Donation purchase link: [tier-purchase-url]</p>
                    <br />
                    Sincerely,'
				, 'poppyz');
		}


        if (self::shouldSaveDefault($this->options['insufficient_funds_email_subject'])) {
            $this->options['insufficient_funds_email_subject'] = __('Direct debit failed due to insufficient funds', 'poppyz');
        }
        if (self::shouldSaveDefault($this->options['insufficient_funds_email_message'])) {
            $this->options['insufficient_funds_email_message'] = __(
                'Hi [first-name],
                    <br />
                    <p>Your payment for the program round [tier] associated with course [course] has failed due to insufficient funds in your bank account. You can still pay for it by going to your account page [account-link].</p>
                    <br />
                    Sincerely,'
                , 'poppyz');
        }

        if (self::shouldSaveDefault($this->options['failed_payment_email_subject'])) {
            $this->options['failed_payment_email_subject'] = __('Debit collection canceled', 'poppyz');
        }
        if (self::shouldSaveDefault($this->options['failed_payment_email_message'])) {
            $this->options['failed_payment_email_message'] = __('Your payment for the tier [tier] which belongs to course [course] has failed .The SEPA direct debit has stopped, you can do the remaining payments manually, go to your account page for the overview.', 'poppyz');
        }


        if (self::shouldSaveDefault($this->options['failed_payment_admin_email_subject'])) {
            $this->options['failed_payment_admin_email_subject'] = __('Canceled collection', 'poppyz');
        }
        if (self::shouldSaveDefault($this->options['failed_payment_admin_email_message'])) {
            $this->options['failed_payment_admin_email_message'] = __('A SEPA direct debit has failed, it concerns [tier] belonging to course [course]. Your customer must now make the remaining payments manually on his/her account page where the overview of payments can be viewed. Your customer will receive an email of this event as well. Please contact your customer: [first-name] [last-name] - [user-email]', 'poppyz');
        }

        if (self::shouldSaveDefault($this->options['failed_payment_admin_email_message'])) {
            $this->options['failed_payment_admin_email_message'] = __('A SEPA direct debit has failed, it concerns [tier] belonging to course [course]. Your customer must now make the remaining payments manually on his/her account page where the overview of payments can be viewed. Your customer will receive an email of this event as well. Please contact your customer: [first-name] [last-name] - [user-email]', 'poppyz');
        }
        if (self::shouldSaveDefault($this->options['failed_payment_admin_email_message'])) {
            $this->options['failed_payment_admin_email_message'] = __('A SEPA direct debit has failed, it concerns [tier] belonging to course [course]. Your customer must now make the remaining payments manually on his/her account page where the overview of payments can be viewed. Your customer will receive an email of this event as well. Please contact your customer: [first-name] [last-name] - [user-email]', 'poppyz');
        }

        if (self::shouldSaveDefault($this->options['pending_email_subject'])) {
            $this->options['pending_email_subject'] = __('Payment failed', 'poppyz');
        }
        if (self::shouldSaveDefault($this->options['pending_email_message'])) {
            $this->options['pending_email_message'] = __(
                'Hi [first-name],<br />
            <p>You have started purchasing [tier]. However, you have not completed the purchase, perhaps because something went wrong. If so, you can email me. I will gladly help you further! </p>
            <br />
            Sincerely,
            ', 'poppyz');
        }

        if (self::shouldSaveDefault($this->options['registration_form_description'])) {
            $this->options['registration_form_description'] = __('Please complete the fields below. We need this information for billing purposes and account creation.', 'poppyz');
        }
		$bullets_default_content = "
								<ul>
									<li>Lorem ipsum dolor sit amet consectetur adipiscing</li>
									<li>Praesent elementum augue id nunc efficitur</li>
									<li>Cras porttitor eros venenatis aliquet tortor at faucibus</li>
									<li>Pellentesque dui nisl, suscipit id nunc ac</li>
								</ul>
								";
		if (self::shouldSaveDefault($this->options['registration_form_extra_content'])) {
			$this->options['registration_form_extra_content'] = __($bullets_default_content, 'poppyz');
		}

        if (self::shouldSaveDefault($this->options['terms_checkbox'])) {
            unset($this->options['terms_checkbox']);
            $this->options['terms_checkbox'][] = __('I agree with the Terms and Conditions (make a link)', 'poppyz');
            $this->options['terms_checkbox'][] = __('I agree with the Privacy statement (make a link).', 'poppyz');
            $this->options['terms_checkbox'][] = __('I agree that my data will be used to send updates and access the lessons.', 'poppyz');
        }

        if (self::shouldSaveDefault($this->options['free_purchase_button_text'])) {
            $this->options['free_purchase_button_text'] = __('Free - Subscribe now!', 'poppyz');
        }

        if ($this->reset) {
            $this->options['insufficient_funds_email_message'] =  __( 'Thank you for your purchase.'  ,'poppyz');
            $this->options['failed_payment_email_message'] =  __( 'Your payment for the tier [tier] which belongs to course [course] has failed .The SEPA direct debit has stopped, you can do the remaining payments manually, go to your account page for the overview.' ,'poppyz');
            $this->options['failed_payment_admin_email_message'] =  __( 'A SEPA direct debit has failed, it concerns [tier] belonging to course [course]. Your customer must now make the remaining payments manually on his/her account page where the overview of payments can be viewed. Your customer will receive an email of this event as well. Please contact your customer: [first-name] [last-name] - [user-email]' ,'poppyz');

            $this->options['purchase_page_content'] =  '';
            $this->options['purchase_button_text'] =  '';

            $this->options['registration_form_description'] =  '';
			$this->options['registration_form_extra_content'] =  '';
            $this->options['review_page_content']  = '';
            $this->options['invoice_page_content'] =  '';

            $this->options['return_page_success_title'] =  __( 'Thank you for your purchase.'  ,'poppyz');
            $this->options['return_page_success_content'] =  __( "You can now start your course. Click [course-link] to begin." ,'poppyz');
            $this->options['return_page_failed_content'] =  "<h2>" .  __( 'You were not able to purchase the course.' ,'poppyz')   . "</h2>";

            $this->options['course_not_accessible_text'] =   __("Sorry, you do not have access to this course." ,'poppyz');
            $this->options['lesson_not_yet_available_text'] =  __( 'Sorry, this lesson isn\'t available yet. It will be available on: [start-date]' ,'poppyz');
            $this->options['lesson_excluded_text'] =  __( 'Sorry, this lesson is excluded from the course that you\'re subscribed to.' ,'poppyz');
            $this->options['lesson_not_accessible_text'] = __("Sorry, you do not have access to this lesson." ,'poppyz');
            $this->options['tier_limit_reached_text'] =  __( 'The number of subscriptions allowed for this tier has been reached.' ,'poppyz');
            $this->options['tier_expired_text'] =   __("Sorry, this tier already expired." ,'poppyz');

            $this->options['terms_condition_title'] =  __( 'Terms and conditions'  ,'poppyz');
            $this->options['terms_condition'] =  __( 'If you order a tier from [company], you agree to the terms and conditions of [company].' ,'poppyz');
            $this->options['sepa_text'] = __( '<strong>Name: </strong> {Name} <br /> <strong> Address: </strong> {Address} <br /> <strong> Postal code / city: </strong> {POSTCODE} <br /> <strong> Country: </strong> {COUNTRY} <br /> <p> After approval, you give permission to {COMPANY}: <ul> <li> to send direct debit orders to your bank for an amount of your to debit your account and </li> <li> your bank to debit an amount from your account on an ongoing basis in accordance with the order of {COMPANY}. </li> </ul> </p> <p> If you do not agree with this charge, you can have it reversed. Please contact your bank within eight weeks of the debit. Please sk your bank for the conditions. </p>'  ,'poppyz') ;
            $this->options['sepa_text_after'] =  __( '<strong>Name: </strong> {Name} <br /> <strong> Address: </strong> {Address} <br /> <strong> Postal code / city: </strong> {POSTCODE} <br /> <strong> Country: </strong> {COUNTRY} <br /> <p> With your completed payment you give permission to {COMPANY}: <ul> <li> to send direct debit orders to your bank for an amount debit from your account and </li> <li> your bank to continuously debit an amount from your account in accordance with the instruction of {COMPANY}. </li> </ul> </p> <p> If if you do not agree with this charge, you can have it reversed. Please contact your bank within eight weeks of the debit. Please ask your bank for the conditions. </p>'  ,'poppyz');
            $this->options['sepa_checkbox'] = __( 'I agree with the terms of automatic charging' ,'poppyz' ) ;
            $this->options['vat_shifted_text'] = __( 'VAT Shifted? Only check this if your company is registered outside the Netherlands and within the EU.' ,'poppyz' ) ;
            $this->options['terms_checkbox_required'] = __( 'You must first tick all check boxes to continue.' ,'poppyz' );
			$this->options['purchase_expiration_date'] = __( 'Purchase expiration date' ,'poppyz' );
			$this->options['closure_after_purchase'] = __( 'Closure after purchase' ,'poppyz' );
            update_option( 'ppy_settings' , $this->options );

        }

        return $this->options;
    }

    public function shouldSaveDefault($option) {
        return !isset($option) || $this->reset;
    }

}