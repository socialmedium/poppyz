<?php

namespace Poppyz\Includes\Newsletters;

class Mailchimp implements NewsletterInterface {
    private $apiKey;
    private $apiUrl;

    public function initialize() {
        $this->apiKey = \Poppyz_Core::get_option( 'mc_api_key' );

        $index = strrpos($this->apiKey, "-");
        if( $index === false ) {
            return false;
        }
        $dc = substr( $this->apiKey, $index + 1  );
        $this->apiUrl = 'https://' . $dc . '.api.mailchimp.com/3.0/';
        return true;
    }

    public function getLists() {
        $target = 'lists?count=100';
        $response = wp_remote_get(  $this->apiUrl . $target ,
            array(
                'timeout' => 10,
                'user-agent' => 'PHP-MCAPI/2.0',
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'apikey ' . $this->apiKey
                ),
                'sslverify' => false
            )
        );
        
        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }

        return json_decode( $response['body'] );
    }

    public function getFormattedLists() {
        $formatted_list = [];
        $lists = $this->getLists();
        if ( is_object($lists) || is_array($lists) && !is_wp_error( $lists->lists  ) ) {
            foreach ( $lists->lists as $list ) {
                $formatted_list[$list->id] = $list->name;
            }
        } else {
            return $lists;
        }
        return $formatted_list;
    }

    public function subscribe( $user_id, $tier_id, $args) {
        $user = get_user_by( 'id', $user_id );

        $list_id = get_post_meta( $tier_id, PPY_PREFIX . 'mailchimp_list', true );
        $tags = get_post_meta( $tier_id, PPY_PREFIX . 'mailchimp_tags', true );

        $index = strrpos($this->apiKey, "-");
        if( $index === false || empty( $list_id ) ||  $list_id == '-1' ) return false;

        $dc = substr( $this->apiKey, $index + 1  );
        $data = array(
            'apikey'        => $this->apiKey,
            'email_address' => $user->user_email,
            'status'        => 'subscribed'
        );
        if ( !empty($user->first_name) ) {
            $data['merge_fields']['FNAME'] = $user->first_name;
        }
        if ( !empty($user->last_name) ) {
            $data['merge_fields']['LNAME'] = $user->last_name;
        }

        if ($tags) {
            $tags = preg_replace('/\s*,\s*/', ',', $tags ); //remove spaces before and after commas
            $tags = trim($tags, ',');
            $tags = explode(',', $tags);
            $data['tags'] = $tags;
        }

        $data = apply_filters( 'ppy_mailchimp_custom_fields', $user_id, $list_id, $data);

        $url = 'https://' . $dc . '.api.mailchimp.com/3.0/';

        $auth = base64_encode( 'user:'. $this->apiKey );
        $json_data = json_encode( $data );

        $user_email = md5($user->user_email);
        $target = 'lists/' . $list_id .  '/members/' . $user_email;

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url . $target );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Basic '.$auth ) );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0' );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );

        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );

        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );

        $result = curl_exec( $ch );
        curl_close( $ch );

        $result = json_decode( $result );

        if ( $result->status != '404' || $result->status == 'subscribed' ) {
            //if subscribed, add tag to the member
            if (isset($data['tags']) && is_array($data['tags'])) {
                $formatted_tags = array();
                foreach ( $data['tags'] as $tag) {
                    $formatted_tags[] = [
                        "name" => $tag,
                        "status" => 'active'
                    ];
                }
                $body = [
                    'tags' => $formatted_tags,
                ];
                $body = wp_json_encode( $body );
                $response = wp_remote_post(  $url . $target . '/tags',
                    array(
                        'timeout' => 10,
                        'headers' => array(
                            'User-Agent' => 'PHP-MCAPI/2.0',
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Basic '.$auth
                        ),
                        'sslverify' => false,
                        'body' => $body,
                        'data_format' => 'body',
                    )
                );
            }

            //if subscribed, just update the user information on mailchimp
            $type = 'PATCH';
            $target = 'lists/' . $list_id .  '/members/' . $result->id;

        } else {
            //if not, add a new mailchimp subscriber
            $type = 'POST';
            $target = 'lists/' . $list_id . '/members/';
        }

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url . $target );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Basic '.$auth ) );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0' );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );

        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $type );

        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data );

        $result = curl_exec( $ch );
        curl_close( $ch );

        $result = json_decode( $result );

        return $result;
    }

}