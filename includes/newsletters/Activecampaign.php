<?php

namespace Poppyz\Includes\Newsletters;

class Activecampaign implements NewsletterInterface {
    private $apiKey;
    private $apiUrl;

    public function initialize() {
        $this->apiKey =  \Poppyz_Core::get_option( 'ac_api_key' );
        $this->apiUrl =  \Poppyz_Core::get_option( 'ac_api_url' );

        if ( !$this->apiKey || !$this->apiUrl ) return false;

        $this->apiUrl .= '/admin/api.php?';
        return true;
    }
    public function getLists() {
        $params = array(
            'api_key'      => $this->apiKey,
            'api_action'   => 'list_list',
            'api_output'   => 'json',
            'ids' => 'all'
        );


        $query = "";
        foreach( $params as $key => $value ) $query .= $key . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');


        //set_time_limit(30);

        // define a final API request - GET
        $response = wp_remote_get(  $this->apiUrl . $query,
            array(
                'timeout' => 10,
                'user-agent' => 'PHP-acAPI/2.0',
                'sslverify' => false
            )
        );

        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }

        return json_decode( $response['body'] );
    }

    public function getFormattedLists() {
        $formatted_list = false;
        $lists = $this->getLists();
        if ( is_object( $lists ) ) {
            foreach ( $lists as $list ) {
                if (is_object( $list ) && isset( $list->name ) )
                    $formatted_list[$list->id] = $list->name;
            }
        } else {
            $formatted_list = $lists;
        }
        return $formatted_list;
    }

    public function subscribe( $user_id, $tier_id, $args) {


        $user = get_user_by( 'id', $user_id );
        $company = get_user_meta( $user_id, PPY_PREFIX . 'company', true);
        $phone = get_user_meta( $user_id, PPY_PREFIX . 'phone', true);
        $list_id = get_post_meta( $tier_id, PPY_PREFIX . 'av_list', true);

        $tags = get_post_meta( $tier_id, PPY_PREFIX . 'av_tags', true);
        if ( !$tags ) {
            $tags = \Poppyz_Core::get_option( 'nl_tags' );
        }

        if ( !$list_id ) return false;

        $params = array(
            'api_key'      => $this->apiKey,
            'api_action'   => 'contact_sync',
            'api_output'   => 'json',
        );

        $post = array(
            'email'                    => $user->user_email,
            'first_name'               => $user->first_name,
            'last_name'                => $user->last_name,
            'p[' . $list_id .']'                   => $list_id, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
            'status[' . $list_id . ']'              => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 1)
        );


        if ( !empty( $company ) ) {
            $post['orgname'] = $company;
        }
        if ( !empty( $phone ) ) {
            $post['phone'] = $phone;
        }

        if ( !empty( $tags ) ) {
            $post['tags'] = rtrim( $tags, ',' );
        }

        // This section takes the input fields and converts them to the proper format
        $query = "";
        foreach( $params as $key => $value ) $query .= $key . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        $data = "";
        foreach( $post as $key => $value ) $data .= $key . '=' . urlencode($value) . '&';
        $data = rtrim($data, '& ');

        $response = wp_remote_post( $this->apiUrl . $query, array(
                'method' => 'POST',
                'httpversion' => '1.0',
                'sslverify' => false,
                'body' => $data
            )
        );

        //error_log( $response['body'] . 'xxx' );

        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }

        return json_decode( $response['body'] );

    }

}