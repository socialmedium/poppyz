<?php
namespace Poppyz\Includes\Newsletters;

class Newsletter {

    protected static $_services = [];
    protected $newsletter;
    protected $user;
    protected $tier;
    protected $data;

    /**
     * @param $service_alias
     * @return NewsletterInterface|null
     *
     */
    /*public static function getService( $service_alias ) {
        $service_alias = strtolower( $service_alias );

        if( isset( self::$_services[ $service_alias ] ) )
            return self::$_services[ $service_alias ];

        $class_name = "Poppyz\Includes\Newsletters\\" . ucfirst( $service_alias );
        if( !class_exists( $class_name ) )
            return null;

        self::$_services[ $service_alias ] = new $class_name();

        return self::$_services[ $service_alias ];
    }*/

    public function __construct( NewsletterInterface $newsletter ) {
        $this->newsletter = $newsletter;
    }

    public function setNewsletter( NewsletterInterface  $newsletter ) {
        $this->newsletter = $newsletter;
        return $this->newsletter;
    }

    public function setUser( $user_id ) {
        $this->user = get_userdata( $user_id );
    }

    public function setTier( $tier_id ) {
        $this->tier = get_post( $tier_id );
    }

    public function setData( $args ) {
        $this->data = $args;
    }

    public function getLists() {
        if (!$this->newsletter->initialize()) return false;
        return $this->newsletter->getLists();
    }
    public function getFormattedLists() {
        if (!$this->newsletter->initialize()) return false;
        return $this->newsletter->getFormattedLists();
    }

    public function subscribe() {
        if (!$this->newsletter->initialize()) return false;
        if ( !$this->user || !$this->tier ) {
            throw new \Exception('User and/or tier has not been set');
        }
        return $this->newsletter->subscribe( $this->user->ID, $this->tier->ID, $this->data );
    }

    public static function subscribeAll( $user_id, $tier_id ) {
        $newsletter = new self( new Mailchimp() );
        $newsletter->setUser( $user_id );
        $newsletter->setTier( $tier_id );
        $newsletter->subscribe();

        $newsletter->setNewsletter(new Aweber());
        $newsletter->subscribe();

        $newsletter->setNewsletter(new Laposta());
        $newsletter->subscribe();

        $newsletter->setNewsletter(new Mailerlite());
        $newsletter->subscribe();

        $newsletter->setNewsletter(new Activecampaign());
        $newsletter->subscribe();
    }

}