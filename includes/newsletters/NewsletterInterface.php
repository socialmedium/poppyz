<?php
namespace Poppyz\Includes\Newsletters;

interface NewsletterInterface {
    public function initialize();
    public function subscribe($user_id, $tier_id, $args);
    public function getLists();
    public function getFormattedLists();
}