<?php

namespace Poppyz\Includes\Newsletters;

class Laposta implements NewsletterInterface {
    private $apiKey;
    private $apiUrl;

    public function initialize() {
        $this->apiKey = \Poppyz_Core::get_option( 'la_api_key' );;
        if (!$this->apiKey) return false;
        $url = 'https://api.laposta.nl/';
        $version = 'v2/';
        $this->apiUrl = $url . $version;
        return true;
    }
    public function getLists() {
        $target = 'list';
        
        $response = wp_remote_get(  $this->apiUrl . $target ,
            array(
                'timeout' => 10,
                'headers' => array(
                    'Authorization' => 'Basic ' . base64_encode( $this->apiKey . ':'),
                ),
                'sslverify' => false
            )
        );
        
        $body = json_decode($response['body']);
        if ( isset( $body->error ) ) {
            return $body->error->message;
        }
        
        return json_decode( $response['body'] );
    }

    public function getFormattedLists() {
        $formatted_list = false;
        $la = $this->getLists();

        if ( is_object( $la ) ) {
            foreach ( $la as $lists ) {
                if ( count( $lists ) > 0 ) {
                    foreach ($lists as $list) {
                        if ($list->list->state == 'active') {
                            $formatted_list[$list->list->list_id] = $list->list->name;
                        }
                    }
                }
            }
        } else {
            return $la;
        }
        return $formatted_list;
    }

    public function subscribe( $user_id, $tier_id, $args ) {
        $target = 'member';
        $list_id = get_post_meta( $tier_id, PPY_PREFIX . 'la_list', true );

        if ( empty($list_id) || $list_id == '-1') {
            return false;
        }
        $user = get_user_by( 'id', $user_id );
        require_once( PPY_DIR_PATH . 'public/class-poppyz-user.php' );
        $custom_fields = \Poppyz_User::custom_fields();
        foreach ( $custom_fields as $name => $value) {
            $$value = get_user_meta( $user->ID, PPY_PREFIX . $value, true );
        }

        $data = [
            'email' => $user->user_email,
            'list_id' => $list_id,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'custom_fields' => [
                'name' => $user->first_name . ' ' . $user->last_name,
                'company' => $company,
                'city' => $city,
                'country' => $country,
                'phone' => $phone,
                'zip' => $zipcode,
            ]
        ];

        $data = http_build_query( $data );
        $response = wp_remote_post(  $this->apiUrl . $target ,
            array(
                'timeout' => 10,
                'headers' => array(
                    'Authorization' => 'Basic ' . base64_encode( $this->apiKey . ':'),
                ),
                'sslverify' => false,
                'body' => $data,
                'data_format' => 'body',
            )
        );


        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }

        return json_decode( $response['body'] );

    }

}