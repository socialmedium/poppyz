<?php

namespace Poppyz\Includes\Newsletters;

class Aweber implements NewsletterInterface {
    private $apiKey;
    private $aweber;
    private $account;

    public function initialize() {
        $this->apiKey = \Poppyz_Core::get_option( 'aw_code' );

        if ( empty( $this->apiKey )  ) {
            return false;
        }

        $consumer_key = \Poppyz_Core::get_option( 'aw_consumer_key' );
        $consumer_secret = \Poppyz_Core::get_option( 'aw_consumer_secret' );
        $access_key = \Poppyz_Core::get_option( 'aw_access_key' );
        $access_secret = \Poppyz_Core::get_option( 'aw_access_secret' );

        if ( !class_exists('AWeberAPI') ) {
            require_once(PPY_DIR_PATH . 'includes/aweber_api/aweber_api.php');
        }
        try {
            $this->aweber = new \AWeberAPI($consumer_key, $consumer_secret);
            $this->account = $this->aweber->getAccount($access_key, $access_secret);
        } catch ( \AWeberAPIException $exc ) {
            error_log($exc->message);
            return false;
        }
        return true;
    }

    public static function getAuthorizationLink() {
        return 'https://auth.aweber.com/1.0/oauth/authorize_app/da214c21';
    }

    public function getLists() {
        try {
            return $this->account->lists;
        } catch(\AWeberAPIException $exc) {
            error_log($exc->message);
            return false;
        }
    }

    public function getFormattedLists() {
        $formatted_list = false;
        $lists = $this->getLists();
        if (empty($lists)) return false;
        foreach ($lists as $list) {
            $formatted_list[$list->id] = $list->name;
        }
        return $formatted_list;
    }

    public function subscribe( $user_id, $tier_id, $args) {
        try {
            $user = get_user_by( 'id', $user_id );
            $list_id = get_post_meta( $tier_id, PPY_PREFIX . 'aweber_list', true);

            if ( !$list_id ) return false;
            $listURL = '/accounts/' . $this->account->data['id'] .  '/lists/' . $list_id;
            $list = $this->account->loadFromUrl($listURL);
            $name = $user->first_name . ' ' . $user->last_name;

            $subscribers = $list->subscribers;

            //if already subscribed, just update the name
            /*$params = array( 'status' => 'subscribed', 'email' => $user->user_email );
            $found_subscribers = $subscribers->find( $params );
            foreach($found_subscribers as $subscriber) {
                if( $subscriber->email == $user->user_email ) {
                    return false; //the user is already subscribed to this list, don't continue
                }
            }*/

            # create a subscriber
            $params = array(
                'email' => $user->user_email,
                'name' => $name,
            );
            $new_subscriber = $subscribers->create( $params );
        } catch( \AWeberAPIException $exc ) {
            ob_start();
            echo 'Aweber error: ';
            var_dump($exc);
            $contents = ob_get_contents();
            ob_end_clean();
            error_log($contents);
            return false;
        }
    }

}