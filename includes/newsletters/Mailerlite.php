<?php

namespace Poppyz\Includes\Newsletters;

class Mailerlite implements NewsletterInterface {
    private $apiKey;
    private $apiUrl;

    public function initialize() {
        $this->apiKey = \Poppyz_Core::get_option( 'ml_api_key' );
        $this->apiUrl = 'http://api.mailerlite.com/api/v2/';
        if ( !$this->apiKey || !$this->apiUrl ) return false;
        return true;
    }

    public function getLists() {
        $target = 'groups';
        $response = wp_remote_get(  $this->apiUrl . $target ,
            array(
                'timeout' => 10,
                'headers' => array(
                    'User-Agent' => 'MailerLite PHP SDK/2.0',
                    'Content-Type' => 'application/json',
                    'X-MailerLite-ApiKey' => $this->apiKey
                ),
                'sslverify' => false
            )
        );

        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }

        return json_decode( $response['body'] );
    }

    public function getFormattedLists() {
        $formatted_list = false;
        $lists = $this->getLists();
        if (is_array($lists)){
            foreach ( $lists as $list ) {
                $formatted_list[$list->id] = $list->name;
            }
        } else {
            return $lists->error->message;
        }

        return $formatted_list;
    }

    public function subscribe( $user_id, $tier_id, $args) {

        $list_id = get_post_meta( $tier_id, PPY_PREFIX . 'mailerlite_list', true );
        $target = (!empty( $list_id ) && $list_id !== '-1' ) ? 'groups/' . $list_id . '/subscribers' : 'subscribers';

        $user = get_user_by( 'id', $user_id );
        require_once( PPY_DIR_PATH . 'public/class-poppyz-user.php' );
        $custom_fields = \Poppyz_User::custom_fields();
        foreach ( $custom_fields as $name => $value) {
            $$value = get_user_meta( $user->ID, PPY_PREFIX . $value, true );
        }
        $data = [
            'email' => $user->user_email,
            'fields' => [
                'name' => $user->first_name,
                'last_name' =>  $user->last_name,
                'company' => $company,
                'city' => $city,
                'country' => $country,
                'phone' => $phone,
                'zip' => $zipcode,
            ]
        ];

        $data = wp_json_encode( $data );
        $response = wp_remote_post(  $this->apiUrl . $target ,
            array(
                'timeout' => 10,
                'headers' => array(
                    'User-Agent' => 'MailerLite PHP SDK/2.0',
                    'Content-Type' => 'application/json',
                    'X-MailerLite-ApiKey' => $this->apiKey
                ),
                'sslverify' => false,
                'body' => $data,
                'data_format' => 'body',
            )
        );

        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        }

        return json_decode( $response['body'] );
    }

}