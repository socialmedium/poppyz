<?php
class Poppyz_Analytics {

    public $ga_id;
    public $ga_set_domain_name;
    public $ga_standard_tracking_enabled;
    public $ga_use_universal_analytics;
    public $ga_anonymize_enabled;
    public $ga_ecommerce_tracking_enabled;

    public function __construct() {
        $constructor = $this->init_options();
        // Contains snippets/JS tracking code
        include_once('Analytics/analytics-js.php');
        Poppyz_Analytics_JS::get_instance($constructor);

        add_action('wp_head', array($this, 'tracking_code_display'), 999999);
        add_action('ppy_subscription_deleted', array($this, 'delete_tracked_status'));
    }

    public function init_options() {
        $options = array(
            'ga_id',
            'ga_set_domain_name',
            'ga_standard_tracking_enabled',
            'ga_use_universal_analytics',
            'ga_anonymize_enabled',
            'ga_ecommerce_tracking_enabled',
        );

        $constructor = array();
        foreach ($options as $option) {
            $constructor[$option] = $this->$option = Poppyz_Core::get_option($option);
        }

        return $constructor;
    }

    public function tracking_code_display() {
        $display_ecommerce_tracking = false;

        if (/*is_admin() || current_user_can('manage_options') || */!$this->ga_id) {
            return;
        }
        if (is_page() && 'on' === $this->ga_ecommerce_tracking_enabled) {
            $order_id = isset($_GET['order_id']) ? $_GET['order_id'] : 0;
            $ppy_sub = new Poppyz_Subscription();
            $order = $ppy_sub->get_subscription_by_order($order_id, get_current_user_id());

            if ($order) {
                if ($order->payment_status == 'paid') {
                    $display_ecommerce_tracking = true;
                    echo $this->get_ecommerce_tracking_code($order);
                    update_post_meta($order->id, '_ga_tracked_subscription', 1);
                }
            }
        }

        if (Poppyz_Core::is_poppyz_page() && !$display_ecommerce_tracking) {
            $display_ecommerce_tracking = true;
            echo $this->get_standard_tracking_code();
        }
        if (!$display_ecommerce_tracking && 'on' === $this->ga_standard_tracking_enabled) {

            echo $this->get_standard_tracking_code();
        }
    }

    protected function get_standard_tracking_code() {
        return "<!-- Poppyz Google Analytics Integration -->
                " . Poppyz_Analytics_JS::get_instance()->header() . "
                <!-- /Poppyz Google Analytics Integration -->";
    }

    protected function get_ecommerce_tracking_code($order) {

        $order_details = array();
        require_once(PPY_DIR_PATH . 'public/class-poppyz-user.php');
        $custom_fields = Poppyz_User::custom_fields();
        foreach ($custom_fields as $name => $value) {
            $$value = get_user_meta($order->user_id, PPY_PREFIX . $value, true);
        }

        $order_details['id'] = $order->id;
        $order_details['order_id'] = $order->order_id;
        $order_details['course_id'] = $order->course_id;
        $order_details['tier_id'] = $order->tier_id;
        $order_details['name'] = Poppyz_Core::generate_payment_title($order->id, $order->amount, false);
        $order_details['total'] = $order->amount;
        $order_details['city'] = $city;
        $order_details['address'] = $address;
        $order_details['country'] = $country;

        $code = Poppyz_Analytics_JS::get_instance()->add_transaction($order_details);

        return "
                <!-- Poppyz Google Analytics E-Commerce Integration -->
                " . Poppyz_Analytics_JS::get_instance()->header() . "
                <script type='text/javascript'>$code</script>
                <!-- /Poppyz Google Analytics E-Commerce Integration -->
                ";
    }

    public function delete_tracked_status($order_id) {
        delete_post_meta($order_id, '_ga_tracked_subscription');
    }
}

new Poppyz_Analytics();