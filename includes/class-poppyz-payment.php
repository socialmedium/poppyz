<?php

class Poppyz_Payment {

    /**
     * The Mollie version
     *
     * @since    0.5.0
     * @access   protected
     * @var      string    $version    The version of the Moneybird API.
     */
    protected $version;


    protected $admin_id;

    /**
     * The Mollie object
     *
     * @since    0.5.0
     * @access   protected
     * @var      string    $mollie    The object of Mollie_API_Client.
     */
    public $mollie;

    /**
     * The subscription table
     *
     * @since    0.5.0
     * @access   private
     * @var      string    $subs_table    The subscription table.
     */
    private $subs_table;

    /**
     * The subscription payments table
     *
     * @since    0.5.0
     * @access   private
     * @var      string    $payments_table    The subscription payments table.
     */
    private $payments_table;


    public function __construct() {
        global $wpdb;
        /*$this->version = "v2";
        $this->admin_id = "114668";*/
        $this->subs_table = $wpdb->prefix . 'course_subscriptions';
        $this->payments_table = $wpdb->prefix . 'course_subscriptions_payments';

    }

    public function _initialize_mollie($paymentMode = "") {
        require_once PPY_DIR_PATH . "includes/Mollie/vendor/autoload.php";
        require_once( PPY_DIR_PATH . 'public/class-poppyz-user.php' );

        $this->mollie = new \Mollie\Api\MollieApiClient();

		if (self::hasNoMollieKey()) {
			//if Mollie API live and test are empty
			print 'No API Key has been supplied for Mollie.';
			exit;

		} else if ( empty(Poppyz_Core::get_option( 'mo_api_key')) || Poppyz_Core::get_option( 'mo_api_key') == "" ) {
			//If Mollie API live is empty and test API key is not empty
			$this->mollie->setApiKey( Poppyz_Core::get_option( 'mo_api_test_key' ) );
			$data['payment_mode'] = "Test";

		} else if ( !empty(current_user_can( 'manage_options' )) && isset($_POST['mollieTestMode']) && $_POST['mollieTestMode'] == true ) {
			//if user is admin and the test mode is checked.
			$this->mollie->setApiKey( Poppyz_Core::get_option( 'mo_api_test_key' ) );
			$data['payment_mode'] = "Test";

		}  else if ( !current_user_can( 'manage_options' ) && isset($_POST['mollieTestMode']) && $_POST['mollieTestMode'] == true ) {
			//if user is not admin but the mollie test mode is being forced to check.
			$this->mollie->setApiKey( Poppyz_Core::get_option( 'mo_api_key' ) );
			$data['payment_mode'] = "Live";

		} else if ( Poppyz_Core::get_option("mollieTestMode") == "on" && current_user_can( 'manage_options' )) {
			//if admin option test mode is on and user is in admin role.
			$this->mollie->setApiKey( Poppyz_Core::get_option( 'mo_api_test_key' ) );
			$data['payment_mode'] = "Test";

		} else if (!empty($paymentMode)) {
			//condition is based on the payment mode.
			$mo_key = $paymentMode == "Test" ? Poppyz_Core::get_option( 'mo_api_test_key' ) : Poppyz_Core::get_option( 'mo_api_key' );
			$this->mollie->setApiKey( $mo_key );

		} else {
			$this->mollie->setApiKey( Poppyz_Core::get_option( 'mo_api_key' ) );
			$data['payment_mode'] = "Live";
		}

		if ( isset ($_POST[PPY_PREFIX . 'subs_id']) && isset($data['payment_mode']) && Poppyz_Core::column_exists( 'payment_mode' )) {
			Poppyz_Subscription::update_subscription( "update", $_POST[PPY_PREFIX . 'subs_id'], $data );
		}

    }

    /**
     * Initialize the Moneybird API with your API key.
     *
     * See: https://www.mollie.nl/beheer/account/profielen/
     *
     * @since     1.0.0
     */
    public function _initilalize_moneybird_v2()
    {
        $mb_admin_id = Poppyz_Core::get_option('mb_admin_id');
        $mb_api_key = Poppyz_Core::get_option('mb_api_key');
        if (strlen($mb_admin_id) < 10 || !is_numeric($mb_admin_id) ) {
            return false;
        }
        require_once(PPY_DIR_PATH . 'includes/Moneybird_v2/ApiConnector.php');
        spl_autoload_register('emilebons\moneybird\ApiConnector::autoload');
        return array('mb_admin_id' => $mb_admin_id, 'mb_api_key' => $mb_api_key);
    }

    public static function is_mb_activated() {
        $mb_admin_id = Poppyz_Core::get_option('mb_admin_id');
        $mb_api_key = Poppyz_Core::get_option('mb_api_key');
        if (empty($mb_admin_id) || empty($mb_api_key)) {
            return false;
        }
        return true;
    }

    public function register_payment_mb( $amount, $invoice_id, $payment_id = null ) {

        $mb_options = $this->_initilalize_moneybird_v2();

        if ($mb_options == false) {
            return false;
        }
        $salesInvoiceFactory = new emilebons\moneybird\SalesInvoice\SalesInvoiceFactory( $mb_options['mb_admin_id'], null, $mb_options['mb_api_key'] );
        $payment_info = array( 'payment' =>
            array(
                'payment_date' => date(DATE_ATOM, time()),
                'price' => $amount,
                'transaction_identifier' => $payment_id
            )
        );


        try {
            $salesInvoiceFactory->registerPayment( $invoice_id, $payment_info );
            $salesInvoiceFactory->save();
        }
        catch(\Moneybird\Exception $exception) {
            Poppyz_Core::log('Moneybird register payment error: ' . $exception->getMessage(), 'register_payment_mb', true );
        }
    }

    public function process_invoice_mb( $invoice_id, $skip_ownership_check = false ) {
        if ( !$invoice_id ) {
            print 'Sorry, invoice not found.';
            exit();
        }
        if ( !$skip_ownership_check ) {
            $current_user = wp_get_current_user();
            $is_owner = $this->is_mb_invoice_owner( $current_user->ID, $invoice_id );

            if ( !$is_owner ) {
                print 'Sorry, this is not your invoice.';
                exit();
            }
        }

        try {
            $mb_options = $this->_initilalize_moneybird_v2();

            if ($mb_options == false) return false;

            $salesInvoiceFactory = new emilebons\moneybird\SalesInvoice\SalesInvoiceFactory( $mb_options['mb_admin_id'], null, $mb_options['mb_api_key'] );

            $salesInvoiceFactory->getInvoiceById( $invoice_id );
            $file = $salesInvoiceFactory->getPDF();

            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="invoice.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');

            readfile($file);
        } catch(\Moneybird\Exception $exception) {
            error_log($exception->getMessage());
        }

    }

    public function is_mb_invoice_owner( $user_id, $invoice_id ) {
        global $wpdb;
        $subs_table = $wpdb->prefix . "course_subscriptions";
        $payments_table = $wpdb->prefix . "course_subscriptions_payments";

        $is_owner =  $wpdb->get_var( $wpdb->prepare(
            "
                    SELECT s.id
                    FROM $subs_table s
                    INNER JOIN $payments_table p
                    ON s.id = p.subscription_id
                    WHERE p.invoice_id = %d OR p.invoice_id_other = %s
                    AND s.user_id = %d
                    ",
            $invoice_id,
            $invoice_id,
            $user_id
        ) );
        return apply_filters( 'ppy_is_mb_invoice_owner', $is_owner, $user_id, $invoice_id );
    }

    /**
     * @return array|null
     */
    public function get_invoice_profiles() {
        try {
            $mb_options = $this->_initilalize_moneybird_v2();
            if ($mb_options == false) return null;
            $documentStyleFactory = new emilebons\moneybird\DocumentStyle\DocumentStyleFactory( $mb_options['mb_admin_id'], null, $mb_options['mb_api_key'] );
            return $documentStyleFactory->listAll();

        } catch (Exception $e) {
            $profiles = null;
        }

        return $profiles;
    }

    public function get_invoice_workflows() {
        try {
            $mb_options = $this->_initilalize_moneybird_v2();
            if ($mb_options == false) return null;
            $workflowFactory = new emilebons\moneybird\Workflow\WorkflowFactory( $mb_options['mb_admin_id'], null, $mb_options['mb_api_key'] );
            return $workflowFactory->listAll();

        } catch (Exception $e) {
            $profiles = null;
        }

        return $profiles;
    }

    public function get_invoice_taxrates($filter = '') {

        try {
            $mb_options = $this->_initilalize_moneybird_v2();
            if ($mb_options == false) return null;
            $taxrateFactory = new emilebons\moneybird\Taxrate\TaxrateFactory( $mb_options['mb_admin_id'], null, $mb_options['mb_api_key'] );
            return $taxrateFactory->listAll($filter);

        } catch (Exception $e) {
            $taxrate = null;
        }

        return $taxrate;
    }

    public function get_invoice_categories() {
        try {
            $mb_options = $this->_initilalize_moneybird_v2();
            if ($mb_options == false) return null;
            $ledgerAccountFactory = new emilebons\moneybird\LedgerAccount\LedgerAccountFactory( $mb_options['mb_admin_id'], null, $mb_options['mb_api_key'] );
            $categories = $this->get_invoice_filtered_categories($ledgerAccountFactory);
            return $categories;
        } catch (Exception $e) {
            $categories = null;
        }

        return $categories;
    }

    public function get_invoice_filtered_categories( $ledgerAccountFactory ) {
        $categories = array();
        foreach ( $ledgerAccountFactory->getById() as $category ) {
            if ( !empty( $category['account_type'] ) && $category['account_type'] == 'revenue' )
                $categories[] = array( 'id' => $category['id'], 'name' => $category['name'] );
        }
        return json_decode(json_encode($categories)); //convert to object so it has the same structure as the others
    }

    public function create_invoice_mb( $user_id, $price, $tier_id, $subs_id = 0, $coupon_code =  null, $poppyz_invoice_id = '' ) {

        //moneybird version 2
        $mb_options = $this->_initilalize_moneybird_v2();
        if ($mb_options == false) return false;

        $user = get_userdata( $user_id );
        $course_id = Poppyz_Core::get_course_by_tier( $tier_id );
        global $ppy_lang;

        //check if the user is already a moneybird contact
        $contact_id = get_user_meta( $user_id, PPY_PREFIX . 'mb_contact_id', true );

        $custom_fields = Poppyz_User::custom_fields();
        foreach ( $custom_fields as $name => $value) {
            $$value = get_user_meta( $user_id, PPY_PREFIX . $value, true );
        }

        $contact_name =  $user->user_firstname . ' ' . $user->user_lastname;

        if ( empty( $company ) ) {
            $company = $contact_name;
        }

        $mb_tax_included = Poppyz_Core::get_price_tax( $tier_id ) == 'inc';

        $mb_tax_rate = Poppyz_Core::get_option( 'invoice_taxrate' );
        $invoice_category = Poppyz_Core::get_option( 'invoice_category' );
        $tier_category_mb =  get_post_meta( $tier_id, PPY_PREFIX . 'tier_category_mb', true );
        $vatshifted =  Poppyz_Invoice::is_vatshifted( $user_id ) ? ' (' . __('VAT shifted','poppyz') .')' : '';
        $vat_out_of_scope =  Poppyz_Invoice::is_vat_out_of_scope( $user_id ) ? 1 : 0;
        if ( get_post_meta( $tier_id, PPY_PREFIX . 'tier_tax_rate_mb', 'true' ) && get_post_meta( $tier_id, PPY_PREFIX . 'tier_tax_rate_mb', 'true' ) != "" ) {
            $mb_tax_rate = get_post_meta( $tier_id, PPY_PREFIX . 'tier_tax_rate_mb', 'true' );
        }
        if ( $tier_category_mb && $tier_category_mb != '-1' ) {
            $invoice_category = $tier_category_mb;
        }

        //if ( empty( $payment_method ) ) $payment_method = 'standard';

        $invoice_profile_id = ( Poppyz_Core::get_option( 'invoice_profile' ) != false ) ? Poppyz_Core::get_option( 'invoice_profile' ) : 1 ;

        //this only happens to users created from the admin, make sure they have first names and last names:
        $name = $user->user_firstname . $user->user_lastname;
        if ( empty( $name ) ) {
            //echo sprintf( __( 'It appears that you have not entered your name on your profile. Please complete your profile <a href="%s">here</a>.','poppyz'), get_edit_profile_url() );
            //exit;
        }

        $contact_id = get_user_meta( $user_id, PPY_PREFIX . 'mb_contact_id', true );

        $contact_array = array (
            'company_name' => $company,
            'firstname' =>  $user->user_firstname,
            'lastname' => $user->user_lastname,
            'address1' =>  $address,
            'zipcode' => $zipcode,
            'city' => $city,
            'country' => $country,
            'email' => $user->user_email,
            'phone' => $phone,
        );
        if ( get_user_meta( $user_id, PPY_PREFIX . 'vatnumber', true ) ) {
            $contact_array['tax_number'] = get_user_meta( $user_id, PPY_PREFIX . 'vatnumber', true );
        }

        $contact_data = array( 'contact' => $contact_array);

        $contactFactory = new emilebons\moneybird\Contact\ContactFactory( $mb_options['mb_admin_id'], null, $mb_options['mb_api_key'] );
        if ( $contact_id ) {
            $contact_data['id'] = $contact_id;
        }

        $contactFactory->setData( $contact_data );

        try {
			$contact = $contactFactory->save();
        } catch (Exception $e) {
            if ($e->getCode() == '400' || $e->getCode() == '404') {
                delete_user_meta( $user_id, PPY_PREFIX . 'mb_contact_id' );
                unset( $contact_id['id'] );
                $contactFactory->setData( $contact_data );
                $contact = $contactFactory->save();
                error_log( 'contact id not found: ' . $e->getCode() . '  - ' .  $e->getMessage() );
				//echo __( 'Your MoneyBird contact information cannot be found, creating a new one... ' ,'poppyz') . '<script>window.location.reload()</script>';
            } else {
                error_log( $e->getCode() . ' cid- ' .  $e->getMessage() );
            }
        }

        update_user_meta( $user_id, PPY_PREFIX . 'mb_contact_id', $contact->id );

        //Invoice creation
        $salesInvoiceFactory = new emilebons\moneybird\SalesInvoice\SalesInvoiceFactory( $mb_options['mb_admin_id'], null, $mb_options['mb_api_key'] );

        $invoice_title_simple = get_the_title( $course_id ) . ' - ' . get_the_title( $tier_id );
        $ppy_sub = new Poppyz_Subscription();
        $subs = $ppy_sub->get_subscription_by_id($subs_id);
        $payments_made =  $subs->payments_made;
        $normal_amount = $subs->amount;
        $payment_method = get_post_meta( $tier_id, PPY_PREFIX . 'payment_method', true );
        $invoice_title = Poppyz_Core::generate_payment_title( $subs_id, $normal_amount, true );
        $payment_number = get_post_meta( $tier_id, PPY_PREFIX .  'number_of_payments', true );
        $payment_method = $subs->payment_method;

        $payment_plan_text = '';
        if ( $payment_method == 'plan' ) {
            global $ppy_lang;
            if ( $subs_id ) {
                $payment_plan_text =  ' - ' . sprintf( __( 'Payment Plan: %d of %d ' ,'poppyz'), $payments_made, $payment_number);
            } else {
                $payment_plan_text = ' - ' . sprintf( __( 'Payment Plan: 1 of %d' ,'poppyz'), $payment_number);
            }
        }

        if ( $coupon_code ) {
            $payment_plan_text = $payment_plan_text . ' ' . __( 'Coupon' ,'poppyz') . ': ' . $coupon_code;
        }

        $poppyz_invoice_number = '';
        if ( $poppyz_invoice_id ) {
            $poppyz_invoice_title = get_the_title( $poppyz_invoice_id );
            if ( $poppyz_invoice_title ) {
                $poppyz_invoice_number = ' #' . $poppyz_invoice_title;
            }
        }

        if (!$mb_tax_included && !$vatshifted && !$vat_out_of_scope) { //since we already have the computed total amount with tax, we should retrieve the old value to correct taxation in MoneyBird
            $tax_amount = Poppyz_Core::get_fixed_tax_amount( $price, $tier_id, 'inc' );
            $price = $price - $tax_amount;
        }

        $details_args = array(
            'description' =>  $invoice_title . ' ' . $payment_plan_text . $vatshifted,
            'price' => $price
        );

        if ( $mb_tax_rate ) {
            $details_args['tax_rate_id'] = $mb_tax_rate;
        }

        if ( $vatshifted || $vat_out_of_scope ) {
            $mb_zero_tax_rate = $this->get_invoice_taxrates(['percentage' => 0 ]);
            if ( !empty($mb_zero_tax_rate) ) {
                $details_args['tax_rate_id'] = $mb_zero_tax_rate[0]->id;
            }
        }
        if ( !Poppyz_Core::is_same_country( $user_id ) ) {
            $user_country = get_user_meta($user_id, 'ppy_country', true) ?: 'NL';
            $trc_data = get_option( 'trc_data' );
            if ($trc_data) {
                if ( isset( $trc_data[$user_country] ) ) {
                    $mb_zero_tax_rate = $this->get_invoice_taxrates(['percentage' => $trc_data[$user_country] ]);
                    $details_args['tax_rate_id'] = $mb_zero_tax_rate[0]->id;
                }
            }
        }

        if ( $invoice_category > 0 ) {
            $details_args['ledger_account_id'] = $invoice_category;
        }
        $salesInvoiceData = array(
            'sales_invoice' =>
                array(
                    'reference' =>  $poppyz_invoice_number,
                    'contact_id' => $contact->id,
                    'details_attributes' => array( $details_args ),
                    'currency' => Poppyz_Core::get_currency()
                )
        );

        $workflow_name = 'invoice_workflow';
        /*if ( $payment_method == 'membership' || $payment_method == 'plan' ){
            // new subscription, first payment
            if ( $subs_id == 0 ) {
                $workflow_name = 'invoice_workflow_fp';
            } else {
                $workflow_name = 'invoice_workflow_auto';
            }
        }*/
        $invoice_workflow_id = ( Poppyz_Core::get_option( $workflow_name ) != false ) ? Poppyz_Core::get_option( $workflow_name ) : false;

        $tier_workflow_id = get_post_meta( $tier_id, PPY_PREFIX . 'tier_workflow', 'true' );
        if ( $tier_workflow_id && $tier_workflow_id != '-1' ) {
            $invoice_workflow_id = $tier_workflow_id;
        }

        if ( $invoice_profile_id != 1 ) {
            $salesInvoiceData['sales_invoice']['document_style_id'] = $invoice_profile_id;
        }
        if ( $invoice_workflow_id ) {
            $salesInvoiceData['sales_invoice']['workflow_id'] = $invoice_workflow_id;
        }


        $salesInvoiceData['sales_invoice']['prices_are_incl_tax'] = $mb_tax_included;

        try {
            $salesInvoiceFactory->setData($salesInvoiceData);

            $salesInvoice = $salesInvoiceFactory->save();

            $salesInvoiceFactory->getInvoiceById($salesInvoice->id);
            $salesInvoiceFactory->send();

            $invoice_id = $salesInvoice->id;
            //$total_unpaid = $salesInvoice->totalUnpaid;
        } catch (Exception $e) {
            echo $e->getMessage();
            error_log( 'create invoice error: ' . $e->getCode() . '  - ' .  $e->getMessage() );
            exit();
        }
        return $invoice_id;
    }



    public function is_subscription_owner( $subs_id, $user_id ) {
        global $wpdb;

        $sql = $wpdb->prepare(
            "
            SELECT user_id
            FROM $this->subs_table
            WHERE id = %d
            AND user_id = %d
	      ",
            $subs_id,
            $user_id
        );
        return $wpdb->get_var(
            $sql
        );

    }

    public function get_price_by_invoice( $invoice_id ) {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare(
                "
            SELECT amount
            FROM $this->subs_table
            WHERE invoice_id = %s
	      ",
                $invoice_id
            )
        );
    }

    /**
     * Process the payment
     *
     * @since     1.0.0
     * @return    null
     */
    public function process_payment() {

        if ( isset( $_POST[PPY_PREFIX . 'purchase'] ) ) {
            $this->checkout();
        }

        //payment has taken place
        if ( isset( $_GET['process-order'] ) ) {
            try {
                $this->process_order();
            }
            catch (Exception $e) {
                Poppyz_Core::log( htmlspecialchars($e->getMessage()), 'process_order_catch', true );
            }
        }

        if ( isset( $_GET['process-order-subscription'] ) ) {
            try {
                $this->process_order_subscription();
            }
            catch (Exception $e) {
                Poppyz_Core::log( htmlspecialchars($e->getMessage()), 'process_order_subscription_catch', true );
            }
        }
    }

    public function process_order() {
		$ppy_sub = new Poppyz_Subscription();
		$subscription = $ppy_sub->get_subscription_by_payment_id( $_POST["id"] );

		if(!Poppyz_Core::column_exists( 'payment_mode' )) {
			$this->_initialize_mollie();
			$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
		} else {
			$this->_initialize_mollie( $subscription->payment_mode );
			if ($subscription->payment_mode == "Test") {
				$mo_key = Poppyz_Core::get_option( 'mo_api_test_key' );
			} else {
				$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
			}
		}

        Poppyz_Core::log('Webhook called, payment_id: ' .  $_POST["id"], 'process_order', true );
		$mo_test_key = Poppyz_Core::get_option( 'mo_api_test_key' );
		if ( empty( $mo_key ) && empty($mo_test_key) )  {
			print 'No API Key has been supplied for Mollie.';
			exit;
		}
        global $wpdb;
        $payment  = $this->mollie->payments->get( $_POST["id"] );
        $meta_invoice_id = !empty( $payment->metadata->invoice_id ) ? $payment->metadata->invoice_id : null;
        $payment_number = $payment->metadata->payment_number;
        $subscription = $ppy_sub->get_subscription_by_payment_id( $_POST["id"] );

        if (!$subscription) {
            Poppyz_Core::log('Subscription not found, payment_id: ' . $_POST["id"], 'process_order', true );
            echo 'Subscription not found, payment_id: ' . $_POST["id"];
            return false;
        }
        $subs_id = $subscription->id;
        $tier_id = $subscription->tier_id;
        $user_id = $subscription->user_id;

        $payments_made = $subscription->payments_made;
        $payment_id = $payment->id;


        if ( $payment->isPaid() && !$payment->hasRefunds() && !$payment->hasChargebacks() ) {
			if ( !empty($payment->metadata->on_demand) ) {
				sleep(1);
			}
            if ( $payment_id == $this->get_last_payment_id( $subs_id ) ){
                echo 'double posting payment id: ' . $payment_id;
                error_log('double posting payment id: ' . $payment_id );
                return false;
            } //already has been paid, prevent double posting
			$db_payment_id = $this->get_last_payment_id( $subs_id );

            $update = $this->process_first_payment( $subscription, $payment );

            $this->process_invoice( $subscription, $payment );

            $additional_product_subscriptions = !empty( $payment->metadata->additional_product_subscriptions ) ? $payment->metadata->additional_product_subscriptions : null;
            if ($additional_product_subscriptions) {
                $fields = array(
                    'status' => "on",
                    'payment_id' => $payment_id,
                    'payment_status' => 'paid',
                    'payments_made' => 1,
					'payment_mode' => $subscription->payment_mode
                );

                $fields_format = array(
                    '%s',
                    '%s',
                    '%s',
                    '%d',
					'%s'
                );
                foreach (explode(',', $additional_product_subscriptions) as $additional_product_subscription) {
                    $wpdb->update(
                        $this->subs_table,
                        $fields,
                        array( 'id' => $additional_product_subscription ),
                        $fields_format,
                        array( '%d' )
                    );
                    //only process an invoice if it's not a bundled product type since have tiers that will be free
                    if (empty( $payment->metadata->has_bundled_products ) ) {
                        $this->process_invoice($ppy_sub->get_subscription_by_id($additional_product_subscription), $payment);
                    }
                }
            }

			//process payment for additional eproduct
			$additional_eproduct_subscriptions = !empty( $payment->metadata->additional_eproduct_subscriptions ) ? $payment->metadata->additional_eproduct_subscriptions : null;
			if ($additional_eproduct_subscriptions) {
				foreach (explode(',', $additional_eproduct_subscriptions) as $additional_eproduct_subscription) {
					$ppvv_payment = new Poppyz_Verkoop_Payment();
					$ppvv_payment->process_additional_eproduct_payment( $payment, $additional_eproduct_subscription );
				}
			}

			$donation = get_user_meta( $user_id, PPY_PREFIX . 'donation_amount_'.$subs_id, true);

            Poppyz_Core::subscribe_newsletter( $user_id, $tier_id );
            if ( !$payment_number ) {
                Poppyz_Subscription::send_thank_you_message( $subs_id, $user_id ); //only send on first payment
                Poppyz_Core::send_notification_email( $subs_id, $user_id, array('admin' => true, 'donation' => $donation), 'new_purchase');
            }
            do_action( 'ppy_payment_completed', $subs_id, $payment->metadata );
            return $update;

        } elseif ($payment->isFailed() ) {
            /*error_log('Enter fail on standard');
            ob_start();
            var_dump($payment);
            $contents = ob_get_contents();
            ob_end_clean();
            error_log($contents);*/

            $this->process_failed_payment( $subs_id, $payment, $payments_made );
            Poppyz_Core::log( print_r( $payment, true ), 'order_process_payment_failed', true );
        } elseif ( $payment->hasChargebacks() || $payment->hasRefunds() ){
            /*error_log('Enter Chargedback');
            ob_start();
            var_dump($payment->details);
            $contents = ob_get_contents();
            ob_end_clean();
            error_log($contents);*/

            $this->process_chargeback( $subs_id, $meta_invoice_id , $payment->id );
            $this->cancel_membership ( $subs_id );
            Poppyz_Core::log( print_r( $payment, true), 'chargeback_or_refunds', true );
        } elseif ( $payment->isOpen() == false ) {
            //The payment isn't paid and isn't open anymore. We can assume it was aborted.
            do_action( 'ppy_payment_cancelled', $subs_id );
        } else {
            Poppyz_Core::log( print_r( $payment, true), 'payment_status_unknown', true );
        }

    }

    public function process_order_subscription() {
        Poppyz_Core::log('Webhook called, start process order for payment_id: ' .  $_POST["id"], 'process_order_subscription' );

		$ppy_sub = new Poppyz_Subscription();
		global $ppy_lang;
		$subs_id = $_GET['subs_id'];
		$subs = $ppy_sub->get_subscription_by_id( $subs_id );

		if(!Poppyz_Core::column_exists( 'payment_mode' )) {
			$this->_initialize_mollie();
			$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
		} else {
			$this->_initialize_mollie( $subs->payment_mode );
			if ($subs->payment_mode == "Test") {
				$mo_key = Poppyz_Core::get_option( 'mo_api_test_key' );
			} else {
				$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
			}
		}

		$mo_test_key = Poppyz_Core::get_option( 'mo_api_test_key' );
		if ( empty( $mo_key ) && empty($mo_test_key) )  {
            print 'No API Key has been supplied for Mollie.';
            exit;
        }

        //do not process if a test mollie payment hook is called in a live mollie account setting and vice versa
        if (strpos($mo_key, 'live_') === 0 && isset( $_GET['testmode'] ) ) {
            Poppyz_Core::log('Testmode called to live setting: ' .  $_POST["id"], 'mismatch' );
            return;
        }
        if (strpos($mo_key, 'test_') === 0 && empty( $_GET['testmode'] ) ) {
            Poppyz_Core::log('Live called to testmode:  ' .  $_POST["id"], 'mismatch' );
            return;
        }


        if ( !$subs ) {
            Poppyz_Core::log( 'Subscription not found: ' . $subs_id, 'process_order_subscription', true );
            return false;
        }
        $tier_id = $subs->tier_id;
        $course_id = $subs->course_id;
        $user_id = $subs->user_id;
        $payments_made = $subs->payments_made;
        $payment_method = $subs->payment_method;
        $mollie_customer_id = $subs->mollie_customer_id;
        $version = $subs->version;
		$payments_mode = $subs->payment_mode;
        $vatshifted = isset( $subs->vatshifted ) ?  $subs->vatshifted : 0;
        $vat_out_of_scope = isset( $subs->vat_out_of_scope ) ?  $subs->vat_out_of_scope : 0;

        $payment  = $this->mollie->payments->get( $_POST["id"] );

        $price = $ppy_sub->compute_discounted_price( $subs_id );
        $total_with_tax = Poppyz_Core::price_with_tax( $price, $tier_id, $user_id );
        $coupon =  (isset($payment->metadata->coupon)) ? $payment->metadata->coupon : null;

        //$subscription  = $this->mollie->customers_subscriptions->withParentId($customer)->get( $_POST['subscriptionId'] );
        //if payment is paid and has no refund and chargebacks, OR if it failed because of insuf funds, treat it as paid and just process chargeback so payment is registered first.
        if ( $payment->isPaid() && !$payment->hasRefunds() && !$payment->hasChargebacks()  ) {
            $price = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true );

            if  ( $subs->payment_id == $payment->id ) return false; //this payment is already existing, do not allow double posting of payment

            if ( $subs->version == 2 ) { //added for bugged subscriptions that haven't had updated payments ids, double check
                if ( $this->get_payment_by_payment_id(  $payment->id ) ) return false; //this payment is already existing, do not allow double posting of payment
            }

            //increment payments made
            $fields = array(
                'payments_made' => $payments_made + 1,
                'payment_id' => $payment->id, //update the payment id again for memberhips/payment plans
            );

            $fields_format = array(
                '%d',
                '%s',
            );

            global $wpdb;
            $wpdb->update(
                $this->subs_table,
                $fields,
                array( 'id' => $subs_id ),
                $fields_format,
                array( '%s' )
            );

            $invoice = new Poppyz_Invoice();
            usleep( rand( 0,4000000 ) ); //add random delay to prevent duplication of generated invoice numbers
            $data['payment_id'] = $payment->id;
			$data['mode'] = $payments_mode;
            $invoice_id = $invoice->save_invoice( $subs_id, $data );
            $invoice->generate_pdf( $invoice_id, "E" );
            $invoice->update_invoice_status( $invoice_id, 'paid' );

            $coupon_code = null;
            if ( $coupon > 0 ) {
                $coupon_code = get_post_meta( $coupon, PPY_PREFIX . 'coupon_code', true );
            }
            $invoice_type = '';
            $mb_invoice_id = $this->create_invoice_mb( $user_id, $total_with_tax, $tier_id, $subs_id, $coupon_code, $invoice_id );
            if ( $mb_invoice_id ) {
                $this->register_payment_mb( $total_with_tax, $mb_invoice_id, $payment->id );
                $invoice_type = 'moneybird';
            } else {
                if ( apply_filters( 'ppy_send_invoices', true, $user_id, $subs_id ) ) {
                    $invoice->generate_pdf( $invoice_id, "EU" ); //send invoice to user
                }
                $mb_invoice_id = '';
            }

            if ( $version == 2 ) {
                //if there are past insufficient funds payments, add them to the number of payment of the payment table to fix the latest payment number.
                $insufficient_funds_count = $this->get_insufficient_funds_payments($subs_id);
                if ($insufficient_funds_count > 0 ) {
                    $payments_made = $payments_made + $insufficient_funds_count;
                }
                $this->save_payment( $subs_id, $payment->id, $invoice_id, $payments_made + 1, 'paid', $invoice_type, $mb_invoice_id );
                echo 'Success';
            }

        } elseif ($payment->isFailed() ) {
            $this->process_failed_payment($subs_id, $payment, $payments_made);
            Poppyz_Core::log( print_r( $payment, true ), 'order_process_payment_failed', true );
        }
        elseif ( $payment->hasChargebacks() || $payment->hasRefunds() ){
            $this->process_chargeback( $subs_id, null, $payment->id );
            $this->cancel_membership( $subs_id );
            Poppyz_Core::log( print_r( $payment, true), 'chargeback_or_refunds', true );
        }  else {
            add_filter( 'wp_mail_content_type', function( $content_type ) {
                return 'text/html';
            });
            $message = sprintf( __( 'Your payment for the tier <b>%s</b> which belongs to course <b>%s</b> has failed. Please re-subscribe <a href="%s">here</a> to gain back access.' ,'poppyz'), get_the_title( $tier_id ), get_the_title( $course_id ), Poppyz_Core::get_tier_purchase_url( $tier_id )  );

            //wp_mail( $user_id, __( 'Payment failed.' ), $message );
            //Poppyz_Core::update_subscription( 'cancel', $subs_id );
        }
        //error_log('end');

        /*$payment_id = $payment->id;
        $price = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true);
        $this->send_invoice( $user_id, 1, $price, $tier_id, $subs_id  );*/
    }

    public function mollie_checkout( $subs_id, $on_demand = false, $customer_id = null ) {
		//check if the Mollie test mode is checked and update the subscription payment_mode. Check also if API key is not empty.
		if ( (isset($_POST['mollieTestMode']) && $_POST['mollieTestMode'] == true && (!empty(current_user_can( 'manage_options' ))) || Poppyz_Core::get_option( 'mo_api_key') == "" ) ) {
			$data['payment_mode'] = "Test";
			$testmode = "&testmode=1";
			Poppyz_Subscription::update_subscription( "update", $subs_id, $data );
		} else {
			$testmode = "";
			$data['payment_mode'] = "Live";
			Poppyz_Subscription::update_subscription( "update", $subs_id, $data );
		}

		$ppy_sub = new Poppyz_Subscription();
		$subs = $ppy_sub->get_subscription_by_id( $subs_id );
		$subs_id = $subs->id;
		$tier_id = $subs->tier_id;
		$course_id = $subs->course_id;
		$payment_method = $subs->payment_method;

        $additional_products =  Poppyz_Core::get_additional_products_cookie();

        $is_tier_bundled_product = Poppyz_Core::is_tier_bundled_products($tier_id);

        if ($is_tier_bundled_product) {
            $additional_products = Poppyz_Core::get_tier_additional_products($tier_id, true);
        }

        //Poppyz_Core::remove_additional_products_cookie();
        $has_additional_products = !empty($additional_products) ? $additional_products : null;

        $user_id = get_current_user_id();

        $user_info = get_userdata( $user_id );
        $full_name = $user_info->first_name . ' ' . $user_info->last_name;
        if ( empty( trim( $full_name ) ) ) {
            $full_name = $user_info->display_name;
        }
        $user_email = $user_info->user_email;


        $is_automatic = get_post_meta( $tier_id, PPY_PREFIX . 'payment_automatic', true );
        if ( $subs->payment_type == 'automatic' || $subs->payment_type == '' ) {
            $is_automatic = true;
        }
        $has_insufficient_funds = $this->get_insufficient_funds_payments($subs->id);
        if ( $subs->payment_type == 'manual' || $has_insufficient_funds > 0 ) {
            $is_automatic = false;
        }
        $payment_type = ( $is_automatic ) ? 'automatic' : 'manual';

        $ppy_coupon = new Poppyz_Coupon();

        $coupon = 0;

        if ( !empty( $_COOKIE['ppy_coupon'] ) ) {
            $ppy_coupon = new Poppyz_Coupon();
            $coupon = $ppy_coupon->get_coupon_by_code( $_COOKIE['ppy_coupon'] );
            $id = $coupon->ID;
            $valid_coupon = $ppy_coupon->is_coupon_valid( $id, $tier_id );
            if ( !is_wp_error( $valid_coupon ) && $valid_coupon ) {
                $coupon = $id;
            } else {
				$coupon = 0;
			}
        }
        //check for malicious users
        if ( !$this->is_subscription_owner( $subs_id, $user_id ) ) {
            print 'Sorry, this is not your subscription.';
            exit;
        }
        $normal_price = null;
        if ( $this->is_next_payment( $subs ) ) {
            $normal_price = $price = $ppy_sub->compute_discounted_price( $subs_id );
            $discount = false;
        } else {
            $amount = $subs->amount;
            $normal_price = $ppy_coupon->compute_discounted_price( $amount, $tier_id );
            if ( $subs->initial_amount > 0 ) {
                $amount = $subs->initial_amount;
            }
            $price = $ppy_coupon->compute_discounted_price( $amount, $tier_id );
            $discount = $ppy_coupon->get_discount_amount( $amount, $tier_id );
        }

        //round cents to .02,the minimum amount mollie accepts
        if ($price < .02 && $price > 0) {
            $price = .02;
        }
		$order_id = uniqid();
		if(!Poppyz_Core::column_exists( 'payment_mode' )) {
			$this->_initialize_mollie();
			$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
		} else {
			$this->_initialize_mollie( $subs->payment_mode );
			if ($subs->payment_mode == "Test") {
				$mo_key = Poppyz_Core::get_option( 'mo_api_test_key' );
			} else {
				$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
			}
		}

        $has_additional_products_querystring = $has_additional_products ? '&additional_products-sold' : '';

        if ( $price == 0  && !$has_additional_products_querystring ) {
            Poppyz_Subscription::update_subscription( 'subscribe', $subs_id );
            Poppyz_Core::subscribe_newsletter( $user_id, $tier_id );
            if ( $coupon > 0 ) {
                Poppyz_Coupon::record_coupon_usage( $coupon, $user_id );
            }

            global $ppy_lang;
            $tier_return_page = get_post_meta( $tier_id, PPY_PREFIX . 'tier_thank_you_page', true );
            //if tier return page is not set, use the default return page in plugin settings
            $return_page = ($tier_return_page) ? get_permalink($tier_return_page) : get_permalink( Poppyz_Core::get_option( 'return_page'  ) );

			//check if there is a scheduled email donation set on the tier.
			$userScheduledDonation = Poppyz_Core::setScheduledDonation($tier_id, $user_id);
            if ( $on_demand == false && (!isset($_POST[PPY_PREFIX . 'donation_amount']) || empty($_POST[PPY_PREFIX . 'donation_amount'])) ) {
                if ( $return_page ) {
                    wp_redirect( $return_page  . '/?success&subs_id='. $subs_id );
                    //wp_redirect( get_permalink( $return_page ) . '?success=1&subs_id=' . $subs_id );
                    exit();
                } else {
                    echo "<p>" . sprintf(  __( "You can now start your course. Click <a href='%s'>here</a> to begin." ,'poppyz'), get_permalink( $course_id ) )  . "</p>";
                    exit();
                }

            } else if ( isset($_POST[PPY_PREFIX . 'donation_amount']) && !empty($_POST[PPY_PREFIX . 'donation_amount'])  ) {
				//donation for free tier.
				$order_id = uniqid();
				$customer = null;
				if ( $customer_id ) {
					$customer = $this->mollie->customers->get( $customer_id );
				}
				if ( !$customer ) {
					$customer = $this->mollie->customers->create([
						"name"  => $full_name,
						"email" => $user_email
					]);
				}
				$is_mb = ( $this->_initilalize_moneybird_v2() ) ? '&invoice=mb' : ''; //check if mb is activated, use this invoice instead when returning to the site from payment
				$donation = ($_POST[PPY_PREFIX . 'donation_amount'] == "others") ? $_POST[PPY_PREFIX . 'donation_amount_custom'] : $_POST[PPY_PREFIX . 'donation_amount'];

                if ( $donation > 0 ) {
					$payment_details = array(
						"amount"       => array( 'currency' => Poppyz_Core::get_currency(), 'value' => number_format($donation, 2, '.', '') ) ,
						"description"  => __("Donation", "poppyz"),
						"webhookUrl"   => site_url('?process-order'),
						"redirectUrl"  =>  $return_page . "?return&order_id=" . $order_id . $is_mb . $has_additional_products_querystring . $testmode,
						"sequenceType"  =>  \Mollie\Api\Types\SequenceType::SEQUENCETYPE_FIRST,
					);


					update_user_meta( $user_id, PPY_PREFIX . 'donation_amount_'.$subs_id, $donation);
					$customer->createPayment($payment_details);
				}

            } else {
				return $subs->id;
			}
        }



        $customer = null;
        if ( $customer_id ) {
            $customer = $this->mollie->customers->get( $customer_id );
        }
        if ( !$customer ) {
            $customer = $this->mollie->customers->create([
                "name"  => $full_name,
                "email" => $user_email
            ]);
        }
        if ( !$customer ) {
            die('Customer in Mollie not found.');
        }

        $total_with_tax = Poppyz_Core::price_with_tax( $price, $tier_id, $user_id );

        $normal_amount_with_tax = Poppyz_Core::price_with_tax( $normal_price, $tier_id );

        $payment_number =  ( !empty( $_POST[PPY_PREFIX . 'payment_number'] ) ) ? $_POST[PPY_PREFIX . 'payment_number'] : 0;

        $is_mb = ( $this->_initilalize_moneybird_v2() ) ? '&invoice=mb' : ''; //check if mb is activated, use this invoice instead when returning to the site from payment


        $vatshifted =  Poppyz_Invoice::is_vatshifted( $user_id ) ? 1 : 0;
        $vat_out_of_scope =  Poppyz_Invoice::is_vat_out_of_scope( $user_id ) ? 1 : 0;
        $tier_return_page = get_post_meta( $tier_id, PPY_PREFIX . 'tier_thank_you_page', true );
        //if tier return page is not set, use the default return page in plugin settings
        $return_page = ($tier_return_page) ? get_permalink($tier_return_page) : get_permalink( Poppyz_Core::get_option( 'return_page'  ) );

        //determine the normal amount, despite having or not having an initial amount
        $normal_amount = $subs->amount;
		$tier_discount = get_post_meta( $tier_id, PPY_PREFIX .  'tier_discount', true );
		if ( !empty($tier_discount) || !empty($coupon) ) {
			$raw_normal_amount = $normal_amount;
			$raw_total_with_tax = Poppyz_Core::price_with_tax( $raw_normal_amount, $tier_id, $user_id );
		} else {
			$raw_total_with_tax = $total_with_tax;
		}

		$discount_implementation = get_post_meta( $tier_id, PPY_PREFIX .  'discount_implementation_option', true );

		if ( empty($discount_implementation) || $discount_implementation == "all" ) {
			$normal_amount = $ppy_coupon->compute_discounted_price( $normal_amount, $tier_id );
		}


		$tier_discount = get_post_meta( $tier_id, PPY_PREFIX .  'tier_discount', true );
		$tier_discount = ( !empty($tier_discount) ) ? $tier_discount : 0;

        if ($normal_amount < .02) {
            $normal_amount = .02;
        }
        if ($normal_amount_with_tax < .02) {
            $normal_amount_with_tax = .02;
        }

		$normal_amount_with_tax = ( !empty($discount_implementation) && $discount_implementation == "first_payment" ) ? $raw_total_with_tax : $normal_amount_with_tax;



        $metadata = array(
            "subs_id" => $subs->id,
            "coupon" => $coupon,
			"tier_discount" => $tier_discount,
            "payment_number" => $payment_number,
            "vatshifted" => $vatshifted,
            "vat_out_of_scope" => $vat_out_of_scope,
            "upsell" => (bool)$on_demand,
            "on_demand" => (bool)$on_demand,
            "normal_amount" => $normal_amount,
            "normal_amount_with_tax" => $normal_amount_with_tax,
        );

		if ( isset($_GET['additional_products-sold'])) {
			$metadata['additional_products-sold'] = true;
		}
        if ($subs->initial_amount > 0) {
            $metadata['initial_amount'] = $subs->initial_amount;
        }

        $payment_title = Poppyz_Core::generate_payment_title( $subs->id, $normal_amount_with_tax, true );


        if ( $has_additional_products && $on_demand == false ) {
			$additional_eproduct_subscriptions = [];
			$additional_product_subscriptions = [];
			delete_user_meta( $user_id, PPY_PREFIX . 'pending_eproduct' ); // clear the eproducts added to usermeta.
			$eproduct_total_with_tax = 0;
            foreach ($additional_products as $additional_prod_tier_id) {
                $pending_id = Poppyz_Subscription::is_pending_tier($user_id, $additional_prod_tier_id);
                $price = get_post_meta($additional_prod_tier_id, PPY_PREFIX . 'tier_price', true);
                $initial_price = (float)get_post_meta( $additional_prod_tier_id, PPY_PREFIX . 'tier_initial_price', true );
				$productType = get_post_type( $additional_prod_tier_id );
                if ($pending_id) {
                    $subscription_id = $pending_id;
                    //update to latest information
                    $data['price'] = $price;
                    $data['date'] = date('d-m-Y');
                    $data['payment_type'] = '';
                    $data['initial_amount'] = $initial_price;
                    $ppy_sub->update_subscription('update', $subscription_id, $data);
					$additional_product_subscriptions[] = $subscription_id;
                } else if ( $productType == "ppyv_product" )  {
					$additional_eproduct_subscriptions[] = $additional_prod_tier_id;
					$eproduct_price = get_post_meta($additional_prod_tier_id, PPY_PREFIX . 'tier_price', true);
					$eproduct_total_with_tax = $eproduct_total_with_tax + Poppyz_Core::price_with_tax( $eproduct_price, $additional_prod_tier_id, $user_id );
				} else {
                    $subscription_id = $ppy_sub->add_pending_subscription($user_id, $additional_prod_tier_id, $price, '', $initial_price);
					$additional_product_subscriptions[] = $subscription_id;

					$additional_product_data['payment_mode'] = (empty($testmode)) ? "Live" : "Test";
					Poppyz_Subscription::update_subscription( "update", $subscription_id, $additional_product_data );
                }

            }

            $metadata['additional_products'] =  implode(',', $additional_products);
            $metadata['additional_product_subscriptions'] =  implode(',', $additional_product_subscriptions);
			$metadata['additional_eproduct_subscriptions'] =  implode(',', $additional_eproduct_subscriptions);

            if (!$is_tier_bundled_product) {
                $total_with_tax = $total_with_tax + Poppyz_Payment::get_total_amount($additional_product_subscriptions);
				$total_with_tax = $total_with_tax + $eproduct_total_with_tax;
            }

        }

        if ($is_tier_bundled_product) {
            $metadata['has_bundled_products'] = 1;
        }

		if ( isset($_POST[PPY_PREFIX . 'donation_amount']) ) {
			$donation = ($_POST[PPY_PREFIX . 'donation_amount'] == "others") ? $_POST[PPY_PREFIX . 'donation_amount_custom'] : $_POST[PPY_PREFIX . 'donation_amount'];

			if ( $donation > 0 ) {
				$total_with_tax = $total_with_tax + $donation;
				update_user_meta( $user_id, PPY_PREFIX . 'donation_amount_'.$subs->id, $donation);
				update_user_meta( $user_id, PPY_PREFIX . 'donation_exist', true);
				//error_log($_POST[PPY_PREFIX . 'donation_amount_custom']."  -2");
			}

		}

        if ($total_with_tax < .02) {
            $total_with_tax = .02;
        }

        $payment_details = array(
            "amount"       => array( 'currency' => Poppyz_Core::get_currency(), 'value' => number_format($total_with_tax, 2, '.', '') ) ,
            "description"  => $payment_title,
            "webhookUrl"   => site_url('?process-order'),
            "redirectUrl"  =>  $return_page . "?return&order_id=" . $order_id . $is_mb . $has_additional_products_querystring . $testmode,
            "metadata"     =>  apply_filters( 'ppy_payment_metadata', $metadata, $subs_id ),
        );

        if ( $is_automatic && ( $payment_method == 'membership' || $payment_method == 'plan' ) ) {
            if ( $on_demand ) {
                //if on demand there is already a mandate so no need to do first payment. Create the subscription right away.
                $payment_details['sequenceType'] = \Mollie\Api\Types\SequenceType::SEQUENCETYPE_RECURRING;
            } else {
                $payment_details['sequenceType'] = \Mollie\Api\Types\SequenceType::SEQUENCETYPE_FIRST;
            }
        } else {
            if ( $on_demand ) {
                $payment_details['sequenceType'] = \Mollie\Api\Types\SequenceType::SEQUENCETYPE_RECURRING;
            } elseif (Poppyz_Core::get_tier_upsells( $tier_id ) ) { //if it's a standard tier with upsells, get a mandate by making a first payment
                $payment_details['sequenceType'] = \Mollie\Api\Types\SequenceType::SEQUENCETYPE_FIRST;
            }
        }
        try {
            $payment = $customer->createPayment($payment_details);
			//error_log(print_r($customer,true));

        } catch (Exception $e) {
            if ( $e->getCode() == '422') {
                echo '<div style="text-align: center;">Sorry, something went wrong with your payment. Error: ' . $e->getMessage() . '</div>';
            }
            error_log( "Error: " . htmlspecialchars($e->getMessage()) );
            die();
        }



		global $wpdb;
        $update_array =  array(
            'order_id' => $order_id,
            'payment_status' => 'open',
            'mollie_customer_id' => $customer->id,
            'payment_id' => $payment->id,
            'vatshifted' => $vatshifted,
            'vat_out_of_scope' => $vat_out_of_scope
        );

        $type_array = array(
            '%s',
            '%s',
            '%s',
            '%s',
            '%d',
            '%d',
        );

        if ( $discount ) {
            $update_array['discount_amount'] = $discount;
            $type_array[] = '%f';
        }


        $wpdb->update(
            $this->subs_table,
            $update_array,
            array( 'id' => $subs_id ),
            $type_array,
            array( '%d' )
        );

        if (!empty($additional_product_subscriptions)) {
            foreach ($additional_product_subscriptions as $additional_product_subscription) {
				if (isset($update_array['discount_amount'])) {
					//unset the discount amount for additional products
					unset($update_array['discount_amount']);
					//remove the type %f element that was added
					array_pop($type_array);
				}
                $wpdb->update(
                    $this->subs_table,
                    $update_array,
                    array( 'id' => $additional_product_subscription ),
                    $type_array,
                    array( '%d' )
                );
            }
        }

        Poppyz_Coupon::remove_coupon_session();

        if ( $on_demand ) {
            //accept right away as paid since it will take time to process this.
			if ( $payment->id != $this->get_last_payment_id( $subs_id ) ) {
				$db_payment_id = $this->get_last_payment_id( $subs_id );
				$subs = $ppy_sub->get_subscription_by_id( $subs_id );
				$this->process_first_payment( $subs, $payment );
				$this->process_invoice( $subs, $payment );
			}

            return $subs_id;
        } else {
            header("Location: " . $payment->getCheckoutUrl());
        }

    }
	public function ppyv_mollie_checkout( $product_id, $parentProductId = null, $on_demand = false ) {
		$user_id = get_current_user_id();
		$user_info = get_userdata( $user_id );
		$ppy_coupon = new Poppyz_Coupon();
		$customer_id = get_post_meta( $parentProductId, PPYV_PREFIX . $user_id . '_mollie_customer_id' );

		if ( empty($customer_id) && isset($_POST['ppy_mollie_customer_id']) ) {
			$customer_id = $_POST['ppy_mollie_customer_id'];
		} else {
			$customer_id = $customer_id[0];
		}

		$full_name = $user_info->first_name . ' ' . $user_info->last_name;
		if ( empty( trim( $full_name ) ) ) {
			$full_name = $user_info->display_name;
		}
		$user_email = $user_info->user_email;

		$coupon = 0;
		if ( !empty( $_COOKIE['ppy_coupon'] ) ) {
			$ppy_coupon = new Poppyz_Coupon();
			$coupon = $ppy_coupon->get_coupon_by_code( $_COOKIE['ppy_coupon'] );
			$id = $coupon->ID;
			$valid_coupon = $ppy_coupon->is_coupon_valid( $id, $product_id );
			if ( !is_wp_error( $valid_coupon ) && $valid_coupon ) {
				$coupon = $id;
			}
		}

		$product_price = get_post_meta($product_id, PPY_PREFIX . 'tier_price', true);
		$price = $ppy_coupon->compute_discounted_price($product_price);
		$discount = $ppy_coupon->get_discount_amount($product_price);

		if ( !$discount ){
			$discount = 0;
		}
		//round cents to .02,the minimum amount mollie accepts
		if ($price < .02 && $price > 0) {
			$price = .02;
		}

		if ( $price == 0 ) {
			$this->process_free_product($product_id, $user_id, $coupon);
		} else {

			if ( isset($_POST['subscription_id']) && empty($_POST['mollieTestMode']) ) {
				// if eproduct upsell is from tier and it is live, delete the test mode postmeta.
				delete_post_meta( $product_id, PPYV_PREFIX . "mollieTestMode_" . $user_id );
			} elseif (isset($_POST['subscription_id']) && $_POST['mollieTestMode'] == true ) {
				// add mollieTestMode if the subscriber is using test mode in tier.
				add_post_meta( $product_id, PPYV_PREFIX . "mollieTestMode_" . $user_id, true );
			} else if ( Poppyz_Core::get_option( 'mo_api_key') == "" || empty(Poppyz_Core::get_option( 'mo_api_key')) ) {
				// live api key is empty
				add_post_meta( $product_id, PPYV_PREFIX . "mollieTestMode_" . $user_id, true );
			} else if ( isset($_POST['product_id']) && isset($_POST['mollieTestMode']) && $_POST['mollieTestMode'] == true ) {
				// administrator - eproduct upsell eproduct
				add_post_meta( $product_id, PPYV_PREFIX . "mollieTestMode_" . $user_id, true );
			}

			$mollieTestMode = get_post_meta( $product_id, PPYV_PREFIX . "mollieTestMode_" . $user_id, true );
			$paymentMode = ( isset($mollieTestMode) && $mollieTestMode == true ) ? "Test" : "Live";
			$this->_initialize_mollie($paymentMode);

			$is_mb = ( $this->_initilalize_moneybird_v2() ) ? '&invoice=mb' : ''; //check if mb is activated, use this invoice instead when returning to the site from payment
			$total_with_tax = Poppyz_Core::price_with_tax( $price, $product_id, $user_id );
			$tier_return_page = get_post_meta( $product_id, PPY_PREFIX . 'tier_thank_you_page', true );

			//if tier return page is not set, use the default return page in plugin settings
			$return_page = ($tier_return_page) ? get_permalink($tier_return_page) : get_permalink( Poppyz_Core::get_option( 'return_page'  ) );
			$testMode = ( isset($mollieTestMode) && $mollieTestMode == true ) ? "&testmode=1" : "";

			if ( $customer_id ) {
				$customer = $this->mollie->customers->get( $customer_id );
			}

			if ( !$customer ) {
				$customer = $this->mollie->customers->create([
					"name"  => $full_name,
					"email" => $user_email
				]);
			}
			try {
				$payment_details = array(
					"amount"       => array( 'currency' => Poppyz_Core::get_currency(), 'value' => number_format($total_with_tax, 2, '.', '') ) ,
					"description" => get_the_title($product_id),
					"webhookUrl" => site_url('?process-product-order'.$testMode),
					"redirectUrl" => $return_page . "?product_order_id=" . $product_id . $is_mb,
					"metadata" => array(
						"product_id" => $product_id,
						"coupon" => $coupon,
						"user_id" => $user_id,
						"discount" => $discount
					));
				if ( $on_demand ) {
					$payment_details['sequenceType'] = \Mollie\Api\Types\SequenceType::SEQUENCETYPE_RECURRING;
				} elseif (Poppyz_Core::get_tier_upsells( $product_id ) ) { //if it's a standard tier with upsells, get a mandate by making a first payment
					$payment_details['sequenceType'] = \Mollie\Api\Types\SequenceType::SEQUENCETYPE_FIRST;
				}


				$payment = $customer->createPayment($payment_details);
			} catch (Exception $e) {
				if ( $e->getCode() == '422') {
					echo '<div style="text-align: center;">Sorry, something went wrong with your payment. Error: ' . $e->getMessage() . '</div>';
				}
				error_log( "Error: " . htmlspecialchars($e->getMessage()) );
				die();
			}
			$user_id = get_current_user_id();
			$mollie_subscription_id = $customer->id;
			$payment_id = $payment->id;

			delete_post_meta( $product_id, PPYV_PREFIX . $user_id . '_payment_id' );
			delete_post_meta( $product_id, PPYV_PREFIX . $user_id . '_mollie_customer_id' );

			add_post_meta( $product_id, PPYV_PREFIX . $user_id . '_payment_id' , $payment_id );
			add_post_meta( $product_id, PPYV_PREFIX . $user_id . '_mollie_customer_id' , $mollie_subscription_id );

			return $product_id;
		}
	}

    public static function is_next_payment( $subscription ) {
        $ppy_sub = new Poppyz_Subscription();

        if ( is_numeric( $subscription ) ) {
            $subscription = $ppy_sub->get_subscription_by_id( $subscription );
        }

        if ( is_object( $subscription ) ) {
            $tier_id = $subscription->tier_id;
            $payments_made = $subscription->payments_made;

            if ( $subscription->payment_method == 'plan') {
                $number_of_payments = get_post_meta( $tier_id, PPY_PREFIX . 'number_of_payments', true );
                if ( $payments_made > 0 && $payments_made <= $number_of_payments ) {
                    return true;
                }
            } elseif ($subscription->payment_method == 'membership') {
                return ($payments_made > 0);
            }
        }
        return false;
    }

    public function cancel_membership( $subs_id, $delete_mandates = false ) {


		$ppy_sub = new Poppyz_Subscription();
		$subs = $ppy_sub->get_subscription_by_id( $subs_id );
		if ( !$subs ) {
			error_log('cancel_membership- Subscription not found:' . $subs_id);
			return false;
		}

		if(!Poppyz_Core::column_exists( 'payment_mode' )) {
			$this->_initialize_mollie();
			$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
		} else {
			$this->_initialize_mollie( $subs->payment_mode );
			if ($subs->payment_mode == "Test") {
				$mo_key = Poppyz_Core::get_option( 'mo_api_test_key' );
			} else {
				$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
			}
		}
		$mo_test_key = Poppyz_Core::get_option( 'mo_api_test_key' );
		if ( empty( $mo_key ) && empty($mo_test_key) )  {
			//print 'No API Key has been supplied for Mollie.';
			return false;
		}

        $user_id = $subs->user_id;
        $subscription_id = $subs->mollie_subscription_id;
        $mollie_customer_id = $subs->mollie_customer_id;

        if ( $mollie_customer_id && $subscription_id ) {
            try {
                //cancel mollie subscription
                $customer = $this->mollie->customers->get( $mollie_customer_id );
                $customer->cancelSubscription($subscription_id);
                
                if ( $delete_mandates ) {
                    foreach($customer->mandates() as $m) {
                        $mandate = $customer->getMandate($m->id);
                        $mandate->revoke();
                    }
                }

                return true;
            } catch ( Exception $ex ) {
                error_log($ex);
                return false;
            }
        }
    }

    public function process_insufficient_funds( $subs_id, $payment_id, $payment_number ) {
        $ppy_sub = new Poppyz_Subscription();
        $subs = $ppy_sub->get_subscription_by_id( $subs_id );
        if (!$subs) {
            Poppyz_Core::log('Subscription not found: ' . $subs_id, 'insufficient_funds', true );
            return false;
        }
        $this->save_payment(  $subs_id, $payment_id, null, $payment_number, 'insufficient_funds' );
        Poppyz_Core::send_notification_email( $subs_id, $subs->user_id, null, 'insufficient_funds' );
    }

    public function process_chargeback( $subs_id, $invoice_number = null, $payment_id = null ) {
        $ppy_sub = new Poppyz_Subscription();
        $subs = $ppy_sub->get_subscription_by_id( $subs_id );
        if (!$subs) {
            Poppyz_Core::log('subscription not found ' .  $subs_id, 'process_chargeback', true );
            return false;
        }
        $ppy_invoice = new Poppyz_Invoice();
        $user_id = $subs->user_id;
        $payments_made = $subs->payments_made;

        if ($payments_made == 0 ) return false; //no need to chargeback, payment has not been made yet.

        if ( $invoice_number ) {
            $invoice = $ppy_invoice->get_invoice_by_code( $invoice_number );
            $ppy_invoice->duplicate_to_credit( $invoice->ID );
        } else if ( $payment_id ) {
            $exists = $this->update_payment( $payment_id, 'chargedback' );
            if ( $exists ) {
                $invoice_id = $this->get_invoice_by_payment_id( $payment_id );
                $ppy_invoice->duplicate_to_credit( $invoice_id );
            }
        }

        //revert last payment made, turn off access, and change payment type to manual
        $fields = array(
            'payments_made' => $payments_made - 1,
            'status' => 'off',
            'payment_status' => 'pending',
            'payment_type' => 'manual'
        );
        $fields_format = array(
            '%d',
            '%s',
            '%s',
            '%s'
        );

        global $wpdb;
        $wpdb->update(
            $this->subs_table,
            $fields,
            array( 'id' => $subs_id ),
            $fields_format,
            array( '%s' )
        );

        add_filter( 'wp_mail_content_type', function( $content_type ) {
            return 'text/html';
        });

        Poppyz_Core::send_notification_email( $subs_id, $user_id, null, 'failed_payment' );
        Poppyz_Core::send_notification_email( $subs_id, $user_id, ['admin' => true], 'failed_payment_admin' );
    }

    public function get_payment( $id ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;
        $sql = $wpdb->prepare( "SELECT *
                FROM $this->payments_table
                WHERE id = %d", $id );


        return $wpdb->get_row( $sql );
    }

    public function get_payment_by_invoice( $invoice_id ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;
        $sql = $wpdb->prepare( "SELECT *
                FROM $this->payments_table
                WHERE invoice_id = %d", $invoice_id );


        return $wpdb->get_row( $sql );
    }

    public function get_payment_by_invoice_other( $invoice_id ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;
        $sql = $wpdb->prepare( "SELECT *
                FROM $this->payments_table
                WHERE invoice_id_other = %d", $invoice_id );


        return $wpdb->get_row( $sql );
    }

    public function get_payment_by_payment_id( $payment_id ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;
        $sql = $wpdb->prepare( "SELECT *
                FROM $this->payments_table
                WHERE payment_id = %s", $payment_id );

        return $wpdb->get_row( $sql );
    }

    public function get_payments( $subs_id ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;

        $sql = $wpdb->prepare( "SELECT *
                FROM $this->payments_table
                WHERE subscription_id = %d", $subs_id );

        return $wpdb->get_results( $sql );
    }

    public function save_payment( $subs_id, $payment_id, $invoice_id, $number_of_payment = 1, $status = 'paid', $invoice_type = '', $invoice_id_other = '' ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        if ( !$number_of_payment ) $number_of_payment = 1;
        if ( !$invoice_id ) $invoice_id = 0;
        global $wpdb;
        $fields =  array(
            'subscription_id' => $subs_id,
            'payment_id' => $payment_id,
            'invoice_id' => $invoice_id,
            'number_of_payment' => $number_of_payment,
            'status' => $status,
            'invoice_type' => $invoice_type,
            'invoice_id_other' => $invoice_id_other,
            'created' => date("Y-m-d H:i:s")
        );

        $values = array(
            '%d',
            '%s',
            '%d',
            '%d',
            '%s',
            '%s',
            '%s',
            '%s',
        );

        $wpdb->insert(
            $this->payments_table,
            $fields,
            $values
        );
    }

    public function update_payment( $payment_id, $status, $new_payment_id = null, $new_invoice_id = null ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;
        $fields = array(
            'status' => $status,
            'updated' => date("Y-m-d H:i:s")
        );
        $fields_format = array(
            '%s',
            '%s',
        );
        if ( $new_payment_id ) {
            $fields['payment_id'] = $new_payment_id;
            $fields_format[] = '%s';
        }
        if ( $new_invoice_id ) {
            $fields['invoice_id'] = $new_invoice_id;
            $fields_format[] = '%s';
        }

        return $wpdb->update(
            $this->payments_table,
            $fields,
            array( 'payment_id' => $payment_id ),
            $fields_format,
            array( '%s' )
        );

    }

    public function get_chargeback_payment_id( $subs_id, $payment_number ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;
        $sql =
            $wpdb->prepare( "SELECT payment_id
                FROM $this->payments_table
                WHERE subscription_id = %d
                AND number_of_payment = %d
                AND (status = %s
                OR status = %s)",
                $subs_id,
                $payment_number,
                'chargedback',
                'insufficient_funds'
            );

        return $wpdb->get_var( $sql );
    }

    public function get_payment_invoice( $id ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;
        $sql = $wpdb->prepare(
            "
	            SELECT invoice_id
	            FROM $this->payments_table
	            WHERE id = %d
	      	",
            $id
        );
        return $wpdb->get_var(
            $sql
        );
    }

    public function get_invoice_by_payment_id( $payment_id ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;
        $sql = $wpdb->prepare(
            "
	            SELECT invoice_id
	            FROM $this->payments_table
	            WHERE payment_id = %s
	      	",
            $payment_id
        );
        return $wpdb->get_var(
            $sql
        );
    }

    public function get_payment_by_number( $subs_id, $number_of_payment ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;
        $sql = $wpdb->prepare(
            "SELECT *
            FROM $this->payments_table
            WHERE subscription_id = %d 
            AND number_of_payment = %d",
            $subs_id,
            $number_of_payment
        );
        return $wpdb->get_row( $sql );
    }

    public function get_last_payment_id( $subs_id ) {
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;
        $sql = $wpdb->prepare(
            "SELECT payment_id
            FROM $this->payments_table
            WHERE subscription_id = %d ORDER BY id DESC LIMIT 1",
            $subs_id
        );
        return $wpdb->get_var( $sql );
    }

    public function get_insufficient_funds_payments($subs_id){
        if ( !Poppyz_Core::table_exists( $this->payments_table ) ) return false;
        global $wpdb;
        $sql = $wpdb->prepare(
            "SELECT *
            FROM $this->payments_table
            WHERE subscription_id = %d 
            AND status = %s",
            $subs_id,
            'insufficient_funds'
        );
        $wpdb->get_results( $sql );
        return $wpdb->num_rows;
    }

    /**
     *
     * @param $subscription
     * @param $payment
     */
    public function process_invoice( $subscription, $payment ): void
    {
        global $wpdb;
        $subs_id = $subscription->id;
        $tier_id = $subscription->tier_id;
        $user_id = $subscription->user_id;
        $payments_made = $subscription->payments_made;
		$payments_mode = $subscription->payment_mode;
        $version =  $subscription->version; //if the sub is saved in version 2, then perform the new model structure in saving payments in the course_subscriptions-payments table

        $payment_id = $payment->id;
		$payment_meta_subs_id = (!empty($payment->metadata->subs_id)) ? $payment->metadata->subs_id : $payment->metadata->product_id;
        $coupon = ($subs_id == $payment_meta_subs_id) ? $payment->metadata->coupon : 0;
        $meta_invoice_id = !empty( $payment->metadata->invoice_id ) ? $payment->metadata->invoice_id : null;
        $payment_number = $payment->metadata->payment_number;
        $total_with_tax = $payment->amount->value;

        $additional_product_subscriptions = !empty( $payment->metadata->additional_product_subscriptions ) ? $payment->metadata->additional_product_subscriptions : null;
        $additional_product_ids = [];
        if (!empty($additional_product_subscriptions)) {
            $additional_product_ids = explode(',', $additional_product_subscriptions);
        }
        $has_bundled_products = !empty( $payment->metadata->has_bundled_products ) ? $payment->metadata->has_bundled_products : null;

        $data = array();
        if ($coupon > 0) {
            $data['coupon_code'] = get_post_meta($coupon, PPY_PREFIX . 'coupon_code', true);
        }
        $invoice = new Poppyz_Invoice();
        if ($payment_number) {
            $data['payment_number'] = $payment_number;
        }
        if ($additional_product_subscriptions) {
            $data['additional_product_subscriptions'] = $additional_product_subscriptions;
        }
        if ($has_bundled_products) {
            $data['has_bundled_products'] = $has_bundled_products;
        }

        if (!in_array($subs_id, $additional_product_ids)) {  //only add the tier_discount on the main subscription, if it belongs to the additional products skip it
            $data['tier_discount'] = ( !empty($payment->metadata->tier_discount) ) ? $payment->metadata->tier_discount : 0;
        }

        $data['payment_id'] = $payment_id;
		$data['mode'] = $payments_mode;
        $invoice_id = $invoice->save_invoice($subs_id, $data);

        $coupon_code = null;
        if ($coupon > 0) {
            Poppyz_Coupon::record_coupon_usage($coupon, $user_id);
            $coupon_code = get_post_meta($coupon, PPY_PREFIX . 'coupon_code', true);
        }

        $invoice_type = '';

        $mb_invoice_id = $this->create_invoice_mb($user_id, $total_with_tax, $tier_id, $subs_id, $coupon_code, $invoice_id);

        if ($mb_invoice_id) {
            $this->register_payment_mb($total_with_tax, $mb_invoice_id, $payment_id);
            $wpdb->update(
                $this->subs_table,
                array(
                    'invoice_id' => $mb_invoice_id
                ),
                array('id' => $subs_id),
                array(
                    '%s'
                ),
                array('%s')
            );
            $invoice_type = 'moneybird';
        } else {
            if (apply_filters('ppy_send_invoices', true, $user_id, $subs_id)) {
                $invoice->generate_pdf($invoice_id, "E"); //send invoice to admin
                $invoice->generate_pdf($invoice_id, "EU"); //send invoice to user
            }
            $mb_invoice_id = '';
        }

        $invoice->update_invoice_status($invoice_id, 'paid');
        $has_chargeback = false;
        if ($version == 2) { //only save if the subscription uses the v2 structure
            if ($payment_number) { //chargedback payment
                error_log('enter chargeback payment');
                $pid = $this->get_chargeback_payment_id($subs_id, $payment_number);
                ob_start();
                echo '$pid:  ';
                var_dump($pid);
                $contents = ob_get_contents();
                ob_end_clean();
                error_log($contents);

                if ($pid) {
                    $has_chargeback = true;
                    $this->update_payment($pid, 'chargeback-paid', $payment->id, $invoice_id);
                }
            }

            if (!$has_chargeback) {
                //if there are past insufficient funds payments, add them to the number of payment of the payment table to fix the latest payment number.
                $insufficient_funds_count = $this->get_insufficient_funds_payments($subs_id);
                if ($insufficient_funds_count > 0) {
                    $payments_made = $payments_made + $insufficient_funds_count;
                }
                $this->save_payment($subs_id, $payment->id, $invoice_id, $payments_made + 1, 'paid', $invoice_type, $mb_invoice_id);
            }
        }
    }

    /**
     * @param object|null $subscription
     * @param string $payment
     * @return bool|int
     */
    public function process_first_payment($subscription, $payment)
    {
        global $wpdb;
		if(!Poppyz_Core::column_exists( 'payment_mode' )) {
			$this->_initialize_mollie();
			$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
		} else {
			$this->_initialize_mollie( $subscription->payment_mode );
			if ($subscription->payment_mode == "Test") {
				$mo_key = Poppyz_Core::get_option( 'mo_api_test_key' );
			} else {
				$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
			}
		}

        $payment_id = $payment->id;
        $payment_status = $payment->status;
        $coupon = $payment->metadata->coupon;

        if ( $payment->metadata->upsell == 'true' ) {
            $payment_status = 'paid';
        }


        $vatshifted = $subscription->vatshifted ?? 0;
        $vat_out_of_scope = $subscription->vat_out_of_scope ?? 0;
        $subs_id = $subscription->id;
        $tier_id = $subscription->tier_id;
        $course_id = $subscription->course_id;
        $mollie_customer_id = $subscription->mollie_customer_id;
        $subscription_date = $subscription->subscription_date;
        $payments_made = $subscription->payments_made;
        $payment_method = $subscription->payment_method;

		$type_of_discount = get_post_meta( $tier_id, PPY_PREFIX . "discount_implementation_option", true );

		$first_payment_discount = ( !empty($type_of_discount) && $type_of_discount == "first_payment" && (!empty($payment->metadata->tier_discount) || !empty($payment->metadata->coupon))) ? true : false;

        if ( !empty( $payment->metadata->initial_amount ) || !empty( $payment->metadata->additional_products) || $first_payment_discount == true ) {
            $total_with_tax = $payment->metadata->normal_amount_with_tax;
        } else {
            $total_with_tax = $payment->amount->value;

			//check if donation exist
			$donation = get_user_meta( $subscription->user_id, PPY_PREFIX . 'donation_amount_'.$subscription->id, true);
			if ( !empty($donation) ) {
				//remove the donation from the total with tax.
				$total_with_tax = $total_with_tax - $donation;
			}
        }



		//check if there is a scheduled email donation set on the tier.
		$userScheduledDonation = Poppyz_Core::setScheduledDonation($tier_id, $subscription->user_id);



		$fields = array(
            'status' => "on",
            'payment_id' => $payment_id,
            'payment_status' => $payment_status,
            'payments_made' => $payments_made + 1,
        );

        $fields_format = array(
            '%s',
            '%s',
            '%s',
            '%d'
        );

        if (($payment_method == 'membership' || $payment_method == 'plan') &&
            ($payment->sequenceType == \Mollie\Api\Types\SequenceType::SEQUENCETYPE_FIRST ||
                $payment->sequenceType == \Mollie\Api\Types\SequenceType::SEQUENCETYPE_RECURRING)) { //first payment OR on demand payment of a payment plan/membership
            $times = null;

            if ($payment_method == 'membership') {
                $interval = get_post_meta($tier_id, PPY_PREFIX . 'membership_interval', true);
                $timeframe = get_post_meta($tier_id, PPY_PREFIX . 'membership_timeframe', true);

            } elseif ($payment_method == 'plan') {
                $times = (int)get_post_meta($tier_id, PPY_PREFIX . 'number_of_payments', true);
                if ($times > 1 && !$payment->sequenceType !== \Mollie\Api\Types\SequenceType::SEQUENCETYPE_RECURRING) {
                    $times = $times - 1;
                }
                $interval = get_post_meta($tier_id, PPY_PREFIX . 'payment_frequency', true);
                $timeframe = get_post_meta($tier_id, PPY_PREFIX . 'payment_timeframe', true);
            }
            if (empty($interval)) {
                $interval = 1;
            }
            if (empty($timeframe)) {
                $timeframe = 'months';
            }

            //make the first subscription payment charge
            $start_date = date('Y-m-d', strtotime($subscription_date . ' + ' . $interval . ' ' . $timeframe));

            $initial_interval = get_post_meta($tier_id, PPY_PREFIX . 'initial_payment_frequency', true);
            if ( $initial_interval ) {
                $initial_payment_timeframe = get_post_meta($tier_id, PPY_PREFIX . 'initial_payment_timeframe', true);
                $start_date = date('Y-m-d', strtotime($subscription_date . ' + ' . $initial_interval . ' ' . $initial_payment_timeframe));
            }

            /*if ( $is_upsell ) { //charge right away if upsell
                $start_date =  date( 'Y-m-d', strtotime( $subscription_date  ) );
            }*/
            $mode = '';
            if (strpos($mo_key, 'test_') === 0) {
                $mode = '&testmode=1';
            }

            $payment_title = Poppyz_Core::generate_payment_title($subs_id, $total_with_tax, true);

            $customer = $this->mollie->customers->get($mollie_customer_id);

            try {
                $mollie_subscription = $customer->createSubscription(array(
                    "amount" => array('currency' => Poppyz_Core::get_currency(), 'value' => number_format($total_with_tax, 2, '.', '')),
                    "interval" => $interval . " $timeframe",
                    "startDate" => $start_date,
                    "description" => $payment_title,
                    "method" => NULL,
                    "webhookUrl" => site_url('?process-order-subscription&subs_id=' . $subs_id . $mode),
                    "times" => $times,
                    "metadata" => array(
                        "subs_id" => $subs_id,
                        "vatshifted" => $vatshifted,
                        "vat_out_of_scope" => $vat_out_of_scope,
                        "coupon" => $coupon
                    ),
                ));
                $fields['mollie_subscription_id'] = $mollie_subscription->id;
                $fields_format[] = '%s';
            } catch (Exception $exception) {
                error_log('Mollie create subscription error: ' . $exception->getMessage());
            }
        }


        $update = $wpdb->update(
            $this->subs_table,
            $fields,
            array('id' => $subs_id),
            $fields_format,
            array('%s')
        );
        return $update;
    }

    public function checkout(): void
    {
		$mo_key = Poppyz_Core::get_option( 'mo_api_key' );
		$mo_test_key = Poppyz_Core::get_option( 'mo_api_test_key' );
		if ( empty( $mo_key ) && empty($mo_test_key) )  {
			//print 'No API Key has been supplied for Mollie.';
			print 'No API Key has been supplied for Mollie.';
			exit;
		}

        if (!isset($_REQUEST[PPY_PREFIX . 'pay_nonce']) || !wp_verify_nonce($_REQUEST[PPY_PREFIX . 'pay_nonce'], PPY_PREFIX . 'process_payment')) {
            print 'Sorry, your nonce did not verify.';
            exit;
        }
        if (isset($_POST['upsell'])) {
            $ppy_sub = new Poppyz_Subscription();
            $sub_ids = [];
			$product_ids = [];
            foreach ($_POST['upsell'] as $tier_id) {
				$productType = get_post_type( $tier_id );
                $user_id = get_current_user_id();
				if ( $productType == "ppyv_product" ) {
					$parentProductId = ( isset($_GET['product_order_id']) ) ? $_GET['product_order_id'] : "";
					$sub_ids[] = $this->ppyv_mollie_checkout( $tier_id, $parentProductId, true );
				} else {
					$pending_id = Poppyz_Subscription::is_pending_tier($user_id, $tier_id);
					$price = get_post_meta($tier_id, PPY_PREFIX . 'tier_price', true);
					$initial_price = (float)get_post_meta( $tier_id, PPY_PREFIX . 'tier_initial_price', true );

					if (isset($_POST[PPY_PREFIX . 'subs_id'])) {
						$subs_id = $_POST[PPY_PREFIX . 'subs_id'];
					} elseif ($pending_id) {
						$subs_id = $pending_id;
						//update to latest information
						$data['price'] = $price;
						$data['date'] = date('d-m-Y');
						$data['payment_type'] = '';
						$data['initial_amount'] = $initial_price;
						$ppy_sub->update_subscription('update', $subs_id, $data);
					} else {
						$subs_id = $ppy_sub->add_pending_subscription($user_id, $tier_id, $price, '', $initial_price);
					}

					//If the Live Mollie API key is empty and the test API key is set, set the Test API key as default.
					if ( (empty( $mo_key ) || $mo_key == "" ) && !empty( $mo_test_key ) ) {
						$data['payment_mode'] = "Test";
						Poppyz_Subscription::update_subscription( "update", $subs_id, $data );
					}

					$sub_ids[] = $this->mollie_checkout($subs_id, true, $_POST[PPY_PREFIX . 'mollie_customer_id']);
				}

            }

            global $wp;
            $tier_return_page = get_post_meta($tier_id, PPY_PREFIX . 'tier_upsell_thank_you_page', true);
            //if tier return page is not set, use the default return page in plugin settings
            $return_page = ($tier_return_page) ? get_permalink($tier_return_page) : home_url($wp->request);
			if ( isset($_POST['subscription_id']) ) {
				$main_sub = $ppy_sub->get_subscription_by_id($_POST['subscription_id']);
				$upsell_product_id = ( !empty($sub_ids) ) ? implode(',', $sub_ids) :  "";
				$additional_product = (isset($_POST['additional_products-sold'])) ? "&additional_products-sold" : "";
				wp_redirect($return_page . '/?return&order_id=' . $main_sub->order_id . '&upsells-sold=' . $upsell_product_id . $additional_product);
			} else {
				$product_id = (  !empty($sub_ids) ) ? implode(',', $sub_ids) :  "";
				$additional_product = (isset($_POST['additional_products-sold'])) ? "&additional_products-sold" : "";
				wp_redirect($return_page . '/?product_order_id=' . $_POST['product_id'] . '&upsells-sold=' . $product_id . $additional_product);
			}

        } else {
            $subs_id = $_POST[PPY_PREFIX . 'subs_id'];
            $this->mollie_checkout($subs_id);
        }
    }

    /**
     * @param $payment
     * @param $subs_id
     * @param $payments_made
     */
    public function process_failed_payment($subs_id, $payment, $payments_made): void
    {
        //payment is returned as failed if there was a problem with processing of payment, check if it's because of insufficient funds.
        $insufficient_funds = ($payment->method == 'creditcard' && $payment->details->failureReason == 'insufficient_funds') ||
            ($payment->method == 'directdebit' && ($payment->details->bankReasonCode == 'MS03' || $payment->details->bankReasonCode == 'AM04'));

        Poppyz_Core::log('Method: ' . print_r($payment->method, true), 'process_order_subscription_failed_payment', true);
        Poppyz_Core::log('Details: ' . print_r($payment->details, true), 'process_order_subscription_failed_payment', true);

        if ($insufficient_funds) {
            Poppyz_Core::log('Insufficient funds.', 'process_order_subscription_failed_payment', true);
            $this->process_insufficient_funds($subs_id, $payment->id, $payments_made + 1);
        } else {
            $this->process_chargeback($subs_id, null, $payment->id);
            $this->cancel_membership($subs_id);
        }
    }

    public static function get_total_amount($sub_ids = array()) {

        $total_amount = 0;
        foreach ($sub_ids as $sub_id) {
            $ppy_sub = new Poppyz_Subscription();
            $subs = $ppy_sub->get_subscription_by_id( $sub_id );
            $subs_id = $subs->id;
            $tier_id = $subs->tier_id;
            $course_id = $subs->course_id;
            $payment_method = $subs->payment_method;
            $amount = $subs->amount;
            $user_id = $subs->user_id;
            $total_amount += Poppyz_Core::price_with_tax( $amount, $tier_id, $user_id );
        }
        return $total_amount;
    }

    /**
     * @return bool
     */
    public static function hasNoMollieKey(): bool
    {
        return (empty(Poppyz_Core::get_option('mo_api_key')) && Poppyz_Core::get_option('mo_api_key') == "") && (empty(Poppyz_Core::get_option('mo_api_test_key')) && Poppyz_Core::get_option('mo_api_test_key') == "");
    }

}