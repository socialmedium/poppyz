<?php
namespace Poppyz\Includes\Customer;

class Customer {

    protected $user;

    public function __construct( $user )
    {

    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
}