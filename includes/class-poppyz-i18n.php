<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://example.com
 * @since      0.5.0
 *
 * @package    Poppyz
 * @subpackage Poppyz/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.5.0
 * @package    Poppyz
 * @subpackage Poppyz/includes
 * @author     Socialmedium <info@socialmedium.nl>
 */
class Poppyz_i18n {

	/**
	 * The domain specified for this plugin.
	 *
	 * @since    0.5.0
	 * @access   private
	 * @var      string    $domain    The domain identifier for this plugin.
	 */
	private $domain = 'poppyz';

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.5.0
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain(
			$this->domain,
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}

	/**
	 * Set the domain equal to that of the specified domain.
	 *
	 * @since    0.5.0
	 * @param    string    $domain    The domain that represents the locale of this plugin.
	 */
	public function set_domain( $domain ) {
		//TODO: change how translation works.
		$this->domain = $domain;
	}

    /**
     * Handy  function to output translatable text
     *
     * @since 0.6.0
     */
    public function __s( $text, $echo = false) {
        if (!$echo) return __($text, $this->domain);
        _e($text, $this->domain);
    }

}
global $ppy_lang;
$ppy_lang = new Poppyz_i18n();