<?php
class Poppyz_Widgets {

    public function __construct() {
        require_once PPY_DIR_PATH . 'includes/widgets/poppyz-lesson-widget.php';
    }

    public function ppy_load_widgets() {
        register_widget( 'Poppyz_Lesson_Widget' );
    }

}
