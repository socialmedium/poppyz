<?php

/**
 * This handles all coupon methods
 *
 * @link       http://socialmedium.nl
 * @since      0.5.0
 *
 * @package    poppyz
 * @subpackage poppyz/includes
 */


class Poppyz_Coupon {

    /**
     * Get coupons
     *
     * Get all coupons
     *
     * @since 1.0
     * @param array $args Query arguments
     * @return mixed array if discounts exist, false otherwise
     */
    function get_coupons( $args = array() ) {
        $defaults = array(
            'post_type'      => PPY_COUPON_PT,
            'posts_per_page' => 30,
            'paged'          => null,
            'post_status'    => array( 'active', 'inactive', 'expired' )
        );

        $args = wp_parse_args( $args, $defaults );

        $coupons = get_posts( $args );

        if ( $coupons ) {
            return $coupons;
        }

        if( ! $coupons && ! empty( $args['s'] ) ) {
            // If no coupons are found and we are searching, re-query with a meta key to find discounts by code
            $args['meta_key']     = PPY_PREFIX . 'coupon_code';
            $args['meta_value']   = $args['s'];
            $args['meta_compare'] = 'LIKE';
            unset( $args['s'] );
            $coupons = get_posts( $args );
        }

        if( $coupons ) {
            return $coupons;
        }

        return false;
    }



    /**
     * Get coupon
     *
     * Retrieves a coupon by its ID.
     *
     * @since 1.0
     * @param integer $coupon_id Coupon ID
     * @return array
     */
    function get_coupon( $coupon_id = 0 ) {

        if( empty( $coupon_id ) ) {
            return false;
        }

        $coupon = get_post( $coupon_id );

        if ( get_post_type( $coupon_id ) != PPY_COUPON_PT ) {
            return false;
        }

        return $coupon;
    }

    /**
     * Get coupon by code
     *
     * @param string $code
     *
     * @since       1.0
     * @return      mixed
     */
    function get_coupon_by_code( $code = '' ) {

        if( empty( $code ) || ! is_string( $code ) ) {
            return false;
        }

        return $this->get_coupon_by( 'code', $code );

    }

    /**
     * Retrieve discount by a given field
     *
     * @since       2.0
     * @param       string $field The field to retrieve the discount with
     * @param       mixed $value The value for $field
     * @return      mixed
     */
    function get_coupon_by( $field = '', $value = '' ) {

        if( empty( $field ) || empty( $value ) ) {
            return false;
        }

        if( ! is_string( $field ) ) {
            return false;
        }

        switch( strtolower( $field ) ) {

            case 'code':
                $coupon = $this->get_coupons( array(
                    'meta_key'       => PPY_PREFIX . 'coupon_code',
                    'meta_value'     => $value,
                    'posts_per_page' => 1,
                    'post_status'    => 'any'
                ) );

                if( $coupon ) {
                    $coupon = $coupon[0];
                }

                break;

            case 'id':
                $coupon = $this->get_coupon( $value );

                break;

            case 'name':
                $coupon = get_posts( array(
                    'post_type'      => PPY_COUPON_PT,
                    'name'           => $value,
                    'posts_per_page' => 1,
                    'post_status'    => 'any'
                ) );

                if( $coupon ) {
                    $coupon = $coupon[0];
                }

                break;

            default:
                return false;
        }

        if( ! empty( $coupon ) ) {
            return $coupon;
        }

        return false;
    }

    /**
     * Saves a coupon, if it exists, update it.
     * @since 1.0
     * @param string $details
     * @param int $coupon_id
     * @return WP_Error|int Whether or not discount code was created
     */
    function save_coupon( $details, $coupon_id = null ) {

        $meta = array(
            'code'              => isset( $details['code'] )             ? $details['code']              : '',
            'name'              => isset( $details['name'] )             ? $details['name']              : '',
            'status'            => isset( $details['status'] )           ? $details['status']            : 'active',
            //'uses'              => isset( $details['uses'] )             ? $details['uses']              : '',
            //'max_uses'          => isset( $details['max'] )              ? $details['max']               : '',
            'amount'            => isset( $details['amount'] )           ? $details['amount']            : '',
            'start_date'             => isset( $details['start_date'] )            ? $details['start_date']             : '',
            'end_date'        => isset( $details['end_date'] )       ? $details['end_date']        : '',
            'usage_limit'        => isset( $details['usage_limit'] )       ? $details['usage_limit']        : '',
            'type'              => isset( $details['type'] )             ? $details['type']              : '',
            'tier_ids'              => isset( $details['tier_ids'] )             ? $details['tier_ids']              : '',
        );

        if ( $meta['name'] == '' || $meta['code'] == '' || $meta['amount'] == '' ) {
            return new WP_Error('missing_fields');
        }

		$meta['amount'] = Poppyz_Core::save_formatted_price( $meta['amount'] );

        if ( !is_numeric( $meta['amount']) ) {
            return new WP_Error('wrong_amount');
        }

        $start_timestamp        = strtotime( $meta['start_date'] );

        if( ! empty( $meta['start_date'] ) ) {
            $meta['start_date']      = date( 'd-m-Y H:i:s', $start_timestamp );
        }

        if( ! empty( $meta['end_date'] ) ) {

            $meta['end_date'] = date( 'd-m-Y H:i:s', strtotime( date( 'd-m-Y', strtotime( $meta['end_date'] ) ) . ' 23:59:59' ) );
            $end_timestamp      = strtotime( $meta['end_date'] );

            if( ! empty( $meta['start_date'] ) && $start_timestamp > $end_timestamp ) {
                // Set the end_date date to the start date if start is later than end_date
                $meta['end_date'] = $meta['start_date'];
            }
        }

        if ( ! empty( $coupon_id ) && $this->coupon_exists( $coupon_id ) ) {

            wp_update_post( array(
                'ID'          => $coupon_id,
                'post_title'  => $meta['name'],
                'post_status' => $meta['status']
            ) );

            foreach( $meta as $key => $value ) {
                update_post_meta( $coupon_id, PPY_PREFIX .'coupon_' . $key, $value );
            }

            return $coupon_id;

        } else {

            $coupon_id = wp_insert_post( array(
                'post_type'   => PPY_COUPON_PT,
                'post_title'  => $meta['name'],
                'post_status' => 'active'
            ) );

            foreach( $meta as $key => $value ) {
                update_post_meta( $coupon_id, PPY_PREFIX .'coupon_'  . $key, $value );
            }

            return $coupon_id;
        }

    }


    /**
     * Deletes a coupon.
     *
     * @since 1.0
     * @param int $coupon_id Coupon ID (default: 0)
     * @return void
     */
    function remove_coupon( $coupon_id = 0 ) {
        wp_delete_post( $coupon_id, true );

    }



    function update_coupon_status( $code_id = 0, $new_status = 'active' ) {
        $discount = $this->get_coupon(  $code_id );

        if ( $discount ) {
            wp_update_post( array( 'ID' => $code_id, 'post_status' => $new_status ) );
            return true;
        }

        return false;
    }

    public function get_bulk_actions() {
        $actions = array(
            'activate'   => __( 'Activate', 'easy-digital-downloads' ),
            'deactivate' => __( 'Deactivate', 'easy-digital-downloads' ),
            'delete'     => __( 'Delete', 'easy-digital-downloads' )
        );

        return $actions;
    }


    function coupon_exists( $code_id ) {
        if ( $this->get_coupon(  $code_id ) ) {
            return true;
        }

        return false;
    }


    function is_coupon_expired( $code_id = null ) {
        $coupon = $this->get_coupon(  $code_id );
        $return   = false;

        if ( $coupon ) {
            $expiration = $this->get_coupon_end_date( $code_id );
            if ( $expiration ) {
                $expiration = strtotime( $expiration );
                if ( $expiration < current_time( 'timestamp' ) ) {
                    $this->update_coupon_status( $code_id, 'inactive' );
                    $return = true;
                }
            }
        }

        return $return;
    }

    function is_coupon_valid( $code_id, $tier_id = null ) {
        $coupon = $this->get_coupon(  $code_id );
        $return = true;
        $error = false;
        global $ppy_lang;

        if ( $coupon ) {
            $start_date = $this->get_coupon_start_date( $code_id );
            $expiration = $this->get_coupon_end_date( $code_id );
	        $usage_limit = $this->get_coupon_usage_limit( $code_id );
	        $usages = $this->get_coupon_usages( $code_id );

            if ( $coupon->post_status == 'inactive' ) {
                $error = new WP_Error( 'invalid', __('The coupon you entered is invalid.' ,'poppyz') );
            }
            if ( $start_date ) {
                $start_date = strtotime( $start_date );
                if ( $start_date > current_time( 'timestamp' ) ) {
                    $error = new WP_Error( 'invalid', __('The coupon you entered is not yet valid.' ,'poppyz') );
                }
            }
            if ( $expiration ) {
                $expiration = strtotime( $expiration );
                if ( $expiration < current_time( 'timestamp' ) ) {
                    $error = new WP_Error( 'invalid', __('The coupon you entered is no longer valid.' ,'poppyz') );
                }
            }

	        if ( $usage_limit > 0 && $usages >= $usage_limit ) {
                $error = new WP_Error( 'invalid', __('Max coupon usage has been reached.' ,'poppyz') );
	        }

	        $coupon_tiers = $this->get_coupon_tiers( $code_id );
            if ( is_array($coupon_tiers) && $tier_id > 0 ) {
	            if ( !in_array( $tier_id, $coupon_tiers ) ) {
                    $error = new WP_Error( 'invalid', __('The coupon you entered does not work for this tier.' ,'poppyz') );
                }
            }
        } else {
            $error = new WP_Error( 'invalid', __('The coupon you entered is invalid.' ,'poppyz') );
        }

        if ( $error ) {
            Poppyz_Coupon::remove_coupon_session();
            return $error;
        }

        return $return;
    }

    public static function set_coupon_session( $coupon_code ) {
        setcookie('ppy_coupon', $coupon_code, time() + 3600, '/');
    }
    public static function remove_coupon_session(){
        if ( isset( $_COOKIE['ppy_coupon' ]) ) {
            unset( $_COOKIE['ppy_coupon'] ) ;
            setcookie('ppy_coupon', '', time() - 3600, '/');
        }
    }

    public function compute_discounted_price( $base_price, $tier_id = null, $coupon_code = null ) {
        if ( $coupon_code == null ) {
            $coupon_code = $_COOKIE['ppy_coupon'] ?? null;
        }
		$tier_discount = get_post_meta( $tier_id, PPY_PREFIX .  'tier_discount', true );
        if ( $coupon_code ) {
            $coupon = $this->get_coupon_by_code( $coupon_code );
            $id = $coupon->ID;
            $coupon_discount = $this->get_coupon_amount( $id );
            $type = $this->get_coupon_type( $id );
            if ( !is_wp_error( $this->is_coupon_valid( $id, $tier_id ) ) && $this->is_coupon_valid( $id, $tier_id ) ) {
                if ( $type == 'flat' ) {
                    $final_price =  $base_price - $coupon_discount;
                } else {
                    $final_price =  round( $base_price * ((100 - $coupon_discount) / 100), 3);
                }
                if ( $final_price < 0 ) $final_price = 0; //negative value is free
				if ( !empty($tier_discount) &&  $final_price > $tier_discount ) {
					$final_price = $final_price - $tier_discount;
				} else if ( !empty($tier_discount) &&  $final_price <= $tier_discount ) {
					$final_price = 0;
				}
                return $final_price;
            }
        }

		if ( !empty($tier_discount) &&  $base_price > $tier_discount ) {
			return $base_price - $tier_discount;
		} else if ( !empty($tier_discount) &&  $base_price <= $tier_discount ) {
			return 0;
		}

        return $base_price;
    }

    public function get_discount_amount( $base_price, $tier_id = null ) {
		$discount = 0;
        if ( !empty( $_COOKIE['ppy_coupon'] ) ) {
            $coupon = $this->get_coupon_by_code( $_COOKIE['ppy_coupon'] );
            $id = $coupon->ID;
            $coupon_discount = $this->get_coupon_amount( $id );
            $type = $this->get_coupon_type( $id );
            if ( $this->is_coupon_valid( $id ) ) {
                if ( $type == 'flat' ) {
                    $discount = $coupon_discount;
                } else {
                    $discount = round( $coupon_discount / 100 * $base_price, 3 );
                }
                if ( $discount < 0 ) $discount = 0; //negative value is free
            }
        }
		if ( !empty($tier_id) ) {
			$tier_discount = get_post_meta( $tier_id, PPY_PREFIX .  'tier_discount', true );
			if ( !empty($tier_discount) ) {
				$discount = $discount + $tier_discount;
			}
		}

        return $discount;
    }


    function get_coupon_start_date( $code_id = null ) {
        $start_date = get_post_meta( $code_id,  PPY_PREFIX . 'coupon_start_date', true );
        return $start_date;
    }
    function get_coupon_end_date( $code_id = null ) {
        $expiration = get_post_meta( $code_id, PPY_PREFIX . 'coupon_end_date', true );
        return $expiration;
    }
    function get_coupon_type( $code_id = null ) {
        $type = strtolower( get_post_meta( $code_id,  PPY_PREFIX . 'coupon_type', true ) );
        return $type;
    }
    function get_coupon_code( $code_id = null ) {
        $code = strtolower( get_post_meta( $code_id,  PPY_PREFIX . 'coupon_code', true ) );
        return $code;
    }
    function get_coupon_amount( $code_id = null ) {
        $amount = strtolower( get_post_meta( $code_id,  PPY_PREFIX . 'coupon_amount', true ) );
        return $amount;
    }

	function get_coupon_tiers( $code_id = null ) {
		$tiers =  get_post_meta( $code_id,  PPY_PREFIX . 'coupon_tier_ids', true );
		return $tiers;
	}

    function get_coupon_usages( $code_id = null ) {
        $usages =  get_post_meta( $code_id,  PPY_PREFIX . 'coupon_usages', true );
        if ( !$usages ) $usages = 0;
        return $usages;
    }



    function get_coupon_usage_limit( $code_id = null ) {
        $usages =  get_post_meta( $code_id,  PPY_PREFIX . 'coupon_usage_limit', true );
        if ( !$usages ) $usages = 0;
        return $usages;
    }

    function format_coupon_rate( $type, $amount ) {
        if ( $type == 'flat' ) {
            return Poppyz_Core::format_price( $amount );
        } else {
            return $amount . '%';
        }
    }

    public static function record_coupon_usage($id, $user_id) {
        self::increment_coupon_usage( $id );
        self::add_coupon_user( $id, $user_id );
    }

    public static function increment_coupon_usage( $id ) {
        $usages =  (int) get_post_meta( $id,  PPY_PREFIX . 'coupon_usages', true ) ;
        $usages = $usages + 1;
        update_post_meta( $id, PPY_PREFIX . 'coupon_usages', $usages );

    }
    public static function add_coupon_user( $id, $user_id ) {
        $saved_user_ids = get_post_meta( $id,  PPY_PREFIX . 'coupon_users', true );

        if (!$saved_user_ids) {
            $saved_user_ids = [$user_id];
        } elseif (!in_array($user_id, $saved_user_ids)){
            array_push($saved_user_ids, $user_id);
        }
        update_post_meta( $id, PPY_PREFIX . 'coupon_users', $saved_user_ids );
    }
    public static function get_coupon_users( $code_id ) {
        return get_post_meta( $code_id,  PPY_PREFIX . 'coupon_users', true );
    }
}
