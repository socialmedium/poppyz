<?php

class Affiliate_WP_Poppyz extends Affiliate_WP_Base {

    /**
     * Get things started
     *
     * @access  public
     * @since   1.0
     */
    public function init() {

        $this->context = 'poppyz';
        add_action( 'ppy_order_started', array( $this, 'add_pending_referral' ), 10, 2 );
        add_action( 'ppy_payment_completed', array( $this, 'mark_referral_complete' ) );
        add_action( 'ppy_payment_cancelled', array( $this, 'revoke_referral_on_delete' ) );
        add_filter( 'ppy_total_amount', array( $this, 'calculate_total_amount' ), 10, 2 );
    }

    function plugin_is_active() {
        return class_exists( 'Poppyz_Core' );
    }

    /**
     * Records a pending referral when an invoice is created
     *
     * @access  public
     * @since   1.0
     */
    public function add_pending_referral( $subscription_id = 0, $amount = null ) {

        if ( $this->was_referred() ) {
            $ppy_sub = new Poppyz_Subscription();
            $subs = $ppy_sub->get_subscription_by_id( $subscription_id );
            $course_id = $subs->course_id;
            $tier_id = $subs->tier_id;

            $client = get_user_by( 'id', $subs->user_id );

            if ( $this->is_affiliate_email( $client->user_email ) ) {
                return; // Customers cannot refer themselves
            }

            // get referral total

            $referral_total = $this->calculate_referral_amount( $amount, $subscription_id );

            // Referral description
            $desc = get_the_title( $course_id ) . ' - ' . get_the_title( $tier_id );

            if( empty( $desc ) ) {
                return;
            }

            // insert a pending referral
            $referral_id = $this->insert_pending_referral( $referral_total, $subscription_id, $desc );

        }

    }

    /**
     * Mark a referral as complete when an order is completed
     *
     * @access  public
     * @since   1.6
     */
    public function mark_referral_complete( $subscription_id ) {


        $referral = affiliate_wp()->referrals->get_by( 'reference', $subscription_id, $this->context );


        /*if ( empty( $referral ) ) {
	        $ppy_sub = new Poppyz_Subscription();
	        $subs = $ppy_sub->get_subscription_by_id(  $subscription_id );
	        $tier_id = $subs->tier_id;
	        $price = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true);
            $this->add_pending_referral( $subscription_id, $price );

        }*/

        $this->complete_referral( $subscription_id );

    }

    /**
     * Revoke a referral when an order is deleted
     *
     * @access  public
     * @since   1.6
     */
    public function revoke_referral_on_delete( $subscription_id = 0 ) {

        /*if( ! affiliate_wp()->settings->get( 'revoke_on_refund' ) ) {
            return;
        }*/

        $this->reject_referral( $subscription_id );

    }


    public function calculate_total_amount( $amount, $subscription_id ) {
        $referral = affiliate_wp()->referrals->get_by( 'reference', $subscription_id  );
        if ( $referral ) {
            $amount -= $referral->amount;
        }

        return $amount;
    }


}
new Affiliate_WP_Poppyz;
