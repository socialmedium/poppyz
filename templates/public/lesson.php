<?php
global $ppy_lang;
wp_reset_postdata();
if ( !$user_id ) {
    $content =  '<div id="et-boc" class="et-boc"><div class="et_pb_row et_pb_row_1 no-access-message">' . sprintf( __( 'You must first login to view lessons. Click <a href="%s">here</a> to login <br />','poppyz'),  wp_login_url( get_permalink() ) ) . '</div></div>';
} else {
    $current_user = wp_get_current_user();
    $lesson_id = get_the_ID();
    $ppy_subs = new Poppyz_Subscription();
    $access = $ppy_subs->is_subscribed( $lesson_id,  $user_id );
    if ( is_wp_error ( $access ) ) {
        $content = '<div id="et-boc" class="et-boc"><div class="et_pb_row et_pb_row_1 no-access-message">' . $access->get_error_message() . '</div></div>';
    } else {
        $content = Poppyz_Core::progress_button( $lesson_id, $current_user->ID, $content );
    }
    wp_reset_query();
}
echo $content;