<?php
Poppyz_Core::license_check();
global $wp_query;
$ppy_core = new Poppyz_Core();
global $ppy_lang;
$ppy_coupon = new Poppyz_Coupon();
$ppy_subscription = new Poppyz_Subscription();
global $wp;

if (  get_option( 'permalink_structure' ) ) {
    $slug = 'order-id/';
} else {
    $slug = '&order-id=';
}
$current_url =  get_permalink() . $slug . get_query_var( 'order-id' );

$tier_id = $ppy_core->get_tier_by_slug( get_query_var( 'order-id' ) );

$num_subs = Poppyz_Subscription::get_subscription_total_by_tier( $tier_id );
$subs_limit = get_post_meta( $tier_id, PPY_PREFIX . 'tier_subs_limit', true );

if ( $subs_limit > 0 && $num_subs >= $subs_limit ) {
    $limit_text = Poppyz_Core::get_option( 'tier_limit_reached_text' );
    echo '<div id="et-boc" class="et-boc"><div class="et_pb_row et_pb_row_1 no-access-message">';
    echo ($limit_text) ? $limit_text : __( 'The number of subscriptions allowed for this tier has been reached.','poppyz');
    echo '</div>';
    return;
}

$course_id = get_post_meta( $tier_id, PPY_PREFIX . 'tier_course', true );
$price = get_post_meta( $tier_id, PPY_PREFIX . 'tier_price', true );

$payment_method = get_post_meta( $tier_id, PPY_PREFIX . 'payment_method', true );
$title =  get_the_title( $course_id );
$tier_title =  get_the_title( $tier_id );
$content .= '<form id="ppy-form" method="post" action="' . $current_url . '">';
$purchase_expired =  $ppy_subscription->tier_purchase_expired($tier_id);

if ( $course_id && $purchase_expired === false ) {
    global $step;
    $step = 1;
    $_SESSION['step'] = 1;
    if ( is_user_logged_in() && isset( $_REQUEST['buy'] ) ) {
        $user = wp_get_current_user();
        $user_id = $user->ID;
        $restricted = false;
        $restricted = apply_filters( 'ppy_tier_is_restricted', $restricted, $tier_id, $user_id );
		$subscription = $ppy_subscription->get_subscriptions_by_user( $user_id, 'all', 'all', $tier_id );
		$tierDonation = get_user_meta( $user_id, PPY_PREFIX . 'tier_donation_amount', true );
		if (empty($tierDonation) && (isset($_POST[PPY_PREFIX . 'donation_amount_custom']) || isset($_POST[PPY_PREFIX . 'donation_amount']))) {
			$donation = ($_POST[PPY_PREFIX . 'donation_amount'] == "others") ? $_POST[PPY_PREFIX . 'donation_amount_custom'] : $_POST[PPY_PREFIX . 'donation_amount'];
			update_user_meta( $user_id, PPY_PREFIX . 'tier_donation_amount', $donation );
			$tierDonation = get_user_meta( $user_id, PPY_PREFIX . 'tier_donation_amount', true );
		}

		if ( isset($_POST[PPY_PREFIX . 'donation_amount']) && $_POST[PPY_PREFIX . 'donation_amount'] == "others" && ($_POST[PPY_PREFIX . 'donation_amount_custom'] == 0 || empty($_POST[PPY_PREFIX . 'donation_amount_custom'])) ) {
			$donationStatus = false;
		} else if ((isset($_POST[PPY_PREFIX . 'donation_amount']) && !empty($_POST[PPY_PREFIX . 'donation_amount'])) || (isset($tierDonation) && $tierDonation != 0 && !empty($tierDonation)))
		{
			$donationStatus = true;
		}   else {
			$donationStatus = false;
		}

		//it's a free course
		if ( $price == 0 &&  $donationStatus == false) {
            if ( !Poppyz_Subscription::is_subscribed_tier( $user_id, $tier_id ) ) {

				//check if there is a scheduled email donation set on the tier.
				$userScheduledDonation = Poppyz_Core::setScheduledDonation($tier_id, $user_id);

                // Add return page for free course
                $tier_return_page = get_post_meta( $tier_id, PPY_PREFIX . 'tier_thank_you_page', true );
                // Load the return page
                $return_page_link = ($tier_return_page) ? get_permalink($tier_return_page) : null;
                // Add Thank you message if return page is not set
                $tier_return_message = get_post_meta( $tier_id, PPY_PREFIX . 'tier_thank_you_content', true );

                global $plugin_public;
                remove_filter( 'the_title', array($plugin_public, 'change_wp_title') );
                $subs_id = Poppyz_Subscription::add_subscription( $user_id, $tier_id, 0 );

                Poppyz_Core::send_notification_email( $subs_id, $user_id, array('admin' => true), 'new_purchase');
                if ( $return_page_link ){
                    echo("<script>location.href = '".$return_page_link."'</script>");
                    die();
                } elseif ( $tier_return_message ) {
                    $content .=  '<div class="grey-box">' . Poppyz_Core::the_content($tier_return_message) . '</div><br />';
                } else {

                    $content .= '<div class="grey-box">' . sprintf(  __( "You can now start your course. Click <a href='%s'>here</a> to begin." ,'poppyz'), get_permalink( $course_id ) )  . '</div>';
                }
            } else {
                $content .=  '<div class="grey-box">' . sprintf( __( 'You are already subscribed to this tier. Please click <a href="%s ">here</a> to view the course.','poppyz' ), get_the_permalink( $course_id ) ) . '</div>';
            }
        } else {
            $missing = Poppyz_User::check_missing_fields();
            $step = 2;
            if ( $missing ) {
                $content .= '<p class="complete-fields">' . Poppyz_Core::get_option('registration_form_description' ) . '</p>';

                $content .= '<div class="grey-box">';
                foreach ( $missing as $name => $value ) {
                    //if the company field is missing, change the label to Company/Your name to inform the user that he can use his name if there is no company
                    //if ($value ==  'company' ) $name = __( 'Company' ) . '/' . __( 'Your name' );

                    $post_value =  isset( $_POST["reg_" . $value ] ) ? $_POST["reg_" . $value ]  : '';
                    $exclude = apply_filters( 'ppy_custom_fields_exclude', array() );
                    if ( $value == 'country' ) {
                        $content .= '<fieldset><label for="' . $value . '">' . __( 'Country' ,'poppyz') . '</label>' . Poppyz_Core::country_select( $value, $post_value ) . '</fieldset>';
                        continue;
                    }
                    if ( !in_array( $value, $exclude) ) {
                        $content .= '<fieldset><label for="' . $value . '">' . $name . '*</label><input type="text" id="' . $value . '" name="' . $value . '" value="' . $post_value . '" /></fieldset>';
                    }
                    $content = apply_filters( 'ppy_custom_fields_html_missing', $content, '', $name, $value );
                }
                $content .= '</div>';

                $content .= '<input type="hidden" name="save_custom_fields" />';
                $content .= '<input type="submit" name="buy" value="' . __('Save','poppyz') . '" class="align-right" />';
                $content = apply_filters( 'step2_content_after', $content );
            } elseif ($user->first_name == '' || $user->last_name == '') {
                $content .= __( 'First name and last name is required to proceed. Please edit your profile and try again.' ,'poppyz');
            }
            elseif ( $restricted ) {
                $content .= $restricted;
            } elseif ( $restricted ) {
                $content .= $restricted;
            }  elseif ( Poppyz_Subscription::is_pending_plan( $user_id, $tier_id ) ) {
                $content .=  '<div>' . sprintf( __( 'You have already started paying for this program round by  SEPA direct debit. However, this has stopped because you have insufficient funds in your bank account or because you have reversed this manually in your own online banking environment. You must make the remaining payments manually via your <a href="%s">account page</a>' ,'poppyz'),  get_permalink( Poppyz_Core::get_option( 'account_page' ) ) ) . '</div>';
            }
            else {
                $step = 3;
                if ( !isset( $_POST['pay-now'] ) ) {

                    $ppy_sub = new Poppyz_Subscription();

                    $final_price = $price;

                    $initial_price = (float)get_post_meta( $tier_id, PPY_PREFIX . 'tier_initial_price', true );

                    $pending_id = Poppyz_Subscription::is_pending_tier( $user_id, $tier_id );
					$donation_enabled = Poppyz_Core::is_donation_enabled( $tier_id );;
					if ( $donation_enabled
						&& isset($_POST[PPY_PREFIX . 'donation_amount'])
						&& $_POST[PPY_PREFIX . 'donation_amount'] > 0
					) {
						$final_price = (float)$_POST[PPY_PREFIX . 'donation_amount'];
					}
                    if ( isset( $_POST[PPY_PREFIX . 'subs_id'] ) ) {
                        $subs_id = $_POST[PPY_PREFIX . 'subs_id'];
                    } elseif ( $pending_id ) {
                        $subs_id = $pending_id;
                        //update to latest information
                        $data['price'] = $final_price;
                        $data['date'] = date('d-m-Y');
                        $data['payment_type'] = '';
                        $data['initial_amount'] = $initial_price;
                        $ppy_sub->update_subscription( 'update', $subs_id, $data );
                    } else {
                        $subs_id = $ppy_sub->add_pending_subscription( $user_id, $tier_id, $final_price, '', $initial_price );
                    }

					if ( isset($tierDonation) && !empty($tierDonation) ) {
						//set donation amount to usermeta using user ID and Subscription ID.
						update_user_meta( $user_id, PPY_PREFIX . 'donation_amount_'.$subs_id, $tierDonation);
						delete_user_meta( $user_id, PPY_PREFIX . 'tier_donation_amount');
					}
					$price = $final_price;

					$tier_discount = get_post_meta( $tier_id, PPY_PREFIX .  'tier_discount', true );

                    $content .=  '<div id="review-page-content">' .  Poppyz_Core::the_content( Poppyz_Core::get_option( 'review_page_content' ) ) . '</div>';
                    $content .= '<div id="review-details">';
                        $content .= '<h3>' .  __( 'Order Overview' ,'poppyz') . '<a href="' .  get_permalink( Poppyz_Core::get_option( 'edit-profile_page' ) ) . '?redirect_to=' . $current_url . '?buy">' . __( 'Edit Details' ,'poppyz') . '</a></h3>';
                        $content .= '<div class="table-wrapper">';
                            $content .= '<table>';
                            $content .= '<tr>';
                            $content .= '<th>' . $title . '</th>';
                            $content .= '<td>' . $tier_title . '</td>';
                            $content .= '</tr>';
                            $content .= '<tr>';

                            if ( $payment_method == 'plan' ) {


                                $content .= '<th>' .__( 'Payment Plan' ,'poppyz') . '</th>';

                                $content .= '<td>' . apply_filters( 'ppy_tier_title', $tier_id, $price, 'payment_details' ) .  '</td>';
                            } elseif ( $payment_method == 'membership' ) {
                                $membership_duration =  get_post_meta( $tier_id, PPY_PREFIX . 'membership_duration', true );
                                $content .= '<th>' .__( 'Membership' ,'poppyz') . '</th>';
                                $content .= '<td id="base-price">' . apply_filters( 'ppy_tier_title', $tier_id, $price, 'payment_details' ) .  '</td>';
                            } else {
                                $content .= '<th>' .__( 'Price' ,'poppyz') . '</th>';
                                $content .= '<td id="base-price">' . Poppyz_Core::format_price( $price ) . '</td>';
                            }

							if ( !empty($tier_discount) ) {
								$content .= '<tr>';
								$content .= '<th>' .__( 'Promotional discount' ,'poppyz') . '</th>';
								$content .= '<td id="base-price"> - ' . Poppyz_Core::format_price( $tier_discount ) . '</td>';
								$content .= '</tr>';
								$price = $final_price;
							}


                            $content .= '<tr id="coupon-info" style="display: none;">';
                            $content .= '<th>' . __( 'Coupon','poppyz' ) . ' </th>';
                            $content .= '<td></td>';
                            $content .= '</tr>';

                            $content .= '<tr id="subtotal" style="display: none;">';
                            $content .= '<th>' . __( 'Subtotal','poppyz' ) . ' </th>';
                            $content .= '<td></td>';
                            $content .= '</tr>';

							if ( !empty($tier_discount) && $price > $tier_discount ) {
								$content .= '<tr id="final-price">';
								$content .= '<th>' .__( 'Price' ,'poppyz') . '</th>';
								$content .= '<td>'.$price-$tier_discount.'</td>';
								$content .= '</tr>';
							} else if ( !empty($tier_discount) && $price < $tier_discount ) {
								$content .= '<tr id="final-price">';
								$content .= '<th>' .__( 'Price' ,'poppyz') . '</th>';
								$content .= '<td>0</td>';
								$content .= '</tr>';
							} else {
								$content .= '<tr id="final-price" style="display: none;">';
								$content .= '<th>' .__( 'Price' ,'poppyz') . '</th>';
								$content .= '<td></td>';
								$content .= '</tr>';
							}

							if ( !empty($tier_discount) && $tier_discount < $final_price ) {
								$final_price = $final_price - $tier_discount;
							} else if ( !empty($tier_discount) && $tier_discount > $final_price ) {
								$final_price = 0;
							}

							if ($initial_price) {
								$final_price = $initial_price;
							}


							$tax_amount =  Poppyz_Core::get_fixed_tax_amount( $final_price, $tier_id, Poppyz_Core::get_price_tax( $tier_id ) );
							$tax_rate = (float)Poppyz_Core::get_tax_rate( $tier_id, $user_id );
							$total_with_tax = Poppyz_Core::price_with_tax( $final_price, $tier_id, $user_id );
							$final_tax = Poppyz_Core::format_price( $tax_amount ) . ' ('. Poppyz_Core::format_number($tax_rate) .'%)';

							$included_text = '';
							if ( Poppyz_Invoice::is_vatshifted() ) {
								$final_tax = __( '0 (VAT shifted.)','poppyz' );
							} else if ( Poppyz_Invoice::is_vat_out_of_scope() ) {
								$final_tax = __( '0 (VAT out of scope.)' ,'poppyz');
							} else if ( Poppyz_Invoice::is_vat_disabled() || $donation_enabled ) {
								$final_tax = __( '0 (No VAT.)','poppyz' );
							} else {
								$included_text = __( ' - VAT is included' ,'poppyz');
							}

							$bij =  ( Poppyz_Core::get_price_tax( $tier_id ) == 'inc' ) ? $included_text : '';
							$main_tier_vat =  ( Poppyz_Core::get_price_tax( $tier_id ) == 'inc' ) ? 0 : $tax_amount;
							$content .= '<tr id="tax" >';
							$content .= '<th>' . __('VAT' ,'poppyz') . ' </th>';
							$content .= '<input type="hidden" id="main_tier_vat" value="'.$main_tier_vat.'"/>';
							$content .= '<input type="hidden" id="main_tier_vat_raw" value="'.$tax_amount.'"/>';
							$content .= '<td>' . $final_tax . $bij . '</td>';
							$content .= '</tr>';

							//get sub_id of main tier
							$sub_id = Poppyz_Subscription::is_pending_tier( $user_id, $tier_id );

							//additional product
							$additionaProduct = "";
							$sub_ids = array($sub_id);


							if ( isset( $_POST['reg_submit']) || isset( $_POST['buy']) ) {
								$additionalProducts = !empty($_POST['additional_products']) ? $_POST['additional_products'] : null;
								Poppyz_Core::set_additional_products_cookie($additionalProducts);
							}

							if ( isset($_POST['additional_products']) && !empty($_POST['additional_products']) ) {
								$additionalProducts = $_POST['additional_products'];
							} else {
								$additionalProducts = Poppyz_Core::get_additional_products_cookie();
							}

                            //if this tier has bundled products, remove option for clients to choose
                            $is_tier_bundled_product = Poppyz_Core::is_tier_bundled_products($tier_id);
                            if ($is_tier_bundled_product)  {
                                $additionalProducts = Poppyz_Core::get_tier_additional_products($tier_id, true);
                            }

							$bundledProducts = get_post_meta( $tier_id, PPY_PREFIX . 'tier_bundled_products',  true );
							$hideAdditionalProductPrice = ( $bundledProducts == "yes" ) ? "hide-additiona-product-price" : "" ;

                            if ($additionalProducts) {
                                foreach ($additionalProducts as $additionalProductId) {
									$eproductVersionValid = (defined('PPYV_VERSION') && version_compare(PPYV_VERSION, '1.4.8.5') >= 0) ? true : false;
									$productType = get_post_type( $additionalProductId );
									$allowAdditionalProduct =(  $productType == "ppyv_product" && $eproductVersionValid == false ) ? false : true;
									if ($allowAdditionalProduct == true) {
										$price_additional_product = get_post_meta($additionalProductId, PPY_PREFIX . 'tier_price', true);
										$sub_ids[] = Poppyz_Subscription::is_pending_tier($user_id, $additionalProductId);;
										$tax_amount_tax_additional_product = Poppyz_Core::get_fixed_tax_amount($price_additional_product, $additionalProductId, Poppyz_Core::get_price_tax($additionalProductId));
										$tax_rate_tax_additional_product = (float)Poppyz_Core::get_tax_rate($additionalProductId, $user_id);
										$total_with_tax_additional_product = Poppyz_Core::price_with_tax($price_additional_product, $additionalProductId, $user_id);
										$final_tax_additional_product = Poppyz_Core::format_price($tax_amount_tax_additional_product) . ' (' . Poppyz_Core::format_number($tax_rate_tax_additional_product) . '%)';

										$included_text_additional_product = '';
										if (Poppyz_Invoice::is_vatshifted()) {
											$final_tax_additional_product = __('0 (VAT shifted.)', 'poppyz');
										} else if (Poppyz_Invoice::is_vat_out_of_scope()) {
											$final_tax_additional_product = __('0 (VAT out of scope.)', 'poppyz');
										} else if (Poppyz_Invoice::is_vat_disabled() || $donation_enabled) {
											$final_tax_additional_product = __('0 (No VAT.)', 'poppyz');
										} else {
											$included_text_additional_product = __(' - VAT is included', 'poppyz');
										}
										$bij_additional_product = (Poppyz_Core::get_price_tax($additionalProductId) == 'inc') ? $included_text_additional_product : '';


										$productTitle = get_the_title($additionalProductId);
										$additionaProduct .= "<li>";
										$additionaProduct .= "<div class='additional-product-title-list'>" . $productTitle . "</div>";
										$additionaProduct .= "<div class='additional-product-tax " . $hideAdditionalProductPrice . "'>" . Poppyz_Core::format_price($price_additional_product) . " ( " . $final_tax_additional_product . $bij_additional_product . " ) </div>";
										$additionaProduct .= "</li>";
									}
                                }
                                $content .= '<tr>';
                                $content .= '<th>' . __( 'Additional Products','poppyz' ) . ' </th>';
                                $content .= '<td><ul class="additional-product-list-ul">'.$additionaProduct.'</ul></td>';
                                $content .= '</tr>';
                            }

							$donation = get_user_meta( $user_id, PPY_PREFIX . 'donation_amount_'.$subs_id, true );
							if ( (isset($_POST[PPY_PREFIX . 'donation_amount']) && !empty($_POST[PPY_PREFIX . 'donation_amount']) )
								|| (isset($donation) && !empty($donation) ) ) {

								$donation = ( isset($_POST[PPY_PREFIX . 'donation_amount']) && !empty($_POST[PPY_PREFIX . 'donation_amount'])) ? $_POST[PPY_PREFIX . 'donation_amount'] : $donation;
								$donation = ( $donation == "others" ) ? $_POST[PPY_PREFIX . 'donation_amount_custom'] : $donation;
								$donation = ( !empty($donation) ) ? $donation : 0;
								$donationFormatted = Poppyz_Core::format_price( $donation );
								$content .= '<tr id="donation" >';
								$content .= '<th>' . __('Donation' ,'poppyz') . ' </th>';
								$content .= '<td>' . $donationFormatted;
								$content .= '<input type="hidden" name="' . PPY_PREFIX . 'donation_amount" value="'.$donation.'"/>';
								$content .=  '</td>';
								$content .= '</tr>';
							}

							$total = Poppyz_Core::getTierTotalPrice($tier_id);
							if ( isset($donation) && !empty($donation) ) {
								$total = $total + $donation;
							}
							$total_vat = Poppyz_Core::getTotalTax($tier_id);
							$totalIncludeVAT = $total + $total_vat;

							$total_unfiltered_vat = Poppyz_Core::getUnfilteredTotalTax($tier_id);

							$content .= '</tr>';
							if (!empty($additionalProducts)) {
								$content .= '<tr id="vat" >';
								$content .= '<th>' . __('Total VAT', 'poppyz') . ' </th>';
								$content .= '<td><span id="total-vat-output" >' . Poppyz_Core::format_price($total_unfiltered_vat);
								$content .= '</span></td>';
								$content .= '</tr>';
							}

							$content .= '<tr id="total" >';
							$content .= '<th>' . __('Total Payment' ,'poppyz') . ' </th>';
							$content .= '<input type="hidden" id="total-vat" value="'.$total_unfiltered_vat.'"/>';
							$content .= '<input type="hidden" id="unfiltered-total-vat" value="'.$total_unfiltered_vat.'"/>';
							$content .= '<input type="hidden" id="main_tier_price_include_vat" value="'.$totalIncludeVAT.'"/>';
							$content .= '<input type="hidden" id="total_payment" value="'.$totalIncludeVAT.'"/>';
							$content .= '<td id="total_payment_output">' . Poppyz_Core::format_price($totalIncludeVAT);
							$content .=  '</td>';
							$content .= '</tr>';

                            $content .= '<tr>';
                            $content .= '<th>' . __( 'Company' ,'poppyz') . '</th>';
                            $company = get_user_meta( $user->ID, PPY_PREFIX . 'company', true ) ? get_user_meta( $user->ID, PPY_PREFIX . 'company', true ) : '-';
                            $content .= '<td>' . $company . '</td>';
                            $content .= '</tr>';
                            $content .= '<tr>';
                            $content .= '<th>' .__( 'Name','poppyz' ) . '</th>';
                            $content .= '<td>' . $user->first_name . ' ' . $user->last_name . '</td>';
                            $content .= '</tr>';
                            $content .= '<tr>';
                            $content .= '<th>' .__( 'Address','poppyz' ) . '</th>';
                            $content .=
                                '<td>' .
                                get_user_meta( $user->ID, PPY_PREFIX . 'address', true ) . '<br />' .
                                get_user_meta( $user->ID, PPY_PREFIX . 'zipcode', true )   .  ' ' . get_user_meta( $user->ID, PPY_PREFIX . 'city', true )    . '<br />' .
                                get_user_meta( $user->ID, PPY_PREFIX . 'country', true )   .
                                '</td>';

                            $content .= '</tr>';

                            $vat_number = get_user_meta( $user->ID, PPY_PREFIX . 'vatnumber', true );
                            if ( $vat_number ) {
                                $content .= '<tr>';
                                $content .= '<th>' .__( 'VAT Number' ,'poppyz') . '</th>';
                                $content .= '<td>' . $vat_number . '</td>';
                                $content .= '</tr>';
                            }

							if ( Poppyz_Core::get_option( 'coupons_disabled' ,'poppyz') != 'on' && !$donation_enabled ) {
                                $content .= '<tr id="coupon-container">';
                                $content .= '<th>' .__( 'Coupon','poppyz' ) . '</th>';
                                $content .= '<td>
                                        <div class="coupon-form" style="display: none;">
                                            <input type="text" id="coupon" autocomplete="off" value="" />
                                            <input type="hidden" id="tier_id" name="tier_id" value="' . $tier_id . '"> 
                                            <input type="button" id="apply-coupon" value="' . __( 'Apply' ,'poppyz') . '" data-price="' . $price . '" />
                                        </div>
                                        <a href="#" class="coupon-add-link">' . __( 'Do you have a coupon?','poppyz' ) .'</a>
                                        </td>';
                                $content .= '</tr>';
                            }


                            if ( !empty( $_COOKIE['ppy_coupon'] ) ) {
                                $content .= '<script>';
                                $content .= 'jQuery( document ).ready(function($) {';
                                $content .= "\n$('#coupon').val('" . $_COOKIE['ppy_coupon'] . "');\n";
                                $content .= "$('#apply-coupon').trigger('click');\n";
                                $content .= "})";
                                $content .= '</script>';
                            }

                            $content .= '</table>';
                        $content .= '</div>';
                    $content .= '</div>';

                    $content .= '<div id="terms"><div id="terms-content">';

                        $hide_terms = Poppyz_Core::get_option( 'hide_terms' );

                        if ( !$hide_terms ) {
                            $terms_condition = Poppyz_Core::terms_condition();
                            $terms_checkboxes = Poppyz_Core::terms_checkboxes();
                            $terms_condition = str_replace( '[company]',  Poppyz_Core::get_option('invoice_company'), $terms_condition );
                            $content .= $terms_condition;
                            $content .= '<div class="terms-checkboxes">' . $terms_checkboxes . '</div>';
                        }
                        $is_automatic = get_post_meta( $tier_id, PPY_PREFIX . 'payment_automatic', true );

                        if ( $is_automatic != 'manual' && ( $payment_method == 'plan' || $payment_method == 'membership' ) ) {
                            $sepa_text = Poppyz_Core::get_option( 'sepa_text' );
                            $sepa = '<h3>' . __('Direct debit (SEPA)','poppyz') . '</h3>';
                            $sepa_checkbox = Poppyz_Core::get_option( 'sepa_checkbox' );
                            $sepa .= '<div class="backdrop"></div>';
                            $sepa .= '<p><input type="checkbox" class="terms-checkbox" id="show-sepa" name="show-sepa" value="1" /> <label for="show-sepa">' . $sepa_checkbox . '</label></p>';
                            $sepa .= '<a href="#" class="lightbox-toggle">' . __('More information','poppyz') . '</a>';
                            $sepa .= '<div id="sepa" class="poppyz-popup"> <div class="close">x</div>';
                            //$sepa .= '<br /><strong>' . __( 'Incassant-id' ) . ':</strong> Test' ;
                            //$sepa .= '<br /><strong>' . __( 'Kenmerk Machtiging' ) . ':</strong> ' . $subscription->payment_id ;
                            $sepa_text = str_replace( '[company]',  Poppyz_Core::get_option('invoice_company'), $sepa_text );
                            $sepa .= Poppyz_Core::the_content( $sepa_text );


                            require_once PPY_DIR_PATH . 'public/class-poppyz-user.php';
                            $user_fields = Poppyz_User::custom_fields();
                            foreach ( $user_fields as $field => $value ) {
                                if ($value == 'vat_out_of_scope') {
                                    if (  get_user_meta( $user->ID, PPY_PREFIX . $value, true ) ) {
                                        $sepa .= '<br/> VAT out of scope activated (just activate this when your company is registered outside the EU).';
                                    }
                                    continue;
                                }
                                $sepa .= '<br /><strong>' . $field . ':</strong> ' . get_user_meta( $user->ID, PPY_PREFIX . $value, true );
                            }

                            $sepa .= '</div>';
                            $content .= $sepa;
                        }

                        if ( !empty( $terms_checkboxes ) ) {
                            $content .= Poppyz_Core::terms_alert();
                        }
					$content .= '</div>';
					$mo_api_test_key = Poppyz_Core::get_option('mo_api_test_key');
						if( current_user_can('administrator') && !empty($mo_api_test_key) ) {
							$content .= '<p><input type="checkbox" class="activate-test-mode" id="activate-test-mode" name="mollieTestMode" value="1" /> <label for="activate-test-mode">' . __("This is a test order (only visible to admins).", "poppyz") . '</label></p>';
						}

                        $content .= '<input type="hidden" name="pay-now" value="1" />';
                        $content .= '<input type="hidden" value="' . $subs_id . '" name="' . PPY_PREFIX . 'subs_id" id="' . PPY_PREFIX . 'subs_id"  />';
                        $pay_now_text = apply_filters( 'ppy_pay_now_text',  __( 'Pay now' ,'poppyz')  );

                        $content .= '<input id="pay-now" class="cart" type="submit" value="' . $pay_now_text . '" name="' . PPY_PREFIX . 'purchase" />';
                    $content .= '</div>';
                    // $content .= '<input type="submit" class="align-right" name="buy" value="' . __('Proceed to payment') . '" />';
                } else {
                    /*$custom_fields = Poppyz_User::custom_fields();
                    foreach ( $custom_fields as $name => $value ) {
                        $$value = get_user_meta( $user_id, PPY_PREFIX . $value, true );
                    }*/


                    //affiliate discount
                    //$final_price = apply_filters( 'ppy_total_amount', $final_price );

                    /*$invoice = new Poppyz_Invoice();
                    //$_SESSION['invoice_id'] = 1;

                    $details = array();

                    $details['user_id'] = $user_id;
                    $details['tier_id'] = $tier_id;
                    $details['amount'] = $final_price;
                    $details['subscribe-course-date'] = date( "m/d/Y H:i:s" );

                    $invoice_id = $invoice->save_invoice( $details, $user_id );


                    $step = 4;

                    if ( $invoice_id !== false) { //check first if an invoice was generated; if it returns false most probably no credentials were entered in the plugin settings.
                        $invoice_path = site_url( '/?process_invoice=' . $invoice_id );
                        $content .= '<input id="pay-now" class="cart" type="submit" value="' . __( 'Pay now' ) . '" name="' . PPY_PREFIX . 'purchase" />';
                        $content .= '<h3>' .  __( 'Invoice' ) . '</h3>';
                        $content .= '<div id="review-details">';
                        $content .= Poppyz_Core::the_content( Poppyz_Core::get_option( 'invoice_page_content' ) );
                        $content .= '<h5>' . sprintf( __('This invoice has been sent to your email <strong>(%s)</strong>'), $user->user_email ) . '</h5>';

                        $content .= '</div>';

                        $content .= '<iframe height="500px" width="100%" id="invoice-pdf" src="' . $invoice_path . '"></iframe>';
                        $content .= '<input type="hidden" value="' .$invoice_id . '" name="' . PPY_PREFIX . 'subs_id" />';
                    } else {
                        $content .= '<h3>' .  __( 'Cannot proceed with the payment. Please contact the website administrator to enable invoicing.' ) . '</h3>';
                    }*/
                }

            }
        }
    } else {
        $content .= do_shortcode('[purchase-button tier_id="' . $tier_id .'" content="' . $content . '"]');
    }
    $content .=  wp_nonce_field( PPY_PREFIX . 'process_payment', PPY_PREFIX . 'pay_nonce', false );


    $content .= '</form>';
} else if ( $purchase_expired !== false ) {
	if ( !empty(Poppyz_Core::get_option( 'tier_purchase_link_expired_text' ) ) ){
		$content .=  Poppyz_Core::get_option( 'tier_purchase_link_expired_text' );
	} else {
		$content .= __( 'Sorry, this link has already expired.' ,'poppyz');
	}
} else {
    $content .= __( 'Order not found.','poppyz' );
}
echo $content;

wp_reset_query();

