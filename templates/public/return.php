<?php
global $ppy_lang;
if ( !is_user_logged_in() ) {
    $content .= sprintf( __( 'You must first login to view courses. Click <a href="%s">here</a> to login. <br />','poppyz'),  wp_login_url( get_permalink() ) );
    return;
}

$current_user = wp_get_current_user();
$user_id = $current_user->ID;
$ppy_sub = new Poppyz_Subscription;

$subscription = false;

if ( isset( $_GET['order_id'] ) ) {
    $subscription = Poppyz_Subscription::get_subscription_by_order( $_GET['order_id'], $user_id );
} elseif ( isset( $_GET['success'] ) && isset( $_GET['subs_id'] )  ) {
    $subscription = $ppy_sub->get_subscription_by_id( $_GET['subs_id'] );
}
$ppy_payment = new Poppyz_Payment();
$ppy_payment->_initialize_mollie($subscription->payment_mode);

$subs = [$subscription];


$upsell_sold = !empty($_GET['upsells-sold']) ? explode(",", $_GET['upsells-sold']) : '';

if ( $upsell_sold ) {
	foreach ($upsell_sold as $upsell_sub_id) {
		$subscription2 = $ppy_sub->get_subscription_by_id( $upsell_sub_id );
		$subs[] = (!empty($subscription2)) ? $subscription2 : $upsell_sub_id;
	}
}

if (isset($_GET['additional_products-sold'])) {
    $payment  = $ppy_payment->mollie->payments->get( $subscription->payment_id );
    $additional_product_subscriptions = !empty( $payment->metadata->additional_product_subscriptions ) ? $payment->metadata->additional_product_subscriptions : null;
	$additional_eproduct_subscriptions = !empty( $payment->metadata->additional_eproduct_subscriptions ) ? $payment->metadata->additional_eproduct_subscriptions : null;
    //it should not be a bundled product because additional products of that type don't have invoices (because they are free)
    if (empty( $payment->metadata->has_bundled_products ) ) {
        foreach (explode(',', $additional_product_subscriptions) as $id ) {
            $additional_product_subscription = $ppy_sub->get_subscription_by_id( $id );
            $subs[] = (!empty($additional_product_subscription)) ? $additional_product_subscription : $upsell_sold;
        }
    }
	if (empty( $payment->metadata->has_bundled_products ) ) {
		foreach (explode(',', $additional_eproduct_subscriptions) as $id ) {
			$subs[] = $id;
		}
	}
}


$content .= '<div class="return-content et_pb_row et_pb_row_0 poppyz_row">';

/*if ( !empty ( $_GET[ 'upsells'] ) ) {
    $subscription = null; //prevent showing of subscription
    $sold_upsells = explode(',', $_GET[ 'upsells'] );
    foreach ( $sold_upsells as $u ) {

    }
}*/
$payment  = $ppy_payment->mollie->payments->get( $subscription->payment_id );
if ($subscription->payment_status == 'open' && ($payment->status == 'open' || $payment->status == 'paid')) {
	echo  '<div class="et_pb_row et_pb_row_0 poppyz_row"><h2 class=" et_pb_row">'.__("Payment on process", "poppyz").'</h2></div>';
	$content .= "
		<script>
			$(document).ready(function(){
				$('.return-content').hide();
				$('#payment-failed-text').hide();
                $('.entry-title').hide();
			});
			function get_payment_id(count) {
						let payment_id ='';
						var data = {
							action: 'is_payment_successful',
							sub_id : '" . $subscription->id . "',
							security: ppy_object.ppy_ajax_nonce,
						};
						$.ajax ({
							type: 'POST',
							url: ppy_object.ajax_url,
							data: data,
							success: function( response ) {
                                if ( response != '' ) {
                                    location.reload();
                                }
                                if ( count == 4 ) {
                                    $('.entry-title').fadeIn();
                                    $('#payment-failed-text').fadeIn();
                                }
							}
						});
						return payment_id;
					}
			for (let i = 0; i < 4; i++) {
				setTimeout(function(){get_payment_id(i);}, 2000);
			}
		</script>";
}

if ($subscription != null && $subscription->payment_status == 'paid') {
    global $plugin_public;
    remove_filter( 'the_title', array($plugin_public, 'change_wp_title') );

    foreach ( $subs as $sub ) {

		$productType = get_post_type( $sub );

		if ( $productType == "ppyv_product" ) {
			//check is there is an eproduct Upsell.
			$product_id = $sub;

			$product_link = get_permalink( $product_id );
			$product_name = get_the_title( $product_id );

			$payment_successful_message = get_post_meta( $product_id, PPY_PREFIX . 'tier_thank_you_content', true );

			if ( !$payment_successful_message ) {
				$payment_successful_message = "<p>" .  __( "You can now view your product. Click [product-link] to begin.", "poppyz" )   . "</p>";
			}

			$payment_successful_message = str_replace( '[product-link]',  sprintf(  __( "<a href='%s'>here</a>", "poppyz" ), $product_link ), $payment_successful_message );
			$payment_successful_message = str_replace( '[name]', $current_user->first_name . ' ' . $current_user->last_name , $payment_successful_message );

			$content .=  '<div class="grey-box et_pb_row">' . $payment_successful_message . '</div><br />';
			$ppy_lang = new Poppyz_i18n();
			$mb = '';
			$invoice_id = Poppyz_Core::ppy_get_invoice( $product_id, $user_id );
			if ( ( isset( $_GET['invoice'] ) && $_GET['invoice'] == 'mb' ) && Poppyz_Core::ppy_get_invoice_mb( $product_id,  $user_id ) ) { //display mb invoice instead
				$invoice_id = Poppyz_Core::ppy_get_invoice_mb( $product_id,  $user_id );
				$mb = '&invoice=mb&product=' . $product_id;
			}
			remove_filter( 'the_title', 'ppyv_change_wp_title' );
			if ( $invoice_id !== false ) { //check first if an invoice was generated; if it returns false most probably no credentials were entered in the plugin settings.
				$invoice_path = site_url( '/?process_invoice=' . $invoice_id . $mb );
				$content .= '<h3>' .  $ppy_lang->__s( 'Invoice' ) . '</h3>';
				$content .= '<div id="review-details">';
				$content .= Poppyz_Core::the_content( Poppyz_Core::get_option( 'invoice_page_content' ) );
				$content .= '<h5>' . sprintf( $ppy_lang->__s('This invoice has been sent to your email <strong>(%s)</strong>'), $current_user->user_email ) . '</h5>';

				$content .= '</div>';

				$content .= '<iframe height="500px" width="100%" id="invoice-pdf" src="' . $invoice_path . '"></iframe>';
				$content .= '<input type="hidden" value="' .$invoice_id . '" name="' . PPY_PREFIX . 'subs_id" />';
			}


			if ( Poppyz_Core::get_tier_upsells( $product_id ) && !isset($additional_eproduct_subscriptions) ) {
				try {
					$ppy_payment = new Poppyz_Payment();
					$ppy_payment->_initialize_mollie();

					if (!isset($_GET['upsell-pass']) && !isset($_GET['upsells-sold'])) { //upsell-pass means the customer clicked on the cancel upsell button, so display the regular invoice
						$showUpsell = show_ppyv_upsell_form( $content, $product_id );
						if ($showUpsell) {
							//echo $showUpsell;
							//do not proceed with the rest if there's an upsell
							return;
						}
					}
					$payment_id = get_post_meta( $product_id, PPYV_PREFIX . $user_id . '_payment_id' );
					$payment  = $ppy_payment->mollie->payments->get( $payment_id[0] );
					$invoice_title = $payment->description;

					$sepa_text = Poppyz_Core::get_option( 'sepa_text_after' );
					$sepa = '<div id="sepa">';

					//$sepa .= '<br /><strong>' . __( 'Incassant-id' ) . ':</strong> Test' ;
					//$sepa .= '<br /><strong>' . __( 'Kenmerk Machtiging' ) . ':</strong> ' . $subscription->payment_id ;
					$sepa .= '<h3>' . $invoice_title . '</h3>';
					$sepa_text = str_replace( '[company]',  Poppyz_Core::get_option('invoice_company'), $sepa_text );
					$sepa .= Poppyz_Core::the_content( $sepa_text );

					require_once PPY_DIR_PATH . 'public/class-poppyz-user.php';
					$user_fields = Poppyz_User::custom_fields();



				} catch (Exception $e) {

				}
			}
		} else if ( is_object($sub) )  {
			$payment_successful_message = get_post_meta( $sub->tier_id, PPY_PREFIX . 'tier_thank_you_content', true );

			$options = get_option( 'ppy_settings' );
			$user_name = !empty($current_user->first_name) ?  $current_user->first_name : $current_user->display_name;

			$course_name = get_the_title( $sub->course_id ) . ' - ' . get_the_title( $sub->tier_id );
			$course_link = get_permalink( $sub->course_id );

			if ( empty( $payment_successful_message ) ) {
				$payment_successful_message = $options['return_page_success_content'];

				if ( empty($payment_successful_message) ) {
					$payment_successful_message = "<p>" .  __( "You can now start your course [course-name]. Click [course-link] to begin." ,'poppyz')   . "</p>";
					//$payment_successful_message = "<p>" . sprintf(  __( "Hi %s, <br /> Your payment has been completed so you can now start the course %s. Goodluck with your lessons!" ), $user_name, $course_name )  . "</p>";
				}
			}

			$payment_successful_message = str_replace( '[course-link]',  sprintf(  __( "<a href='%s'>here</a>"  ,'poppyz'), $course_link ), $payment_successful_message );
			$payment_successful_message = str_replace( '[course-name]',  $course_name, $payment_successful_message );
			$payment_successful_message = str_replace( '[name]', $user_name, $payment_successful_message );
			$payment_successful_message = Poppyz_Core::the_content( $payment_successful_message );
			$content .=  '<div class="grey-box">' . $payment_successful_message . '</div>';


			$invoice = new Poppyz_Invoice();
			$mb = '';
			$invoice_id = Poppyz_Invoice::get_invoice_by_subscription( $sub->id );
			if ( isset( $_GET['invoice'] ) && $_GET['invoice'] == 'mb' ) { //display mb invoice instead
				$invoice_id = $sub->invoice_id;
				$mb = '&invoice=mb';
			}
			if ( $invoice_id !== false ) { //check first if an invoice was generated; if it returns false most probably no credentials were entered in the plugin settings.
				$invoice_path = site_url( '/?process_invoice=' . $invoice_id . $mb );
				$content .= '<h3>' .  __( 'Invoice'  ,'poppyz') . '</h3>';
				$content .= '<div id="review-details">';
				$content .= Poppyz_Core::the_content( Poppyz_Core::get_option( 'invoice_page_content' ) );
				$content .= '<h5>' . sprintf( __('This invoice below has been sent to your email <strong>(%s)</strong>','poppyz'), $current_user->user_email ) . '</h5>';

				$content .= '</div>';

				$content .= '<iframe height="500px" width="100%" class="invoice-pdf" src="' . $invoice_path . '"></iframe>';
				$content .= '<input type="hidden" value="' .$invoice_id . '" name="' . PPY_PREFIX . 'subs_id" />';
				$content .= '<hr />';
			}
		}


    }

    if ( $subscription->payment_type ) {
        $payment_type = $subscription->payment_type;
        $is_automatic = $subscription->payment_type == 'automatic';
    } else {
        $is_automatic = get_post_meta(  $subscription->tier_id, PPY_PREFIX . 'payment_automatic', true );
        $payment_type = ( $is_automatic ) ? 'automatic' : 'manual';
    }

	$has_upsell = false;
	$upsell_data = Poppyz_Core::get_tier_upsells( $subscription->tier_id );
	foreach($upsell_data as $data) {
		if ( !empty($data) ) {
			$has_upsell = true;
			break;
		}
	}

    if ( Poppyz_Core::get_tier_upsells( $subscription->tier_id ) && $is_automatic && $has_upsell == true/*&& ( $subscription->payment_method == 'plan' || $subscription->payment_method == 'membership' )*/  ) {
        try {

            $ppy_payment = new Poppyz_Payment();
            $ppy_payment->_initialize_mollie();

            if (!isset($_GET['upsell-pass']) && !isset($_GET['upsells-sold'])) { //upsell-pass means the customer clicked on the cancel upsell button, so display the regular invoice
                $showUpsell = Poppyz_Subscription::show_upsell_form( $content, $subscription);
                if ($showUpsell) {
					echo '<form id="ppy-form" class="upsells v2" action="" method="post">';
                    echo $showUpsell;
					echo '<div class="actions upsells-action"><button name="' . PPY_PREFIX . 'purchase" class="proceed et_pb_button poppyz-confirm" id="poppyz-confirm" style="margin-bottom: 10px;">' . __('Add to order' ,'poppyz') . '</button><br />';
                  	echo '<a class="cancel" href="?return&upsell-pass&order_id='. $_GET['order_id']. '">' . __('No, thank you','poppyz') . '</a></div>';
					echo '</form>';
                    //do not proceed with the rest if there's an upsell
                    return;
                }
            }
            $invoice_title = $payment->description;

            $sepa_text = Poppyz_Core::get_option( 'sepa_text_after' );
            $sepa = '<div id="sepa">';

            //$sepa .= '<br /><strong>' . __( 'Incassant-id' ) . ':</strong> Test' ;
            //$sepa .= '<br /><strong>' . __( 'Kenmerk Machtiging' ) . ':</strong> ' . $subscription->payment_id ;
            $sepa .= '<h3>' . $invoice_title . '</h3>';
            $sepa_text = str_replace( '[company]',  Poppyz_Core::get_option('invoice_company'), $sepa_text );
            $sepa .= Poppyz_Core::the_content( $sepa_text );

            require_once PPY_DIR_PATH . 'public/class-poppyz-user.php';
            $user_fields = Poppyz_User::custom_fields();

            foreach ( $user_fields as $field => $value ) {
                $sepa .= '<br /><strong>' . $field . ':</strong> ' . get_user_meta( $subscription->user_id, PPY_PREFIX . $value, true );
            }

            $customer = $ppy_payment->mollie->customers->get( $subscription->mollie_customer_id );
            $mandate = $customer->getMandate( $payment->mandateId );

            $sepa .= '';
            $has_mandate = false;
            if ( $mandate ) {
                $mandate_details = $mandate->details;
                foreach ( $mandate_details as $field => $value ) {
                    if ( in_array( $field, array( 'cardFingerprint', 'cardSecurity', 'feeRegion' ) ) ) continue;
                    if ( $field == 'consumerName' ) {
                        $field = __( 'Name','poppyz' );
                    } elseif ( $field == 'consumerAccount' ) {
                        $field = 'IBAN (' . __( 'Bank Account Number','poppyz' ) . ')';
                    } elseif ( $field == 'consumerBic' ) {
                        $field = __( 'Bic','poppyz' );
                    } elseif ( $field == 'cardHolder' ) {
                        $field = __( 'Card Holder','poppyz' );
                    } elseif ( $field == 'cardNumber' ) {
                        $field = __( 'Card Number' ,'poppyz');
                    } elseif ( $field == 'cardLabel' ) {
                        $field = __( 'Card Label','poppyz' );
                    } elseif ( $field == 'cardExpiryDate' ) {
                        $field = __( 'Card Expiry Date','poppyz' );
                    }

                    $sepa .= '<br /><strong>' . $field . ':</strong> ' . $value;
                    $has_mandate = true;
                }
            }

            $sepa .= '<br /><strong>' . __('Date','poppyz') . ':</strong> ' . date_i18n( get_option( 'date_format' ), strtotime( $payment->paidAt ) );

            $sepa .= '</div>';
            $content .= $sepa;

            /*$upsells = Poppyz_Core::get_tier_upsells( $subscription->tier_id );

            $can_buy = false;
            foreach ( $upsells as $upsell_id ) { //check if the client already has subscribed, if he's subscribed to all the tiers already dont show the form
                if (Poppyz_Subscription::is_subscribed_tier( $user_id, $upsell_id ) ) continue;
                $can_buy = true;
            }

            if ( $has_mandate && $upsells && count( $upsells ) > 0 && $can_buy ) {
                $content .= '<form id="upsells-form" action="" method="post">';
                $upsell_content =  Poppyz_Core::get_tier_upsell_description( $subscription->tier_id ) ? Poppyz_Core::get_tier_upsell_description( $subscription->tier_id ) : '<h1>Upsell products: </h2>';
                $content .= Poppyz_Core::the_content( $upsell_content );
                $can_buy = false;
                    $content .= '<ul>';
                    foreach ( $upsells as $upsell_id ) {
                        if (Poppyz_Subscription::is_subscribed_tier( $user_id, $upsell_id ) ) continue; //skip if already subscribed to the tier
                        $content .= '<li><label>';
                            $content .= '<input type="checkbox" value="' . $upsell_id . '" name="upsell[]">' . get_the_title( $upsell_id );
                        $content .= '</li></label>';
                    }
                    $content .= '</ul>';
                    $content .= '<input type="submit" value="' . __( 'Buy' ) . '" name="' . PPY_PREFIX . 'purchase" class="et_pb_contact_submit et_pb_button" />';
                    $content .= '<input type="hidden" value="' . $customer->id . '" name="' . PPY_PREFIX . 'mollie_customer_id" />';
                    $content .= wp_nonce_field( PPY_PREFIX . 'process_payment', PPY_PREFIX . 'pay_nonce', false );

                $content .= '</form>';
            }*/
        } catch (Exception $e) {

        }
    }

    $content .= Poppyz_Core::return_action( 'ppy_after_purchase_content', $subscription );
    //$content .= __( 'You can now start with the course' ) . ' <a style="margin-left: 20px;" class="btn" href="' . $course_link . '">' . __( 'Click here to start!' ) . '</a>';
} else {
    $payment_failed_message = Poppyz_Core::get_option('return_page_failed_content');
    if ( !$payment_failed_message ) {
        $payment_failed_message = "<h2>" . __( 'You were not able to purchase the course.','poppyz' ) . '</h2>';
    }
    $content .= "<div id='payment-failed-text'>" . $payment_failed_message . "</div>";
}
$content .= '</div>';
echo $content;