<?php
global $ppy_lang;
$ppy_sub = new Poppyz_Subscription();
$ppy_payment = new Poppyz_Payment();
$current_user = wp_get_current_user();
$user_id = $current_user->ID;
require_once PPY_DIR_PATH . 'public/class-poppyz-user.php';
$user_fields = Poppyz_User::custom_fields();

/*if ( isset( $_GET['cancel_pending'] ) ) {
    $sub = $ppy_sub->get_subscription_by_id( $_GET['cancel_pending'] );
    if ( $sub ) {
        if ($sub->user_id == $user_id) {
            if ( $sub->payment_method == 'membership' ||  $sub->payment_method == 'plan' ) {
                $cancelled = $ppy_payment->cancel_membership( $sub->id );
            }
            $ppy_sub->delete_subscription( $sub->id, false );
            $content .= '<p>' . __( "Your subscription has been cancelled. You will be able to access your tier until the expiration date of your subscription." ). '</p>';

        }
    }
}*/


$subscriptions = $ppy_sub->get_subscriptions_by_user( $user_id, 'on', 'standard' );
$planned_subscriptions = $ppy_sub->get_planned_subscriptions_by_user( $user_id );
$membership_subscriptions = $ppy_sub->get_membership_subscriptions_by_user( $user_id );
$pending_subscriptions = $ppy_sub->get_subscriptions_by_user( $user_id, 'pending' );
$hide_invoices = apply_filters( 'ppy_hide_invoices', false, $user_id );

/*if (isset($_GET['test'])) {
    $ppy_payment->_initialize_mollie();
    $customer = $ppy_payment->mollie->customers->get('');
    $subscription = $customer->getSubscription('');
    var_dump($subscription);
}*/

$content .= '<div id="subscriptions-container">';
$content .= '<a class="et_pb_button alignright" href="' . wp_logout_url() . '">' . __('Logout','poppyz') . '</a><br /><br />';

if ( isset($_POST['cancel_membership'])) {
    $cancel = $ppy_payment->cancel_membership( $_POST['subs_id'] );
    $ppy_sub->delete_subscription( $_POST['subs_id'], false );
    $content .= '<div><p>' . __( "Your subscription has been cancelled. You will be able to access your tier until the expiration date of your subscription." ). '</p></div>';
}

if ( isset( $_GET['pay_pending'] ) ) {
    $ppy_sub = new Poppyz_Subscription();
    $subs = $ppy_sub->get_subscription_by_id( $_GET['pay_pending'] );

    if ( $subs && $subs->user_id == $user_id ) {
        $content .= '<form method="post" action="' . get_permalink() . '">';
        $invoice_path = site_url('/?process_invoice=' . $_GET['pay_pending']);
        $content .= '<iframe height="500px" width="100%" id="invoice-pdf" src="' . $invoice_path . '"></iframe>';
        $content .= '<input type="hidden" value="' . $_GET['pay_pending'] . '" name="' . PPY_PREFIX . 'subs_id" />';
        $content .= '<input type="submit" value="' . __('Pay now','poppyz') . '" name="' . PPY_PREFIX . 'purchase" />';
        $content .= wp_nonce_field(PPY_PREFIX . 'process_payment', PPY_PREFIX . 'pay_nonce', false);
        $content .= '</form>';
    } else {
        $content .= '<h3>' . __( 'Invoice not found.' ,'poppyz') . '</h3>';
    }

} else {
    if ( $subscriptions ) {
        $content .= '<div class="subscription">';
        $content .= '<h3>' . __( 'Subscriptions' ,'poppyz') . '</h3>';
        $content .= '<div class="table-wrapper">';
        $content .= '<table class="subscription-list ppy-table">';
        $content .= '<thead>';
        $content .= '<tr>';
        $content .= '<th>' . __( 'Course','poppyz'). '</th>';
        $content .= '<th>' . __( 'Tier' ,'poppyz'). '</th>';
        $content .= '<th>' . __( 'Invoices' ,'poppyz') . '</th>';
        $content .= '</tr>';
        $content .= '</thead>';
        $content .= '<tbody>';
        foreach ( $subscriptions as $subs ) {
            $invoices_html = '';
			$tier_status = get_post_status($subs->tier_id);
            if ( !$hide_invoices ) {
                $invoices =  Poppyz_Invoice::get_subscription_invoices( $subs->id );
            } else {
                $invoices = false;
            }

            if ( $subs->payment_status != 'admin' ) {
                if ( $invoices ) {
                    foreach ( $invoices as $invoice ) {

                        if (isset($invoice->invoice_id)) { //this is from the payments table and not from the custom post type invoice id
                            $invoice_id = $invoice->invoice_id;
                        } else {
                            $invoice_id = $invoice->ID; //this is from the post table
                        }
                        $title = get_the_title($invoice_id );
                        $payment_data = $ppy_payment->get_payment_by_invoice( $invoice_id );
                        if ( $payment_data ) {
                            if ($payment_data->invoice_type == 'moneybird') {
                                $title = $payment_data->invoice_id_other;
                            }
                        }

                        $invoices_html .= '<a href="' . site_url('?process_invoice=' .  $invoice_id ) . '">' . $title . '</a> ';
                        if ($payment_data && $payment_data->invoice_type == 'plugandpay') {
                            $invoices_html = __('Mailed via Plug&Pay', 'poppyz');
                        }
                    }
                }
            } else {
                $invoices_html .= __( 'Subscribed by the admin' , 'poppyz');
            }

            $content .= "<tr>";
            $removed_tier_text = "";
			if ( $tier_status == "publish" ) {
				$content .= '<td><a href="' . get_permalink($subs->course_id) . '">' . get_the_title($subs->course_id) . '<br /></a><small>' . date_i18n(get_option('date_format'), strtotime($subs->subscription_date)) . '</small></td>';
			} else {
				$content .= '<td><strong>' . get_the_title($subs->course_id) . '</strong></strong><br /><small>' . date_i18n(get_option('date_format'), strtotime($subs->subscription_date)) . '</small></td>';
				$removed_tier_text = "<br/><strong>Removed by admin</strong>";
            }

            $content .= '<td>' . get_the_title( $subs->tier_id ) . $removed_tier_text . '</td>';
            $content .= '<td>' . $invoices_html . '</td>';
            $content .= "</tr>";
        }
        $content .= '</tbody>';
        $content .= '</table>';
        $content .= '</div>';
        $content .= '</div>';
    }

    if ( $planned_subscriptions ) {
        $content .= '<div class="subscription">';
        $content .= '<h3>' . __( 'Payment Plans' , 'poppyz'). '</h3>';
        $content .= '<div class="table-wrapper">';
        $content .= '<table class="subscription-list ppy-table">';
        $content .= '<thead>';
        $content .= '<tr>';
        $content .= '<th>' . __( 'Course', 'poppyz') . '</th>';
        $content .= '<th>' . __( 'Tier', 'poppyz') . '</th>';
        $content .= '<th>' . __( 'Payments' , 'poppyz') . '</th>';
        $content .= '<th>' . __( 'Invoices' , 'poppyz') . '</th>';
        $content .= '</tr>';
        $content .= '</thead>';
        $content .= '<tbody>';
        foreach ( $planned_subscriptions as $subs ) {
            $subscription_date = $subs->subscription_date;

            $number_of_payments = $subs->tier_number_of_payments;


            $payments_made = $subs->payments_made;

			$tier_status = get_post_status($subs->tier_id);

            if ( $payments_made == 0 && $subs->payment_status != 'admin' && $subs->payment_type != 'manual' ) {
                continue; //if 0 payment and not added by admin, it means this payment is pending and should not appear.
            }

            $payments_left = $number_of_payments - $payments_made;

            $payment_frequency = $subs->payment_frequency;
            $payment_timeframe = $subs->payment_timeframe;
            if ( empty( $payment_timeframe ) ) {
                $payment_timeframe = 'months';
            }

            if ( $payments_left > 0 && $subs->payment_status != 'admin' ) {
                $due_date = strtotime( $subscription_date . ' + ' . $payment_frequency * ($number_of_payments - 1) . " $payment_timeframe" );
                $status = ( time() >  $due_date  ) ?  '<br /><strong class="overdue">' . __( 'Overdue' , 'poppyz') . '</strong>' : '';
            } else {
                $status = '<br />' .__( 'Fully Paid' , 'poppyz');
            }

            $invoices_html = [];
            $invoice = [];

            if ( !$hide_invoices ) {
                $invoices =  Poppyz_Invoice::get_subscription_invoices( $subs->id );
            } else {
                $invoices = false;
            }
            if ( $invoices ) {
                foreach ( $invoices as $invoice ) {
                    if ( isset( $invoice->invoice_id ) ) { //this is from the payments table and not from the custom post type invoice id
                        $invoice_id = $invoice->invoice_id;
                    } else {
                        $invoice_id = $invoice->ID;
                    }
                    $payment_data = $ppy_payment->get_payment_by_invoice( $invoice_id );
                    $title = get_the_title( $invoice_id );
                    if ( $payment_data ) {
                        $is_chargedback = ( $payment_data->status == 'chargedback')  ? ' - ' . __( 'Chargedback', 'poppyz') : '';
                        $invoice_payments_made = $payment_data->number_of_payment;

                        if ($payment_data->invoice_type == 'moneybird') {
                            $title = $payment_data->invoice_id_other;
                        }
                    } else {
                        $invoice_payments_made = get_post_meta( $invoice_id, PPY_PREFIX . 'invoice_payments_made', true );
                        $is_chargedback = Poppyz_Invoice::has_credit_invoice( $subs->id, $invoice_payments_made ) ? ' - ' . __( 'Chargedback' , 'poppyz') : '';
                    }
                    $invoices_html[$invoice_payments_made] = '<a href="' . site_url('?process_invoice=' .  $invoice_id ) . '">' . $title . '</a>' . $is_chargedback;
                    if ($payment_data && $payment_data->invoice_type == 'plugandpay') {
                        $invoices_html[$invoice_payments_made] = __('Mailed via Plug&Pay', 'poppyz');
                    }
                }
            }
            $is_automatic = get_post_meta(  $subs->tier_id, PPY_PREFIX . 'payment_automatic', true );
            if ( $subs->payment_type == 'automatic' || $subs->payment_type == '' ) {
                $is_automatic = true;
            }
            if ( $subs->payment_type == 'manual' ) {
                $is_automatic = false;
            }
            $content .= "<tr>";
			if ( $tier_status == "publish") {
				$content .= '<td><a href="' . get_permalink($subs->course_id) . '">' . get_the_title($subs->course_id) . '<br /></a><small>' . date_i18n(get_option('date_format'), strtotime($subs->subscription_date)) . '</small></td>';
			} else {
				$content .= '<td><strong>' . get_the_title($subs->course_id) . '</strong><br /><small>' . date_i18n(get_option('date_format'), strtotime($subs->subscription_date)) . '</small></td>';
            }


			if ( empty($tier_status) ) {
				$content .= '<td><strong>' . __( 'Removed by admin', 'poppyz'). '</strong></td>';
			} else {
				$content .= '<td>' . get_the_title( $subs->tier_id ) . $status . '</td>';
			}

            $content .= '<td>';
            if ( $subs->payment_status == 'cancelled' && $tier_status == "publish" ) {
                $content .= '<span class="red dashicons-before dashicons-no-alt">' . __( 'Cancelled' , 'poppyz') . '</span>';
            } elseif ( $subs->payment_status == 'admin' ) {
                $content .= '<span class="green dashicons-before dashicons-saved">' . __( 'Subscribed by the admin', 'poppyz'). '</span><br />';
            } else {
                $has_pay_button = false; //only limit the pay button to 1 instance

                if ( $subs->version == 2 ) {
                    $insufficient_funds = $ppy_payment->get_insufficient_funds_payments($subs->id);
                    // $number_of_payments = $number_of_payments + $insufficient_funds;
                }
                // var_dump($number_of_payments);
                $invoice_text = '';

				if ( empty($tier_status) ) {
					$number_of_payments = count($invoices_html);
                }
                $payments_content = "";
                for ( $i=0; $i < $number_of_payments; $i++ ) {

                    $frequency = $payment_frequency * $i ;
                    $due_date =  date_i18n( get_option( 'date_format' ), strtotime( $subscription_date . ' + ' . $frequency . ' ' . $payment_timeframe ));
                    $initial_interval = get_post_meta($subs->tier_id, PPY_PREFIX . 'initial_payment_frequency', true);
                    if ( $i > 0 && $initial_interval ) {
                        $initial_payment_timeframe = get_post_meta($subs->tier_id, PPY_PREFIX . 'initial_payment_timeframe', true);
                        $due_date = date_i18n(get_option( 'date_format' ), strtotime( $subs->subscription_date . ' + ' . $initial_interval . ' ' . $initial_payment_timeframe));
                        if ( $i > 1 ) {
                            $due_date = date_i18n(get_option( 'date_format' ), strtotime( $due_date . ' + ' . ( $frequency - 1 ). ' ' . $initial_payment_timeframe));
                        }
                    }
                    $payment_data = null;

                    if ( $subs->version == 2 ) {
                        $payment_data = $ppy_payment->get_payment_by_number( $subs->id, $i+1 );
                        $payment_needed = $payment_data ? $payment_data->status != 'paid' && $payment_data->status != 'chargeback-paid' : true;
                        $x = true;
                    } else {
                        $payment_needed =  $i == $payments_made;
                        $x = $i <= $payments_made - 1 ;
                    }

                    if ( $x && !$payment_needed ) {
                        $due_date_status = ' - <span class="green dashicons-before dashicons-saved">' . __( 'Paid' , 'poppyz') . '</span>';
                        if ( !$hide_invoices ) {
                            $invoiceContent = ( $invoices && isset( $invoices_html[$i+1] ) ) ?  $invoices_html[$i+1] : ' - ' . __( 'Deleted' , 'poppyz');
                            $invoice_text .= "<li id='invoice_".$i."' class='account_page_plan_invoice' num='".$i."'>".$invoiceContent."</li>";
                            //$due_date_status .= $invoice_text;
                        }
                    } else {
                        $due_date_status = ' - <span class="gray dashicons-before dashicons-clock">' . __( 'Remaining payment' , 'poppyz') . '</span>';

                        if ( !$has_pay_button && $payment_needed && (!$is_automatic || $insufficient_funds ) ) { //if it's payment needed and with insufficient funds, then show the button since it's still automatic
                            $has_pay_button = true;
                            $due_date_status = '<form method="post" action="' . Poppyz_Core::get_tier_purchase_url( $subs->tier_id ) .'?buy">';
                            $due_date_status .= wp_nonce_field( PPY_PREFIX . 'process_payment', PPY_PREFIX . 'pay_nonce', false );
                            $due_date_status .= '<input type="hidden" name="pay-now" value="1" >';
                            $due_date_status .= '<input type="hidden" name="' . PPY_PREFIX . 'subs_id' . '" value="' . $subs->id  . '" >';

                            if ( $payment_data ) {
                                //$due_date_status .= $payment_data->number_of_payment .'<br />';
                                $due_date_status .= '<input type="hidden" name="' . PPY_PREFIX . 'payment_number' . '" value="' . $payment_data->number_of_payment . '" >';
                            }

                            $due_date_status .= '<input class="manual-pay-btn et_pb_button" type="submit" value="' . __( 'Pay' , 'poppyz') .  '" name="' . PPY_PREFIX . 'purchase' . '" >';
                            $due_date_status .= '</form>';
                            //$due_date_status .= ' - <a href="' . Poppyz_Core::get_tier_purchase_url( $subs->tier_id ) . '?buy&sub_id=' . $subs->id .'">' . __( 'Pay' ) . '</a>';
                        }
                    }
					$payments_content .= "<li id='date_status_".$i."' class='plan_date_status' num='".$i."'>".$due_date . $due_date_status . '</li>';
                }
                if ( $number_of_payments != $payments_made ) { //not fully paid yet
                    //$content .= '<a href="?cancel_pending=' . $subs->id . '">' . __( 'Cancel' ) . '</a>';
                }
            }
			$content .= "<ul class='account_page_ul'>".$payments_content ."</ul>";
            $content .= '</td>';
            $content .= '<td class="top">';
            $content .=  "<ul class='account_page_ul'>".$invoice_text."</ul>";
            $content .= '</td>';


            $content .= "</tr>";
        }
        $content .= '</tbody>';
        $content .= '</table>';
        $content .= '</div>';
        $content .= '</div>';
    }
    if ( $membership_subscriptions ) {
        $content .= '<div class="subscription">';
        $content .= '<h3>' . __( 'Memberships', 'poppyz') . '</h3>';
        $content .= '<div class="table-wrapper">';
        
        $content .= '<table class="subscription-list ppy-table">';
        $content .= '<tr>';
        $content .= '<th>' . __( 'Course', 'poppyz') . '</th>';
        $content .= '<th>' . __( 'Tier' , 'poppyz') . '</th>';
        $content .= '<th>' . __( 'Payments' , 'poppyz'). '</th>';
        $content .= '</tr>';

        foreach ( $membership_subscriptions as $subs ) {

            $subscription_date = $subs->subscription_date;
            $due_date_status = '';
            $payments_made = $subs->payments_made;
            $membership_interval = !empty ( $subs->membership_interval ) ? $subs->membership_interval : 1;
            $membership_timeframe = !empty ( $subs->membership_timeframe ) ? $subs->membership_timeframe : 'months';

            $due_date =  strtotime( $subscription_date . ' + ' . $membership_interval * $payments_made . ' ' . $membership_timeframe );
            $due_date_string =  date_i18n( get_option( 'date_format' ), $due_date );
            $tier_status = get_post_status($subs->tier_id);

			$status = '';
            if ( time() >  $due_date && $subs->payment_status != 'admin' && $subs->payment_status != 'cancelled' )  {
                $status = '<br /><strong class="overdue">' . __( 'Overdue', 'poppyz') . '</strong>';
            } else if ( $tier_status != "publish" ) {
                //if tier is deleted
				$status = '<strong class="red">' . __( 'Removed by admin' , 'poppyz') . '</strong>';
			} else if ($subs->payment_status == 'cancelled') {
                $status = '<br /><strong class="red">' . __( 'Cancelled' , 'poppyz') . '</strong>';
            }

            $content .= "<tr>";
            if ( $tier_status == "publish" ) {
				$content .= '<td><a href="' . get_permalink( $subs->course_id ) . '">' . get_the_title( $subs->course_id ) . '<br /></a><small>' . date_i18n( get_option( 'date_format' ), strtotime( $subs->subscription_date ) ) . '</small></td>';
            } else {
				//$content .= '<td><a href="' . get_permalink( $subs->course_id ) . '">' . get_the_title( $subs->course_id ) . '<br /></a><small>' . date_i18n( get_option( 'date_format' ), strtotime( $subs->subscription_date ) ) . '</small></td>';
				$content .= '<td><strong>' . get_the_title( $subs->course_id ) . '</strong><br /><small>' . date_i18n( get_option( 'date_format' ), strtotime( $subs->subscription_date ) ) . '</small></td>';
            }

            $content .= '<td>' . get_the_title( $subs->tier_id ) . $status . '</td>';

            $content .= '<td>';
            $is_automatic = get_post_meta(  $subs->tier_id, PPY_PREFIX . 'payment_automatic', true );

            if ( $subs->payment_status == 'cancelled' || $subs->payment_status != 'admin' ) {
                $invoices_html = '';

                if ( !$hide_invoices ) {
                    $invoices =  Poppyz_Invoice::get_subscription_invoices( $subs->id );
                } else {
                    $invoices = false;
                }
                $is_chargedback = false;
                $payment_data = false;
                if ( $invoices ) {
                    foreach ( $invoices as $invoice ) {
                        if ( isset( $invoice->invoice_id ) ) { //this is from the payments table and not from the custom post type invoice id
                            $invoice_id = $invoice->invoice_id;
                        } else {
                            $invoice_id = $invoice->ID;
                        }
                        $payment_data = $ppy_payment->get_payment_by_invoice( $invoice_id );
                        $title = get_the_title( $invoice_id);
                        if ( $payment_data ) {
                            $is_chargedback = ( $payment_data->status == 'chargedback')  ? ' - ' . __( 'Chargedback' , 'poppyz') : '';
                            $invoice_payments_made = $payment_data->number_of_payment;
                            if ($payment_data->invoice_type == 'moneybird') {
                                $title = $payment_data->invoice_id_other;
                            }
                        } else {
                            $invoice_payments_made = get_post_meta( $invoice_id, PPY_PREFIX . 'invoice_payments_made', true );
                            $is_chargedback = Poppyz_Invoice::has_credit_invoice( $subs->id, $invoice_payments_made ) ? ' - ' . __( 'Chargedback' , 'poppyz') : '';
                        }
                        $invoices_html .= get_the_date( get_option( 'date_format' ), $invoice_id ) . ' - <span class="green dashicons-before dashicons-saved"> ' . __( 'Paid', 'poppyz')  . ' </span> - <a href="' . site_url('?process_invoice=' .  $invoice_id) . '">' . $title . '</a>' . $is_chargedback . '<br /> ';
                        if ($payment_data && $payment_data->invoice_type == 'plugandpay') {
                            $invoices_html = __('Mailed via Plug&Pay', 'poppyz');
                        }
                    }
                }
            }

            $cancel_button_is_active = "";
            if ( $subs->payment_type == 'automatic' || $subs->payment_type == '' ) {
                $is_automatic = true;
            }
            if ( $subs->payment_type == 'manual' ) {
                $is_automatic = false;
            }
            if ( $subs->payment_status == 'cancelled'  && $tier_status == "publish"  ) {
                $due_date_status = $invoices_html.'<strong class="red">' . __( 'Cancelled' , 'poppyz') . '</strong>';
                $cancel_button_is_active = "disabled";
            } elseif ( $subs->payment_status == 'admin' ) {
                $content .= '<span class="green dashicons-before dashicons-saved">' . __( 'Subscribed by the admin' , 'poppyz') . '</span><br />';
            } else {
                $due_date_status = '';
                if ( !$hide_invoices ) {
                    $due_date_status .=  $invoices_html;
                }
                if ( !$is_automatic ) {
                    if ( $is_chargedback ) {
                        $due_date_status .= '<form method="post" action="' . Poppyz_Core::get_tier_purchase_url( $subs->tier_id ) .'?buy">';
                    } else {
                        $due_date_status .= $due_date_string . ' - <form method="post" action="' . Poppyz_Core::get_tier_purchase_url( $subs->tier_id ) .'?buy">';
                    }

                    //$due_date_status .= '<form method="post" action="' . Poppyz_Core::get_tier_purchase_url( $subs->tier_id ) .'?buy">';
                    $due_date_status .= wp_nonce_field( PPY_PREFIX . 'process_payment', PPY_PREFIX . 'pay_nonce', false );
                    $due_date_status .= '<input type="hidden" name="pay-now" value="1" >';
                    if ( $payment_data ) {
                        //$due_date_status .= $payment_data->number_of_payment .'<br />';
                        $due_date_status .= '<input type="hidden" name="' . PPY_PREFIX . 'payment_number' . '" value="' . $payment_data->number_of_payment . '" >';
                    }
                    $due_date_status .= '<input type="hidden" name="' . PPY_PREFIX . 'subs_id' . '" value="' . $subs->id  . '" >';
                    $due_date_status .= '<br /><br /><input  class="manual-pay-btn et_pb_button" type="submit" value="' . __( 'Pay', 'poppyz' ) .  '" name="' . PPY_PREFIX . 'purchase' . '" >';
                    $due_date_status .= '</form>' ;
                    //$due_date_status .= ' - <a href="' . Poppyz_Core::get_tier_purchase_url( $subs->tier_id ) . '?buy&sub_id=' . $subs->id .'">' . __( 'Pay' ) . '</a>';
                } elseif ( $tier_status == "publish") {
                    $due_date_status .= $due_date_string . ' - <span class="gray dashicons-before dashicons-clock">' . __( 'Remaining payment' ) . '</span>';
                }
				$tier_purchase_expiry = get_post_meta( $subs->tier_id, PPY_PREFIX . 'allow_membership_cancellation', true );
                if( $cancel_button_is_active != "disabled" && $tier_status == "publish" && $tier_purchase_expiry == "on") {
                    $due_date_status .= '<form action="'.get_permalink().'" id="'.$subs->id.'-cancel-form"  class="cancel-membership-form" method="post">';
                    $due_date_status .= '<input type="hidden" name="cancel_membership" value="1">';
                    $due_date_status .= '<button name="subs_id" type="submit" value="'.$subs->id.'" class="cancel-membership" '.$cancel_button_is_active.'>' . __( 'Stop' , 'poppyz')  . '</button>';
                    $due_date_status .= '</form>';
                }
                //$due_date_status .= ' - <a href="?cancel_pending=' . $subs->id . '">' . __( 'Cancel' ) . '</a>';
            }
            $content .=  $due_date_status . '<br />';

            // $content .= $due_date .  $overdue . ;
            $content .= '</td>';
            
            $content .= "</tr>";
        }
        $content .= '</table>';
        $content .= '</div>';
        $content .= '</div>';
    }

    if ( $pending_subscriptions ) {
        $content .= '<div class="subscription">';
        $content .= '<h3>' . __( 'Pending subscriptions', 'poppyz' ) . '</h3>';
        $content .= '<div class="table-wrapper">';
        $content .= '<table class="subscription-list ppy-table">';
        $content .= '<tr>';
        $content .= '<th>' . __( 'Course' , 'poppyz') . '</th>';
        $content .= '<th>' . __( 'Tier', 'poppyz' ) . '</th>';
        $content .= '<th>' . __( 'Payment Method' , 'poppyz') . '</th>';
        $content .= '<th>' . __( 'Actions' , 'poppyz') . '</th>';
        $content .= '</tr>';
        foreach ( $pending_subscriptions as $subs ) {

            $payment = $ppy_sub->get_payment_method( $subs->tier_id );
			$tier_status = get_post_status($subs->tier_id);

            $num_payments = $subs->payments_made;

            $content .= "<tr>";
            $content .= '<td><a href="' . get_permalink( $subs->course_id ) . '">' . get_the_title( $subs->course_id ) . '<br /></a><small>' . date_i18n( get_option( 'date_format' ), strtotime( $subs->subscription_date ) ) . '</small></td>';
            $content .= '<td>' . get_the_title( $subs->tier_id ) . '</td>';

            if ( $payment['method'] != 'plan' ) {
                $content .= '<td>' . __( 'Standard' , 'poppyz') . '</td>';
                $content .= '<td><a class="btn" href="' . Poppyz_Core::get_tier_purchase_url( $subs->tier_id ) . '?buy">' . __( 'Pay' , 'poppyz') . '</a>';
                $content .= ' / <a href="?cancel_pending=' . $subs->id . '">' . __( 'Delete' , 'poppyz') . '</a></td>';
            } else {
                $content .= '<td>' . __( 'Payment Plan' , 'poppyz') . '</td>';
                $content .= '<td>';
                if ( $num_payments == 0 ) {
                    $content .= '<a href="' . Poppyz_Core::get_tier_purchase_url( $subs->tier_id ) . '?buy">' . __( 'Make first payment' , 'poppyz') . '</a>';
                }
                $content .= '</td>';
            }


            $content .= "</tr>";
        }
        $content .= '</table>';
        $content .= '</div>';
        $content .= '</div>';
    }

    $content = apply_filters( 'ppy_account_page_item', $content );

}
$content .= '</div>';

$text_user_email =  Poppyz_Core::get_option( 'reg_user_email' );
$text_first_name =  Poppyz_Core::get_option( 'reg_first_name' );
$text_last_name =  Poppyz_Core::get_option( 'reg_last_name' );

$email_text = $text_user_email ? $text_user_email : __('Email', 'poppyz');
$first_name_text = $text_first_name ? $text_first_name : __('First Name', 'poppyz');
$last_name_text = $text_last_name ? $text_last_name : __('Last Name', 'poppyz');

$content .= '<div id="profile-details">';
$content .= '<h3> ' . __('Your credentials', 'poppyz') .  '<a href="' . get_permalink( Poppyz_Core::get_option( 'edit-profile_page' ) ) . '"> ' . __('Edit', 'poppyz') . '</a></h3>';
$content .= '<div id="profile-fields">';
$content .= '<div>
                        <fieldset><label>' . $email_text . ':</label>
                        <span>' . $current_user->user_email . '</span></fieldset>
                        <fieldset><label>' . $first_name_text . ':</label>
                        <span>' . $current_user->first_name . '</span></fieldset>
                        <fieldset><label>' . $last_name_text . ':</label>
                        <span>' . $current_user->last_name . '</span></fieldset>
                    </div>
                    <div>';

foreach ( $user_fields as $field => $value ) {
    if ($value == 'vat_out_of_scope' || $value == 'vatshifted' ) {
        if ( get_user_meta( $user_id, PPY_PREFIX . $value, true ) ) {
            //$content .= '<p><label>VAT out of scope activated (just activate this when your company is registered outside the EU).</label></p> ';
        }
        continue;
    }
    $field_value =  get_user_meta( $user_id, PPY_PREFIX . $value, true ) ? get_user_meta( $user_id, PPY_PREFIX . $value, true ) : '-';
    $content .= '<fieldset><label>' . $field . ':</label>
                   <span>' .  $field_value  . '</span></fieldset>';
}
$content .= '</div>';
$content .= '</div>';
$content .= '</div>';

echo $content;
?>
<script>
    $(document).ready(function(){
        $(".cancel-membership-form").submit(function(e){
            var form_id = event.target.id;
            var formData = {
                subs_id: form_id.replace('-cancel-form',''),
                cancel_membership: $("[name='cancel_membership']").val(),
                };
            
            if (!confirm("Do you want to cancel this membership?")){
                 return false;
            } 
    
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo get_permalink();?>",
                data: formData,
                success: function() {
                    location.reload();
                }
            });
            
        });

        var ids = $('.account_page_plan_invoice').map(function() {
            var numAttribute = $(this).attr('num');
            var dateStatusHeight = $("#date_status_"+numAttribute).height();
            $('#invoice_'+numAttribute).css('height', dateStatusHeight+'px');
            $('#date_status_'+numAttribute).css('height', dateStatusHeight+'px');
            return $(this).attr('num');
        });
        console.log(ids);
        
    });
</script>