<?php
global $ppy_lang;
$output = "";
$message = "";
if (! empty($_GET['action']) )
{
    if ( 'failed' == $_GET['action'] )
        $message = __( 'There was a problem with your username or password.','poppyz');
    elseif ( 'loggedout' == $_GET['action'] )
        $message = __( 'You are now logged out.' ,'poppyz');
    elseif ( 'recovered' == $_GET['action'] )
        $message = __( 'Check your e-mail for the confirmation link.' ,'poppyz');
}

$redirect_page = Poppyz_Core::get_option( 'redirect-login_page' );
$account_page = Poppyz_Core::get_option( 'account_page' );
if  ( !empty( $redirect_page ) && $redirect_page != 'http://' ) {
    $default_redirect =  $redirect_page;
} elseif ( !empty( $account_page ) ) {
    $default_redirect = get_permalink( $account_page );
} else {
    $default_redirect = site_url();
}

$redirect = ( isset( $_GET['redirect_to'] ) ) ? $_GET['redirect_to'] : $default_redirect;

if ( $message ) $output .= '<div class="message"><p style="text-align: center">'. $message .'</p></div>';
$output .= wp_login_form('echo=0&redirect=' .  $redirect );
$output .= '<a href="'. wp_lostpassword_url( add_query_arg('action', 'recovered', get_permalink()) ) .'" title="Recover Lost Password">' . __( 'Lost your password?' ) . '</a>';
$content .= $output;
echo $content;