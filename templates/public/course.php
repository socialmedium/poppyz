<?php
global $ppy_lang;

if ( !$user_id ) {
    $content = '<div id="et-boc" class="et-boc"><div class="et_pb_row et_pb_row_1 no-access-message">' . sprintf( __( 'You must first login to view courses. Click <a href="%s">here</a> to login. <br />','poppyz' ),  wp_login_url( get_permalink() ) ) . '</div></div>';
} else {
    wp_reset_postdata();
    $course_id = get_the_ID();
    $ppy_subs = new Poppyz_Subscription();
    $access = $ppy_subs->is_subscribed( $course_id, $user_id );
    if ( is_wp_error ( $access ) ) {
        $content = '<div id="et-boc" class="et-boc"><div class="et_pb_row et_pb_row_1 no-access-message"> ' . $access->get_error_message() . '</div></div>';
    } else {
        $content .=  Poppyz_Core::progress_list( $course_id );
        $content .= Poppyz_Core::return_action( 'ppy_view_content', $course_id );
    }
    wp_reset_query();
}
echo $content;