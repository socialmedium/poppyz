<?php global $ppy_lang; ?>
<table class="form-table">

    <tr>
        <th>
            <label for="ty-email-from"><?php echo __( 'Sender Email:','poppyz'); ?></label>
        </th>
        <td>
            <?php echo Poppyz_Core::form_settings_helper( 'text', $email_type . '_from', $options  ); ?>
            <p class="description full">
                <?php echo __( "We suggest leaving this field empty to lessen the chances of your emails going to the spam folder. If you really wish to change this we recommend using a plugin like <strong>WP Mail SMTP</strong>." ,'poppyz'); ?>
            </p>
        </td>
    </tr>
    <tr>
        <th>
            <label for="ty-email-form-name"><?php echo __( 'Sender Name:' ,'poppyz'); ?></label>
        </th>
        <td>
            <?php echo Poppyz_Core::form_settings_helper( 'text', $email_type . '_from_name', $options  ); ?>
        </td>
    </tr>
    <tr>
        <th>
            <label for="ty-email-subject"><?php echo __( 'Email Subject' ,'poppyz');?></label>
        </th>
        <td>
            <?php echo Poppyz_Core::form_settings_helper( 'text', $email_type . '_subject', $options  ); ?>
        </td>
    </tr>
    <?php
    $settings = array(
        'textarea_rows' => 20,
        'editor_height' => 425,
        'tabindex' => 1,
        'textarea_name' => 'ppy_settings[' . $email_type . '_message]'
    );
    ?>
    <tr>
        <th>
            <label for="ty-email-message"><?php echo __( 'Email Message:','poppyz'); ?></label>
        </th>
        <td>
            <?php wp_editor( $options[ $email_type . '_message'] , $email_type . '_message', $settings); ?>
            <p class="description full"><?php echo $content_description ?? __( 'Shortcodes: [first-name], [last-name], [account-link], [password-link], [user-email], [tier], [tier-link], [course], [course-link], [amount], [invoice-link], [customer-id]' ,'poppyz'); ?></p>
        </td>
    </tr>
    <tr>
        <th>
            <label for="<?php echo $email_type; ?>_disabled"><?php echo __( 'Disable notification?','poppyz');?></label>
        </th>
        <td>
            <?php

                if (isset($disabled) && $disabled === 'on') {
                    $default_disabled = 'on';
                } else {
                    $default_disabled = 'off';
                }
            ?>
            <?php echo Poppyz_Core::form_settings_helper( 'checkbox', $email_type . '_disabled', $options, $default_disabled, null,
                $notification_description ?? null, '',  $tooltip ?? null ); ?>
        </td>
    </tr>

</table>