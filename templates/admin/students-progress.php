<?php
global $ppy_lang;
$subs = new Poppyz_Subscription();

?>
<h2><?php echo __( 'Students Progress','poppyz'); ?></h2>

<div class="poppyz-notice notice-info dashicons-before">
    <?php if (empty($progress))
        echo __('Note: This feature will only be available if the progress button is activated. More info can be found <a href="https://poppyz.nl/documentation/progress-list/">here</a>', 'poppyz'); ?>
</div>

<table class="widefat " id="students-list">
    <thead>
    <tr>
        <th><?php echo __( 'Name' , 'poppyz'); ?></th>
        <th><?php echo __( 'Progress' ,'poppyz'); ?></th>
        <th><?php echo __( 'Total' ,'poppyz'); ?></th>
    </tr>
    </thead>

    <tbody>
    <?php
        $students = Poppyz_Subscription::get_users_with_subscription();
        foreach ( $students as $student ) {
            $user = get_userdata($student);
            $name = "<i>" . __( 'User Deleted'  ,'poppyz') . "</i>";

            if ( $user ) {
                $name = $user->first_name . ' ' . $user->last_name . ' (' . $user->user_login  .')';
            }
            if (empty($user->ID)) { continue; }
            $progress = '';
            $lessons = Poppyz_Core::get_lessons_read_with_course( $user->ID );
            $total = count($lessons) ;
            if ($lessons) {
                foreach ($lessons as $course => $lessons ) {
                    $progress .= '<table>';
                    $progress .= '<tr>';
                    $progress .= '<td>';
                    $progress .= '<strong>' . get_the_title($course) . '</strong>';
                    foreach ( $lessons as $lesson ) {
                        $progress .= '<div>' . get_the_title($lesson) . '</div>';
                    }
                    $progress .= '</td>';
                    $progress .= '</tr>';
                    $progress .= '</table>';
                }
            }
            ?>
            <tr>
                <td class="name">
                    <?php echo $name; ?>
                </td>
                <td>
                    <?php echo $progress; ?>
                </td>
                <td>
                    <?php echo $total; ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<script>
    jQuery(document).ready(function($){
        $('#students-list').dataTable({
            searching: true,
            order: [],
            deferRender:    true,
            scroller:       true,
            "oLanguage": {
                "sSearch": "",
            },
            language: {
                searchPlaceholder: '<?php _e("Search"); ?>'
            }
        });
    });
</script>