<div class="wrap">
    <h2><?php echo $title;  ?></h2>
    <form id="ppy-coupon-update" method="post" action="">
        <?php echo  $alert; ?>
     
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-name"><?php echo __( 'Name','poppyz' ); ?></label>
                </th>
                <td>
                    <input name="name" id="ppy-name" type="text" value="<?php echo esc_attr( stripslashes( $name ) ); ?>" style="width: 300px;"/>
                    <p class="description"><?php echo __( 'The name of this coupon','poppyz' ); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-code"><?php echo __( 'Code','poppyz' ); ?></label>
                </th>
                <td>
                    <input type="text" id="ppy-code" name="code" value="<?php echo $code ; ?>" style="width: 300px;"/>
                    <p class="description"><?php echo __( 'Enter a code for this coupon, for example: LESS10','poppyz' ); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-type"><?php echo __( 'Type' ,'poppyz' ); ?></label>
                </th>
                <td>
                    <select id="ppy-type" name="type">
                        <option value="flat" <?php selected( 'flat', $type ) ?>><?php echo __( 'Flat Rate' ,'poppyz' ); ?></option>
                        <option value="percentage" <?php selected( 'percentage', $type ) ?>><?php echo __( 'Percentage' ,'poppyz' ); ?></option>
                    </select>
                    <p class="description"><?php echo __( 'The type of discount applied' ,'poppyz' ); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-amount"><?php echo __( 'Amount','poppyz' ); ?></label>
                </th>
                <td>
					<?php $amount = (empty($amount)) ? 0 : $amount;?>
					<?php echo Poppyz_Core::currency_symbol(); ?> <input type="text" id="ppy-amount" name="amount" value="<?php echo Poppyz_Core::format_price( $amount, '' ); ?>" style="width: 283px;"/>
                    <p class="description"><?php echo __( 'Amount of discount','poppyz' ); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-start-date"><?php echo __( 'Start Date' ,'poppyz' ); ?></label>
                </th>
                <td>
                    <input type="text" id="ppy-start-date" name="start_date" value="<?php echo $start_date ; ?>" style="width: 300px;" class="date-picker" />
                    <p class="description"><?php echo __( 'The date when the coupon can be used' ,'poppyz' ); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-end-date"><?php echo __( 'End Date' ,'poppyz' ); ?></label>
                </th>
                <td>
                    <input type="text" id="ppy-end-date" name="end_date" value="<?php echo $end_date ; ?>" style="width: 300px; "  class="date-picker" />
                    <p class="description"><?php echo __( 'The date when the coupon expires','poppyz' ); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-usage-limit"><?php echo __( 'Limit' ,'poppyz' ); ?></label>
                </th>
                <td>
                    <input type="number" id="ppy-usage-limit" name="usage_limit" value="<?php echo $usage_limit ; ?>" style="width: 300px;"  />
                    <p class="description"><?php echo __( 'Number of uses of the coupon.' ,'poppyz' ); ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-tier"><?php echo __( 'Tier' ,'poppyz' ); ?></label>
                </th>
                <td>
		            <?php $tiers = Poppyz_Core::get_all_tiers(); ?>
		            <?php if ( $tiers->have_posts() ) : ?>
                        <select class="poppyz-select2" name="tier_ids[]" id="ppy-tier" multiple="multiple">
			            <?php while ( $tiers->have_posts() ) : $tiers->the_post(); ?>
                            <?php
                                $checked = is_array($tier_ids) && in_array( get_the_ID(), $tier_ids );
                                $course_name =  get_the_title( Poppyz_Core::get_course_by_tier( get_the_ID() ) );
                            ?>
                            <option <?php if ($checked) echo 'selected=selected'; ?>  value="<?php echo get_the_ID(); ?>" id="<?php echo 'tier_' . get_the_ID(); ?>"><?php echo get_the_title(); ?> - <?php echo $course_name; ?></option><br />
			            <?php endwhile; ?>
                        </select>
		            <?php endif; ?>
		            <?php wp_reset_postdata(); ?>
                    <p class="description"><?php echo __( 'Select tiers here to only enable this coupon on particular tiers.','poppyz' ); ?></p>
                </td>
            </tr>
            <?php do_action( 'ppy_coupon_apply_to_tiers', $tier_ids ); ?>
            </tbody>
        </table>

        <p class="submit">
            <?php wp_nonce_field('ppy-nonce'); ?>
            <input type="hidden" name="ppy-action" value="update-coupon" />
            <input type="hidden" name="id" value="<?php echo $id; ?>" />
            <input type="submit" value="<?php echo __( 'Update','poppyz' ); ?>" class="button-primary"/>
        </p>
    </form>
    <script>
        jQuery(document).ready(function($) {
            $('.date-picker').datepicker({dateFormat: 'dd-mm-yy'});
            $('.poppyz-select2').select2();
        });
    </script>
</div>