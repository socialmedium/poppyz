<?php
global $ppy_lang;
$ajax_nonce = wp_create_nonce(  PPY_PREFIX . "ajax_mc" );
//flush the rewrite rules
set_site_transient( PPY_PREFIX . 'settings_visited', true );

?>
<img class="ppy-loading" src="<?php echo admin_url() ?>/images/spinner-2x.gif " />
<div id="ppy-settings-page" class="wrap white ppy-one-column">
<!--    <h2>--><?php //echo __( 'Settings' ,'poppyz'); ?><!--</h2>-->
    <?php
    $notification = get_settings_errors('ppy_settings');
    if (empty($notification) &&  isset( $_GET['settings-updated'] ) ) {
        echo "<div class='notice-success poppyz-notice dashicons-before'>" . __( 'Settings saved.' ,'poppyz') .  "</div>";
    } else {
        settings_errors('ppy_settings');
    }
    ?>

    <form method="post" action="options.php" autocomplete="false" id="ppy-form-settings" class="main-box poppyz-form" >
        <?php
        settings_fields( 'ppy_settings' );
        do_settings_sections( 'ppy_settings' );
        $options = get_option( 'ppy_settings' );
        $options['reminder_email_message'] = (isset($options['reminder_email_message'])) ? $options['reminder_email_message'] : '';
        $options['overdue_email_message'] = (isset($options['overdue_email_message'])) ? $options['overdue_email_message'] : '';
        $options['pending_email_message'] = (isset($options['pending_email_message'])) ? $options['pending_email_message'] : '';
        $options['invoice_email_message'] = (isset($options['invoice_email_message'])) ? $options['invoice_email_message'] : '';
		$options['scheduled_donation_notification_message'] = (isset($options['scheduled_donation_notification_message'])) ? $options['scheduled_donation_notification_message'] : '';
        $options['membership_email_message'] = (isset($options['membership_email_message'])) ? $options['membership_email_message'] : '';
        $options['default_country'] = (isset($options['default_country'])) ? $options['default_country'] : 'NL'; //user's country
        $options['country'] = (isset($options['country'])) ? $options['country'] : 'NL'; //company's country
        $options['insufficient_funds_email_message'] = !empty($options['insufficient_funds_email_message']) ? $options['insufficient_funds_email_message'] : __( 'Your payment for the tier [tier] which belongs to course [course] has failed due to insufficient funds. Please pay it manually by going to your account page [account-link] and make sure you have sufficient funds for your next SEPA direct debit payment.','poppyz');
        $options['failed_payment_email_message'] = !empty($options['failed_payment_email_message']) ? $options['failed_payment_email_message'] : __( 'Your payment for the tier [tier] which belongs to course [course] has failed .The SEPA direct debit has stopped, you can do the remaining payments manually, go to your account page for the overview.' ,'poppyz');
        $options['failed_payment_admin_email_message'] = !empty($options['failed_payment_admin_email_message']) ? $options['failed_payment_admin_email_message'] : __( 'A SEPA direct debit has failed, it concerns [tier] belonging to course [course]. Your customer must now make the remaining payments manually on his/her account page where the overview of payments can be viewed. Your customer will receive an email of this event as well. Please contact your customer: [first-name] [last-name] - [user-email]' ,'poppyz');

        ?>
        <nav class="tabs">
            <ul class="ppy-vertical-nav-tabs">
                <li><a href="#plugin-pages" class="nav-tab dashicons-text-page"><?php echo __('Pages' ,'poppyz'); ?></a></li>
                <li class="has-tabs ">
                    <a href="#email-settings" class="nav-tab dashicons-email-alt"><?php echo __('Emails' ,'poppyz'); ?></a>
                </li>
                <li class="sub-tab email-settings"><a href="#thank-you-mail" class="nav-tab"><?php echo __('Thank You' ,'poppyz'); ?></a></li>
                <li class="sub-tab email-settings"><a href="#invoice-mail" class="nav-tab"><?php echo __( 'Invoice' ,'poppyz'); ?></a></li>
                <li class="sub-tab email-settings"><a href="#scheduled-donation-mail" class="nav-tab"><?php echo __( 'Schedule Donation Email' ,'poppyz'); ?></a></li>
                <li class="sub-tab email-settings"><a href="#payment-reminder-mail" class="nav-tab"><?php echo __( 'Payment Reminder' ,'poppyz'); ?></a></li>
                <li class="sub-tab email-settings"><a href="#payment-overdue-mail" class="nav-tab"><?php echo __( 'Payment Overdue' ,'poppyz'); ?></a></li>
                <li class="sub-tab email-settings"><a href="#payment-pending-mail" class="nav-tab"><?php echo __( 'Payment Pending','poppyz'); ?></a>
                <li class="sub-tab email-settings"><a href="#misc-mail" class="nav-tab"><?php echo __( 'Failed Payments' ,'poppyz'); ?></a></li>

                <li class="has-tabs"><a href="#text-settings" class="nav-tab dashicons-text"><?php echo __('Default Texts' ,'poppyz');?></a></li>

                <li class="sub-tab text-settings"><a href="#purchase-text" class="nav-tab"><?php echo __( 'Payments' ,'poppyz'); ?></a></li>
                <li class="sub-tab text-settings"><a href="#notifications-text" class="nav-tab"><?php echo __( 'Notifications'  ,'poppyz'); ?></a></li>
                <li class="sub-tab text-settings"><a href="#terms-text" class="nav-tab"><?php echo __( 'AVG'  ,'poppyz'); ?></a></li>

                <li><a href="#register-form" class="nav-tab dashicons-id-alt"><?php echo __('Register Form' ,'poppyz'); ?></a></li>
                <li><a href="#payment-options" class="nav-tab dashicons-money"><?php echo __('Payment' ,'poppyz'); ?></a></li>
                <li><a href="#invoice-options" class="nav-tab dashicons-analytics"><?php echo __('Invoice' ,'poppyz'); ?></a></li>

                <li class="has-tabs"><a href="#integration-options" class="nav-tab dashicons-networking"><?php echo __('Integration' ,'poppyz'); ?></a></li>
                <li class="sub-tab integration-options"><a href="#newsletters" class="nav-tab"><?php echo __( 'Newsletters'  ,'poppyz'); ?></a></li>
                <li class="sub-tab integration-options"><a href="#payments" class="nav-tab"><?php echo __( 'Payments'  ,'poppyz'); ?></a></li>
                <li class="sub-tab integration-options"><a href="#analytics" class="nav-tab"><?php echo __( 'Analytics'  ,'poppyz'); ?></a></li>

                <li><a href="#activation" class="nav-tab dashicons-info"><?php echo __('Poppyz' ,'poppyz');?></a></li>
                <li><a href="#extras" class="nav-tab dashicons-admin-generic"><?php echo __('Extras' ,'poppyz'); ?></a></li>
                <?php do_action( 'ppy_settings_tab' ); ?>
            </ul>
        </nav>
        <div class="nav-content" id="plugin-pages">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php _e('Pages'); ?></h2>
                </div>
            </div>
            <table class="form-table">
                <tbody>
                <tr>
                    <th>
                        <label for="login-page"><?php echo __( 'Login Page'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <select class="page-select" name="ppy_settings[login_page]" id="login-page">
                            <?php
                            $pages = get_pages();
                            echo '<option value="-1">' . __( 'Select a page...'  ,'poppyz') .  '</option>';
                            foreach ( $pages as $page ) {
                                $option = '<option value="' . $page->ID . '" ' . selected( $options['login_page'],  $page->ID, false  ) . '>';
                                $option .= $page->post_title;
                                $option .= '</option>';
                                echo $option;
                            }
                            ?>
                        </select>
                    </td>

                </tr>

                <tr>
                    <th>
                        <label for="edit-profile-page"><?php echo __( 'Edit Profile Page' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <select class="page-select" name="ppy_settings[edit-profile_page]" id="edit-profile-page">
                            <?php
                            $pages = get_pages();
                            echo '<option value="-1">' . __( 'Select a page...' ,'poppyz') .  '</option>';
                            foreach ( $pages as $page ) {
                                $option = '<option value="' . $page->ID . '" ' . selected( $options['edit-profile_page'],  $page->ID, false  ) . '>';
                                $option .= $page->post_title;
                                $option .= '</option>';
                                echo $option;
                            }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="edit-profile-page"><?php echo __( 'Account Page'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <select class="page-select" name="ppy_settings[account_page]" id="account-page">
                            <?php
                            $pages = get_pages();
                            echo '<option value="-1">' . __( 'Select a page...'  ,'poppyz').  '</option>';
                            foreach ( $pages as $page ) {
                                $option = '<option value="' . $page->ID . '" ' . selected( $options['account_page'],  $page->ID, false  ) . '>';
                                $option .= $page->post_title;
                                $option .= '</option>';
                                echo $option;
                            }
                            ?>
                        </select>
                    </td>

                </tr>
                <tr>
                    <th>
                        <label for="purchase-page"><?php echo __( 'Purchase Page' ,'poppyz');?></label>
                    </th>
                    <td>
                        <select class="page-select" name="ppy_settings[purchase_page]" id="purchase-page">
                            <?php
                            $pages = get_pages();
                            echo '<option value="-1">' . __( 'Select a page...'  ,'poppyz').  '</option>';
                            foreach ( $pages as $page ) {
                                $option = '<option value="' . $page->ID . '" ' . selected( $options['purchase_page'],  $page->ID, false  ) . '>';
                                $option .= $page->post_title;
                                $option .= '</option>';
                                echo $option;
                            }
                            ?>
                        </select>
                        <!--<p>
                            <input type="checkbox" name="ppy_settings[disable_auto_login]" id="disable-auto-login" <?php /*if ( isset( $options['disable_auto_login'] ) ) checked(  $options['disable_auto_login'], "on" ); */?> />
                            <label for="disable-auto-login"><?php /*echo __( 'Disable auto login after registration on the purchase page?' ); */?></label>
                        </p>-->
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="return-page"><?php echo __( 'Return Page' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <select required class="page-select" name="ppy_settings[return_page]" id="return-page">
                            <?php
                            $pages = get_pages();
                            echo '<option value="">' . __( 'Select a page...' ,'poppyz') .  '</option>';
                            foreach ( $pages as $page ) {
                                $option = '<option value="' . $page->ID . '" ' . selected( $options['return_page'],  $page->ID, false ) . '>';
                                $option .= $page->post_title;
                                $option .= '</option>';
                                echo $option;
                            }
                            ?>
                        </select>
                        <span class="description"><?php echo __( 'This page will be used after the payment process.' ,'poppyz'); ?></span>
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="redirect-login-page"><?php echo __( 'Redirect After Login'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'redirect-login_page', $options ); ?>
                        <span class="description"><?php echo __( 'Enter a URL where you want the user to get redirect to after login. If left blank, the user will be redirected to the Account page' ,'poppyz'); ?></span>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="redirect-logout-page"><?php echo __( 'Redirect After Logout', 'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'redirect-logout_page', $options ); ?>
                        <span class="description"><?php echo __( 'Enter a URL where you want the user to get redirect to after logging out. If left blank, the user will be redirected to the homepage' ,'poppyz'); ?></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <?php
            $client_name = (isset($options['client_name']) && $options['client_name'] != '') ? $options['client_name'] : '';
        ?>
        <div id="email-settings"></div>

        <div id="thank-you-mail">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'New User Email'  ,'poppyz'); ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="new_user_notification_disabled"><?php echo __( 'Disable New User notification?'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'new_user_notification_disabled', $options, 'off', null,
                            __( 'This will prevent WordPress\' new user email notification from being sent to the customer. 
                            Only use this if you have a way to send the password link to the user like using the [password-link] shortcode in the Thank You email below.' ,'poppyz'), '',  __( 'Keep in mind that the Thank You email only sends after payment. So the password link won\'t be sent to the user if they don\'t complete the payment.' ,'poppyz') ?? null ); ?>
                    </td>
                </tr>
            </table>
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Thank You'  ,'poppyz');  ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <?php
                $template_args = [
                    'options' =>  $options,
                    'email_type' =>  'ty_email',
                ];
            ?>
            <?php echo Poppyz_Template::get_template('admin/email-settings.php', $template_args );  ?>
            <table class="form-table">
                <tr>
                    <th><label for="password_link_text"><?php echo __( 'Password link text'  ,'poppyz'); ?></label></th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'password_link_text', $options ); ?>
                        <p class="description full">
                            <?php echo __( "It's recommended to display the raw password reset URL in your mail because some mail clients can't process HTML. But if you want it to be a link instead use this field. This will display the text with a link to the password reset page." ,'poppyz'); ?>
                        </p>
                    </td>
                </tr>
            </table>
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'New Purchase Notification for Admin' ,'poppyz'); ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <?php
            $template_args = [
                'options' =>  $options,
                'email_type' =>  'new_purchase_email',
            ];
            ?>
            <?php echo Poppyz_Template::get_template('admin/email-settings.php', $template_args );  ?>

        </div>

        <div id="invoice-mail">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Invoice'  ,'poppyz');  ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <?php
            $template_args = [
                'options' =>  $options,
                'email_type' =>  'invoice_email',
            ];
            ?>
            <?php echo Poppyz_Template::get_template('admin/email-settings.php', $template_args );  ?>
        </div>

        <div id="scheduled-donation-mail">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Scheduled Donation'  ,'poppyz');  ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
			<?php
			$template_args = [
				'options' =>  $options,
				'email_type' =>  'scheduled_donation_notification_email',
				'content_description' => __( 'Shortcodes: [due-date], [first-name], [last-name], [account-link], [password-link], [user-email], [tier], [tier-link], [course], [course-link], [amount], [tier-purchase-url]'  ,'poppyz')
			];
			?>
			<?php echo Poppyz_Template::get_template('admin/email-settings.php', $template_args );  ?>
        </div>

        <div id="payment-reminder-mail">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'SEPA collection notification'  ,'poppyz');   ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <?php
            $template_args = [
                'options' =>  $options,
                'email_type' =>  'reminder_email',
                'content_description' => __( 'Shortcodes: [due-date], [first-name], [last-name], [account-link], [password-link], [user-email], [tier], [tier-link], [course], [course-link], [amount], [customer-id]'  ,'poppyz')
                    . '<br />' . __( 'Sample usage: Hi [first-name], On [due-date], the costs of [tier] will be collected by means of SEPA direct debit. The amount that will be debited is [amount]. Your unique mandate reference has been assigned by us [customer-id]' ,'poppyz')
            ];

            echo Poppyz_Template::get_template('admin/email-settings.php', $template_args );
            ?>
            <h2><?php echo __( 'Payment Reminder - Manual' ,'poppyz');  ?></h2>
            <?php
            $template_args = [
                'options' =>  $options,
                'email_type' =>  'reminder_manual_email',
                'content_description' => __( 'Shortcodes: [due-date], [first-name], [last-name], [account-link], [password-link], [user-email], [tier], [tier-link], [course], [course-link], [amount]'  ,'poppyz')
            ];

            echo Poppyz_Template::get_template('admin/email-settings.php', $template_args );
            ?>
        </div>

        <div id="payment-overdue-mail">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Payment Overdue'  ,'poppyz');  ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <?php
            $template_args = [
                'options' =>  $options,
                'email_type' =>  'overdue_email',
                'content_description' => __( 'Shortcodes: [due-date], [first-name], [last-name], [account-link], [password-link], [user-email], [tier], [tier-link], [course], [course-link], [amount]'  ,'poppyz')
            ];

            echo Poppyz_Template::get_template('admin/email-settings.php', $template_args );
            ?>
        </div>

        <div id="payment-pending-mail">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Payment Pending' ,'poppyz'); ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <input type="hidden" class="yes_no_button" name="ppy_settings[pending_email_disabled]" value="off" />
            <?php
            $template_args = [
                'options' =>  $options,
                'email_type' =>  'pending_email',
                'content_description' => __( 'Shortcodes: [due-date], [first-name], [last-name], [account-link], [password-link], [user-email]'  ,'poppyz'),
                'disabled' => 'on',
            ];

            echo Poppyz_Template::get_template('admin/email-settings.php', $template_args );


            ?>
            <table class="form-table">
                <tr>
                    <th>
                        <label><?php echo __( 'Hours to wait before sending a reminder email. Default is 48 hours (2 days)' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'number', 'pending_mail_waiting_hours', $options, '48', 'pending-mail-waiting-hours' ); ?>
                    </td>
                </tr>
            </table>
        </div>

        <div id="misc-mail">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Failed Payment - Insufficient Funds'  ,'poppyz'); ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <?php
            $template_args = [
                'options' =>  $options,
                'email_type' =>  'insufficient_funds_email',
                'content_description' => __( 'Shortcodes: [first-name], [last-name], [account-link], [password-link], [user-email], [tier], [tier-link], [course], [course-link], [amount], [customer-id]'  ,'poppyz')
            ];

            echo Poppyz_Template::get_template('admin/email-settings.php', $template_args );
            ?>
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Failed Payment Email - Others'  ,'poppyz');  ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>

            <?php
            $template_args = [
                'options' =>  $options,
                'email_type' =>  'failed_payment_email',
                'content_description' => __( 'Shortcodes: [first-name], [last-name], [account-link], [password-link], [user-email], [tier], [tier-link], [course], [course-link], [amount], [customer-id]' )
            ];

            echo Poppyz_Template::get_template('admin/email-settings.php', $template_args );
            ?>


            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Failed Payment Email - Admin'  ,'poppyz');  ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <?php
            $template_args = [
                'options' =>  $options,
                'email_type' =>  'failed_payment_admin_email',
                'content_description' => __( 'Shortcodes: [first-name], [last-name], [account-link], [password-link], [user-email], [tier], [tier-link], [course], [course-link], [amount], [customer-id]'  ,'poppyz')
            ];

            echo Poppyz_Template::get_template('admin/email-settings.php', $template_args );
            ?>
        </div>

        <div id="text-settings"></div>
        <?php
        //set default if empty
        $purchase_page_content = isset($options['purchase_page_content']) ? $options['purchase_page_content'] : '';
        $purchase_button_text = isset($options['purchase_button_text']) ? $options['purchase_button_text'] : '';
        $free_purchase_button_text = isset($options['free_purchase_button_text']) ? $options['free_purchase_button_text'] : '';
        $purchase_page_content = isset($options['purchase_page_content']) ? $options['purchase_page_content'] : '';

        $registration_form_description = isset($options['registration_form_description']) ? $options['registration_form_description'] : '';
		$registration_form_extra_content = isset($options['registration_form_extra_content']) ? $options['registration_form_extra_content'] : '';
        $review_page_content = isset($options['review_page_content']) ? $options['review_page_content'] : '';
        $invoice_page_content = isset($options['invoice_page_content']) ? $options['invoice_page_content'] : '';
        $return_page_success_title = isset($options['return_page_success_title']) ? $options['return_page_success_title'] : '';
        $return_page_success_content = isset($options['return_page_success_content']) ? $options['return_page_success_content'] : '';
        $return_page_failed_content = isset($options['return_page_failed_content']) ? $options['return_page_failed_content'] : '';

        $course_not_accessible_text = isset($options['course_not_accessible_text']) ? $options['course_not_accessible_text'] : '';
        $tier_expired_text = isset($options['tier_expired_text']) ? $options['tier_expired_text'] : '';
		$tier_purchase_link_expired_text = isset($options['tier_purchase_link_expired_text']) ? $options['tier_purchase_link_expired_text'] : '';

        $lesson_not_yet_available_text = isset($options['lesson_not_yet_available_text']) ? $options['lesson_not_yet_available_text'] : '';
        $lesson_excluded_text = isset($options['lesson_excluded_text']) ? $options['lesson_excluded_text'] : '';
        $lesson_not_accessible_text = isset($options['lesson_not_accessible_text']) ? $options['lesson_not_accessible_text'] : '';

        $tier_limit_reached_text = isset($options['tier_limit_reached_text']) ? $options['tier_limit_reached_text'] : '';

        $terms_condition_title = isset($options['terms_condition_title']) ? $options['terms_condition_title'] : __( 'Terms and conditions'  ,'poppyz');
        $terms_condition = isset($options['terms_condition']) ? $options['terms_condition'] : __( 'If you order a tier from [company], you agree to the terms and conditions of [company].' ,'poppyz');

        //$sepa_text = !empty($options['sepa_text']) ? $options['sepa_text'] : __( '<strong>Naam: </strong>{Naame}<br /><strong>Adres: </strong>{Adres}<br /><strong>Postcode/woonplaats: </strong>{POSTCODE}<br /><strong>Land: </strong>{LAND}<br /><p>Na akkoord geeft u toestemming aan [company]:<ul><li>om doorlopend incasso-opdrachten te sturen naar uw bank om een bedrag van uw rekening af te schrijven en</li><li>uw bank om doorlopend een bedrag van uw rekening af te schrijven overeenkomstig de opdracht van [company].</li></ul></p><p>Als u het niet eens bent met deze afschrijving kunt u deze laten terugboeken. Neem hiervoor binnen acht weken na afschrijving contact op met uw bank. Vraag uw bank naar de voorwaarden.</p>'  ,'poppyz');
        $sepa_text = !empty($options['sepa_text']) ? $options['sepa_text'] : __( '<strong>Name: </strong> {Name} <br /> <strong> Address: </strong> {Address} <br /> <strong> Postal code / city: </strong> {POSTCODE} <br /> <strong> Country: </strong> {COUNTRY} <br /> <p> After approval, you give permission to {COMPANY}: <ul> <li> to send direct debit orders to your bank for an amount of your to debit your account and </li> <li> your bank to debit an amount from your account on an ongoing basis in accordance with the order of {COMPANY}. </li> </ul> </p> <p> If you do not agree with this charge, you can have it reversed. Please contact your bank within eight weeks of the debit. Please sk your bank for the conditions. </p>'  ,'poppyz');
        //$sepa_text_after = !empty($options['sepa_text_after']) ? $options['sepa_text_after'] : __( '<strong>Naam: </strong>{Naame}<br /><strong>Adres: </strong>{Adres}<br /><strong>Postcode/woonplaats: </strong>{POSTCODE}<br /><strong>Land: </strong>{LAND}<br /><p>Met je afgeronde betaling geef je toestemming aan [company]:<ul><li>om doorlopend incasso-opdrachten te sturen naar uw bank om een bedrag van uw rekening af te schrijven en</li><li>uw bank om doorlopend een bedrag van uw rekening af te schrijven overeenkomstig de opdracht van [company].</li></ul></p><p>Als u het niet eens bent met deze afschrijving kunt u deze laten terugboeken. Neem hiervoor binnen acht weken na afschrijving contact op met uw bank. Vraag uw bank naar de voorwaarden.</p>'  ,'poppyz');
        $sepa_text_after = !empty($options['sepa_text_after']) ? $options['sepa_text_after'] : __( '<strong>Name: </strong> {Name} <br /> <strong> Address: </strong> {Address} <br /> <strong> Postal code / city: </strong> {POSTCODE} <br /> <strong> Country: </strong> {COUNTRY} <br /> <p> With your completed payment you give permission to {COMPANY}: <ul> <li> to send direct debit orders to your bank for an amount debit from your account and </li> <li> your bank to continuously debit an amount from your account in accordance with the instruction of {COMPANY}. </li> </ul> </p> <p> If if you do not agree with this charge, you can have it reversed. Please contact your bank within eight weeks of the debit. Please ask your bank for the conditions. </p>'  ,'poppyz');
        $sepa_checkbox = !empty($options['sepa_checkbox']) ? $options['sepa_checkbox'] : __( 'I agree with the terms of automatic charging' );

        ?>
        <div id="purchase-text">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Payments'  ,'poppyz');   ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <table class="form-table">
                <?php
                $settings = array(
                    'textarea_rows' => 15,
                    'editor_height' => 425,
                    'tabindex' => 1,
                );
                ?>
                <tr>
                    <th>
                        <label><?php echo __( 'Purchase Content:' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[purchase_page_content]';
                        wp_editor( $purchase_page_content, 'purchase_page_content', $settings);
                        ?>
                        <p class="description"><?php echo __( 'This content shows on top of the tier page, after the course title and price text.' ); ?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Purchase Button Text:' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'purchase_button_text', $options, '', null ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Free Button Text:'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'free_purchase_button_text', $options, '', null ); ?>
                    </td>
                </tr>

                <tr>
                    <th>
                        <label><?php echo __( 'Review Details Content:' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[review_page_content]';
                        wp_editor( $review_page_content, 'review_page_content', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'This content shows on top the review details page.' ,'poppyz'); ?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Invoice Page Content:'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[invoice_page_content]';
                        wp_editor( $invoice_page_content, 'invoice_page_content', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'This content shows on top the invoice page.'  ,'poppyz');?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Payment Successful Page Title:'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        if ( empty( $return_page_success_title ) ) {
                            $return_page_success_title = __( 'Thank you for your purchase.'  ,'poppyz');
                        }
                        ?>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'return_page_success_title', $options, $return_page_success_title  ); ?>
                        <p class="description full"><?php echo __( 'This title shows after a successful payment'  ,'poppyz'); ?></p>
                    </td>
                </tr>
                <tr>

                    <th>
                        <label><?php echo __( 'Payment Successful Page Content:' ,'poppyz');?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[return_page_success_content]';
                        if ( empty( $return_page_success_content ) ) {
                            $return_page_success_content = "<p>" .  __( "You can now start your course. Click [course-link] to begin." ,'poppyz')   . "</p>";

                        }
                        wp_editor( $return_page_success_content, 'return_page_success_content', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'This content shows after a successful payment. Use the shortcode [course-link] to insert a link to the course that has been purchased and [name] for the buyer. Use [course-name] to display the course title.'  ,'poppyz'); ?></p>
                    </td>
                </tr>
                <tr>

                    <th>
                        <label><?php echo __( 'Payment Failed Page Content:'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[return_page_failed_content]';
                        if ( empty( $return_page_failed_content ) ) {
                            $return_page_failed_content = "<h2>" .  __( 'You were not able to purchase the course.' ,'poppyz')   . "</h2>";
                        }
                        wp_editor( $return_page_failed_content, 'return_page_failed_content', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'This content shows after a failed payment.' ,'poppyz') ?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Sepa checkbox text'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[sepa_checkbox]';
                        wp_editor( $sepa_checkbox, "sepa_checkbox", $settings);
                        ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Sepa Text:' ); ?><br />
                            <p class="description full"><?php echo __( 'The text inside the curly braces are not shortcodes. Please change them manually to their correct values before saving. The [company] shortcode is from the invoice company name'  ,'poppyz');?></p>
                        </label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[sepa_text]';
                        wp_editor( $sepa_text, 'sepa_text', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'The [company] shortcode is taken from the invoice company name'  ,'poppyz'); ?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Sepa Text After Payment:' ); ?><br />
                            <p class="description full"><?php echo __( 'The text inside the curly braces are not shortcodes. Please change them manually to their correct values before saving.' ,'poppyz'); ?></p>
                        </label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[sepa_text_after]';
                        wp_editor( $sepa_text_after, 'sepa_text_after', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'The [company] shortcode is taken from the invoice company name' ); ?></p>
                    </td>
                </tr>
            </table>
        </div>
        <div id="notifications-text">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Notifications'  ,'poppyz');  ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <table class="form-table">
                <?php
                $settings = array(
                    'textarea_rows' => 15,
                    'editor_height' => 425,
                    'tabindex' => 1,
                );
                ?>
                <tr>
                    <th>
                        <label><?php echo __( 'Course not accessible:' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[course_not_accessible_text]';
                        $default = __("Sorry, you do not have access to this course." ,'poppyz');
                        if ( empty( $course_not_accessible_text ) ) {
                            $course_not_accessible_text = $default;
                        }
                        wp_editor( $course_not_accessible_text, 'course_not_accessible_text', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'Default: '  ,'poppyz') . $default; ?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Lesson not yet available:'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[lesson_not_yet_available_text]';
                        $default = __( 'Sorry, this lesson isn\'t available yet. It will be available on: [start-date]' ,'poppyz');
                        if ( empty( $lesson_not_yet_available_text ) ) {
                            $lesson_not_yet_available_text = $default;
                        }
                        wp_editor( $lesson_not_yet_available_text, 'lesson_not_yet_available_text', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'Default: ' ,'poppyz') . $default; ?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Lesson excluded:' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[lesson_excluded_text]';
                        $default = __( 'Sorry, this lesson is excluded from the course that you\'re subscribed to.' ,'poppyz');
                        if ( empty( $lesson_excluded_text ) ) {
                            $lesson_excluded_text = $default;
                        }
                        wp_editor( $lesson_excluded_text, 'lesson_excluded_text', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'Default: ' ,'poppyz') . $default; ?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Lesson not accessible:'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[lesson_not_accessible_text]';
                        $default = __("Sorry, you do not have access to this lesson." ,'poppyz');
                        if ( empty( $lesson_not_accessible_text ) ) {
                            $lesson_not_accessible_text = $default;
                        }
                        wp_editor( $lesson_not_accessible_text, 'lesson_not_accessible_text', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'Default: '  ,'poppyz') . $default; ?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Tier limit reached text:'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[tier_limit_reached_text]';
                        $default = __( 'The number of subscriptions allowed for this tier has been reached.' );
                        if ( empty( $tier_limit_reached_text ) ) {
                            $tier_limit_reached_text = $default;
                        }
                        wp_editor( $tier_limit_reached_text, 'tier_limit_reached_text', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'Default: '  ,'poppyz') . $default; ?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Tier expired text:' ,'poppyz');?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[tier_expired_text]';
                        $default = __("Sorry, this tier already expired." ,'poppyz');
                        if ( empty( $tier_expired_text ) ) {
                            $tier_expired_text = $default;
                        }
                        wp_editor( $tier_expired_text, 'tier_expired_text', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'Default: ' ,'poppyz') . $default; ?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Tier purchase link expired text:' ,'poppyz');?></label>
                    </th>
                    <td>
						<?php
						$settings['textarea_name'] = 'ppy_settings[tier_purchase_link_expired_text]';
						$default = __("Sorry, this link has already expired." ,'poppyz');
						if ( empty( $tier_purchase_link_expired_text ) ) {
							$tier_purchase_link_expired_text = $default;
						}
						wp_editor( $tier_purchase_link_expired_text, 'tier_purchase_link_expired_text', $settings);
						?>
                        <p class="description full"><?php echo __( 'Default: ' ,'poppyz') . $default; ?></p>
                    </td>
                </tr>
            </table>
        </div>
        <div id="terms-text">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'AVG'  ,'poppyz');  ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <table class="form-table">
                <?php
                $settings = array(
                    'textarea_rows' => 15,
                    'editor_height' => 425,
                    'tabindex' => 1,
                );
                ?>
                <tr class="hide-terms">
                    <th>
                        <label><?php echo __( 'Hide Terms and Conditions'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'hide_terms', $options, '', null ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Terms and Condition Title'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'terms_condition_title', $options, $terms_condition_title  ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Terms and conditions'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings['textarea_name'] = 'ppy_settings[terms_condition]';
                        wp_editor( $terms_condition, 'terms_condition', $settings);
                        ?>
                    </td>
                </tr>
                <?php
                /*$default_terms_checkboxes = array(
                    __( 'I agree with the Terms and Conditions (make a link)' ),
                    __( 'I agree with the Privacy statement (make a link).' ),
                    __( 'I agree that my data will be used to send updates and access the lessons.' ),
                );*/
                ?>
                <?php for ( $i=0; $i<5; $i++ ) : ?>
                    <tr>
                        <th>
                            <?php $checkbox_number = $i + 1; ?>
                            <label><?php echo __( 'Terms checkbox'  ,'poppyz') . ' ' . $checkbox_number; ?></label>
                        </th>
                        <td>
                            <?php
                            //$default_text =  isset( $default_terms_checkboxes[$i] ) ? $default_terms_checkboxes[$i] : '';
                            $terms_checkbox = !empty( $options['terms_checkbox'][$i] ) ? $options['terms_checkbox'][$i] : '';
                            $settings['textarea_name'] = 'ppy_settings[terms_checkbox][]';
                            wp_editor( $terms_checkbox, "terms_checkbox_$i", $settings);
                            ?>
                        </td>
                    </tr>
                <?php endfor; ?>
                <tr>
                    <th>
                        <label><?php echo __( 'Checkboxes required message'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php  $terms_checkbox_required = !empty( $options['terms_checkbox_required'] ) ? $options['terms_checkbox_required'] : __( 'You must first tick all check boxes to continue.' ); ?>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'terms_checkbox_required', $options, $terms_checkbox_required  ); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div id="register-form">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Registration Fields' ,'poppyz'); ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>

            <table class="form-table" style="width: auto;">
                <tr>
                    <th><?php // echo __( 'Field' ); ?></th>
                    <td><?php echo __( 'New labels for fields'  ,'poppyz'); ?></td>

                </tr>

                <tr>
                    <th>
                        <label><?php echo  __( 'Email' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'reg_user_email', $options, '', null ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo  __( 'First Name'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'reg_first_name', $options, '', null ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo  __( 'Last Name' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'reg_last_name', $options, '', null ); ?>
                    </td>
                    <!--<td>
                        <?php /*echo Poppyz_Core::form_settings_helper( 'checkbox', 'reg_required_address', $options); */?>
                    </td>-->
                </tr>

                <tr>
                    <th>
                        <label><?php echo  __( 'Address'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'reg_address', $options, '', null ); ?>
                    </td>
                    <!--<td>
                        <?php /*echo Poppyz_Core::form_settings_helper( 'checkbox', 'reg_required_address', $options); */?>
                    </td>-->
                </tr>
                <tr>
                    <th>
                        <label><?php echo  __( 'Zipcode'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'reg_zipcode', $options, '', null ); ?>
                    </td>
                    <!--<td>
                        <?php /*echo Poppyz_Core::form_settings_helper( 'checkbox', 'reg_required_zipcode', $options); */?>
                    </td>-->
                </tr>
                <tr>
                    <th>
                        <label><?php echo  __( 'City' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'reg_city', $options, '', null); ?>
                    </td>
                    <!--<td>
                        <?php /*echo Poppyz_Core::form_settings_helper( 'checkbox', 'reg_required_city', $options); */?>
                    </td>-->
                </tr>
                <tr>
                    <th>
                        <label><?php echo  __( 'Country' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'reg_country', $options, '', null ); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Default' ,'poppyz'); ?></th>
                    <td><?php echo  Poppyz_Core::country_select( 'ppy_settings[default_country]',  $options['default_country'] ); ?></td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo  __( 'Company'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'reg_company', $options, '', null ); ?>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'reg_disabled_company', $options, '', null, '', 'invisible'); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo  __( 'Phone' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'reg_phone', $options, '', null ); ?>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'reg_disabled_phone', $options, '', null, '', 'invisible'); ?>

                    </td>
                </tr>
            </table>
            <table class="form-table">
                <tr>
                    <th>
                        <label><?php echo __( 'Registration Form Description:'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        $settings = array(
                            'textarea_rows' => 20,
                            'editor_height' => 425,
                            'tabindex' => 1,
                            'textarea_name' => 'ppy_settings[registration_form_description]'
                        );
                        wp_editor( $registration_form_description, 'registration_form_description', $settings);
                        ?>
                        <p class="description full"><?php echo __( 'This content shows above the registration from.' ,'poppyz'); ?></p>
                    </td>
                </tr>
            </table>
            <table class="form-table">
                <tr>
                    <th>
                        <label><?php echo __( 'Extra Content:'  ,'poppyz'); ?></label>
                    </th>
                    <td>
						<?php
						$settings = array(
							'textarea_rows' => 20,
							'editor_height' => 425,
							'tabindex' => 1,
							'textarea_name' => 'ppy_settings[registration_form_extra_content]'
						);
						wp_editor( $registration_form_extra_content, 'registration_form_extra_content', $settings);
						?>
                        <p class="description full"><?php echo __( 'This content shows above the registration from.' ,'poppyz'); ?></p>
                    </td>
                </tr>
            </table>
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2>ReCAPTCHA V3</h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <p class="description full"><?php echo sprintf(__( 'Read on how to get the keys from <a href="%s">here</a>' ,'poppyz') , 'https://poppyz.nl/hrf_faq/why-do-i-keep-getting-a-lot-of-new-users-that-look-suspicious-and-how-do-i-stop-them/') ?></p>
            <table class="form-table">
                <tr>
                    <th>
                        <label><?php echo __( 'reCAPTCHA V3 Site Key'  ,'poppyz');?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'recaptcha_v3_site_key', $options, '', null ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'reCAPTCHA V3 Secret Key'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'recaptcha_v3_secret_key', $options, '', null ); ?>
                    </td>
                </tr>
            </table>
        </div>

        <div id="payment-options">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Payment' ,'poppyz'); ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <?php $currency = isset( $options['currency'] ) ? $options['currency'] : 'eur'; ?>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="mc-api-key"><?php echo __( 'Currency' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <select class="short" name="ppy_settings[currency]" id="login-page">
                            <option <?php selected($currency, 'eur'); ?> value="eur">EUR</option>
                            <option <?php selected($currency, 'usd'); ?> value="usd">USD</option>
                        </select>
                    </td>
                </tr>
            </table>

            <table class="form-table">
                <!--<tr>
                    <th>
                        <label for="enable-taxes"><?php /*echo __( 'Taxes' ); */?></label>
                    </th>
                    <td>
                        <input type="checkbox" name="ppy_settings[enable_taxes]" id="enable-taxes" <?php /*if ( isset( $options['enable_taxes'] ) ) checked(  $options['enable_taxes'], "on" ); */?> />
                        <span><?php /*echo __( 'Enable taxes?' ); */?></span>
                    </td>
                </tr>-->
                <tr>
                    <th>
                        <label for="tax-rate"><?php echo __( 'Tax Rate' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <input class="ppy-input short" type="number" name="ppy_settings[tax_rate]" id="tax-rate" value="<?php echo (isset($options['tax_rate']) && $options['tax_rate'] != '') ? $options['tax_rate'] : '21'; ?>" />
                        <small>%</small>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Country' ,'poppyz'); ?></th>
                    <td><?php echo Poppyz_Core::country_select( 'ppy_settings[country]',  $options['country'] ); ?></td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __( 'Prices entered with tax' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <div class="radio-wrapper">
                            <label for="price-tax"><input type="radio" id="price-tax" name="ppy_settings[price_tax]" value="inc" <?php if ( isset( $options['price_tax'] ) ) checked(  $options['price_tax'], "inc" ); ?> /><?php echo __( 'incl. VAT' ); ?></label>
                            <label for="price-tax2"><input type="radio" id="price-tax2" name="ppy_settings[price_tax]" value="exc" <?php echo ( isset( $options['price_tax'] ) ) ? checked(  $options['price_tax'], "exc", false ) : 'checked=checked'; ?> /><?php echo __( 'excl. VAT' ); ?></label>
                        </div>
                        <!--<p class="description full"><?php /*echo __( 'If the price is 10 euros, then the total amount on the invoice under "excluding VAT" is 10 + VAT = 12.10 (in the case of VAT 21%). And 10 euros in the case of "including VAT".' ); */?></p>-->
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="show-vat-shifted"><?php echo __( 'Vat shifted field'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'show_vat_shifted', $options, '', null, __( 'Activate this field if you have clients outside the country you are in and within the EU.'  ,'poppyz'), 'eye' ); ?>
                        <div class="callout-box"><label><?php echo __('Text to show to the user: '); ?></label><?php echo Poppyz_Core::form_settings_helper( 'text', 'vat_shifted_text', $options, __( 'VAT Shifted? Only check this if your company is registered outside the Netherlands and within the EU.'  ,'poppyz'), '', null, 'full' ); ?></div>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="show-out-scope"><?php echo __( 'VAT out of scope field'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'vat_out_of_scope', $options, '', 'show-out-scope', __( ' Activate this field if you have clients outside the EU.'  ,'poppyz'), 'eye' ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="vat_belgium"><?php echo __( 'Vat Number Field'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'vat_belgium', $options, '', null, __( 'This is just needed when you\'re company is registered in Belgium and you sell B2B.' ), 'eye' ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="vat_disabled"><?php echo __( 'Disable VAT?' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'vat_disabled', $options, '', null, __( 'Check this if your company is excluded from VAT.' ), 'enable-disable' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="coupons-disabled"><?php echo __( 'Disable coupons?' ,'poppyz');?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'coupons_disabled', $options, '', null, __( 'Do not allow users to use coupons.' ), 'enable-disable'  ); ?>
                    </td>
                </tr>
            </table>
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2>Mollie</h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="mo-api-key"><?php echo __( 'API Key'  ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'mo_api_key', $options, '', 'mo-api-key' ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="mo-api-key"><?php echo __( 'Test API Key'  ,'poppyz'); ?></label>
                    </th>
                    <td>
						<?php echo Poppyz_Core::form_settings_helper( 'text', 'mo_api_test_key', $options, '', 'mo-api-test-key' ); ?>
                        <span class="description"><?php echo __( 'When logged in as an admin, you can make a test purchase without making a real payment.' ,'poppyz'); ?></span>
                    </td>
                </tr>
                <tr style="display:none;">
                    <th>
                        <label for="coupons-disabled"><?php echo __( 'Activate Test API Key' ,'poppyz');?></label>
                    </th>
                    <td>
						<?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'mollieTestMode', $options, '', null, '', 'enable-disable'  ); ?>
                    </td>
                </tr>
            </table>
        </div>

        <div id="invoice-options">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __(' Invoice Options '); ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="invoice-expiration-days"><?php echo __( 'Days before an invoice expires' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'number', 'invoice_expiration_days', $options, '', '', '', 'ppy-input short' ); ?><small><?php echo __('days'); ?></small>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="invoice-starting-number"><?php echo __( 'Invoice starting number'  ,'poppyz'); ?></label>
                    </th>
                    <td>
		                <?php echo Poppyz_Core::form_settings_helper( 'number', 'invoice_starting_number', $options, '', '', '0001', 'ppy-input', __( 'The current year will automatically be prepended and the number will having leading zero\'s i.e 2021-0001'  ,'poppyz')  ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="invoice-starting-number"><?php echo __( 'Test Invoice starting number'  ,'poppyz'); ?></label>
                    </th>
                    <td>
						<?php echo Poppyz_Core::form_settings_helper( 'number', 'test_invoice_starting_number', $options, '', '', '0001', 'ppy-input', __( 'The current year will automatically be prepended and the number will having leading zero\'s i.e 2021-0001'  ,'poppyz')  ); ?>
                    </td>
                </tr>
            </table>

            <h2><?php echo __( 'Invoice Details' ); ?></h2>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="invoice-company"><?php echo __( 'Company Name' ,'poppyz')?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'invoice_company', $options ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="invoice-logo"><?php echo __('Logo', 'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'hidden', 'invoice_logo', $options ); ?>
                        <input id="upload_logo_button" type="button" class="button" value="<?php echo __( 'Upload Logo'  ,'poppyz');  ?>" />
                        <br /><small><?php echo __( 'Only upload images that are jpeg or png. Recommended size: 250px x 250px' ,'poppyz');  ?></small>
                        <div id="upload_invoice_logo_preview">
                            <br />

                            <?php
                            if ( isset( $options['invoice_logo'] ) && !empty( $options['invoice_logo'] ) ) :
                                $image_src = wp_get_attachment_image_src( $options['invoice_logo'], 'invoice-logo' ); ?>
                                <img style="max-width:100%;" src="<?php echo $image_src[0]; ?>" />
                                <input id="delete-invoice-logo" name="ppy_settings[delete_invoice_logo]" type="submit" class="button" value="<?php _e( 'Delete' ); ?>" />
                            <?php else:  ?>
                                <img src="" style="display: none;" />
                            <?php endif; ?>
                        </div>

                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="invoice-company"><?php echo __( 'Contact Person' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'invoice_contact_person', $options ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="invoice-zipcode"><?php echo __( 'Phone' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'invoice_phone', $options ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="invoice-email"><?php echo __( 'Email a copy?' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'send_invoice', $options, '', '', __('Send copies of new invoices. ?','poppyz') , 'enable-disable' ); ?>
                        <br />
                        <div class="callout-box invoice-email-box">
                            <p class="description"><?php echo __('Email Address','poppyz');  ?></p>
                            <?php echo Poppyz_Core::form_settings_helper( 'text', 'invoice_email', $options ); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="invoice-address"><?php echo __( 'Address' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'invoice_address', $options ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="invoice-zipcode"><?php echo __( 'Zipcode' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'invoice_zipcode', $options ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="invoice-city"><?php echo __( 'City','poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'invoice_city', $options ); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="invoice-country"><?php echo __( 'Country' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <?php
                        $invoice_country = isset( $options['invoice_country'] ) ? $options['invoice_country'] : '';
                        echo Poppyz_Core::form_settings_helper( 'text', 'invoice_country', $options );
                        ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="invoice-kvk"><?php echo __( 'IBAN' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'invoice_iban', $options ); ?>
                    </td>
                <tr>
                <tr>
                    <th>
                        <label for="invoice-kvk"><?php echo __( 'KVK number' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'invoice_kvk', $options ); ?>
                    </td>
                <tr>
                    <th>
                        <label for="invoice-btw"><?php echo __( 'BTW number' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'invoice_btw', $options ); ?>
                    </td>
                </tr>

                <?php
                $options['invoice_footer'] = isset( $options['invoice_footer'] ) ? $options['invoice_footer'] : '';
                $settings = array(
                    'teeny' => true,
                    'media_buttons' => false,
                    'textarea_rows' => 15,
                    'editor_height' => 425,
                    'tabindex' => 1,
                    'textarea_name' => 'ppy_settings[invoice_footer]'
                );
                ?>
                <tr>
                    <th>
                        <label for="invoice-footer"><?php echo __( 'Footer Text: ','poppyz'); ?></label>
                    </th>
                    <td>
                        <?php wp_editor( $options['invoice_footer'] , 'invoice_footer', $settings); ?>
                        <p class="description"><?php echo __( 'This text will appear at the bottom of the invoice.' ,'poppyz');  ?></p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label><?php echo __('Sample','poppyz');  ?></label>
                    </th>
                    <td>
                        <a class="ppy_button button" target="_blank" href="<?php echo esc_url( wp_nonce_url( add_query_arg( array( 'page' => 'poppyz-invoices', 'ppy-action' => 'generate-pdf', 'invoice' => 0, 'preview' => 1 ) ), 'ppy-nonce' ) ); ?>"><strong><?php echo __( 'Preview invoice template','poppyz');  ?></strong></a>
                    </td>
                </tr>

            </table>
        </div>

        <div id="integration-options">
        </div>
            <?php
                $newsletter = new \Poppyz\Includes\Newsletters\Newsletter( new \Poppyz\Includes\Newsletters\Mailchimp());
                $mailchimp_list = $newsletter->getFormattedLists();

                $newsletter->setNewsletter(new \Poppyz\Includes\Newsletters\Aweber());
                $aweber_list = $newsletter->getFormattedLists();
			    $awApiKey =  Poppyz_Core::get_option( 'aw_code' );

                $newsletter->setNewsletter(new \Poppyz\Includes\Newsletters\Laposta());
                $la_list = $newsletter->getFormattedLists();

                $newsletter->setNewsletter(new \Poppyz\Includes\Newsletters\Mailerlite());
                $mailerlite_list = $newsletter->getFormattedLists();

                $newsletter->setNewsletter(new \Poppyz\Includes\Newsletters\Activecampaign());
                $ac_list = $newsletter->getFormattedLists();
			    $acApiKey =  Poppyz_Core::get_option( 'ac_api_key' );

            ?>
        <div id="newsletters">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2>Mailchimp</h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <?php echo (!is_object($mailchimp_list) && !is_array($mailchimp_list) && !empty($mailchimp_list))? __( "We can't connect to MailChimp. Please make sure you entered the correct credentials" ,"poppyz") : ""; ?>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="mc-api-key"><?php echo __( 'API Key' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <input type="text" name="ppy_settings[mc_api_key]" id="mc-api-key" value="<?php echo (isset($options['mc_api_key']) && $options['mc_api_key'] != '') ? $options['mc_api_key'] : ''; ?>" /><br/>
                    </td>
                </tr>
            </table>

            <h2>MailerLite</h2>
            <?php echo (!is_object($mailerlite_list) && !is_array($mailerlite_list) && !empty($mailerlite_list))? __( "We can't connect to MailerLite. Please make sure you entered the correct credentials" ,"poppyz") : ""; ?>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="mc-api-key"><?php echo __( 'API Key','poppyz');  ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'text', 'ml_api_key', $options  ); ?>
                    </td>
                </tr>
            </table>

            <?php
            require_once( PPY_DIR_PATH . 'includes/class-poppyz-newsletter.php' );
            $ppy_nl = new Poppyz_Newsletter();
            ?>
            <h2>AWeber</h2>
			<?php echo (empty($aweber_list) && !empty($awApiKey))? __( "We can't connect to AWeber. Please make sure you entered the correct credentials" ,"poppyz") : ""; ?>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="aw-code"><?php echo __( 'Authorization Code' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <textarea cols="80" rows="5" name="ppy_settings[aw_code]" id="aw-code" ><?php echo (isset($options['aw_code']) && $options['aw_code'] != '') ? $options['aw_code'] : ''; ?></textarea><br/>
                        <a href="<?php echo $ppy_nl->aw_get_authorization_link(); ?>" target="_blank"><?php echo __('Get authorization code','poppyz'); ?></a>
                        <input type="hidden" name="ppy_settings[aw_consumer_key]" value="<?php echo (isset($options['aw_consumer_key']) && $options['aw_consumer_key'] != '') ? $options['aw_consumer_key'] : ''; ?>" />
                        <input type="hidden" name="ppy_settings[aw_consumer_secret]" value="<?php echo (isset($options['aw_consumer_secret']) && $options['aw_consumer_secret'] != '') ? $options['aw_consumer_secret'] : ''; ?>" />
                        <input type="hidden" name="ppy_settings[aw_access_key]" value="<?php echo (isset($options['aw_access_key']) && $options['aw_access_key'] != '') ? $options['aw_access_key'] : ''; ?>" />
                        <input type="hidden" name="ppy_settings[aw_access_secret]" value="<?php echo (isset($options['aw_access_secret']) && $options['aw_access_secret'] != '') ? $options['aw_access_secret'] : ''; ?>" />

                    </td>
                </tr>
            </table>
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2>Active Campaign</h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
			<?php echo (empty($ac_list) && !empty($acApiKey))? __( "We can't connect to Active Campaign. Please make sure you entered the correct credentials" ,"poppyz") : ""; ?>
            <?php echo (!is_object($ac_list) && !is_array($ac_list) && !empty($ac_list))? __( "We can't connect to Active Campaign. Please make sure you entered the correct credentials" ,"poppyz") : ""; ?>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="ac-api-url"><?php echo __( 'API URL' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <input type="text" name="ppy_settings[ac_api_url]" id="ac-api-url" value="<?php echo (isset($options['ac_api_key']) && $options['ac_api_url'] != '') ? $options['ac_api_url'] : ''; ?>" /><br/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="ac-api-key"><?php echo __( 'API Key','poppyz');  ?></label>
                    </th>
                    <td>
                        <input type="text" name="ppy_settings[ac_api_key]" id="ac-api-key" value="<?php echo (isset($options['ac_api_key']) && $options['ac_api_key'] != '') ? $options['ac_api_key'] : ''; ?>" /><br/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="nl-tags"><?php echo __( 'Tags' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <input type="text" name="ppy_settings[nl_tags]" id="nl-tags" value="<?php echo (isset($options['nl_tags']) && $options['nl_tags'] != '') ? $options['nl_tags'] : ''; ?>" /><br/>
                        <p class="description"><?php echo __( 'Use a comma separated list, e.g tag1,tag2,tag3' ,'poppyz');  ?></p>
                    </td>
                </tr>
            </table>
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2>Laposta</h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>
            <?php echo (!is_object($la_list) && !is_array($la_list) && !empty($la_list))? __( "We can't connect to Laposta. Please make sure you entered the correct credentials" ,"poppyz") : ""; ?>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="la-api-key"><?php echo __( 'API Key','poppyz');  ?></label>
                    </th>
                    <td>
                        <input type="text" name="ppy_settings[la_api_key]" id="la-api-key" value="<?php echo (isset($options['la_api_key']) && $options['la_api_key'] != '') ? $options['la_api_key'] : ''; ?>" /><br/>
                    </td>
                </tr>
            </table>
        </div>
        <div id="payments">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2>Moneybird</h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>


            <table class="form-table" id="mb-v2">
                <tr>
                    <th>
                        <label for="mb-api-key"><?php echo __( 'Administration ID' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <input type="text" name="ppy_settings[mb_admin_id]" id="mb-admin-id" value="<?php echo (isset($options['mb_admin_id']) && $options['mb_admin_id'] != '') ? $options['mb_admin_id'] : ''; ?>" /><br/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="mb-api-key"><?php echo __( 'API Key' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <input type="text" name="ppy_settings[mb_api_key]" id="mb-api-key" value="<?php echo ( isset( $options['mb_api_key']) && $options['mb_api_key'] != '') ? $options['mb_api_key'] : ''; ?>" /><br/>
                    </td>
                </tr>
            </table>

            <table class="form-table" id="mb-invoice-profile-section">
                <?php
                $payment = new Poppyz_Payment();
                $invoice_profiles = $payment->get_invoice_profiles();
                $invoice_workflows = $payment->get_invoice_workflows();
                $invoice_taxrates = $payment->get_invoice_taxrates();
                $invoice_categories = $payment->get_invoice_categories();
                ?>

                <tr>
                    <th>
                        <label for="mb-invoice-profile"><?php echo __( 'Invoice Profile' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php
                        if (  isset( $options['mb_api_key'] ) && $options['mb_api_key'] != ''  && $invoice_profiles  ) : ?>
                            <select name="ppy_settings[invoice_profile]" id="mb-invoice-profile">
                                <?php
                                $options['invoice_profile'] = isset($options['invoice_profile']) ? $options['invoice_profile'] : '';
                                //echo '<option value="-1">' . __( 'Select a page...' ) .  '</option>';
                                foreach ( $invoice_profiles as $profile ) {
                                    $option = '<option value="' . $profile->id . '" ' . selected( $options["invoice_profile"],  $profile->id, false ) . '>';
                                    $option .=  $profile->name;
                                    $option .= '</option>';
                                    echo $option;
                                }
                                ?>
                            </select>
                        <?php else: ?>
                            <small><em><?php echo __( 'Please save your correct MoneyBird credentials first to select an invoice profile.' ,'poppyz');  ?></em></small>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php if ( $invoice_workflows ) : ?>
                    <tr>
                        <th>
                            <label for="mb-invoice-workflow"><?php echo __( 'Invoice Workflow' ,'poppyz');  ?></label>
                        </th>
                        <td>
                            <?php
                            if ( isset( $options['mb_api_key'] ) && $options['mb_api_key'] != ''  ) : ?>
                                <select name="ppy_settings[invoice_workflow]" id="mb-invoice-workflow">
                                    <?php
                                    $options["invoice_workflow"] = isset($options['invoice_workflow']) ? $options['invoice_workflow'] : '';
                                    //echo '<option value="-1">' . __( 'Select a page...' ) .  '</option>';
                                    foreach ( $invoice_workflows as $workflow ) {
                                        if ( $workflow->active != true || $workflow->type != "InvoiceWorkflow" ) continue;
                                        $option = '<option value="' . $workflow->id . '" ' . selected( $options["invoice_workflow"],  $workflow->id, false ) . '>';
                                        $option .=  $workflow->name;
                                        $option .= '</option>';
                                        echo $option;
                                    }
                                    ?>
                                </select>
                            <?php else: ?>
                                <small><em><?php echo __( 'Please save your correct MoneyBird credentials first to select an invoice workflow.' ,'poppyz');  ?></em></small>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <!--<tr>
                        <th>
                            <label for="mb-invoice-workflow-fp"><?php /*echo __( 'Invoice Workflow - First Payment' ); */?></label>
                        </th>
                        <td>
                            <?php
/*                            if ( isset( $options['mb_api_key'] ) && $options['mb_api_key'] != ''  ) : */?>
                                <select name="ppy_settings[invoice_workflow_fp]" id="mb-invoice-workflow-fp">
                                    <?php
/*                                    $options["invoice_workflow_fp"] = isset($options['invoice_workflow_fp']) ? $options['invoice_workflow_fp'] : '';
                                    //echo '<option value="-1">' . __( 'Select a page...' ) .  '</option>';
                                    foreach ( $invoice_workflows as $workflow ) {
                                        if ( $workflow->active != true || $workflow->type != "InvoiceWorkflow" ) continue;
                                        $option = '<option value="' . $workflow->id . '" ' . selected( $options["invoice_workflow_fp"],  $workflow->id, false ) . '>';
                                        $option .=  $workflow->name;
                                        $option .= '</option>';
                                        echo $option;
                                    }
                                    */?>
                                </select>
                            <?php /*else: */?>
                                <small><em><?php /*echo __( 'Please save your correct MoneyBird credentials first to select an invoice workflow.' ); */?></em></small>
                            <?php /*endif; */?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label for="mb-invoice-workflow-auto"><?php /*echo __( 'Invoice Workflow - Automatic Payments' ); */?></label>
                        </th>
                        <td>
                            <?php
/*                            if ( isset( $options['mb_api_key'] ) && $options['mb_api_key'] != ''  ) : */?>
                                <select name="ppy_settings[invoice_workflow_auto]" id="mb-invoice-workflow-auto">
                                    <?php
/*                                    $options["invoice_workflow_auto"] = isset($options['invoice_workflow_auto']) ? $options['invoice_workflow_auto'] : '';
                                    //echo '<option value="-1">' . __( 'Select a page...' ) .  '</option>';
                                    foreach ( $invoice_workflows as $workflow ) {
                                        if ( $workflow->active != true || $workflow->type != "InvoiceWorkflow" ) continue;
                                        $option = '<option value="' . $workflow->id . '" ' . selected( $options["invoice_workflow_auto"],  $workflow->id, false ) . '>';
                                        $option .=  $workflow->name;
                                        $option .= '</option>';
                                        echo $option;
                                    }
                                    */?>
                                </select>
                            <?php /*else: */?>
                                <small><em><?php /*echo __( 'Please save your correct MoneyBird credentials first to select an invoice workflow.' ); */?></em></small>
                            <?php /*endif; */?>
                        </td>
                    </tr>-->
                <?php endif; ?>

                <?php if ( $invoice_taxrates ) : ?>
                    <tr>
                        <th>
                            <label for="mb-invoice-taxrates"><?php echo __( 'Taxrate','poppyz');  ?></label>
                        </th>
                        <td>
                            <?php

                            if ( ( isset( $options['mb_email'] ) && $options['mb_email'] != '' ) || ( isset( $options['mb_api_key'] ) && $options['mb_api_key'] != '' ) && $invoice_taxrates  ) : ?>
                                <select name="ppy_settings[invoice_taxrate]" id="mb-invoice-taxrates">
                                    <?php
                                    $options["invoice_taxrate"] = isset($options['invoice_taxrate']) ? $options['invoice_taxrate'] : '';
                                    //echo '<option value="-1">' . __( 'Select a page...' ) .  '</option>';
                                    foreach ( $invoice_taxrates as $taxrate ) {

                                        if ( $taxrate->active != true || ( $taxrate->taxRateType != 'sales_invoice' && $taxrate->taxRateType != 1 ) ) continue;
                                        $option = '<option value="' . $taxrate->id . '" ' . selected( $options["invoice_taxrate"],  $taxrate->id, false ) . '>';
                                        $option .=  $taxrate->name;
                                        $option .= '</option>';
                                        echo $option;
                                    }
                                    ?>
                                </select>
                            <?php else: ?>
                                <small><em><?php echo __( 'Please save your correct MoneyBird credentials first to select an invoice taxrate.' ,'poppyz');  ?></em></small>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if ( $invoice_categories ) : ?>
                    <tr>
                        <th>
                            <label for="mb-invoice-taxrates"><?php echo __( 'Category' ,'poppyz');  ?></label>
                        </th>
                        <td>
                            <?php

                            if ( ( isset( $options['mb_email'] ) && $options['mb_email'] != '' ) || ( isset( $options['mb_api_key'] ) && $options['mb_api_key'] != '' ) && $invoice_taxrates  ) : ?>
                                <select name="ppy_settings[invoice_category]" id="mb-invoice-category">
                                    <?php
                                    $options["invoice_category"] = isset($options['invoice_category']) ? $options['invoice_category'] : '';
                                    echo '<option value="-1">' . __( 'Select a category...','poppyz') .  '</option>';
                                    foreach ( $invoice_categories as $category ) {
                                        $option = '<option value="' . $category->id . '" ' . selected( $options["invoice_category"],  $category->id, false ) . '>';
                                        $option .=  $category->name;
                                        $option .= '</option>';
                                        echo $option;
                                    }
                                    ?>
                                </select>
                            <?php else: ?>
                                <small><em><?php echo __( 'Please save your correct MoneyBird credentials first to select an invoice taxrate.','poppyz'); ?></em></small>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endif; ?>
            </table>
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2>Autorespond</h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>

            <table class="form-table">
                <tr>
                    <th>
                        <label for="secret-key"><?php echo __( 'Secret Key' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <input type="text" name="ppy_settings[secret_key]" id="secret-key" value="<?php echo ( isset( $options['secret_key'] ) && $options['secret_key'] != '' ) ? $options['secret_key'] : ''; ?>" /> &nbsp;<input type="button" class="button" id="gen-button" value="<?php echo __( 'Generate new code','poppyz');  ?>" />
                    </td>
                </tr>
            </table>
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2>Plug & Pay</h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>

            <table class="form-table">
                <tr>
                    <th>
                        <label for="secret-key"><?php echo __( 'Token' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <input type="text" multiple="multiple" name="ppy_settings[plugandpay_token]" id="plugandpay-token" value="<?php echo ( isset( $options['plugandpay_token'] ) && $options['plugandpay_token'] != '' ) ? $options['plugandpay_token'] : ''; ?>" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="analytics">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2>Analytics</h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>

            <table class="form-table">
                <tr>
                    <th>
                        <label for="ga-id"><?php echo __( 'Google Analytics 4 ID','poppyz');  ?></label>
                    </th>
                    <td>
                        <input placeholder="G-XXXXXXXXXX" type="text" name="ppy_settings[ga_id]" id="ga-id" value="<?php echo ( isset( $options['ga_id'] ) && $options['ga_id'] != '' ) ? $options['ga_id'] : ''; ?>" />
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="ga-domain"><?php echo __( 'Set Domain Name' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <input type="text" name="ppy_settings[ga_set_domain_name]" id="ga-domain" value="<?php echo ( isset( $options['ga_set_domain_name'] ) && $options['ga_set_domain_name'] != '' ) ? $options['ga_set_domain_name'] : ''; ?>" />
                        <br /><small><?php echo __( 'This is optional. ','poppyz');  ?><a target="_blank" href="https://developers.google.com/analytics/devguides/collection/gajs/gaTrackingSite#multipleDomains"><?php echo __( 'More information. ' ); ?></a></small>
                    </td>
                </tr>
            </table>

            <h3><?php echo __( 'Tracking Options' ,'poppyz'); ?></h3>

            <table class="form-table">
                <tr>
                    <th>
                        <label for="ga-standard-tracking-enabled"><?php echo __( 'Enable tracking' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'ga_standard_tracking_enabled', $options, '', null,  __( 'There is no need to enable this if there is already another plugin for Google Analytics' ,'poppyz') ); ?>
                    </td>
                </tr>
                <tr>
                    <td><label for="ga-anonymize-enabled"><?php echo __( 'Anonymize IP addresses.' ,'poppyz'); ?></label></td>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'ga_anonymize_enabled', $options, '', null,  __( 'Enabling this option is mandatory in certain countries due to national privacy laws.' ) ); ?>
                    </td>
                </tr>
                <tr>
                    <td><label for="ga-ecommerce-tracking-enabled"><?php echo __( 'Track Purchases' ,'poppyz'); ?></label></td>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'ga_ecommerce_tracking_enabled', $options, '', null,  __( 'Track successful purchases.','poppyz') ); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div id="activation">
            <?php
            $license_status = ( isset( $options['license_status'] ) && $options['license_status'] != '' ) ? $options['license_status'] : '';
            ?>


            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2 class="activation-status"><?php echo __( 'Poppyz' ,'poppyz'); ?><?php echo $license_status == 'valid' ? '<span class="valid">' . __('Active','poppyz') . '</span>' : '<span class="invalid">' . __('Inactive','poppyz') . '</span>'; ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                    <a href="https://poppyz.nl/documentation/">Go to manual</a>
                </div>
            </div>


            <h3 class="license-status"></h3>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="license-key"><?php echo __( 'License Key:' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <input type="text" name="ppy_settings[license_key]" id="license-key" value="<?php echo ( isset( $options['license_key'] ) && $options['license_key'] != '' ) ? $options['license_key'] : ''; ?>" />
                        <input type="hidden" name="ppy_settings[license_status]" id="license-status" value="<?php echo $license_status ?>" />
                        <input type="hidden" name="ppy_settings[version]" value="<?php echo PPY_VERSION; ?>" />
                        <input type="button" class="button <?php if ( $license_status == 'valid' ) echo 'deactivate'; ?>" id="activate" value="<?php echo ( $license_status == 'valid' ) ? __( 'Deactivate' ,'poppyz')  : __( 'Activate','poppyz') ; ?>" /><br/>
                    </td>
                </tr>
            </table>
           

            <h2><?php echo __( 'Plugin Info','poppyz'); ?></h2>
            <table class="form-table">
                <tr>
                    <th><?php echo __( 'Plugin Version: ','poppyz'); ?></th>
                    <td><?php echo PPY_VERSION; ?></td>
                </tr>

                <?php $license_data = Poppyz_Core::get_license_data(); ?>
                <?php if ( $license_data ) : ?>
                    <tr>
                        <th><?php echo __( 'License Status: ','poppyz');?></th>
                        <td><?php echo $license_data['status']; ?></td>
                    </tr>
                    <tr>
                        <th><?php echo __( 'Expiration Date: ' ,'poppyz');?></th>
                        <td><?php echo $license_data['expiration_date']; ?></td>
                    </tr>

                <?php else: ?>
                    <tr>
                        <th><?php echo __( 'License Status: ','poppyz'); ?></th>
                        <td><?php echo 'invalid'; ?></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <th><?php echo __( 'Plugin Website: ' ,'poppyz'); ?></th>
                    <td><a style="color: #2673AA; text-decoration: none;" href="<?php echo PPY_WEBSITE; ?>" target="_blank"><?php echo PPY_WEBSITE; ?></a></td>
                </tr>
            </table>
            <div class="clear"></div>
            <?php
            if ( $license_data && $license_data['expiration_date']  != 'lifetime' ) {
                $target = new DateTime( $license_data['expiration_date'] );
                $origin  = new DateTime( date('Y-m-d' ) );
                $interval = $origin->diff($target);
                $interval = $interval->format('%R%a');
                if ($interval <= 7 && $interval >= 0) {
                    echo '<p class="danger red">' . __('Your license is about to expire in 1 week. If you are subscribed to a recurring payment then this warning can be disregarded.','poppyz') . '</p>';
                }
            }

            ?>

        </div>
        <div id="extras">
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Extras' ,'poppyz');   ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">

                </div>
            </div>
            <table class="form-table">
                <tr>
                    <th>
                        <label><?php _e('Reset all texts to default?', 'poppyz'); ?></label>
                    </th>
                    <td>
                        <a style="float: left; margin-right: 30px;" id="reset_default_text" class="ppy_button" href="#"><?php _e('Reset', 'poppyz'); ?></a>
                        <p class="description">
                            <?php _e('This will reset all texts on the settings page to their default including your changes. The language set in WordPress will determine the default text used. Make sure you are switched to your preferred language before resetting.', 'poppyz') ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="enable-module-page"><?php echo __( 'Module pages' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'enable-module-page', $options, '', null, __( 'Checking this option will add links to the module names on the lesson list. More info can be found <a href="https://poppyz.nl/en/documentation/module-pages/" target="_blank">here</a>.' ) ); ?>
                    </td>
                </tr>

                <tr class="module-page-section">
                    <th>

                    </th>
                    <td>
                        <div class="callout-box">
                            <label for="module-page-display"><?php echo __( 'Module page display','poppyz');  ?></label>
                            <?php $module_page_display = isset( $options['module_page_display'] ) ? $options['module_page_display'] : 'grid'; ?>
                            <select name="ppy_settings[module-page-display]" id="module-page-display">
                                <option <?php selected($module_page_display, 'grid'); ?> value="grid"><?php echo __('Grid' ,'poppyz');  ?></option>
                                <option <?php selected($module_page_display, 'list'); ?> value="list"><?php echo __('List' ,'poppyz');  ?></option>
                            </select>
                            <p class="description"><?php echo __( 'Choose a layout to use on module pages.' ); ?></p>

                            <label class="margin-top10" for="add-lesson-list-module-page"><?php echo __( 'Add lesson list','poppyz'); ?></label>
                            <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'add-lesson-list-module-page', $options ); ?>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="disable-main-menu"><?php echo __( 'Hide main menu on all courses and lessons?' ,'poppyz');  ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'disable_main_menu', $options, '',null, __( 'This feature is currenly only available for the Divi and Poppyz themes.','poppyz') ); ?>
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="show-admin-bar"><?php echo __('Show admin bar?', 'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'show_admin_bar', $options, '', null, __( 'By default, Poppyz hides the admin bar for regular users. Check this option to enable it.','poppyz')); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="enable-debug"><?php echo __( 'Enable Debugging?', 'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'enable_debugging', $options, '', null, __( 'This is for Poppyz developer support. Enabling this will log all Poppyz processes on the website', 'poppyz') ); ?>
                    </td>
                </tr>
            </table>
            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2><?php echo __( 'Experimental Features' ,'poppyz');   ?></h2>
                </div>
                <div class="ppy-settings-gotomanual-button">
                </div>
            </div>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="enable_subs_manager"><?php echo __( 'Turn on Subscriptions Manager (Experimental)', 'poppyz' ); ?></label>
                    </th>
                    <td>
                        <?php echo Poppyz_Core::form_settings_helper( 'checkbox', 'enable_subs_manager', $options, '', null,  __( 'Checking this box will display a form in the Students page where the admin can assign and remove subscriptions in bulk. The form will only show after filtering the students.', 'poppyz' ) ); ?>
                    </td>
                </tr>
            </table>
        </div>
        <?php do_action( 'ppy_settings_content', $options ); ?>
        <input type="submit" name="submit" id="submit" class="button button-primary hidden" value="Wijzigingen opslaan">
    </form>
</div>
<div class="ppy-settings-buttons-wrapper">
    <div id="ppy-settings-buttons">
        <label for="submit" id="ppy-settings-button-update" class="has-icon dashicons-before ppy_button"><?php echo  __( 'Save Changes', 'poppyz'); ?></label>
    </div>
</div>
    <script>
        jQuery(document).ready(function($){

            $( document ).on( 'click', 'input#gen-button', function() {
                $.ajax ({
                    type: 'POST',
                    url: ajaxurl,
                    data:  {
                        action: 'generate_secret_key',
                        security: '<?php echo $ajax_nonce; ?>'
                    },
                    success: function(response) {
                        $('#secret-key').val(response);
                    }
                });
            });

            $( document ).on( 'click', '#reset_default_text', function(e) {
                e.preventDefault();
                var confirmed = confirm("<?php _e('This will reset all texts to their default including your custom changes. Do you wish to proceed?','poppyz'); ?>");
                if (!confirmed) return false;
                $.ajax ({
                    type: 'POST',
                    url: ajaxurl,
                    data:  {
                        action: 'process_settings_page',
                        security: '<?php echo $ajax_nonce; ?>'
                    },
                    success: function(response) {
                        alert('Reset successful.');
                        history.go(0);
                    }
                });
            });


            $( document ).on( 'click', 'input#activate', function() {
                var btn = $(this);
                var process = 'activate';
                if (btn.hasClass('deactivate')) {
                    process = 'deactivate';
                }

                $.ajax ({
                    type: 'POST',
                    dataType: "json",
                    url: ajaxurl,
                    data:  {
                        action: 'update_license',
                        process: process,
                        license_key: $('#license-key').val(),
                        security: '<?php echo $ajax_nonce; ?>'
                    },
                    success: function(response) {
                        if(response.success == true ) {
                            var lstatus = $('h3.license-status span');
                            var lstatus_hidden = $('#license-status');
                            var activation_status = $('.activation-status');
                            if (btn.hasClass('deactivate')) {
                                btn.val('<?php echo __('Activate', 'poppyz'); ?>');
                                btn.removeClass('deactivate');
                                lstatus.html('<?php echo __('Status: Inactive', 'poppyz'); ?>');
                                lstatus_hidden.val('');
                                activation_status.html('Poppyz <span class="invalid"><?php echo __('Inactive', 'poppyz'); ?></span>');
                            } else {
                                btn.val('<?php echo __('Deactivate') ?>');
                                btn.addClass('deactivate');
                                lstatus.html('<?php echo __('Status: Active') ?>');
                                lstatus_hidden.val('valid');
                                activation_status.html('Poppyz <span class="valid"><?php echo __('Active', 'poppyz'); ?></span>');
                            }

                        } else if (response == 0){
                            alert('<?php echo __('Could not connect to the licensing server.', 'poppyz'); ?>');
                        } else {
                            alert('<?php echo __('Invalid license.', 'poppyz'); ?>');
                        }
                    }
                });
            });

            //toggleMBVersion($('#mb-v2-active'));
            $('#mb-v2-active').click(function() {
                toggleMBVersion($(this));
                //remove the invoice profiles when switching versions
                $('#mb-invoice-profile').empty()
                    .append('<option selected="selected" value="1">1</option>').hide()
                ;
                $('#mb-invoice-profile-section td small').remove();
                $('#mb-invoice-profile-section td').append('<small><em><?php echo __( 'Please save your correct MoneyBird credentials first to select an invoice profile.' , 'poppyz'); ?></em></small>');
            });

            $( document ).on( 'click', 'input#submit', function() {
                if ( $('#license-status').val() != 'valid' ) {
                    alert( '<?php echo __( 'Cannot save your settings. Please activate your license first on the Activation tab.', 'poppyz'); ?>' );
                    return false;
                }
            });

            function toggleMBVersion(obj) {
                if (obj.prop('checked')) {
                    $('#mb-v1').hide();
                    $('#mb-v2').show();
                    $('#mb-v1 input').val("");

                } else {
                    $('#mb-v1').show();
                    $('#mb-v2').hide();
                }
            }

            $('#invoice-starting-number').blur(function(event) {
                var num = $(this).val();
                var re = new RegExp("^(?!0{4})[0-9]{4}$");
                if ( num != "" && re.test(num) == false ) {
                    var formatted = ("0000" + num).slice(-4);
                    $(this).val(formatted);

                }
            });
            //allow only 1 checkbox to be checked on the vat fields settings
            /*$('input.vat-checkbox').on('change', function() {
                $('input.vat-checkbox').not(this).prop('checked', false);
            });*/


            var custom_uploader;
            $('#upload_logo_button').click(function(e) {

                e.preventDefault();

                //If the uploader object has already been created, reopen the dialog
                if (custom_uploader) {
                    custom_uploader.open();
                    return;
                }

                //Extend the wp.media object
                custom_uploader = wp.media.frames.file_frame = wp.media({
                    title: 'Choose Image',
                    button: {
                        text: 'Choose Image'
                    },
                    multiple: false
                });

                //When a file is selected, grab the URL and set it as the text field's value
                custom_uploader.on('select', function() {
                    attachment = custom_uploader.state().get('selection').first().toJSON();
                    $('#upload_image').val(attachment.url);
                    $('#invoice-logo').val(attachment.id);
                    $('#upload_invoice_logo_preview img').show();
                    $('#upload_invoice_logo_preview img').attr('src', attachment.url);
                });

                //Open the uploader dialog
                custom_uploader.open();

            });


            toggleTermsDisplay($('#hide-terms'));
            $('#hide-terms').click(function() {
                toggleTermsDisplay($(this));
            });
            function toggleTermsDisplay(obj) {
                var terms = $('div#terms-text table');
                if (obj.prop('checked')) {
                    terms.find('tr').not('.hide-terms').hide();
                } else {
                    terms.find('tr').not('.hide-terms').show();
                }
            }
            toggleInvoiceEmailDisplay($('#send-invoice'));
            $('#send-invoice').click(function() {
                toggleInvoiceEmailDisplay($(this));
            });
            function toggleInvoiceEmailDisplay(obj) {
                var invoice_email = $('.invoice-email-box');
                if (obj.prop('checked')) {
                    invoice_email.show();
                    invoice_email.attr("required");
                } else {
                    invoice_email.val("");
                    invoice_email.hide();
                }
            }

            toggleModuleSection($('#enable-module-page'));
            $('#enable-module-page').click(function() {
                toggleModuleSection($(this));
            });
            function toggleModuleSection(obj) {
                if (obj.prop('checked')) {
                    $('.module-page-section').show();
               } else {
                    $('.module-page-section').hide();
                }
            }
        });

    </script>
