<?php
    global $ppy_lang;
?>

<div class="wrap white">

    <div id="icon-options-general" class="icon32"></div>
<!--    <h2>--><?php //echo __( 'Courses' ,'poppyz'); ?><!--<a href="--><?php //echo admin_url( 'post-new.php?post_type=' . PPY_COURSE_PT ); ?><!--" class="add-new-h2 ppy_button">--><?php //echo __( 'Add New' ,'poppyz'); ?><!--</a></h2>-->
    <!-- main content -->

    <div class="main-box">
        <?php
            $ppy_core = new Poppyz_Core();
            $courses = $ppy_core->get_courses( array( 'publish', 'draft', 'private', 'pending', 'future' ) );
            $i = 0;
        ?>
        <table class="widefat ppy-admin-table">
            <?php if ($courses->have_posts()) :  ?>
                <?php
                    $alt = ($i%2 == 0) ? "class='alt'" : "";
                    $i++;

                ?>
                <thead>
                <tr>
                    <th class="row-title"><?php echo __( 'Courses'  ,'poppyz'); ?></th>
                    <th class="actions"><?php echo __( 'Actions' ,'poppyz'); ?></th>
                </tr>
                </thead>
                <tbody>
                    <?php while ( $courses->have_posts() ) : $courses->the_post(); $i++; ?>

                        <tr <?php echo $alt; ?> >
                            <?php $status = ( get_post_status() == 'draft' ) ? __( ' - <em>Draft</em>' ,'poppyz') : ''  ?>
                            <td class="row-title"><label for="tablecell"><?php the_title( '<a href="' . get_edit_post_link() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a>' ); echo $status; ?></label></td>
                            <td class="actions">
                                <a href="<?php the_permalink() ?>" class="has-icon" target="_blank" title="<?php _e('View'); ?>"><span class='dashicons dashicons-visibility'></span></a>
                                <a href="<?php echo get_edit_post_link(); ?>" class="has-icon" title="<?php _e('Edit'); ?>"><span class='dashicons dashicons-edit-large'></span></a>
                                <a href="<?php echo 'admin.php?action=ppy_clone_course&amp;post=' . get_the_ID();  ?>" title="<?php _e('Clone'); ?>" class="has-icon"><span class="dashicons dashicons-admin-page"></span></a>
                                <a href="<?php echo 'admin.php?action=ppy_delete_course&amp;post=' . get_the_ID();  ?>" title="<?php _e('Delete'); ?>" onclick="return confirm('<?php echo  __('Are you sure you want to delete this course? Click OK to delete the course and its lessons.','poppyz' ); ?>');" class="has-icon"><span class="dashicons dashicons-trash"></span></a>
                            </td>
                        </tr>

                    <?php endwhile; ?>
                </tbody>
            <?php else :  ?>
                <tr>
                    <td><?php echo __('No Courses Found', 'poppyz'); ?></td>
                </tr>
            <?php endif; ?>
        </table>
    </div> <!-- .postbox -->


</div> <!-- .wrap -->