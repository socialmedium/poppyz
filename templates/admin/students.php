<?php
global $ppy_lang;

$ppy_core = new Poppyz_Core();
$ppy_sub = new Poppyz_Subscription();
$ppy_payment = new Poppyz_Payment();


$current_url = '?page=poppyz-students';
$filter = array();
$csv_data = array();
$message = "";
$search = "";
$subs_manager_enabled = Poppyz_Core::get_option('enable_subs_manager') == 'on';
$enable_student_pagination = true;
$classArrowName   = "";
$classArrowDate   = "";
$classArrowTier   = "";
$classArrowCourse = "";
$name = "";
$course = "";
$tier = "";
$sub = "";

if ( isset( $_REQUEST['action'] ) ) {
    $action = strtolower( $_REQUEST['action'] );
    $data = array();
    if ( $action == 'remind' ) {
        if ( $_POST['remind-subject'] ) {
            $data['email_subject']  = $_POST['remind-subject'];
        }
        if ( $_POST['remind-message'] ) {
            $data['email_content']  = $_POST['remind-message'];
        }
    }

    if ( $action == 'subscribe' ) {
        $data['payment_status'] = 'admin';
    }

	$message =  Poppyz_Subscription::update_subscription( $action, $_REQUEST['id'], $data );
	if ( $action == 'cancel' || $action == 'refund'  || $action == 'delete' ) {
        if (!Poppyz_Payment::hasNoMollieKey()){ //add this check to prevent mollie from executing when a student was only added manually by an admin so there are no actual mollie subscriptions to cancel
            $ppy_payment->cancel_membership( $_REQUEST['id'] );
        }
	}
	if ( $action == "bulk-action" && isset($_POST['student-bulk-action']) ) {
		$studentBulkAction = $_POST['student-bulk-action'];
		foreach ( $_POST['subscription_id'] as $subscriptionId ) {
			Poppyz_Subscription::update_subscription( $_POST['student-bulk-action'], $subscriptionId );
			if ( $studentBulkAction == 'cancel' || $studentBulkAction == 'refund'  || $studentBulkAction == 'delete' ) {
                if (Poppyz_Payment::hasNoMollieKey()) continue;
				$ppy_payment->cancel_membership( $subscriptionId );
			}
		}
	}
}



if ( isset( $_POST['add'] ) ) {
    if ( !empty( $_POST['select-user'] ) && $_POST['select-user'] != '-1'  && !empty( $_POST['select-tier'] ) && $_POST['select-tier'] != '-1' ) {
        $send_mail = isset( $_POST['yes-ty-email'] );
        if ( empty($_POST['subscribe-course-date'] ) || ( !empty($_POST['subscribe-course-date'] ) && DateTime::createFromFormat( 'Y-m-d', $_POST['subscribe-course-date']  ) !== false ) ){
            Poppyz_Subscription::add_subscription( $_POST['select-user'], $_POST['select-tier'], '' , $_POST['subscribe-course-date'], $send_mail, 'admin' );
            $message = __( 'The subscription has been successfully added.' ,'poppyz');
        } else {
            $message = __( 'Invalid subscription date.' ,'poppyz');
        }

    } else {
        $message = __( 'Please fill in all the required fields.','poppyz' );
    }
} elseif ( $subs_manager_enabled && ( isset( $_POST['assign'] ) || isset( $_POST['delete-all'] ) ) && !empty( $_POST['student_ids'] && !empty( $_POST['select-tier'] ) && $_POST['select-tier'] != '-1' ) ) {
    $student_ids = explode(',', $_POST['student_ids'] );
    $message = '';

    foreach ( $student_ids as $id ) {
        $userinfo = get_userdata( $id );
        if (!$userinfo) continue;
        if (  isset( $_POST['delete-all'] ) ) {
            $sub_id = Poppyz_Subscription::is_subscribed_tier( $id, $_POST['select-tier'] );
            $pending = Poppyz_Subscription::has_pending_subscription_for_tier( $id, $_POST['select-tier'] );
            if ( $sub_id ) {
                Poppyz_Subscription::update_subscription( 'cancel', $sub_id );
                $message .= $userinfo->user_nicename . ' ' . __( 'is now unsubscribed to the selected tier:.','poppyz' ) . get_the_title( $_POST['select-tier'] ) . '<br />';
            } elseif ( $pending ) {
                Poppyz_Subscription::update_subscription( 'delete', $pending );
                $message .= $userinfo->user_nicename . ' ' . __( 'had a pending/cancelled subscription that is now deleted.' ,'poppyz') . '<br />';
            }
        } else {

            $pending_sub = Poppyz_Subscription::has_pending_subscription_for_tier( $id, $_POST['select-tier'] );
            $is_subscribed = !Poppyz_Subscription::is_subscribed_tier( $id, $_POST['select-tier'] );
            if ( $pending_sub ) {
                $data['payment_status'] = 'admin';
                Poppyz_Subscription::update_subscription( 'subscribe', $pending_sub, $data );
                $message .= $userinfo->user_nicename . ' ' . __( 'had a pending subscription and is now successfully subscribed.' ,'poppyz') . '<br />';
            } elseif ( $is_subscribed ) {
                Poppyz_Subscription::add_subscription( $id, $_POST['select-tier'], '' , $_POST['subscribe-course-date'], false, 'admin' );
                $message .= $userinfo->user_nicename . ' ' . __( 'successfully subscribed.' ) . '<br />';
            } else {
                $message .= $userinfo->user_nicename . ' ' . __( 'is already subscribed.' ,'poppyz') . '<br />';
            }
        }
    }
} else {
    //$ppy_sub->notify_overdue_subscriptions();
}
if ($enable_student_pagination) {
    $classArrowName   = "poppyz-arrow poppyz-down";
    $classArrowDate   = "poppyz-arrow poppyz-down";
    $classArrowTier   = "poppyz-arrow poppyz-down";
    $classArrowCourse = "poppyz-arrow poppyz-down";
    $sub = 1;
    $tier = 1;
    $course = 1;
    $name = 1;
    if (isset($_GET['query']) == 'asc') {
        $sub = 1;
        $tier = 1;
        $course = 1;
        $name = 1;
        if (isset($_GET['subscription_date'])) {
            $d = DateTime::createFromFormat( 'Y-m-d', sanitize_text_field($_GET['subscription_date']) );
            if ($d) {
                $date = date('Y-m-d', $d->getTimestamp());
                $filter['subscription_date'] = $date;
                $search .= "&subscription_date=" . $date;
            } else {
                $message = __( 'Invalid Date' );
                $filter['subscription_date'] = null;
            }
        }

        if (isset($_GET['sub']) && (boolean) $_GET['sub']) {
            $sub = 0;
            $classArrowDate = "poppyz-arrow poppyz-up";
        }


        if (isset($_GET['tier_id']) && $_GET['tier_id'] != "all"){
                $filter['u.tier_id'] =  (int)$_GET['tier_id'];
                $search .= "&tier_id=" . (int)$_GET['tier_id'];

        }

        if (isset($_GET['tier']) && (boolean) $_GET['tier']) {
            $tier = 0;
            $classArrowTier = "poppyz-arrow poppyz-up";
        }


        if (isset($_GET['course_id'])){
            $filter['u.course_id'] =  (int)$_GET['course_id'];
            $search .= "&course_id=" . (int)$_GET['course_id'];

        }

        if (isset($_GET['course']) && (boolean) $_GET['course']) {
            $course = 0;
            $classArrowCourse = "poppyz-arrow poppyz-up";
        }


        if (isset($_GET['user_id'])) {

            $filter['u.user_id'] =(int)$_GET['user_id'];
            $search .= "&user_id=" .(int)$_GET['user_id'];


        }

        if (isset($_GET['name']) && (boolean) $_GET['name']) {
            $name = 0;
            $classArrowName = "poppyz-arrow poppyz-up";
        }
    }


}
if ( isset( $_REQUEST['filter'] ) || isset( $_REQUEST['export'] ) ) {
	$_SERVER['REQUEST_URI'] = remove_query_arg( '_wp_http_referer', $_SERVER['REQUEST_URI'] );
	if ( !empty( $_REQUEST['select-user'] ) && $_REQUEST['select-user'] != '-1' ) {
		$filter['u.user_id'] =  $_REQUEST['select-user'];
		$search .= "&user_id=" .$_REQUEST['select-user'] ;
	}
	if ( !empty( $_REQUEST['select-course'] ) && $_REQUEST['select-course'] != '-1' && $_REQUEST['select-course'] != 'all') {
		$filter['u.course_id'] =  $_REQUEST['select-course'];
		$search .= "&course_id=" .$_REQUEST['select-course'];
	}
	if ( !empty( $_REQUEST['select-tier'] ) && $_REQUEST['select-tier'] != '-1'  && $_REQUEST['select-tier'] != "all") {

		$filter['u.tier_id'] =  $_REQUEST['select-tier'];
		$search .= "&tier_id=" .$_REQUEST['select-tier'];
	}
	if ( !empty( $_REQUEST['subscribe-course-date'] ) && $_REQUEST['subscribe-course-date'] != '-1' ) {
		$d = DateTime::createFromFormat( 'Y-m-d', $_REQUEST['subscribe-course-date'] );
		if ( $d !== false ) {
			$date = date( 'Y-m-d', $d->getTimestamp() ) ;
			$filter['subscription_date'] = $date;
			$search .= "&subscription_date=" . $date . "&sub=0";

		} else {
			$message = __( 'Invalid Date' );
			$filter['subscription_date'] = null;
		}
	}
}

$page = 1;
if(!empty($_REQUEST['pagination'])) {
    if(false === $_REQUEST['pagination']) {
        $page = 1;
    } else {
        $page =  $_REQUEST['pagination'];
    }
}

if (!$enable_student_pagination) {
    $page = null;
}

if (isset($_REQUEST['items-per-page'])) {
    Poppyz_Core::save_option('students_items_per_page', $_REQUEST['items-per-page']);
}

$sort_request = [];
$sort_req  ="ASC";
if (isset($_GET['sort'])) 
{
   $sort_request = $ppy_core->sort();
   $sort_req =  $sort_request['extra'];
}
$records_per_page = Poppyz_Core::get_option('students_items_per_page') ?: 20;
$pagination_options = array('records_per_page' => $records_per_page, 'page' => $page, "sort_request" =>$sort_request);
if ( !empty($_GET['search_user']) ) {
	$students = $ppy_core->get_students( $filter, $pagination_options, $_GET['search_user'] );
} else {
	$students = $ppy_core->get_students( $filter, $pagination_options );
}

$courses  = $ppy_core->get_courses();

$total_rows = count($students);


if ( !empty($_GET['search_user']) ) {
	$total_students = $ppy_core->students_count($filter, $_GET['search_user']);
} else {
	$total_students = $ppy_core->students_count($filter);
}

$total_pages = ceil((int)$total_students / (int)$records_per_page);

$pagination_html = "";
if ( $enable_student_pagination ) {
    for($page = 1; $page <= $total_pages; $page++) {
        $class = ( isset($_REQUEST['pagination']) && $page == $_REQUEST['pagination']) ? 'class=active' : '';
        if (isset($_REQUEST)) {
            $request = $_REQUEST;
            unset($request['pagination']);
            unset($request['page']);
            $post = '&'. http_build_query($request);
        }
        $pagination_html .= '<li ' . $class . '><a href="' . $current_url . '&pagination=' . $page . $post . '">' . $page . '</a></li>';
    }

}

?>
<div class="wrap white">
    <div class="main-box">
		<?php if ( !empty( $message ) ) : ?>
            <div id="message" class="poppyz-notice notice-success below-h2 dashicons-before">
                <p><?php echo $message; ?></p>
            </div>
		<?php endif; ?>
        <div class="ppy-students-tools-wrapper">
            <div id="add_user_to_course_wrapper" class="courses_page_poppyz-students students-add-user-course inactive-nav-students student-form-nav"><a href="#" class="ppy-toggle ppy-toggle-student"><?php _e( 'Add a user to a course' ,'poppyz'); ?></a></div>
            <div id="search_user_to_course_wrapper" class="courses_page_poppyz-students students-search-user inactive-students-search-user student-form-nav"><a href="#" class=" ppy-toggle-search-student"><span class="dashicons dashicons-search"></span> <?php _e( 'Search' ,'poppyz'); ?></a></div>
            <div class="ppy-students-tools-row"><span class="export-label">Export</span>
                <input type="hidden" name="student_id_array" id="student_id_array" />
                <button  id="export-pdf" name="download" value="" class="inactive ppy_button has-icon clear export-button students-export-button"><span class="dashicons dashicons-download "></span><?php echo __( 'PDF' ,'poppyz'); ?></button>
                <button  id="export-xml" name="export-xml" class="inactive ppy_button has-icon clear export-button students-export-button"><span class="dashicons dashicons-download"></span><?php echo __( 'XML',"poppyz" ); ?></button>
                <button  id="export-csv" name="export-csv" class="inactive ppy_button has-icon clear export-button students-export-button"><span class="dashicons dashicons-download"></span><?php echo __( 'CSV',"poppyz" ); ?></button>
            </div>
        </div>


        <div id="subscription-form-wrapper" class="ppy-toggle-wrapper">
            <form class="main-box poppyz-form"  action="" method="post">
				<?php Poppyz_Admin::subscription_form(true, true); ?>
            </form>
        </div>
        <div class="backdrop"></div>
        <form class="poppyz-form poppyz-popup" id="reminder-form" method="post" action="" style="display: none;">

            <?php
            $reminder_email_subject = Poppyz_Core::get_option( 'overdue_email_subject' );
            $reminder_email_message = Poppyz_Core::get_option( 'overdue_email_message' );
            ?>
            <h3><?php _e( 'Reminder Mail' );   ?></h3>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="remind-subject"><?php _e( 'Email Subject' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <input type="text" name="remind-subject" id="remind-subject" value="<?php echo $reminder_email_subject; ?>"  /><br/>
                    </td>
                </tr>
                <?php
                $settings = array(
                    'teeny' => true,
                    'textarea_rows' => 25,
                    'tabindex' => 1,
                    'textarea_name' => 'remind-message'
                );
                ?>
                <tr>
                    <th>
                        <label for="email-form-name"><?php _e( 'Email Message:' ,'poppyz'); ?></label>
                    </th>
                    <td>
                        <?php wp_editor( $reminder_email_message, 'remind-message', $settings); ?>
                        <small><?php _e( 'Shortcodes: [due-date], [account-link]' ,'poppyz'); ?></small>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="2"><input class="button button-primary button-large" type="submit" value="<?php _e( 'Submit' ); ?>" /> &nbsp; <button id="cancel-send" class="button button-large"><?php _e( 'Cancel' ); ?></button></td>
                </tr>
            </table>
            <input type="hidden" name="action" value="remind" />
            <input type="hidden" id="remind-id" name="id" value="" />
        </form>
        <form action="<?php echo (!empty($filter)) ? "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."&".http_build_query( $filter ) : "";?>" method="post" id="student-form">
            <div class="poppyz-top-tools-wrapper">
                <div class="poppyz-top-tools">
                    <select id="student-bulk-action" name="student-bulk-action" class="">
                        <option><?php echo __("Bulk action", "poppyz");?></option>
                        <option value="subscribe"><?php echo __("Subscribe", "poppyz");?></option>
                        <option value="cancel"><?php echo __("Cancel", "poppyz");?></option>
                        <option value="delete"><?php echo __("Delete", "poppyz");?></option>
                    </select>
                    <button name="action" value="bulk-action" id="bulk-action" class="btn-link apply-action" type="submit"><?php echo __("Apply", "poppyz");?></button>
                </div>
                <div class="poppyz-top-tools">
                    <ul class="ppy-students-pagination">
                        <?php echo $pagination_html; ?>
                        <?php if (isset($_REQUEST['select-user']) || isset($_GET['subscription_date'])  || isset($_GET['tier_id'])  || isset($_GET['course'])  || isset($_GET['name'])) echo '<a href="' . $current_url . '"> ' . __('Reset Search') . '</a>'; ?>
                    </ul>
                </div>
            </div>


            <input type="hidden" name="export_type" id="export_type" value="" />
            <table class="widefat ppy-admin-table table-sort-search" id="students-list">
                <thead>
                    <tr>
                        <th class="checkbox-td"><input type="checkbox" class="check_student_id" name="check_all"></th>
                        <th><?php _e('Name (username)', 'poppyz'); ?><a href="<?php echo admin_url('admin.php'); ?>?page=poppyz-students&pagination=<?php echo (int) isset($_GET['pagination']) ? $_GET['pagination'] : "1"; ?>&sort=1&query=<?php echo $sort_req == 'asc' ? 'desc' : 'asc' ?><?php echo $search; ?>&name=<?php echo $name; ?>" class='sort'><i class="<?php echo $classArrowName;?>"></i></a></th>
                        <th><?php _e('Course', 'poppyz'); ?><a href="<?php echo admin_url('admin.php'); ?>?page=poppyz-students&pagination=<?php echo (int) isset($_GET['pagination']) ? $_GET['pagination'] : "1"; ?>&sort=2&query=<?php echo $sort_req == 'asc' ? 'desc' : 'asc' ?><?php echo $search; ?>&course=<?php echo $course;?>" class='sort'><i class="<?php echo $classArrowCourse; ?>"></i></a></th>
                        <th><?php _e('Tier', 'poppyz'); ?><a href="<?php echo admin_url('admin.php'); ?>?page=poppyz-students&pagination=<?php echo (int) isset($_GET['pagination']) ? $_GET['pagination'] : "1"; ?>&sort=3&query=<?php echo $sort_req == 'asc' ? 'desc' : 'asc' ?><?php echo $search; ?>&tier=<?php echo $tier;?>" class='sort'><i class="<?php echo $classArrowTier; ?> "></i></a></th>
                        <th><?php _e('Subscription Date', 'poppyz'); ?><a href="<?php echo admin_url('admin.php'); ?>?page=poppyz-students&pagination=<?php echo (int) isset($_GET['pagination']) ? $_GET['pagination'] : "1"; ?>&sort=4&query=<?php echo $sort_req == 'asc' ? 'desc' : 'asc' ?><?php echo $search; ?>&sub=<?php echo $sub;?>" class='sort'><i class="<?php echo $classArrowDate; ?> "></i></a></th>
                        <th><?php _e( 'Payments' ,'poppyz'); ?></th>
                        <th><?php _e( 'Status' ,'poppyz'); ?><a href="<?php echo admin_url('admin.php'); ?>?page=poppyz-students&pagination=<?php echo (int) isset($_GET['pagination']) ? $_GET['pagination'] : "1"; ?>&sort=5&query=<?php echo $sort_req == 'asc' ? 'desc' : 'asc' ?><?php echo $search; ?>&sub=<?php echo $sub;?>" class='sort'><i class="<?php echo $classArrowDate; ?> "></i></a></th>
                        <th><?php _e( 'Actions' ,'poppyz'); ?></th>
                    </tr>
                </thead>
                <tbody>

                <?php for ( $i =0; $i<count($students);  $i++) : ?>
                    <tr>
                        <td class="checkbox-td">
                            <input type="checkbox" name="subscription_id[]" class="poppyz-table-id-checkbox" value="<?php echo $students[$i]['subscription']['subscription_id'];?>" id="<?php echo $i;?>">
                            <input type="hidden" id="student_status_<?php echo $i;?>" value="<?php echo $students[$i]['subscription']['status']; ?>" />
                            <input type="hidden" id="student_name_<?php echo $i;?>" value="<?php echo $students[$i]['fullname']; ?>" />
                            <input type="hidden" name="sub_id_<?php echo $students[$i]['user_id'];?>" value="<?php echo $students[$i]['subscription']['subscription_id'];?>"/>
                        </td>
                        <td class="student-name-column" data-label="<?php _e('Name (username)', 'poppyz'); ?>"><a href="<?php echo $students[$i]['student_profile'];?>" class="poppyz-table-text poppyz-table-<?php echo $i;?>-text"><?php echo $students[$i]['fullname']; ?></a></td>
                        <td data-label="<?php _e('Course', 'poppyz'); ?>"><a href="<?php echo $students[$i]['course_link'];?>" class="poppyz-table-text poppyz-table-<?php echo $i;?>-text"><?php echo $students[$i]['course']; ?></a></td>
                        <td data-label="<?php _e('Tier', 'poppyz'); ?>"><a href="<?php echo $students[$i]['tier_link'];?>" class="poppyz-table-text poppyz-table-<?php echo $i;?>-text"><?php echo $students[$i]['tier']; ?></a></td>
                        <td data-label="<?php _e( 'Subscription Date' ,'poppyz'); ?>" title="" class="poppyz-table-text poppyz-table-<?php echo $i;?>-text"><?php echo date_i18n( 'F d Y H:i:s', strtotime( $students[$i]['subscription']['subscription_date'] ) ); ?></td>
                        <td class="sign-in-td student-text student-<?php echo $i;?>-text" data-label="<?php _e( 'Signed In' ,'poppyz'); ?>"> <?php echo  $students[$i]['subscription']['plan_status'] ; ?> </td>
                        <td data-label="<?php _e( 'Status' ,'poppyz'); ?>" class="student-status student-text student-<?php echo $i;?>-text"><?php echo $students[$i]['subscription']['status']; ?></td>
                        <td data-label="<?php _e( 'Actions' ,'poppyz'); ?>" class="poppyz-table-text poppyz-table-<?php echo $i;?>-text"><?php echo $students[$i]['subscription']['actions']; ?></td>
                    </tr>
                <?php endfor; ?>
                </tbody>
            </table>
        </form>

        <ul class="ppy-students-pagination">
            <?php echo $pagination_html; ?>
        </ul>
        <small><?php echo __('Items per page', 'poppyz'); ?></small>
        <input type="text" class="items-per-page" name="items-per-page" placeholder="" value="<?php echo Poppyz_Core::get_option('students_items_per_page') ?: 20  ?>" />
        <button><?php echo __('Save', 'poppyz'); ?></button>
    </div>
</div>   

<?php

$ajax_nonce = wp_create_nonce(  PPY_PREFIX . "ajax_subscription" );
$ppy_core = new Poppyz_Core();
$ppy_subs = new Poppyz_Subscription();
global $ppy_lang;
$courses = $ppy_core->get_courses();

?>
<script type="text/javascript">
    jQuery('.cancel').on('click', function () {
        return confirm("<?php _e( "This will cancel the user's subscription. Are you sure you want to continue?",'poppyz' ) ?>");
    });
    jQuery('.delete').on('click', function () {
        return confirm("<?php _e( "This will delete the user's subscription permanently. Are you sure you want to continue?" ,'poppyz') ?>");
    });
    jQuery('.subscribe').on('click', function () {
        return confirm("<?php _e( "This will manually mark the user as paid. Do you want to continue?",'poppyz' ) ?>");
    });
    jQuery('.confirm-refund').on('click', function () {
        return confirm("<?php _e( "This will revert the last payment of the user and cancel the subscription. Use this only if the user refunded his last payment. Continue?" ,'poppyz') ?>");
    });
    jQuery('.confirm-assign').on('click', function () {
        return confirm("<?php _e( "This will assign ALL currently displayed users to the selected tier. A database backup is strongly recommended. Continue?" ,'poppyz') ?>");
    });
    jQuery('.confirm-delete').on('click', function () {
        return confirm("<?php _e( "This will delete subscriptions of currently displayed users assigned to the selected tier. A database backup is strongly recommended. Continue?",'poppyz' ) ?>");
    });
    jQuery(document).ready(function($){
        $('.date-picker').datepicker({ dateFormat: 'dd-mm-yy' });

        //populate the tier based on the selected course
        $( document ).on( 'change', 'select.select-course', function() {
            var course_id = $(this).val();
            var data = {
                action: 'populate_tiers',
                security: '<?php echo $ajax_nonce; ?>',
                course_id: course_id
            };
            $.ajax ({
                type: 'POST',
                url: ajaxurl,
                data: data,
                success: function(response) {
                    $(".select-tier").empty().append(response);
                }
            });
        });

        $('.select-courses').each(function() {
            $courses = $(this); // memorize $(this)
            $tiers = $courses.next('select'); // find a sibling to $this.
            $courses.change(function($courses) {
                return function() {
                    var course_id = $courses.val();
                    var data = {
                        action: 'populate_tiers',
                        security: '<?php echo $ajax_nonce; ?>',
                        course_id: course_id
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            $tiers.empty().append(response);
                        }
                    });
                }
            }($courses));
        });
        $('.remind').click(function(e) {
            e.preventDefault();
            var id = $(this).attr("href");
            $('#reminder-form').fadeIn();
            $('#remind-id').val(id);
            return false;
        });
        $('#cancel-send').click(function(e) {
            e.preventDefault();
            $('#reminder-form').fadeOut();
            //location.reload();
        });

    
    });
</script>