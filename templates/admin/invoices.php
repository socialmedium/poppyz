<?php

global $ppy_lang;
$ppy_invoice = new Poppyz_Invoice();
Poppyz_Invoice::rename_duplicate_titles();
$invoice_list_table = new Poppyz_Invoice_List_Table();
$invoice_list_table->prepare_items();

if (isset($_GET['ppy-action']) && $_GET['ppy-action'] == 'edit-invoice') {
    $id = $_GET['invoice'];
    $invoice = $ppy_invoice->get_invoice($id); 
    $company = $ppy_invoice->get_invoice_field($id, 'company');
    if ( isset( $_GET['ppy-message'] ) ) {
        $message = $_GET['ppy-message'];
        if ( $message == 'invoice_updated' ) {
            $alert = '<div class="notice notice-success"><p>' . __( 'The invoice has been updated.' ,'poppyz') . '</p></div>';
        } else {
            $alert = '<div class="notice notice-error"><p>' . __( 'Please fill in all the required fields.' ,'poppyz')  . '</p></div>';
        }
    }
    $template_args = [
        'id ' => $id,
        'ppy_lang' => $ppy_lang,
        'product' => $ppy_invoice->get_invoice_field($id, 'product'),
        'amount' => Poppyz_Core::format_price($ppy_invoice->get_invoice_field($id, 'amount'), ''),
        'alert' => $alert,
        'firstname' => $ppy_invoice->get_invoice_field($id, 'firstname'),
        'lastname' => $ppy_invoice->get_invoice_field($id, 'lastname'),
        'address' => $ppy_invoice->get_invoice_field($id, 'address'),
        'company'=>$ppy_invoice->get_invoice_field($id, 'company'),
        'zipcode' => $ppy_invoice->get_invoice_field($id, 'zipcode'),
        'city' => $ppy_invoice->get_invoice_field($id, 'city'),
        'country' => $ppy_invoice->get_invoice_field($id, 'country'),
        'date_created' => $ppy_invoice->get_invoice_field($id, 'date_created'),
        'date_expired' => $ppy_invoice->get_invoice_field($id, 'date_expired')
    ];
    echo Poppyz_Template::get_template('admin/invoices-form.php', $template_args);

} elseif (isset($_GET['ppy-action']) && $_GET['ppy-action'] == 'add-invoice') {
    $template_args = [
        'id' => '',
        'ppy_lang' => $ppy_lang,
        'product' => '',
        'amount' => '',
        'alert' => '',
        'firstname' =>'',
        'lastname' => '',
        'address' => '',
        'company' => '',
        'zipcode' => '',
        'city' => '',
        'country' =>'',
        'date_created' => '',
        'date_expired' => ''

    ];
    echo Poppyz_Template::get_template('admin/invoices-form.php', $template_args);

} else { ?>
<div class="wrap white">
  
    <?php if (isset($_GET['ppy-message']) && $_GET['ppy-message'] == 'invoice_added') echo '<div class="notice notice-success"><p>' . __('Invoice successfully added.','poppyz') . ' </p></div>' ?>
        <form class="poppyz-form" id="ppy-invoice" method="get" action="">

            <?php $invoice_list_table->search_invoice(__('Search','poppyz') , 'search-invoice'); ?>

            <input type="hidden" name="page" value="poppyz-invoices" />
            <input type="hidden" name="bulk" value="1" />
            <?php $invoice_list_table->views() ?>
            <?php $invoice_list_table->display() ?>
        </form>
    <br />
    <p><a href="<?php echo esc_url(wp_nonce_url(add_query_arg(array('ppy-action' => 'generate-pdf', 'invoice' => 0, 'download' => 1)), 'ppy-nonce')); ?>">
            <?php echo __('Download all invoices as PDF','poppyz'); ?>
        </a>
    </p>
    <p><a href="<?php echo esc_url(wp_nonce_url(add_query_arg(array('ppy-action' => 'export-xml', 'invoice' => 0, 'download' => 1)), 'ppy-nonce')); ?>">
            <?php echo __('Download all invoices as XML','poppyz'); ?>
        </a>
    </p>
    <p><a href="<?php echo esc_url(wp_nonce_url(add_query_arg(array('ppy-action' => 'export-csv', 'invoice' => 0, 'download' => 1)), 'ppy-nonce')); ?>">
            <?php echo __('Download all invoices as CSV','poppyz'); ?>
    </a>
    </p>

    <form id="filter-earnings" method="get" action="<?php echo admin_url('admin.php'); ?>">
        <fieldset id="future-payments">
            <?php
                    $tier = null;
                    if (isset($_GET['filter-earnings'])) {
                        if ($_GET['select-tier'] != '-1') {
                            $tier = $_GET['select-tier'];
                            echo '<div class="update-nag">' . __('Showing totals from tier: ','poppyz') . get_the_title($tier) . '</div>';
                        } else {
                            echo '<div class="update-nag">' . __('Please select a tier','poppyz') . '</div>';
                        }
                    }


                   // $remaining_payments = Poppyz_Statistics::get_remaining_payments( strtotime(date('Y-m-d')), $date_in_12_months, "all", true );
//                    $total_statistics = Poppyz_Statistics::get_total_statistics(false, false, true, false, "total_only");
//                    $total_earnings = $total_statistics['total_paid_payments'];
                    $date_in_12_months = date('Y-m-d', strtotime('+12 month', strtotime(date('Y-m-d'))));

                    $remaining_payments = Poppyz_Statistics::get_remaining_payments(strtotime(date('Y-m-d')), $date_in_12_months, "all", true);

            ?>
            <h3>
                <?php echo __('Total Earnings: ','poppyz'); ?> <span id="invoice_total_earnings"></span>
            </h3>
            <h3>
                <?php  echo __('Outstanding payments: ', 'poppyz') . '<span>' . $remaining_payments['total_amount'].'</span>';?>
            </h3>
            <!--<input type="hidden" name="filter-earnings" />
                <input type="hidden" name="page" value="poppyz-invoices" />
                -->
            <?php /*Poppyz_Admin::subscription_form(false, true); */?>
        </fieldset>
    </form>

    <style>
        #future-payments .date-picker {
            display: none;
        }
    </style>

    <script>
        jQuery(document).ready(function ($) {
            $('.date-picker').datepicker({ dateFormat: 'yy-mm-dd' });
        });
    </script>
</div>
<?php } ?>