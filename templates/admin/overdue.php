<?php
global $ppy_lang;
$ppy_core = new Poppyz_Core();
$ppy_invoice = new Poppyz_Invoice();
$ajax_nonce = wp_create_nonce(  PPY_PREFIX . "statistics" );
$ajax_nonce2 = wp_create_nonce(  PPY_PREFIX . "ajax_subscription" );
$courses = Poppyz_Core::get_courses();
$current_url = get_admin_url() . 'admin.php';


if ( isset( $_REQUEST['action'] ) ) {
	$action = strtolower( $_REQUEST['action'] );
	$data = array();
	if ( $action == 'remind' ) {
		if ( $_POST['remind-subject'] ) {
			$data['email_subject']  = $_POST['remind-subject'];
		}
		if ( $_POST['remind-message'] ) {
			$data['email_content']  = $_POST['remind-message'];
		}
	}
	$message =  Poppyz_Subscription::update_subscription( $action, $_REQUEST['id'], $data );
}


?>
<div class="wrap white ppy-one-column" id="overdue-page">

    <?php

        $date = new DateTime();
        $date_now = $date->format( get_option('date_format') );
        $month = $date->format( 'F' );
        $this_month = $date->format( 'm' );
        $year = $date->format( 'Y' );
        $last_year =  (int)$year - 1;
        $last_month = (int)$this_month - 1;
        $next_month = (int)$this_month + 1;

        //get last year
        $ly_start = mktime(0, 0, 0, 1, 1, $last_year);
        $ly_end = mktime(0, 0, 0, 12, 31, $last_year);
        $ly_start_date  = date( 'Y-m-d 00:00:00', $ly_start );
        $ly_end_date  = date( 'Y-m-d 12:59:59', $ly_end );

        //get last month

        //if current month is January so last month is 12
        if ( $last_month == 0  ) {
            $last_month = 12;
            $year = $year - 1;
        }

        //if current month is December so next month is 1
        if ( $next_month == 13  ) {
            $next_month = 1;
            $year = $year + 1;
        }

        //get last month
        $lm_start = mktime(0, 0, 0, $last_month, 1, $year);
        $lm_end = mktime(0, 0, 0, $last_month, cal_days_in_month(CAL_GREGORIAN, $last_month, $year), $year);
        $lm_start_date  = date( 'Y-m-d 00:00:00', $lm_start );
        $lm_end_date  = date( 'Y-m-d 12:59:59', $lm_end );

        //get this year
        $ty_start = mktime(0, 0, 0, 1, 1, $year);
        $ty_end = mktime(0, 0, 0, 12, 31, $year);
        $ty_start_date  = date( 'Y-m-d 00:00:00', $ty_start );
        $ty_end_date  = date( 'Y-m-d 12:59:59', $ty_end );

        //get this month
        $tm_start = mktime(0, 0, 0, $this_month, 1, $year);
        $tm_end = mktime(0, 0, 0, $this_month, cal_days_in_month(CAL_GREGORIAN, $this_month, $year), $year);
        $tm_start_date  = date( 'Y-m-d 00:00:00', $tm_start );
        $tm_end_date  = date( 'Y-m-d 12:59:59', $tm_end );

        $start_date = ( !empty( $_GET['start-date'] ) )  ? $_GET['start-date'] . ' 00:00:00' : null;
        $end_date = ( !empty( $_GET['end-date'] ) )  ? $_GET['end-date'] . ' 12:59:59' : null;


        //for future payments

        //get up to next month
        $nm_start = mktime(0, 0, 0, $next_month, 1, $year);
        $nm_end = mktime(0, 0, 0, $next_month, 31, $year);
        $nm_start_date  = date( 'Y-m-d 00:00:00', $nm_start );
        $nm_end_date  = date( 'Y-m-d 12:59:59', $nm_end );

        //get up to next year
        $ny_start = mktime(0, 0, 0, $this_month, 1, $year);
        $ny_start_date  = date( 'Y-m-d 00:00:00', $ny_start );
        $ny_end_date = date("Y-m-d 12:59:59", strtotime(date("Y-m-d", $ny_start) . " + 1 year" ) );

        $period = '';
        if (  isset( $_GET['period']  ) ) {
            $period =  $_GET['period'];
            if ( $period == 'last_year' ) {
                $start_date =  $ly_start_date;
                $end_date =  $ly_end_date;
            } elseif ( $period == 'this_year' ) {
                $start_date =  $ty_start_date;
                $end_date =  $ty_end_date;
            } elseif ( $period == 'this_month' ) {
                $start_date =  $tm_start_date;
                $end_date =  $tm_end_date;
            } elseif ( $period == 'last_month') {
                $start_date =  $lm_start_date;
                $end_date =  $lm_end_date;
            } elseif ( $period == 'next_month') {
                $start_date =  $nm_start_date;
                $end_date =  $nm_end_date;
            }
        }
        ?>

    <div id="ppy-form-settings" class="main-box poppyz-form" >
		<?php if ( !empty( $message ) ) : ?>
            <div id="message" class="poppyz-notice notice-success below-h2 dashicons-before">
                <p><?php echo $message; ?></p>
            </div>
		<?php endif; ?>
        <div class="white-box" id="overdue">

                <?php
                if ( isset( $_GET['future-period'] ) ) {
                    $period = $_GET['future-period'];
                    if ( $period == 'next_year' ) {
                        $start_date =  $ny_start_date;
                        $end_date =  $ny_end_date;
                    } else {
                        $start_date =  $tm_start_date;
                        $end_date =  $tm_end_date;
                    }
                } else {
                    $start_date = null;
                    $end_date = null;
                }
                $tier = null;
                if ( isset( $_GET['select-tier'] ) ) {
                    if ( $_GET['select-tier'] != '-1' ) {
                        $tier = $_GET['select-tier'];
                        echo '<div class="update-nag">' . __('Total for: ', 'poppyz') . get_the_title( $tier ) .'</div>';
                    } else {
                        echo '<div class="update-nag">' . __('Please select a tier', 'poppyz') .'</div>';
                    }
                }
                $status = (!empty($_GET['status'])) ? $_GET['status'] : null;
				$date_last_12_months = date('Y-m-d', strtotime('-12 month', strtotime(date('Y-m-d'))));
				$from_date = ( isset( $_GET['from_date'] ) && empty( $_GET['reset'] ) ) ? strtotime($_GET['from_date']) : strtotime($date_last_12_months);
				$to_date = ( isset( $_GET['to_date'] ) && empty( $_GET['reset'] ) ) ? strtotime($_GET['to_date']) : strtotime(date('Y-m-d'));
				$view_all = ( isset( $_GET['view_all'] ) ) ? true : false;
				$remaining_payments = Poppyz_Core::get_overdue_list( $from_date, $to_date, $view_all );

                ?>
                <div class="container">
                    <form method="get" action="<?php echo $current_url . '?page=poppyz-overdue'; ?>" id="student-overdue-form">
                        <?php if (!empty($period)) : ?>
                            <input type="hidden" name="future-period" value="<?php echo $period; ?>" />
                        <?php endif; ?>
                        <div class="pr-filter-wrapper">
                            <input type="hidden" name="page" value="poppyz-overdue" />

                            <div class="field-wrapper">
                                <input type="text" id="ppy-from-date" name="from_date" class="date-picker-overdue" placeholder="From" value="<?php echo (empty($view_all))?date("Y-m-d",$from_date):""; ?>">
                            </div>
                            <div class="field-wrapper">
                                <input type="text" id="ppy-to-date" name="to_date" class="date-picker-overdue" placeholder="To" value="<?php echo (empty($view_all))?date("Y-m-d",$to_date):""; ?>">
                            </div>
                            <div class="field-wrapper">
                                <input type="submit" id="overdue-filter" class="ppy_button" name="filter" value="Apply" >
                            </div>
                            <div class="field-wrapper">
                                <input type="submit" id="overdue-filter" class="ppy_button" name="reset" value="Reset" >
                            </div>
                            <div class="field-wrapper">
                                <input type="submit" id="overdue-filter" class="ppy_button" name="view_all" value="View All" >
                            </div>
                            <div id="load-warning" class="field-wrapper">
                                <?php echo __("This will take time to load.");?>
                            </div>
                        </div>



                        <script>
                            jQuery(document).ready(function($){
                                $('.date-picker-range').datepicker({
                                    dateFormat: 'yy-mm-dd',
                                    minDate: 0
                                });

                            });
                        </script>
                    </form>


                    <table class="widefat ppy-admin-table" id="overdue-list">
						<?php
						$table_html =  '<thead><tr>';
						$table_html .=  '<th>' . __( 'Due Date' , 'poppyz'). '</th>';
						$table_html .=  '<th>' . __( 'Name (username)' , 'poppyz'). '</th>';
						$table_html .=  '<th>' . __( 'Course' , 'poppyz') . '</th>';
						$table_html .=  '<th>' . __( 'Tier'  , 'poppyz') . '</th>';
						$table_html .=  '<th>' . __( 'Amount' , 'poppyz') . '</th>';
						$table_html .=  '<th>' . __( 'Interval' , 'poppyz') . '</th>';
						$table_html .=  '<th></th>';
						$table_html .=  '</tr></thead>';
						echo $table_html;
						?>
						<?php
                            echo $remaining_payments['table_data'];
                         ?>
                    </table>

                    <form class="poppyz-form poppyz-popup" id="reminder-form" method="post" action="" style="display: none;">

						<?php
						$reminder_email_subject = Poppyz_Core::get_option( 'overdue_email_subject' );
						$reminder_email_message = Poppyz_Core::get_option( 'overdue_email_message' );
						?>
                        <h3><?php _e( 'Reminder Mail' );   ?></h3>
                        <table class="form-table">
                            <tr>
                                <th>
                                    <label for="remind-subject"><?php _e( 'Email Subject' ,'poppyz'); ?></label>
                                </th>
                                <td>
                                    <input type="text" name="remind-subject" id="remind-subject" value="<?php echo $reminder_email_subject; ?>"  /><br/>
                                </td>
                            </tr>
							<?php
							$settings = array(
								'teeny' => true,
								'textarea_rows' => 25,
								'tabindex' => 1,
								'textarea_name' => 'remind-message'
							);
							?>
                            <tr>
                                <th>
                                    <label for="email-form-name"><?php _e( 'Email Message:' ,'poppyz'); ?></label>
                                </th>
                                <td>
									<?php wp_editor( $reminder_email_message, 'remind-message', $settings); ?>
                                    <small><?php _e( 'Shortcodes: [due-date], [account-link]' ,'poppyz'); ?></small>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="2"><input class="button button-primary button-large" type="submit" value="<?php _e( 'Submit' ); ?>" /> &nbsp; <button id="cancel-send" class="button button-large"><?php _e( 'Cancel' ); ?></button></td>
                            </tr>
                        </table>
                        <input type="hidden" name="action" value="remind" />
                        <input type="hidden" id="remind-id" name="id" value="" />
                    </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($) {
        $('#overdue-filter').click(function(){
            $("#load-warning").fadeIn();
        });

        /*$(window).on("throttledresize", function (event) {
            initChart();
        });*/

        $(window).on("throttledresize", function (event) {
            initChart();
        });

        if(window.location.hash) {
            $(window.location.hash).click();
        }

        $('.date-picker-overdue').datepicker({
            dateFormat: 'yy-mm-dd',
        });
        $('#filter_subscription').val("<?php echo __( 'Show', 'poppyz') ?>");
        $('#filter_subscription').attr("name", "");


        //populate the tier based on the selected course
        $( document ).on( 'change', 'select.select-course', function() {
            var course_id = $(this).val();
            var data = {
                action: 'populate_tiers',
                security: '<?php echo $ajax_nonce2; ?>',
                course_id: course_id
            };
            $.ajax ({
                type: 'POST',
                url: ajaxurl,
                data: data,
                success: function(response) {
                    $(".select-tier").empty().append(response);
                }
            });
        });

        $('.select-courses').each(function() {
            $courses = $(this); // memorize $(this)
            $tiers = $courses.next('select'); // find a sibling to $this.
            $courses.change(function($courses) {
                return function() {
                    var course_id = $courses.val();
                    var data = {
                        action: 'populate_tiers',
                        security: '<?php echo $ajax_nonce2; ?>',
                        course_id: course_id
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            $tiers.empty().append(response);
                        }
                    });
                }
            }($courses));
        });

        $('.remind').click(function(e) {
            e.preventDefault();
            var id = $(this).attr("href");
            $('#reminder-form').fadeIn();
            $('#remind-id').val(id);
            return false;
        });
        $('#cancel-send').click(function(e) {
            e.preventDefault();
            $('#reminder-form').fadeOut();
            //location.reload();
        });

        $('#overdue-list').dataTable({
            searching: true,
            order: [],
            deferRender:    true,
            scroller:       true,
            dom: 'rtip',
            columnDefs: [
                { "orderable": false, "targets": 6 }
            ],
            "oLanguage": {
                "sSearch": "",
            },
            language: {
                searchPlaceholder: '<?php _e("Search"); ?>'
            }
        });


    });
</script>