<div class="wrap">

    <h2><?php echo __( 'Edit Invoice' ,'poppyz');  ?></h2>
    <form id="ppy-invoice-update" method="post" action="">
        <?php echo $alert; ?>
        <h3><?php echo __( 'Edit invoice for: ' ,'poppyz'); ?></h3>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-company"><?php echo __( 'Product' ,'poppyz');  ?></label>
                </th>
                <td>
                    <input name="product" id="ppy-product" type="text" value="<?php echo $product; ?>" style="width: 300px;"/>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-amount"><?php echo __( 'Amount' ,'poppyz');  ?></label>
                </th>
                <td>
                    <input name="amount" id="ppy-amount" type="text" value="" style="width: 300px;"/>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-company"><?php echo __( 'Company','poppyz');  ?></label>
                </th>
                <td>
                    <input name="company" id="ppy-company" type="text" value="<?php echo $company; ?>" style="width: 300px;"/>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-firstname"><?php echo __( 'Firstname' ,'poppyz'); ?></label>
                </th>
                <td>
                    <input name="firstname" id="ppy-firstname" type="text" value="<?php echo $firstname; ?>" style="width: 300px;"/>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-lastname"><?php echo __( 'Lastname' ,'poppyz');  ?></label>
                </th>
                <td>
                    <input name="lastname" id="ppy-lastname" type="text" value="<?php echo $lastname; ?>" style="width: 300px;"/>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-address"><?php echo __( 'Address' ,'poppyz');  ?></label>
                </th>
                <td>
                    <input name="address" id="ppy-address" type="text" value="<?php echo $address; ?>" style="width: 300px;"/>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-zipcode"><?php echo __( 'Zipcode' ,'poppyz');  ?></label>
                </th>
                <td>
                    <input name="zipcode" id="ppy-zipcode" type="text" value="<?php echo $zipcode; ?>" style="width: 300px;"/>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-city"><?php echo __( 'City' ,'poppyz'); ?></label>
                </th>
                <td>
                    <input name="city" id="ppy-city" type="text" value="<?php echo $city; ?>" style="width: 300px;"/>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-country"><?php echo __( 'Country' ,'poppyz');  ?></label>
                </th>
                <td>
                    <?php echo Poppyz_Core::country_select( 'country', $country );  ?>
                </td>
            </tr>

            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-start-date"><?php echo __( 'Date Created' ,'poppyz');  ?></label>
                </th>
                <td>
                    <input type="text" id="ppy-date-created" name="date_created" value="<?php echo $date_created; ?>" style="width: 300px;" class="date-picker" />
                    <p class="description"><?php echo __( 'Leave blank if you want to use the current date today' ,'poppyz');  ?></p>
                </td>
            </tr>
            <tr>
                <th scope="row" valign="top">
                    <label for="ppy-date-expired"><?php echo __( 'Date Expires' ,'poppyz'); ?></label>
                </th>
                <td>
                    <input type="text" id="ppy-date-expired" name="date_expired" value="<?php echo $date_expired; ?>" style="width: 300px; "  class="date-picker" />
                    <p class="description"><?php echo __( 'The date that the invoice should be paid and will expire.' ,'poppyz');  ?></p>
                </td>
            </tr>
            </tbody>
        </table>
        <p class="submit">
            <?php wp_nonce_field('ppy-nonce'); ?>
            <input type="hidden" name="ppy-action" value="update-invoice" />
            <input type="hidden" name="id" value="<?php echo $id; ?>" />
            <input type="submit" value="<?php echo __( 'Update' ,'poppyz'); ?>" class="button-primary"/>
        </p>
    </form>
    <script>
        jQuery(document).ready(function($) {
            $('.date-picker').datepicker({dateFormat: 'dd-mm-yy'});
        });
    </script>
</div>
