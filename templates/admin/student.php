<?php
    $user_id = $_GET['student-id'];
    $user = get_userdata( $user_id );
    if (!$user) {
        die('User not found');
    }
    $ppy_subs = new Poppyz_Subscription();
    $ppy_payment = new Poppyz_Payment();

    $subscriptions = $ppy_subs->get_subscriptions_by_user( $user_id, 'all', 'standard' );
    $planned_subscriptions = $ppy_subs->get_planned_subscriptions_by_user( $user_id );
    $membership_subscriptions = $ppy_subs->get_membership_subscriptions_by_user( $user_id );
    $hide_invoices = apply_filters( 'ppy_hide_invoices', false, $user_id );

    global $ppy_lang;

    $current_url = '?page=poppyz-students&student-id=' . $user_id;
?>
<div class="wrap white">
    <h2><?php echo $user->display_name; ?></h2>

    <?php
        $content = '';
        if ( $planned_subscriptions ) {
            $content .= '<div class="subscription">';
            $content .= '<h3>' . __('Payment Plans','poppyz') . '</h3>';
            $content .= '<div class="table-wrapper">';
            $content .= '<table class="subscription-list ppy-table">';
            $content .= '<thead>';
            $content .= '<tr>';
            $content .= '<th>' . __('Course','poppyz') . '</th>';
            $content .= '<th>' . __('Tier','poppyz') . '</th>';
            $content .= '<th>' . __('Payments','poppyz') . '</th>';
            $content .= '<th>' . __('Invoices','poppyz') . '</th>';
            $content .= '</tr>';
            $content .= '</thead>';
            $content .= '<tbody>';
            foreach ($planned_subscriptions as $subs) {

                $subscription_date = $subs->subscription_date;

                $number_of_payments = $subs->tier_number_of_payments;

                $payments_made = $subs->payments_made;

                if ($payments_made == 0 && $subs->payment_status != 'admin' && $subs->payment_type != 'manual') {
                    continue; //if 0 payment and not added by admin, it means this payment is pending and should not appear.
                }

                $payments_left = $number_of_payments - $payments_made;

                $payment_frequency = $subs->payment_frequency;
                $payment_timeframe = $subs->payment_timeframe;
                if (empty($payment_timeframe)) {
                    $payment_timeframe = 'months';
                }

                if ($payments_left > 0 && $subs->payment_status != 'admin') {
                    $due_date = strtotime($subscription_date . ' + ' . $payment_frequency * ($number_of_payments - 1) . " $payment_timeframe");
                    $status = (time() > $due_date) ? '<br /><strong class="overdue">' . __('Overdue','poppyz') . '</strong>' : '';
                } else {
                $status = '<br />' . __('Fully Paid', 'poppyz');
                }

                $invoices_html = [];
                $invoice = [];

                if (!$hide_invoices) {
                    $invoices = Poppyz_Invoice::get_subscription_invoices($subs->id);
                } else {
                    $invoices = false;
                }

                if ($invoices) {
                    foreach ($invoices as $invoice) {
                        if (isset($invoice->invoice_id)) { //this is from the payments table and not from the custom post type invoice id
                            $invoice_id = $invoice->invoice_id;
                        } else {
                            $invoice_id = $invoice->ID;
                        }
                        $payment_data = $ppy_payment->get_payment_by_invoice($invoice_id);
                        $title = get_the_title($invoice_id);
                        if ($payment_data) {
                            $is_chargedback = ($payment_data->status == 'chargedback') ? ' - ' . __('Chargedback','poppyz') : '';
                            $invoice_payments_made = $payment_data->number_of_payment;

                            if ($payment_data->invoice_type == 'moneybird') {
                                $title = $payment_data->invoice_id_other;
                            }
                        } else {
                            $invoice_payments_made = get_post_meta($invoice_id, PPY_PREFIX . 'invoice_payments_made', true);
                            $is_chargedback = Poppyz_Invoice::has_credit_invoice($subs->id, $invoice_payments_made) ? ' - ' . __('Chargedback') : '';
                        }
                        $invoices_html[$invoice_payments_made] = '<a href="' . site_url('?process_invoice=' . $invoice_id) . '">' . $title . '</a>' . $is_chargedback;
                        if ($payment_data && $payment_data->invoice_type == 'plugandpay') {
                            $invoices_html[$invoice_payments_made] = __('Mailed via Plug&Pay','poppyz');
                        }
                    }
                }
                $is_automatic = get_post_meta($subs->tier_id, PPY_PREFIX . 'payment_automatic', true);
                if ($subs->payment_type == 'automatic' || $subs->payment_type == '') {
                    $is_automatic = true;
                }
                if ($subs->payment_type == 'manual') {
                    $is_automatic = false;
                }
                $content .= "<tr>";
                $content .= '<td><a href="' . get_permalink($subs->course_id) . '">' . get_the_title($subs->course_id) . '<br /></a><small>' . date_i18n(get_option('date_format'), strtotime($subs->subscription_date)) . '</small></td>';
                $content .= '<td>' . get_the_title($subs->tier_id) . $status . '</td>';

                $content .= '<td>';
                if ($subs->payment_status == 'cancelled') {
                    $content .= '<span class="red dashicons-before dashicons-no-alt">' . __('Cancelled','poppyz') . '</span>';
                } elseif ($subs->payment_status == 'admin') {
                    $content .= '<span class="green dashicons-before dashicons-saved">' . __('Subscribed by the admin','poppyz') . '</span><br />';
                } else {
                    $has_pay_button = false; //only limit the pay button to 1 instance

                    if ($subs->version == 2) {
                        $insufficient_funds = $ppy_payment->get_insufficient_funds_payments($subs->id);
                        // $number_of_payments = $number_of_payments + $insufficient_funds;
                    }
                    // var_dump($number_of_payments);
                    $invoice_text = '';
                    for ($i = 0; $i < $number_of_payments; $i++) {
                        $frequency = $payment_frequency * $i;
                        $due_date = date_i18n(get_option('date_format'), strtotime($subscription_date . ' + ' . $frequency . ' ' . $payment_timeframe));
                        $payment_data = null;

                        if ($subs->version == 2) {
                            $payment_data = $ppy_payment->get_payment_by_number($subs->id, $i + 1);
                            $payment_needed = $payment_data ? $payment_data->status != 'paid' && $payment_data->status != 'chargeback-paid' : true;
                            $x = true;
                        } else {
                            $payment_needed = $i == $payments_made;
                            $x = $i <= $payments_made - 1;
                        }

                        if ($x && !$payment_needed) {
                            $due_date_status = ' - <span class="green dashicons-before dashicons-saved">' . __('Paid','poppyz'). '</span>';
                            if (!$hide_invoices) {
                                $invoice_text .= ($invoices && isset($invoices_html[$i + 1])) ? $invoices_html[$i + 1] : ' - ' . __('Deleted','poppyz');
                                $invoice_text .= '<br />';
                                //$due_date_status .= $invoice_text;
                            }
                        } else {
                            $due_date_status = ' - <span class="gray dashicons-before dashicons-clock">' . __('Remaining payment','poppyz') . '</span>';

                            $due_date_status .= '<a href="' . $subs->id . '" class="add_payment et_pb_button">' . __('Add payment','poppyz') . '</a>';

                            $content .= $due_date . $due_date_status . '<br />';
                        }
                        if ($number_of_payments != $payments_made) { //not fully paid yet
                            //$content .= '<a href="?cancel_pending=' . $subs->id . '">' . __( 'Cancel' ) . '</a>';
                        }
                    }
                    $content .= '</td>';
                    $content .= '<td class="top">';
                    $content .= $invoice_text;
                    $content .= '</td>';


                    $content .= "</tr>";
                }
                $content .= '</tbody>';
                $content .= '</table>';
                $content .= '</div>';
                $content .= '</div>';
            }
            echo $content;
        }
    ?>
    <form id="add-payment-form" method="post" action="<?php echo $current_url; ?>">
        <input type="hidden" id="payment-subs-id" name="payment-subs-id" value="0" />
        <input type="hidden" id="add-payment" name="add-payment" value="1" />
        <input type="text" name="payment_id" placeholder="<?php echo __('Payment ID', 'poppyz'); ?>" />
        <input type="text" name="payment_id" placeholder="<?php echo __('Amount', 'poppyz'); ?>" />
        <input type="text" name="payment_number" placeholder="<?php  echo __('Payment Number (optional)', 'poppyz'); ?>" />&nbsp;&nbsp;&nbsp;
        <input type="checkbox" name="create_invoice" checked="checked"> Create invoice? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="submit" class="button btn button-large" value="Record Payment">
    </form>
    <script>
        $('.add_payment').click(function(e) {
            e.preventDefault();
            var id = $(this).attr("href");
            $('#add-payment').slideToggle();
            $('#payment-subs-id').val(id);
            return false;
        });
    </script>
</div>