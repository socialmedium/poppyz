<?php
 global $ppy_lang;
 require_once (PPY_DIR_PATH . 'admin/class-poppyz-coupon-table.php');
 $ppy_coupon = new Poppyz_Coupon();
 //Create an instance of our package class...
 $coupon_list_table = new Poppyz_Coupon_List_Table();
 //Fetch, prepare, sort, and filter our data...
 $coupon_list_table->prepare_items();
 $alert ="";
  if ( isset( $_GET['ppy-action'] ) && $_GET['ppy-action'] == 'edit-coupon' ) { 
        $id = $_GET['coupon'];
        if ( isset( $_GET['ppy-message'] ) ) {
            $message = $_GET['ppy-message'];
            if ( $message == 'coupon_updated' ) {
                $alert .= '<div class="poppyz-notice notice-success dashicons-before">' . __( 'The coupon has been updated.' ,'poppyz') . '</div>';
            } elseif ( $message == 'coupon_exists' ) {
                $alert .= '<div class="poppyz-notice notice-error dashicons-before">' . __( 'The coupon code already exists. Please create another one.' ,'poppyz') . '</div>';
            } elseif ($message == 'missing_fields' ) {
                $alert .=  '<div class="poppyz-notice notice-error dashicons-before">' . __( 'Please fill in all the required fields.','poppyz'). '</div>';
            } elseif ($message == 'wrong_amount' ) {
                $alert .=  '<div class="poppyz-notice notice-error dashicons-before">' . __( 'Please make sure that you enter only numbers in the amount field.' ,'poppyz'). '</div>';
            }
        }
     $coupon = $ppy_coupon->get_coupon( $id ); 
     $template_args = [
        'id' =>  $id,
        'title' => __( 'Edit Coupon' ,'poppyz'),
        'ppy_lang' => $ppy_lang,
        'alert' => $alert,
        'name' => isset($coupon->post_title) ? $coupon->post_title  : '',
        'code' => $ppy_coupon->get_coupon_code( $id ),
        'type'=>  $ppy_coupon->get_coupon_type( $id ),
        'amount'=>$ppy_coupon->get_coupon_amount( $id ),
        'start_date'=>$ppy_coupon->get_coupon_start_date( $id ),
        'end_date'=>$ppy_coupon->get_coupon_end_date( $id ),
        'tier_ids' => $ppy_coupon->get_coupon_tiers( $id ),
        'usage_limit' =>$ppy_coupon->get_coupon_usage_limit( $id ),
     ];
    echo Poppyz_Template::get_template('admin/coupon-form.php',$template_args);
        
 } elseif(  isset( $_GET['ppy-action'] ) && $_GET['ppy-action'] == 'add-coupon' ) 
 {  
    if ( isset( $_GET['ppy-message'] ) ) {
        $message = $_GET['ppy-message'];
        if ( $message == 'coupon_add_failed' ) {
            $alert .= '<div class="notice notice-error"><p>' . __( 'Please fill in all the required fields.' ,'poppyz'). '</p></div>';
        } elseif ( $message == 'coupon_exists' ) {
            $alert .=  '<div class="notice notice-error"><p>' . __( 'The coupon code already exists. Please create another one.' ,'poppyz'). '</p></div>';
        }
    }
    $template_args = [
        'id' =>'',
        'title' => __( 'Add Coupon','poppyz'),
        'ppy_lang' => $ppy_lang,
        'alert' => '',
        'name' => '',
        'code' => '',
        'type'=> '',
        'amount'=> '',
        'start_date'=> '',
        'end_date'=> '',
        'tier_ids' => '',
        'usage_limit' => '',

    ];
    echo Poppyz_Template::get_template('admin/coupon-form.php',$template_args);

 } else { ?>
<div class="wrap white">
<!--    <h2>--><?php //echo __('Coupons', 'poppyz'); ?><!--<a href="--><?php //echo esc_url(add_query_arg(array('ppy-action' => 'add-coupon'))); ?><!--" class="add-new-h2 ppy_button">--><?php //echo __('Add New', 'poppyz'); ?><!--</a></h2>-->
    <form class="poppyz-form" id="ppy-coupon" method="post" action="">
        <input type="hidden" name="page" value="poppyz-coupons" />
        <input type="hidden" name="bulk" value="1" />
        <?php $coupon_list_table->views() ?>
        <?php $coupon_list_table->display() ?>
    </form>
</div>
<?php } ?>

<?php 

if ( isset($id) ) {
    $coupon_users = $ppy_coupon->get_coupon_users( $id );
    $coupon_user_rows = "";
    
    if ( !empty($coupon_users) ) {
    foreach ( $coupon_users as $coupon_user ) {
        $user_data = get_user_by("id", $coupon_user);
		if ( isset($user_data->data->user_login) ) {
			$user_column = sprintf('<td >%s</td><td >%s</td>', $user_data->data->display_name, $user_data->data->user_login);
			$coupon_user_rows .= sprintf('<tr>%s</tr>', $user_column);
		}
    }
?>
    <div class="wrap white">
        <h2><?php echo __('Coupon Users', 'poppyz'); ?></h2>
        <div class="main-box">
            <table class="wp-list-table widefat fixed striped table-view-list coupons">
                <thead>
                    <tr>
                        <th scope="col" id="name" class="column-name column-primary ">
                            <?php echo __('Name', 'poppyz'); ?>
                        </th>
                        <th scope="col" id="email" class="column-email">
                            <?php echo __('Email', 'poppyz'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody id="the-list">
                    <?php echo $coupon_user_rows;?>
                </tbody>
            </table>
        </div>
    </div>
<?php 
    }
}
?>