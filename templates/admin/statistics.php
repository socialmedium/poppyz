<?php
global $ppy_lang;
$ppy_core = new Poppyz_Core();
$ppy_invoice = new Poppyz_Invoice();
$ajax_nonce = wp_create_nonce(  PPY_PREFIX . "statistics" );
$ajax_nonce2 = wp_create_nonce(  PPY_PREFIX . "ajax_subscription" );
$courses = Poppyz_Core::get_courses();
$current_url = get_admin_url() . 'admin.php';

if ( isset( $_REQUEST['action'] ) ) {
	$action = strtolower( $_REQUEST['action'] );
	$data = array();
	if ( $action == 'remind' ) {
		if ( $_POST['remind-subject'] ) {
			$data['email_subject']  = $_POST['remind-subject'];
		}
		if ( $_POST['remind-message'] ) {
			$data['email_content']  = $_POST['remind-message'];
		}
	}
	$message =  Poppyz_Subscription::update_subscription( $action, $_REQUEST['id'], $data );
}
?>
<div class="wrap white ppy-one-column" id="statistics-page">

    <?php /*echo '<select name="select-course" class="select-course" >';
                echo '<option value="-1">' . __('Select course...') .  '</option>';
                //get select data
                if ( $courses->have_posts() ) {
                    while ( $courses->have_posts() ) {
                        $courses->the_post();
                        echo '<option value="' . get_the_ID() . '">' . get_the_title() . '</option>';
                    }
                }
                echo '</select>';

                echo '<select name="select-tier" class="select-tier">';
                echo '<option value="-1">' . __( 'None' ) . '</option>';
                echo '</select>';

        */?>

    <!--<input type="submit" onclick="location.href = 'admin.php?page=poppyz-statistics';return false;"
               id="get-sales" value="<?php /*echo __( 'Reset' ); */?>" />-->

    <?php

        $date = new DateTime();
        $date_now = $date->format( get_option('date_format') );
        $month = $date->format( 'F' );
        $this_month = $date->format( 'm' );
        $year = $date->format( 'Y' );
        $last_year =  (int)$year - 1;
        $last_month = (int)$this_month - 1;
        $next_month = (int)$this_month + 1;

        //get last year
        $ly_start = mktime(0, 0, 0, 1, 1, $last_year);
        $ly_end = mktime(0, 0, 0, 12, 31, $last_year);
        $ly_start_date  = date( 'Y-m-d 00:00:00', $ly_start );
        $ly_end_date  = date( 'Y-m-d 12:59:59', $ly_end );

        //get last month

        //if current month is January so last month is 12
        if ( $last_month == 0  ) {
            $last_month = 12;
            $year = $year - 1;
        }

        //if current month is December so next month is 1
        if ( $next_month == 13  ) {
            $next_month = 1;
			$nm_year = $year + 1;
        } else {
			$nm_year = $year;
		}

        //get last month
        $lm_start = mktime(0, 0, 0, $last_month, 1, $year);
        $lm_end = mktime(0, 0, 0, $last_month, cal_days_in_month(CAL_GREGORIAN, $last_month, $year), $year);
        $lm_start_date  = date( 'Y-m-d 00:00:00', $lm_start );
        $lm_end_date  = date( 'Y-m-d 12:59:59', $lm_end );

        //get this year
        $ty_start = mktime(0, 0, 0, 1, 1, $year);
        $ty_end = mktime(0, 0, 0, 12, 31, $year);
        $ty_start_date  = date( 'Y-m-d 00:00:00', $ty_start );
        $ty_end_date  = date( 'Y-m-d 12:59:59', $ty_end );

        //get this month
        $tm_start = mktime(0, 0, 0, $this_month, 1, $year);
        $tm_end = mktime(0, 0, 0, $this_month, cal_days_in_month(CAL_GREGORIAN, $this_month, $year), $year);
        $tm_start_date  = date( 'Y-m-d 00:00:00', $tm_start );
        $tm_end_date  = date( 'Y-m-d 12:59:59', $tm_end );

        $start_date = ( !empty( $_GET['start-date'] ) )  ? $_GET['start-date'] . ' 00:00:00' : null;
        $end_date = ( !empty( $_GET['end-date'] ) )  ? $_GET['end-date'] . ' 12:59:59' : null;


        //for future payments

        //get up to next month
        $nm_start = mktime(0, 0, 0, $next_month, 1, $nm_year);
        $nm_end = mktime(0, 0, 0, $next_month, 31, $nm_year);
        $nm_start_date  = date( 'Y-m-d 00:00:00', $nm_start );
        $nm_end_date  = date( 'Y-m-d 12:59:59', $nm_end );

        //get up to next year
        $ny_start = mktime(0, 0, 0, $this_month, 1, $year);
        $ny_start_date  = date( 'Y-m-d 00:00:00', $ny_start );
        $ny_end_date = date("Y-m-d 12:59:59", strtotime(date("Y-m-d", $ny_start) . " + 1 year" ) );
        $view_all = false;
        $period = '';
        if (  isset( $_GET['period']  ) ) {
            $period =  $_GET['period'];
            if ( $period == 'last_year' ) {
                $start_date =  $ly_start_date;
                $end_date =  $ly_end_date;
            } elseif ( $period == 'this_year' ) {
                $start_date =  $ty_start_date;
                $end_date =  $ty_end_date;
            } elseif ( $period == 'this_month' ) {
                $start_date =  $tm_start_date;
                $end_date =  $tm_end_date;
            } elseif ( $period == 'last_month') {
                $start_date =  $lm_start_date;
                $end_date =  $lm_end_date;
            } elseif ( $period == 'next_month') {
                $start_date =  $nm_start_date;
                $end_date =  $nm_end_date;
            } elseif ( empty($period) && empty($start_date) && empty($start_date) ) {
                $view_all = true;
            }
        } else {
			$view_all = true;
        }
        ?>

    <div id="ppy-form-settings" class="main-box poppyz-form" >
        <nav class="tabs">
            <ul class="ppy-vertical-nav-tabs">
                <li><a href="#sales-list" class="nav-tab dashicons-analytics sales-list"><?php echo __('Turnover', 'poppyz'); ?></a></li>
                <li><a href="#graph-period" class="nav-tab dashicons-chart-bar statistics-earnings-nav"><?php echo __('Earnings', 'poppyz'); ?></a></li>
                <li><a href="#overdue" class="nav-tab dashicons-chart-pie statistics-earnings-nav statistics-overdue-nav"><?php echo __('Overdue', 'poppyz'); ?></a></li>
                <li><a href="#sold-period" class="nav-tab dashicons-chart-pie"><?php echo __('Sold tiers', 'poppyz');?></a></li>
                <li><a href="#future-payments" id="future-payments-tab" class="nav-tab dashicons-money statistics-future-payments-nav"><?php echo __('Future payments', 'poppyz'); ?></a></li>
                <li><a href="#buyer-period" class="nav-tab dashicons-star-filled"><?php echo __('Top 5 Buyers', 'poppyz'); ?></a></li>
                <li><a href="#students-progress" class="nav-tab dashicons-yes-alt"><?php echo __('Complete Lessons', 'poppyz'); ?></a></li>
                <?php do_action( 'ppy_meta_box_tier_options' ); ?>
            </ul>
        </nav>
        <div class="white-box" id="sales-list">
            <div class="statistics_loading"><span class="statistics-loader"></span></div>
<!--			--><?php //if ( isset($_GET['statistics']) && $_GET['statistics'] == "sales-list" ) :?>
                <?php
                //$statistics = Poppyz_Statistics::get_total_statistics($start_date, $end_date, $view_all, false, "turnover" );
                $bar_data = array();
                $range_html = '';
                if ( $start_date ) {
                    $range_html .= date( get_option( 'date_format' ) , strtotime( $start_date ) ) . ' - ';
                }
                if ( $end_date ) {
                    $d = DateTime::createFromFormat( 'Y-m-d H:i:s', $end_date );
                    if ( $d ) {
                        $end_date = date( get_option( 'date_format' ), $d->getTimestamp() ) ;
                    }

                    $range_html .= $end_date;
                }
                $table_html = '';
                ?>
                <div class="ppy-settings-heading-wrapper">
                    <div class="ppy-settings-header">
                        <h2 class="statistics-h2"><?php echo __( 'Specified overview of turnover' , 'poppyz') ?></h2>
                    </div>
                    <div class="ppy-settings-header">
                        <div class="turnover-total"><span id="total_turnover"></span></div>
                    </div>
                </div>
                <div class="container relative">
                            <div class="filters">
                                <strong class="select-period"><?php echo __( 'Filter on' , 'poppyz') ?>: </strong>
                                <?php Poppyz_Statistics::filter_links( 'period', $period, 'filter-turnover' ); ?>
                                <input type="hidden" value="poppyz-statistics" name="page" />
                                <?php echo __( 'From: '  , 'poppyz') ?><input type="text" class="short date-picker" id="ppy-start-date" name="start-date" value="<?php if ( isset( $_GET['start-date'] ) ) echo $_GET['start-date']; ?>" />
                                <?php echo __( 'To: '  , 'poppyz') ?><input type="text" class="short date-picker" id="ppy-end-date" name="end-date" value="<?php if ( isset( $_GET['end-date'] ) ) echo $_GET['end-date']; ?>" />
<!--                                <input type="submit" id="get-sales" name="get-sales" class="button" value="--><?php //echo __( 'Show' , 'poppyz'); ?><!--" />-->
                                <button class="button" id="get-sales"><?php echo __( 'Show' , 'poppyz'); ?></button>

                            </div>
                        <h3 class="range range_turnover"><?php echo $range_html; ?></h3>
                        <table class="widefat ppy-admin-table" id="turnovers-table">
                            <thead>
                                <tr>
                                    <th><?php echo  __( 'Invoice ID' , 'poppyz');?></th>
                                    <th><?php echo __( 'Subscription ID' , 'poppyz');?></th>
                                    <th><?php echo __( 'Tier'  , 'poppyz');?></th>
                                    <th><?php echo __( 'Payment Date' , 'poppyz');?></th>
                                    <th><?php echo __( 'Amount'  , 'poppyz');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                </div>
<!--            --><?php //endif;?>
            <div style="clear: both;"></div>
        </div>
		<?php if ( isset($_GET['statistics']) && $_GET['statistics'] != "sales-list" ) :?>
            <?php
            $start_date = date( 'Y-m-d 00:00:00' );
            $end_date  = date( 'Y-m-d 12:60:60' );
            $period = '';
            if ( isset( $_GET['graph-period'] ) ) {
                $period = $_GET['graph-period'];

                if ( $period == 'last_year' ) {
                    $start_date =  $ly_start_date;
                    $end_date =  $ly_end_date;

                } elseif ( $period == 'last_month' ) {
                    $start_date =  $lm_start_date;
                    $end_date =  $lm_end_date;
                }
                else {
                    $start_date = $ty_start_date;
                    $end_date = $ty_end_date;
                }
            } else {
                $start_date = $ty_start_date;
                $end_date = $ty_end_date;
            }
            $total_statistics = Poppyz_Statistics::get_total_statistics($start_date, $end_date, false);

            if ( $period == 'last_month') {
                foreach ( $total_statistics['data_by_day'] as $d => $v ) {
                    $bar_data[] = array( strval($d), intval( $v ) );
                }
            } else {
                foreach ( $total_statistics['data_by_month'] as $m => $v ) {
                    $bar_data[] = array( $m, intval( $v ) );
                }
            }

            ?>
		<?php endif;?>
            <?php
            $graph_title = __('Earnings this year', 'poppyz');
            $bar_graph_main_title  =  __( 'Turnover per month' , 'poppyz');
            if ( $period == 'last_year' ) {
                $graph_title =  __( 'Earnings last year', 'poppyz');
            } elseif ( $period == 'last_month') {
                $bar_graph_main_title  =  __( 'Turnover per day' , 'poppyz');
                $graph_title =  __( 'Earnings last month' , 'poppyz');
            }
            ?>

        <div class="white-box" id="graph-period">
            <div class="statistics-loading-graph-period"><span class="statistics-loader"></span></div>
                <div class="ppy-settings-heading-wrapper">
                    <div class="ppy-settings-header">
                        <h2 class="statistics-h2 bar-graph-h2"><?php echo $bar_graph_main_title; ?></h2>
                    </div>
                </div>
                <div class="container">
                    <form class="filter-form" method="get" action="<?php //echo $current_url . '#graph-period'; ?>">
                        <div class="filters">
                            <?php Poppyz_Statistics::filter_links( 'graph-period', $period, 'filter-earnings' ); ?>
                            <input type="hidden" value="poppyz-statistics" name="page" />
                            <!--<a <?php /*if ( $period == '' ) echo 'class=selected' */?> href="<?php /*echo $current_url */?>?page=poppyz-statistics#graph-period" ><?php /*echo __( 'This year' ) */?></a>  -
                            <a <?php /*if ( $period == 'last_year' ) echo 'class=selected' */?> href="<?php /*echo $current_url */?>?page=poppyz-statistics&graph-period=last_year#graph-period" ><?php /*echo __( 'Last year' ) */?></a>  -
                            <a <?php /*if ( $period == 'last_month' ) echo 'class=selected' */?> href="<?php /*echo $current_url */?>?page=poppyz-statistics&graph-period=last_month#graph-period" ><?php /*echo __( 'Last month' ) */?></a>-->
                        </div>
                    </form>
                    <h3 class="range"><?php //echo $graph_title; ?></h3>
                    <div id="barchart" class="chart" style="width: 100%; height: 100%;"></div>

                </div>
        </div>

		<?php
            if (isset($_GET['statistics']) && $_GET['statistics'] != "sales-list") {
                $all = false;
                $pie_title = __('Overdue this year', 'poppyz');
                $pie_graph_main_title = __('Overdue', 'poppyz');
                if ($period == 'last_year') {
                    $pie_title = __('Overdue last year', 'poppyz');
                } elseif ($period == 'last_month') {
                    $pie_graph_main_title = __('Overdue per day', 'poppyz');
                    $pie_title = __('Overdue last month', 'poppyz');
                }
                $date_in_last_12_months = date('Y-m-d', strtotime('-12 month', strtotime(date('Y-m-d'))));
                $overdue_start_date = (isset($_GET['from_date'])) ? strtotime($_GET['from_date']) : $date_in_last_12_months;
                $overdue_end_date = (isset($_GET['to_date'])) ? strtotime($_GET['to_date']) : strtotime(strtotime(date('Y-m-d')));


                $period = '';
                if (isset($_GET['graph-period'])) {
                    $period = $_GET['graph-period'];

                    if ($period == 'last_year') {
                        $overdue_start_date = $ly_start_date;
                        $overdue_end_date = $ly_end_date;


                    } elseif ($period == 'last_month') {
                        $overdue_start_date = $lm_start_date;
                        $overdue_end_date = $lm_end_date;
                    } elseif ($period == 'this_year') {
                        $overdue_start_date = $ty_start_date;
                        $overdue_end_date = $ty_end_date;
                    } else if ($period == 'all') {
                        $overdue_start_date = $ty_start_date;
                        $overdue_end_date = $ty_end_date;
                        $all = true;
                    } else {
                        $overdue_start_date = $date_in_last_12_months;
                        $overdue_end_date = date('Y-m-d');
                    }
                } else {
                    $overdue_start_date = $date_in_last_12_months;
                    $overdue_end_date = date('Y-m-d');
                }


                $overdue_percentage = $total_statistics['overdue_percentage'];
                $pie_sub_payment = $total_statistics['data'];
            }
		?>
        <div class="white-box" id="overdue">
            <div class="statistics-loading-overdue"><span class="statistics-loader"></span></div>
            <div id="message" class="poppyz-notice notice-success below-h2 dashicons-before no-overdue-statistics">
                <p><?php echo __("No overdue payments, awesome!", "poppyz"); ?></p>
            </div>

            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2 class="statistics-h2"><?php echo __( 'Percentage of Turnover', 'poppyz'); ?></h2>
                </div>
            </div>
            <div class="container">
                <form class="filter-form" id="overdue-form" method="get" action="<?php echo $current_url . '#overdue'; ?>">
                    <div class="filters">
                        <?php Poppyz_Statistics::filter_links( 'graph-period', $period, 'filter-overdue' ); ?>
                        <input type="hidden" value="poppyz-statistics" name="page" />
                        <!--<a <?php /*if ( $period == '' ) echo 'class=selected' */?> href="<?php /*echo $current_url */?>?page=poppyz-statistics#graph-period" ><?php /*echo __( 'This year' ) */?></a>  -
                        <a <?php /*if ( $period == 'last_year' ) echo 'class=selected' */?> href="<?php /*echo $current_url */?>?page=poppyz-statistics&graph-period=last_year#graph-period" ><?php /*echo __( 'Last year' ) */?></a>  -
                        <a <?php /*if ( $period == 'last_month' ) echo 'class=selected' */?> href="<?php /*echo $current_url */?>?page=poppyz-statistics&graph-period=last_month#graph-period" ><?php /*echo __( 'Last month' ) */?></a>-->
                    </div>
                </form>
                <h3 class="range"><?php echo $pie_title; ?></h3>

                <h3 class="range no-overdue-statistics"><?php echo __('No overdue payments, awesome!'); ?></h3>
                <div id="piechart_3d3" class="chart" style="width: 100%; height: 100%;"></div>

                <div id="legend"></div>
<!--                <table class="widefat ppy-admin-table">-->
<!--					--><?php
//					$table_html =  '<thead><tr>';
//					$table_html .=  '<th>' . __( 'Due Date' , 'poppyz'). '</th>';
//					$table_html .=  '<th>' . __( 'Name (username)' , 'poppyz'). '</th>';
//					$table_html .=  '<th>' . __( 'Course' , 'poppyz') . '</th>';
//					$table_html .=  '<th>' . __( 'Tier'  , 'poppyz') . '</th>';
//					$table_html .=  '<th>' . __( 'Amount' , 'poppyz') . '</th>';
//					$table_html .=  '<th>' . __( 'Interval' , 'poppyz') . '</th>';
//					$table_html .=  '<th></th>';
//					$table_html .=  '</tr></thead>';
//					echo $table_html;
//					?>
<!--					--><?php
//
//					echo $total_statistics['output'];
//					?>
<!--                </table>-->

            </div>
        </div>
        <div class="white-box" id="sold-period">
                <?php
                $period = '';
                if ( isset( $_GET['sold-period'] ) ) {
                    $period = $_GET['sold-period'];

                    if ( $period == 'last_year' ) {
                        $start_date =  $ly_start_date;
                        $end_date =  $ly_end_date;

                    } elseif ( $period == 'last_month' ) {
                        $start_date =  $lm_start_date;
                        $end_date =  $lm_end_date;
                    } else {
                        $start_date = $ty_start_date;
                        $end_date = $ty_end_date;
                    }
                } else {
                    $start_date = null;
                    $end_date = null;
                }
                ?>

                <div class="ppy-settings-heading-wrapper">
                    <div class="ppy-settings-header">
                        <h2  class="statistics-h2"><?php echo __( 'Top Sold tiers', 'poppyz'); ?></h2>
                    </div>
                </div>
                <div class="container">
                    <p>
                        <?php Poppyz_Statistics::filter_links( 'sold-period', $period ); ?>
                    </p>
                    <?php

                    $top_tiers = Poppyz_Statistics::get_top_selling_tiers( $start_date, $end_date, 9999 );
                    if ( $top_tiers ) {
                        $total = 0;
                        $invoice_date = false;
                        if ( $start_date . $end_date ){
                            $invoice_date[0] = $start_date;
                            $invoice_date[1] = $end_date;
                        }
                        echo '<table class="fixed widefat ppy-admin-table">';
                        echo '<tr><th>' . __( 'Tier Name' , 'poppyz') . ' </th><th>' . __( 'Sales', 'poppyz') . '</th><th>' . __( 'Amount' , 'poppyz') . '</th></tr>';
                        $pie_data_tiers = array();
                        foreach ( $top_tiers as $t ) {
                            $sub_total = 0;
                            $invoices = Poppyz_Invoice::get_invoices_by_tier( $t->tier_id, $invoice_date );
                            foreach ( $invoices as $invoice ) {
                                $id = $invoice->ID;
                                $subs_id = $ppy_invoice->get_invoice_field( $id, 'subs_id' );
                                $date_created = $ppy_invoice->get_invoice_field( $id, 'date_created' );
                                $amount = $ppy_invoice->get_invoice_field( $id, 'amount' );
                                $tax_rate = (int)$ppy_invoice->get_invoice_field( $id, 'tax_rate' );
                                $tier_id = $ppy_invoice->get_invoice_field( $id, 'tier_id' );
                                $price_fields = $ppy_invoice::get_price_fields( $id );
                                $total_with_tax = $price_fields['total_with_tax'];
                                $sub_total += $total_with_tax;
                            }

                            $pie_data_tiers[]=  array( (string)get_the_title( $t->tier_id ), intval($t->frequency) );


                            echo '<tr>';
                            echo '<td><a target="_blank" href="' . get_edit_post_link( $t->tier_id) . '">' . get_the_title( $t->tier_id ) . '</a></td>';
                            echo '<td>' . $t->frequency . '</td>';
                            echo '<td>' . Poppyz_Core::format_price( $sub_total ) . '</td>';
                            echo '</tr>';
                        }
                        echo '</table>';

                    }
                    ?>
                    <div id="piechart_3d" class="chart" style="width: 100%; height: 100%;"></div>
                </div>
        </div>
        <div class="white-box" id="future-payments">
            <div class="statistics-loading-future-payments"><span class="statistics-loader"></span></div>
                <?php
                $payment_method = (!empty($_GET['payment_method'])) ? $_GET['payment_method'] : null;
				$date_in_12_months = date('Y-m-d', strtotime('+12 month', strtotime(date('Y-m-d'))));
				$start_date = strtotime(date('Y-m-d'));
				$end_date = strtotime($date_in_12_months);
                ?>
                <div class="ppy-settings-heading-wrapper">
                    <div class="ppy-settings-header">
                        <h2  class="statistics-h2"><?php echo __( 'Future payments: ', 'poppyz');?><span id="total_future_payments_statistics"></span></h2>
                    </div>
                </div>
                <div class="container">
                   <!-- <div>
                        <a <?php /*if ( $period == '' ) echo 'class=selected' */?> href="<?php /*echo $current_url */?>?page=poppyz-statistics#future-payments" ><?php /*echo __( 'View all' ) */?></a>  -
                        <a <?php /*if ( $period == 'this_month' ) echo 'class=selected' */?> href="<?php /*echo $current_url */?>?page=poppyz-statistics&future-period=this_month#future-payments" ><?php /*echo __( 'This month' ) */?></a>  -
                        <a <?php /*if ( $period == 'next_year' ) echo 'class=selected' */?> href="<?php /*echo $current_url */?>?page=poppyz-statistics&future-period=next_year#future-payments" ><?php /*echo __( 'Next 12 months' ) */?></a>
                    </div>
                    <br />-->
                    <div class="poppyz-notice notice-info dashicons-before">
						<?php if (empty($progress))
							echo __('Note: Amounts for memberships are only for the next 12 months.', 'poppyz'); ?>
                    </div>
                    <div id="remaining-payment-form">
                        <div class="pr-filter-wrapper">
                            <input type="hidden" name="page" value="poppyz-statistics" />

                            <div class="field-wrapper">
                                <input type="text" id="ppy-fp-from-date" name="from_date" class="date-picker-range" placeholder="From" value="<?php echo date("Y-m-d",$start_date); ?>">
                            </div>
                            <div class="field-wrapper">
                                <input type="text" id="ppy-fp-to-date" name="to_date" class="date-picker-range" placeholder="To" value="<?php echo date("Y-m-d",$end_date); ?>">
                            </div>
                            <div class="field-wrapper">
                                <select name="payment_method" id="payment_method">
                                    <option value="all"><?php echo __("All", "poppyz");?></option>
                                    <option value="plan" <?php echo (isset($_GET['payment_method']) && $_GET['payment_method'] == "overdue")  ? "selected" : ""; ?>><?php echo __("Plan", "poppyz");?></option>
                                    <option value="membership" <?php echo (isset($_GET['payment_method']) && $_GET['payment_method'] == "payment_method")  ? "selected" : ""; ?>><?php echo __("Membership", "poppyz");?></option>
                                </select>
                            </div>
                            <div class="field-wrapper">
                                <input type="submit" id="get-future-payments" class="ppy_button" name="filter" value="Apply" >
                            </div>
                            <div class="field-wrapper">
                                <input type="submit" id="fp-reset" class="ppy_button" name="reset" value="Reset" >
                            </div>
                        </div>
                        <script>
                            jQuery(document).ready(function($){
                                $('.date-picker-range').datepicker({ dateFormat: 'yy-mm-dd', minDate: 0});
                            });
                        </script>
                    </div>
                    <div id="fp_chart" class="chart" style="width: 100%; height: 500px"></div>
            </div>
        </div>
        <div class="white-box" id="buyer-period">
            <?php
            $period = '';
            if ( isset( $_GET['buyer-period'] ) ) {
                $period = $_GET['buyer-period'];

                if ( $period == 'last_year' ) {
                    $start_date =  $ly_start_date;
                    $end_date =  $ly_end_date;

                } elseif ( $period == 'last_month' ) {
                    $start_date =  $lm_start_date;
                    $end_date =  $lm_end_date;
                } else {
                    $start_date = $ty_start_date;
                    $end_date = $ty_end_date;
                }
            } else {
                $start_date = null;
                $end_date = null;
            }

            ?>

            <div class="ppy-settings-heading-wrapper">
                <div class="ppy-settings-header">
                    <h2 class="statistics-h2"><?php echo __( 'Top 5 Buyers', 'poppyz'); ?></h2>
                </div>
            </div>
            <div class="container">
                <p>
                    <?php Poppyz_Statistics::filter_links( 'buyer-period', $period ); ?>
                </p>
                <?php

                $buyers = Poppyz_Statistics::get_top_buyers( $start_date, $end_date);
                if ( $top_tiers && $buyers ) {
                    echo '<table class="fixed widefat">';
                    echo '<tr><th>' . __( 'User' ) . ' </th><th>' . __( 'Subscriptions' , 'poppyz'). '</td></tr>';
                    $pie_data_users = array();
                    foreach ( $buyers as $b ) {
                        $user_id = $b->user_id;
                        $user = get_user_by( 'ID', $user_id );
                        $name = $name_only = __('User Deleted', 'poppyz');
                        if ( $user ) {
                            $name = $name_only = $user->first_name . ' ' . $user->last_name . ' (' . $user->user_email . ') ';
                            $name = '<a target="_blank" href="' . get_edit_user_link( $user_id ) .  '">' . $name . '</a></td>';
                        }

                        echo '<tr>';
                        echo '<td>' . $name . '</td>';
                        echo '<td>' . $b->frequency . '</td>';
                        echo '</tr>';
                        $pie_data_users[]=  array( $name_only , intval($b->frequency) );
                    }
                    echo '</table>';
                }
                ?>

                <div id="piechart_3d2" class="chart" style="width: 100%; height: 100%;"></div>
            </div>
        </div>
        <hr />
        <div class="white-box" id="students-progress">
			<?php  echo Poppyz_Template::get_template( 'admin/students-progress.php' ); ?>

        </div>
    </div>
</div>
<style>
    #future-payments .date-picker {display: none;}
</style>
<script>
    jQuery(document).ready(function($) {

        google.charts.load('current', {
            packages: ['corechart']
        });
        google.setOnLoadCallback(initChart);

        /*$(window).on("throttledresize", function (event) {
            initChart();
        });*/

        $(window).on("throttledresize", function (event) {
            initChart();
        });

        function initChart() {
            var options = {
                is3D: false,
                width: '100%',
                height: '100%',
                pieSliceText: 'percentage',
                colors: ['#29C4A9', '#EF974A', '#4387D4', '#EF5555', '#EF974A'],
                chartArea: {
                    left: "3%",
                    top: "3%",
                    height: "94%",
                    width: "94%",
                    backgroundColor: '#f6f9fb'
                },
                legend: {
                    position: 'right'
                },
                backgroundColor: '#f6f9fb',


            };
            var options_with_currency = {
                is3D: false,
                width: '100%',
                height: '100%',
                pieSliceText: 'percentage',
                colors: ['#29C4A9', '#EF974A', '#4387D4', '#EF5555', '#EF974A'],
                chartArea: {
                    left: "3%",
                    top: "3%",
                    height: "94%",
                    width: "94%",
                    backgroundColor: '#f6f9fb'
                },
                backgroundColor: '#f6f9fb',


            };
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Tiers');
            data.addColumn('number', 'Sales');

            data.addRows(<?php echo json_encode($pie_data_tiers); ?>);

            drawChart(data, options, 'piechart_3d');

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Tiers');
            data.addColumn('number', 'Sales');

            data.addRows(<?php echo json_encode($pie_data_users); ?>);

            drawChart(data, options, 'piechart_3d2');
        }

        function drawChart(data, options, id) {
            var chart = new google.visualization.PieChart(document.getElementById(id));
            chart.draw(data, options);
        }



        $( ".tabs ul > li a" ).on( "click", function() {
            $(window).resize();
        });

        if(window.location.hash) {
            $(window.location.hash).click();
        }


        $('.date-picker').datepicker({dateFormat: 'yy-mm-dd'});
        $('#filter_subscription').val("<?php echo __( 'Show', 'poppyz') ?>");
        $('#filter_subscription').attr("name", "");


        //populate the tier based on the selected course
        $( document ).on( 'change', 'select.select-course', function() {
            var course_id = $(this).val();
            var data = {
                action: 'populate_tiers',
                security: '<?php echo $ajax_nonce2; ?>',
                course_id: course_id
            };
            $.ajax ({
                type: 'POST',
                url: ajaxurl,
                data: data,
                success: function(response) {
                    $(".select-tier").empty().append(response);
                }
            });
        });

        $('.select-courses').each(function() {
            $courses = $(this); // memorize $(this)
            $tiers = $courses.next('select'); // find a sibling to $this.
            $courses.change(function($courses) {
                return function() {
                    var course_id = $courses.val();
                    var data = {
                        action: 'populate_tiers',
                        security: '<?php echo $ajax_nonce2; ?>',
                        course_id: course_id
                    };
                    $.ajax ({
                        type: 'POST',
                        url: ajaxurl,
                        data: data,
                        success: function(response) {
                            $tiers.empty().append(response);
                        }
                    });
                }
            }($courses));
        });
        $('.remind').click(function(e) {
            e.preventDefault();
            var id = $(this).attr("href");
            $('#reminder-form').fadeIn();
            $('#remind-id').val(id);
            return false;
        });
        $('#cancel-send').click(function(e) {
            e.preventDefault();
            $('#reminder-form').fadeOut();
            //location.reload();
        });



        /*$('#sales-list table').dataTable({
            searching: false,
            order: [],
            "bLengthChange" : false, //thought this line could hide the LengthMenu
            "bInfo":false,
            /!*deferRender:    true,
            scroller:       true,*!/
            "oLanguage": {
                "sSearch": "",
            },
            language: {
                searchPlaceholder: '<?php _e("Search"); ?>'
            }

        });*/

    });
</script>